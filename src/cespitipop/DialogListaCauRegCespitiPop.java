package cespitipop;

/*
 * DialogListaCespiti.java
 *
 * Created on 8 novembre 2006, 11.07
 */

import java.awt.Frame;
import java.awt.event.KeyEvent;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import nucleo.*;

/**
 *
 * @author svilupp
 */
public class DialogListaCauRegCespitiPop extends javax.swing.JDialog {

    private final String COL_CODICE = "Codice";
    private final String COL_DESCRIZIONE = "Descrizione";
    private final String COL_SEGNO = "Segno";
    private final String COL_FISCALE = "Fiscale";
    private final String COL_CIVILISTICA = "Civilistica";
    
    private ConnessioneServer conn;    
    private String codSelez = "";
    
    private int[] id;
    
    private int idSelez = -1;
    private int amm = -1;

    /**
     * Creates new form DialogListaCespiti
     */
    public DialogListaCauRegCespitiPop(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public DialogListaCauRegCespitiPop(Frame parent, boolean modal, ConnessioneServer conn) {
        this(parent, modal);
        this.conn = conn;
        this.amm = -1;
    }

    public DialogListaCauRegCespitiPop(Frame parent, boolean modal, ConnessioneServer conn, int amm) {
        this(parent, modal);
        this.conn = conn;
        this.amm = amm;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        lista = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Lista causali interne di registrazione");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        lista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        lista.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        lista.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                listaKeyPressed(evt);
            }
        });
        lista.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lista);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(707, 319));
    }// </editor-fold>//GEN-END:initComponents

    private void listaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaMouseClicked
        if (evt.getClickCount() == 2 && lista.getRowCount() > 0) {
            idSelez = id[lista.getSelectedRow()];
            codSelez = (String) lista.getValueAt(lista.getSelectedRow(), 0);
            dispose();
        }
    }//GEN-LAST:event_listaMouseClicked

    private void listaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_listaKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER && lista.getRowCount() > 0) {
            idSelez = id[lista.getSelectedRow()];
            codSelez = (String) lista.getValueAt(lista.getSelectedRow(), 0);
            dispose();
        }
    }//GEN-LAST:event_listaKeyPressed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        QueryInterrogazioneClient qRic = null;
        String qry = "";
        Record r = null;

        qry = "SELECT * FROM caubs WHERE bscaustato = 1 ";
        if (amm > -1) {
            qry += " AND bscauraggr = " + amm;
        }
        qry += " ORDER BY bscauraggr,bscaucod";
        qRic = new QueryInterrogazioneClient(conn, qry);
        qRic.apertura();
        int nRec = qRic.numeroRecord();
        
        // crea modello tabella
        final String[] names = {            
            COL_CODICE,COL_DESCRIZIONE,COL_SEGNO,COL_FISCALE,COL_CIVILISTICA
        };
        
        final Object[][] data = new Object[nRec][names.length];
        id = new int[nRec];

        for (int i = 0; i < nRec; i++) {
            r = qRic.successivo();
            data[i][0] = r.leggiStringa("bscaucod");
            data[i][1] = r.leggiStringa("bscaudescr");
            data[i][2] = Formattazione.formatta(r.leggiIntero("bscausegno"), "#0", ',', 2);
            int fc = r.leggiIntero("bscaubinario");
            if (fc == 0) {
                data[i][3] = new Boolean(true);
                data[i][4] = new Boolean(true);
            } else if (fc == 1) {
                data[i][3] = new Boolean(true);
                data[i][4] = new Boolean(false);
            } else if (fc == 2) {
                data[i][4] = new Boolean(true);
                data[i][3] = new Boolean(false);
            }
            id[i] = r.leggiIntero("bscauid");
        }
        
        TableModel modello = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                return false;
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        qRic.chiusura();

        lista.setModel(modello);
        FunzioniUtilita.impostaTabellaPop(lista, true);
        
        lista.getColumn(COL_CODICE).setPreferredWidth(90);
        lista.getColumn(COL_DESCRIZIONE).setPreferredWidth(400);
        lista.getColumn(COL_SEGNO).setPreferredWidth(40);
        lista.getColumn(COL_FISCALE).setPreferredWidth(60);
        lista.getColumn(COL_CIVILISTICA).setPreferredWidth(60);
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
 
    }//GEN-LAST:event_formWindowClosing

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed

    }//GEN-LAST:event_formKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DialogListaCauRegCespitiPop(new javax.swing.JFrame(), true).show();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable lista;
    // End of variables declaration//GEN-END:variables

    public String leggiCodice() {
        return codSelez;
    }

    public int leggiId() {
        return idSelez;
    }
}
