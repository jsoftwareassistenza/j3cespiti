/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cespitipop;

import dbjazz.cespiti.Db_gruppiSpecie;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;
import java.io.File;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import nucleo.ConnessioneServer;
import nucleo.EnvJazz;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogic;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;
import ui.beans.GestTabPop;
import ui.beans.PannelloMenuPop;

import ui.beans.PannelloMenuPopListener;
import ui.beans.PopButton;
import ui.dialog.DialogUiPop;

/**
 *
 * @author pgx71
 */
public class PannelloCruscottoCespiti extends javax.swing.JPanel implements PannelloPop, PannelloMenuPopListener, PannelloPopListener {

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    Statement st = null;

    public Window padre;
    private Frame formGen;
    private JLabel labelWait;
    private WaitPop wait;
    public PannelloPopListener listener = null;
    static public HashMap<String, JPanel> hpannelli = null;

    private String azienda = "";
    private String esContabile = "";
    private String pwdok = "";
    private String key = "";
    private String utente = "";

    PannelloCategorieHome phome = null;

    public static final int UI_MODOCRUSCOTTO_SCHERMOINTERO = 0;
    public static final int UI_MODOCRUSCOTTO_BARRAMENU = 1;
    public int ui_modocruscotto = UI_MODOCRUSCOTTO_SCHERMOINTERO;

    private boolean max = false;

    /**
     * Creates new form PannelloAnnullamentiEadPop
     */
    public PannelloCruscottoCespiti() {
        initComponents();
        nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public PannelloCruscottoCespiti(Frame formGen) {
        this();
        this.formGen = formGen;
    }

    public PannelloCruscottoCespiti(Window padre, Frame formGen) {
        this();
        this.padre = padre;
        this.formGen = formGen;
    }

    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.wait = wait;
        this.key = key;

        hpannelli = new HashMap();

        listener = this;
        pmenu.impostaListener(this);

        String prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cruscottocespiti", "opecespiti");
        String prop1 = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cruscottocespiti", "opecespitiadmin");
        String prop2 = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cruscottocespiti", "tipoaccesso");

        utente = EnvJazz.recOperatore.leggiStringa("opelogin");

        boolean ko = false;

//        routine();
        aggiornaTabelleCespiti(connAz);
        
        if (prop.equals("")) {
            if (prop1.equals("")) {
                System.out.println("Caricamento gruppibs...");
                caricaGruppiBs();
                System.out.println("Caricamento gruppibs OK");

                JOptionPane.showMessageDialog(this, "Primo Accesso", "",
                        JOptionPane.WARNING_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/warning.png")));

                int ris1 = JOptionPane.showConfirmDialog(this, "Inserire obbligatoriamente un operatore per continuare.",
                        "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                if (ris1 == JOptionPane.YES_OPTION) {
                    int tipoutente = EnvJazz.recOperatore.leggiIntero("opeaccesso");
                    int fase = 1;
                    DialogImpostazioniGestioneCespiti d = new DialogImpostazioniGestioneCespiti(formGen, true, connAz, connCom, azienda, esContabile, tipoutente, fase);
                    UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                    d.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(this, "Accedere alla procedura come supervisore", "",
                            JOptionPane.WARNING_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/warning.png")));
                    ko = true;
                }
            }
            else
            {
                boolean verifica=BusinessLogic.esisteTabella(connAz.conn, "gruppibs");
                if(!verifica)
                {
                    System.out.println("Caricamento gruppibs...");
                    caricaGruppiBs();
                    System.out.println("Caricamento gruppibs OK");
                }
            }
        }

        if (!BusinessLogicCespiti.verificaAccessoUtenteCespiti(connAz.conn, utente)) {
            JOptionPane.showMessageDialog(this, "NON abilitato alle funzioni.\nImpossibile proseguire.", "CHIUDO",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            ko = true;
        }

        if (BusinessLogicCespiti.verificaUtenteAdminCespiti(connAz.conn, utente)) {
            pwdok = "pwd";
        } else {
            pwdok = "";
        }

        if (!ko) {
            inizializzaMenuBase();

            hpannelli.clear();
            pTab.removeAll();

            azioneHome();

            correzioneMovimentibs();
            
            inserisciProprietaContabilizzazione();
        }
    }

    private void inizializzaMenuBase() {
        int gfont = grandezzaFontMenu();

        pmenu.impostaListener(this);

        PopButton bm0 = new PopButton();
        bm0.setText("Categorie");
        bm0.setBackground(EnvJazz.COLOR_GIALLO);
        bm0.setDefaultBackground(EnvJazz.COLOR_GIALLO);
        bm0.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm0.setForeground(Color.WHITE);
        bm0.setHighlightedColor(EnvJazz.COLOR_GIALLO);
        bm0.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm0.setFocusPainted(false);
        bm0.setActionKey("Categorie");
        ArrayList<PopButton> vMenum0 = new ArrayList();
        pmenu.aggiungiPopButton(bm0, vMenum0);

        PopButton bm1 = new PopButton();
        bm1.setText("Immobilizzazioni");
        bm1.setBackground(EnvJazz.COLOR_ROSSO);
        bm1.setDefaultBackground(EnvJazz.COLOR_ROSSO);
        bm1.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm1.setForeground(Color.WHITE);
        bm1.setHighlightedColor(EnvJazz.COLOR_ROSSO);
        bm1.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm1.setFocusPainted(false);
        bm1.setActionKey("Ricerca Cespiti");
        ArrayList<PopButton> vMenum1 = new ArrayList();
        pmenu.aggiungiPopButton(bm1, vMenum1);

        PopButton bmv = new PopButton();
        bmv.setText("Incrementi");
        bmv.setBackground(EnvJazz.COLOR_TIFF);
        bmv.setDefaultBackground(EnvJazz.COLOR_TIFF);
        bmv.setDisabledBackgroundColor(Color.DARK_GRAY);
        bmv.setForeground(Color.WHITE);
        bmv.setHighlightedColor(EnvJazz.COLOR_TIFF);
        bmv.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bmv.setFocusPainted(false);
        bmv.setActionKey("Incrementi");

        ArrayList<PopButton> vMenumv = new ArrayList();
        pmenu.aggiungiPopButton(bmv, vMenumv);

        PopButton bma = new PopButton();
        bma.setText("Alienazioni");
        bma.setBackground(EnvJazz.COLOR_BLU);
        bma.setDefaultBackground(EnvJazz.COLOR_BLU);
        bma.setDisabledBackgroundColor(Color.DARK_GRAY);
        bma.setForeground(Color.WHITE);
        bma.setHighlightedColor(EnvJazz.COLOR_BLU);
        bma.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bma.setFocusPainted(false);
        bma.setActionKey("Alienazioni");
        ArrayList<PopButton> vMenuma = new ArrayList();
        pmenu.aggiungiPopButton(bma, vMenuma);

        PopButton bm4 = new PopButton();
        bm4.setText("Documenti");
        bm4.setBackground(EnvJazz.COLOR_GIALLO);
        bm4.setDefaultBackground(EnvJazz.COLOR_GIALLO);
        bm4.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm4.setForeground(Color.WHITE);
        bm4.setHighlightedColor(EnvJazz.COLOR_GIALLO);
        bm4.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm4.setFocusPainted(false);
        bm4.setActionKey("Documenti");
        ArrayList<PopButton> vMenum4 = new ArrayList();
        pmenu.aggiungiPopButton(bm4, vMenum4);

        PopButton bAmmortamenti = new PopButton();
        bAmmortamenti.setText("Ammortamenti");
        bAmmortamenti.setBackground(EnvJazz.COLOR_ROSSO);
        bAmmortamenti.setDefaultBackground(EnvJazz.COLOR_ROSSO);
        bAmmortamenti.setDisabledBackgroundColor(Color.DARK_GRAY);
        bAmmortamenti.setForeground(Color.WHITE);
        bAmmortamenti.setHighlightedColor(EnvJazz.COLOR_ROSSO);
        bAmmortamenti.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bAmmortamenti.setFocusPainted(false);
        bAmmortamenti.setActionKey("ammortamenti");
        ArrayList<PopButton> vMenuamm = new ArrayList();
        pmenu.aggiungiPopButton(bAmmortamenti, vMenuamm);

        PopButton bm5 = new PopButton();
        bm5.setText("Stampe");
        bm5.setBackground(EnvJazz.COLOR_TIFF);
        bm5.setDefaultBackground(EnvJazz.COLOR_TIFF);
        bm5.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm5.setForeground(Color.WHITE);
        bm5.setHighlightedColor(EnvJazz.COLOR_TIFF);
        bm5.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm5.setFocusPainted(false);
        bm5.setActionKey("Stampe");
        ArrayList<PopButton> vbm5 = new ArrayList();
        pmenu.aggiungiPopButton(bm5, vbm5);

        PopButton bm6 = new PopButton();
        bm6.setText("Chiusure");
        bm6.setBackground(EnvJazz.COLOR_BLU);
        bm6.setDefaultBackground(EnvJazz.COLOR_BLU);
        bm6.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm6.setForeground(Color.WHITE);
        bm6.setHighlightedColor(EnvJazz.COLOR_BLU);
        bm6.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm6.setFocusPainted(false);
        bm6.setActionKey("Chiusure");
        ArrayList<PopButton> vbm6 = new ArrayList();
        pmenu.aggiungiPopButton(bm6, vbm6);

        if (ui_modocruscotto == UI_MODOCRUSCOTTO_BARRAMENU) {
            this.setSize(this.getWidth(), 64);
            this.setPreferredSize(new Dimension(this.getWidth(), 64));
            if (padre != null) {
                ((JDialog) padre).getContentPane().setPreferredSize(new Dimension(this.getWidth(), 64));
                padre.pack();
            }
        }

        this.paintAll(this.getGraphics());
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ptesta = new javax.swing.JPanel();
        pmenu = new ui.beans.PannelloMenuPop();
        jPanel2 = new javax.swing.JPanel();
        buttonTuttoSchermo = new ui.beans.PopButton();
        buttonImpostazioni = new ui.beans.PopButton();
        jPanel4 = new javax.swing.JPanel();
        buttonHome = new ui.beans.PopButton();
        pwork = new javax.swing.JPanel();
        pTab = new javax.swing.JTabbedPane();

        setMaximumSize(new java.awt.Dimension(900, 660));
        setMinimumSize(new java.awt.Dimension(900, 660));
        setPreferredSize(new java.awt.Dimension(900, 660));
        setLayout(new java.awt.BorderLayout());

        ptesta.setBackground(java.awt.Color.white);
        ptesta.setPreferredSize(new java.awt.Dimension(900, 42));
        ptesta.setLayout(new java.awt.BorderLayout(4, 0));
        ptesta.add(pmenu, java.awt.BorderLayout.CENTER);

        jPanel2.setMinimumSize(new java.awt.Dimension(80, 25));
        jPanel2.setLayout(new java.awt.BorderLayout());

        buttonTuttoSchermo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_zoom_out_map_black_24dp.png"))); // NOI18N
        buttonTuttoSchermo.setBackground(java.awt.Color.white);
        buttonTuttoSchermo.setBorderPainted(false);
        buttonTuttoSchermo.setFocusPainted(false);
        buttonTuttoSchermo.setHighlightedColor(new java.awt.Color(246, 246, 246));
        buttonTuttoSchermo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        buttonTuttoSchermo.setPreferredSize(new java.awt.Dimension(32, 20));
        buttonTuttoSchermo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonTuttoSchermoActionPerformed(evt);
            }
        });
        jPanel2.add(buttonTuttoSchermo, java.awt.BorderLayout.WEST);

        buttonImpostazioni.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_settings_black_24dp.png"))); // NOI18N
        buttonImpostazioni.setBackground(java.awt.Color.white);
        buttonImpostazioni.setBorderPainted(false);
        buttonImpostazioni.setFocusPainted(false);
        buttonImpostazioni.setHighlightedColor(new java.awt.Color(246, 246, 246));
        buttonImpostazioni.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        buttonImpostazioni.setPreferredSize(new java.awt.Dimension(32, 25));
        buttonImpostazioni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonImpostazioniActionPerformed(evt);
            }
        });
        jPanel2.add(buttonImpostazioni, java.awt.BorderLayout.EAST);

        ptesta.add(jPanel2, java.awt.BorderLayout.LINE_END);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setLayout(new java.awt.BorderLayout());

        buttonHome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_home_black_24dp.png"))); // NOI18N
        buttonHome.setBackground(java.awt.Color.white);
        buttonHome.setBorderPainted(false);
        buttonHome.setFocusPainted(false);
        buttonHome.setHighlightedColor(new java.awt.Color(246, 246, 246));
        buttonHome.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        buttonHome.setPreferredSize(new java.awt.Dimension(40, 25));
        buttonHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonHomeActionPerformed(evt);
            }
        });
        jPanel4.add(buttonHome, java.awt.BorderLayout.WEST);

        ptesta.add(jPanel4, java.awt.BorderLayout.LINE_START);

        add(ptesta, java.awt.BorderLayout.NORTH);

        pwork.setLayout(new java.awt.GridLayout(1, 1));
        pwork.add(pTab);

        add(pwork, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonTuttoSchermoActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonTuttoSchermoActionPerformed
    {//GEN-HEADEREND:event_buttonTuttoSchermoActionPerformed
        if (padre != null) {
            if (!max) {
                Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                padre.setSize((int) d.getWidth(), (int) d.getHeight() - 36);
                padre.setLocation(0, 0);
                padre.paintAll(padre.getGraphics());
                max = true;
            } else {
                Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                padre.setSize(((int) d.getWidth() - 1000) / 2, ((int) d.getHeight() - 700) / 2);
                padre.setLocation(0, 0);
                padre.paintAll(padre.getGraphics());
                max = false;
            }
        }
    }//GEN-LAST:event_buttonTuttoSchermoActionPerformed

    private void buttonImpostazioniActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonImpostazioniActionPerformed
    {//GEN-HEADEREND:event_buttonImpostazioniActionPerformed
        utente = EnvJazz.recOperatore.leggiStringa("opelogin");
        if (BusinessLogicCespiti.verificaAccessoUtenteCespiti(connAz.conn, utente)) {
            if (BusinessLogicCespiti.verificaUtenteAdminCespiti(connAz.conn, utente)) {
                DialogImpostazioniGestioneCespiti d = new DialogImpostazioniGestioneCespiti(formGen, true, connAz, connCom, azienda, esContabile, -1, 2);
                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                //        UtilForm.posRelSchermo(d, UtilForm.DX);
                d.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(this, "Operatore non abilitato", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        }
    }//GEN-LAST:event_buttonImpostazioniActionPerformed

    private void buttonHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonHomeActionPerformed

        hpannelli.clear();
        pTab.removeAll();

        azioneHome();

    }//GEN-LAST:event_buttonHomeActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonHome;
    private ui.beans.PopButton buttonImpostazioni;
    private ui.beans.PopButton buttonTuttoSchermo;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTabbedPane pTab;
    private ui.beans.PannelloMenuPop pmenu;
    private javax.swing.JPanel ptesta;
    private javax.swing.JPanel pwork;
    // End of variables declaration//GEN-END:variables

    public void impostaListener(PannelloPopListener l) {
        listener = l;
    }

    @Override
    public void notificaClickPulsanteMenuPop(PannelloMenuPop origine, String key) {
        System.out.println("EVENTO MENU:" + key);
        int dimschermo = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        int maxalt = dimschermo - ((int) this.getLocationOnScreen().getY() + 64 + 64);
        if (hpannelli.get("home") != null) {
            hpannelli.clear();
            pTab.removeAll();
        }
        if (key.equals("Categorie")) {
            PannelloCategorieCespiti pannelloCateg = new PannelloCategorieCespiti(formGen, formGen, this);
            pannelloCateg.impostaListener(listener);
            pannelloCateg.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "Categorie");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloCateg);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Categorie", pTab, ind, keyass, hpannelli, true, true, EnvJazz.COLOR_GIALLO, Color.BLACK);
                pTab.add((JPanel) pannelloCateg, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloCateg, "Categorie", keyass, EnvJazz.COLOR_GIALLO, Color.BLACK);
                dx.getContentPane().add(pannelloCateg);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("Ricerca Cespiti")) {
            PannelloGestioneCespiti pannelloCespiti = new PannelloGestioneCespiti(formGen, formGen);
            pannelloCespiti.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "Ricerca Cespiti");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloCespiti);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Immobilizzazioni", pTab, ind, keyass, hpannelli, true, true, EnvJazz.COLOR_ROSSO, Color.BLACK);
                pTab.add((JPanel) pannelloCespiti, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloCespiti, "Elenco Cespiti", keyass, EnvJazz.COLOR_ROSSO, Color.BLACK);
                dx.getContentPane().add(pannelloCespiti);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("Incrementi")) {
            PannelloGestioneIncrementi pannelloIncrementi = new PannelloGestioneIncrementi(formGen, formGen);
            pannelloIncrementi.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "Incrementi");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloIncrementi);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Incrementi", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloIncrementi, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloIncrementi, "Incrementi", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloIncrementi);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("Alienazioni")) {
            PannelloGestioneAlienazioni pannelloAlienazioni = new PannelloGestioneAlienazioni(formGen, formGen, this);
            pannelloAlienazioni.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "Alienazioni");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloAlienazioni);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Alienazioni", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloAlienazioni, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloAlienazioni, "Alienazioni", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloAlienazioni);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("Documenti")) {

//            JOptionPane.showMessageDialog(this, "Working in progress", "",
//                        JOptionPane.WARNING_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/warning.png")));
            PannelloRegistrazioneDocumenti pannelloVendita = new PannelloRegistrazioneDocumenti(formGen);
            pannelloVendita.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "Documenti");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloVendita);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Documenti contabili", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloVendita, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloVendita, "Fattura vendita", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloVendita);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("ncacq")) {
            PannelloGestVenditaCespiti pannelloVendita = new PannelloGestVenditaCespiti(formGen, formGen);
            pannelloVendita.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "ncacq");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloVendita);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Nota credito acquisto", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloVendita, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloVendita, "Nota credito acquisto", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloVendita);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("ammortamenti")) {
            PannelloCreaAmmortamenti pannelloAmmorta = new PannelloCreaAmmortamenti(formGen, formGen);
            pannelloAmmorta.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "ammortamenti");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloAmmorta);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Ammortamenti", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloAmmorta, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloAmmorta, "Ammortamenti", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloAmmorta);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("primanota")) {
            PannelloGestVenditaCespiti pannelloVendita = new PannelloGestVenditaCespiti(formGen, formGen);
            pannelloVendita.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "primanota");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloVendita);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "P.n. contabilita' generale", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloVendita, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloVendita, "P.n. contabilita' generale", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloVendita);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("Stampe")) {
            PannelloStampaControlloCespitiPop pannelloStampa = new PannelloStampaControlloCespitiPop(formGen, formGen);
            pannelloStampa.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "Stampe");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloStampa);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Stampa libro", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloStampa, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloStampa, "Stampa libro", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloStampa);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        } else if (key.equals("Chiusure")) {
            PannelloChiusuraEsercizioCespiti pannelloChiudi = new PannelloChiusuraEsercizioCespiti(formGen, formGen);
            pannelloChiudi.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "Chiusura");
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put(keyass, pannelloChiudi);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Chiusura esercizio", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) pannelloChiudi, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, pannelloChiudi, "Chiusura esercizio", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(pannelloChiudi);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        }
    }

    private int grandezzaFontMenu() {
        int gfont = 16;
        if (EnvJazz.risoluzioneX > 1440) {
            gfont = 16;
        }
        if (EnvJazz.risoluzioneX > 1024) {
            gfont = 14;
        } else {
            gfont = 12;
        }
        return gfont;
    }

    @Override
    public void notificaChiusura(PannelloPop origine, String key) {
        System.out.println("CHIUSURA PANNELLO " + key);
        if (hpannelli.get(key) != null) {
            for (int i = 0; i < pTab.getComponentCount(); i++) {
                if (pTab.getComponentAt(i).equals(origine)) {
                    pTab.remove(i);
                    break;
                }
            }
            hpannelli.remove(key);
        }
    }

    @Override
    public void notificaCambioTitolo(PannelloPop origine, String key, String nuovoTitolo) {
    }

    private String calcolaChiavePannello(String key, boolean nuovatab) {
        String keyass = key;
        if (nuovatab) {
            boolean keyok = false;
            int keyprog = 0;
            while (!keyok) {
                keyprog++;
                String k = key + "_" + keyprog;
                boolean ktrovata = false;
                Iterator it = hpannelli.keySet().iterator();
                while (it.hasNext()) {
                    String k2 = (String) it.next();
                    if (k2.equals(k)) {
                        ktrovata = true;
                    }
                }
                if (!ktrovata) {
                    keyass = k;
                    keyok = true;
                }
            }
        }
        return keyass;
    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo) {

    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo, Color coloresfondo, int idVis) {
        apriProgramma(keynuovo, nuovoTitolo, "", nuovoPannello.getClass().getName(), keyorigine, coloresfondo, Color.BLACK, true, idVis, true);

    }

    public void apriProgramma(String key, String etichetta, String eticposfisc, String nomeclasse, String jarpers, Color coloreSfondo, Color coloreTesto, boolean nuovatab, int idVis, boolean inizializza) {
        if (hpannelli.get(key) != null && !nuovatab) {
            pTab.setSelectedComponent(hpannelli.get(key));
        } else {
            String keyass = key;
            if (nuovatab) {
                boolean keyok = false;
                int keyprog = 0;
                while (!keyok) {
                    keyprog++;
                    String k = key + "_" + keyprog;
                    boolean ktrovata = false;
                    Iterator it = hpannelli.keySet().iterator();
                    while (it.hasNext()) {
                        String k2 = (String) it.next();
                        if (k2.equals(k)) {
                            ktrovata = true;
                        }
                    }
                    if (!ktrovata) {
                        keyass = k;
                        keyok = true;
                    }
                }
            }
            System.out.println("KEY ASS:" + keyass);
            try {
                // identifica i parametri
                Class[] c = new Class[1];
                c[0] = Class.forName("java.awt.Frame");
                Class cl = Class.forName(nomeclasse);
                Constructor cc = cl.getConstructor(c);
                Object[] parc = new Object[1];
                parc[0] = formGen;
                Object pannello = cc.newInstance(parc);
                if (inizializza) {
                    Class[] cm = new Class[7];
                    cm[0] = Class.forName("nucleo.ConnessioneServer");
                    cm[1] = Class.forName("nucleo.ConnessioneServer");
                    cm[2] = Class.forName("java.lang.String");
                    cm[3] = Class.forName("java.lang.String");
                    cm[4] = Integer.TYPE;
                    cm[5] = Class.forName("nucleo.WaitPop");
                    cm[6] = Class.forName("java.lang.String");

                    if (nomeclasse.equals("cespitipop.PannelloGestioneCespiti") && jarpers.equals("Categorie")) {
                        ((PannelloGestioneCespiti) pannello).settaDacategoria(true);
                    }

                    Method m = cl.getMethod("inizializza", cm);
                    Object[] parin = new Object[7];
                    parin[0] = connAz;
                    parin[1] = EnvJazz.connComune;
                    parin[2] = EnvJazz.azienda;
                    parin[3] = EnvJazz.esContabile;
                    parin[4] = idVis;
                    parin[5] = wait;
                    parin[6] = keyass;
                    m.invoke(pannello, parin);
                }
                Class[] cm2 = new Class[1];
                cm2[0] = Class.forName("nucleo.PannelloPopListener");
                Method m2 = cl.getMethod("impostaListener", cm2);
                Object[] parin2 = new Object[1];
                parin2[0] = this;
                m2.invoke(pannello, parin2);

                hpannelli.put(keyass, (JPanel) pannello);
                int ind = pTab.getTabCount();

                //PannelloGestTabPop pgesttab = new PannelloGestTabPop(formGen, "<html>" + etichetta + "</html>", eticposfisc, pTab, ind, keyass, hpannelli, true, true, coloreSfondo, coloreTesto);
                GestTabPop pgesttab = new GestTabPop(formGen, etichetta, pTab, ind, keyass, hpannelli, true, true, EnvJazz.COLOR_ROSSO, Color.BLACK);
                pgesttab.setSize(new Dimension(300, 52));
                pgesttab.setPreferredSize(new Dimension(300, 52));
                pTab.add((JPanel) pannello, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);

            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(this, "Errore apertura modulo " + nomeclasse + ":\n" + e.getLocalizedMessage(), "Errore",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        }
    }
    
    private void routine()
    {
        String qry="SELECT * FROM movimentibs WHERE mbstipocf=2";
        QueryInterrogazioneClient query= new QueryInterrogazioneClient(connAz,qry);
        query.apertura();
        for(int i=0; i<query.numeroRecord();i++)
        {
            Record r=query.successivo();
            if(r!=null)
            {
                String qryupdate="UPDATE movimentibs SET mbsammortamento="+r.leggiDouble("mbsammortamento")+","
                        + "mbsammdeducib="+r.leggiDouble("mbsammortamento")+",mbsimpofondo="+r.leggiDouble("mbsimpofondo")+
                        " WHERE mbscespid="+r.leggiIntero("mbscespid")+" AND mbseserc=\""+r.leggiStringa("mbseserc")+"\" AND mbstipocf=1";
                QueryAggiornamentoClient queryagg= new QueryAggiornamentoClient(connAz, qryupdate);
                queryagg.esecuzione();
            }
        }
        
        qry="SELECT * FROM log_amm";
        query= new QueryInterrogazioneClient(connAz,qry);
        query.apertura();
        for(int i=0; i<query.numeroRecord();i++)
        {
            Record r=query.successivo();
            if(r!=null)
            {
                String qryupdate="UPDATE log_amm SET logamm_importof="+r.leggiDouble("logamm_importoc")+",logamm_importod="+r.leggiDouble("logamm_importoc")+
                        " WHERE logamm_eserc=\""+r.leggiStringa("logamm_eserc")+"\" AND logamm_catid="+r.leggiIntero("logamm_catid")+
                                " AND logamm_data=\""+r.leggiStringa("logamm_data")+"\"";
                QueryAggiornamentoClient queryagg= new QueryAggiornamentoClient(connAz, qryupdate);
                queryagg.esecuzione();
            }
        }
        query.chiusura();
        return;
    }

    private boolean aggiornaTabelleCespiti(ConnessioneServer connAz) {
        boolean verifica = true;

        boolean gruppo = false;
        boolean specie = false;
        boolean sotto = false;
        boolean cod = false;
        
        verifica= BusinessLogic.esisteTabella(connAz.conn, "gruppibs");
        if(!verifica)
        {
           Db_gruppiSpecie d= new Db_gruppiSpecie(connAz.conn);
        }
        else
        {
            String qry = "SELECT * FROM gruppibs";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            int n=query.numeroRecord();
            query.chiusura();
            if(n==0)
            {
                caricaGruppiBs();
            }
            
        }

        String qry = "SHOW INDEX FROM categoriebs";
        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
        query.apertura();
        Record r = query.successivo();
        while (r != null) {
            if (r.leggiStringa("COLUMN_NAME").equals("catbsgruppo")) {
                gruppo = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("catbsspecie")) {
                specie = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("catbssotto")) {
                sotto = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("catbscod")) {
                cod = true;
            }
            r = query.successivo();
        }

        if (!gruppo) {
            String qryalter = "ALTER TABLE categoriebs ADD KEY (catbsgruppo)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!specie) {
            String qryalter = "ALTER TABLE categoriebs ADD KEY (catbsspecie)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!sotto) {
            String qryalter = "ALTER TABLE categoriebs ADD KEY (catbssotto)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!cod) {
            String qryalter = "ALTER TABLE categoriebs ADD KEY (catbscod)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        boolean mastro = false;
        boolean conto = false;
        sotto = false;
        qry = "SHOW INDEX FROM gruppibs";
        query = new QueryInterrogazioneClient(connAz, qry);
        query.apertura();
        r = query.successivo();
        while (r != null) {
            if (r.leggiStringa("COLUMN_NAME").equals("gruppobsmastro")) {
                mastro = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("gruppobsconto")) {
                conto = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("gruppobssotto")) {
                sotto = true;
            }
            r = query.successivo();
        }

        if (!mastro) {
            String qryalter = "ALTER TABLE gruppibs ADD KEY (gruppobsmastro)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!conto) {
            String qryalter = "ALTER TABLE gruppibs ADD KEY (gruppobsconto)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!sotto) {
            String qryalter = "ALTER TABLE gruppibs ADD KEY (gruppobssotto)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        boolean mbscespid = false;
        boolean mbsdata = false;
        boolean mbstipomov = false;
        boolean mbstipocf = false;
        boolean mbsimpbene = false;
        boolean mbsimpofondo = false;
        qry = "SHOW INDEX FROM movimentibs";
        query = new QueryInterrogazioneClient(connAz, qry);
        query.apertura();
        r = query.successivo();
        while (r != null) {
            if (r.leggiStringa("COLUMN_NAME").equals("mbscespid")) {
                mbscespid = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("mbsdata")) {
                mbsdata = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("mbstipomov")) {
                mbstipomov = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("mbstipocf")) {
                mbstipocf = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("mbsimpbene")) {
                mbsimpbene = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("mbsimpofondo")) {
                mbsimpofondo = true;
            }

            r = query.successivo();
        }

        if (!mbscespid) {
            String qryalter = "ALTER TABLE movimentibs ADD KEY (mbscespid)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!mbsdata) {
            String qryalter = "ALTER TABLE movimentibs ADD KEY (mbsdata)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!mbstipomov) {
            String qryalter = "ALTER TABLE movimentibs ADD KEY (mbstipomov)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!mbstipocf) {
            String qryalter = "ALTER TABLE movimentibs ADD KEY (mbstipocf)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!mbsimpbene) {
            String qryalter = "ALTER TABLE movimentibs ADD KEY (mbsimpbene)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!mbsimpofondo) {
            String qryalter = "ALTER TABLE movimentibs ADD KEY (mbsimpofondo)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        boolean cespacespid = false;
        boolean cespdataacq = false;
        boolean cespvaloreacqc = false;
        boolean cespeserca = false;
        boolean cespdataesterna = false;
        boolean cespvaloreacq = false;
        qry = "SHOW INDEX FROM cespitiacq";
        query = new QueryInterrogazioneClient(connAz, qry);
        query.apertura();
        r = query.successivo();
        while (r != null) {
            if (r.leggiStringa("COLUMN_NAME").equals("cespacespid")) {
                cespacespid = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("cespdataacq")) {
                cespdataacq = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("cespvaloreacqc")) {
                cespvaloreacqc = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("cespeserca")) {
                cespeserca = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("cespdataesterna")) {
                cespdataesterna = true;
            }

            if (r.leggiStringa("COLUMN_NAME").equals("cespvaloreacq")) {
                cespvaloreacq = true;
            }

            r = query.successivo();
        }

        if (!cespacespid) {
            String qryalter = "ALTER TABLE cespitiacq ADD KEY (cespacespid)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!cespdataacq) {
            String qryalter = "ALTER TABLE cespitiacq ADD KEY (cespdataacq)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!cespvaloreacqc) {
            String qryalter = "ALTER TABLE cespitiacq ADD KEY (cespvaloreacqc)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!cespeserca) {
            String qryalter = "ALTER TABLE cespitiacq ADD KEY (cespeserca)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!cespdataesterna) {
            String qryalter = "ALTER TABLE cespitiacq ADD KEY (cespdataesterna)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        if (!cespvaloreacq) {
            String qryalter = "ALTER TABLE cespitiacq ADD KEY (cespvaloreacq)";
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryalter);
            qa.esecuzione();
        }

        String qryalter2 = "ALTER TABLE cespitiacq CHANGE cespnumesterno cespnumesterno VARCHAR(255) DEFAULT '' NOT NULL";
        QueryAggiornamentoClient qa2 = new QueryAggiornamentoClient(connAz, qryalter2);
        qa2.esecuzione();

        //chiusurebs
        if (BusinessLogic.esisteTabella(connAz.conn, "chiusurebs")) {

            if (!BusinessLogic.esisteCampo(connAz.conn, "chiusurebs", "cbsbenitot")) {
                try {
                    String qryupdate = "ALTER TABLE chiusurebs ADD cbsbenitot VARCHAR(15) NOT NULL default \"\"";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cbsbenitot non inserito in tabella chiusurebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "chiusurebs", "cbsgru")) {
                try {
                    String qryupdate = "ALTER TABLE chiusurebs ADD cbsgru VARCHAR(20) NOT NULL default \"\"";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cbsgru non inserito in tabella chiusurebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "chiusurebs", "cbscategoria")) {
                try {
                    String qryupdate = "ALTER TABLE chiusurebs ADD cbscategoria VARCHAR(255) NOT NULL default \"\"";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cbscategoria non inserito in tabella chiusurebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "chiusurebs", "cbscod")) {
                try {
                    String qryupdate = "ALTER TABLE chiusurebs ADD cbscod VARCHAR(20) NOT NULL default \"\"";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cbscod non inserito in tabella chiusurebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "chiusurebs", "cbsbeniprec")) {
                try {
                    String qryupdate = "ALTER TABLE chiusurebs ADD cbsbeniprec VARCHAR(15) NOT NULL default \"\"";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cbsbeniprec non inserito in tabella chiusurebs");
                    verifica = false;
                    return verifica;
                }
            }

        }

        //movimentibs
        if (BusinessLogic.esisteTabella(connAz.conn, "movimentibs")) {
            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbsinsuss")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbsinsuss double(12,2) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbsinsuss non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbsmovjazzid")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbsmovjazzid int(11) NOT NULL default -1";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbsmovjazzid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbsammindeducib")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbsammindeducib double(12,2) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbsammindeducib non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbsammdeducib")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbsammdeducib double(12,2) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbsammdeducib non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbscaumovid")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbscaumovid int(11) NOT NULL default -1";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbscaumovid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbscriterio")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbscriterio int(11) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbscriterio non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbsimpofondo")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbsimpofondo double(12,2) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbsimpofondo non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbsimpbene")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbsimpbene double(12,2) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbsimpbene non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "movimentibs", "mbstipocf")) {
                try {
                    String qryupdate = "ALTER TABLE movimentibs ADD mbstipocf int(11) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: mbstipocf non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }
        }

        //cespiti
        if (BusinessLogic.esisteTabella(connAz.conn, "cespiti")) {
            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespdatafine")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespdatafine varchar(10) NOT NULL default \'0000-00-00\'";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespdatafine non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespdatainizio")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespdatainizio varchar(10) NOT NULL default \'0000-00-00\'";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespdatainizio non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespserbid")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespserbid int(11) DEFAULT -1 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespserbid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespautoid")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespautoid int(11) DEFAULT -1 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespautoid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespriepilogativo")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespriepilogativo int(11) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespriepilogativo non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespintero")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespintero int(11) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespintero non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespdataalien")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespdataalien varchar(10) NOT NULL default \'0000-00-00\'";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespdataalien non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespiti", "cespmoviniid")) {
                try {
                    String qryupdate = "ALTER TABLE cespiti ADD cespmoviniid int(11) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: cespmoviniid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

        }

        // categoriebs
        if (BusinessLogic.esisteTabella(connAz.conn, "categoriebs")) {

            try {
                DatabaseMetaData dbm = connAz.conn.getMetaData();
                ResultSet rst = dbm.getTables(connAz.conn.getCatalog(), "", "categoriebs", null);
                rst.next();
                try {
                    DatabaseMetaData dbm1 = connAz.conn.getMetaData();
                    ResultSet rsm = dbm1.getColumns(connAz.conn.getCatalog(), "", rst.getString("TABLE_NAME"), "catbsquota1");
                    rsm.next();
                    if (rsm.getInt("DECIMAL_DIGITS") == 3) {
                        st = connAz.conn.createStatement();
                        st.execute("ALTER TABLE " + rst.getString("TABLE_NAME") + " CHANGE catbsquota1 catbsquota1 double(6,2) default 0");
                        st.close();
                        System.out.println("Agg.campo catbsquota1");
                    }
                    rsm.close();
                } catch (Exception et) {
                    System.out.println("Err Agg.campo catbsquota1");
                    et.printStackTrace();
                }
                rst.close();
            } catch (Exception e) {
                System.out.println("Err Agg.campo catbsquota1");
                e.printStackTrace();
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsgruppo")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsgruppo varchar(10) DEFAULT '' NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsgruppo non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsspecie")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsspecie varchar(10) DEFAULT '' NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsspecie non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbssotto")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbssotto varchar(10) DEFAULT '' NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbssotto non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsecc")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsecc varchar(5) DEFAULT '' NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsecc non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsquotac")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsquotac double(6,2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsecc non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsdetriva")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsdetriva double(6,2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsdetriva non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsdetrcosto")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsdetrcosto double(6,2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsdetrcosto non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsmaxcosto")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsmaxcosto double(6,2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsmaxcosto non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsmaxnolo")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsmaxnolo double(6,2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsmaxnolo non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsclasserip")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsclasserip int(0) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsclasserip non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsbilancio")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsbilancio varchar(20) DEFAULT '' NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsbilancio non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsflagtipoammc")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsflagtipoammc int(2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsflagtipoammc non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbscriterio1")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbscriterio1 int(2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbscriterio1 non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbscriterio2")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbscriterio2 int(2) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbscriterio2 non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbspdccostoid1")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbspdccostoid1 INT(11) DEFAULT -1 NOT NULL AFTER catbspdccostoid";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbspdccostoid1 non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsimmpdcid")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsimmpdcid INT(11) DEFAULT -1 NOT NULL AFTER catbspdccostoid1";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsimmpdcid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsfondopdcid")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsfondopdcid INT(11) DEFAULT -1 NOT NULL AFTER catbsimmpdcid";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsfondopdcid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbscostopdcid")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbscostopdcid INT(11) DEFAULT -1 NOT NULL AFTER catbsfondopdcid";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbscostopdcid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbspdccostoid2")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbspdccostoid2 INT(11) DEFAULT -1 NOT NULL AFTER catbspdccostoid1";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbspdccostoid2 non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbspdcplusid")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbspdcplusid INT(11) DEFAULT -1 NOT NULL AFTER catbspdccostoid2";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbspdcplusid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbspdcplusratid")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbspdcplusratid INT(11) DEFAULT -1 NOT NULL AFTER catbspdcplusid";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbspdcplusratid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbspdcminusid")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbspdcminusid INT(11) DEFAULT -1 NOT NULL AFTER catbspdcplusratid";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbspdcminusid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbspdcmanutid")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbspdcmanutid INT(11) DEFAULT -1 NOT NULL AFTER catbspdcminusid";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbspdcmanutid non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            try {
                String qryupdate = "ALTER TABLE categoriebs CHANGE catbscod catbscod VARCHAR(20) DEFAULT \"\" NOT NULL ";
                QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                qa.esecuzione();
            } catch (Exception e0) {
                System.out.println("campo: catbscod non portato a 20");
            }

            try {
                String qryupdate = "ALTER TABLE categoriebs CHANGE catbsdescr catbsdescr VARCHAR(255) DEFAULT \"\" NOT NULL ";
                QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                qa.esecuzione();
            } catch (Exception e0) {
                System.out.println("campo: catbsdescr non portato a 255");
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsquotaf")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsquotaf DOUBLE(6,2) DEFAULT 0.00 NOT NULL AFTER catbsquota1";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsquotaf non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsquotaage")) {
                try {

                    String qryupdate = "ALTER TABLE categoriebs ADD catbsquotaage DOUBLE(6,2) DEFAULT 0.00 NOT NULL AFTER catbsquotaf";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsquotaage non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsdurataanni")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsdurataanni int(3) DEFAULT 0 NOT NULL AFTER catbsquotaage";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsdurataanni non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsfabbrnoamm")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsfabbrnoamm double(6,2) DEFAULT 0 NOT NULL AFTER catbsdurataanni";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsfabbrnoamm non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbstolleranza")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbstolleranza double(6,2) DEFAULT 0.00 NOT NULL AFTER catbsfabbrnoamm";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbstolleranza non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsnoammcf")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsnoammcf int(1) DEFAULT 0 NOT NULL AFTER catbstolleranza";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsnoammcf non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsintero")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsintero int(1) DEFAULT 0 NOT NULL ";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsintero non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsubic")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsubic char(1) DEFAULT \'N\' NOT NULL AFTER catbsintero";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsubic non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsmac")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsmac char(1) DEFAULT \'N\' NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsmac non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsauto")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsauto char(1) DEFAULT \'N\' NOT NULL AFTER catbsmac";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsauto non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsconv")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsconv char(1) DEFAULT \'N\' NOT NULL AFTER catbsauto";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbscata non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbscata")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbscata char(1) DEFAULT \'N\' NOT NULL AFTER catbsconv";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbscata non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "categoriebs", "catbsstato")) {
                try {
                    String qryupdate = "ALTER TABLE categoriebs ADD catbsstato int(1) DEFAULT 0 NOT NULL AFTER catbscata";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: catbsstato non inserito in tabella categoriebs");
                    verifica = false;
                    return verifica;
                }
            }

        }

        //cespitiven
        if (BusinessLogic.esisteTabella(connAz.conn, "cespitiven")) {
            if (!BusinessLogic.esisteCampo(connAz.conn, "cespitiven", "vplusc")) {
                try {
                    String qryupdate = " ALTER TABLE cespitiven ADD vplusc DOUBLE(16,2)  NOT NULL default 0.00";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: vplusc non inserito in tabella cespitiven");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "cespitiven", "vplusflagripc")) {
                try {
                    String qryupdate = " ALTER TABLE cespitiven ADD vplusflagripc INT(11)  NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: vplusflagripc non inserito in tabella cespitiven");
                    verifica = false;
                    return verifica;
                }
            }

        }

        //causali movimentazione cespiti
        if (BusinessLogic.esisteTabella(connAz.conn, "caubs")) {

            if (!BusinessLogic.esisteCampo(connAz.conn, "caubs", "bscaubinario")) {
                try {
                    String qryupdate = " ALTER TABLE caubs ADD bscaubinario INT(11) NOT NULL default 0";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: bsdocjazzcod non inserito in tabella caubs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "caubs", "bsdocjazzcod")) {
                try {
                    String qryupdate = " ALTER TABLE caubs ADD bsdocjazzcod VARCHAR(10) DEFAULT \"\" NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: bsdocjazzcod non inserito in tabella caubs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "caubs", "bsdocjazzcod1")) {
                try {
                    String qryupdate = " ALTER TABLE caubs ADD bsdocjazzcod1 VARCHAR(10) DEFAULT \"\" NOT NULL AFTER bsdocjazzcod";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: bsdocjazzcod1 non inserito in tabella caubs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "caubs", "bsdocjazzcod2")) {
                try {
                    String qryupdate = " ALTER TABLE caubs ADD bsdocjazzcod2 VARCHAR(10) DEFAULT \"\" NOT NULL AFTER bsdocjazzcod1";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: bsdocjazzcod2 non inserito in tabella caubs");
                    verifica = false;
                    return verifica;
                }
            }

            if (!BusinessLogic.esisteCampo(connAz.conn, "caubs", "bscautipo")) {
                try {
                    String qryupdate = " ALTER TABLE caubs ADD bscautipo INT(11) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: bscautipo non inserito in tabella caubs");
                    verifica = false;
                    return verifica;
                }

            } else {
                try {
                    String qryupdate = " ALTER TABLE caubs CHANGE bscautipo bscautipo INT(11) DEFAULT 0 NOT NULL";
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                } catch (Exception e1) {
                    System.out.println("campo: bscaubinario non inserito in tabella caubs");
                    verifica = false;
                    return verifica;
                }
            }
        }
        return verifica;
    }

    private void caricaGruppiBs() {
        try {
            InputStream is = getClass().getResourceAsStream("/cespitipop/risorse/gruppibs.sql");
            Files.copy(is, Paths.get("tmp" + File.separator + "gruppibs.txt"), StandardCopyOption.REPLACE_EXISTING);

            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, "DELETE FROM gruppibs");
            qd.esecuzione();
            RandomAccessFile in = new RandomAccessFile("tmp" + File.separator + "gruppibs.txt", "r");
            String linea = in.readLine();
            while (linea != null) {
                if (linea.trim().toLowerCase().startsWith("CREATE")) {
                    if (!BusinessLogic.esisteTabella(connAz.conn, "gruppibs")) {
                        byte[] txtbufl = linea.getBytes(ISO_8859_1);
                        linea = new String(txtbufl, UTF_8);
                        QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, linea);
                        qins.esecuzione();
                    }
                }

                if (linea.trim().toLowerCase().startsWith("insert")) {
                    byte[] txtbufl = linea.getBytes(ISO_8859_1);
                    linea = new String(txtbufl, UTF_8);
                    QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, linea);
                    qins.esecuzione();
                }
                linea = in.readLine();
            }
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void azioneHome() {
        if (hpannelli.get("home") != null) {
            pTab.setSelectedComponent(hpannelli.get("home"));
        } else {
            int dimschermo = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
            int maxalt = dimschermo - ((int) this.getLocationOnScreen().getY() + 64 + 64);
            phome = new PannelloCategorieHome(formGen, formGen);
            phome.inizializza(connAz, connCom, azienda, esContabile, -1, wait, "home");

            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO) {
                hpannelli.put("home", phome);

                GestTabPop pgesttab = new GestTabPop(formGen, "Tipologie", pTab, 0, "home", hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) phome, 0);
                pTab.setTabComponentAt(0, pgesttab);
                pTab.setSelectedIndex(0);
                pTab.paintAll(pTab.getGraphics());
            } else {
                DialogUiPop dx = new DialogUiPop(formGen, false, phome, "home", "home", new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(phome);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt) {
                    dx.setSize(dx.getWidth(), maxalt);
                }
                dx.setVisible(true);
            }
        }
    }

    
    private void inserisciProprietaContabilizzazione()
    {
        if(BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti", "usacruscotto").equals(""))
        {
            String qry="SELECT * FROM caubs WHERE bscaucod=\"ftven\"";
            QueryInterrogazioneClient query= new QueryInterrogazioneClient(connAz,qry);
            query.apertura();
            Record r= query.successivo();
            query.chiusura();
            if(r!=null)
            {
                if(!r.leggiStringa("bsdocjazzcod").equals(""))
                {
                    BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti", "usacruscotto", "S");
                }
            }        
        }
    }
        
    private void correzioneMovimentibs() {
        ArrayList<Record> gg = new ArrayList<Record>();
        String qry = "SELECT mbsquotaord,mbsquotaappl,mbsnote,mbscespid,cespcategid FROM movimentibs INNER JOIN cespiti ON mbscespid=cespid WHERE mbstipomov=2 "
                + "AND mbscaumovid=17 AND mbsquotaord=0.00 ORDER BY cespcategid";
        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
        query.apertura();
        int idpreccat = -1;
        for (int i = 0; i < query.numeroRecord(); i++) {
            Record r = query.successivo();
            if (idpreccat != r.leggiIntero("cespcategid")) {
                String qry2 = "SELECT catbsquotac,catbsquotaf FROM categoriebs WHERE catbsid=" + r.leggiIntero("cespcategid");
                QueryInterrogazioneClient query2 = new QueryInterrogazioneClient(connAz, qry2);
                query2.apertura();
                Record rcat = query2.successivo();
                query2.chiusura();

                Record r2 = new Record();
                r2.insElem("cespcategid", r.leggiIntero("cespcategid"));
                r2.insElem("catbsquotac", rcat.leggiDouble("catbsquotac"));
                r2.insElem("catbsquotaf", rcat.leggiDouble("catbsquotaf"));
                gg.add(r2);
                idpreccat = r.leggiIntero("cespcategid");
            }
        }

        for (int i = 0; i < gg.size(); i++) {
            String qryupdate = "UPDATE movimentibs INNER JOIN cespiti ON mbscespid=cespid SET mbsquotaord=" + gg.get(i).leggiDouble("catbsquotac") + ",mbsquotaappl=" + gg.get(i).leggiDouble("catbsquotaf")
                    + " WHERE mbstipomov=2 AND mbscaumovid=17 AND mbsquotaord=0.00 AND cespcategid=" + gg.get(i).leggiIntero("cespcategid");
            QueryAggiornamentoClient queryagg = new QueryAggiornamentoClient(connAz, qryupdate);
            queryagg.esecuzione();
        }
    }

}
