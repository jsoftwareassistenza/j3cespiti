/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
 * DialogStampaSchedeArt.java
 *
 * Created on 21-set-2010, 16.31.01
 */
package cespitipop;

import java.awt.Frame;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import nucleo.ConnessioneServer;
import nucleo.Data;
import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.OpValute;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.businesslogic.BusinessLogicImpostazioni;

/**
 *
 * @author halleyport
 */
public class DialogVisualizzaCategorieSTD extends javax.swing.JDialog {

    private ConnessioneServer connAz = null;
    private String keyecc, azienda;

    private final String COL_ECC = "I";
    private final String COL_COD = "Cod.";
    private final String COL_NOME = "Sottocategoria";
    private final String COL_DTI = "Val. da";
    private final String COL_DTF = "a data";
    private final String COL_AGEV = "Agev.";
    private final String COL_DETRIVA = "D. iva";
    private final String COL_DETRC = "Detr.C.";
    private final String COL_FABBR = "% NO amm.";
    private final String COL_CF = "Binario";
    private final String COL_MAXC = "Massimale";
    private final String COL_MAXN = "Mass. nolo";
    private final String COL_CLRIP = "Cl. rip.";
    private final String COL_SEL = "S.";

    ArrayList<Record> vcatbsid = new ArrayList();
    private int[] id;
    Record recc = new Record();
    String gruppoSelez = "";
    String specieSelez = "";
    String flagCriterioFiscale = "0";

    /**
     * Creates new form DialogStampaSchedeArt
     */
    public DialogVisualizzaCategorieSTD(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public DialogVisualizzaCategorieSTD(Frame parent, boolean modal, ConnessioneServer connAz,
            String azienda, String keyecc) {
        this(parent, modal);
        this.connAz = connAz;
        this.azienda = azienda;
        this.keyecc = keyecc;

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovaCateg = new ui.beans.PopButton();
        grucat = new javax.swing.JLabel();
        catbsgruppo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Selezione categorie precaricate");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(tab);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 40));
        jPanel1.setLayout(null);

        buttonNuovaCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovaCateg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setToolTipText("Inserisci sottocategorie selezionate");
        buttonNuovaCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaCategActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovaCateg);
        buttonNuovaCateg.setBounds(32, 4, 56, 32);

        grucat.setBackground(new java.awt.Color(202, 236, 221));
        grucat.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        grucat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        grucat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        grucat.setOpaque(true);
        jPanel1.add(grucat);
        grucat.setBounds(312, 8, 40, 24);

        catbsgruppo.setBackground(new java.awt.Color(202, 236, 221));
        catbsgruppo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        catbsgruppo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        catbsgruppo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        catbsgruppo.setOpaque(true);
        jPanel1.add(catbsgruppo);
        catbsgruppo.setBounds(364, 8, 480, 24);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(1044, 639));
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowOpened
    {//GEN-HEADEREND:event_formWindowOpened

        flagCriterioFiscale = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "flagCriterioFiscale");

        gruppoSelez = "";
        specieSelez = "";
        String qry = "SELECT pval FROM proprieta WHERE pprog = \"cespiti_jazz\" AND pprop = \"gruppo\"";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record r = q.successivo();
        q.chiusura();
        if (r != null) {
            gruppoSelez = r.leggiStringa("pval");
        }

        qry = "SELECT pval FROM proprieta WHERE pprog = \"cespiti_jazz\" AND pprop = \"specie\"";
        q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        r = q.successivo();
        q.chiusura();
        if (r != null) {
            specieSelez = r.leggiStringa("pval");
        }

        qry = "SELECT gruppobsbil,gruppobsecc,gruppobssotto,gruppobscateg,gruppobsaliq FROM gruppibs"
                + " WHERE gruppobsmastro = \"" + gruppoSelez + "\""
                + " AND gruppobsconto = \"" + specieSelez + "\""
                + " AND gruppobssotto <> \"\""
                + " AND gruppobsecc = \"" + keyecc + "\"";
//        System.out.println(qry);
        q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        recc = q.successivo();
        q.chiusura();
        if (recc != null) {
            grucat.setText(recc.leggiStringa("gruppobssotto"));
            catbsgruppo.setText(recc.leggiStringa("gruppobscateg"));
            interrogazione(keyecc);
        }

    }//GEN-LAST:event_formWindowOpened

    private void buttonNuovaCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaCategActionPerformed

        for (int i = 0; i < vcatbsid.size(); i++) {
            Record rb = vcatbsid.get(i);
            if (((Boolean) tab.getValueAt(i, 0)).booleanValue()) {
                Record rins = new Record();
                rins.insElem("catbsgruppo", gruppoSelez);
                rins.insElem("catbsspecie", specieSelez);
                rins.insElem("catbssotto", recc.leggiStringa("gruppobssotto"));
                rins.insElem("catbscod", recc.leggiStringa("gruppobssotto") + rb.leggiStringa("catsscod"));
                rins.insElem("catbsecc", keyecc);
                rins.insElem("catbsdescr", rb.leggiStringa("catssdescr"));
                rins.insElem("catbstipo", (int) 2);
                rins.insElem("catbsquotac", recc.leggiDouble("gruppobsaliq"));
                rins.insElem("catbsquotaf", recc.leggiDouble("gruppobsaliq"));
                double quota = rins.leggiDouble("catbsquotaf") / 2;
                rins.insElem("catbsquota1", quota);

                double tolleranza = 0;
                int duranni = (int) OpValute.arrotondaMat(100 / recc.leggiDouble("gruppobsaliq"), 0);
                double riprova = recc.leggiDouble("gruppobsaliq") * duranni;
                if (riprova != 100) {
                    tolleranza = 0.01;
                } else {
                    tolleranza = 0.00;
                }
                rins.insElem("catbsdurataanni", duranni);
                rins.insElem("catbstolleranza", tolleranza);

                rins.insElem("catbsflagtipoammc", 1);
                
                rins.insElem("catbsdetriva", rb.leggiDouble("catssdetriva"));
                rins.insElem("catbsdetrcosto", rb.leggiDouble("catssdetrcosto"));
                rins.insElem("catbsquotaage", rb.leggiDouble("catssquotaage"));

                rins.insElem("catbsfabbrnoamm", rb.leggiDouble("catssdetrfabbr"));
                rins.insElem("catbsnoammcf", rb.leggiIntero("catssbinario"));
                if (rb.leggiIntero("catssbinario") == 1 && rb.leggiDouble("catssdetrfabbr") == 100.00) {
                    rins.insElem("catbscriterio1", 5);
                } else {
                    if (flagCriterioFiscale.equals("0")) {
                        rins.insElem("catbscriterio1", 2);
                        rins.insElem("catbscriterio2", (int) 1);
                    } else {
                        rins.insElem("catbscriterio1", 1);
                        rins.insElem("catbscriterio2", (int) 0);
                    }
                }
                rins.insElem("catbsmaxcosto", rb.leggiDouble("catssmaxcosto"));
                rins.insElem("catbsmaxnolo", rb.leggiDouble("catssmaxnolo"));

                rins.insElem("catbsclasserip", rb.leggiIntero("catssclmanutid"));

                rins.insElem("catbsbilancio", recc.leggiStringa("gruppobsbil"));
                rins.insElem("catbsintero", (int) 0);
                rins.insElem("catbsstato", (int) 1);
                rins.insElem("catbsivaiddetrab", rb.leggiIntero("catbsivaiddetrab"));
                rins.convalida(true);

                String qry2 = "SELECT catbscod FROM categoriebs WHERE catbscod = \"" + rins.leggiStringa("catbscod") + "\"";
                QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connAz, qry2);
                qc.apertura();
                if (qc.numeroRecord() < 1) {
                    QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "categoriebs", "catbsid", rins);
                    qins.esecuzione();
                }
                qc.chiusura();
            }
        }

    }//GEN-LAST:event_buttonNuovaCategActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                DialogVisualizzaCategorieSTD dialog = new DialogVisualizzaCategorieSTD(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {

                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonNuovaCateg;
    private javax.swing.JLabel catbsgruppo;
    private javax.swing.JLabel grucat;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables
private void interrogazione(String keyi) {
        vcatbsid = new ArrayList();
        String qry = "SELECT * FROM categspeciali WHERE catssstato = 1";

        if (!keyi.equals("")) {
            qry += " AND catsscodecc = \"" + keyi + "\"";
        }
        qry += " ORDER BY catsscod,catssdescr";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vcatbsid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        //
        mostraLista();
    }

    private void mostraLista() {

        final String[] names = {
            COL_SEL, COL_ECC, COL_COD, COL_NOME, COL_DTI, COL_DTF, COL_AGEV, COL_DETRIVA, COL_DETRC, COL_FABBR, COL_CF, COL_MAXC, COL_MAXN, COL_CLRIP
        };
        final Object[][] data = new Object[vcatbsid.size()][names.length];

        id = new int[vcatbsid.size()];

        for (int i = 0; i < vcatbsid.size(); i++) {
            Record rec = vcatbsid.get(i);
            data[i][0] = new Boolean(false);
            data[i][1] = rec.leggiStringa("catsscodecc");
            data[i][2] = rec.leggiStringa("catsscod");
            data[i][3] = rec.leggiStringa("catssdescr");

            data[i][4] = (new Data(rec.leggiStringa("catssdtiniz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][5] = (new Data(rec.leggiStringa("catssdtfine"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][6] = Formattazione.formValuta(rec.leggiDouble("catssquotaage"), 3, 2, 0);
            data[i][7] = Formattazione.formValuta(rec.leggiDouble("catssdetriva"), 3, 2, 0);
            data[i][8] = Formattazione.formValuta(rec.leggiDouble("catssdetrcosto"), 3, 2, 0);
            data[i][9] = Formattazione.formValuta(rec.leggiDouble("catssdetrfabbr"), 3, 2, 0);
            if (rec.leggiIntero("catssbinario") == 0) {
                data[i][10] = "C e F";
            } else if (rec.leggiIntero("catssbinario") == 1) {
                data[i][10] = "solo F";
            } else {
                data[i][10] = "solo C";
            }
            data[i][11] = Formattazione.formValuta(rec.leggiDouble("catssmaxcosto"), 8, 2, 0);
            data[i][12] = Formattazione.formValuta(rec.leggiDouble("catssmaxnolo"), 8, 2, 0);
            String query = "SELECT bsclmanutcod,bsclmanutquota FROM bsclassimanut"
                    + " WHERE bsclmanutid = " + rec.leggiIntero("catssclmanutid");
            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, query);
            q.apertura();
            Record rm = q.successivo();
            if (rm != null) {
                data[i][13] = rm.leggiStringa("bsclmanutcod") + " : " + rm.leggiDouble("bsclmanutquota");
            } else {
                data[i][13] = "";
            }

            id[i] = rec.leggiIntero("catssid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if(col == FunzioniUtilita.getColumnIndex(tab, COL_SEL))
                return true;
                return false;
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tab, true);

        tab.getColumn(COL_SEL).setPreferredWidth(30);
        tab.getColumn(COL_ECC).setPreferredWidth(28);
        tab.getColumn(COL_COD).setPreferredWidth(60);
        tab.getColumn(COL_NOME).setPreferredWidth(280);
        tab.getColumn(COL_DTI).setPreferredWidth(80);
        tab.getColumn(COL_DTF).setPreferredWidth(80);
        tab.getColumn(COL_AGEV).setPreferredWidth(52);
        tab.getColumn(COL_DETRIVA).setPreferredWidth(60);
        tab.getColumn(COL_DETRC).setPreferredWidth(60);
        tab.getColumn(COL_FABBR).setPreferredWidth(70);
        tab.getColumn(COL_CF).setPreferredWidth(60);
        tab.getColumn(COL_MAXC).setPreferredWidth(90);
        tab.getColumn(COL_MAXN).setPreferredWidth(90);
        tab.getColumn(COL_CLRIP).setPreferredWidth(60);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_MAXC).setCellRenderer(rightRenderer);
        tab.getColumn(COL_MAXN).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tab.getColumn(COL_DTI).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DTF).setCellRenderer(centerRenderer);
        tab.getColumn(COL_AGEV).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DETRIVA).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DETRC).setCellRenderer(centerRenderer);
        tab.getColumn(COL_FABBR).setCellRenderer(centerRenderer);
        tab.getColumn(COL_CLRIP).setPreferredWidth(60);

    }

}
