package cespitipop;

import archivibasepop.dialogstart.DialogClientiBasePop;
import documenti.ausiliari.DialogRicercaFtAcconto;
import documentipop.DialogCambioNumDocPop;
import documentipop.DialogGestScadDocPop;
import documentipop.DialogRicercaFattureClientiPop;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v12.DatiPagamentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v12.DettaglioPagamentoType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v12.FatturaElettronicaBodyType;
import it.gov.agenziaentrate.ivaservizi.docs.xsd.fatture.v12.FatturaElettronicaType;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import nucleo.ConnessioneServer;
import nucleo.Data;
import nucleo.EnvJazz;
import nucleo.ExecGestioni;
import nucleo.Formattazione;
import nucleo.FunzioniDB;
import nucleo.QueryAggiornamentoClient;
import nucleo.UtilForm;
import nucleo.PannelloPop;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.PlainDocument;
import static nucleo.Data.asDate;
import nucleo.Debug;
import nucleo.Edit;
import nucleo.FDouble;
import nucleo.FunzioniUtilita;
import nucleo.OpValute;
import nucleo.PannelloPopListener;
import nucleo.Query;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.TxtCellRenderer;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogic;
import nucleo.businesslogic.BusinessLogicAmbiente;
import nucleo.businesslogic.BusinessLogicAttivazione;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.businesslogic.archivibase.BusinessLogicArchiviBase;
import nucleo.businesslogic.archivibase.BusinessLogicArchiviBaseFatturazione;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;
import nucleo.businesslogic.contabilita.BusinessLogicContabilita;
import nucleo.businesslogic.documenti.BusinessLogicDocumenti;
import nucleo.einvoice.DecodificheXML;
import nucleo.funzStringa;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloGestVenditaCespiti extends javax.swing.JPanel implements PannelloPop {

    private final String COL_BUTTON1 = "Button1";
    private final String COL_BUTTON2 = "Button2";
    private final String COL_DTSCADENZA = "Data scadenza";
    private final String COL_IMPORTO = "Importo";
    private final String COL_CODIVA = "Codice IVA";
    private final String COL_ALIQUOTA = "Aliquota";
    private final String COL_IMPONIBILE = "Imponibile";
    private final String COL_IMPOSTA = "Imposta";
    private final String COL_DTDOC = "Data DOC";
    private final String COL_NDOC = "N.DOC";
    private final String COL_TOTDOC = "Totale doc.";
    private final String COL_ACCONTO = "Acconto";
    private final String COL_LUOGOCONS = "Luogo di consegna merce";
    private final String COL_TIPO = "Tipo";
    private final String COL_CODART = "Codice art.";
    private final String COL_DESCR = "Descrizione";
    private final String COL_UM = "UM";
    private final String COL_COLLI = "Colli/crt";
    private final String COL_QTA = "Quantita";
    private final String COL_PREZZO = "Prezzo";
    private final String COL_SCONTO1 = "% Sconto 1";
    private final String COL_SCONTO2 = "% Sconto 2";
    private final String COL_SCONTO3 = "% Sconto 3";
    private final String COL_LOTTO = "N. Ordine";
    private final String COL_OMAGGIO = "Omaggio";
    private final String COL_TIPORIGA = "Tipo riga";
    private final String COL_CODCOMMESSA = "Cod.commessa";
    private final String COL_CUP = "Cod.CUP";
    private final String COL_CIG = "Cod.CIG";

    private final String COL_DOC = "Documento";
    private final String COL_SEZ = "Sezionale";
    private final String COL_DATA = "Data";
    private final String COL_NUM = "Numero";
    private final String COL_IMP = "Imponibile";
    private final String COL_TOT = "Totale doc.";
    private final String COL_IMPRES = "Imp.residuo";
    private final String COL_ELIMINA = "Elimina";
    
    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;

    private boolean ric = false;
    private boolean dlg = false;
    private boolean flag = false;
    private boolean scadmodif = false;
    private boolean mostradoc = false;
    private boolean pagmodif = false;
    private boolean oma = false;
    private boolean allegaPdf = false;
    private boolean daBarCode = false;
    private boolean docModificato = false;
    private boolean chiediconferma = true;

    private String azienda = "";
    private String esContabile = "";
    private String esmov = "";
    private String key = "";
    private String qiva = "";
    private String qiva1 = "";
    private String qiva2 = "";
    private String qiva3 = "";
    private String percorso_alternativo = "";
    private String caucondescr = "";
    private Record raz = new Record();
    private int movdocId = -1;
    private int tipodoc = 0;
    // 0 = fattura
    // 1 = nota credito
    private int depId = -1;
    private int pedId = -1;
    private int listinoId = -1;
    private int rigacorr = -1;
    private int rigacorrdatiagg = -1;
    private int modo = EnvJazz.INS;
    private int iva4t = -1;
    private int iva4i = -1;
    private int iva4n = -1;
    private int iva10t = -1;
    private int iva10i = -1;
    private int iva10n = -1;
    private int iva22t = -1;
    private int iva22i = -1;
    private int iva22n = -1;
    private int pagprecId = -1;
    private int ivaomaggioId = -1;
    private int TAB_NOTACREDITO = 0;
    private int TAB_ORDINI = 1;
    private int TAB_CLIENTE = 2;
    private int TAB_PREFERITI = 3;
    private int TAB_CARRELLO = 4;
    private int TAB_RIEPILOGO = 5;
    private int TAB_DATIAGG = 6;
    private int TAB_RITENUTA = 7;
    private int TAB_ALLEGATI = 8;
    private int cauId = -1;
    private int docfatturaId = -1;

    private boolean[] foto;

    private Record rDoc = null;
    
    private int tipologia=-1;

    private ArrayList<Record> vrighe = null;
    private ArrayList<Record> vdettpar = null;
//    private ArrayList<Record> vpref = null;
    private TreeMap<String, Record> hiva = null;
    private ArrayList<Record> vscad = null;
    private ArrayList<Record> vdatiagg = null;
    private ArrayList<Record> vord = null;
    private ArrayList<Record> vnotac = null;
    private ArrayList<Record> vfattacconto= new ArrayList<Record>();
    public ArrayList<Record> vallegati = null;
    public ArrayList<Record> vdettddt = null;
    private ArrayList<Record> listcesp;
    private ArrayList<Record> cespitidaregistrare = new ArrayList<Record>();

    private double sclicalc1 = 0;
    private double sclicalc2 = 0;
    private double sclicalc3 = 0;
    private double ivasplitp = 0;
    private double dif=0.00;

    private Record rcli = null;
    private Record rSez = null;
    private Record rDoc1 = null;
    private Record rcaumag = null;
    private Record rpardoc = null;
    private Record rordcorr = null;
    private Record rftcorr = null;
    private Record rCliente = null;
    private Record rpar = null;
    private Record rivadoc=null;
    private Record rfac=null;

    private double totbene = 0.00;
    private double totfondo = 0.00;
    private double totresto = 0.00;    
    
    public Vector listaAnticipi = new Vector();

    private PannelloPopListener listener = null;

    //private String percorso_emessi = "";
    //private String percorso_elaborati = "";
    public ArrayList<String> ves;

    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();
//    boolean neg = false;

    public PannelloGestVenditaCespiti() {
        this.initComponents();
        this.setSize(1250, 734);
        this.setPreferredSize(new Dimension(1250, 734));
        this.setMinimumSize(new Dimension(1250, 734));

        waitloc.impostaColoreSfondo(Color.WHITE);
        label_datacons.setVisible(false);
        datacons.setVisible(false);
        alink.setDocument(new PlainDocument());

        waitloc.ferma();

        qta.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                ricalcolaRiga();
            }
        });

        prezzo.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                ricalcolaRiga();
            }
        });

        sc1.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                ricalcolaRiga();
            }
        });

        totpag.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                //if (totpag.getValore() > 0)
                {
                    if (pagamento.leggiId() != -1 && datamov.getData() != null && !scadmodif) {
                        vscad = new ArrayList();
                        Record rpag = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "pagamenti", "pagid", pagamento.leggiId(), "");
                        double totresiduo = OpValute.arrotondaMat(Formattazione.estraiDouble(totnetto.getText()) - totpag.getValore(), 2);
                        ArrayList<Record> vscadtmp = BusinessLogicDocumenti.calcolaScadenzeDocumento(connAz.leggiConnessione(), rpag, totresiduo, datamov.getData(), 0, 0, 0, 2);
                        for (int i = 0; i < vscadtmp.size(); i++) {
                            Record rscadtmp = vscadtmp.get(i);
                            Record rsc = new Record();
                            rsc.insElem("data", rscadtmp.leggiStringa("data"));
                            rsc.insElem("importo", rscadtmp.leggiDouble("importo"));
                            vscad.add(rsc);
                        }
                    }
                    mostraScadenze();
                }
            }
        });

        descrart.setEventoKeyPressed(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                if (evento.getKeyCode() == KeyEvent.VK_ENTER) {
                    evento.consume();
                } else if (evento.getKeyCode() == KeyEvent.VK_TAB) {
                    evento.consume();
                    codiva.fuoco();
                }

            }
        });

        notepiede.setEventoKeyPressed(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                if (evento.getKeyCode() == KeyEvent.VK_ENTER) {
                    evento.consume();
                }
            }
        });

        datamov.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                aggiornaNumeroDocumento();
            }
        });

        datamov.setEventoRicercaData(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                aggiornaNumeroDocumento();
            }
        });
    }

    public PannelloGestVenditaCespiti(Frame formGen) {
        this();
        this.formGen = formGen;
    }

    public PannelloGestVenditaCespiti(Window padre, Frame formGen) {
        this();
        this.padre = padre;
        this.formGen = formGen;
    }

    public void impostaParametri(int tipodoc) {
        this.tipodoc = tipodoc;
    }

    public void impostaRecDoc(Record rDoc) {
        this.rDoc = rDoc;
    }
    
    public void impostaModalitaNotaCredito(int tipologia) {
        this.tipologia = tipologia;
    }

    public void impostaCliente(Record rCliente) {
        this.rCliente = rCliente;
    }

    public void impostaRigheDettaglio(ArrayList<Record> vdettpar) {
        this.vdettpar = vdettpar;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int movdocId, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.esmov = esContabile;
        this.key = key;
        this.movdocId = movdocId;

        raz = BusinessLogicArchiviBase.leggiDatiAzienda(EnvJazz.connAmbiente.conn, connCom.conn, azienda);
        if (movdocId != -1) {
            String qry = "SELECT documenti.* FROM movimentidoc" + esmov + " INNER JOIN documenti ON movdocdocumid = docid WHERE movdocid = " + movdocId;
            QueryInterrogazioneClient qdoc = new QueryInterrogazioneClient(connAz, qry);
            qdoc.apertura();
            rDoc = qdoc.successivo();
            qdoc.chiusura();
        }

        if(movdocId!=-1)
        {
            String qryncespiti = "SELECT * FROM cespitiven WHERE cespftjazzid=" + movdocId;
            QueryInterrogazioneClient querycespitinn = new QueryInterrogazioneClient(connAz, qryncespiti);
            querycespitinn.apertura();
            Record rcespiten = querycespitinn.successivo();
            if(rcespiten!=null)
            {
                tipologia= rcespiten.leggiIntero("cespvtiponc");
            }
            querycespitinn.chiusura();
        }

        if (tipodoc == 0) {
            String qry = "SELECT * FROM documenti WHERE docid = " + rDoc.leggiIntero("docid");
            QueryInterrogazioneClient qdoc = new QueryInterrogazioneClient(connAz, qry);
            qdoc.apertura();
            if (rDoc == null) {
                rDoc = qdoc.successivo();
            }
            qdoc.chiusura();

            String[] campi = {"cauconid", "cauconcod", "caucondescr"};
            Record rcau = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "caucon", "cauconid", rDoc.leggiIntero("doccauconid"), "", campi);
            if (rcau != null) {
                cauId = rcau.leggiIntero("cauconid");
                caucondescr = rcau.leggiStringa("caucondescr");
            }

            qry = "SELECT * FROM sezionalinum WHERE sufxid = " + rDoc.leggiIntero("docsuffnumid");
            QueryInterrogazioneClient qn = new QueryInterrogazioneClient(connAz, qry);
            qn.apertura();
            rSez = qn.successivo();
            qn.chiusura();
            qry = "SELECT * FROM sezionalinum WHERE sufxcod = \"" + rSez.leggiStringa("sufxcod") + "\" AND "
                    + "sufxannoinizio = \"" + esContabile.substring(4, 8) + "\" AND "
                    + "sufxannofine = \"" + esContabile.substring(4, 8) + "\"";
            qn = new QueryInterrogazioneClient(connAz, qry);
            qn.apertura();
            rSez = new Record();
            rSez = qn.successivo();
            qn.chiusura();
        } else if (tipodoc == 4) {
            String qry = "SELECT * FROM documenti WHERE docid = " + rDoc.leggiIntero("docid");
            QueryInterrogazioneClient qdoc = new QueryInterrogazioneClient(connAz, qry);
            qdoc.apertura();
            if (rDoc == null) {
                rDoc = qdoc.successivo();
            }
            qdoc.chiusura();

            String[] campi = {"cauconid", "cauconcod", "caucondescr"};
            Record rcau = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "caucon", "cauconid", rDoc.leggiIntero("doccauconid"), "", campi);
            if (rcau != null) {
                cauId = rcau.leggiIntero("cauconid");
                caucondescr = rcau.leggiStringa("caucondescr");
            }

            qry = "SELECT * FROM sezionalinum WHERE sufxid = " + rDoc.leggiIntero("docsuffnumid");
            QueryInterrogazioneClient qn = new QueryInterrogazioneClient(connAz, qry);
            qn.apertura();
            rSez = qn.successivo();
            qn.chiusura();

            qry = "SELECT * FROM sezionalinum WHERE sufxcod = \"" + rSez.leggiStringa("sufxcod") + "\" AND "
                    + "sufxannoinizio = \"" + esContabile.substring(4, 8) + "\" AND "
                    + "sufxannofine = \"" + esContabile.substring(4, 8) + "\"";
            qn = new QueryInterrogazioneClient(connAz, qry);
            qn.apertura();
            rSez = new Record();
            rSez = qn.successivo();
            qn.chiusura();

        }
        // causali trasporto
        ArrayList<Record> vct = BusinessLogicArchiviBaseFatturazione.ricercaCausaliTrasporto(connAz.leggiConnessione(), -1, "", "", "", 1);
        for (int i = 0; i < vct.size(); i++) {
            Record rct = vct.get(i);
            causaletrasporto.addItem(rct.leggiStringa("ctdescr"));
        }

        // aspetto beni
        ArrayList<Record> vab = BusinessLogicArchiviBaseFatturazione.ricercaAspettoBeni(connAz.leggiConnessione(), -1, "", "", "", 1);
        for (int i = 0; i < vab.size(); i++) {
            Record rab = vab.get(i);
            aspettobeni.addItem(rab.leggiStringa("abdescr"));
        }

        cliente.passaParametri(connAz, connCom, formGen, formGen, nomecli, true);
        cliente.gestioneAggiuntiva(new CambioCliente());
        consegna.passaParametri(connAz, connCom, formGen, formGen, descrcons, false);
        consegna.gestioneAggiuntiva(new CambioConsegna());
        codiva.passaParametri(connCom, formGen, formGen, descraliq, false, "", -1);
        codiva1.passaParametri(connCom, formGen, formGen, descriva, false, "", -1);
        vettore.passaParametri(connAz,connCom, formGen, formGen, descrvettore, false);
        vettore.gestioneAggiuntiva(new CambioVettore());
        pagamento.passaParametri(connAz, connCom, formGen, formGen, descrpag, true);
        pagamento.gestioneAggiuntiva(new CambioPagamento());
        porto.passaParametri(connAz, connCom, formGen, formGen, descrporto, false);

        cercaPercorsoAlternativo();

        numerofisc.setVisible(false);
        label_numfiscale.setVisible(false);

        ptesta_rif.setVisible(false);
        if (tipodoc == 0) {
            label_datamov.setText("Data fattura");
            label_nummov.setText("N.fattura");
            if (movdocId != -1) {
                String qry = "SELECT * FROM eft_archivio" + esContabile + " WHERE eftmovdocid = " + movdocId + " AND eftescontmov = \"" + esContabile + "\"";
                QueryInterrogazioneClient qeft = new QueryInterrogazioneClient(connAz, qry);
                qeft.apertura();
                Record reft = qeft.successivo();
                qeft.chiusura();
                if (reft != null) {
                    if (reft.leggiIntero("eftstato") == 0) {
                        buttonSalva.setEnabled(true);
                    } else {
                        buttonSalva.setEnabled(false);
                    }
                }
            }

            TAB_CLIENTE = 0;
            TAB_CARRELLO = 1;
            TAB_RIEPILOGO = 2;
            TAB_DATIAGG = 3;
            TAB_ALLEGATI = 4;

            ptab.remove(pnotac);
            ptab.setSelectedIndex(TAB_CLIENTE);
        } else if (tipodoc == 4) {
            label_datamov.setText("Data nota di credito");
            label_nummov.setText("N.nota di credito");
            if (movdocId != -1) {
                String qry = "SELECT * FROM eft_archivio" + esContabile + " WHERE eftmovdocid = " + movdocId + " AND eftescontmov = \"" + esContabile + "\"";
                QueryInterrogazioneClient qeft = new QueryInterrogazioneClient(connAz, qry);
                qeft.apertura();
                if (qeft.numeroRecord() > 0) {
                    Record reft = qeft.successivo();
                    if (reft.leggiIntero("eftstato") == 0) {
                        buttonSalva.setEnabled(true);
                    } else {
                        buttonSalva.setEnabled(false);
                    }
                }
            }

            buttonRicercaCespite.setEnabled(false);

            TAB_CLIENTE = 0;
            TAB_NOTACREDITO = 1;
            TAB_CARRELLO = 2;
            TAB_RIEPILOGO = 3;
            TAB_DATIAGG = 4;
            TAB_ALLEGATI = 5;
            ptab.setSelectedIndex(TAB_CLIENTE);
        }

        allegaPdf = (BusinessLogicImpostazioni.leggiProprieta(connAz.leggiConnessione(), "jbridge", "allegapdf").equals("S"));

        String prop = BusinessLogicImpostazioni.leggiProprieta(connAz.leggiConnessione(), "docpop", "deposito");
        if (!prop.equals("")) {
            Record rdep = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "depositi", "depcod", -1, prop);
            if (!rdep.nullo("depid")) {
                depId = rdep.leggiIntero("depid");
            } else {
                JOptionPane.showMessageDialog(this, "Deposito mancante sui parametri documenti POP.\nConfigurare i parametri.",
                        "", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                if (listener != null) {
                    listener.notificaChiusura(this, key);
                }
                if (padre != null) {
                    padre.dispose();
                }
                return;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Deposito mancante sui parametri documenti POP.\nConfigurare i parametri.",
                    "", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            if (listener != null) {
                listener.notificaChiusura(this, key);
            }
            if (padre != null) {
                padre.dispose();
            }
            return;
        }

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.leggiConnessione(), "docpop", "puntoemdoc");
        if (!prop.equals("")) {
            Record rped = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "puntiemdoc", "pedcod", -1, prop);
            if (!rped.nullo("pedid")) {
                pedId = rped.leggiIntero("pedid");
            } else {
                JOptionPane.showMessageDialog(this, "Punto emissione documenti mancante sui parametri documenti POP.\nConfigurare i parametri.",
                        "", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                if (listener != null) {
                    listener.notificaChiusura(this, key);
                }
                if (padre != null) {
                    padre.dispose();
                }
                return;
            }
        } else {
            JOptionPane.showMessageDialog(this, "Punto emissione documenti mancante sui parametri documenti POP.\nConfigurare i parametri.",
                    "", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            if (listener != null) {
                listener.notificaChiusura(this, key);
            }
            if (padre != null) {
                padre.dispose();
            }
            return;
        }

        if (rDoc == null) {
            JOptionPane.showMessageDialog(this, "Documento scelto non configurato sui parametri documenti cespiti.\nConfigurare i parametri.",
                    "", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));

            if (listener != null) {
                listener.notificaChiusura(this, key);
            }

            if (padre != null) {
                padre.dispose();
            }
            return;
        }

        if (movdocId != -1) {
            mostraDocumento(movdocId);
        } else {
            preparaNuovo();
            if (rCliente != null) {
                cliente.visualizzaDati(rCliente.leggiIntero("cliid"), rCliente.leggiStringa("clicod"));
            }

            if (vdettpar != null && vdettpar.size() > 0) {
                for (int i = 0; i < vdettpar.size(); i++) {
                    Record rtmp = vdettpar.get(i);
                    Record rx = new Record();
                    if (rtmp.leggiIntero("tiporiga") == 2) {
                        // descrittivo
                        Record rxd = new Record();
                        rxd.insElem("tiporiga", (int) 2);
                        rxd.insElem("artid", (int) -1);
                        rxd.insElem("artcod", "");
                        rxd.insElem("artdescr", rtmp.leggiStringa("descr"));
//                        rxd.insElem("artum", "");
                        rxd.insElem("ivaid", (int) -1);
                        rxd.insElem("ivacod", "");
                        rxd.insElem("colli", (int) 0);
                        rxd.insElem("qta", (double) 0);
                        rxd.insElem("prezzo", (double) 0);
                        rxd.insElem("scart1", (double) 0);
                        rxd.insElem("scart2", (double) 0);
                        rxd.insElem("scart3", (double) 0);
                        rxd.insElem("sccli1", (double) 0);
                        rxd.insElem("sccli2", (double) 0);
                        rxd.insElem("sccli3", (double) 0);
                        rxd.insElem("esclsctestata", (double) 0);
                        rxd.insElem("lordo", (double) 0);
                        rxd.insElem("importo", (double) 0);
                        rxd.insElem("lotto", "");
                        rxd.insElem("oma", (int) 0);
                        rxd.insElem("serviziodatai", "0000-00-00");
                        rxd.insElem("serviziodataf", "0000-00-00");
                        vrighe.add(rxd);
                    } else {
                        rx.insElem("tiporiga", rtmp.leggiIntero("tiporiga"));
                        rx.insElem("artid", rtmp.leggiIntero("artid"));
                        rx.insElem("artcod", rtmp.leggiStringa("artcod"));
                        rx.insElem("artdescr", rtmp.leggiStringa("descr"));
//                        rx.insElem("artum", rtmp.leggiStringa("um"));
                        int ivaId = rtmp.leggiIntero("ivaid");
                        rx.insElem("ivaid", ivaId);
                        Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                        rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                        rx.insElem("colli", rtmp.leggiIntero("colli"));
                        rx.insElem("qta", rtmp.leggiDouble("qta"));
                        if (rtmp.leggiDouble("prezzo") != 0) {
                            rx.insElem("prezzo", rtmp.leggiDouble("prezzo"));
                            rx.insElem("scart1", (double) 0);
                            rx.insElem("scart2", (double) 0);
                            rx.insElem("scart3", (double) 0);
                            rx.insElem("sccli1", (double) 0);
                            rx.insElem("sccli2", (double) 0);
                            rx.insElem("sccli3", (double) 0);
                            rx.insElem("esclsctestata", (double) 0);
                            double val = rtmp.leggiDouble("qta") * rtmp.leggiDouble("prezzo");
                            rx.insElem("lordo", OpValute.arrotondaMat(val, 2));
                            rx.insElem("importo", OpValute.arrotondaMat(val, 2));
                        } else {
                            double prz = 0;
                            double scart1 = 0, scart2 = 0, scart3 = 0;
                            double sccli1 = 0, sccli2 = 0, sccli3 = 0;
                            double esclsctestata = 0;
                            rx.insElem("prezzo", prz);
                            rx.insElem("scart1", scart1);
                            rx.insElem("scart2", scart2);
                            rx.insElem("scart3", scart3);
                            rx.insElem("sccli1", sccli1);
                            rx.insElem("sccli2", sccli2);
                            rx.insElem("sccli3", sccli3);
                            rx.insElem("esclsctestata", esclsctestata);
                            double val = rtmp.leggiDouble("qta") * prz;
                            if (scart1 != 0) {
                                val -= val * scart1 / 100;
                            }
                            if (scart2 != 0) {
                                val -= val * scart2 / 100;
                            }
                            if (scart3 != 0) {
                                val -= val * scart3 / 100;
                            }
                            if (sccli1 != 0) {
                                val -= val * sccli1 / 100;
                            }
                            if (sccli2 != 0) {
                                val -= val * sccli2 / 100;
                            }
                            if (sccli3 != 0) {
                                val -= val * sccli3 / 100;
                            }
                            rx.insElem("lordo", OpValute.arrotondaMat(rtmp.leggiDouble("qta") * prz, 2));
                            rx.insElem("importo", OpValute.arrotondaMat(val, 2));
                        }
                        rx.insElem("lotto", rtmp.leggiStringa("lotto"));
                        rx.insElem("oma", (int) 0);
                        rx.insElem("serviziodatai", "0000-00-00");
                        rx.insElem("serviziodataf", "0000-00-00");
                        vrighe.add(rx);
                    }
                }
                mostraCarrello();
                ricalcolaTotale();
            }
        }

        valoreparziale.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                calcolaResiduiParziali();
            }
        });

        prezzo.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                ricalcolaRiga();
            }
        });

        sc1.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                ricalcolaRiga();
            }
        });

        imponibilenoiva.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {

                double imp1 = imponibilealiq.getValore();
                double imp2 = imponibilenoiva.getValore();

                double tot = imp1 + imp2;

                importo.setText(Formattazione.formValuta(tot, 12, 2, 2));

                calcolaPlusvalenze();
            }
        });

        imponibilealiq.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {

                double imp1 = imponibilealiq.getValore();
                double imp2 = imponibilenoiva.getValore();

                double tot = imp1 + imp2;

                importo.setText(Formattazione.formValuta(tot, 12, 2, 2));

                calcolaPlusvalenze();
            }
        });

    }

    public void calcolaResiduiParziali() {
        double val = valoreparziale.getValore() / totbene;
        double parzialef = totfondo * val;
        parzialefondo.setText(Formattazione.formatta(OpValute.arrotondaMat(parzialef, 2), "#######0.00", 0));

        double residuo = valoreparziale.getValore() - parzialef;
        residuoparziale.setText(Formattazione.formatta(OpValute.arrotondaMat(residuo, 2), "#######0.00", 0));

        if (imponibilealiq.getValore() > 0) {
            calcolaPlusvalenze();
        }
    }

    public void calcolaPlusvalenze() {
        double valimp = imponibilealiq.getValore();
        double valnoniva = imponibilenoiva.getValore();

        double tot = valimp + valnoniva;

        if (tot > 0) {
            double valplus = tot - Formattazione.estraiDouble(residuoparziale.getText());
            if (valplus > 0) {
                plusvalenza.setText(Formattazione.formatta(OpValute.arrotondaMat(valplus, 2), "#######0.00", 0));
                minusvalenza.setText("");
            } else {
                plusvalenza.setText("");
                minusvalenza.setText(Formattazione.formatta(OpValute.arrotondaMat(valplus, 2), "#######0.00", 0));
            }
        }
    }

    private void ricalcolaPlusvalenzeMinusvalenze() {   
        double valfondo=0.00;
        double valore=0.00;
        HashMap<Integer,Double> fondo= new HashMap<>();
        HashMap<Integer,Double> valorestorico= new HashMap<>();
        HashMap<Integer,Record> padre= new HashMap<>();
        for (int i = 0; i < cespitidaregistrare.size(); i++) 
        {
            Record r = cespitidaregistrare.get(i);
            if(r.leggiIntero("cespid")!=-1)
            {
                if(r.leggiIntero("cespcapoid")!=-1)
                {         
                    if(fondo.containsKey(r.leggiIntero("cespcapoid")))
                    {
                        valfondo=fondo.get(r.leggiIntero("cespcapoid"));
                        valfondo+=r.leggiDouble("valorefondo");
                    }
                    else
                    {
                        valfondo=0.00;
                        valfondo+=r.leggiDouble("valorefondo");
                    }                

                    fondo.put(r.leggiIntero("cespcapoid"), valfondo);

                    if(valorestorico.containsKey(r.leggiIntero("cespcapoid")))
                    {
                        valore=valorestorico.get(r.leggiIntero("cespcapoid"));
                        valore+=r.leggiDouble("valorestorico");
                    }
                    else
                    {
                        valore=0.00;
                        valore+=r.leggiDouble("valorestorico");
                    }
                    valorestorico.put(r.leggiIntero("cespcapoid"), valore);
                }
                else
                {    
                    if(fondo.containsKey(r.leggiIntero("cespid")))
                    {
                        valfondo=fondo.get(r.leggiIntero("cespid"));
                        valfondo+=r.leggiDouble("valorefondo");
                    }
                    else
                    {
                        valfondo=0.00;
                        valfondo+=r.leggiDouble("valorefondo");
                    }

                    fondo.put(r.leggiIntero("cespid"), valfondo);

                    if(valorestorico.containsKey(r.leggiIntero("cespid")))
                    {
                        valore=valorestorico.get(r.leggiIntero("cespid"));
                        valore+=r.leggiDouble("valorestorico");
                    }
                    else
                    {
                        valore=0.00;
                        valore+=r.leggiDouble("valorestorico");
                    }

                    valorestorico.put(r.leggiIntero("cespid"), valore);
                    padre.put(r.leggiIntero("cespid"), r);
                }
            }
        }
            
        Set<Integer> chiavi=valorestorico.keySet();
        Iterator it = chiavi.iterator();
        while (it.hasNext()) {
            int cespid=(Integer) it.next();
            double residuo = valorestorico.get(cespid).doubleValue() - fondo.get(cespid).doubleValue();
            
            double tot = padre.get(cespid).leggiDouble("importo");
            double valplus = tot - residuo;
            
            padre.get(cespid).eliminaCampo("plusvalenza");
            padre.get(cespid).eliminaCampo("minusvalenza");

            if (valplus > 0) {
                padre.get(cespid).insElem("minusvalenza", 0.00);
                padre.get(cespid).insElem("plusvalenza", valplus);
            } else {
                padre.get(cespid).insElem("minusvalenza", -valplus);
                padre.get(cespid).insElem("plusvalenza", 0.00);
            }

            for(int i=0;i<cespitidaregistrare.size();i++)
            {
                if(cespitidaregistrare.get(i).leggiIntero("cespid")==cespid)
                {
                    cespitidaregistrare.remove(i);
                    cespitidaregistrare.add(i,padre.get(cespid));
                }
            }
            
        }
        

        

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        documentoTesto1 = new halleytech.models.DocumentoTesto();
        documentoTesto2 = new halleytech.models.DocumentoTesto();
        documentoNumero1 = new halleytech.models.DocumentoNumero();
        documentoNumero2 = new halleytech.models.DocumentoNumero();
        editcolli = new javax.swing.JTextField();
        documentoNumero3 = new halleytech.models.DocumentoNumero();
        editqta = new javax.swing.JTextField();
        documentoValuta1 = new halleytech.models.DocumentoValuta();
        documentoNumero4 = new halleytech.models.DocumentoNumero();
        documentoValuta2 = new halleytech.models.DocumentoValuta();
        documentoValuta3 = new halleytech.models.DocumentoValuta();
        documentoValuta4 = new halleytech.models.DocumentoValuta();
        documentoValuta5 = new halleytech.models.DocumentoValuta();
        documentoValuta6 = new halleytech.models.DocumentoValuta();
        buttonGroup1 = new javax.swing.ButtonGroup();
        ptab = new javax.swing.JTabbedPane();
        ptestata = new javax.swing.JPanel();
        ptesta_rif = new javax.swing.JPanel();
        datarif = new ui.beans.CampoDataPop();
        label_datarif = new javax.swing.JLabel();
        label_numrif = new javax.swing.JLabel();
        numrif = new ui.beans.CampoTestoPop();
        ptesta_pcli = new javax.swing.JPanel();
        pcli1 = new javax.swing.JPanel();
        cliente = new archivibasepop.beans.BeanGestClientePop();
        JLabel62 = new javax.swing.JLabel();
        buttonNuovo = new ui.beans.PopButton();
        pcli2 = new javax.swing.JPanel();
        JLabel56 = new javax.swing.JLabel();
        nomecli = new javax.swing.JLabel();
        JLabel92 = new javax.swing.JLabel();
        indcli = new javax.swing.JLabel();
        JLabel93 = new javax.swing.JLabel();
        loccli = new javax.swing.JLabel();
        pidfisc = new javax.swing.JPanel();
        JLabel94 = new javax.swing.JLabel();
        JLabel46 = new javax.swing.JLabel();
        pidfisc2 = new javax.swing.JPanel();
        pivacli = new javax.swing.JLabel();
        cfcli = new javax.swing.JLabel();
        JLabel95 = new javax.swing.JLabel();
        tipocli = new javax.swing.JLabel();
        ptesta_dest = new javax.swing.JPanel();
        pdest1 = new javax.swing.JPanel();
        consegna = new archivibasepop.beans.BeanGestConsegnaPop();
        JLabel59 = new javax.swing.JLabel();
        pdest2 = new javax.swing.JPanel();
        JLabel52 = new javax.swing.JLabel();
        descrcons = new javax.swing.JLabel();
        JLabel54 = new javax.swing.JLabel();
        indcons = new javax.swing.JLabel();
        JLabel50 = new javax.swing.JLabel();
        loccons = new javax.swing.JLabel();
        ptesta_pag = new javax.swing.JPanel();
        ppag1 = new javax.swing.JPanel();
        pagamento = new archivibasepop.beans.BeanGestPagamentoPop();
        JLabel86 = new javax.swing.JLabel();
        ppag2 = new javax.swing.JPanel();
        JLabel51 = new javax.swing.JLabel();
        descrpag = new javax.swing.JLabel();
        ptesta_sconti = new javax.swing.JPanel();
        pnotac = new javax.swing.JPanel();
        jScrollNotac = new javax.swing.JScrollPane();
        tabnotac = new javax.swing.JTable();
        JLabel64 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        label_sogg9 = new javax.swing.JLabel();
        notac_numft = new ui.beans.CampoTestoPop();
        label_sogg15 = new javax.swing.JLabel();
        notac_dataft = new ui.beans.CampoDataPop();
        buttonInsNotac = new ui.beans.PopButton();
        JLabel66 = new javax.swing.JLabel();
        notac_ftselez = new javax.swing.JLabel();
        buttonRicercaFatt = new ui.beans.PopButton();
        pacconto = new javax.swing.JPanel();
        jScrollNotac1 = new javax.swing.JScrollPane();
        tabnotac1 = new javax.swing.JTable();
        JLabel70 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        label_sogg11 = new javax.swing.JLabel();
        notac_numft1 = new ui.beans.CampoTestoPop();
        label_sogg16 = new javax.swing.JLabel();
        notac_dataft1 = new ui.beans.CampoDataPop();
        buttonInsFtacconto = new ui.beans.PopButton();
        JLabel81 = new javax.swing.JLabel();
        notac_ftselez1 = new javax.swing.JLabel();
        buttonRicercaFatt1 = new ui.beans.PopButton();
        label_sogg12 = new javax.swing.JLabel();
        imponibilealiq1 = new ui.beans.CampoValutaPop();
        pcarrello = new javax.swing.JPanel();
        pcarr1 = new javax.swing.JPanel();
        pScrollCarrello = new javax.swing.JScrollPane();
        tabcarrello = new javax.swing.JTable();
        pcomcarrello = new javax.swing.JPanel();
        buttonSpostaGiu = new ui.beans.PopButton();
        buttonSpostaSu = new ui.beans.PopButton();
        pinsriga = new javax.swing.JPanel();
        descriva = new ui.beans.PopDescrLabelLineBorder();
        codiva = new archivibasepop.beans.BeanGestIvaPop();
        JLabel67 = new javax.swing.JLabel();
        qta = new ui.beans.CampoValutaPop();
        JLabel53 = new javax.swing.JLabel();
        prezzo = new ui.beans.CampoValutaPop();
        JLabel69 = new javax.swing.JLabel();
        sc1 = new ui.beans.CampoValutaPop();
        JLabel71 = new javax.swing.JLabel();
        importo = new javax.swing.JLabel();
        JLabel48 = new javax.swing.JLabel();
        buttonIns = new ui.beans.PopButton();
        JLabel55 = new javax.swing.JLabel();
        descrart = new ui.beans.CampoTestoMultilineaPop();
        JLabel84 = new javax.swing.JLabel();
        buttonScorporo = new ui.beans.PopButton();
        label_um = new javax.swing.JLabel();
        buttonRicercaCespite = new ui.beans.PopButton();
        imponibilealiq = new ui.beans.CampoValutaPop();
        imponibilenoiva = new ui.beans.CampoValutaPop();
        descraliq = new ui.beans.PopDescrLabelLineBorder();
        label_um2 = new javax.swing.JLabel();
        codiva1 = new archivibasepop.beans.BeanGestIvaPop();
        JLabel57 = new javax.swing.JLabel();
        residuostorico = new javax.swing.JLabel();
        jLabminuss3 = new javax.swing.JLabel();
        valoreparziale = new ui.beans.CampoValutaPop();
        parzialefondo = new javax.swing.JLabel();
        JLabel58 = new javax.swing.JLabel();
        JLabel60 = new javax.swing.JLabel();
        valorestorico = new javax.swing.JLabel();
        JLabel61 = new javax.swing.JLabel();
        valorefondo = new javax.swing.JLabel();
        JLabel63 = new javax.swing.JLabel();
        residuoparziale = new javax.swing.JLabel();
        jLabminuss1 = new javax.swing.JLabel();
        jLabminuss2 = new javax.swing.JLabel();
        plusvalenza = new javax.swing.JLabel();
        minusvalenza = new javax.swing.JLabel();
        priep = new javax.swing.JPanel();
        pdatitrasp = new javax.swing.JPanel();
        JLabel73 = new javax.swing.JLabel();
        JLabel2 = new javax.swing.JLabel();
        tipotrasporto = new javax.swing.JComboBox();
        JLabel3 = new javax.swing.JLabel();
        causaletrasporto = new javax.swing.JComboBox();
        JLabel10 = new javax.swing.JLabel();
        aspettobeni = new javax.swing.JComboBox();
        porto = new archivibasepop.beans.BeanGestPortoPop();
        JLabel8 = new javax.swing.JLabel();
        descrporto = new ui.beans.PopDescrLabelLineBorder();
        orapart = new ui.beans.CampoOraPop();
        datapart = new ui.beans.CampoDataPop();
        JLabel75 = new javax.swing.JLabel();
        targa = new ui.beans.CampoTestoPop();
        JLabel11 = new javax.swing.JLabel();
        totpesonetto = new ui.beans.CampoValutaPop();
        JLabel72 = new javax.swing.JLabel();
        totpesolordo = new ui.beans.CampoValutaPop();
        JLabel80 = new javax.swing.JLabel();
        totpallets = new ui.beans.CampoInteroPop();
        JLabel78 = new javax.swing.JLabel();
        totcolli = new ui.beans.CampoInteroPop();
        JLabel83 = new javax.swing.JLabel();
        notepiede = new ui.beans.CampoTestoMultilineaPop();
        JLabel79 = new javax.swing.JLabel();
        JLabel74 = new javax.swing.JLabel();
        label_scad = new javax.swing.JLabel();
        jScrollIva = new javax.swing.JScrollPane();
        tabiva = new javax.swing.JTable();
        jScrollScad = new javax.swing.JScrollPane();
        tabscad = new javax.swing.JTable();
        buttonInsScad = new ui.beans.PopButton();
        avvisoscad = new javax.swing.JLabel();
        totscad = new javax.swing.JLabel();
        label_totscad = new javax.swing.JLabel();
        label_totpag = new javax.swing.JLabel();
        JLabel68 = new javax.swing.JLabel();
        JLabel65 = new javax.swing.JLabel();
        JLabel76 = new javax.swing.JLabel();
        totimp = new javax.swing.JLabel();
        totiva = new javax.swing.JLabel();
        totdoc = new javax.swing.JLabel();
        totpag = new ui.beans.CampoValutaPop();
        pvettore = new javax.swing.JPanel();
        vettore = new archivibasepop.beans.BeanGestVettorePop();
        JLabel9 = new javax.swing.JLabel();
        descrvettore = new ui.beans.PopDescrLabelLineBorder();
        JLabel87 = new javax.swing.JLabel();
        oraritiro = new ui.beans.CampoOraPop();
        dataritiro = new ui.beans.CampoDataPop();
        JLabel77 = new javax.swing.JLabel();
        totaccred = new javax.swing.JLabel();
        JLabel82 = new javax.swing.JLabel();
        totnetto = new javax.swing.JLabel();
        JLabel85 = new javax.swing.JLabel();
        totabbuono = new javax.swing.JLabel();
        pdatiagg = new javax.swing.JPanel();
        jScrollDatiAgg = new javax.swing.JScrollPane();
        tabdatiagg = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        label_sogg6 = new javax.swing.JLabel();
        datiagg_tiporec = new javax.swing.JComboBox();
        label_sogg5 = new javax.swing.JLabel();
        label_sogg8 = new javax.swing.JLabel();
        datiagg_codcommessa = new ui.beans.CampoTestoPop();
        datiagg_codcig = new ui.beans.CampoTestoPop();
        label_sogg4 = new javax.swing.JLabel();
        datiagg_codcup = new ui.beans.CampoTestoPop();
        label_sogg10 = new javax.swing.JLabel();
        datiagg_numdoc = new ui.beans.CampoTestoPop();
        label_sogg7 = new javax.swing.JLabel();
        datiagg_datadoc = new ui.beans.CampoDataPop();
        buttonInsDatiAgg = new ui.beans.PopButton();
        datiagg_impbollo = new ui.beans.CampoValutaPop();
        label_sogg17 = new javax.swing.JLabel();
        datiagg_bollovirtuale = new javax.swing.JCheckBox();
        pallegati = new javax.swing.JPanel();
        jScrollAll = new javax.swing.JScrollPane();
        taball = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        label_sogg25 = new javax.swing.JLabel();
        label_sogg26 = new javax.swing.JLabel();
        label_sogg27 = new javax.swing.JLabel();
        label_sogg28 = new javax.swing.JLabel();
        label_sogg29 = new javax.swing.JLabel();
        aalgcompr = new javax.swing.JComboBox();
        anome = new ui.beans.CampoTestoPop();
        alink = new ui.beans.CampoTestoPop();
        buttonRicFileAll = new ui.beans.PopButton();
        aformato = new ui.beans.CampoTestoPop();
        adescr = new ui.beans.CampoTestoPop();
        buttonInsAllegati = new ui.beans.PopButton();
        pcom = new javax.swing.JPanel();
        pcom1 = new javax.swing.JPanel();
        buttonSalva = new ui.beans.PopButton();
        waitloc = new nucleo.WaitJazzBianco();
        clifenodescr = new javax.swing.JCheckBox();
        pcom2 = new javax.swing.JPanel();
        ptesta = new javax.swing.JPanel();
        ptesta2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        label_datamov = new javax.swing.JLabel();
        datamov = new ui.beans.CampoDataPop();
        buttonModNum = new ui.beans.PopButton();
        nomecli2 = new javax.swing.JLabel();
        totdoc2 = new javax.swing.JLabel();
        label_datacons = new javax.swing.JLabel();
        JLabel88 = new javax.swing.JLabel();
        datacons = new ui.beans.CampoDataPop();
        JLabel89 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        label_nummov = new javax.swing.JLabel();
        numero = new javax.swing.JLabel();
        stato = new javax.swing.JLabel();
        label_numfiscale = new javax.swing.JLabel();
        numerofisc = new ui.beans.CampoInteroPop();

        documentoTesto1.scriviLung(10);

        documentoTesto2.scriviLung(10);

        editcolli.setDocument(documentoNumero3);
        editcolli.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        editqta.setDocument(documentoValuta1);
        editqta.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        documentoValuta1.scriviDecimali(3);
        documentoValuta1.scriviInteri(12);

        documentoValuta2.scriviDecimali(3);
        documentoValuta2.scriviInteri(12);

        documentoValuta3.scriviDecimali(4);
        documentoValuta3.scriviInteri(12);

        documentoValuta4.scriviInteri(3);

        documentoValuta5.scriviInteri(3);

        documentoValuta6.scriviInteri(3);

        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMinimumSize(new java.awt.Dimension(1250, 734));
        setPreferredSize(new java.awt.Dimension(1250, 734));
        setLayout(new java.awt.BorderLayout());

        ptab.setBackground(new java.awt.Color(255, 255, 255));
        ptab.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        ptab.setPreferredSize(new java.awt.Dimension(1338, 610));

        ptestata.setBackground(java.awt.Color.white);
        ptestata.setForeground(new java.awt.Color(10, 150, 150));
        ptestata.setLayout(new javax.swing.BoxLayout(ptestata, javax.swing.BoxLayout.Y_AXIS));

        ptesta_rif.setBackground(java.awt.Color.white);
        ptesta_rif.setMaximumSize(new java.awt.Dimension(4000, 40));
        ptesta_rif.setMinimumSize(new java.awt.Dimension(600, 40));
        ptesta_rif.setLayout(null);
        ptesta_rif.add(datarif);
        datarif.setBounds(324, 4, 120, 24);

        label_datarif.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_datarif.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_datarif.setText("Eventuale DDT trasporto   data");
        label_datarif.setMaximumSize(new java.awt.Dimension(95, 16));
        label_datarif.setMinimumSize(new java.awt.Dimension(95, 16));
        label_datarif.setPreferredSize(new java.awt.Dimension(95, 16));
        ptesta_rif.add(label_datarif);
        label_datarif.setBounds(108, 4, 212, 24);

        label_numrif.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_numrif.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_numrif.setText("Num.riferimento");
        label_numrif.setMaximumSize(new java.awt.Dimension(95, 16));
        label_numrif.setMinimumSize(new java.awt.Dimension(95, 16));
        label_numrif.setPreferredSize(new java.awt.Dimension(95, 16));
        ptesta_rif.add(label_numrif);
        label_numrif.setBounds(448, 4, 116, 24);
        ptesta_rif.add(numrif);
        numrif.setBounds(568, 4, 200, 24);

        ptestata.add(ptesta_rif);

        ptesta_pcli.setBackground(java.awt.Color.white);
        ptesta_pcli.setMaximumSize(new java.awt.Dimension(4000, 240));
        ptesta_pcli.setMinimumSize(new java.awt.Dimension(95, 240));
        ptesta_pcli.setPreferredSize(new java.awt.Dimension(415, 240));
        ptesta_pcli.setLayout(new java.awt.BorderLayout());

        pcli1.setBackground(java.awt.Color.white);
        pcli1.setPreferredSize(new java.awt.Dimension(320, 10));
        pcli1.setLayout(null);
        pcli1.add(cliente);
        cliente.setBounds(144, 24, 172, 24);

        JLabel62.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel62.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel62.setText("Cliente");
        JLabel62.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel62.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel62.setPreferredSize(new java.awt.Dimension(95, 16));
        pcli1.add(JLabel62);
        JLabel62.setBounds(84, 24, 56, 24);

        buttonNuovo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ESTRATTO_CONTO_NEW.png"))); // NOI18N
        buttonNuovo.setToolTipText("Scheda contabile");
        buttonNuovo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovoActionPerformed(evt);
            }
        });
        pcli1.add(buttonNuovo);
        buttonNuovo.setBounds(52, 24, 24, 24);

        ptesta_pcli.add(pcli1, java.awt.BorderLayout.WEST);

        pcli2.setBackground(java.awt.Color.white);
        pcli2.setLayout(new java.awt.GridLayout(10, 0));

        JLabel56.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel56.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel56.setText("Nominativo");
        JLabel56.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel56.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel56.setPreferredSize(new java.awt.Dimension(95, 16));
        pcli2.add(JLabel56);

        nomecli.setBackground(new java.awt.Color(202, 236, 221));
        nomecli.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        nomecli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        nomecli.setOpaque(true);
        pcli2.add(nomecli);

        JLabel92.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel92.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel92.setText("Indirizzo");
        JLabel92.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel92.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel92.setPreferredSize(new java.awt.Dimension(95, 16));
        pcli2.add(JLabel92);

        indcli.setBackground(new java.awt.Color(202, 236, 221));
        indcli.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        indcli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        indcli.setOpaque(true);
        pcli2.add(indcli);

        JLabel93.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel93.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel93.setText("Località");
        JLabel93.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel93.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel93.setPreferredSize(new java.awt.Dimension(95, 16));
        pcli2.add(JLabel93);

        loccli.setBackground(new java.awt.Color(202, 236, 221));
        loccli.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        loccli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        loccli.setOpaque(true);
        pcli2.add(loccli);

        pidfisc.setBackground(java.awt.Color.white);
        pidfisc.setMaximumSize(new java.awt.Dimension(32767, 48));
        pidfisc.setMinimumSize(new java.awt.Dimension(0, 48));
        pidfisc.setPreferredSize(new java.awt.Dimension(0, 48));
        pidfisc.setLayout(new java.awt.GridLayout(1, 2));

        JLabel94.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel94.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel94.setText("partita IVA");
        JLabel94.setMaximumSize(new java.awt.Dimension(2, 24));
        JLabel94.setMinimumSize(new java.awt.Dimension(2, 24));
        JLabel94.setPreferredSize(new java.awt.Dimension(2, 24));
        pidfisc.add(JLabel94);

        JLabel46.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel46.setText("codice fiscale");
        JLabel46.setMaximumSize(new java.awt.Dimension(2, 24));
        JLabel46.setMinimumSize(new java.awt.Dimension(2, 24));
        JLabel46.setPreferredSize(new java.awt.Dimension(2, 24));
        pidfisc.add(JLabel46);

        pcli2.add(pidfisc);

        pidfisc2.setLayout(new java.awt.GridLayout(1, 2));

        pivacli.setBackground(new java.awt.Color(202, 236, 221));
        pivacli.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        pivacli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        pivacli.setMaximumSize(new java.awt.Dimension(2, 24));
        pivacli.setMinimumSize(new java.awt.Dimension(2, 24));
        pivacli.setOpaque(true);
        pivacli.setPreferredSize(new java.awt.Dimension(2, 24));
        pidfisc2.add(pivacli);

        cfcli.setBackground(new java.awt.Color(202, 236, 221));
        cfcli.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        cfcli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        cfcli.setMaximumSize(new java.awt.Dimension(2, 24));
        cfcli.setMinimumSize(new java.awt.Dimension(2, 24));
        cfcli.setOpaque(true);
        cfcli.setPreferredSize(new java.awt.Dimension(2, 24));
        pidfisc2.add(cfcli);

        pcli2.add(pidfisc2);

        JLabel95.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel95.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel95.setText("tipologia cliente");
        JLabel95.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel95.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel95.setPreferredSize(new java.awt.Dimension(95, 16));
        pcli2.add(JLabel95);

        tipocli.setBackground(new java.awt.Color(202, 236, 221));
        tipocli.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        tipocli.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        tipocli.setOpaque(true);
        pcli2.add(tipocli);

        ptesta_pcli.add(pcli2, java.awt.BorderLayout.CENTER);

        ptestata.add(ptesta_pcli);

        ptesta_dest.setBackground(java.awt.Color.white);
        ptesta_dest.setMaximumSize(new java.awt.Dimension(4000, 144));
        ptesta_dest.setMinimumSize(new java.awt.Dimension(320, 144));
        ptesta_dest.setPreferredSize(new java.awt.Dimension(320, 144));
        ptesta_dest.setLayout(new java.awt.BorderLayout());

        pdest1.setBackground(java.awt.Color.white);
        pdest1.setMinimumSize(new java.awt.Dimension(320, 10));
        pdest1.setPreferredSize(new java.awt.Dimension(320, 10));
        pdest1.setLayout(null);
        pdest1.add(consegna);
        consegna.setBounds(172, 24, 144, 24);

        JLabel59.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel59.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel59.setText("Luogo di consegna");
        JLabel59.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel59.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel59.setPreferredSize(new java.awt.Dimension(95, 16));
        pdest1.add(JLabel59);
        JLabel59.setBounds(24, 24, 144, 24);

        ptesta_dest.add(pdest1, java.awt.BorderLayout.WEST);

        pdest2.setBackground(java.awt.Color.white);
        pdest2.setLayout(new java.awt.GridLayout(6, 0));

        JLabel52.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel52.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel52.setText("Descrizione");
        JLabel52.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel52.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel52.setPreferredSize(new java.awt.Dimension(95, 16));
        pdest2.add(JLabel52);

        descrcons.setBackground(new java.awt.Color(202, 236, 221));
        descrcons.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        descrcons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrcons.setOpaque(true);
        pdest2.add(descrcons);

        JLabel54.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel54.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel54.setText("Indirizzo");
        JLabel54.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel54.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel54.setPreferredSize(new java.awt.Dimension(95, 16));
        pdest2.add(JLabel54);

        indcons.setBackground(new java.awt.Color(202, 236, 221));
        indcons.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        indcons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        indcons.setOpaque(true);
        pdest2.add(indcons);

        JLabel50.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel50.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel50.setText("Località");
        JLabel50.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel50.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel50.setPreferredSize(new java.awt.Dimension(95, 16));
        pdest2.add(JLabel50);

        loccons.setBackground(new java.awt.Color(202, 236, 221));
        loccons.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        loccons.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        loccons.setOpaque(true);
        pdest2.add(loccons);

        ptesta_dest.add(pdest2, java.awt.BorderLayout.CENTER);

        ptestata.add(ptesta_dest);

        ptesta_pag.setBackground(java.awt.Color.white);
        ptesta_pag.setMaximumSize(new java.awt.Dimension(4000, 48));
        ptesta_pag.setMinimumSize(new java.awt.Dimension(320, 48));
        ptesta_pag.setPreferredSize(new java.awt.Dimension(0, 48));
        ptesta_pag.setLayout(new java.awt.BorderLayout());

        ppag1.setBackground(java.awt.Color.white);
        ppag1.setMinimumSize(new java.awt.Dimension(320, 10));
        ppag1.setPreferredSize(new java.awt.Dimension(320, 10));
        ppag1.setLayout(null);
        ppag1.add(pagamento);
        pagamento.setBounds(144, 24, 172, 26);

        JLabel86.setBackground(new java.awt.Color(255, 255, 255));
        JLabel86.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel86.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel86.setText("Pagamento");
        JLabel86.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel86.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel86.setPreferredSize(new java.awt.Dimension(95, 16));
        ppag1.add(JLabel86);
        JLabel86.setBounds(28, 24, 112, 24);

        ptesta_pag.add(ppag1, java.awt.BorderLayout.WEST);

        ppag2.setBackground(java.awt.Color.white);
        ppag2.setLayout(new java.awt.GridLayout(2, 0));

        JLabel51.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        JLabel51.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel51.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel51.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel51.setPreferredSize(new java.awt.Dimension(95, 16));
        ppag2.add(JLabel51);

        descrpag.setBackground(new java.awt.Color(202, 236, 221));
        descrpag.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        descrpag.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrpag.setOpaque(true);
        ppag2.add(descrpag);

        ptesta_pag.add(ppag2, java.awt.BorderLayout.CENTER);

        ptestata.add(ptesta_pag);

        ptesta_sconti.setBackground(java.awt.Color.white);
        ptesta_sconti.setMaximumSize(new java.awt.Dimension(4000, 40));
        ptesta_sconti.setMinimumSize(new java.awt.Dimension(0, 40));
        ptesta_sconti.setPreferredSize(new java.awt.Dimension(0, 40));
        ptesta_sconti.setLayout(null);
        ptestata.add(ptesta_sconti);

        ptab.addTab("Cliente", new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_person_black_24dp.png")), ptestata); // NOI18N

        pnotac.setLayout(new java.awt.BorderLayout());

        tabnotac.setBackground(new java.awt.Color(255, 255, 232));
        tabnotac.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tabnotac.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabnotacMouseClicked(evt);
            }
        });
        jScrollNotac.setViewportView(tabnotac);

        pnotac.add(jScrollNotac, java.awt.BorderLayout.CENTER);

        JLabel64.setBackground(java.awt.Color.white);
        JLabel64.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        JLabel64.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel64.setText("Elenco fatture a cui fa riferimento la nota di credito:");
        JLabel64.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel64.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel64.setOpaque(true);
        JLabel64.setPreferredSize(new java.awt.Dimension(95, 24));
        pnotac.add(JLabel64, java.awt.BorderLayout.NORTH);

        jPanel5.setBackground(java.awt.Color.white);
        jPanel5.setPreferredSize(new java.awt.Dimension(0, 60));
        jPanel5.setLayout(null);

        label_sogg9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg9.setText("N.Fattura");
        jPanel5.add(label_sogg9);
        label_sogg9.setBounds(588, 8, 112, 24);

        notac_numft.setCaratteri(20);
        jPanel5.add(notac_numft);
        notac_numft.setBounds(704, 8, 144, 24);

        label_sogg15.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg15.setText("Data Fattura");
        jPanel5.add(label_sogg15);
        label_sogg15.setBounds(848, 8, 124, 24);
        jPanel5.add(notac_dataft);
        notac_dataft.setBounds(976, 8, 120, 24);

        buttonInsNotac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonInsNotac.setActionCommand("Esegui");
        buttonInsNotac.setDoubleBuffered(true);
        buttonInsNotac.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonInsNotac.setMaximumSize(new java.awt.Dimension(47, 22));
        buttonInsNotac.setMinimumSize(new java.awt.Dimension(47, 22));
        buttonInsNotac.setPreferredSize(new java.awt.Dimension(47, 22));
        buttonInsNotac.setToolTipText("");
        buttonInsNotac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsNotacActionPerformed(evt);
            }
        });
        jPanel5.add(buttonInsNotac);
        buttonInsNotac.setBounds(1192, 8, 44, 44);

        JLabel66.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel66.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel66.setText("Fattura selezionata");
        JLabel66.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel66.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel66.setPreferredSize(new java.awt.Dimension(95, 16));
        jPanel5.add(JLabel66);
        JLabel66.setBounds(8, 8, 128, 44);

        notac_ftselez.setBackground(new java.awt.Color(202, 236, 221));
        notac_ftselez.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        notac_ftselez.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        notac_ftselez.setOpaque(true);
        jPanel5.add(notac_ftselez);
        notac_ftselez.setBounds(140, 8, 332, 44);

        buttonRicercaFatt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_search_white_24dp.png"))); // NOI18N
        buttonRicercaFatt.setActionCommand("Esegui");
        buttonRicercaFatt.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonRicercaFatt.setMaximumSize(new java.awt.Dimension(30, 40));
        buttonRicercaFatt.setMinimumSize(new java.awt.Dimension(30, 40));
        buttonRicercaFatt.setPreferredSize(new java.awt.Dimension(30, 40));
        buttonRicercaFatt.setToolTipText("Ricerca ordine cliente");
        buttonRicercaFatt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRicercaFattActionPerformed(evt);
            }
        });
        jPanel5.add(buttonRicercaFatt);
        buttonRicercaFatt.setBounds(476, 8, 44, 44);

        pnotac.add(jPanel5, java.awt.BorderLayout.SOUTH);

        ptab.addTab("Nota credito", pnotac);

        pacconto.setLayout(new java.awt.BorderLayout());

        tabnotac1.setBackground(new java.awt.Color(255, 255, 232));
        tabnotac1.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tabnotac1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabnotac1MouseClicked(evt);
            }
        });
        jScrollNotac1.setViewportView(tabnotac1);

        pacconto.add(jScrollNotac1, java.awt.BorderLayout.CENTER);

        JLabel70.setBackground(java.awt.Color.white);
        JLabel70.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        JLabel70.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel70.setText("Elenco fatture acconto:");
        JLabel70.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel70.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel70.setOpaque(true);
        JLabel70.setPreferredSize(new java.awt.Dimension(95, 24));
        pacconto.add(JLabel70, java.awt.BorderLayout.NORTH);

        jPanel7.setBackground(java.awt.Color.white);
        jPanel7.setPreferredSize(new java.awt.Dimension(0, 60));
        jPanel7.setLayout(null);

        label_sogg11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg11.setText("Importo");
        jPanel7.add(label_sogg11);
        label_sogg11.setBounds(820, 35, 112, 24);

        notac_numft1.setCaratteri(20);
        jPanel7.add(notac_numft1);
        notac_numft1.setBounds(650, 10, 144, 24);

        label_sogg16.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg16.setText("Data Fattura");
        jPanel7.add(label_sogg16);
        label_sogg16.setBounds(810, 5, 124, 24);
        jPanel7.add(notac_dataft1);
        notac_dataft1.setBounds(940, 5, 120, 24);

        buttonInsFtacconto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonInsFtacconto.setActionCommand("Esegui");
        buttonInsFtacconto.setDoubleBuffered(true);
        buttonInsFtacconto.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonInsFtacconto.setMaximumSize(new java.awt.Dimension(47, 22));
        buttonInsFtacconto.setMinimumSize(new java.awt.Dimension(47, 22));
        buttonInsFtacconto.setPreferredSize(new java.awt.Dimension(47, 22));
        buttonInsFtacconto.setToolTipText("");
        buttonInsFtacconto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsFtaccontoActionPerformed(evt);
            }
        });
        jPanel7.add(buttonInsFtacconto);
        buttonInsFtacconto.setBounds(1190, 10, 44, 44);

        JLabel81.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel81.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel81.setText("Fattura selezionata");
        JLabel81.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel81.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel81.setPreferredSize(new java.awt.Dimension(95, 16));
        jPanel7.add(JLabel81);
        JLabel81.setBounds(8, 8, 128, 44);

        notac_ftselez1.setBackground(new java.awt.Color(202, 236, 221));
        notac_ftselez1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        notac_ftselez1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        notac_ftselez1.setOpaque(true);
        jPanel7.add(notac_ftselez1);
        notac_ftselez1.setBounds(140, 8, 332, 44);

        buttonRicercaFatt1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_search_white_24dp.png"))); // NOI18N
        buttonRicercaFatt1.setActionCommand("Esegui");
        buttonRicercaFatt1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonRicercaFatt1.setMaximumSize(new java.awt.Dimension(30, 40));
        buttonRicercaFatt1.setMinimumSize(new java.awt.Dimension(30, 40));
        buttonRicercaFatt1.setPreferredSize(new java.awt.Dimension(30, 40));
        buttonRicercaFatt1.setToolTipText("Ricerca ordine cliente");
        buttonRicercaFatt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRicercaFatt1ActionPerformed(evt);
            }
        });
        jPanel7.add(buttonRicercaFatt1);
        buttonRicercaFatt1.setBounds(476, 8, 44, 44);

        label_sogg12.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg12.setText("N.Fattura");
        jPanel7.add(label_sogg12);
        label_sogg12.setBounds(540, 10, 112, 24);

        imponibilealiq1.setDecimali(5);
        imponibilealiq1.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        imponibilealiq1.setInteri(12);
        imponibilealiq1.setSegno(2);
        jPanel7.add(imponibilealiq1);
        imponibilealiq1.setBounds(940, 35, 120, 24);

        pacconto.add(jPanel7, java.awt.BorderLayout.SOUTH);

        ptab.addTab("Fattura acconto", pacconto);

        pcarrello.setLayout(new java.awt.BorderLayout());

        pcarr1.setLayout(new java.awt.BorderLayout());

        tabcarrello.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tabcarrello.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabcarrelloMouseClicked(evt);
            }
        });
        pScrollCarrello.setViewportView(tabcarrello);

        pcarr1.add(pScrollCarrello, java.awt.BorderLayout.CENTER);

        pcomcarrello.setBackground(java.awt.Color.white);
        pcomcarrello.setPreferredSize(new java.awt.Dimension(44, 10));
        pcomcarrello.setLayout(null);

        buttonSpostaGiu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_arrow_downward_white_24dp.png"))); // NOI18N
        buttonSpostaGiu.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonSpostaGiu.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonSpostaGiu.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonSpostaGiu.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonSpostaGiu.setToolTipText("Sposta riga selezionata giù");
        buttonSpostaGiu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSpostaGiuActionPerformed(evt);
            }
        });
        pcomcarrello.add(buttonSpostaGiu);
        buttonSpostaGiu.setBounds(4, 48, 36, 36);

        buttonSpostaSu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_arrow_upward_white_24dp.png"))); // NOI18N
        buttonSpostaSu.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonSpostaSu.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonSpostaSu.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonSpostaSu.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonSpostaSu.setToolTipText("Sposta riga selezionata su");
        buttonSpostaSu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSpostaSuActionPerformed(evt);
            }
        });
        pcomcarrello.add(buttonSpostaSu);
        buttonSpostaSu.setBounds(4, 4, 36, 36);

        pcarr1.add(pcomcarrello, java.awt.BorderLayout.EAST);

        pinsriga.setBackground(java.awt.Color.white);
        pinsriga.setPreferredSize(new java.awt.Dimension(0, 200));
        pinsriga.setLayout(null);

        descriva.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        pinsriga.add(descriva);
        descriva.setBounds(730, 64, 156, 24);
        pinsriga.add(codiva);
        codiva.setBounds(660, 30, 68, 24);

        JLabel67.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel67.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel67.setText("Quantita'");
        JLabel67.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel67.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel67.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel67.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel67);
        JLabel67.setBounds(180, 0, 80, 24);

        qta.setDecimali(4);
        qta.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        qta.setInteri(12);
        pinsriga.add(qta);
        qta.setBounds(180, 30, 50, 24);

        JLabel53.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel53.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel53.setText("Prezzo");
        JLabel53.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel53.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel53.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel53.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel53);
        JLabel53.setBounds(250, 0, 84, 24);

        prezzo.setDecimali(5);
        prezzo.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        prezzo.setInteri(12);
        prezzo.setSegno(2);
        pinsriga.add(prezzo);
        prezzo.setBounds(250, 30, 104, 24);

        JLabel69.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel69.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel69.setText("% sc. 1");
        JLabel69.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel69.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel69.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel69.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel69);
        JLabel69.setBounds(390, 0, 64, 24);

        sc1.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        sc1.setInteri(3);
        pinsriga.add(sc1);
        sc1.setBounds(390, 30, 65, 24);

        JLabel71.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel71.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel71.setText("Quota non imponibile");
        JLabel71.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel71.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel71.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel71.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel71);
        JLabel71.setBounds(410, 60, 140, 24);

        importo.setBackground(new java.awt.Color(202, 236, 221));
        importo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        importo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        importo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        importo.setOpaque(true);
        pinsriga.add(importo);
        importo.setBounds(980, 60, 157, 24);

        JLabel48.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel48.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel48.setText("Imponibile vendita B.S.");
        JLabel48.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel48.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel48.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel48.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel48);
        JLabel48.setBounds(980, 30, 156, 24);

        buttonIns.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonIns.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonIns.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonIns.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonIns.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonIns.setToolTipText("Inserisci");
        buttonIns.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsActionPerformed(evt);
            }
        });
        pinsriga.add(buttonIns);
        buttonIns.setBounds(1190, 30, 45, 44);

        JLabel55.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel55.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel55.setText("Descrizione");
        JLabel55.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel55.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel55.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel55.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel55);
        JLabel55.setBounds(630, 95, 80, 24);
        pinsriga.add(descrart);
        descrart.setBounds(710, 95, 530, 100);

        JLabel84.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel84.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel84.setText("Cespite/bene strumentale");
        JLabel84.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel84.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel84.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel84.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel84);
        JLabel84.setBounds(0, 0, 176, 24);

        buttonScorporo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_skip_previous_white_24dp.png"))); // NOI18N
        buttonScorporo.setActionCommand("Esegui");
        buttonScorporo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonScorporo.setMaximumSize(new java.awt.Dimension(30, 40));
        buttonScorporo.setMinimumSize(new java.awt.Dimension(30, 40));
        buttonScorporo.setPreferredSize(new java.awt.Dimension(30, 40));
        buttonScorporo.setToolTipText("Scorporo IVA dal prezzo");
        buttonScorporo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonScorporoActionPerformed(evt);
            }
        });
        pinsriga.add(buttonScorporo);
        buttonScorporo.setBounds(355, 30, 24, 24);

        label_um.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_um.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_um.setText("Imponib.");
        label_um.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        label_um.setMaximumSize(new java.awt.Dimension(95, 16));
        label_um.setMinimumSize(new java.awt.Dimension(95, 16));
        label_um.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(label_um);
        label_um.setBounds(480, 30, 60, 24);

        buttonRicercaCespite.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_zoom_in_white_24dp.png"))); // NOI18N
        buttonRicercaCespite.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonRicercaCespite.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonRicercaCespite.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonRicercaCespite.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonRicercaCespite.setToolTipText("Sposta riga selezionata giù");
        buttonRicercaCespite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRicercaCespiteActionPerformed(evt);
            }
        });
        pinsriga.add(buttonRicercaCespite);
        buttonRicercaCespite.setBounds(32, 28, 92, 24);

        imponibilealiq.setDecimali(5);
        imponibilealiq.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        imponibilealiq.setInteri(12);
        imponibilealiq.setSegno(2);
        pinsriga.add(imponibilealiq);
        imponibilealiq.setBounds(550, 30, 96, 24);

        imponibilenoiva.setDecimali(5);
        imponibilenoiva.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        imponibilenoiva.setInteri(12);
        pinsriga.add(imponibilenoiva);
        imponibilenoiva.setBounds(550, 64, 96, 24);

        descraliq.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        pinsriga.add(descraliq);
        descraliq.setBounds(730, 30, 156, 24);

        label_um2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_um2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_um2.setText("Aliquota");
        label_um2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        label_um2.setMaximumSize(new java.awt.Dimension(95, 16));
        label_um2.setMinimumSize(new java.awt.Dimension(95, 16));
        label_um2.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(label_um2);
        label_um2.setBounds(660, 0, 52, 24);
        pinsriga.add(codiva1);
        codiva1.setBounds(660, 64, 68, 24);

        JLabel57.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel57.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel57.setText("Parziale fondo");
        JLabel57.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel57.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel57.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel57.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel57);
        JLabel57.setBounds(144, 103, 110, 24);

        residuostorico.setBackground(new java.awt.Color(202, 236, 221));
        residuostorico.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        residuostorico.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        residuostorico.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        residuostorico.setOpaque(true);
        pinsriga.add(residuostorico);
        residuostorico.setBounds(30, 175, 92, 24);

        jLabminuss3.setBackground(new java.awt.Color(255, 255, 255));
        jLabminuss3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabminuss3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabminuss3.setText("Valore parziale");
        pinsriga.add(jLabminuss3);
        jLabminuss3.setBounds(144, 55, 110, 24);

        valoreparziale.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        valoreparziale.setInteri(12);
        valoreparziale.setText("Immettere il valore della quota parziale che si vende");
        pinsriga.add(valoreparziale);
        valoreparziale.setBounds(160, 80, 92, 24);

        parzialefondo.setBackground(new java.awt.Color(202, 236, 221));
        parzialefondo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        parzialefondo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        parzialefondo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        parzialefondo.setOpaque(true);
        pinsriga.add(parzialefondo);
        parzialefondo.setBounds(160, 127, 92, 24);

        JLabel58.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel58.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel58.setText("Residuo storico");
        JLabel58.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel58.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel58.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel58.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel58);
        JLabel58.setBounds(0, 151, 120, 24);

        JLabel60.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel60.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel60.setText("Valore storico");
        JLabel60.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel60.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel60.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel60.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel60);
        JLabel60.setBounds(0, 55, 120, 24);

        valorestorico.setBackground(new java.awt.Color(202, 236, 221));
        valorestorico.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        valorestorico.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        valorestorico.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        valorestorico.setOpaque(true);
        pinsriga.add(valorestorico);
        valorestorico.setBounds(30, 79, 92, 24);

        JLabel61.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel61.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel61.setText("Valore fondo");
        JLabel61.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel61.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel61.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel61.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel61);
        JLabel61.setBounds(0, 103, 120, 24);

        valorefondo.setBackground(new java.awt.Color(202, 236, 221));
        valorefondo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        valorefondo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        valorefondo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        valorefondo.setOpaque(true);
        pinsriga.add(valorefondo);
        valorefondo.setBounds(30, 127, 92, 24);

        JLabel63.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel63.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel63.setText("Residuo parziale");
        JLabel63.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel63.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel63.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel63.setPreferredSize(new java.awt.Dimension(95, 16));
        pinsriga.add(JLabel63);
        JLabel63.setBounds(144, 151, 110, 24);

        residuoparziale.setBackground(new java.awt.Color(202, 236, 221));
        residuoparziale.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        residuoparziale.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        residuoparziale.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        residuoparziale.setOpaque(true);
        pinsriga.add(residuoparziale);
        residuoparziale.setBounds(160, 175, 92, 24);

        jLabminuss1.setBackground(new java.awt.Color(255, 255, 255));
        jLabminuss1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabminuss1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabminuss1.setText("Plusvalenza");
        pinsriga.add(jLabminuss1);
        jLabminuss1.setBounds(410, 100, 90, 24);

        jLabminuss2.setBackground(new java.awt.Color(255, 255, 255));
        jLabminuss2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabminuss2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabminuss2.setText("Minusvalenza");
        pinsriga.add(jLabminuss2);
        jLabminuss2.setBounds(410, 130, 90, 24);

        plusvalenza.setBackground(new java.awt.Color(202, 236, 221));
        plusvalenza.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        plusvalenza.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        plusvalenza.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        plusvalenza.setOpaque(true);
        pinsriga.add(plusvalenza);
        plusvalenza.setBounds(510, 100, 92, 24);

        minusvalenza.setBackground(new java.awt.Color(202, 236, 221));
        minusvalenza.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        minusvalenza.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        minusvalenza.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        minusvalenza.setOpaque(true);
        pinsriga.add(minusvalenza);
        minusvalenza.setBounds(510, 130, 92, 24);

        pcarr1.add(pinsriga, java.awt.BorderLayout.PAGE_END);

        pcarrello.add(pcarr1, java.awt.BorderLayout.CENTER);

        ptab.addTab("Dettaglio", new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_shopping_cart_black_24dp.png")), pcarrello); // NOI18N

        priep.setBackground(java.awt.Color.white);
        priep.setLayout(null);

        pdatitrasp.setBackground(new java.awt.Color(255, 255, 255));
        pdatitrasp.setLayout(null);

        JLabel73.setBackground(new java.awt.Color(10, 150, 150));
        JLabel73.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        JLabel73.setForeground(java.awt.Color.white);
        JLabel73.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel73.setText("Dati trasporto");
        JLabel73.setToolTipText("");
        JLabel73.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel73.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel73.setOpaque(true);
        JLabel73.setPreferredSize(new java.awt.Dimension(95, 16));
        pdatitrasp.add(JLabel73);
        JLabel73.setBounds(0, 0, 1243, 33);

        JLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel2.setText("Trasporto a cura del");
        JLabel2.setMaximumSize(new java.awt.Dimension(90, 16));
        JLabel2.setMinimumSize(new java.awt.Dimension(0, 24));
        JLabel2.setPreferredSize(new java.awt.Dimension(0, 24));
        pdatitrasp.add(JLabel2);
        JLabel2.setBounds(4, 33, 276, 24);

        tipotrasporto.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        tipotrasporto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Mittente", "Destinatario", "Vettore" }));
        tipotrasporto.setMinimumSize(new java.awt.Dimension(0, 24));
        tipotrasporto.setPreferredSize(new java.awt.Dimension(95, 25));
        tipotrasporto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tipotrasportoActionPerformed(evt);
            }
        });
        pdatitrasp.add(tipotrasporto);
        tipotrasporto.setBounds(4, 57, 310, 25);

        JLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel3.setText("Causale del trasporto");
        JLabel3.setMaximumSize(new java.awt.Dimension(119, 16));
        JLabel3.setMinimumSize(new java.awt.Dimension(0, 24));
        JLabel3.setPreferredSize(new java.awt.Dimension(0, 24));
        pdatitrasp.add(JLabel3);
        JLabel3.setBounds(324, 32, 463, 24);

        causaletrasporto.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        causaletrasporto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Vendita", "Trasferimento" }));
        causaletrasporto.setMinimumSize(new java.awt.Dimension(0, 24));
        causaletrasporto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                causaletrasportoActionPerformed(evt);
            }
        });
        pdatitrasp.add(causaletrasporto);
        causaletrasporto.setBounds(324, 56, 310, 25);

        JLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel10.setText("Aspetto esteriore beni");
        JLabel10.setMaximumSize(new java.awt.Dimension(120, 16));
        JLabel10.setMinimumSize(new java.awt.Dimension(0, 24));
        JLabel10.setPreferredSize(new java.awt.Dimension(0, 24));
        pdatitrasp.add(JLabel10);
        JLabel10.setBounds(644, 32, 406, 24);

        aspettobeni.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        aspettobeni.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "A vista", "Casse e cartoni", "Casse", "Cartoni", "Scatole", "Sacchi", "Fusti", "Taniche", "Cubitainer", "Pancali", "Dame", "Alla rinfusa" }));
        aspettobeni.setMinimumSize(new java.awt.Dimension(0, 24));
        aspettobeni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aspettobeniActionPerformed(evt);
            }
        });
        pdatitrasp.add(aspettobeni);
        aspettobeni.setBounds(644, 56, 308, 25);

        priep.add(pdatitrasp);
        pdatitrasp.setBounds(0, 0, 1244, 82);
        priep.add(porto);
        porto.setBounds(116, 120, 144, 24);

        JLabel8.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel8.setText("Porto");
        JLabel8.setMaximumSize(new java.awt.Dimension(120, 16));
        JLabel8.setMinimumSize(new java.awt.Dimension(120, 16));
        JLabel8.setPreferredSize(new java.awt.Dimension(120, 16));
        priep.add(JLabel8);
        JLabel8.setBounds(-8, 120, 120, 24);

        descrporto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        priep.add(descrporto);
        descrporto.setBounds(264, 120, 268, 24);

        orapart.setBackground(java.awt.Color.white);
        priep.add(orapart);
        orapart.setBounds(852, 120, 100, 24);
        priep.add(datapart);
        datapart.setBounds(728, 120, 120, 24);

        JLabel75.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel75.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel75.setText("Data/ora partenza merce");
        JLabel75.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel75.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel75.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(JLabel75);
        JLabel75.setBounds(544, 120, 180, 24);

        targa.setCaratteri(30);
        targa.setMinimumSize(new java.awt.Dimension(120, 24));
        targa.setPreferredSize(new java.awt.Dimension(120, 24));
        priep.add(targa);
        targa.setBounds(728, 152, 224, 24);

        JLabel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel11.setText("Targa mezzo");
        JLabel11.setMaximumSize(new java.awt.Dimension(120, 16));
        JLabel11.setMinimumSize(new java.awt.Dimension(120, 24));
        JLabel11.setPreferredSize(new java.awt.Dimension(120, 24));
        priep.add(JLabel11);
        JLabel11.setBounds(632, 152, 92, 24);

        totpesonetto.setDecimali(3);
        priep.add(totpesonetto);
        totpesonetto.setBounds(824, 184, 128, 24);

        JLabel72.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel72.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel72.setText("Peso netto totale Kg");
        JLabel72.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel72.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel72.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(JLabel72);
        JLabel72.setBounds(672, 184, 148, 24);

        totpesolordo.setDecimali(3);
        priep.add(totpesolordo);
        totpesolordo.setBounds(532, 184, 128, 24);

        JLabel80.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel80.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel80.setText("Peso lordo totale Kg");
        JLabel80.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel80.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel80.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(JLabel80);
        JLabel80.setBounds(388, 184, 140, 24);
        priep.add(totpallets);
        totpallets.setBounds(264, 184, 100, 24);

        JLabel78.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel78.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel78.setText("pallets");
        JLabel78.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel78.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel78.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(JLabel78);
        JLabel78.setBounds(168, 184, 92, 24);
        priep.add(totcolli);
        totcolli.setBounds(116, 184, 92, 24);

        JLabel83.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel83.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel83.setText("Totale colli");
        JLabel83.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel83.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel83.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(JLabel83);
        JLabel83.setBounds(-12, 184, 124, 24);
        priep.add(notepiede);
        notepiede.setBounds(116, 216, 836, 48);

        JLabel79.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel79.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel79.setText("Note libere");
        JLabel79.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel79.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel79.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(JLabel79);
        JLabel79.setBounds(20, 216, 92, 24);

        JLabel74.setBackground(new java.awt.Color(10, 150, 150));
        JLabel74.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        JLabel74.setForeground(java.awt.Color.white);
        JLabel74.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel74.setText("Riepilogo IVA");
        JLabel74.setToolTipText("");
        JLabel74.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel74.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel74.setOpaque(true);
        JLabel74.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(JLabel74);
        JLabel74.setBounds(8, 268, 504, 24);

        label_scad.setBackground(new java.awt.Color(10, 150, 150));
        label_scad.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        label_scad.setForeground(java.awt.Color.white);
        label_scad.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_scad.setText("Scadenze");
        label_scad.setToolTipText("");
        label_scad.setMaximumSize(new java.awt.Dimension(95, 16));
        label_scad.setMinimumSize(new java.awt.Dimension(95, 16));
        label_scad.setOpaque(true);
        label_scad.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(label_scad);
        label_scad.setBounds(524, 268, 428, 24);

        jScrollIva.setViewportView(tabiva);

        priep.add(jScrollIva);
        jScrollIva.setBounds(8, 292, 504, 92);

        jScrollScad.setViewportView(tabscad);

        priep.add(jScrollScad);
        jScrollScad.setBounds(524, 292, 428, 92);

        buttonInsScad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonInsScad.setActionCommand("Esegui");
        buttonInsScad.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonInsScad.setMaximumSize(new java.awt.Dimension(30, 40));
        buttonInsScad.setMinimumSize(new java.awt.Dimension(30, 40));
        buttonInsScad.setPreferredSize(new java.awt.Dimension(30, 40));
        buttonInsScad.setToolTipText("Inserisci nuova scadenza");
        buttonInsScad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsScadActionPerformed(evt);
            }
        });
        priep.add(buttonInsScad);
        buttonInsScad.setBounds(956, 336, 48, 48);

        avvisoscad.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        avvisoscad.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        avvisoscad.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_warning_red_24dp.png"))); // NOI18N
        avvisoscad.setMaximumSize(new java.awt.Dimension(95, 16));
        avvisoscad.setMinimumSize(new java.awt.Dimension(95, 16));
        avvisoscad.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(avvisoscad);
        avvisoscad.setBounds(956, 388, 24, 24);

        totscad.setBackground(new java.awt.Color(202, 236, 221));
        totscad.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totscad.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totscad.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totscad.setOpaque(true);
        priep.add(totscad);
        totscad.setBounds(820, 388, 132, 24);

        label_totscad.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_totscad.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_totscad.setText("Totale scadenze");
        label_totscad.setMaximumSize(new java.awt.Dimension(95, 16));
        label_totscad.setMinimumSize(new java.awt.Dimension(95, 16));
        label_totscad.setPreferredSize(new java.awt.Dimension(95, 16));
        priep.add(label_totscad);
        label_totscad.setBounds(688, 388, 128, 24);

        label_totpag.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_totpag.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_totpag.setText("Importo pagato");
        label_totpag.setMaximumSize(new java.awt.Dimension(95, 16));
        label_totpag.setMinimumSize(new java.awt.Dimension(200, 24));
        label_totpag.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(label_totpag);
        label_totpag.setBounds(568, 416, 248, 24);

        JLabel68.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel68.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel68.setText("Netto documento");
        JLabel68.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel68.setMinimumSize(new java.awt.Dimension(200, 24));
        JLabel68.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(JLabel68);
        JLabel68.setBounds(20, 472, 365, 24);

        JLabel65.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel65.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel65.setText("IVA");
        JLabel65.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel65.setMinimumSize(new java.awt.Dimension(200, 24));
        JLabel65.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(JLabel65);
        JLabel65.setBounds(328, 388, 56, 24);

        JLabel76.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel76.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel76.setText("Imponibile");
        JLabel76.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel76.setMinimumSize(new java.awt.Dimension(200, 24));
        JLabel76.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(JLabel76);
        JLabel76.setBounds(-240, 388, 365, 24);

        totimp.setBackground(new java.awt.Color(202, 236, 221));
        totimp.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totimp.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totimp.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totimp.setMinimumSize(new java.awt.Dimension(200, 24));
        totimp.setOpaque(true);
        totimp.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(totimp);
        totimp.setBounds(128, 388, 124, 24);

        totiva.setBackground(new java.awt.Color(202, 236, 221));
        totiva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totiva.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totiva.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totiva.setMinimumSize(new java.awt.Dimension(200, 24));
        totiva.setOpaque(true);
        totiva.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(totiva);
        totiva.setBounds(388, 388, 124, 24);

        totdoc.setBackground(new java.awt.Color(202, 236, 221));
        totdoc.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totdoc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totdoc.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totdoc.setMinimumSize(new java.awt.Dimension(200, 24));
        totdoc.setOpaque(true);
        totdoc.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(totdoc);
        totdoc.setBounds(388, 416, 124, 24);

        totpag.setInteri(12);
        totpag.setMinimumSize(new java.awt.Dimension(200, 24));
        priep.add(totpag);
        totpag.setBounds(820, 416, 132, 24);

        pvettore.setBackground(java.awt.Color.white);
        pvettore.setLayout(null);

        vettore.setMinimumSize(new java.awt.Dimension(148, 24));
        pvettore.add(vettore);
        vettore.setBounds(88, 0, 148, 24);

        JLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel9.setText("Vettore");
        JLabel9.setMaximumSize(new java.awt.Dimension(120, 16));
        JLabel9.setMinimumSize(new java.awt.Dimension(120, 24));
        JLabel9.setPreferredSize(new java.awt.Dimension(120, 24));
        pvettore.add(JLabel9);
        JLabel9.setBounds(28, 0, 56, 24);

        descrvettore.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrvettore.setMinimumSize(new java.awt.Dimension(200, 24));
        descrvettore.setPreferredSize(new java.awt.Dimension(200, 24));
        pvettore.add(descrvettore);
        descrvettore.setBounds(236, 0, 269, 24);

        JLabel87.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel87.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel87.setText("data e ora ritiro merce");
        JLabel87.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel87.setMinimumSize(new java.awt.Dimension(100, 24));
        JLabel87.setPreferredSize(new java.awt.Dimension(100, 24));
        pvettore.add(JLabel87);
        JLabel87.setBounds(528, 0, 169, 24);

        oraritiro.setBackground(java.awt.Color.white);
        oraritiro.setMinimumSize(new java.awt.Dimension(100, 24));
        oraritiro.setPreferredSize(new java.awt.Dimension(100, 24));
        pvettore.add(oraritiro);
        oraritiro.setBounds(824, 0, 100, 24);
        pvettore.add(dataritiro);
        dataritiro.setBounds(700, 0, 120, 24);

        priep.add(pvettore);
        pvettore.setBounds(28, 88, 960, 24);

        JLabel77.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel77.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel77.setText("Totale documento");
        JLabel77.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel77.setMinimumSize(new java.awt.Dimension(200, 24));
        JLabel77.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(JLabel77);
        JLabel77.setBounds(20, 416, 365, 24);

        totaccred.setBackground(new java.awt.Color(202, 236, 221));
        totaccred.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totaccred.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totaccred.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totaccred.setMinimumSize(new java.awt.Dimension(200, 24));
        totaccred.setOpaque(true);
        totaccred.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(totaccred);
        totaccred.setBounds(128, 444, 124, 24);

        JLabel82.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel82.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel82.setText("detrazioni Enasarco");
        JLabel82.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel82.setMinimumSize(new java.awt.Dimension(200, 24));
        JLabel82.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(JLabel82);
        JLabel82.setBounds(252, 444, 132, 24);

        totnetto.setBackground(new java.awt.Color(202, 236, 221));
        totnetto.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totnetto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totnetto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totnetto.setMinimumSize(new java.awt.Dimension(200, 24));
        totnetto.setOpaque(true);
        totnetto.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(totnetto);
        totnetto.setBounds(388, 472, 124, 24);

        JLabel85.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel85.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel85.setText("detrazioni ritenute");
        JLabel85.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel85.setMinimumSize(new java.awt.Dimension(200, 24));
        JLabel85.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(JLabel85);
        JLabel85.setBounds(-240, 444, 365, 24);

        totabbuono.setBackground(new java.awt.Color(202, 236, 221));
        totabbuono.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totabbuono.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totabbuono.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totabbuono.setMinimumSize(new java.awt.Dimension(200, 24));
        totabbuono.setOpaque(true);
        totabbuono.setPreferredSize(new java.awt.Dimension(200, 24));
        priep.add(totabbuono);
        totabbuono.setBounds(388, 444, 124, 24);

        ptab.addTab("Riepilogo", new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_list_black_24dp.png")), priep); // NOI18N

        pdatiagg.setBackground(java.awt.Color.white);
        pdatiagg.setLayout(new java.awt.BorderLayout());

        tabdatiagg.setBackground(new java.awt.Color(255, 255, 232));
        tabdatiagg.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        tabdatiagg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabdatiaggMouseClicked(evt);
            }
        });
        jScrollDatiAgg.setViewportView(tabdatiagg);

        pdatiagg.add(jScrollDatiAgg, java.awt.BorderLayout.CENTER);

        jPanel4.setBackground(java.awt.Color.white);
        jPanel4.setPreferredSize(new java.awt.Dimension(0, 144));
        jPanel4.setLayout(null);

        label_sogg6.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg6.setText("N.documento");
        jPanel4.add(label_sogg6);
        label_sogg6.setBounds(588, 8, 112, 24);

        datiagg_tiporec.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        datiagg_tiporec.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Dati ordine di acquisto", "Dati contratto", "Dati convenzione", "Dati ricezione" }));
        jPanel4.add(datiagg_tiporec);
        datiagg_tiporec.setBounds(228, 8, 240, 24);

        label_sogg5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg5.setText("Tipo riga");
        jPanel4.add(label_sogg5);
        label_sogg5.setBounds(44, 8, 180, 24);

        label_sogg8.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg8.setText("Codice commessa/convenzione");
        jPanel4.add(label_sogg8);
        label_sogg8.setBounds(-20, 36, 244, 24);

        datiagg_codcommessa.setCaratteri(100);
        jPanel4.add(datiagg_codcommessa);
        datiagg_codcommessa.setBounds(228, 36, 240, 24);

        datiagg_codcig.setCaratteri(15);
        jPanel4.add(datiagg_codcig);
        datiagg_codcig.setBounds(228, 64, 240, 24);

        label_sogg4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg4.setText("Codice Identificativo Gara (CIG)");
        jPanel4.add(label_sogg4);
        label_sogg4.setBounds(-8, 64, 232, 24);

        datiagg_codcup.setCaratteri(15);
        jPanel4.add(datiagg_codcup);
        datiagg_codcup.setBounds(704, 36, 392, 24);

        label_sogg10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg10.setText("Codice Unitario Progetto (CUP)");
        jPanel4.add(label_sogg10);
        label_sogg10.setBounds(456, 36, 244, 24);

        datiagg_numdoc.setCaratteri(20);
        jPanel4.add(datiagg_numdoc);
        datiagg_numdoc.setBounds(704, 8, 144, 24);

        label_sogg7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg7.setText("Data documento");
        jPanel4.add(label_sogg7);
        label_sogg7.setBounds(848, 8, 124, 24);
        jPanel4.add(datiagg_datadoc);
        datiagg_datadoc.setBounds(976, 8, 120, 24);

        buttonInsDatiAgg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonInsDatiAgg.setActionCommand("Esegui");
        buttonInsDatiAgg.setDoubleBuffered(true);
        buttonInsDatiAgg.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonInsDatiAgg.setMaximumSize(new java.awt.Dimension(47, 22));
        buttonInsDatiAgg.setMinimumSize(new java.awt.Dimension(47, 22));
        buttonInsDatiAgg.setPreferredSize(new java.awt.Dimension(47, 22));
        buttonInsDatiAgg.setToolTipText("");
        buttonInsDatiAgg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsDatiAggActionPerformed(evt);
            }
        });
        jPanel4.add(buttonInsDatiAgg);
        buttonInsDatiAgg.setBounds(1176, 8, 56, 56);
        jPanel4.add(datiagg_impbollo);
        datiagg_impbollo.setBounds(228, 112, 128, 24);

        label_sogg17.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg17.setText("Importo bollo");
        jPanel4.add(label_sogg17);
        label_sogg17.setBounds(132, 112, 92, 24);

        datiagg_bollovirtuale.setBackground(java.awt.Color.white);
        datiagg_bollovirtuale.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        datiagg_bollovirtuale.setText("Bollo virtuale");
        datiagg_bollovirtuale.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                datiagg_bollovirtualeActionPerformed(evt);
            }
        });
        jPanel4.add(datiagg_bollovirtuale);
        datiagg_bollovirtuale.setBounds(20, 112, 110, 24);

        pdatiagg.add(jPanel4, java.awt.BorderLayout.SOUTH);

        ptab.addTab("Dati aggiuntivi / Fattura B2B", pdatiagg);

        pallegati.setLayout(new java.awt.BorderLayout());

        taball.setBackground(new java.awt.Color(255, 255, 232));
        taball.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        taball.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                taballMouseClicked(evt);
            }
        });
        jScrollAll.setViewportView(taball);

        pallegati.add(jScrollAll, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(java.awt.Color.white);
        jPanel6.setPreferredSize(new java.awt.Dimension(10, 168));
        jPanel6.setLayout(null);

        label_sogg25.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg25.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg25.setText("Percorso dell'allegato");
        jPanel6.add(label_sogg25);
        label_sogg25.setBounds(24, 8, 180, 24);

        label_sogg26.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg26.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg26.setText("nome del file allegato");
        jPanel6.add(label_sogg26);
        label_sogg26.setBounds(24, 40, 180, 24);

        label_sogg27.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg27.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg27.setText("Algoritmo di compressione");
        jPanel6.add(label_sogg27);
        label_sogg27.setBounds(24, 72, 180, 24);

        label_sogg28.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg28.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg28.setText("Formato dell'allegato");
        jPanel6.add(label_sogg28);
        label_sogg28.setBounds(24, 104, 180, 24);

        label_sogg29.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_sogg29.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_sogg29.setText("Descrizione del documento");
        jPanel6.add(label_sogg29);
        label_sogg29.setBounds(24, 136, 180, 24);

        aalgcompr.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        aalgcompr.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nessuno", "ZIP", "RAR" }));
        jPanel6.add(aalgcompr);
        aalgcompr.setBounds(208, 72, 104, 24);

        anome.setCaratteri(60);
        jPanel6.add(anome);
        anome.setBounds(208, 40, 412, 24);
        jPanel6.add(alink);
        alink.setBounds(208, 8, 412, 24);

        buttonRicFileAll.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_search_white_20dp.png"))); // NOI18N
        buttonRicFileAll.setActionCommand("Esegui");
        buttonRicFileAll.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonRicFileAll.setMaximumSize(new java.awt.Dimension(30, 40));
        buttonRicFileAll.setMinimumSize(new java.awt.Dimension(30, 40));
        buttonRicFileAll.setPreferredSize(new java.awt.Dimension(30, 40));
        buttonRicFileAll.setToolTipText("Ricerca percorso allegato");
        buttonRicFileAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRicFileAllActionPerformed(evt);
            }
        });
        jPanel6.add(buttonRicFileAll);
        buttonRicFileAll.setBounds(624, 8, 24, 24);

        aformato.setCaratteri(10);
        jPanel6.add(aformato);
        aformato.setBounds(208, 104, 104, 24);

        adescr.setCaratteri(100);
        jPanel6.add(adescr);
        adescr.setBounds(208, 136, 412, 24);

        buttonInsAllegati.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonInsAllegati.setActionCommand("Esegui");
        buttonInsAllegati.setDoubleBuffered(true);
        buttonInsAllegati.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        buttonInsAllegati.setMaximumSize(new java.awt.Dimension(47, 22));
        buttonInsAllegati.setMinimumSize(new java.awt.Dimension(47, 22));
        buttonInsAllegati.setPreferredSize(new java.awt.Dimension(47, 22));
        buttonInsAllegati.setToolTipText("");
        buttonInsAllegati.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsAllegatiActionPerformed(evt);
            }
        });
        jPanel6.add(buttonInsAllegati);
        buttonInsAllegati.setBounds(672, 8, 56, 56);

        pallegati.add(jPanel6, java.awt.BorderLayout.SOUTH);

        ptab.addTab("Allegati", pallegati);

        add(ptab, java.awt.BorderLayout.CENTER);

        pcom.setBackground(java.awt.Color.white);
        pcom.setPreferredSize(new java.awt.Dimension(0, 56));
        pcom.setLayout(new java.awt.BorderLayout());

        pcom1.setBackground(java.awt.Color.white);
        pcom1.setPreferredSize(new java.awt.Dimension(500, 200));
        pcom1.setLayout(null);

        buttonSalva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_save_white_24dp.png"))); // NOI18N
        buttonSalva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonSalva.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonSalva.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonSalva.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonSalva.setToolTipText("Salva documento");
        buttonSalva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvaActionPerformed(evt);
            }
        });
        pcom1.add(buttonSalva);
        buttonSalva.setBounds(4, 4, 48, 48);
        pcom1.add(waitloc);
        waitloc.setBounds(64, 12, 32, 32);

        clifenodescr.setBackground(new java.awt.Color(255, 255, 255));
        clifenodescr.setFont(new java.awt.Font("Dialog", 0, 11)); // NOI18N
        clifenodescr.setText("Non riportare figli in fattura elettronica");
        clifenodescr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clifenodescrActionPerformed(evt);
            }
        });
        pcom1.add(clifenodescr);
        clifenodescr.setBounds(110, 20, 360, 20);

        pcom.add(pcom1, java.awt.BorderLayout.WEST);

        pcom2.setBackground(java.awt.Color.white);
        pcom2.setPreferredSize(new java.awt.Dimension(350, 0));
        pcom2.setLayout(null);
        pcom.add(pcom2, java.awt.BorderLayout.EAST);

        add(pcom, java.awt.BorderLayout.PAGE_END);

        ptesta.setBackground(java.awt.Color.white);
        ptesta.setPreferredSize(new java.awt.Dimension(2, 64));
        ptesta.setLayout(new java.awt.BorderLayout());

        ptesta2.setBackground(java.awt.Color.white);
        ptesta2.setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(780, 100));
        jPanel1.setLayout(null);

        label_datamov.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_datamov.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_datamov.setText("Data XXXX");
        label_datamov.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        label_datamov.setMaximumSize(new java.awt.Dimension(95, 16));
        label_datamov.setMinimumSize(new java.awt.Dimension(95, 16));
        label_datamov.setPreferredSize(new java.awt.Dimension(95, 16));
        jPanel1.add(label_datamov);
        label_datamov.setBounds(12, 4, 208, 24);
        jPanel1.add(datamov);
        datamov.setBounds(12, 28, 120, 24);

        buttonModNum.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_autorenew_white_20dp.png"))); // NOI18N
        buttonModNum.setActionCommand("Ricerca");
        buttonModNum.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonModNum.setMaximumSize(new java.awt.Dimension(16, 22));
        buttonModNum.setMinimumSize(new java.awt.Dimension(16, 22));
        buttonModNum.setPreferredSize(new java.awt.Dimension(16, 22));
        buttonModNum.setToolTipText("modifica numero e data documento");
        buttonModNum.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonModNumActionPerformed(evt);
            }
        });
        jPanel1.add(buttonModNum);
        buttonModNum.setBounds(136, 28, 24, 24);

        nomecli2.setBackground(new java.awt.Color(202, 236, 221));
        nomecli2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        nomecli2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        nomecli2.setOpaque(true);
        jPanel1.add(nomecli2);
        nomecli2.setBounds(232, 28, 288, 24);

        totdoc2.setBackground(new java.awt.Color(202, 236, 221));
        totdoc2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totdoc2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totdoc2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totdoc2.setMinimumSize(new java.awt.Dimension(200, 24));
        totdoc2.setOpaque(true);
        totdoc2.setPreferredSize(new java.awt.Dimension(200, 24));
        jPanel1.add(totdoc2);
        totdoc2.setBounds(524, 28, 116, 24);

        label_datacons.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_datacons.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_datacons.setText("Data consegna");
        label_datacons.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        label_datacons.setMaximumSize(new java.awt.Dimension(95, 16));
        label_datacons.setMinimumSize(new java.awt.Dimension(200, 24));
        label_datacons.setPreferredSize(new java.awt.Dimension(200, 24));
        jPanel1.add(label_datacons);
        label_datacons.setBounds(644, 4, 124, 24);

        JLabel88.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel88.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel88.setText("Cliente");
        JLabel88.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel88.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel88.setMinimumSize(new java.awt.Dimension(95, 16));
        JLabel88.setPreferredSize(new java.awt.Dimension(95, 16));
        jPanel1.add(JLabel88);
        JLabel88.setBounds(232, 4, 95, 24);
        jPanel1.add(datacons);
        datacons.setBounds(644, 28, 120, 24);

        JLabel89.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel89.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        JLabel89.setText("Totale documento");
        JLabel89.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        JLabel89.setMaximumSize(new java.awt.Dimension(95, 16));
        JLabel89.setMinimumSize(new java.awt.Dimension(200, 24));
        JLabel89.setPreferredSize(new java.awt.Dimension(200, 24));
        jPanel1.add(JLabel89);
        JLabel89.setBounds(524, 4, 124, 24);

        ptesta2.add(jPanel1, java.awt.BorderLayout.WEST);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(400, 100));
        jPanel2.setLayout(null);

        label_nummov.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_nummov.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_nummov.setText("Numero");
        label_nummov.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        label_nummov.setMaximumSize(new java.awt.Dimension(95, 16));
        label_nummov.setMinimumSize(new java.awt.Dimension(95, 16));
        label_nummov.setPreferredSize(new java.awt.Dimension(95, 16));
        jPanel2.add(label_nummov);
        label_nummov.setBounds(172, 4, 224, 24);

        numero.setBackground(new java.awt.Color(202, 236, 221));
        numero.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        numero.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        numero.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        numero.setOpaque(true);
        jPanel2.add(numero);
        numero.setBounds(172, 28, 84, 24);

        stato.setBackground(new java.awt.Color(202, 236, 221));
        stato.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        stato.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        stato.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        stato.setOpaque(true);
        jPanel2.add(stato);
        stato.setBounds(260, 28, 136, 24);

        label_numfiscale.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_numfiscale.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_numfiscale.setText("Numero Fiscale");
        label_numfiscale.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        label_numfiscale.setMaximumSize(new java.awt.Dimension(95, 16));
        label_numfiscale.setMinimumSize(new java.awt.Dimension(95, 16));
        label_numfiscale.setPreferredSize(new java.awt.Dimension(95, 16));
        jPanel2.add(label_numfiscale);
        label_numfiscale.setBounds(4, 4, 160, 24);
        jPanel2.add(numerofisc);
        numerofisc.setBounds(4, 28, 160, 24);

        ptesta2.add(jPanel2, java.awt.BorderLayout.LINE_END);

        ptesta.add(ptesta2, java.awt.BorderLayout.CENTER);

        add(ptesta, java.awt.BorderLayout.NORTH);

        getAccessibleContext().setAccessibleDescription("");
    }// </editor-fold>//GEN-END:initComponents

    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    private void buttonSalvaActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonSalvaActionPerformed
    {//GEN-HEADEREND:event_buttonSalvaActionPerformed
        if (datamov.getText().equals("")) {
            JOptionPane.showMessageDialog(this, "Data movimento mancante", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            datamov.requestFocus();
            return;
        }

        if (!BusinessLogicAmbiente.dataInEsercizio(EnvJazz.connAmbiente.conn, azienda, esContabile, new Data(datamov.getText(), Data.GG_MM_AAAA).formatta(Data.AAAA_MM_GG, "-"))) {
            BusinessLogicAmbiente.mostraErrorMessage(this, "Errore", "Data documento fuori esercizio");
            datamov.requestFocus();
            return;
        }

        ves = BusinessLogicAmbiente.leggiEserciziPeriodo(EnvJazz.connAmbiente.conn, azienda, datamov.getDataStringa(), datamov.getDataStringa());
        if (ves.size() == 0) {
            JOptionPane.showMessageDialog(this, "La data di registrazione del movimento non appartiene a nessun esercizio attivo", "Errore", JOptionPane.ERROR_MESSAGE,
                    new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if ((datarif.getData() != null || !numrif.getText().trim().equals("")) && (datarif.getData() == null || numrif.getText().trim().equals(""))) {
            JOptionPane.showMessageDialog(this, "Indicare la data e il numero di riferimento del documento", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CLIENTE);
            datarif.requestFocus();
            return;
        }

        for (int i = 0; i < cespitidaregistrare.size(); i++) {
            Record rcesp = cespitidaregistrare.get(i);
            if (rcesp.leggiIntero("cespid") != -1) {
                if (rcesp.leggiIntero("plusvalenzapdc") == -1) {
                    JOptionPane.showMessageDialog(this, "Configura il conto di plusvalenza sulla categoria", "Errore",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }

                if (rcesp.leggiIntero("minusvalenzapdc") == -1) {
                    JOptionPane.showMessageDialog(this, "Configura il conto di minusvalenza sulla categoria", "Errore",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }
            }
        }

        if (tipodoc == 0 || tipodoc == 1) {
            // controllo sequenza protocollo - data
            int ns = 0;
            String qry = "SELECT movdocid FROM movimentidoc" + esContabile
                    + " WHERE movdocnumint2 = 0 AND movdocsufft = \"" + rSez.leggiStringa("sufxcod") + "\" AND "
                    + "movdocdtft > \"" + datamov.getDataStringa() + "\" AND movdocnumft < " + numero.getText().trim();
            QueryInterrogazioneClient qs = new QueryInterrogazioneClient(connAz, qry);
            qs.apertura();
            ns = qs.numeroRecord();
            qs.chiusura();

            if (ns > 0) {
                JOptionPane.showMessageDialog(this, "Ci sono movimenti con data superiore al " + datamov.getText() + " e numero documento inferiore a " + numero.getText() + "\n"
                        + "Salvataggio bloccato", "Avviso", JOptionPane.WARNING_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/warning.png")));
                return;
            }
        }

        if (cliente.leggiId() == -1) {
            JOptionPane.showMessageDialog(this, "Indicare il cliente", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CLIENTE);
            cliente.fuoco();
            return;
        }

        if (pagamento.leggiId() == -1) {
            JOptionPane.showMessageDialog(this, "Indicare il pagamento", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CLIENTE);
            pagamento.fuoco();
            return;
        }

        if (vrighe.size() == 0) {
            JOptionPane.showMessageDialog(this, "Indicare almeno un articolo", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CARRELLO);
            return;
        }

        if (Formattazione.estraiDouble(totnetto.getText()) < 0) {
            JOptionPane.showMessageDialog(this, "Importo del documento negativo", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_RIEPILOGO);
            return;
        }

        if (totpag.getValore() > 0 && totpag.getValore() > Formattazione.estraiDouble(totnetto.getText())) {
            JOptionPane.showMessageDialog(this, "Importo pagato superiore al totale del documento", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_RIEPILOGO);
            totpag.requestFocus();
            return;
        }

        double tscad = 0;
        for (Record rsc : vscad) {
            tscad += rsc.leggiDouble("importo");
        }

        tscad = OpValute.arrotondaMat(tscad, 2);
        double nettoapag = OpValute.arrotondaMat(Formattazione.estraiDouble(totnetto.getText()) - totpag.getValore() - ivasplitp, 2);
        if (nettoapag != tscad && (tipodoc == 0 || tipodoc == 1)) {
            int riss = JOptionPane.showConfirmDialog(this, "Totale netto del documento diverso dalla somma degli importi delle scadenze\nVuoi ricalcolare le scadenze?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (riss == JOptionPane.YES_OPTION) {
                scadmodif = false;
                ricalcolaTotale();
            } else {
                ptab.setSelectedIndex(TAB_RIEPILOGO);
                return;
            }
        }

        if (tipodoc == 1 && vnotac.size() == 0) {
            int riss = JOptionPane.showConfirmDialog(this, "Non sono state indicate fatture collegate a questa nota di credito.\nVuoi effettuare il collegamento?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (riss == JOptionPane.YES_OPTION) {
                ptab.setSelectedIndex(TAB_NOTACREDITO);
                return;
            }
        }

        if (tipodoc == 1) {
            boolean fatven = false, ncven = false, fatacq = false, ncacq = false;
            if (rDoc.leggiIntero("tdsegno") == 0 && rDoc.leggiIntero("tdtipo") == 1) {
                fatven = true;
            } else if (rDoc.leggiIntero("tdsegno") == 1 && rDoc.leggiIntero("tdtipo") == 1) {
                ncven = true;
            }

            if (rDoc.leggiIntero("tdsegno") == 1 && rDoc.leggiIntero("tdtipo") == 0) {
                fatacq = true;
            } else if (rDoc.leggiIntero("tdsegno") == 0 && rDoc.leggiIntero("tdtipo") == 0) {
                ncacq = true;
            }

            Record rpag = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "pagamenti", "pagid", pagamento.leggiId(), "");
            if ((ncven || ncacq) && rDoc.leggiIntero("tdtnum") == 2 && rpag.leggiIntero("pagtipo") == 0) {
                JOptionPane.showMessageDialog(this, "Tipo di pagamento portafoglio non ammesso per questo documento\nModificare il codice di pagamento",
                        "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            }
        }
        ArrayList<Integer> vcodiva = new ArrayList();
        for (int i = 0; i < vrighe.size(); i++) {
            vcodiva.add(vrighe.get(i).leggiIntero("ivaid"));
        }

        if (BusinessLogicAttivazione.controlloPluginAttivoLocale(EnvJazz.connAmbiente.leggiConnessione(), EnvJazz.azienda, EnvJazz.PLUGIN_JBRIDGE)) {
            ArrayList<String> verrctr = BusinessLogicDocumenti.elencoErroriFatturaElettronica(connAz.leggiConnessione(), connCom.leggiConnessione(), EnvJazz.connAmbiente.leggiConnessione(), azienda, cliente.leggiId(), pagamento.leggiId(), vcodiva);
            if (verrctr.size() > 0) {
                String err = "";
                for (String er : verrctr) {
                    err += er + "\r\n";
                }

                JOptionPane.showMessageDialog(this, "Errori di contenuto sulla fattura elettronica:\r\n\r\n" + err + "\r\nCorreggere gli errori segnalati",
                        "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            }
        }
//        }

        boolean salvaok = true;
        if (chiediconferma) {
            int riss = JOptionPane.showConfirmDialog(this, "Confermi salvataggio Documento?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (riss == JOptionPane.YES_OPTION) {
                salvaok = true;
            } else {
                salvaok = false;
            }
        }

//        System.out.println("esmov:" + esmov);
        if (salvaok) {
            
            if(clifenodescr.isSelected())
            {
                String qryupdate="UPDATE clienti SET clifenodescr=1 WHERE cliid="+cliente.leggiId();
                QueryAggiornamentoClient query= new QueryAggiornamentoClient(connAz,qryupdate);
                query.esecuzione();
            }
            
            if (BusinessLogicAttivazione.controlloPluginAttivoLocale(EnvJazz.connAmbiente.leggiConnessione(), EnvJazz.azienda, EnvJazz.PLUGIN_JBRIDGE)) {
                File ff = new File("");
                String pathTmp = ff.getAbsolutePath() + File.separator + "tmpDoc";
                File f = new File(pathTmp);
                boolean ok = true;
                if (f.exists()) {
                    ok = deleteDirectory(f);
                }

                if (ok) {
                    (f).mkdir();
                    f.setExecutable(true);
                }

                String qry = "SELECT * FROM eft_archivio" + esContabile + " WHERE eftmovdocid=" + movdocId + " AND eftescontmov=\"" + esmov + "\"";
                QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
                q.apertura();
                Record r = q.successivo();
                q.chiusura();
                if (r != null) {
                    if (r.leggiIntero("eftfirmato") == 1) {
                        int ris1 = JOptionPane.showConfirmDialog(this, "Si sta modificando un documento già firmato. \nLa conferma comporterà l'eliminazione dello stesso e la necessità di procedere di nuovo alla firma. \nSi desidera proseguire comunque con la modifica?",
                                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));

                        if (ris1 == JOptionPane.YES_OPTION) {
                            String qryupdate = "UPDATE eft_archivio" + esContabile + " SET eftfirmato=0 WHERE eftmovdocid=" + movdocId + " AND eftescontmov=\"" + esmov + "\"";
                            QueryAggiornamentoClient qagg = new QueryAggiornamentoClient(connAz, qryupdate);
                            qagg.esecuzione();
                        }
                        salvaDoc();
                    } else {
                        salvaDoc();
                    }
                } else {
                    salvaDoc();
                }
            } else {
                salvaDoc();
            }

            if (listener != null) {
                listener.notificaChiusura(this, key);
            }
        }
    }//GEN-LAST:event_buttonSalvaActionPerformed

    private void salvaDoc() {
        waitloc.lancia();

        if (datapart.getData() == null) {
            datapart.setText(datamov.getText());
        }

        if (orapart.leggiOrario().equals("00:00")) {
            orapart.impostaOrarioAttuale();
        }

        if (vettore.leggiId() != -1) {
            if (dataritiro.getData() == null) {
                dataritiro.setText(datamov.getText());
            }

            if (oraritiro.leggiOrario().equals("00:00")) {
                oraritiro.impostaOrarioAttuale();
            }
        }

        String qry = "SELECT * FROM contifattven";
        QueryInterrogazioneClient qpar = new QueryInterrogazioneClient(connAz, qry);
        qpar.apertura();
        rpar = qpar.successivo();
        qpar.chiusura();

        qry = "SELECT * FROM caumag WHERE cmid = " + rDoc.leggiIntero("doccaumagid");
        QueryInterrogazioneClient qcm = new QueryInterrogazioneClient(connAz, qry);
        qcm.apertura();
        Record rcm = qcm.successivo();
        qcm.chiusura();

        qry = "SELECT * FROM caumag WHERE cmcod = \"rc\"";
        QueryInterrogazioneClient qcmreso = new QueryInterrogazioneClient(connAz, qry);
        qcmreso.apertura();
        Record rcmreso = qcmreso.successivo();
        qcmreso.chiusura();

        if (rcmreso == null) {
            String qry2 = "SELECT * FROM caumag WHERE cmtipo = 1 AND cmraggr = 6";
            qcmreso = new QueryInterrogazioneClient(connAz, qry2);
            qcmreso.apertura();
            rcmreso = qcmreso.successivo();
            qcmreso.chiusura();
        }

        Data dataReg = datamov.getData();

//        int seztipo = rSez.leggiIntero("sufxtipo");
//        if (seztipo == 0 || seztipo == 1 || seztipo == 3)
//        {
//            // cerca il sezionale dell'anno solare in base a data doc.
//            String anno = "" + dataReg.anno();
//            String qry2="SELECT * FROM sezionalinum WHERE sufxcod = \"" + rSez.leggiStringa("sufxcod") + "\" AND "
//                    + "sufxannoinizio = \"" + anno + "\" AND "
//                    + "sufxannofine = \"" + anno + "\"";
//            qn = new QueryInterrogazioneClient(connAz,qry2);
//            qn.apertura();
//            rSez = qn.successivo();
//            qn.chiusura();
//        } else
//        {
//            String annoInizio = esContabile.substring(0, 4);
//            String annoFine = esContabile.substring(4, 8);
//            String qry2="SELECT * FROM sezionalinum WHERE sufxcod = \"" + rSez.leggiStringa("sufxcod") + "\" AND "
//                    + "sufxannoinizio = \"" + annoInizio + "\" AND "
//                    + "sufxannofine = \"" + annoFine + "\"";
//            qn = new QueryInterrogazioneClient(connAz,qry2);
//            qn.apertura();
//            rSez = qn.successivo();
//            qn.chiusura();
//        }
        Record rMovDoc = new Record();
        rMovDoc.insElem("movdocnumpe", (int) 0);
        rMovDoc.insElem("movdocdtpe", "0000-00-00");
        rMovDoc.insElem("movdocnumrpe", (int) 0);
        rMovDoc.insElem("movdocdtrpe", "0000-00-00");
        rMovDoc.insElem("movdocnumre", numrif.getText().trim());
        rMovDoc.insElem("movdocdtre", datarif.getDataStringa());
        rMovDoc.insElem("movdocnumpu", (int) 0);
        rMovDoc.insElem("movdocdtpu", "0000-00-00");
        rMovDoc.insElem("movdocsufint1", "");
        rMovDoc.insElem("movdocnumint1", (int) 0);
        rMovDoc.insElem("movdocdtint1", "0000-00-00");
        rMovDoc.insElem("movdocsufint2", "");
        rMovDoc.insElem("movdocnumint2", (int) 0);
        rMovDoc.insElem("movdocdtint2", "0000-00-00");
        rMovDoc.insElem("movdocsufft", "");
        rMovDoc.insElem("movdocnumft", (int) 0);
        rMovDoc.insElem("movdocdtft", "0000-00-00");
        rMovDoc.insElem("movdocsufiva", "");
        rMovDoc.insElem("movdocnumiva", (int) 0);
        rMovDoc.insElem("movdocdtiva", "0000-00-00");

        Record rt = new Record();
        rt.insElem("testaanagid", rcli.leggiIntero("cliid"));
        rt.insElem("testatipocf", 1);
        rt.insElem("testapdcid", rcli.leggiIntero("clicontoid"));
        rt.insElem("testaspeseinc", rcli.leggiIntero("clispeseinc"));
        rt.insElem("testaspesebolli", rcli.leggiIntero("clispesebolli"));
        rt.insElem("testaspesetrasp", rcli.leggiIntero("clispesesped"));
        rt.insElem("testaspesecauz", rcli.leggiIntero("clispesecauz"));
        rt.insElem("testalistinoid", (int) -1);
        rt.insElem("testanprz", (int) 1);
        rt.insElem("testavaluid", (int) 2);
        rt.insElem("testacambio", (double) 1);
        rt.insElem("testaagenid", rcli.leggiIntero("cliageid"));
        rt.insElem("testalinguaid", (int) -1);
        rt.insElem("testabancaid", rcli.leggiIntero("clibancaid"));
        rt.insElem("testaagenzia", rcli.leggiStringa("cliagenzia"));
        rt.insElem("testacab", rcli.leggiStringa("clicab"));
        rt.insElem("testaivaid", rcli.leggiIntero("cliivaid"));
        rt.insElem("testaivaservid", (int) -1);
        int pagid = pagamento.leggiId();
        rt.insElem("testapagid", pagid);
        rt.insElem("testadestid", consegna.leggiId());
        rt.insElem("testaraggrbolle", (int) 1);

        double lordo = 0;
        Vector righe = new Vector();
        int riga = 0;
//            int totcoll = 0;
//            double totpeso = 0;
//            double totpesolordo = 0;
        for (int i = 0; i < vrighe.size(); i++) {
            Record r = vrighe.get(i);
            if (r.leggiIntero("tiporiga") == 0) {
                // articolo
                Record rmag = new Record();
                rmag = new Record();
                rmag.insElem("corpomagriga", riga++);
                rmag.insElem("corpomaganagid", (int) -1);
                rmag.insElem("corpomagartid", r.leggiIntero("artid"));
                rmag.insElem("corpomagartmovid", r.leggiIntero("artid"));
                rmag.insElem("corpomagvoceid", (int) -1);
                rmag.insElem("corpomaglotto", r.leggiStringa("lotto"));
                rmag.insElem("corpomaglottoid", (int) -1);
                Record rcmx = rcm;
                if (r.leggiDouble("prezzo") < 0 && rcmreso != null) {
                    rcmx = rcmreso;
                }
                rmag.insElem("corpomagcaumag", rcmx.leggiIntero("cmid"));
                rmag.insElem("corpomagcmtipo", rcmx.leggiIntero("cmtipo"));
                rmag.insElem("corpomagcmraggr", rcmx.leggiIntero("cmraggr"));
                rmag.insElem("corpomagdescr", r.leggiStringa("artdescr"));

                String qry3 = "SELECT * FROM articoli LEFT JOIN comunejazz.codiva ON artcodivaid = ivaid WHERE artid = " + r.leggiIntero("artid");
                QueryInterrogazioneClient qa = new QueryInterrogazioneClient(connAz, qry3);
                qa.apertura();
                Record ra = qa.successivo();
                qa.chiusura();

//                rmag.insElem("corpomagum", r.leggiStringa("artum"));
                rmag.insElem("corpomagcolli", r.leggiIntero("colli"));
                //totcoll += r.leggiIntero("colli");
                rmag.insElem("corpomagcollievasi", ((int) 0));
                if (ra != null) {
                    rmag.insElem("corpomagpezzi", ra.leggiDouble("artnpezzi"));
                } else {
                    rmag.insElem("corpomagpezzi", 0.00);
                }

                if (ra != null) {
                    if (ra.leggiIntero("artcauzid") < 1) {
                        rmag.insElem("corpomagcontenuto", (double) 0.0);
                    } else {
                        String qry4 = "SELECT * FROM cauzioni  WHERE cauzid = " + ra.leggiIntero("artcauzid");
                        QueryInterrogazioneClient qz = new QueryInterrogazioneClient(connAz, qry4);
                        qz.apertura();
                        Record rz = qz.successivo();
                        qz.chiusura();
                        rmag.insElem("corpomagcontenuto", r.leggiDouble("qta") * rz.leggiDouble("cauzcapacita"));
                    }
                }

                rmag.insElem("corpomagpesonetto", r.leggiDouble("qta") * ra.leggiDouble("artpeso"));
                //totpeso += (r.leggiDouble("qta") * ra.leggiDouble("artpeso"));
                rmag.insElem("corpomagpesolordo", r.leggiDouble("qta") * ra.leggiDouble("artpesolordo"));
                //totpesolordo += (r.leggiDouble("qta") * ra.leggiDouble("artpesolordo"));
                rmag.insElem("corpomagqta", r.leggiDouble("qta"));
                rmag.insElem("corpomagqtaevasa", r.leggiDouble("qta"));
                rmag.insElem("corpolistinoid", (int) -1);
                rmag.insElem("corpomagnlis", (int) 1);
                rmag.insElem("corpomagprz", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprzvaluta", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprlis", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprlisvaluta", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagartsc1", r.leggiDouble("scart1"));
                rmag.insElem("corpomagartsc2", (double) 0.00);
                rmag.insElem("corpomagartsc3", (double) 0.00);
                rmag.insElem("corpomagartmag", (double) 0.0);
                rmag.insElem("corpomagartprovg", (double) 0.0);
                rmag.insElem("corpomagivaid", r.leggiIntero("ivaid"));

                String qryiva = "SELECT ivaaliq FROM codiva WHERE ivaid = " + r.leggiIntero("ivaid");
                QueryInterrogazioneClient qi = new QueryInterrogazioneClient(connCom, qryiva);
                qi.apertura();
                rmag.insElem("corpomagaliq", qi.successivo().leggiDouble("ivaaliq"));
                qi.chiusura();

                if (r.leggiDouble("esclsctestata") == 1) {
                    rmag.insElem("corpomagquotaprovg", (double) 1.0);
                    rmag.insElem("corpomagcfsc1", (double) 0.0);
                    rmag.insElem("corpomagcfsc2", (double) 0.0);
                    rmag.insElem("corpomagcfsc3", (double) 0.0);
                } else {
                    rmag.insElem("corpomagquotaprovg", (double) 0.0);
                    rmag.insElem("corpomagcfsc1", r.leggiDouble("sccli1"));
                    rmag.insElem("corpomagcfsc2", r.leggiDouble("sccli2"));
                    rmag.insElem("corpomagcfsc3", r.leggiDouble("sccli3"));
                }

                rmag.insElem("corpomagscprom", (double) 0.0);
                rmag.insElem("corpomagmag", (double) 0.0);
                rmag.insElem("corpomagquotaprovg", (double) 0.0);
                lordo += r.leggiDouble("lordo");
                rmag.insElem("corpomaglordo", r.leggiDouble("lordo"));
                rmag.insElem("corpomaglordovaluta", r.leggiDouble("lordo"));
                rmag.insElem("totscontiart", (double) 0.0);
                rmag.insElem("totsconticli", (double) 0.0);
                rmag.insElem("totsconticli1", (double) 0.0);
                rmag.insElem("totsconticli2", (double) 0.0);
                rmag.insElem("totsconticli3", (double) 0.0);
                rmag.insElem("totscontiprom", (double) 0.0);
                rmag.insElem("corpomagnetto", r.leggiDouble("importo"));
                rmag.insElem("corpomagnettoivato", r.leggiDouble("importo"));
                rmag.insElem("corpomagnettovaluta", r.leggiDouble("importo"));
                rmag.insElem("totsconto", (double) 0.0);
                rmag.insElem("corpomagquotascval", (double) 0.0);
                rmag.insElem("corpomagquotascvalvaluta", (double) 0.0);
                rmag.insElem("corpomagage1", rcli.leggiIntero("cliageid"));
                rmag.insElem("corpomagcfpro1", (double) 0.0);
                rmag.insElem("corpomagage2", (int) -1);
                rmag.insElem("corpomagcfpro2", (double) 0.0);
                rmag.insElem("corpomagage3", (int) -1);
                rmag.insElem("corpomagcfpro3", (double) 0.0);
                rmag.insElem("corpomagvaluta", (int) 2);
                rmag.insElem("corpomagcambio", (double) 1);
                rmag.insElem("corpomagcolliagg", (int) 0);
                rmag.insElem("corpomagmacchinaid", (int) -1);
                rmag.insElem("corpomagubicid", (int) -1);
                rmag.insElem("corpomagdtcons", "0000-00-00");
                rmag.insElem("corpomagflagchiusa", (int) 0);
                rmag.insElem("db", "");
                rmag.insElem("dbcomp", "");
                rmag.insElem("corpomagtipodb", (int) 0);
                if (r.esisteCampo("rigaorigid")) {
                    rmag.insElem("rigaorigid", r.leggiIntero("rigaorigid"));
                    rmag.insElem("rigaorigescont", r.leggiStringa("rigaorigescont"));
                }
                
                righe.addElement(rmag);
            } else if (r.leggiIntero("tiporiga") == 1) {
                // non codificato
                Record rmag = new Record();
                rmag = new Record();
                rmag.insElem("corpomagriga", riga++);
                rmag.insElem("corpomaganagid", (int) -1);
                rmag.insElem("corpomagartid", (int) -1);
                rmag.insElem("corpomagartmovid", (int) -1);
                rmag.insElem("corpomagvoceid", (int) -1);
                rmag.insElem("corpomaglotto", r.leggiStringa("lotto"));
                rmag.insElem("corpomaglottoid", (int) -1);
                Record rcmx = rcm;
                if (r.leggiDouble("prezzo") < 0 && rcmreso != null) {
                    rcmx = rcmreso;
                }
                rmag.insElem("corpomagcaumag", rcmx.leggiIntero("cmid"));
                rmag.insElem("corpomagcmtipo", rcmx.leggiIntero("cmtipo"));
                rmag.insElem("corpomagcmraggr", rcmx.leggiIntero("cmraggr"));
                rmag.insElem("corpomagdescr", r.leggiStringa("artdescr"));
//                rmag.insElem("corpomagum", r.leggiStringa("artum"));
                rmag.insElem("corpomagcolli", r.leggiIntero("colli"));
                rmag.insElem("corpomagcollievasi", ((int) 0));
                rmag.insElem("corpomagpezzi", (double) 0);
                rmag.insElem("corpomagcontenuto", r.leggiDouble("qta"));
                rmag.insElem("corpomagpesonetto", (double) 0);
                rmag.insElem("corpomagpesolordo", (double) 0);
                rmag.insElem("corpomagqta", r.leggiDouble("qta"));
                rmag.insElem("corpomagqtaevasa", r.leggiDouble("qta"));
                rmag.insElem("corpolistinoid", (int) -1);
                rmag.insElem("corpomagnlis", (int) 1);
                rmag.insElem("corpomagprz", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprzvaluta", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprlis", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprlisvaluta", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagartsc1", r.leggiDouble("scart1"));
                rmag.insElem("corpomagartsc2", (double) 0.00);
                rmag.insElem("corpomagartsc3", (double) 0.00);
                rmag.insElem("corpomagartmag", (double) 0.0);
                rmag.insElem("corpomagartprovg", (double) 0.0);
                rmag.insElem("corpomagivaid", r.leggiIntero("ivaid"));
                String qryiva = "SELECT ivaaliq FROM codiva WHERE ivaid = " + r.leggiIntero("ivaid");
                QueryInterrogazioneClient qi = new QueryInterrogazioneClient(connCom, qryiva);
                qi.apertura();
                rmag.insElem("corpomagaliq", qi.successivo().leggiDouble("ivaaliq"));
                qi.chiusura();

                if (r.leggiDouble("esclsctestata") == 1) {
                    rmag.insElem("corpomagquotaprovg", (double) 1.0);
                    rmag.insElem("corpomagcfsc1", (double) 0.0);
                    rmag.insElem("corpomagcfsc2", (double) 0.0);
                    rmag.insElem("corpomagcfsc3", (double) 0.0);
                } else {
                    rmag.insElem("corpomagquotaprovg", (double) 0.0);
                    rmag.insElem("corpomagcfsc1", r.leggiDouble("sccli1"));
                    rmag.insElem("corpomagcfsc2", r.leggiDouble("sccli2"));
                    rmag.insElem("corpomagcfsc3", r.leggiDouble("sccli3"));
                }
                rmag.insElem("corpomagscprom", (double) 0.0);
                rmag.insElem("corpomagmag", (double) 0.0);
                rmag.insElem("corpomagquotaprovg", (double) 0.0);
                lordo += r.leggiDouble("lordo");
                rmag.insElem("corpomaglordo", r.leggiDouble("lordo"));
                rmag.insElem("corpomaglordovaluta", r.leggiDouble("lordo"));
                rmag.insElem("totscontiart", (double) 0.0);
                rmag.insElem("totsconticli", (double) 0.0);
                rmag.insElem("totsconticli1", (double) 0.0);
                rmag.insElem("totsconticli2", (double) 0.0);
                rmag.insElem("totsconticli3", (double) 0.0);
                rmag.insElem("totscontiprom", (double) 0.0);
                rmag.insElem("corpomagnetto", r.leggiDouble("importo"));
                rmag.insElem("corpomagnettoivato", r.leggiDouble("importo"));
                rmag.insElem("corpomagnettovaluta", r.leggiDouble("importo"));
                rmag.insElem("totsconto", (double) 0.0);
                rmag.insElem("corpomagquotascval", (double) 0.0);
                rmag.insElem("corpomagquotascvalvaluta", (double) 0.0);
                rmag.insElem("corpomagage1", rcli.leggiIntero("cliageid"));
                rmag.insElem("corpomagcfpro1", (double) 0.0);
                rmag.insElem("corpomagage2", (int) -1);
                rmag.insElem("corpomagcfpro2", (double) 0.0);
                rmag.insElem("corpomagage3", (int) -1);
                rmag.insElem("corpomagcfpro3", (double) 0.0);
                rmag.insElem("corpomagvaluta", (int) 2);
                rmag.insElem("corpomagcambio", (double) 1);
                rmag.insElem("corpomagcolliagg", (int) 0);
                rmag.insElem("corpomagmacchinaid", (int) -1);
                rmag.insElem("corpomagubicid", (int) -1);
                rmag.insElem("corpomagdtcons", "0000-00-00");
                rmag.insElem("corpomagflagchiusa", (int) 0);
                rmag.insElem("db", "");
                rmag.insElem("dbcomp", "");
                rmag.insElem("corpomagtipodb", (int) 0);

                Record cespite = cespitidaregistrare.get(i);
                if (cespite.leggiIntero("cespid") != -1) {
                    rmag.insElem("corpomagpdcid", cespite.leggiIntero("immobilizzazionepdc"));
                }
                righe.addElement(rmag);
            } else {
                // descrittivo
                Record rmag = new Record();
                rmag = new Record();
                rmag.insElem("corpomagriga", riga++);
                rmag.insElem("corpomaganagid", (int) -1);
                rmag.insElem("corpomagartid", (int) -2);
                rmag.insElem("corpomagartmovid", (int) -2);
                rmag.insElem("corpomagvoceid", (int) -1);
                rmag.insElem("corpomaglotto", r.leggiStringa("lotto"));
                rmag.insElem("corpomaglottoid", (int) -1);
                rmag.insElem("corpomagcaumag", rcm.leggiIntero("cmid"));
                rmag.insElem("corpomagcmtipo", rcm.leggiIntero("cmtipo"));
                rmag.insElem("corpomagcmraggr", rcm.leggiIntero("cmraggr"));
                rmag.insElem("corpomagdescr", r.leggiStringa("artdescr"));
                rmag.insElem("corpomagum", "");
                rmag.insElem("corpomagcolli", r.leggiIntero("colli"));
                rmag.insElem("corpomagcollievasi", ((int) 0));
                rmag.insElem("corpomagpezzi", (double) 0);
                rmag.insElem("corpomagcontenuto", r.leggiDouble("qta"));
                rmag.insElem("corpomagpesonetto", (double) 0);
                rmag.insElem("corpomagpesolordo", (double) 0);
                rmag.insElem("corpomagqta", r.leggiDouble("qta"));
                rmag.insElem("corpomagqtaevasa", r.leggiDouble("qta"));
                rmag.insElem("corpolistinoid", (int) -1);
                rmag.insElem("corpomagnlis", (int) 1);
                rmag.insElem("corpomagprz", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprzvaluta", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprlis", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagprlisvaluta", r.leggiDouble("prezzo"));
                rmag.insElem("corpomagartsc1", r.leggiDouble("scart1"));
                rmag.insElem("corpomagartsc2", 0.00);
                rmag.insElem("corpomagartsc3", 0.00);
                rmag.insElem("corpomagartmag", (double) 0.0);
                rmag.insElem("corpomagartprovg", (double) 0.0);
                rmag.insElem("corpomagivaid", (int) -1);
                rmag.insElem("corpomagaliq", (double) 0);
                rmag.insElem("corpomagquotaprovg", (double) 1.0);
                rmag.insElem("corpomagcfsc1", (double) 0.0);
                rmag.insElem("corpomagcfsc2", (double) 0.0);
                rmag.insElem("corpomagcfsc3", (double) 0.0);
                rmag.insElem("corpomagscprom", (double) 0.0);
                rmag.insElem("corpomagmag", (double) 0.0);
                rmag.insElem("corpomagquotaprovg", (double) 0.0);
                rmag.insElem("corpomaglordo", (double) 0.0);
                rmag.insElem("corpomaglordovaluta", (double) 0.0);
                rmag.insElem("totscontiart", (double) 0.0);
                rmag.insElem("totsconticli", (double) 0.0);
                rmag.insElem("totsconticli1", (double) 0.0);
                rmag.insElem("totsconticli2", (double) 0.0);
                rmag.insElem("totsconticli3", (double) 0.0);
                rmag.insElem("totscontiprom", (double) 0.0);
                rmag.insElem("corpomagnetto", (double) 0.0);
                rmag.insElem("corpomagnettoivato", (double) 0.0);
                rmag.insElem("corpomagnettovaluta", (double) 0.0);
                rmag.insElem("totsconto", (double) 0.0);
                rmag.insElem("corpomagquotascval", (double) 0.0);
                rmag.insElem("corpomagquotascvalvaluta", (double) 0.0);
                rmag.insElem("corpomagage1", (int) -1);
                rmag.insElem("corpomagcfpro1", (double) 0.0);
                rmag.insElem("corpomagage2", (int) -1);
                rmag.insElem("corpomagcfpro2", (double) 0.0);
                rmag.insElem("corpomagage3", (int) -1);
                rmag.insElem("corpomagcfpro3", (double) 0.0);
                rmag.insElem("corpomagvaluta", (int) 2);
                rmag.insElem("corpomagcambio", (double) 1);
                rmag.insElem("corpomagcolliagg", (int) 0);
                rmag.insElem("corpomagmacchinaid", (int) -1);
                rmag.insElem("corpomagubicid", (int) -1);
                rmag.insElem("corpomagdtcons", "0000-00-00");
                rmag.insElem("corpomagflagchiusa", (int) 0);
                rmag.insElem("db", "");
                rmag.insElem("dbcomp", "");
                rmag.insElem("corpomagtipodb", (int) 0);
                righe.addElement(rmag);
            }
        }

        Record rp = new Record();
        rp.insElem("piedestid", consegna.leggiId());
        rp.insElem("pieprovid", (int) -1);
        rp.insElem("piedestind", "");
        rp.insElem("pieloc", "");
        rp.insElem("piecura", tipotrasporto.getSelectedIndex());
        rp.insElem("piecausa", (String) causaletrasporto.getSelectedItem());
        rp.insElem("pieaspe", (String) aspettobeni.getSelectedItem());
        rp.insElem("piesped1id", vettore.leggiId());
        rp.insElem("piespedind1", "");
        rp.insElem("piespeddt1", dataritiro.getDataStringa());
        rp.insElem("piespedho1", oraritiro.leggiOrario());
        rp.insElem("piesped2id", (int) -1);
        rp.insElem("piespedind2", "");
        rp.insElem("piespeddt2", "0000-00-00");
        rp.insElem("piespedho2", "00:00");
        rp.insElem("pienumcol", totcolli.getValoreIntero());
        rp.insElem("pienumpallet", totpallets.getValoreIntero());
        rp.insElem("piepeso", totpesonetto.getValore());
        rp.insElem("piepesolordo", totpesolordo.getValore());
        rp.insElem("pieritdt", datapart.getDataStringa());
        rp.insElem("pieritho", orapart.leggiOrario());
        rp.insElem("pieportoid", porto.leggiId());
        rp.insElem("pienote1", notepiede.getText().trim());
        rp.insElem("pienote2", "");
        rp.insElem("pienote3", "");
        rp.insElem("pienote4", "");
        rp.insElem("pienote5", "");
        rp.insElem("pienote6", "");
        rp.insElem("pienote12", targa.getText().trim());
        Record rr = new Record();
        double timp = Formattazione.estraiDouble(totimp.getText());
        double tdoc = Formattazione.estraiDouble(totdoc.getText());
        double taccr = Formattazione.estraiDouble(totaccred.getText());
        double tabb = Formattazione.estraiDouble(totabbuono.getText());
        double tnetto = Formattazione.estraiDouble(totnetto.getText());
        rr.insElem("riepcodpagid", rt.leggiIntero("testapagid"));
        rr.insElem("riepamezzo", (int) 0);
        rr.insElem("rieptotlordomerce", timp);
        rr.insElem("rieptotspesevarie", (double) 0);
        rr.insElem("rieptotsconticli", (double) 0);
        rr.insElem("rieptotscontiart", (double) 0);
        rr.insElem("rieptotscontiprom", (double) 0);
        rr.insElem("riepomaggi", (double) 0);
        rr.insElem("rieptotmerce", timp);
        rr.insElem("rieptotnetti", timp);
        rr.insElem("rieptrasperc", (double) 0);
        rr.insElem("rieptrasimp", (double) 0);
        rr.insElem("riepscval", (double) 0);
        rr.insElem("riepspeseinc", (double) 0);
        rr.insElem("riepspesebol", (double) 0);
        rr.insElem("riepspeseimb", (double) 0);
        rr.insElem("rieptotimpon", timp);
        rr.insElem("rieptotiva", Formattazione.estraiDouble(totiva.getText()));
        rr.insElem("rieptotale", tdoc);
        rr.insElem("riepaddeb", (double) 0);
        rr.insElem("riepaccred", taccr);
        rr.insElem("riepcauzadd", (double) 0);
        rr.insElem("riepcauzacc", (double) 0);
        rr.insElem("riepabbuono", tabb);
        rr.insElem("riepnettoapag", tnetto);
        rr.insElem("riepesc1", (int) 0);
        rr.insElem("riepesc2", (int) 0);
        rr.insElem("riepggpref", (int) 0);
        rr.insElem("riepaccontofatt", totpag.getValore());
        rr.insElem("riepmargine", (double) 0);
        rr.insElem("riepaccontoprecid", (int) -1);
        rr.insElem("ivaomaggitot",0.00);
        rr.insElem("riepconai",0.00);
        rr.insElem("riepbollovirtuale",0.00);

        Record rpag = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "pagamenti", "pagid", pagamento.leggiId(), "");
        Vector vsc = new Vector();
        for (int i = 0; i < vscad.size(); i++) {
            Record rscad = vscad.get(i);
            Record rscx = new Record();
            if (rscad.esisteCampo("id")) {
                rscx.insElem("scadid", rscad.leggiIntero("id"));
            }
            rscx.insElem("scadportafoglio", (int) 8);
            rscx.insElem("scadimp", rscad.leggiDouble("importo"));
            rscx.insElem("scaddt", rscad.leggiStringa("data"));
            rscx.insElem("scaddocid", (int) -1);
            rscx.insElem("scadsegno", rDoc.leggiIntero("tdsegno"));
            rscx.insElem("scadmodo", rpag.leggiIntero("pagtipo"));
            rscx.insElem("scadvalutaid", (int) 2);
            rscx.insElem("scadcambio", (double) 1);
            rscx.insElem("scadpagid", rpag.leggiIntero("pagid"));
            rscx.insElem("scadage1id", (int) -1);
            rscx.insElem("scadage2id", (int) -1);
            rscx.insElem("scadage3id", (int) -1);
            rscx.insElem("scadbancaid", (int) -1);
            rscx.insElem("scadagenzia", "");
            rscx.insElem("scadcab", "");
            vsc.addElement(rscx);
        }

        Vector righeIva = new Vector();
        Iterator it = hiva.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            Record riva = hiva.get(key);
            Record rx = new Record();
            rx.insElem("mivacodid", riva.leggiIntero("id"));
            rx.insElem("mivaimp", riva.leggiDouble("imponibile"));
            rx.insElem("mivaimpvaluta", riva.leggiDouble("imponibile"));
            rx.insElem("mivaiva", riva.leggiDouble("iva"));
            rx.insElem("mivaivavaluta", riva.leggiDouble("iva"));
            rx.insElem("mivaindetr", (double) 0);
            rx.insElem("mivaindetrvaluta", (double) 0);
            rx.insElem("mivavaluta", (int) 2);
            righeIva.addElement(rx);
        }

        Vector listaMovDocOrig = new Vector();
//        for (int i = 0; i < vord.size(); i++) {
//            Record rord = vord.get(i);
//            Record rx = new Record();
//            rx.insElem("rlmovdocorigescont", rord.leggiStringa("esmov"));
//            rx.insElem("rlmovdocorigid", rord.leggiIntero("movdocid"));
//            listaMovDocOrig.addElement(rx);
//        }

//        String es = ((Record) ves.firstElement()).leggiStringa("esercizio");
        String es = ves.get(0);
        Record ris = FunzioniDB.salvaDocumento(connAz, connCom, es,
                datamov.getDataStringa(), rDoc.leggiIntero("docid"), depId, pedId,
                EnvJazz.recOperatore.leggiIntero("opeid"), movdocId, -1, rMovDoc, rt, righe, righeIva,
                rp, rr, new Vector(), vsc, modo, 2, new Vector(), new Vector(), new Vector(),
                listaMovDocOrig, new Vector(), 0, new Vector(), new Vector());
        if (ris.leggiIntero("esito") == EnvJazz.SUCCESSO) {
            movdocId=ris.leggiIntero("mdid");
            modo = EnvJazz.AGG;
            modificaPrimaNota(es, ris.leggiIntero("mdid"));
            
            // acconti
            for (int i = 0; i < listaAnticipi.size(); i++) {
                Record rant = (Record) listaAnticipi.elementAt(i);
                QueryAggiornamentoClient qdant = new QueryAggiornamentoClient(connAz, "DELETE FROM ftacconto_saldi WHERE facsmovdocftevasid = " + movdocId + " AND facsescontftevas = \"" + esContabile + "\"");
                qdant.esecuzione();
                Record rinsant = new Record();
                rinsant.insElem("facsescontftacc", rant.leggiStringa("escontftacc"));
                rinsant.insElem("facsmovdocftaccid", rant.leggiIntero("movdocftaccid"));
                rinsant.insElem("facsescontftevas", esContabile);
                rinsant.insElem("facsmovdocftevasid", movdocId);
                rinsant.insElem("facspercevas", rant.leggiDouble("percevas"));
                QueryAggiornamentoClient qinsa = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "ftacconto_saldi", "facsid", rinsant);
                qinsa.esecuzione();
                if (rant.leggiDouble("percevas") == 100) {
                    QueryAggiornamentoClient qaggacc = new QueryAggiornamentoClient(connAz, "UPDATE ftacconto SET facchiusa = 1 WHERE facescontftacc = \"" + rant.leggiStringa("escontftacc") + "\" AND facmovdocftaccid = " + rant.leggiIntero("movdocftaccid"));
                    qaggacc.esecuzione();
                }
            }

            String qry2 = "SELECT clistampaprz FROM clienti WHERE cliid = " + cliente.leggiId();
            QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connAz, qry2);
            qc.apertura();
            int sp = qc.successivo().leggiIntero("clistampaprz");
            qc.chiusura();

            Record ragg = new Record();
            ragg.insElem("movdocid", ris.leggiIntero("mdid"));
            if (sp == 0 || sp == 2) {
                ragg.insElem("movdocmodo", (int) 1);
            } else {
                ragg.insElem("movdocmodo", (int) 0);
            }
            QueryAggiornamentoClient qagg = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG,
                    "movimentidoc" + es, "movdocid", ragg);
            qagg.esecuzione();

//            Query qfe = new Query(connAz, "SELECT * FROM movfe WHERE mfeescontmov = \"" + es + "\" AND mfemovdocid = " + ris.leggiIntero("mdid"));
//            qfe.apertura();
//            Record rfe = qfe.successivo(); 
//            qfe.chiusura();
//            if (rfe != null)
//            {
//                rfe.eliminaCampo("mfebollovirtuale");
//                rfe.eliminaCampo("mfeimpbollo");
//                rfe.insElem("mfebollovirtuale", (int) (datiagg_bollovirtuale.isSelected()?1:0));
//                datiagg_impbollo.insValuta(rfe, "mfeimpbollo");
//                QueryAggiornamentoClient qaggfe = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG,
//                        "movfe", "mfeid", rfe);
//                qaggfe.esecuzione();
//            }
//            else
//            {
//                Record rx = new Record();
//                rx.insElem("mfeescontmov", es);
//                rx.insElem("mfemovdocid", ris.leggiIntero("mdid"));
//                rx.insElem("mfebollovirtuale", (int) (datiagg_bollovirtuale.isSelected()?1:0));
//                datiagg_impbollo.insValuta(rx, "mfeimpbollo");
//                QueryAggiornamentoClient qinsfe = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS,
//                        "movfe", "mfeid", rx);
//                qinsfe.esecuzione();
//            }
            // dati aggiuntivi
            HashMap<Integer, Integer> hprog = new HashMap();
            for (int i = 0; i < vdatiagg.size(); i++) {
                Record rx = vdatiagg.get(i);
                int prog = 1;
                if (hprog.get(rx.leggiIntero("mfedatiporec")) == null) {
                    hprog.put(rx.leggiIntero("mfedatiporec"), prog);
                } else {
                    prog = hprog.get(rx.leggiIntero("mfedatiporec"));
                    prog++;
                    hprog.put(rx.leggiIntero("mfedatiporec"), prog);
                }
                rx.insElem("mfedanumrec", prog);
            }

            String qrydelete = "DELETE FROM movfe_datiagg WHERE mfedaescontmov = \"" + es + "\" AND mfedamovdocid = " + ris.leggiIntero("mdid");
            QueryAggiornamentoClient qddagg = new QueryAggiornamentoClient(connAz, qrydelete);
            qddagg.esecuzione();

            for (int i = 0; i < vdatiagg.size(); i++) {
                Record rdatiagg = vdatiagg.get(i);
                Record rx = new Record();
                rx.insElem("mfedaescontmov", es);
                rx.insElem("mfedamovdocid", ris.leggiIntero("mdid"));
                rx.insElem("mfedatiporec", rdatiagg.leggiIntero("mfedatiporec"));
                rx.insElem("mfedanumrec", rdatiagg.leggiIntero("mfedanumrec"));
                rx.insElem("mfedanumdoc", rdatiagg.leggiStringa("mfedanumdoc"));
                rx.insElem("mfedadatadoc", rdatiagg.leggiStringa("mfedadatadoc"));
                rx.insElem("mfedacodcommessa", rdatiagg.leggiStringa("mfedacodcommessa"));
                rx.insElem("mfedacodcup", rdatiagg.leggiStringa("mfedacodcup"));
                rx.insElem("mfedacodcig", rdatiagg.leggiStringa("mfedacodcig"));
                QueryAggiornamentoClient qinsdagg = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS,
                        "movfe_datiagg", "mfedaid", rx);
                qinsdagg.esecuzione();
            }

            // allegati
            qrydelete = "DELETE FROM movfe_allegati WHERE mfeaescontmov = \"" + es + "\" AND mfeamovdocid = " + ris.leggiIntero("mdid");
            QueryAggiornamentoClient qdall = new QueryAggiornamentoClient(connAz, qrydelete);
            qdall.esecuzione();
            for (int i = 0; i < vallegati.size(); i++) {
                Record rall = vallegati.get(i);
                Record rx = new Record();
                rx.insElem("mfeaescontmov", es);
                rx.insElem("mfeamovdocid", ris.leggiIntero("mdid"));
                rx.insElem("mfeanome", rall.leggiStringa("mfeanome"));
                rx.insElem("mfeaalgcompr", rall.leggiStringa("mfeaalgcompr"));
                rx.insElem("mfeaformato", rall.leggiStringa("mfeaformato"));
                rx.insElem("mfeadescr", rall.leggiStringa("mfeadescr"));
                rx.insElem("mfealinkdoc", rall.leggiStringa("mfealinkdoc"));
                QueryAggiornamentoClient qinsall = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS,
                        "movfe_allegati", "mfeaid", rx);
                qinsall.esecuzione();
            }

            // legami con fatture per nota credito
            qrydelete = "DELETE FROM legamidoc WHERE ldmovdocdestid = " + ris.leggiIntero("mdid") + " AND ldescontdest = \"" + es + "\"";
            QueryAggiornamentoClient qdl = new QueryAggiornamentoClient(connAz, qrydelete);
            qdl.esecuzione();

            for (int i = 0; i < vnotac.size(); i++) {
                Record rnotac = vnotac.get(i);
                Record rx = new Record();
                rx.insElem("ldmovdocorigid", rnotac.leggiIntero("movdocid"));
                rx.insElem("ldescontorig", rnotac.leggiStringa("esmov"));
                rx.insElem("ldescontdest", es);
                rx.insElem("ldmovdocdestid", ris.leggiIntero("mdid"));
                rx.insElem("ldfeorignum", rnotac.leggiStringa("numerodocman"));
                rx.insElem("ldfeorigdt", rnotac.leggiStringa("datadocman"));
                QueryAggiornamentoClient qinsld = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS,
                        "legamidoc", "ldid", rx);
                qinsld.esecuzione();
            }

            // dati aggiuntivi corpo
            qrydelete = "DELETE FROM corpofe" + es + " WHERE cfemovdocid = " + ris.leggiIntero("mdid");
            QueryAggiornamentoClient qdcfe = new QueryAggiornamentoClient(connAz, qrydelete);
            qdcfe.esecuzione();

            String qry3 = "SELECT corpomagriga,corpomagid FROM corpodocmag" + es + " WHERE corpomagmovdocid = " + ris.leggiIntero("mdid") + " ORDER BY corpomagriga";
            QueryInterrogazioneClient qcrp = new QueryInterrogazioneClient(connAz, qry3);
            qcrp.apertura();
            for (int i = 0; i < qcrp.numeroRecord(); i++) {
                Record rcrp = qcrp.successivo();
                Record r = vrighe.get(i);
                if (r.esisteCampo("serviziodatai") && !r.leggiStringa("serviziodatai").equals("0000-00-00")) {
                    Record rcfe = new Record();
                    rcfe.insElem("cfemovdocid", ris.leggiIntero("mdid"));
                    rcfe.insElem("cfecorpoid", rcrp.leggiIntero("corpomagid"));
                    rcfe.insElem("cfeserviziodatai", r.leggiStringa("serviziodatai"));
                    rcfe.insElem("cfeserviziodataf", r.leggiStringa("serviziodataf"));
                    QueryAggiornamentoClient qinscfe = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS,
                            "corpofe" + es, "cfeid", rcfe);
                    qinscfe.esecuzione();
                }
            }
            qcrp.chiusura();

            docModificato = true;
        }

        waitloc.ferma();

        movdocId = ris.leggiIntero("mdid");
        esmov = es;

//        salvaNumFiscale();
        JOptionPane.showMessageDialog(this, "Documento salvato con il numero " + ris.leggiIntero("numero"),
                "", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        if (BusinessLogicAttivazione.controlloPluginAttivoLocale(EnvJazz.connAmbiente.leggiConnessione(), EnvJazz.azienda, EnvJazz.PLUGIN_JBRIDGE)) {
            // controllo dati fattura elettronica
            ArrayList<String> verrfe = BusinessLogicDocumenti.elencoErroriFatturaElettronicaMovimento(connAz.leggiConnessione(), connCom.leggiConnessione(),
                    EnvJazz.connAmbiente.leggiConnessione(), azienda, es, movdocId);
            if (verrfe.size() > 0) {
                String err = "";
                for (String er : verrfe) {
                    err += er + "\r\n";
                }
                JOptionPane.showMessageDialog(this, "Errori di contenuto sulla fattura elettronica:\r\n\r\n" + err + "\r\nIl file della fattura elettronica non sarà generato\r\nCorreggere gli errori segnalati e rimemorizzare il documento",
                        "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            } else {
                // genera file fattura elettronica
                boolean generaFE = true;

                if (generaFE) {
                    File ff = new File("");
                    File fctr = new File(ff.getAbsolutePath() + File.separator + "tmpDoc");
                    if (fctr.exists()) {
                        boolean nuovoprog = false;
                        boolean inviata = false;
                        //boolean scartata = false;
                        String qry3 = "SELECT * FROM eft_archivio" + esContabile + " WHERE eftmovdocid = " + movdocId + " AND eftescontmov = \"" + es + "\" ORDER BY eftid DESC";
                        QueryInterrogazioneClient qeft = new QueryInterrogazioneClient(connAz, qry3);
                        qeft.apertura();
                        Record reft = qeft.successivo();
                        qeft.chiusura();

                        qry3 = "SELECT * FROM movfe WHERE mfeescontmov = \"" + es + "\" AND mfemovdocid = " + movdocId;
                        QueryInterrogazioneClient qfe = new QueryInterrogazioneClient(connAz, qry3);
                        qfe.apertura();
                        Record rfe = qfe.successivo();
                        qfe.chiusura();

                        if (reft == null || reft.leggiIntero("eftstato") == 0) {
                            inviata = false;
                        } else {
                            inviata = true;
                        }

                        boolean nuovoprogPA = false;
                        int proginvio = 0;
                        if (!inviata) {
                            boolean clipanuovoprog = false;
                            if (rfe != null && rcli.leggiIntero("clienteazi") == 1) {
                                // ente pubblico
                                Object[] options
                                        = {
                                            "Rigenera con stesso progressivo", "Rigenera con nuovo progressivo"
                                        };
                                Object selectedValue = JOptionPane.showOptionDialog(this, "Il file per la fattura elettronica è stato già generato:\n" + rfe.leggiStringa("mfenomefile") + " con progressivo " + rfe.leggiIntero("mfeprog") + "\n", "Cliente PA: Scelta operazione",
                                        JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);
                                if (selectedValue.toString().equals("0")) {
                                    clipanuovoprog = false;
                                    //proginvio = rfe.leggiIntero("mfeprog");
                                } else {
                                    clipanuovoprog = true;
                                }

                                if (clipanuovoprog) {
                                    // nuovo progressivo
                                    nuovoprogPA = true;
                                    String qry4 = "SELECT eftid FROM eft_archivio" + esContabile + " WHERE eftmovdocid = " + movdocId
                                            + " AND eftescontmov = \"" + es + "\"";
                                    QueryInterrogazioneClient qeftx = new QueryInterrogazioneClient(connAz, qry4);
                                    qeftx.apertura();
                                    for (int neftx = 0; neftx < qeftx.numeroRecord(); neftx++) {
                                        Record reftx = qeftx.successivo();
                                        QueryAggiornamentoClient qaggeft = new QueryAggiornamentoClient(connAz, "UPDATE eft_archivio" + esContabile + " SET eftmovdocid = -1,eftescontmov = \"\" WHERE eftid = " + reftx.leggiIntero("eftid"));
                                        qaggeft.esecuzione();
                                    }
                                    qeft.chiusura();

                                    proginvio = Formattazione.estraiIntero(BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "fatturaelettronica", "proginvio"));
                                    proginvio = BusinessLogicDocumenti.cercaProgressivoFatturaElettronicaLibero(connAz.leggiConnessione(),
                                            !raz.leggiStringa("codicefiscale").equals("") ? raz.leggiStringa("codicefiscale") : raz.leggiStringa("partitaiva"), proginvio, es);
                                    if (proginvio == 0) {
                                        proginvio = 1;
                                    }
                                    nuovoprog = true;
                                } else {
                                    proginvio = rfe.leggiIntero("mfeprog");
                                    nuovoprog = false;
                                }
                            } else {
                                // fattura a privato
                                // se file gia generato in precedenza mantiene lo stesso progressivo
                                // se file non generato ancora, genera nuovo progressivo
                                int precproginvio = 0;
                                if (rfe != null) {
                                    precproginvio = rfe.leggiIntero("mfeprog");
                                }

                                if (precproginvio > 0) {
                                    proginvio = precproginvio;
                                    nuovoprog = false;
                                } else {
                                    proginvio = Formattazione.estraiIntero(BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "fatturaelettronica", "proginvio"));
                                    proginvio = BusinessLogicDocumenti.cercaProgressivoFatturaElettronicaLibero(connAz.leggiConnessione(),
                                            !raz.leggiStringa("codicefiscale").equals("") ? raz.leggiStringa("codicefiscale") : raz.leggiStringa("partitaiva"), proginvio, es);
                                    if (proginvio == 0) {
                                        proginvio = 1;
                                    }
                                    nuovoprog = true;
                                }
                            }
                        } else {
                            // caso inviata o nuova
                            // nuovo progressivo
                            proginvio = Formattazione.estraiIntero(BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "fatturaelettronica", "proginvio"));
                            proginvio = BusinessLogicDocumenti.cercaProgressivoFatturaElettronicaLibero(connAz.leggiConnessione(),
                                    !raz.leggiStringa("codicefiscale").equals("") ? raz.leggiStringa("codicefiscale") : raz.leggiStringa("partitaiva"), proginvio, es);
                            if (proginvio == 0) {
                                proginvio = 1;
                            }
                            nuovoprog = true;
                        }

                        String progb36 = funzStringa.riempiStringa(Integer.toString(proginvio, 36).toUpperCase(), 5, funzStringa.DX, '0');
                        String nffe = "";
                        if (!raz.leggiStringa("partitaiva").equals("")) {
                            nffe = "IT" + raz.leggiStringa("partitaiva") + "_" + funzStringa.riempiStringa(progb36, 5, funzStringa.DX, '0') + ".xml";
                        } else {
                            nffe = "IT" + raz.leggiStringa("codicefiscale") + "_" + funzStringa.riempiStringa(progb36, 5, funzStringa.DX, '0') + ".xml";
                        }

                        // allega PDF fattura
                        String fileAllegatoPdf = "";
                        if ((allegaPdf || rcli.leggiIntero("clifeallegapdf") == 1) && rDoc.leggiIntero("docmodstid") != -1) {
                            Record rmodst = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "modellistampa", "modstid", rDoc.leggiIntero("docmodstid"), "");
                            if (rmodst != null && FunzioniDB.esisteReport(rmodst.leggiStringa("modstreport"))) {
                                String nalleg = "doc_" + rDoc.leggiStringa("doccod") + "_" + numero.getText() + "_" + datamov.getData().formatta(Data.GGMMAAAA) + ".pdf";
                                boolean pdfok = BusinessLogicDocumenti.generaPdfDocumento(connAz, azienda, es, movdocId, rmodst, nalleg);
                                if (pdfok) {
                                    fileAllegatoPdf = "tmp" + File.separator + nalleg;
                                }
                            }
                        }

                        String qrydelete = "DELETE FROM movfe WHERE mfeescontmov = \"" + es + "\" AND mfemovdocid = " + movdocId;
                        QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
                        qd.esecuzione();

                        Record rinsfe = new Record();
                        rinsfe.insElem("mfeescontmov", es);
                        rinsfe.insElem("mfemovdocid", movdocId);
                        rinsfe.insElem("mfenomefile", nffe);
                        rinsfe.insElem("mfeprog", proginvio);
                        rinsfe.insElem("mfedatagen", (new Data()).formatta(Data.AAAA_MM_GG, "-"));
                        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                        rinsfe.insElem("mfeoragen", df.format(new java.util.Date()));
                        rinsfe.insElem("mfebollovirtuale", (int) (datiagg_bollovirtuale.isSelected() ? 1 : 0));
                        datiagg_impbollo.insValuta(rinsfe, "mfeimpbollo");
                        QueryAggiornamentoClient qinsfe = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "movfe", "mfeid", rinsfe);
                        qinsfe.esecuzione();

                        if (nuovoprog) {
                            int progatt = proginvio;
                            progatt++;
                            BusinessLogicImpostazioni.scriviProprieta(connAz.leggiConnessione(), "fatturaelettronica", "proginvio", "" + (progatt));
                        }

                        boolean risfe = BusinessLogicDocumenti.generaFileFatturaElettronica(EnvJazz.connAmbiente.conn, connAz, connCom, azienda, es, movdocId, ff.getAbsolutePath() + File.separator + "tmpDoc" + File.separator + nffe, false, 0, fileAllegatoPdf);
                        if (!risfe) {
                            JOptionPane.showMessageDialog(this, "Errore su generazione file XML fattura elettronica",
                                    "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        } else {
                            try {
                                DecodificheXML dx = new DecodificheXML();
                                FatturaElettronicaType fatturaElettronica = dx.decodificaFatturaElettronica(new File(ff.getAbsolutePath() + File.separator + "tmpDoc" + File.separator + nffe));
                                java.util.List<FatturaElettronicaBodyType> fb = fatturaElettronica.getFatturaElettronicaBody();
                                for (int v = 0; v < fb.size(); v++) {
                                    FatturaElettronicaBodyType fbi = fb.get(v);
                                    Record rins = new Record();
                                    rins.insElem("eftmovdocid", movdocId);
                                    rins.insElem("eftescontmov", es);
                                    rins.insElem("efttipo", (int) 0);
                                    rins.insElem("eftnomeorigfile", nffe);
                                    rins.insElem("efttipodoc", fbi.getDatiGenerali().getDatiGeneraliDocumento().getTipoDocumento().toString());
                                    String ragsoc = "";
                                    if (fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getAnagrafica().getDenominazione() == null || fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getAnagrafica().getDenominazione().equals("null") || fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getAnagrafica().getDenominazione().equals("")) {
                                        ragsoc = fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getAnagrafica().getCognome() + " " + fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getAnagrafica().getNome().toString();
                                    } else {
                                        ragsoc = fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getAnagrafica().getDenominazione();
                                    }

                                    rins.insElem("eftragsoc", ragsoc);
                                    java.util.Date dtft = asDate(fbi.getDatiGenerali().getDatiGeneraliDocumento().getData());
                                    String datafat = convertStringToDate(dtft);
                                    rins.insElem("eftdata", datafat);
                                    rins.insElem("eftnum", fbi.getDatiGenerali().getDatiGeneraliDocumento().getNumero());
                                    if (fbi.getDatiGenerali().getDatiGeneraliDocumento().getDatiRitenuta() != null) {
                                        rins.insElem("eftritenuta", (int) 1);
                                    } else {
                                        rins.insElem("eftritenuta", (int) 0);
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA() != null && !fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA().getIdCodice().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA().getIdCodice().equals("")) {
                                        rins.insElem("eftpiva", fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA().getIdCodice());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getCodiceFiscale() != null && !fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getCodiceFiscale().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getCodiceFiscale().equals("")) {
                                        rins.insElem("eftcodfisc", fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getCodiceFiscale());
                                    }

                                    if (fbi.getDatiGenerali().getDatiGeneraliDocumento().getImportoTotaleDocumento() != null) {
                                        rins.insElem("efttotdoc", (fbi.getDatiGenerali().getDatiGeneraliDocumento().getImportoTotaleDocumento()).doubleValue());
                                    }

                                    ArrayList<Record> pagRecord = salvaDatiPagamento(fbi.getDatiPagamento());
                                    if (!pagRecord.isEmpty()) {
                                        rins.insElem("eftmodpag", pagRecord.get(0).leggiStringa("edpmodpag"));
                                    }

//                                    java.util.List<DatiPagamentoType> lp = fbi.getDatiPagamento();
//                                    if (lp.size() > 0)
//                                    {
//                                        DatiPagamentoType dpt = lp.get(0);
//                                        java.util.List<DettaglioPagamentoType> dept = dpt.getDettaglioPagamento();
//                                        DettaglioPagamentoType dpty = dept.get(0);
//                                        rins.insElem("eftmodpag", dpty.getModalitaPagamento().toString());
//                                    }
                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdPaese() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdPaese().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdPaese().equals("")) {
                                        rins.insElem("eftidpaese", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdPaese());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdCodice() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdCodice().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdCodice().equals("")) {
                                        rins.insElem("eftidcodice", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getIdTrasmittente().getIdCodice());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getProgressivoInvio() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getProgressivoInvio().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getProgressivoInvio().equals("")) {
                                        rins.insElem("eftproginvio", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getProgressivoInvio());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getFormatoTrasmissione() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getFormatoTrasmissione().toString().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getFormatoTrasmissione().toString().equals("")) {
                                        rins.insElem("eftformatotrasm", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getFormatoTrasmissione().toString());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getCodiceDestinatario() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getCodiceDestinatario().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getCodiceDestinatario().equals("")) {
                                        rins.insElem("eftcodicedest", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getCodiceDestinatario());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente() != null && fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getTelefono() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getTelefono().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getTelefono().equals("")) {
                                        rins.insElem("efttelefono", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getTelefono());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente() != null && fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getEmail() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getEmail().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getEmail().equals("")) {
                                        rins.insElem("eftemail", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getContattiTrasmittente().getEmail());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getPECDestinatario() != null && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getPECDestinatario().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getPECDestinatario().equals("")) {
                                        rins.insElem("eftpecdestinatario", fatturaElettronica.getFatturaElettronicaHeader().getDatiTrasmissione().getPECDestinatario());
                                    }

                                    if (fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA() != null && fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA().getIdCodice() != null && !fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA().getIdCodice().equals("null") && !fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA().getIdCodice().equals("")) {
                                        rins.insElem("eftazienda", fatturaElettronica.getFatturaElettronicaHeader().getCessionarioCommittente().getDatiAnagrafici().getIdFiscaleIVA().getIdCodice());
                                    }

                                    // inserisco la data del giorno
                                    String oggi = "";
                                    SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd");
                                    oggi = sdfr.format(new Date());
                                    rins.insElem("eftdataric", oggi);
                                    boolean signed = false;
                                    if (nffe.contains(".p7m") || (fatturaElettronica != null && fatturaElettronica.getSignature() != null)) {
                                        signed = true;
                                    }
                                    rins.insElem("eftfirmato", signed ? 1 : 0);

                                    if (fbi.getDatiGenerali().getDatiGeneraliDocumento().getDatiBollo() != null) {
                                        rins.insElem("eftimpbollo", fbi.getDatiGenerali().getDatiGeneraliDocumento().getDatiBollo().getImportoBollo().doubleValue());
                                    }

                                    if (reft == null || inviata || nuovoprogPA) {
                                        QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz.conn, QueryAggiornamentoClient.INS, "eft_archivio" + esContabile, "eftid", rins);
                                        qins.esecuzione();
                                        int eftid = qins.getID();
                                        String piva = rins.leggiStringa("eftpiva");
                                        String codFisc = "";
                                        if (rins.esisteCampo("eftcodfisc")) {
                                            codFisc = rins.leggiStringa("eftcodfisc");
                                        }
                                        rins = new Record();
                                        rins.insElem("eftid", eftid);
                                        rins.insElem("efttipo", (int) 0);
                                        rins.insElem("eftnomeorigfile", nffe);
                                        rins.insElem("eftdata", datafat);
                                        rins.insElem("eftnum", fbi.getDatiGenerali().getDatiGeneraliDocumento().getNumero());
                                        rins.insElem("eftpiva", piva);
                                        rins.insElem("eftcodfisc", codFisc);
                                        InputStream targetStream = new FileInputStream(ff.getAbsolutePath() + File.separator + "tmpDoc" + File.separator + nffe);
                                        rins.insElem("eftxml", signed ? null : targetStream);
                                        rins.insElem("eftxmlsigned", signed ? targetStream : null);
                                        qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "eft_archivioxml" + esContabile, "eftid", rins);
                                        qins.esecuzione();
                                        // Aggiungo i dati pagamento
                                        for (Record pagamento : pagRecord) {
                                            pagamento.insElem("edpeftid", eftid);
                                            QueryAggiornamentoClient qinspag = new QueryAggiornamentoClient(connAz.conn, QueryAggiornamentoClient.INS, "eft_datipagamento" + esContabile, "edpid", pagamento);
                                            qinspag.esecuzione();
                                        }
                                    } else {
                                        if (reft != null && !inviata) {
                                            rins.insElem("eftid", reft.leggiIntero("eftid"));
                                            QueryAggiornamentoClient qagg = new QueryAggiornamentoClient(connAz.conn, QueryAggiornamentoClient.AGG, "eft_archivio" + esContabile, "eftid", rins);
                                            qagg.esecuzione();

                                            String piva = rins.leggiStringa("eftpiva");
                                            String codFisc = "";
                                            if (rins.esisteCampo("eftcodfisc")) {
                                                codFisc = rins.leggiStringa("eftcodfisc");
                                            }
                                            rins = new Record();
                                            rins.insElem("eftid", reft.leggiIntero("eftid"));
                                            rins.insElem("efttipo", (int) 0);
                                            rins.insElem("eftnomeorigfile", nffe);
                                            rins.insElem("eftdata", datafat);
                                            rins.insElem("eftnum", fbi.getDatiGenerali().getDatiGeneraliDocumento().getNumero());
                                            rins.insElem("eftpiva", piva);
                                            rins.insElem("eftcodfisc", codFisc);
                                            InputStream targetStream = new FileInputStream(ff.getAbsolutePath() + File.separator + "tmpDoc" + File.separator + nffe);
                                            rins.insElem("eftxml", signed ? null : targetStream);
                                            rins.insElem("eftxmlsigned", signed ? targetStream : null);
                                            qagg = new QueryAggiornamentoClient(connAz.conn, QueryAggiornamentoClient.AGG, "eft_archivioxml" + esContabile, "eftid", rins);
                                            qagg.esecuzione();
                                            
                                            String qry4 = "SELECT * FROM eft_notifiche"+esContabile+" WHERE notideft=" + reft.leggiIntero("eftid") + " AND notstato=0 AND notarchiviata=0";
                                            QueryInterrogazioneClient qs = new QueryInterrogazioneClient(connAz, qry4);
                                            qs.apertura();
                                            if (qs.numeroRecord() > 0) {
                                                String qryupdate = "UPDATE eft_notifiche"+esContabile+" SET notarchiviata=1 WHERE notideft=" + reft.leggiIntero("eftid");
                                                qagg = new QueryAggiornamentoClient(connAz.conn, qryupdate);
                                                qagg.esecuzione();
                                            }
                                            qs.chiusura();

                                            QueryAggiornamentoClient qdpag = new QueryAggiornamentoClient(connAz.conn, "DELETE FROM eft_datipagamento"+esContabile+" WHERE edpeftid = " + reft.leggiIntero("eftid"));
                                            qdpag.esecuzione();
                                            // Aggiungo i dati pagamento
                                            for (Record pagamento : pagRecord) {
                                                pagamento.insElem("edpeftid", reft.leggiIntero("eftid"));
                                                QueryAggiornamentoClient qinspag = new QueryAggiornamentoClient(connAz.conn, QueryAggiornamentoClient.INS, "eft_datipagamento"+esContabile, "edpid", pagamento);
                                                qinspag.esecuzione();
                                            }
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                        int caumovId = -1;
                        if (tipodoc == 0) {
                            String qryxz = "SELECT * FROM caubs  WHERE bscaucod=\"ftven\"";
                            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qryxz);
                            qRic.apertura();
                            Record rec = qRic.successivo();
                            qRic.chiusura();
                            if (rec != null) {
                                caumovId = rec.leggiIntero("bscauid");
                            }
                        } else {
                            String qryxz = "SELECT * FROM caubs  WHERE bscaucod=\"ncv\"";
                            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qryxz);
                            qRic.apertura();
                            Record rec = qRic.successivo();
                            qRic.chiusura();
                            if (rec != null) {
                                caumovId = rec.leggiIntero("bscauid");
                            }
                        }

                        String qrydoc = "SELECT * FROM movimentidoc" + esContabile + " WHERE movdocid = " + movdocId;
                        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qrydoc);
                        query.apertura();
                        Record rdocumento = query.successivo();
                        query.chiusura();

                        String qrydelete2 = "DELETE FROM cespitiven WHERE cespftjazzid = " + movdocId;
                        QueryAggiornamentoClient queryagg = new QueryAggiornamentoClient(connAz, qrydelete2);
                        queryagg.esecuzione();

                        qrydelete2 = "DELETE FROM movimentibs WHERE mbsmovjazzid = " + movdocId;
                        queryagg = new QueryAggiornamentoClient(connAz, qrydelete2);
                        queryagg.esecuzione();

                        for (int i = 0; i < cespitidaregistrare.size(); i++) {
                            Record rcesp = cespitidaregistrare.get(i);
                            if (rcesp.leggiIntero("cespid") != -1) {
                                if (tipodoc == 0) {
                                    BusinessLogicCespiti.inserisciAlienazione(connAz.conn, esContabile, movdocId, rcesp, rSez.leggiStringa("sufxcod"), datamov.getDataStringa(), "" + rdocumento.leggiIntero("movdocnumft"), nomecli2.getText(),
                                        pivacli.getText(), i, "Rif." + rdocumento.leggiIntero("movdocnumft") + " del " + datamov.getDataStringa(), rcesp.leggiDouble("importo"),
                                        0.00, rcesp.leggiDouble("qta"), 0.00, rcesp.leggiDouble("valoreparziale"),
                                        rcesp.leggiDouble("parzialefondo"), rcesp.leggiDouble("valorestorico"), rcesp.leggiDouble("valorefondo"),
                                        rcesp.leggiDouble("minusvalenza"), rcesp.leggiDouble("plusvalenza"), caumovId,-1);
                                } else {
                                    if(dif==0)
                                    {
                                        BusinessLogicCespiti.inserisciAlienazione(connAz.conn, esContabile, movdocId, rcesp, rSez.leggiStringa("sufxcod"), datamov.getDataStringa(), "" + rdocumento.leggiIntero("movdocnumft"), nomecli2.getText(),
                                            pivacli.getText(), i, "Rif." + rdocumento.leggiIntero("movdocnumft") + " del " + datamov.getDataStringa(), rcesp.leggiDouble("importo") * (-1),
                                            0.00, rcesp.leggiDouble("qta"), 0.00, rcesp.leggiDouble("valoreparziale") * (-1),
                                            rcesp.leggiDouble("parzialefondo") * (-1), rcesp.leggiDouble("valorestorico") * (-1), rcesp.leggiDouble("valorefondo") * (-1),
                                            rcesp.leggiDouble("minusvalenza") * (-1), rcesp.leggiDouble("plusvalenza") * (-1), caumovId,tipologia);
                                    }
                                    else
                                    {
                                         BusinessLogicCespiti.inserisciAlienazione(connAz.conn, esContabile, movdocId, rcesp, rSez.leggiStringa("sufxcod"), datamov.getDataStringa(), "" + rdocumento.leggiIntero("movdocnumft"), nomecli2.getText(),
                                            pivacli.getText(), i, "Rif." + rdocumento.leggiIntero("movdocnumft") + " del " + datamov.getDataStringa(), rcesp.leggiDouble("importo") * (-1),
                                            0.00, rcesp.leggiDouble("qta"), 0.00, rcesp.leggiDouble("valoreparziale") * (-1),
                                            rcesp.leggiDouble("parzialefondo") * (-1), rcesp.leggiDouble("valorestorico") * (-1), rcesp.leggiDouble("valorefondo") * (-1),
                                            rcesp.leggiDouble("minusvalenza") * (-1), dif * (-1), caumovId,tipologia);
                                    }
                                }
                            }
                        }
                        
                        if(clifenodescr.isSelected())
                        {
                            String qryupdate="UPDATE clienti SET clifenodescr=00 WHERE cliid="+cliente.leggiId();
                            QueryAggiornamentoClient queryw= new QueryAggiornamentoClient(connAz,qryupdate);
                            queryw.esecuzione();
                        }
                    }
                }
            }
        }
    }

    public boolean eliminaFileEmesso(String path) throws Exception {
        boolean deleted = false;
        File f = new File(path);
        deleted = f.delete();
        return deleted;
    }

    private void buttonModNumActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonModNumActionPerformed
    {//GEN-HEADEREND:event_buttonModNumActionPerformed
        String qry = "SELECT * FROM documenti LEFT JOIN sezionalinum ON documenti.docsuffnumid = sezionalinum.sufxid "
                + " WHERE docid = " + rDoc.leggiIntero("docid");
        QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qry);
        qd.apertura();
        Record rDoc = qd.successivo();
        qd.chiusura();

        DialogCambioNumDocPop d = new DialogCambioNumDocPop(formGen, connAz, azienda, esmov,
                movdocId, rDoc.leggiIntero("docid"), Integer.parseInt(numero.getText()),
                new Data(datamov.getText().trim(), Data.GG_MM_AAAA), rDoc);
        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
        d.show();

        if (d.leggiScelta() == DialogCambioNumDocPop.CONFERMA) {
            datamov.setText(d.leggiData().formatta(Data.GG_MM_AAAA, "/"));
            numero.setText("" + d.leggiNumero());
            // aggiusta sezionale
            int ris = BusinessLogicContabilita.aggiustaSezionale(connAz.conn, esmov, rDoc.leggiStringa("sufxcod"),
                    datamov.getText().substring(6, 10), rDoc.leggiIntero("tdtnum"));
            // aggiorna data lotti
            String qryupdate = "UPDATE lotti SET lotdata = \"" + d.leggiData().formatta(Data.AAAA_MM_GG, "-") + "\" "
                    + "WHERE lotescont = \"" + esmov + "\" AND lotmovdocid = " + movdocId;
            QueryAggiornamentoClient qagg = new QueryAggiornamentoClient(connAz, qryupdate);
            qagg.esecuzione();
            chiediconferma = false;
            buttonSalva.doClick();
        }
    }//GEN-LAST:event_buttonModNumActionPerformed


    private void buttonInsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonInsActionPerformed
    {//GEN-HEADEREND:event_buttonInsActionPerformed
        if (datamov.getData() == null) {
            JOptionPane.showMessageDialog(this, "Indicare la data del documento", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            datamov.requestFocus();
            return;
        }

        if (cliente.leggiId() == -1) {
            JOptionPane.showMessageDialog(this, "Indicare il cliente", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CLIENTE);
            cliente.fuoco();
            return;
        }

        if (qta.getValore() > 0 && prezzo.getValore() > 0.00 && imponibilenoiva.getValore() > 0.00) {
            JOptionPane.showMessageDialog(this, "Non si può inserire più di una quantità e la gestione del costo splittato con iva e non iva", "Errore",
                    JOptionPane.ERROR_MESSAGE,
                    new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (listcesp == null) {
            if (descrart.getText().trim().equals("") && qta.getValore() > 0) {
                JOptionPane.showMessageDialog(this, "Indicare il cespite oppure la descrizione per il non codificato", "Errore",
                        JOptionPane.ERROR_MESSAGE,
                        new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            }
        }

        if (descrart.getText().trim().equals("") && listcesp.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Indicare la descrizione", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (qta.getValore() == 0 && (imponibilealiq.getValore() > 0.00 || imponibilenoiva.getValore() > 0.00)) {
            qta.setText("1");
        }

//        if (listcesp != null) {
//            if (codiva.leggiId() == -1 && codiva1.leggiId() == -1) {
//                JOptionPane.showMessageDialog(this, "Indicare il codice IVA", "Errore",
//                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
//                codiva.fuoco();
//            }
//
//            if (imponibilenoiva.getValore() == 0.00 && imponibilealiq.getValore() == 0.00) {
//                JOptionPane.showMessageDialog(this, "Indicare perlomeno un imponibile", "Errore",
//                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
//                return;
//            }
//        }

        if ((imponibilealiq.getValore() > 0.00 && imponibilenoiva.getValore() == 0.00) && prezzo.getValore() == 0.00) {
            String valore = "" + imponibilealiq.getValore();
            prezzo.setText(valore.replace(".", ","));
        }

        if ((imponibilenoiva.getValore() > 0.00 && imponibilealiq.getValore() == 0.00) && prezzo.getValore() == 0.00) {
            String valore = "" + imponibilenoiva.getValore();
            prezzo.setText(valore.replace(".", ","));
        }

//        if(imponibilealiq.getValore() > 0 && imponibilenoiva.getValore() > 0)
//        {
//            double totale=imponibilealiq.getValore()+imponibilenoiva.getValore();
//            String valore="" + totale;            
//            prezzo.setText(valore.replace(".",","));
//        }
        if (imponibilealiq.getValore() > 0.00 && codiva.leggiId() == -1) {
            JOptionPane.showMessageDialog(this, "Indicare codice iva", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (imponibilenoiva.getValore() > 0.00 && codiva1.leggiId() == -1) {
            JOptionPane.showMessageDialog(this, "Indicare non soggettibilita' iva", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (listcesp != null) {
            if (listcesp.size() == 1) {
                Record rcesp = listcesp.get(0);
                Record cespite = listcesp.get(0);
                cespite.insElem("importo", Formattazione.estraiDouble(importo.getText()));
                String descrriga = "";
                if (descrart.getText().trim().equals("")) {
                    descrriga = rcesp.leggiStringa("cespdescriz1");
                } else {
                    if (descrart.getText().trim().length() > 255) {
                        descrriga = descrart.getText();
                        descrriga = descrriga.substring(0, 255);
                    } else {
                        descrriga = descrart.getText();
                    }
                }

                Record rx = new Record();
                if(rcesp.leggiIntero("cespcapoid")!=-1)
                {
                    rx.insElem("tiporiga", (int) 2);                    
                }
                else
                {
                    rx.insElem("tiporiga", (int) 1);
                }
                rx.insElem("artid", -1);
                rx.insElem("artcod", "");
                rx.insElem("artdescr", descrriga);
//                rx.insElem("artum", um.getText().trim());
                int ivaId = -1;

                if (imponibilealiq.getValore() > 0) {
                    ivaId = codiva.leggiId();
                } else {
                    ivaId = codiva1.leggiId();
                }

                if (rcli.leggiIntero("clientepubblico") == 1) {
                    // ente pubblico con split payment, iva a carico del committente   
                    Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                    if (ri.leggiIntero("ivagruppo") == 0 && ri.leggiDouble("ivaaliq") > 0 && ri.leggiIntero("ivacess") == 0) {
                        String qry = "SELECT * FROM codiva WHERE ivaaliq = " + Formattazione.formatta(ri.leggiDouble("ivaaliq"), "##0.00", 0).replaceAll(",", ".") + " AND ivacess = 1 AND ivagest = 0";
                        QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qry);
//                        System.out.println(qisp.query());
                        qisp.apertura();
                        Record risp = qisp.successivo();
                        qisp.chiusura();

                        if (risp != null) {
                            ivaId = risp.leggiIntero("ivaid");
                        }
                    }
                }
                rx.insElem("ivaid", ivaId);
                Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                rx.insElem("colli", 1);

                double prz = prezzo.getValore();
                if (prz == 0) {
                    rx.insElem("qta", (double) 1);
                    cespite.insElem("qta", (double) 1);
                    if (imponibilealiq.getValore() > 0) {
                        prz = imponibilealiq.getValore();
                    } else {
                        prz = imponibilenoiva.getValore();
                    }
                    rx.insElem("prezzo", prz);
                } else {
                    qta.insValuta(rx, "qta");
                    qta.insValuta(cespite, "qta");
                    rx.insElem("prezzo", prz);
                }
                sc1.insValuta(rx, "scart1");

                rx.insElem("sccli1", (double) 0);
                rx.insElem("sccli2", (double) 0);
                rx.insElem("sccli3", (double) 0);

                rx.insElem("esclsctestata", (double) 1);
                rx.insElem("lordo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(qta.getText()) * Formattazione.estraiDouble(prezzo.getText()), 2));
                if (imponibilealiq.getValore() > 0) {
                    rx.insElem("importo", imponibilealiq.getValore());
                } else {
                    rx.insElem("importo", imponibilenoiva.getValore());
                }

                rx.insElem("lotto", rcesp.leggiStringa("cespordine"));
                rx.insElem("oma", (int) 0);
                rx.insElem("serviziodatai", "0000-00-00");
                rx.insElem("serviziodataf", "0000-00-00");
                Record r2x = null;
                if (rigacorr == -1) {
//                    vrighe.add(rx);
                    if (imponibilealiq.getValore() > 0 && imponibilenoiva.getValore() > 0) {
                        r2x = new Record();
                        r2x.insElem("tiporiga", (int) 1);
                        r2x.insElem("artid", -1);
                        r2x.insElem("artcod", "");
                        r2x.insElem("artdescr", "/'/'");
                        rx.insElem("artum", "");

                        ivaId = codiva1.leggiId();

                        if (rcli.leggiIntero("clientepubblico") == 1) {
                            // ente pubblico con split payment, iva a carico del committente   
                            ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                            if (ri.leggiIntero("ivagruppo") == 0 && ri.leggiDouble("ivaaliq") > 0 && ri.leggiIntero("ivacess") == 0) {
                                String qryiva = "SELECT * FROM codiva WHERE ivaaliq = " + Formattazione.formatta(ri.leggiDouble("ivaaliq"), "##0.00", 0).replaceAll(",", ".") + " AND ivacess = 1 AND ivagest = 0";
                                QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qryiva);
//                                System.out.println(qisp.query());
                                qisp.apertura();
                                Record risp = qisp.successivo();
                                qisp.chiusura();

                                if (risp != null) {
                                    ivaId = risp.leggiIntero("ivaid");
                                }
                            }
                        }
                        r2x.insElem("ivaid", ivaId);
                        ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                        r2x.insElem("ivacod", ri.leggiStringa("ivacod"));
                        r2x.insElem("colli", 1);

                        prz = 0.00;
                        if (prz == 0) {
                            r2x.insElem("qta", (double) 1);
                            prz = imponibilenoiva.getValore();
                            r2x.insElem("prezzo", prz);
                        } else {

                            qta.insValuta(r2x, "qta");
                            r2x.insElem("prezzo", prz);
                        }
                        sc1.insValuta(r2x, "scart1");

                        r2x.insElem("sccli1", (double) 0);
                        r2x.insElem("sccli2", (double) 0);
                        r2x.insElem("sccli3", (double) 0);

                        r2x.insElem("esclsctestata", (double) 1);
                        r2x.insElem("lordo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(qta.getText()) * Formattazione.estraiDouble(prezzo.getText()), 2));

                        r2x.insElem("importo", imponibilenoiva.getValore());

                        r2x.insElem("lotto", rcesp.leggiStringa("cespordine"));
                        r2x.insElem("oma", (int) 0);
                        r2x.insElem("serviziodatai", "0000-00-00");
                        r2x.insElem("serviziodataf", "0000-00-00");
                    }
                    vrighe.add(rx);
                    cespite.insElem("valoreparziale", valoreparziale.getValore());
                    cespite.insElem("parzialefondo", Formattazione.estraiDouble(parzialefondo.getText()));
                    cespite.insElem("valorestorico", Formattazione.estraiDouble(valorestorico.getText()));
                    cespite.insElem("valorefondo", Formattazione.estraiDouble(valorefondo.getText()));
                    cespite.insElem("minusvalenza", Formattazione.estraiDouble(minusvalenza.getText()));
                    cespite.insElem("plusvalenza", Formattazione.estraiDouble(plusvalenza.getText()));

                    String qrycespite = "SELECT catbspdcminusid,catbspdcplusid,catbspdcplusid,catbsfondopdcid,catbsimmpdcid FROM cespiti LEFT JOIN categoriebs ON cespcategid=catbsid WHERE cespid=" + rcesp.leggiIntero("cespid");
                    QueryInterrogazioneClient querycespiti = new QueryInterrogazioneClient(connAz, qrycespite);
                    querycespiti.apertura();
                    Record rcespitesalvato = querycespiti.successivo();
                    querycespiti.chiusura();

                    String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.1";
                    QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
                    query.apertura();
                    Record r = query.successivo();
                    query.chiusura();
                    if (r != null) {
                        cespite.insElem("plusvalenzapdc", r.leggiIntero("pdcceejazzid"));
                    } else {
                        cespite.insElem("plusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcplusid"));
                    }

                    qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.3";
                    query = new QueryInterrogazioneClient(connAz, qry);
                    query.apertura();
                    r = query.successivo();
                    query.chiusura();
                    if (r != null) {
                        cespite.insElem("minusvalenzapdc", r.leggiIntero("pdcceejazzid"));
                    } else {
                        cespite.insElem("minusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcminusid"));
                    }

                    cespite.insElem("immobilizzazionepdc", rcespitesalvato.leggiIntero("catbsimmpdcid"));
                    cespite.insElem("fondopdc", rcespitesalvato.leggiIntero("catbsfondopdcid"));

                    cespitidaregistrare.add(cespite);
                    if (r2x != null) {

                        vrighe.add(r2x);
                    }

                    mostraCarrello();
                    ricalcolaTotale();
                    preparaInsRiga();
                } else {
                    Record rold = vrighe.get(rigacorr);
                    if (rold.esisteCampo("rigaorigid")) {
                        rx.insElem("rigaorigid", rold.leggiIntero("rigaorigid"));
                        rx.insElem("rigaorigescont", rold.leggiStringa("rigaorigescont"));
                    }
                    
                    String qrycespite = "SELECT catbspdcminusid,catbspdcplusid,catbspdcplusid,catbsfondopdcid,catbsimmpdcid FROM cespiti LEFT JOIN categoriebs ON cespcategid=catbsid WHERE cespid=" + rcesp.leggiIntero("cespid");
                    QueryInterrogazioneClient querycespiti = new QueryInterrogazioneClient(connAz, qrycespite);
                    querycespiti.apertura();
                    Record rcespitesalvato = querycespiti.successivo();
                    querycespiti.chiusura();                    
                   
                    if(tipodoc==0)
                    {
                        if(rold.leggiIntero("tiporiga")==2)
                        {                            
                            int ggprec=Formattazione.estraiIntero(datamov.getDataStringa().substring(8,10))-1;
                            String data=datamov.getDataStringa().substring(0,8)+ggprec;
                            double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rcesp.leggiIntero("cespid"), esContabile, data);
                            cespite.insElem("valoreparziale", vv[1]);
                            cespite.insElem("parzialefondo", vv[3]);
                            cespite.insElem("valorestorico", vv[1]);
                            cespite.insElem("valorefondo", vv[3]);
                        }
                        else
                        {
                            int ggprec=Formattazione.estraiIntero(datamov.getDataStringa().substring(8,10))-1;
                            String data=datamov.getDataStringa().substring(0,8)+ggprec;
                            double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rcesp.leggiIntero("cespid"), esContabile, data);
                            cespite.insElem("valoreparziale", vv[1]);
                            cespite.insElem("parzialefondo", vv[3]);
                            cespite.insElem("valorestorico", vv[1]);
                            cespite.insElem("valorefondo", vv[3]);
                            double plusminus = imponibilealiq.getValore() - (Formattazione.estraiDouble(residuoparziale.getText()));
                            if (plusminus > 0)
                            {
                                cespite.insElem("plusvalenza", plusminus);
                                cespite.insElem("minusvalenza", 0.00);                                
                            } else 
                            {
                                cespite.insElem("plusvalenza", 0.00);
                                cespite.insElem("minusvalenza", -plusminus);                                
                            }
                        }
                    }
                    else if(tipodoc==4)
                    {                        
                        if(tipologia==1 || tipologia==2)
                        {
                            cespite.insElem("valoreparziale", valoreparziale.getValore());
                            cespite.insElem("parzialefondo", Formattazione.estraiDouble(parzialefondo.getText()));
                            cespite.insElem("valorestorico", Formattazione.estraiDouble(valorestorico.getText()));
                            cespite.insElem("valorefondo", Formattazione.estraiDouble(valorefondo.getText()));
                        }
                        

                        double plusminus = imponibilealiq.getValore() - (Formattazione.estraiDouble(residuoparziale.getText()));
                        if (plusminus > 0) {
                            if(tipologia==2)
                            {
                                cespite.insElem("plusvalenza", 0.00);
                                cespite.insElem("minusvalenza", 0.00);
                            }
                            else
                            {
                                cespite.insElem("plusvalenza", plusminus);
                                cespite.insElem("minusvalenza", 0.00);
                            }
                        } else {
                            if(tipologia==2)
                            {
                                cespite.insElem("plusvalenza", 0.00);
                                cespite.insElem("minusvalenza", 0.00);
                            }
                            else
                            {
                                cespite.insElem("plusvalenza", 0.00);
                                cespite.insElem("minusvalenza", -plusminus);
                            }
                        }
                    }
                    
                    cespite.insElem("immobilizzazionepdc", rcespitesalvato.leggiIntero("catbsimmpdcid"));
                    cespite.insElem("fondopdc", rcespitesalvato.leggiIntero("catbsfondopdcid"));
                    cespite.insElem("plusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcplusid"));
                    cespite.insElem("minusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcminusid"));
                    
                    vrighe.set(rigacorr, rx);
                    cespitidaregistrare.set(rigacorr, cespite);
                    mostraCarrello();
                    ricalcolaTotale();
                    preparaInsRiga();
                }
            } else {
                Record rold = vrighe.get(rigacorr);
                double importoven = 0.00;
                if (imponibilealiq.getValore() == 0) {
                    importoven = imponibilenoiva.getValore();
                } else {                    
                    importoven = imponibilealiq.getValore();
                }

                if(!rold.leggiStringa("lotto").equals(""))
                {
                    ArrayList<Record> figli = new ArrayList<Record>();
                    for (int j = 0; j < listcesp.size(); j++) {
                        Record rcesp = listcesp.get(j);
                        Record cespite = listcesp.get(j);
                        String descrriga = "";
                        if (descrart.getText().trim().equals("")) {
                            descrriga = rcesp.leggiStringa("cespdescriz1");
                        } else {
                            if (descrart.getText().trim().length() > 255) {
                                descrriga = descrart.getText();
                                descrriga = descrriga.substring(0, 255);
                            } else {
                                descrriga = descrart.getText();
                            }
                        }

                        Record rx = new Record();
                        rx.insElem("tiporiga", (int) 1);
                        rx.insElem("artid", -1);
                        rx.insElem("artcod", "");

                        rx.insElem("artdescr", descrriga);
                        //                rx.insElem("artum", um.getText().trim());
                        int ivaId = -1;

                        if (imponibilealiq.getValore() > 0) {
                            ivaId = codiva.leggiId();
                        } else {
                            ivaId = codiva1.leggiId();
                        }

                        if (rcli.leggiIntero("clientepubblico") == 1) {
                            // ente pubblico con split payment, iva a carico del committente   
                            Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                            if (ri.leggiIntero("ivagruppo") == 0 && ri.leggiDouble("ivaaliq") > 0 && ri.leggiIntero("ivacess") == 0) {
                                String qry = "SELECT * FROM codiva WHERE ivaaliq = " + Formattazione.formatta(ri.leggiDouble("ivaaliq"), "##0.00", 0).replaceAll(",", ".") + " AND ivacess = 1 AND ivagest = 0";
                                QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qry);
    //                            System.out.println(qisp.query());
                                qisp.apertura();
                                Record risp = qisp.successivo();
                                qisp.chiusura();

                                if (risp != null) {
                                    ivaId = risp.leggiIntero("ivaid");
                                }
                            }
                        }
                        rx.insElem("ivaid", ivaId);
                        Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                        rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                        rx.insElem("colli", 1);

                        double prz = prezzo.getValore();
                        if (prz == 0) {
                            rx.insElem("qta", (double) 1);
                            cespite.insElem("qta", (double) 1);
                            if (imponibilealiq.getValore() > 0) {
                                prz = imponibilealiq.getValore();
                            } else {
                                prz = imponibilenoiva.getValore();
                            }
                            rx.insElem("prezzo", prz);
                        } else {
                            qta.insValuta(rx, "qta");
                            qta.insValuta(cespite, "qta");
                            rx.insElem("prezzo", prz);
                        }
                        sc1.insValuta(rx, "scart1");

                        rx.insElem("sccli1", (double) 0);
                        rx.insElem("sccli2", (double) 0);
                        rx.insElem("sccli3", (double) 0);

                        rx.insElem("esclsctestata", (double) 1);
                        rx.insElem("lordo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(qta.getText()) * Formattazione.estraiDouble(prezzo.getText()), 2));

                        rx.insElem("lotto", rcesp.leggiStringa("cespordine"));
                        rx.insElem("oma", (int) 0);
                        rx.insElem("serviziodatai", "0000-00-00");
                        rx.insElem("serviziodataf", "0000-00-00");
                        Record r2x = null;
                        if (rigacorr == -1) 
                        {
                            if (imponibilealiq.getValore() > 0 && imponibilenoiva.getValore() > 0 && j == (listcesp.size() - 1)) 
                            {
                                r2x = new Record();
                                r2x.insElem("tiporiga", (int) 1);
                                r2x.insElem("artid", -1);
                                r2x.insElem("artcod", "");
                                r2x.insElem("artdescr", rcesp.leggiStringa("cespdescriz1"));
                                //                        rx.insElem("artum", um.getText().trim());

                                ivaId = codiva1.leggiId();

                                if (rcli.leggiIntero("clientepubblico") == 1) {
                                    // ente pubblico con split payment, iva a carico del committente   
                                    ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                                    if (ri.leggiIntero("ivagruppo") == 0 && ri.leggiDouble("ivaaliq") > 0 && ri.leggiIntero("ivacess") == 0) {
                                        String qryiva = "SELECT * FROM codiva WHERE ivaaliq = " + Formattazione.formatta(ri.leggiDouble("ivaaliq"), "##0.00", 0).replaceAll(",", ".") + " AND ivacess = 1 AND ivagest = 0";
                                        QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qryiva);
    //                                    System.out.println(qisp.query());
                                        qisp.apertura();
                                        Record risp = qisp.successivo();
                                        qisp.chiusura();

                                        if (risp != null) {
                                            ivaId = risp.leggiIntero("ivaid");
                                        }
                                    }
                                }
                                r2x.insElem("ivaid", ivaId);
                                ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                                r2x.insElem("ivacod", ri.leggiStringa("ivacod"));
                                //                        r2x.insElem("artdescr", "Imponibile non soggetto ad iva: " + ri.leggiStringa("ivadescr"));
                                r2x.insElem("colli", 1);

                                prz = 0.00;
                                if (prz == 0) {
                                    r2x.insElem("qta", (double) 1);
                                    prz = imponibilenoiva.getValore();
                                    r2x.insElem("prezzo", prz);
                                } else {
                                    qta.insValuta(r2x, "qta");
                                    r2x.insElem("prezzo", prz);
                                }
                                sc1.insValuta(r2x, "scart1");

                                r2x.insElem("sccli1", (double) 0);
                                r2x.insElem("sccli2", (double) 0);
                                r2x.insElem("sccli3", (double) 0);

                                r2x.insElem("esclsctestata", (double) 1);
                                r2x.insElem("lordo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(qta.getText()) * Formattazione.estraiDouble(prezzo.getText()), 2));

                                r2x.insElem("importo", imponibilealiq.getValore());

                                r2x.insElem("lotto", rcesp.leggiStringa("cespordine"));
                                r2x.insElem("oma", (int) 0);
                                r2x.insElem("serviziodatai", "0000-00-00");
                                r2x.insElem("serviziodataf", "0000-00-00");

                                double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rcesp.leggiIntero("cespid"), esContabile, datamov.getDataStringa());
                                cespite.insElem("valoreparziale", vv[1]);
                                cespite.insElem("parzialefondo", vv[3]);
                                cespite.insElem("valorestorico", vv[1]);
                                cespite.insElem("valorefondo", vv[3]);

                                vrighe.add(rx);
                            } else if (j < (listcesp.size() - 1) && rcesp.leggiIntero("cespcapoid")!=-1) 
                            {
                                r2x = new Record();
                                Record rcespfiglio=rx;
                                r2x.insElem("tiporiga", (int) 2);
                                r2x.insElem("artid", -1);
                                r2x.insElem("artcod", "");
                                r2x.insElem("artdescr", descrriga);
                                //                        rx.insElem("artum", um.getText().trim());

                                String qryiva = "SELECT * FROM codiva WHERE ivacod =\"N2\" AND ivacess = 1 AND ivagest = 0";
                                QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qryiva);
    //                            System.out.println(qisp.query());
                                qisp.apertura();
                                Record risp = qisp.successivo();
                                qisp.chiusura();

                                if (risp != null) {
                                    ivaId = risp.leggiIntero("ivaid");
                                }

                                r2x.insElem("ivaid", ivaId);
                                ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                                r2x.insElem("ivacod", ri.leggiStringa("ivacod"));
                                //                        r2x.insElem("artdescr", "Imponibile non soggetto ad iva: " + ri.leggiStringa("ivadescr"));
                                r2x.insElem("colli", 0);
                                r2x.insElem("qta", (double) 0);
                                r2x.insElem("prezzo", (double) 0);
                                r2x.insElem("scart1", (double) 0);
                                r2x.insElem("scart2", (double) 0);
                                r2x.insElem("scart3", (double) 0);
                                r2x.insElem("sccli1", (double) 0);
                                r2x.insElem("sccli2", (double) 0);
                                r2x.insElem("sccli3", (double) 0);
                                r2x.insElem("esclsctestata", (double) 0);
                                r2x.insElem("lordo", (double) 0);
                                r2x.insElem("importo", (double) 0);
                                r2x.insElem("lotto", rcesp.leggiStringa("cespordine"));
                                r2x.insElem("oma", (int) 0);
                                r2x.insElem("serviziodatai", "0000-00-00");
                                figli.add(r2x);

                                vrighe.add(r2x);

                                double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rcesp.leggiIntero("cespid"), esContabile, datamov.getDataStringa());
                                cespite.insElem("valoreparziale", vv[1]);
                                cespite.insElem("parzialefondo", vv[3]);
                                cespite.insElem("valorestorico", vv[1]);
                                cespite.insElem("valorefondo", vv[3]);

    //                                importoven -= vv[1] - vv[5];
                                cespite.insElem("importo", importoven);
                                cespite.insElem("minusvalenza", 0.00);
                                cespite.insElem("plusvalenza", 0.00);

                                String qrycespite = "SELECT catbspdcminusid,catbspdcplusid,catbspdcplusid,catbsfondopdcid,catbsimmpdcid FROM cespiti LEFT JOIN categoriebs ON cespcategid=catbsid WHERE cespid=" + rcesp.leggiIntero("cespid");
                                QueryInterrogazioneClient querycespiti = new QueryInterrogazioneClient(connAz, qrycespite);
                                querycespiti.apertura();
                                Record rcespitesalvato = querycespiti.successivo();
                                querycespiti.chiusura();

                                String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.1";
                                QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
                                query.apertura();
                                Record r = query.successivo();
                                query.chiusura();
                                if (r != null) {
                                    cespite.insElem("plusvalenzapdc", r.leggiIntero("pdcceejazzid"));
                                } else {
                                    cespite.insElem("plusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcplusid"));
                                }

                                qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.3";
                                query = new QueryInterrogazioneClient(connAz, qry);
                                query.apertura();
                                r = query.successivo();
                                query.chiusura();
                                if (r != null) {
                                    cespite.insElem("minusvalenzapdc", r.leggiIntero("pdcceejazzid"));
                                } else {
                                    cespite.insElem("minusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcminusid"));
                                }

                                cespite.insElem("immobilizzazionepdc", rcespitesalvato.leggiIntero("catbsimmpdcid"));
                                cespite.insElem("fondopdc", rcespitesalvato.leggiIntero("catbsfondopdcid"));

                                cespitidaregistrare.add(cespite);
                            } else 
                            {
                                double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rcesp.leggiIntero("cespid"), esContabile, datamov.getDataStringa());
                                cespite.insElem("valoreparziale", vv[1]);
                                cespite.insElem("parzialefondo", vv[3]);
                                cespite.insElem("valorestorico", vv[1]);
                                cespite.insElem("valorefondo", vv[3]);
                                cespite.insElem("importo", importoven);
                                rx.insElem("importo", imponibilealiq.getValore());

                                double valfondo =vv[3];
                                double valore = vv[1];
                                double valplus = 0.00;
                                if (figli.size() > 0) {
                                    for (int i = 0; i < figli.size(); i++) {
                                        String qry="SELECT cespid FROM cespiti WHERE cespordine=\""+figli.get(i).leggiStringa("lotto")+"\"";
                                        QueryInterrogazioneClient query= new QueryInterrogazioneClient(connAz,qry);
                                        query.apertura();
                                        Record rfiglio=query.successivo();
                                        query.chiusura();

                                        double vvfiglio[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rfiglio.leggiIntero("cespid"), esContabile, datamov.getDataStringa());
                                        valore += vvfiglio[1];
                                        valfondo += vvfiglio[3];
                                    }
                                    double residuo = valore - valfondo;
                                    valplus = importoven - residuo;
                                    if (valplus > 0) {
                                        cespite.insElem("plusvalenza", valplus);
                                        cespite.insElem("minusvalenza", 0.00);
                                    } else {
                                        cespite.insElem("plusvalenza", 0.00);
                                        cespite.insElem("minusvalenza", -valplus);
                                    }
                                } else {
                                    double plusminus = importoven - (vv[1] - vv[5]);
                                    if (plusminus > 0) {
                                        cespite.insElem("plusvalenza", plusminus);
                                        cespite.insElem("minusvalenza", 0.00);
                                    } else {
                                        cespite.insElem("plusvalenza", 0.00);
                                        cespite.insElem("minusvalenza", -plusminus);
                                    }
                                }
                                vrighe.add(rx);
                            }

                            if (rcesp.leggiIntero("cespcapoid")==-1) 
                            {                           
                                String qrycespite = "SELECT catbspdcminusid,catbspdcplusid,catbspdcplusid,catbsfondopdcid,catbsimmpdcid FROM cespiti LEFT JOIN categoriebs ON cespcategid=catbsid WHERE cespid=" + rcesp.leggiIntero("cespid");
                                QueryInterrogazioneClient querycespiti = new QueryInterrogazioneClient(connAz, qrycespite);
                                querycespiti.apertura();
                                Record rcespitesalvato = querycespiti.successivo();
                                querycespiti.chiusura();

                                String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.1";
                                QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
                                query.apertura();
                                Record r = query.successivo();
                                query.chiusura();
                                if (r != null) {
                                    cespite.insElem("plusvalenzapdc", r.leggiIntero("pdcceejazzid"));
                                } else {
                                    cespite.insElem("plusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcplusid"));
                                }

                                qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.3";
                                query = new QueryInterrogazioneClient(connAz, qry);
                                query.apertura();
                                r = query.successivo();
                                query.chiusura();
                                if (r != null) {
                                    cespite.insElem("minusvalenzapdc", r.leggiIntero("pdcceejazzid"));
                                } else {
                                    cespite.insElem("minusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcminusid"));
                                }

                                cespite.insElem("immobilizzazionepdc", rcespitesalvato.leggiIntero("catbsimmpdcid"));
                                cespite.insElem("fondopdc", rcespitesalvato.leggiIntero("catbsfondopdcid"));

                                cespitidaregistrare.add(cespite);
                            }
                        }                        
                        mostraCarrello();
                        ricalcolaTotale();
                    } 
                }
                else
                {
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 1);
                    rx.insElem("artid", (int) -1);
                    rx.insElem("artcod", "");
                    String descrriga = "";
                    if (descrart.getText().trim().length() > 255) {
                        descrriga = descrart.getText();
                        descrriga = descrriga.substring(0, 255);
                    } else {
                        descrriga = descrart.getText();
                    }
                    rx.insElem("artdescr", descrriga);
        //            rx.insElem("artum", um.getText().trim());
                    int ivaId = codiva.leggiId();
                    if (rcli.leggiIntero("clientepubblico") == 1) {
                        // ente pubblico con split payment, iva a carico del committente

                        Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", codiva.leggiId(), "");
                        if (ri.leggiIntero("ivagruppo") == 0 && ri.leggiDouble("ivaaliq") > 0 && ri.leggiIntero("ivacess") == 0) {
                            String qryiva = "SELECT * FROM codiva WHERE ivaaliq = " + Formattazione.formatta(ri.leggiDouble("ivaaliq"), "##0.00", 0).replaceAll(",", ".") + " AND ivacess = 1 AND ivagest = 0";
                            QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qryiva);
        //                    System.out.println(qisp.query());
                            qisp.apertura();
                            Record risp = qisp.successivo();
                            qisp.chiusura();

                            if (risp != null) {
                                ivaId = risp.leggiIntero("ivaid");
                            }
                        }
                    }
                    rx.insElem("ivaid", ivaId);

                    Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
                    rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                    rx.insElem("colli", 1);
                    qta.insValuta(rx, "qta");
                    double prz = prezzo.getValore();

                    rx.insElem("prezzo", prz);
                    sc1.insValuta(rx, "scart1");
                    rx.insElem("sccli1", sclicalc1);
                    rx.insElem("sccli2", sclicalc2);
                    rx.insElem("sccli3", sclicalc3);
                    rx.insElem("esclsctestata", (double) 0);
                    rx.insElem("lordo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(qta.getText()) * Formattazione.estraiDouble(prezzo.getText()), 2));
                    rx.insElem("importo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(importo.getText()), 2));

                    rx.insElem("oma", (int) 0);
                    rx.insElem("serviziodatai", "0000-00-00");
                    rx.insElem("serviziodataf", "0000-00-00");
                    rx.insElem("lotto","");
                    if (rigacorr == -1) {
                        vrighe.add(rx);
                        mostraCarrello();
                        ricalcolaTotale();
                        preparaInsRiga();
                    } else {
                        vrighe.set(rigacorr, rx);
                        mostraCarrello();
                        ricalcolaTotale();
                        preparaInsRiga();
                    }

                    mostraCarrello();
                    ricalcolaTotale();
                }

                preparaInsRiga();
            }
        } else if (qta.getValore() > 0) {
            if (imponibilealiq.getValore() > 0 && imponibilenoiva.getValore() > 0) {
                JOptionPane.showMessageDialog(this, "Sdoppiamento dell'importo previsto solo per immobilizzazioni",
                        "", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                return;
            }

            Record rx = new Record();
            rx.insElem("tiporiga", (int) 1);
            rx.insElem("artid", (int) -1);
            rx.insElem("artcod", "");
            String descrriga = "";
            if (descrart.getText().trim().length() > 255) {
                descrriga = descrart.getText();
                descrriga = descrriga.substring(0, 255);
            } else {
                descrriga = descrart.getText();
            }
            rx.insElem("artdescr", descrriga);
//            rx.insElem("artum", um.getText().trim());
            int ivaId = codiva.leggiId();
            if (rcli.leggiIntero("clientepubblico") == 1) {
                // ente pubblico con split payment, iva a carico del committente

                Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", codiva.leggiId(), "");
                if (ri.leggiIntero("ivagruppo") == 0 && ri.leggiDouble("ivaaliq") > 0 && ri.leggiIntero("ivacess") == 0) {
                    String qryiva = "SELECT * FROM codiva WHERE ivaaliq = " + Formattazione.formatta(ri.leggiDouble("ivaaliq"), "##0.00", 0).replaceAll(",", ".") + " AND ivacess = 1 AND ivagest = 0";
                    QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qryiva);
//                    System.out.println(qisp.query());
                    qisp.apertura();
                    Record risp = qisp.successivo();
                    qisp.chiusura();

                    if (risp != null) {
                        ivaId = risp.leggiIntero("ivaid");
                    }
                }
            }
            rx.insElem("ivaid", ivaId);

            Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", ivaId, "");
            rx.insElem("ivacod", ri.leggiStringa("ivacod"));
            rx.insElem("colli", 1);
            qta.insValuta(rx, "qta");
            double prz = prezzo.getValore();

            rx.insElem("prezzo", prz);
            sc1.insValuta(rx, "scart1");
            rx.insElem("sccli1", sclicalc1);
            rx.insElem("sccli2", sclicalc2);
            rx.insElem("sccli3", sclicalc3);
            rx.insElem("esclsctestata", (double) 0);
            rx.insElem("lordo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(qta.getText()) * Formattazione.estraiDouble(prezzo.getText()), 2));
            rx.insElem("importo", OpValute.arrotondaMat((1.0) * Formattazione.estraiDouble(importo.getText()), 2));

            rx.insElem("oma", (int) 0);
            rx.insElem("serviziodatai", "0000-00-00");
            rx.insElem("serviziodataf", "0000-00-00");
            rx.insElem("lotto","");
            if (rigacorr == -1) {
                vrighe.add(rx);
                mostraCarrello();
                ricalcolaTotale();
                preparaInsRiga();
            } else {
                vrighe.set(rigacorr, rx);
                mostraCarrello();
                ricalcolaTotale();
                preparaInsRiga();
            }

            Record rcesp = new Record();
            rcesp.insElem("cespid", -1);
            cespitidaregistrare.add(rcesp);
        } else {
            Record rx = new Record();
            rx.insElem("tiporiga", (int) 2);
            rx.insElem("artid", (int) -1);
            rx.insElem("artcod", "");

            String descrriga = "";
            if (descrart.getText().trim().length() > 255) {
                descrriga = descrart.getText();
                descrriga = descrriga.substring(0, 255);
            } else {
                descrriga = descrart.getText();
            }
            
            rx.insElem("artdescr", descrriga);
//            rx.insElem("artum", "");
            rx.insElem("ivaid", (int) -1);
            rx.insElem("ivacod", "");
            rx.insElem("colli", (int) 0);
            rx.insElem("qta", (double) 0);
            rx.insElem("prezzo", (double) 0);
            rx.insElem("scart1", (double) 0);
//            rx.insElem("scart2", (double) 0);
//            rx.insElem("scart3", (double) 0);
            rx.insElem("sccli1", (double) 0);
            rx.insElem("sccli2", (double) 0);
            rx.insElem("sccli3", (double) 0);
            rx.insElem("esclsctestata", (double) 0);
            rx.insElem("lordo", (double) 0);
            rx.insElem("importo", (double) 0);
            rx.insElem("lotto", "");
            rx.insElem("oma", (int) 0);
            rx.insElem("serviziodatai", "0000-00-00");
            rx.insElem("serviziodataf", "0000-00-00");
            if (rigacorr == -1) {
                vrighe.add(rx);
                mostraCarrello();
                ricalcolaTotale();
                preparaInsRiga();
            } else {
                vrighe.set(rigacorr, rx);
                mostraCarrello();
                ricalcolaTotale();
                preparaInsRiga();
            }
            Record rcesp = new Record();
            rcesp.insElem("cespid", -1);
            cespitidaregistrare.add(rcesp);
        }
    }//GEN-LAST:event_buttonInsActionPerformed

    private void tabcarrelloMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tabcarrelloMouseClicked
    {//GEN-HEADEREND:event_tabcarrelloMouseClicked
    }//GEN-LAST:event_tabcarrelloMouseClicked

    private void aspettobeniActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_aspettobeniActionPerformed
    {//GEN-HEADEREND:event_aspettobeniActionPerformed
    }//GEN-LAST:event_aspettobeniActionPerformed

    private void causaletrasportoActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_causaletrasportoActionPerformed
    {//GEN-HEADEREND:event_causaletrasportoActionPerformed
    }//GEN-LAST:event_causaletrasportoActionPerformed

    private void tipotrasportoActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_tipotrasportoActionPerformed
    {//GEN-HEADEREND:event_tipotrasportoActionPerformed
        if (tipotrasporto.getSelectedIndex() == 2) {
            pvettore.setVisible(true);
        } else {
            pvettore.setVisible(false);
            vettore.svuota();
            dataritiro.setText("");
            oraritiro.impostaOrario(0, 0);
        }
    }//GEN-LAST:event_tipotrasportoActionPerformed

    private void buttonInsScadActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonInsScadActionPerformed
    {//GEN-HEADEREND:event_buttonInsScadActionPerformed
        if (datamov.getData() == null) {
            JOptionPane.showMessageDialog(this, "Indicare la data del documento", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            datamov.requestFocus();
            return;
        }

        if (cliente.leggiId() == -1) {
            JOptionPane.showMessageDialog(this, "Indicare il cliente", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CLIENTE);
            cliente.fuoco();
            return;
        }

        double tscad = 0;
        for (Record rsc : vscad) {
            tscad += rsc.leggiDouble("importo");
        }

        tscad = OpValute.arrotondaMat(tscad, 2);
        double nettoapag = OpValute.arrotondaMat(Formattazione.estraiDouble(totnetto.getText()) - totpag.getValore(), 2);
        DialogGestScadDocPop dsc = new DialogGestScadDocPop(formGen, true, datamov.getDataStringa(), null, nettoapag, tscad);
        UtilForm.posRelSchermo(dsc, UtilForm.CENTRO);
        dsc.setVisible(true);
        if (dsc.leggiScelta() == DialogGestScadDocPop.OK) {
            Record rscris = dsc.leggiScadenza();
            vscad.add(rscris);
            scadmodif = true;
            mostraScadenze();
        }
    }//GEN-LAST:event_buttonInsScadActionPerformed

    private void tabdatiaggMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_tabdatiaggMouseClicked
    {//GEN-HEADEREND:event_tabdatiaggMouseClicked
        if (tabdatiagg.getSelectedRow() >= 0 && evt.getClickCount() == 2) {
            rigacorrdatiagg = tabdatiagg.getSelectedRow();
            Record rx = vdatiagg.get(tabdatiagg.getSelectedRow());
            datiagg_tiporec.setSelectedIndex(rx.leggiIntero("mfedatiporec"));
            datiagg_numdoc.visTesto(rx, "mfedanumdoc");
            datiagg_datadoc.visData(rx, "mfedadatadoc");
            datiagg_codcommessa.visTesto(rx, "mfedacodcommessa");
            datiagg_codcup.visTesto(rx, "mfedacodcup");
            datiagg_codcig.visTesto(rx, "mfedacodcig");
        }
    }//GEN-LAST:event_tabdatiaggMouseClicked

    private void buttonInsDatiAggActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonInsDatiAggActionPerformed
    {//GEN-HEADEREND:event_buttonInsDatiAggActionPerformed
        if (datiagg_numdoc.getText().trim().equals("") && datiagg_datadoc.getData() == null && datiagg_codcommessa.getText().trim().equals("")
                && datiagg_codcup.getText().trim().equals("") && datiagg_codcig.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Indicare almeno un dato", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (!datiagg_codcup.getText().trim().equals("") && (datiagg_numdoc.getText().trim().equals("") || datiagg_datadoc.getData() == null)) {
            JOptionPane.showMessageDialog(this, "Per codice unitario progetto (CUP) inserire anche numero e data documento di riferimento", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (!datiagg_codcig.getText().trim().equals("") && (datiagg_numdoc.getText().trim().equals("") || datiagg_datadoc.getData() == null)) {
            JOptionPane.showMessageDialog(this, "Per codice unitario progetto (CUP) inserire anche numero e data documento di riferimento", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        Record rx = new Record();
        rx.insElem("mfedatiporec", datiagg_tiporec.getSelectedIndex());
        datiagg_numdoc.insTesto(rx, "mfedanumdoc");
        datiagg_datadoc.insData(rx, "mfedadatadoc");
        datiagg_codcommessa.insTesto(rx, "mfedacodcommessa");
        datiagg_codcup.insTesto(rx, "mfedacodcup");
        datiagg_codcig.insTesto(rx, "mfedacodcig");
        if (rigacorr >= 0) {
            vdatiagg.set(rigacorr, rx);
        } else {
            vdatiagg.add(rx);
        }
        datiagg_numdoc.setText("");
        datiagg_datadoc.setText("");
        datiagg_codcommessa.setText("");
        datiagg_codcup.setText("");
        datiagg_codcig.setText("");
        buttonIns.setText("INS");
        rigacorrdatiagg = -1;
        mostraDatiAggiuntivi();
    }//GEN-LAST:event_buttonInsDatiAggActionPerformed

    private void datiagg_bollovirtualeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_datiagg_bollovirtualeActionPerformed
    {//GEN-HEADEREND:event_datiagg_bollovirtualeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_datiagg_bollovirtualeActionPerformed

    private void buttonNuovoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovoActionPerformed
        if (cliente.leggiId() != -1) {
            String qry = " SELECT clicontoid FROM clienti WHERE cliid=" + cliente.leggiId();
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();
            int contid = r.leggiIntero("clicontoid");

            EnvJazz.msg[0] = "";
            EnvJazz.msg[1] = "ESEGUI";
            Object[] cpar = new Object[9];
            cpar[0] = formGen;
            cpar[1] = new Boolean(true);
            cpar[2] = connAz;
            cpar[3] = connCom;
            cpar[4] = azienda;
            cpar[5] = esContabile;
            cpar[6] = new String("Scheda cliente");
            cpar[7] = new Integer(contid);
            cpar[8] = new Integer(EnvJazz.RITORNO);
            Object d = FunzioniUtilita.creaOggetto("popcontabilita.dialogstart.DialogSchedaContabilePop", cpar);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            FunzioniUtilita.invocaMetodo(d, "show", null);
        } else {
            JOptionPane.showMessageDialog(formGen, "Prima selezionare il cliente", "Errore",
                    JOptionPane.ERROR_MESSAGE,
                    new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            cliente.fuoco();
            return;
        }
    }//GEN-LAST:event_buttonNuovoActionPerformed

    private void buttonSpostaGiuActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonSpostaGiuActionPerformed
    {//GEN-HEADEREND:event_buttonSpostaGiuActionPerformed
        if (tabcarrello.getSelectedRow() < tabcarrello.getRowCount() - 1) {
            int sel = tabcarrello.getSelectedRow();
            Record rsel = vrighe.get(sel);
            Record rsucc = vrighe.get(sel + 1);
            vrighe.set(sel + 1, rsel);
            vrighe.set(sel, rsucc);
            mostraCarrello();
            tabcarrello.setRowSelectionInterval(sel + 1, sel + 1);
        }
    }//GEN-LAST:event_buttonSpostaGiuActionPerformed

    private void buttonSpostaSuActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonSpostaSuActionPerformed
    {//GEN-HEADEREND:event_buttonSpostaSuActionPerformed
        if (tabcarrello.getSelectedRow() > 0) {
            int sel = tabcarrello.getSelectedRow();
            Record rsel = vrighe.get(sel);
            Record rprec = vrighe.get(sel - 1);
            vrighe.set(sel - 1, rsel);
            vrighe.set(sel, rprec);
            mostraCarrello();
            tabcarrello.setRowSelectionInterval(sel - 1, sel - 1);
        }
    }//GEN-LAST:event_buttonSpostaSuActionPerformed

    private void taballMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_taballMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_taballMouseClicked

    private void buttonRicFileAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRicFileAllActionPerformed
        JFileChooser fc = new JFileChooser();
        fc.setDialogType(JFileChooser.CUSTOM_DIALOG);
        fc.setApproveButtonText("OK");
        fc.setDialogTitle("Scelta file allegato");
        fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (fc.showDialog(this.getParent(), null) == JFileChooser.APPROVE_OPTION) {
            alink.setText(fc.getSelectedFile().getPath());
            if (anome.getText().equals("")) {
                File fx = new File(alink.getText());
                if (fx.exists()) {
                    anome.setText(fx.getName());
                }

                if (fx.getName().toLowerCase().endsWith("zip")) {
                    aalgcompr.setSelectedItem("ZIP");
                } else if (fx.getName().toLowerCase().endsWith("rar")) {
                    aalgcompr.setSelectedItem("RAR");
                }

                if (fx.getName().indexOf(".") > 0) {
                    aformato.setText(fx.getName().substring(fx.getName().indexOf(".") + 1));
                }
            }
        }
    }//GEN-LAST:event_buttonRicFileAllActionPerformed

    private void buttonInsAllegatiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsAllegatiActionPerformed
        if (anome.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Indicare nome allegato", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            anome.requestFocus();
            return;
        }

        if (alink.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Indicare il file allegato", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            alink.requestFocus();
            return;
        }

        File f = new File(alink.getText().trim());
        if (!f.exists()) {
            JOptionPane.showMessageDialog(this, "Il file indicato non esiste in questo percorso", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            alink.requestFocus();
            return;
        }

        Record rx = new Record();
        anome.insTesto(rx, "mfeanome");
        if (aalgcompr.getSelectedIndex() > 0) {
            rx.insElem("mfeaalgcompr", (String) aalgcompr.getSelectedItem());
        } else {
            rx.insElem("mfeaalgcompr", "");
        }

        aformato.insTesto(rx, "mfeaformato");
        adescr.insTesto(rx, "mfeadescr");
        alink.insTesto(rx, "mfealinkdoc");
        if (rigacorr >= 0) {
            vallegati.set(rigacorr, rx);
        } else {
            vallegati.add(rx);
        }

        anome.setText("");
        aalgcompr.setSelectedIndex(0);
        aformato.setText("");
        adescr.setText("");
        alink.setText("");
        buttonIns.setText("INS");
        rigacorr = -1;
        mostraAllegati();
    }//GEN-LAST:event_buttonInsAllegatiActionPerformed

    private void buttonScorporoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonScorporoActionPerformed
        double val = prezzo.getValore();
        if (val != 0 && codiva.leggiId() != -1) {
            String qryiva = "SELECT ivaaliq FROM codiva WHERE ivaid = " + codiva.leggiId();
            QueryInterrogazioneClient qi = new QueryInterrogazioneClient(connCom, qryiva);
            qi.apertura();
            double aliq = qi.successivo().leggiDouble("ivaaliq");
            qi.chiusura();

            val = OpValute.arrotondaMat(val * 100.0 / (100.0 + aliq), prezzo.getDecimali());
            prezzo.setText(Formattazione.formValuta(val, 12, prezzo.getDecimali(), Formattazione.SEGNO_SX_NEG));
        } else {
            JOptionPane.showMessageDialog(this, "Per scorporo IVA dal prezzo indicare il prezzo e il codice IVA",
                    "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            prezzo.requestFocus();
        }
        ricalcolaRiga();
    }//GEN-LAST:event_buttonScorporoActionPerformed

    private void buttonRicercaCespiteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRicercaCespiteActionPerformed

        listcesp = new ArrayList();
        DialogRicercaVenditaCespite d = new DialogRicercaVenditaCespite(formGen, true, connAz, azienda, esContabile, datamov.getDataStringa(), "Vendita");
        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
        d.show();
        requestFocus();

        listcesp = d.leggiCespitiEstratti();
        if (listcesp.isEmpty()) {
            JOptionPane.showMessageDialog(this, "Le informazioni qua registrate, se inserite nel dettaglio fattura\n"
                    + "non influiscono sulle immobilizzazoni, perche' nessun cespite e' stato richiamato", "Info",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            requestFocus();
        }

        String descr = "";
        totbene = 0.00;
        totfondo = 0.00;
        totresto = 0.00;
        double qta = 0.00;
        for (int i = 0; i < listcesp.size(); i++) {
//            double vv[] = BusinessLogicCespiti.calcoloValoriAllaData(connAz.conn, azienda, listcesp.get(i).leggiIntero("cespid"), listcesp.get(i).leggiIntero("cespmoviniid"), datamov.getDataStringa());

            double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, listcesp.get(i).leggiIntero("cespid"), esContabile, datamov.getDataStringa());
            totbene += OpValute.arrotondaMat(vv[1], 2);
            totfondo += OpValute.arrotondaMat(vv[3], 2);
            totresto = totbene - totfondo;
            descr += " " + listcesp.get(i).leggiStringa("cespdescriz1");
        }

        valorestorico.setText(Formattazione.formatta(totbene, "#######0.00", 0));
        valorefondo.setText(Formattazione.formatta(totfondo, "#######0.00", 0));
        residuostorico.setText(Formattazione.formatta(totresto, "#######0.00", 0));
        valoreparziale.setText(Formattazione.formatta(totbene, "#######0.00", 0));

        descrart.setText(descr);

        calcolaResiduiParziali();
    }//GEN-LAST:event_buttonRicercaCespiteActionPerformed

    private void tabnotacMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabnotacMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tabnotacMouseClicked

    private void buttonInsNotacActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsNotacActionPerformed
        if (notac_numft.getText().trim().equals("") || notac_dataft.getData() == null) {
            JOptionPane.showMessageDialog(formGen, "Indicare numero e data fattura", "Errore",
                    JOptionPane.ERROR_MESSAGE,
                    new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }
        Record rx = new Record();
        rx.insElem("datadocman", notac_dataft.getDataStringa());
        rx.insElem("numerodocman", notac_numft.getText().trim());
        if (rftcorr != null) {
            rx.insElem("movdocid", rftcorr.leggiIntero("id"));
            rx.insElem("esmov", rftcorr.leggiStringa("esmov"));
            rx.insElem("nomesogg", rftcorr.leggiStringa("nome"));
            rx.insElem("totaledoc", rftcorr.leggiDouble("totaledoc"));
        } else {
            rx.insElem("movdocid", (int) -1);
            rx.insElem("esmov", "");
            rx.insElem("nomesogg", "");
            rx.insElem("totaledoc", (double) 0);
        }
        vnotac.add(rx);
        mostraDocumento(rftcorr.leggiIntero("id"));
        rftcorr = null;
        notac_ftselez.setText("");
        notac_numft.setText("");
        notac_dataft.setText("");
        mostraFattureNotaCredito();

    }//GEN-LAST:event_buttonInsNotacActionPerformed

    private void buttonRicercaFattActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRicercaFattActionPerformed
        if (cliente.leggiId() == -1) {
            JOptionPane.showMessageDialog(formGen, "Indicare prima il cliente", "Errore",
                    JOptionPane.ERROR_MESSAGE,
                    new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CLIENTE);
            cliente.fuoco();
            return;
        }
        DialogRicercaFattureClientiPop d = new DialogRicercaFattureClientiPop(formGen, true, connAz, connCom, azienda, esmov, cliente.leggiId());
        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
        d.setVisible(true);
        if (d.leggiId() != -1) {
            ArrayList<Record> tmp = BusinessLogicDocumenti.ricercaDocumenti(EnvJazz.connAmbiente.conn, connAz.conn, connCom.conn, d.leggiEsercizio(), "",
                    (int) 0, (int) 0,
                    "0000-00-00", "0000-00-00",
                    0, 0, "0000-00-00", "0000-00-00",
                    "", "", "0000-00-00", "0000-00-00",
                    -1, -1, -1, -1, 0, (int) 0,
                    (double) 0, "", "", "", (int) 0, -1, -1, -1, -1,
                    (int) 0, (int) 0, "", -1, -1, -1, -1, -1, -1, -1, true, "", 0, -1, null, d.leggiId(), false, "", "", 0, 0, -1, -1, -1, -1, -1, "", -1, -1, -1, "", "");
            if (tmp != null && tmp.size() > 0) {
                Record rft = tmp.get(0);
                rftcorr = rft;
                notac_ftselez.setText("<html>" + rft.leggiStringa("sezionale") + "/" + rft.leggiIntero("numerodoc") + " del " + (new Data(rft.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/") + "<br>"
                        + rft.leggiStringa("nome") + "</html>");
                notac_numft.setText(rft.leggiStringa("sezionale") + "/" + rft.leggiIntero("numerodoc"));
                notac_dataft.visData(rft, "datadoc");

            } else {
                rftcorr = null;
                notac_ftselez.setText("");
                notac_numft.setText("");
                notac_dataft.setText("");
            }
        }
    }//GEN-LAST:event_buttonRicercaFattActionPerformed

    private void clifenodescrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clifenodescrActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_clifenodescrActionPerformed

    private void tabnotac1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabnotac1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tabnotac1MouseClicked

    private void buttonInsFtaccontoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsFtaccontoActionPerformed
        
        vfattacconto.add(rftcorr);
        
        Record rx = new Record();
        rx.insElem("tiporiga", (int) 1);
        rx.insElem("artid", (int) -1);
        rx.insElem("artcod", "");
        String descrriga = "";
        descrriga =  "Detrazione anticipo Fattura n." + rfac.leggiStringa("movdocsufft") + "/" + rfac.leggiIntero("movdocnumft") + " del "
                    + (new Data(rfac.leggiStringa("movdocdtft"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
        rx.insElem("artdescr", descrriga);
//            rx.insElem("artum", um.getText().trim());
        int ivaId = rivadoc.leggiIntero("ipcodid");
        if (rcli.leggiIntero("clientepubblico") == 1) {
            // ente pubblico con split payment, iva a carico del committente

            Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid",rivadoc.leggiIntero("ipcodid"), "");
            if (ri.leggiIntero("ivagruppo") == 0 && ri.leggiDouble("ivaaliq") > 0 && ri.leggiIntero("ivacess") == 0) {
                String qryiva = "SELECT * FROM codiva WHERE ivaaliq = " + Formattazione.formatta(ri.leggiDouble("ivaaliq"), "##0.00", 0).replaceAll(",", ".") + " AND ivacess = 1 AND ivagest = 0";
                QueryInterrogazioneClient qisp = new QueryInterrogazioneClient(connCom, qryiva);
//                    System.out.println(qisp.query());
                qisp.apertura();
                Record risp = qisp.successivo();
                qisp.chiusura();

                if (risp != null) {
                    ivaId = risp.leggiIntero("ivaid");
                }
            }
        }
        rx.insElem("ivaid", ivaId);

        Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", rivadoc.leggiIntero("ipcodid"), "");
        rx.insElem("ivacod", ri.leggiStringa("ivacod"));
        rx.insElem("colli", 1);
        rx.insElem("qta",1.0);
        double prz = -rftcorr.leggiDouble("totimp");

        rx.insElem("prezzo", prz);
        rx.insElem("scart1",0.00);
        rx.insElem("sccli1", sclicalc1);
        rx.insElem("sccli2", sclicalc2);
        rx.insElem("sccli3", sclicalc3);
        rx.insElem("esclsctestata", (double) 0);
        rx.insElem("lordo", prz);
        rx.insElem("importo", prz);

        rx.insElem("oma", (int) 0);
        rx.insElem("serviziodatai", "0000-00-00");
        rx.insElem("serviziodataf", "0000-00-00");
        rx.insElem("lotto","");
        if (rigacorr == -1) {
            vrighe.add(rx);
            mostraCarrello();
            ricalcolaTotale();
            preparaInsRiga();
        } else {
            vrighe.set(rigacorr, rx);
            mostraCarrello();
            ricalcolaTotale();
            preparaInsRiga();
        }

        Record rcesp = new Record();
        rcesp.insElem("cespid", -1);
        cespitidaregistrare.add(rcesp);

        mostraFattureAcconto();
    }//GEN-LAST:event_buttonInsFtaccontoActionPerformed

    private void buttonRicercaFatt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRicercaFatt1ActionPerformed
       if (cliente.leggiId() == -1) {
            JOptionPane.showMessageDialog(formGen, "Indicare prima il cliente", "Errore",
                    JOptionPane.ERROR_MESSAGE,
                    new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            ptab.setSelectedIndex(TAB_CLIENTE);
            cliente.fuoco();
            return;
        }
       
        DialogRicercaFtAcconto dacc = new DialogRicercaFtAcconto(formGen, true, connAz, connCom, azienda, esContabile, cliente.leggiId());
        UtilForm.posRelSchermo(dacc, UtilForm.CENTRO);
        dacc.setVisible(true);
        if (dacc.leggiRisultato() != null) {
            rftcorr=dacc.leggiRisultato();
            
            double percevas = dacc.leggiPercentualeEvasione();
            if (percevas == 0)
                percevas = 100;
            // aggiunge fattura a lista acconti
            Record rx = new Record();
            rx.insElem("escontftacc", rftcorr.leggiStringa("esmov"));
            rx.insElem("movdocftaccid", rftcorr.leggiIntero("movdocid"));
            rx.insElem("percevas", percevas);
            listaAnticipi.addElement(rx);
            
            // inserisce non codificato su corpo doc per storno fattura (1 riga per ogni codice iva della fattura acconto, da movdociva)
            String qry="SELECT * FROM movimentidoc" + rftcorr.leggiStringa("esmov") + " WHERE movdocid = " + rftcorr.leggiIntero("movdocid");
            Query qfac = new Query(connAz, qry);
            qfac.apertura();
            rfac = qfac.successivo();
            qfac.chiusura();           

            String qry2="SELECT pdcfattantic,cmstornofattantic FROM contifattven";
            QueryInterrogazioneClient qpar = new QueryInterrogazioneClient(connAz,qry2);
            qpar.apertura();
            Record rpar = qpar.successivo();
            int cmstornoId = rpar.leggiIntero("cmstornofattantic");
            qpar.chiusura();
            if (cmstornoId == -1) {
                Query qcs = new Query(connAz, "SELECT cmid FROM caumag WHERE cmtipo = 1 AND cmraggr = 6");
                qcs.apertura();
                if (qcs.numeroRecord() > 0) {
                    cmstornoId = qcs.successivo().leggiIntero("cmid");
                }
                qcs.chiusura();
            }
            if (cmstornoId == -1) {
                cmstornoId = rDoc.leggiIntero("doccaumagid");
            }
            Record rcmstorno = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "caumag", "cmid", cmstornoId, "");

            String qryivaprof="SELECT * FROM ivaproforma" + rfac.leggiStringa("movdocdtft").substring(0, 4) + " WHERE ipmovdocid = " + rftcorr.leggiIntero("movdocid") + " AND ipescont = \"" + rftcorr.leggiStringa("esmov") + "\"";
            Query qivadoc = new Query(connAz,qryivaprof);
            qivadoc.apertura();
            double residuo =0.00;
            for (int i = 0; i < qivadoc.numeroRecord(); i++) {
                rivadoc = qivadoc.successivo();
                double impsaldiprec = 0;
                // cerca altri saldi precedenti della fattura di acconto
                String qrysaldi="SELECT * FROM ftacconto_saldi WHERE facsescontftacc = \"" + rftcorr.leggiStringa("esmov") + 
                        "\" AND facsmovdocftaccid = " + rftcorr.leggiIntero("movdocid") + " AND facsmovdocftevasid <> " + movdocId;
                Query qsldprec = new Query(connAz,qrysaldi);
                qsldprec.apertura();
                for (int j = 0; j < qsldprec.numeroRecord(); j++)
                {
                    Record rsldprec = qsldprec.successivo();
                    String qrytot="SELECT SUM(corpomagnetto) AS tnetto FROM corpodocmag" + rsldprec.leggiStringa("facsescontftevas") + " WHERE corpomagmovdocid = " + rsldprec.leggiIntero("facsmovdocftevasid") + " AND corpomagivaid = " + rivadoc.leggiIntero("ipcodid") +
                            " AND corpomagkey LIKE \"ACC_%\"";
                    Query qcrpprec = new Query(connAz, qrytot);
                    qcrpprec.apertura();
                    Record rcrpprec = qcrpprec.successivo();
                    qcrpprec.chiusura();
                    if (rcrpprec != null)
                    {
                        impsaldiprec += Math.abs(rcrpprec.leggiDouble("tnetto"));
                    }
                }
                qsldprec.chiusura();
                impsaldiprec = OpValute.arrotondaMat(impsaldiprec, 2);
                
                Record rc = new Record();
                double imp = Math.abs(rivadoc.leggiDouble("ipimp"));
                residuo = OpValute.arrotondaMat(imp - impsaldiprec, 2);
                if (percevas > 0 && percevas < 100)
                {
                    residuo = OpValute.arrotondaMat(residuo * percevas / 100.0, 2);
                }
                residuo = -residuo;
                String key = "ACC_" + rftcorr.leggiStringa("esmov") + "_" + rftcorr.leggiIntero("movdocid");               
            }
            qivadoc.chiusura();
            
            notac_numft1.setText(rfac.leggiStringa("movdocsufft") + "/" + rfac.leggiIntero("movdocnumft"));
            notac_dataft1.setText(new Data(rfac.leggiStringa("movdocdtft"),Data.AAAA_MM_GG).formatta(Data.GG_MM_AAAA, "/"));
            imponibilealiq1.setText(Formattazione.formValuta(residuo, 12, 2, 2));
        }
    }//GEN-LAST:event_buttonRicercaFatt1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel JLabel10;
    public javax.swing.JLabel JLabel11;
    public javax.swing.JLabel JLabel2;
    public javax.swing.JLabel JLabel3;
    private javax.swing.JLabel JLabel46;
    private javax.swing.JLabel JLabel48;
    private javax.swing.JLabel JLabel50;
    private javax.swing.JLabel JLabel51;
    private javax.swing.JLabel JLabel52;
    private javax.swing.JLabel JLabel53;
    private javax.swing.JLabel JLabel54;
    private javax.swing.JLabel JLabel55;
    private javax.swing.JLabel JLabel56;
    private javax.swing.JLabel JLabel57;
    private javax.swing.JLabel JLabel58;
    private javax.swing.JLabel JLabel59;
    private javax.swing.JLabel JLabel60;
    private javax.swing.JLabel JLabel61;
    private javax.swing.JLabel JLabel62;
    private javax.swing.JLabel JLabel63;
    private javax.swing.JLabel JLabel64;
    private javax.swing.JLabel JLabel65;
    private javax.swing.JLabel JLabel66;
    private javax.swing.JLabel JLabel67;
    private javax.swing.JLabel JLabel68;
    private javax.swing.JLabel JLabel69;
    private javax.swing.JLabel JLabel70;
    private javax.swing.JLabel JLabel71;
    private javax.swing.JLabel JLabel72;
    private javax.swing.JLabel JLabel73;
    private javax.swing.JLabel JLabel74;
    private javax.swing.JLabel JLabel75;
    private javax.swing.JLabel JLabel76;
    private javax.swing.JLabel JLabel77;
    private javax.swing.JLabel JLabel78;
    private javax.swing.JLabel JLabel79;
    public javax.swing.JLabel JLabel8;
    private javax.swing.JLabel JLabel80;
    private javax.swing.JLabel JLabel81;
    private javax.swing.JLabel JLabel82;
    private javax.swing.JLabel JLabel83;
    private javax.swing.JLabel JLabel84;
    private javax.swing.JLabel JLabel85;
    private javax.swing.JLabel JLabel86;
    private javax.swing.JLabel JLabel87;
    private javax.swing.JLabel JLabel88;
    private javax.swing.JLabel JLabel89;
    public javax.swing.JLabel JLabel9;
    private javax.swing.JLabel JLabel92;
    private javax.swing.JLabel JLabel93;
    private javax.swing.JLabel JLabel94;
    private javax.swing.JLabel JLabel95;
    private javax.swing.JComboBox aalgcompr;
    private ui.beans.CampoTestoPop adescr;
    private ui.beans.CampoTestoPop aformato;
    private ui.beans.CampoTestoPop alink;
    private ui.beans.CampoTestoPop anome;
    public javax.swing.JComboBox aspettobeni;
    private javax.swing.JLabel avvisoscad;
    private javax.swing.ButtonGroup buttonGroup1;
    private ui.beans.PopButton buttonIns;
    private ui.beans.PopButton buttonInsAllegati;
    private ui.beans.PopButton buttonInsDatiAgg;
    private ui.beans.PopButton buttonInsFtacconto;
    private ui.beans.PopButton buttonInsNotac;
    private ui.beans.PopButton buttonInsScad;
    public ui.beans.PopButton buttonModNum;
    private ui.beans.PopButton buttonNuovo;
    private ui.beans.PopButton buttonRicFileAll;
    private ui.beans.PopButton buttonRicercaCespite;
    private ui.beans.PopButton buttonRicercaFatt;
    private ui.beans.PopButton buttonRicercaFatt1;
    public ui.beans.PopButton buttonSalva;
    private ui.beans.PopButton buttonScorporo;
    private ui.beans.PopButton buttonSpostaGiu;
    private ui.beans.PopButton buttonSpostaSu;
    public javax.swing.JComboBox causaletrasporto;
    private javax.swing.JLabel cfcli;
    private archivibasepop.beans.BeanGestClientePop cliente;
    private javax.swing.JCheckBox clifenodescr;
    private archivibasepop.beans.BeanGestIvaPop codiva;
    private archivibasepop.beans.BeanGestIvaPop codiva1;
    private archivibasepop.beans.BeanGestConsegnaPop consegna;
    private ui.beans.CampoDataPop datacons;
    private ui.beans.CampoDataPop datamov;
    private ui.beans.CampoDataPop datapart;
    private ui.beans.CampoDataPop datarif;
    private ui.beans.CampoDataPop dataritiro;
    public javax.swing.JCheckBox datiagg_bollovirtuale;
    private ui.beans.CampoTestoPop datiagg_codcig;
    private ui.beans.CampoTestoPop datiagg_codcommessa;
    private ui.beans.CampoTestoPop datiagg_codcup;
    private ui.beans.CampoDataPop datiagg_datadoc;
    private ui.beans.CampoValutaPop datiagg_impbollo;
    private ui.beans.CampoTestoPop datiagg_numdoc;
    private javax.swing.JComboBox datiagg_tiporec;
    private ui.beans.PopDescrLabelLineBorder descraliq;
    private ui.beans.CampoTestoMultilineaPop descrart;
    private javax.swing.JLabel descrcons;
    private ui.beans.PopDescrLabelLineBorder descriva;
    private javax.swing.JLabel descrpag;
    private ui.beans.PopDescrLabelLineBorder descrporto;
    private ui.beans.PopDescrLabelLineBorder descrvettore;
    private halleytech.models.DocumentoNumero documentoNumero1;
    private halleytech.models.DocumentoNumero documentoNumero2;
    private halleytech.models.DocumentoNumero documentoNumero3;
    private halleytech.models.DocumentoNumero documentoNumero4;
    private halleytech.models.DocumentoTesto documentoTesto1;
    private halleytech.models.DocumentoTesto documentoTesto2;
    private halleytech.models.DocumentoValuta documentoValuta1;
    private halleytech.models.DocumentoValuta documentoValuta2;
    private halleytech.models.DocumentoValuta documentoValuta3;
    private halleytech.models.DocumentoValuta documentoValuta4;
    private halleytech.models.DocumentoValuta documentoValuta5;
    private halleytech.models.DocumentoValuta documentoValuta6;
    private javax.swing.JTextField editcolli;
    private javax.swing.JTextField editqta;
    private ui.beans.CampoValutaPop imponibilealiq;
    private ui.beans.CampoValutaPop imponibilealiq1;
    private ui.beans.CampoValutaPop imponibilenoiva;
    private javax.swing.JLabel importo;
    private javax.swing.JLabel indcli;
    private javax.swing.JLabel indcons;
    private javax.swing.JLabel jLabminuss1;
    private javax.swing.JLabel jLabminuss2;
    private javax.swing.JLabel jLabminuss3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollAll;
    private javax.swing.JScrollPane jScrollDatiAgg;
    private javax.swing.JScrollPane jScrollIva;
    private javax.swing.JScrollPane jScrollNotac;
    private javax.swing.JScrollPane jScrollNotac1;
    private javax.swing.JScrollPane jScrollScad;
    private javax.swing.JLabel label_datacons;
    private javax.swing.JLabel label_datamov;
    private javax.swing.JLabel label_datarif;
    private javax.swing.JLabel label_numfiscale;
    private javax.swing.JLabel label_nummov;
    private javax.swing.JLabel label_numrif;
    private javax.swing.JLabel label_scad;
    private javax.swing.JLabel label_sogg10;
    private javax.swing.JLabel label_sogg11;
    private javax.swing.JLabel label_sogg12;
    private javax.swing.JLabel label_sogg15;
    private javax.swing.JLabel label_sogg16;
    private javax.swing.JLabel label_sogg17;
    private javax.swing.JLabel label_sogg25;
    private javax.swing.JLabel label_sogg26;
    private javax.swing.JLabel label_sogg27;
    private javax.swing.JLabel label_sogg28;
    private javax.swing.JLabel label_sogg29;
    private javax.swing.JLabel label_sogg4;
    private javax.swing.JLabel label_sogg5;
    private javax.swing.JLabel label_sogg6;
    private javax.swing.JLabel label_sogg7;
    private javax.swing.JLabel label_sogg8;
    private javax.swing.JLabel label_sogg9;
    private javax.swing.JLabel label_totpag;
    private javax.swing.JLabel label_totscad;
    private javax.swing.JLabel label_um;
    private javax.swing.JLabel label_um2;
    private javax.swing.JLabel loccli;
    private javax.swing.JLabel loccons;
    private javax.swing.JLabel minusvalenza;
    private javax.swing.JLabel nomecli;
    private javax.swing.JLabel nomecli2;
    private ui.beans.CampoDataPop notac_dataft;
    private ui.beans.CampoDataPop notac_dataft1;
    private javax.swing.JLabel notac_ftselez;
    private javax.swing.JLabel notac_ftselez1;
    private ui.beans.CampoTestoPop notac_numft;
    private ui.beans.CampoTestoPop notac_numft1;
    private ui.beans.CampoTestoMultilineaPop notepiede;
    private javax.swing.JLabel numero;
    private ui.beans.CampoInteroPop numerofisc;
    private ui.beans.CampoTestoPop numrif;
    private ui.beans.CampoOraPop orapart;
    private ui.beans.CampoOraPop oraritiro;
    private javax.swing.JScrollPane pScrollCarrello;
    private javax.swing.JPanel pacconto;
    private archivibasepop.beans.BeanGestPagamentoPop pagamento;
    private javax.swing.JPanel pallegati;
    private javax.swing.JLabel parzialefondo;
    private javax.swing.JPanel pcarr1;
    private javax.swing.JPanel pcarrello;
    private javax.swing.JPanel pcli1;
    private javax.swing.JPanel pcli2;
    private javax.swing.JPanel pcom;
    private javax.swing.JPanel pcom1;
    private javax.swing.JPanel pcom2;
    private javax.swing.JPanel pcomcarrello;
    private javax.swing.JPanel pdatiagg;
    private javax.swing.JPanel pdatitrasp;
    private javax.swing.JPanel pdest1;
    private javax.swing.JPanel pdest2;
    private javax.swing.JPanel pidfisc;
    private javax.swing.JPanel pidfisc2;
    private javax.swing.JPanel pinsriga;
    private javax.swing.JLabel pivacli;
    private javax.swing.JLabel plusvalenza;
    private javax.swing.JPanel pnotac;
    private archivibasepop.beans.BeanGestPortoPop porto;
    private javax.swing.JPanel ppag1;
    private javax.swing.JPanel ppag2;
    private ui.beans.CampoValutaPop prezzo;
    private javax.swing.JPanel priep;
    private javax.swing.JTabbedPane ptab;
    private javax.swing.JPanel ptesta;
    private javax.swing.JPanel ptesta2;
    private javax.swing.JPanel ptesta_dest;
    private javax.swing.JPanel ptesta_pag;
    private javax.swing.JPanel ptesta_pcli;
    private javax.swing.JPanel ptesta_rif;
    private javax.swing.JPanel ptesta_sconti;
    private javax.swing.JPanel ptestata;
    private javax.swing.JPanel pvettore;
    private ui.beans.CampoValutaPop qta;
    private javax.swing.JLabel residuoparziale;
    private javax.swing.JLabel residuostorico;
    private ui.beans.CampoValutaPop sc1;
    private javax.swing.JLabel stato;
    private javax.swing.JTable taball;
    private javax.swing.JTable tabcarrello;
    private javax.swing.JTable tabdatiagg;
    private javax.swing.JTable tabiva;
    private javax.swing.JTable tabnotac;
    private javax.swing.JTable tabnotac1;
    private javax.swing.JTable tabscad;
    private ui.beans.CampoTestoPop targa;
    private javax.swing.JLabel tipocli;
    public javax.swing.JComboBox tipotrasporto;
    private javax.swing.JLabel totabbuono;
    private javax.swing.JLabel totaccred;
    private ui.beans.CampoInteroPop totcolli;
    private javax.swing.JLabel totdoc;
    private javax.swing.JLabel totdoc2;
    private javax.swing.JLabel totimp;
    private javax.swing.JLabel totiva;
    private javax.swing.JLabel totnetto;
    private ui.beans.CampoValutaPop totpag;
    private ui.beans.CampoInteroPop totpallets;
    private ui.beans.CampoValutaPop totpesolordo;
    private ui.beans.CampoValutaPop totpesonetto;
    private javax.swing.JLabel totscad;
    private javax.swing.JLabel valorefondo;
    private ui.beans.CampoValutaPop valoreparziale;
    private javax.swing.JLabel valorestorico;
    private archivibasepop.beans.BeanGestVettorePop vettore;
    private nucleo.WaitJazzBianco waitloc;
    // End of variables declaration//GEN-END:variables

    private void preparaNuovo() {
        modo = EnvJazz.INS;
        listcesp = new ArrayList<Record>();
        oma = false;
        ivaomaggioId = -1;
        scadmodif = false;
        movdocId = -1;
        esmov = esContabile;
        iva4t = -1;
        iva4i = -1;
        iva4n = -1;
        iva10t = -1;
        iva10i = -1;
        iva10n = -1;
        iva22t = -1;
        iva22i = -1;
        iva22n = -1;
        stato.setText("NUOVO DOC.");
        datamov.setEnabled(true);
        // aggiorna data
        String[] dte = FunzioniDB.leggiEsercizio(azienda, esContabile);
        // vede se la data di oggi ricade nell'esercizio
        Data dti = new Data(dte[0], Data.AAAA_MM_GG);
        Data dtf = new Data(dte[1], Data.AAAA_MM_GG);
        Data oggi = new Data();
        if (oggi.maggioreuguale(dti) && oggi.minoreuguale(dtf)) {
//            if (!datamov.isEnabled()) {
                datamov.setText((new Data()).formatta(Data.GG_MM_AAAA, "/"));
//            }
        } else {
            datamov.setText(dti.formatta(Data.GG_MM_AAAA, "/"));
        }
        //numero.setText(BusinessLogicDocumenti.leggiNumeroDocumentoLibero(connAz.leggiConnessione(), rDoc, esmov, datamov.getDataStringa()));
        aggiornaNumeroDocumento();
        buttonModNum.setEnabled(false);
        datacons.setText("");
        cliente.svuota();
        rcli = null;
        listinoId = -1;
        consegna.svuota();
        qta.setEnabled(true);
        prezzo.setEnabled(true);
        imponibilealiq.setEnabled(true);
        codiva.abilita(true);
        codiva1.abilita(true);
        imponibilealiq.setEnabled(true);
        imponibilenoiva.setEnabled(true);
//        System.out.println("SVUOTA RIGHE");
        vrighe = new ArrayList();

        if (rDoc.leggiIntero("doccliid") != -1) {
            String[] dc = BusinessLogicArchiviBase.leggiCodiceNomeCliente(connAz.leggiConnessione(), connCom.leggiConnessione(), rDoc.leggiIntero("doccliid"));
            cliente.visualizzaDati(rDoc.leggiIntero("doccliid"), dc[0]);
            cliente.caricaDatiCorrelati();
        }

        vdettddt = new ArrayList();
        mostraCarrello();
        preparaInsRiga();

        vdatiagg = new ArrayList();
        mostraDatiAggiuntivi();

        tipotrasporto.setSelectedIndex(0);
        causaletrasporto.setSelectedIndex(0);
        aspettobeni.setSelectedIndex(0);
        vettore.svuota();
        dataritiro.setText("");
        oraritiro.impostaOrario(0, 0);
        totcolli.setText("");
        totpallets.setText("");
        totpesolordo.setText("");
        totpesonetto.setText("");
        datapart.setText("");
        orapart.impostaOrario(0, 0);

        vord = new ArrayList();

        vnotac = new ArrayList();

        vallegati = new ArrayList();
        mostraAllegati();

        this.ricalcolaTotale();

        ptab.setSelectedIndex(TAB_CLIENTE);

        cliente.fuoco();
    }

    private void cercaPercorsoAlternativo() {
        Record rp = new Record();
        String qry = "SELECT * FROM pararticoli";
        QueryInterrogazioneClient qp = new QueryInterrogazioneClient(connAz, qry);
        qp.apertura();
        if (qp.numeroRecord() == 1) {
            rp = qp.successivo();
            percorso_alternativo = rp.leggiStringa("parimg");
        }
        qp.chiusura();
    }

    private String visualizzaImmagine(int artid, int artprodrifid) {
        String icon = "";
        if (!percorso_alternativo.equals("")) {
            File f1 = new File(percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artid, 10, "0") + ".gif");
            File f2 = new File(percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artid, 10, "0") + ".jpg");
            File f3 = new File(percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artid, 10, "0") + ".png");
            if (f1.exists()) {
                icon = percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artid, 10, "0") + ".gif";
            } else if (f2.exists()) {
                icon = percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artid, 10, "0") + ".jpg";

            } else if (f3.exists()) {
                icon = percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artid, 10, "0") + ".png";
            } else if (artprodrifid != -1) {
                File f5 = new File(percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artprodrifid, 10, "0") + ".gif");
                File f6 = new File(percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artprodrifid, 10, "0") + ".jpg");
                File f7 = new File(percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artprodrifid, 10, "0") + ".png");
                if (f5.exists()) {
                    icon = percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artprodrifid, 10, "0") + ".gif";
                } else if (f6.exists()) {
                    icon = percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artprodrifid, 10, "0") + ".jpg";
                } else if (f7.exists()) {
                    icon = percorso_alternativo + File.separator + "art" + Formattazione.formIntero(artprodrifid, 10, "0") + ".png";
                }
            }
        } else {
            if (FunzioniDB.esisteImmagineArticolo(artid)) {
                File f = new File("tmpimg");
                if (!f.exists()) {
                    f.mkdir();
                }

                File[] lf = f.listFiles();
                for (int i = 0; i < lf.length; i++) {
                    if (lf[i].isFile()) {
                        lf[i].delete();
                    }
                }
                FunzioniDB.leggiImmagineArticolo(artid);
                f = new File("tmpimg");
                if (!f.exists()) {
                    f.mkdir();
                }
                lf = f.listFiles();
                for (int i = 0; i < lf.length; i++) {
                    if (lf[i].isFile()) {
                        Debug.output(lf[i].getName());
                        icon = "tmpimg" + File.separator + lf[i].getName();
                        break;
                    }
                }
            } else if (artprodrifid != -1 && FunzioniDB.esisteImmagineArticolo(artprodrifid)) {
                FunzioniDB.leggiImmagineArticolo(artprodrifid);
                File f = new File("tmpimg");
                if (!f.exists()) {
                    f.mkdir();
                }

                File[] lf = f.listFiles();
                for (int i = 0; i < lf.length; i++) {
                    if (lf[i].isFile()) {
                        icon = "tmpimg" + File.separator + lf[i].getName();
                        break;
                    }
                }
            }
        }
        return icon;
    }

    protected ImageIcon createImageIcon(String path, String description) {
        if (path != null && !path.equals("")) {
            return new ImageIcon(path, description);
        } else {
//            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }

    private void mostraScadenze() {
        double t = 0;
        if (vscad == null) {
            vscad = new ArrayList();
        }

        final String[] names
                = {
                    COL_BUTTON1, COL_BUTTON2, COL_DTSCADENZA, COL_IMPORTO
                };
        final Object[][] data = new Object[vscad.size()][names.length];
        for (int i = 0; i < vscad.size(); i++) {
            Record r = vscad.get(i);
            data[i][0] = "";
            data[i][1] = "";
            data[i][2] = (new Data(r.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][3] = Formattazione.formValuta(r.leggiDouble("importo"), 12, 2, 0);
            t += r.leggiDouble("importo");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tabscad, COL_BUTTON1)
                        || col == FunzioniUtilita.getColumnIndex(tabscad, COL_BUTTON2)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tabscad.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tabscad, true);
        tabscad.getColumn(COL_BUTTON1).setPreferredWidth(30);
        tabscad.getColumn(COL_BUTTON2).setPreferredWidth(30);

        TableColumn buttonColumn1 = tabscad.getColumn(COL_BUTTON1);
        TablePopButton bEdit = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_create_white_24dp.png")));
            }
        });

        bEdit.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vscad.get(row);
                DialogGestScadDocPop dsc = new DialogGestScadDocPop(formGen, true, datamov.getDataStringa(), r, 0, 0);
                UtilForm.posRelComponente(dsc, jScrollScad);
                dsc.setVisible(true);

                if (dsc.leggiScelta() == DialogGestScadDocPop.OK) {
                    Record rscris = dsc.leggiScadenza();
                    r.eliminaCampo("data");
                    r.eliminaCampo("importo");
                    r.insElem("data", rscris.leggiStringa("data"));
                    r.insElem("importo", rscris.leggiDouble("importo"));
                    scadmodif = true;
                    mostraScadenze();
                }
            }
        });

        buttonColumn1.setCellRenderer(bEdit);
        buttonColumn1.setCellEditor(bEdit);

        TableColumn buttonColumn2 = tabscad.getColumn(COL_BUTTON2);
        TablePopButton bCanc = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
            }
        });

        bCanc.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                vscad.remove(row);
                scadmodif = true;
                mostraScadenze();
            }
        });

        buttonColumn2.setCellRenderer(bCanc);
        buttonColumn2.setCellEditor(bCanc);

        t = OpValute.arrotondaMat(t, 2);
        totscad.setText(Formattazione.formValuta(t, 12, 2, 0));
        double nettoapag = OpValute.arrotondaMat(Formattazione.estraiDouble(totnetto.getText()) - totpag.getValore() - ivasplitp, 2);
        if (nettoapag != t && totscad.isVisible()) {
            avvisoscad.setVisible(true);
        } else {
            avvisoscad.setVisible(false);
        }

        tabscad.getColumn(COL_BUTTON1).setHeaderRenderer(headerRenderer);
        tabscad.getColumn(COL_BUTTON2).setHeaderRenderer(headerRenderer);
    }

    private void mostraRiepilogoIva() {
        ArrayList<Record> vtmp = new ArrayList();
        Iterator it = hiva.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            Record riva = hiva.get(key);
            Record rx = new Record();
            rx.insElem("cod", key);
            rx.insElem("descr", riva.leggiStringa("descr"));
            rx.insElem("aliquota", riva.leggiDouble("aliquota"));
            rx.insElem("imponibile", riva.leggiDouble("imponibile"));
            rx.insElem("iva", riva.leggiDouble("iva"));
            vtmp.add(rx);
        }

        final String[] names
                = {
                    COL_CODIVA, COL_ALIQUOTA, COL_IMPONIBILE, COL_IMPOSTA
                };
        final Object[][] data = new Object[vtmp.size()][names.length];
        for (int i = 0; i < vtmp.size(); i++) {
            Record r = vtmp.get(i);
            data[i][0] = r.leggiStringa("cod") + " " + r.leggiStringa("descr");
            if (r.leggiDouble("aliquota") > 0) {
                data[i][1] = Formattazione.formValuta(r.leggiDouble("aliquota"), 12, 2, 0);
            } else {
                data[i][1] = "";
            }
            data[i][2] = Formattazione.formValuta(r.leggiDouble("imponibile"), 12, 2, 1);
            data[i][3] = Formattazione.formValuta(r.leggiDouble("iva"), 12, 2, 1);
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                return false;
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tabiva.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tabiva, true);
    }

    private void preparaInsRiga() {
        rigacorr = -1;
        sclicalc1 = 0;
        sclicalc2 = 0;
        sclicalc3 = 0;

        codiva.svuota();
        codiva1.svuota();
        listcesp = null;

        valorestorico.setText("");
        valorefondo.setText("");
        residuostorico.setText("");
        valoreparziale.setText("");
        parzialefondo.setText("");
        residuoparziale.setText("");
        descrart.setText("");
        qta.setText("");
        prezzo.setText("");
        sc1.setText("");
//        sc2.setText("");
//        sc3.setText("");
        importo.setText("");
        imponibilealiq.setText("");
        imponibilenoiva.setText("");
        descraliq.setText("");
        plusvalenza.setText("");
        minusvalenza.setText("");

        codiva.abilita(true);
        codiva1.abilita(true);
        valoreparziale.setEnabled(true);
        imponibilealiq.setEnabled(true);
        imponibilenoiva.setEnabled(true);
        qta.setEnabled(true);
        prezzo.setEnabled(true);
        sc1.setEnabled(true);
//        sc2.setEnabled(true);
//        sc3.setEnabled(true);

        oma = false;
        ivaomaggioId = -1;
    }

    private void mostraCarrello() {
        if (rDoc.leggiIntero("doctipo") == 4) {
            final String[] names
                    = {
                        COL_DTDOC, COL_NDOC, COL_TOTDOC, COL_ACCONTO, COL_LUOGOCONS
                    };
            final Object[][] data = new Object[vdettddt.size()][names.length];

            for (int i = 0; i < vdettddt.size(); i++) {
                Record r = vdettddt.get(i);
                data[i][0] = (new Data(r.leggiStringa("data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                data[i][1] = r.leggiStringa("sezionale") + "/" + r.leggiIntero("numero");
                data[i][2] = Formattazione.formatta(r.leggiDouble("totale"), "###,###,##0.00", FDouble.SEGNO_SX);
                data[i][3] = Formattazione.formatta(r.leggiDouble("acconto"), "###,###,##0.00", FDouble.SEGNO_SX);
                data[i][4] = r.leggiStringa("conscod") + " " + r.leggiStringa("consdescr");
            }

            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    return false;
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };
            tabcarrello.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPop(tabcarrello, true);
        } else {
            ImageIcon iconart = new ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_shopping_cart_black_24dp.png"));
            ImageIcon iconnoncod = new ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_list_black_24dp.png"));
            ImageIcon icondescr = new ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_sms_black_24dp.png"));
            final String[] names
                    = {
                        COL_BUTTON1, COL_BUTTON2, COL_TIPO, COL_CODART, COL_DESCR, COL_COLLI, COL_QTA, COL_PREZZO, COL_SCONTO1,
                        COL_IMPORTO, COL_LOTTO, COL_CODIVA, COL_OMAGGIO
                    };
            final Object[][] data = new Object[vrighe.size()][names.length];
            for (int i = 0; i < vrighe.size(); i++) {
                Record r = vrighe.get(i);
                data[i][0] = "";
                data[i][1] = "";
                if (r.leggiIntero("tiporiga") == 0) {
                    data[i][2] = iconart;
                } else if (r.leggiIntero("tiporiga") == 1) {
                    data[i][2] = iconnoncod;
                } else if (r.leggiIntero("tiporiga") == 2) {
                    data[i][2] = icondescr;
                } else {
                    data[i][2] = "";
                }
                data[i][3] = r.leggiStringa("artcod");
                data[i][4] = r.leggiStringa("artdescr");
//                data[i][5] = r.leggiStringa("artum");
                data[i][5] = "" + r.leggiIntero("colli");
                data[i][6] = Formattazione.formValuta(r.leggiDouble("qta"), 12, 2, 0);
                data[i][7] = Formattazione.formValuta(r.leggiDouble("prezzo"), 12, 2, Formattazione.SEGNO_SX_NEG);
                data[i][8] = Formattazione.formValuta(r.leggiDouble("scart1"), 12, 2, 0);
                data[i][9] = Formattazione.formValuta(r.leggiDouble("importo"), 12, 2, Formattazione.SEGNO_SX_NEG);
                data[i][10] = r.leggiStringa("lotto");
                data[i][11] = r.leggiStringa("ivacod");
                if (r.leggiIntero("oma") == 1) {
                    data[i][12] = new Boolean(true);
                } else {
                    data[i][12] = new Boolean(false);
                }
            }
            final TableCellRenderer ctxt = new TxtCellRenderer(new Font("Dialog", Font.PLAIN, 12), Color.WHITE, Color.black, SwingConstants.LEFT, SwingConstants.TOP, true);

            tabcarrello = new JTable() {
                public TableCellRenderer getCellRenderer(int row, int column) {
                    //                if (column == 3 || column == 4) {
                    //                    return ctxt;
                    //                } else {
                    //                    return super.getCellRenderer(row, column);
                    //                }
                    return super.getCellRenderer(row, column);
                }
            };

            pScrollCarrello.getViewport().removeAll();
            pScrollCarrello.getViewport().add(tabcarrello);
            tabcarrello.revalidate();

            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    if (col == FunzioniUtilita.getColumnIndex(tabcarrello, COL_BUTTON1)
                            || col == FunzioniUtilita.getColumnIndex(tabcarrello, COL_BUTTON2)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };
            tabcarrello.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPop(tabcarrello, true);

            TableColumn buttonColumn1 = tabcarrello.getColumn(COL_BUTTON1);
            TableColumn buttonColumn2 = tabcarrello.getColumn(COL_BUTTON2);
            TablePopButton bCanc = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                public void customize(PopButton button, int row, int column) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                    if(movdocId!=-1)
                    {
                        boolean invsdi = BusinessLogicDocumenti.documentoInviatoSDI(connAz.leggiConnessione(), azienda, esContabile, movdocId);
                        if (invsdi) {
                            button.setEnabled(false);
                        }
                        else 
                        {
                            if(tipologia==0 || tipologia == 2)
                            {
                                button.setEnabled(false);
                            }
                            else
                            {
                                button.setEnabled(true);                        
                            }
                        }
                    }
                    else 
                    {
                        if(tipologia==0 || tipologia == 2)
                        {
                            button.setEnabled(false);
                        }
                        else
                        {
                            button.setEnabled(true);                        
                        }
                    }
                }   
            });

            bCanc.addHandler(new TablePopButton.TableButtonPressedHandler() {
                @Override
                public void onButtonPress(int row, int column) {
                    vrighe.remove(row);

                    String qryupdate = "UPDATE cespiti SET cespdataalien = \"0000-00-00\",cespqtaattu = 0 WHERE cespid = " + cespitidaregistrare.get(row).leggiIntero("cespid");
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();

                    cespitidaregistrare.remove(row);
                    ricalcolaPlusvalenzeMinusvalenze();

                    mostraCarrello();
                    ricalcolaTotale();
                    preparaInsRiga();
                }
            });

            TablePopButton bEdit = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                public void customize(PopButton button, int row, int column) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_create_white_24dp.png")));
                }
            });

            bEdit.addHandler(new TablePopButton.TableButtonPressedHandler() {
                @Override
                public void onButtonPress(int row, int column) {
                    Record r = vrighe.get(row);
                    rigacorr = row;

                    if (tipodoc == 0) 
                    {
                        if(r.leggiIntero("tiporiga")==2)
                        {
                            qta.setEnabled(false);
                            prezzo.setEnabled(false);
                            imponibilealiq.setEnabled(false);
                            codiva.abilita(false);
                            valoreparziale.setEnabled(false);
                            imponibilenoiva.setEnabled(false);
                            codiva1.abilita(false);
                        }
                        else
                        {
                            qta.setEnabled(true);
                            prezzo.setEnabled(true);
                            imponibilealiq.setEnabled(true);
                            codiva.abilita(true);
                            valoreparziale.setEnabled(true);
                            imponibilenoiva.setEnabled(true);
                            codiva1.abilita(true);
                        }
                    }
                    else if(tipodoc == 4)
                    {
                        if(tipologia==0)
                        {
                            prezzo.setEnabled(false);
                            qta.setEnabled(false);
                            imponibilealiq.setEnabled(false);
                            imponibilenoiva.setEnabled(false);
                            codiva.abilita(false);
                            codiva1.abilita(false);
                            valoreparziale.setEnabled(false);
                        }
                        else 
                        {
                            if(tipologia==2)
                            {
                                valoreparziale.setEnabled(false);                                
                                valoreparziale.setEnabled(false);
                                codiva.abilita(false);
                            }
                            else
                            {                        
                                
                                valoreparziale.setEnabled(true);
                                if (codiva.leggiId() == -1) {
                                    codiva.abilita(false);
                                } else {
                                    codiva.abilita(true);
                                }
                            }
                        }
                    }
                    
                    if (r.leggiIntero("ivaid") != -1) {
                        Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", r.leggiIntero("ivaid"), "");
                        codiva.visualizzaDati(ri.leggiIntero("ivaid"), ri.leggiStringa("ivacod"), ri.leggiStringa("ivadescr"));
                        codiva1.svuota();
                        imponibilenoiva.setText("");
                    } else {
                        codiva.svuota();
                        imponibilenoiva.setText("");
                    }

                    listcesp = new ArrayList();
                    
                    if(!r.leggiStringa("lotto").equals(""))
                    {
                        String qry = "SELECT * FROM cespiti WHERE cespordine=\"" + r.leggiStringa("lotto") + "\"";
                        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
                        query.apertura();
                        Record rec = query.successivo();
                        query.chiusura();
                        listcesp.add(rec);

                        if (tipodoc == 0) 
                        {
                            totbene=0.00;
                            totfondo=0.00;
                            double plusvalenzaval=0.00;
                            double minusvalenzaval=0.00;
                            String qryfiglio="SELECT * FROM cespiti WHERE cespcapoid="+rec.leggiIntero("cespid");
                            QueryInterrogazioneClient queryfiglio = new QueryInterrogazioneClient(connAz, qryfiglio);
                            queryfiglio.apertura();
                            Record recfiglio = queryfiglio.successivo();
                            queryfiglio.chiusura();
                            
                            if(r.leggiIntero("tiporiga")==1)
                            {
                                for(int j=0;j<cespitidaregistrare.size();j++)
                                {
                                    Record recf=cespitidaregistrare.get(j);
                                    if(recf.leggiIntero("cespid")==rec.leggiIntero("cespid"))
                                    {
                                        totbene+=recf.leggiDouble("valoreparziale");
                                        totfondo+=recf.leggiDouble("parzialefondo");
                                        plusvalenzaval+=recf.leggiDouble("plusvalenza");
                                        minusvalenzaval+=recf.leggiDouble("minusvalenza");
                                    }
                                                                         
                                    if(recfiglio!=null)
                                    {
                                        if(recfiglio.leggiIntero("cespid")==recf.leggiIntero("cespid"))
                                        {
                                            totbene+=recf.leggiDouble("valoreparziale");
                                            totfondo+=recf.leggiDouble("parzialefondo");
                                            plusvalenzaval+=recf.leggiDouble("plusvalenza");
                                            minusvalenzaval+=recf.leggiDouble("minusvalenza"); 
                                        }
                                    }
                                }
                                
                                if(plusvalenzaval>0)
                                {
                                    plusvalenza.setText(""+plusvalenzaval);
                                }
                                else 
                                {
                                    minusvalenza.setText(""+minusvalenzaval);
                                }
                            }
                        }
                        else
                        {
//                            if(tipologia==1)
//                            {
                                int ggprec=Formattazione.estraiIntero(vnotac.get(0).leggiStringa("datadocman").substring(8,10))-1;
                                String data="";
                                if(ggprec<10)
                                {
                                    data=vnotac.get(0).leggiStringa("datadocman").substring(0,8)+"0"+ggprec;
                                }
                                else
                                {
                                    data=vnotac.get(0).leggiStringa("datadocman").substring(0,8)+ggprec;
                                }
                                   
                                double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rec.leggiIntero("cespid"), esContabile, data);
                                totbene = OpValute.arrotondaMat(vv[1], 2);
                                totfondo = OpValute.arrotondaMat(vv[3], 2);
//                            }
//                            else if(tipologia==0 || tipologia==2)
//                            {
//                                double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rec.leggiIntero("cespid"), esContabile, datamov.getDataStringa());
//                                totbene = OpValute.arrotondaMat(vv[1], 2);
//                                totfondo = OpValute.arrotondaMat(vv[3], 2);
//                            }                           
                        }
                        
                        totresto = totbene - totfondo;

                        if(r.leggiIntero("tiporiga")==2)
                        {
                            valorestorico.setText("");
                            valoreparziale.setText("");
                            valorefondo.setText("");
                            residuostorico.setText("");
                            parzialefondo.setText("");
                            residuoparziale.setText("");
                            plusvalenza.setText("");
                            minusvalenza.setText("");
                        }
                        else
                        {
                            valorestorico.setText(Formattazione.formatta(totbene, "#######0.00", 0));
                            valoreparziale.setText(Formattazione.formatta(totbene, "#######0.00", 0));
                            valorefondo.setText(Formattazione.formatta(totfondo, "#######0.00", 0));
                            residuostorico.setText(Formattazione.formatta(totresto, "#######0.00", 0));
                            calcolaResiduiParziali();
                        }
                    }

                    descrart.setText(r.leggiStringa("artdescr"));
//                    um.setText(r.leggiStringa("artum"));
                    qta.visValuta(r, "qta");
                    double prz = r.leggiDouble("prezzo");
                    prezzo.setText(Formattazione.formValuta(prz, 12, 5, 2));
                    sc1.visValuta(r, "scart1");
//                    sc2.visValuta(r, "scart2");
//                    sc3.visValuta(r, "scart3");
                    sclicalc1 = r.leggiDouble("sccli1");
                    sclicalc2 = r.leggiDouble("sccli2");
                    sclicalc3 = r.leggiDouble("sccli3");
                    imponibilealiq.setText(Formattazione.formValuta(r.leggiDouble("importo"), 12, 2, 2));
                    importo.setText(Formattazione.formValuta(r.leggiDouble("importo"), 12, 2, 2));


                    ivaomaggioId = -1;
                    oma = false;
                    
                    if(!r.leggiStringa("lotto").equals(""))
                    {
                        ricalcolaPlusvalenzeMinusvalenze();
                    }
                    qta.requestFocus();
                }
            });

            buttonColumn1.setCellRenderer(bCanc);

            buttonColumn1.setCellEditor(bCanc);

            buttonColumn2.setCellRenderer(bEdit);

            buttonColumn2.setCellEditor(bEdit);
            tabcarrello.getColumn(COL_BUTTON1).setPreferredWidth(20);
            tabcarrello.getColumn(COL_BUTTON2).setPreferredWidth(20);
            tabcarrello.getColumn(COL_TIPO).setPreferredWidth(20);
            tabcarrello.getColumn(COL_CODART).setPreferredWidth(100);
            tabcarrello.getColumn(COL_DESCR).setPreferredWidth(300);
            tabcarrello.getColumn(COL_COLLI).setPreferredWidth(20);
            tabcarrello.getColumn(COL_QTA).setPreferredWidth(80);
            tabcarrello.getColumn(COL_PREZZO).setPreferredWidth(80);
            tabcarrello.getColumn(COL_SCONTO1).setPreferredWidth(50);
            tabcarrello.getColumn(COL_IMPORTO).setPreferredWidth(80);
            tabcarrello.getColumn(COL_LOTTO).setPreferredWidth(80);
            tabcarrello.getColumn(COL_CODIVA).setPreferredWidth(40);
            tabcarrello.getColumn(COL_OMAGGIO).setPreferredWidth(20);

            tabcarrello.getColumn(COL_BUTTON1).setHeaderRenderer(headerRenderer);
            tabcarrello.getColumn(COL_BUTTON2).setHeaderRenderer(headerRenderer);
        }
    }

    class CambioCliente implements ExecGestioni {

        public void lancia() {
            if (cliente.leggiId() != -1) {
                rcli = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "clienti", "cliid", cliente.leggiId(), "");
                if (rcli.leggiIntero("clipagid") == -1) {
                    int riss = JOptionPane.showConfirmDialog(formGen, "Il cliente non ha la forma di pagamento associata.\nModificare i parametri del cliente?",
                            "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                            new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                    if (riss == JOptionPane.YES_OPTION) {
                        DialogClientiBasePop d = new DialogClientiBasePop(formGen, true, connAz, connCom, azienda, esContabile, "", cliente.leggiId(), 1);
                        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                        d.setVisible(true);
                        cliente.visualizzaDati(cliente.leggiId(), cliente.leggiCodice());
                        cliente.caricaDatiCorrelati();
                    } else {
                        cliente.svuota();
                        consegna.impostaCliente(-1);
                        //consegna.svuota();
                        return;
                    }
                } else {
                    int aid = rcli.leggiIntero("clianagid");
                    Record ra = BusinessLogicArchiviBase.leggiDatiAnagrafica(connCom.leggiConnessione(), aid);
                    nomecli.setText(ra.leggiStringa("nome"));
                    nomecli2.setText(ra.leggiStringa("nome"));
                    indcli.setText(ra.leggiStringa("indirizzo"));
                    loccli.setText(ra.leggiStringa("localitacompleta"));
                    pivacli.setText(ra.leggiStringa("partitaiva"));
                    cfcli.setText(ra.leggiStringa("codicefiscale"));
                    if (tipodoc == 0 || tipodoc == 1) {
                        if (rcli.leggiIntero("clientepubblico") == 0 || rcli.leggiIntero("clientepubblico") == 3) {
                            tipocli.setText("no fattura elettronica");
                            int riss = JOptionPane.showConfirmDialog(formGen, "Il cliente non prevede l'emissione della fatture elettronica.\nModificare i parametri del cliente?",
                                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                            if (riss == JOptionPane.YES_OPTION) {
                                DialogClientiBasePop d = new DialogClientiBasePop(formGen, true, connAz, connCom, azienda, esContabile, "", cliente.leggiId(), 1);
                                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                                d.setVisible(true);
                                cliente.visualizzaDati(cliente.leggiId(), cliente.leggiCodice());
                                cliente.caricaDatiCorrelati();
                            } else {
                                cliente.svuota();
                                return;
                            }
                        } else if (rcli.leggiIntero("clientepubblico") == 1) {
                            tipocli.setText("cliente con split payment");
                        } else if (rcli.leggiIntero("clientepubblico") == 2) {
                            tipocli.setText("cliente senza split payment");
                        } else {
                            tipocli.setText("cliente privato");
                        }
                    }

                    String qry = "SELECT anagemailpec FROM anagr WHERE anagid=" + rcli.leggiIntero("clianagid");
                    QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connCom, qry);
                    qc.apertura();
                    Record rc = qc.successivo();
                    qc.chiusura();

                    if (tipodoc == 0 || tipodoc == 1) {
                        if ((rcli.leggiIntero("clienteazi") == 0 || rcli.leggiIntero("clienteazi") == 2) && rcli.leggiStringa("clicoduffpa").equals("") && rc.leggiStringa("anagemailpec").equals("")) {
                            int riss = JOptionPane.showConfirmDialog(formGen, "Codice di interscambio e indirizzo PEC assenti.\n Modificare i parametri del cliente?",
                                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                            if (riss == JOptionPane.YES_OPTION) {
                                DialogClientiBasePop d = new DialogClientiBasePop(formGen, true, connAz, connCom, azienda, esContabile, "", cliente.leggiId(), 1);
                                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                                d.setVisible(true);
                                cliente.visualizzaDati(cliente.leggiId(), cliente.leggiCodice());
                                cliente.caricaDatiCorrelati();
                            } else {
                                return;
                            }
                        }

                        if (rcli.leggiIntero("clienteazi") == 1 && rcli.leggiStringa("clicoduffpa").equals("")) {
                            int riss = JOptionPane.showConfirmDialog(formGen, "Codice di interscambio obbligatorio in caso di ente pubblico.\n Modificare i parametri del cliente?",
                                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                            if (riss == JOptionPane.YES_OPTION) {
                                DialogClientiBasePop d = new DialogClientiBasePop(formGen, true, connAz, connCom, azienda, esContabile, "", cliente.leggiId(), 1);
                                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                                d.setVisible(true);
                                cliente.visualizzaDati(cliente.leggiId(), cliente.leggiCodice());
                                cliente.caricaDatiCorrelati();
                            } else {
                                cliente.svuota();
                                return;
                            }
                        }

                        if (rcli.leggiIntero("clienteazi") == 2 && rcli.leggiIntero("clientepubblico") != 3 && rcli.leggiIntero("clientepubblico") != 0) {
                            int riss = JOptionPane.showConfirmDialog(formGen, "Parametri incongruenti su cliente.\nM Modificare i parametri del cliente?",
                                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                            if (riss == JOptionPane.YES_OPTION) {
                                DialogClientiBasePop d = new DialogClientiBasePop(formGen, true, connAz, connCom, azienda, esContabile, "", cliente.leggiId(), 1);
                                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                                d.setVisible(true);
                                cliente.visualizzaDati(cliente.leggiId(), cliente.leggiCodice());
                                cliente.caricaDatiCorrelati();
                            } else {
                                cliente.svuota();
                                return;
                            }
                        }
                    }

                    if (!mostradoc) {
                        if (!pagmodif) {
                            if (rcli.leggiIntero("clipagid") != -1) {
                                Record rpag = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "pagamenti", "pagid", rcli.leggiIntero("clipagid"), "");
                                pagamento.visualizzaDati(rpag.leggiIntero("pagid"), rpag.leggiStringa("pagcod"), rpag.leggiStringa("pagdescr"));
                            } else {
                                pagamento.svuota();
                            }
                        }

                        if (rcli.leggiIntero("clidestinaz") == 1) {
                            String qry2 = "SELECT * FROM consegne WHERE conscliid = " + cliente.leggiId();
                            QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qry2);
                            qd.apertura();
                            if (qd.numeroRecord() == 1) {
                                Record rd = qd.successivo();
                                consegna.visualizzaDati(rd.leggiIntero("consid"), rd.leggiStringa("conscod"), rd.leggiStringa("consdenom"));
                            }
                            qd.chiusura();
                        }
                        listinoId = rcli.leggiIntero("clilistinoid");
                    }
                    consegna.impostaCliente(cliente.leggiId());
                }
            } else {
                rcli = null;
                nomecli.setText("");
                nomecli2.setText("");
                indcli.setText("");
                loccli.setText("");
                pivacli.setText("");
                tipocli.setText("");
                cfcli.setText("");
                consegna.svuota();
                consegna.impostaCliente(-1);
                pagamento.svuota();
                listinoId = -1;
            }
        }
    }

    class CambioConsegna implements ExecGestioni {

        public void lancia() {
            if (consegna.leggiId() != -1) {
                Record rdest = BusinessLogicArchiviBase.leggiDatiConsegna(connAz.leggiConnessione(), connCom.leggiConnessione(), consegna.leggiId());
                descrcons.setText(rdest.leggiStringa("consdenom"));
                indcons.setText(rdest.leggiStringa("consind") + " " + rdest.leggiStringa("consnumciv"));
                loccons.setText(rdest.leggiStringa("consloc") + " " + rdest.leggiStringa("conscomune") + " " + rdest.leggiStringa("consprov"));
            } else {
                descrcons.setText("");
                indcons.setText("");
                loccons.setText("");
            }
        }
    }

    class CambioPagamento implements ExecGestioni {

        public void lancia() {
            if (pagprecId != -1 && !totimp.getText().equals("0,00")) {
                pagmodif = true;
                pagprecId = pagamento.leggiId();
                if (pagmodif) {
                    int riss = JOptionPane.showConfirmDialog(formGen, "E' cambiata la forma di pagamento. Vuoi ricalcolare le scadenze?",
                            "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                    if (riss == JOptionPane.YES_OPTION) {
                        scadmodif = false;
                        ricalcolaTotale();
                    } else {
                        ptab.setSelectedIndex(TAB_RIEPILOGO);
                        return;
                    }
                }
            } else if (pagprecId == -1) {
                pagprecId = pagamento.leggiId();
            }
        }
    }

    class CambioIvaCassaPrevidenza implements ExecGestioni {

        public void lancia() {
            ricalcolaTotale();
        }
    }

    class CambioVettore implements ExecGestioni {

        public void lancia() {
            if (vettore.leggiId() != -1) {
                Record rvett = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "vettori", "vettid", vettore.leggiId(), "");
                if (!rvett.leggiStringa("vetttarga").equals("")) {
                    targa.setText(rvett.leggiStringa("vetttarga"));
                }
            }
        }
    }

    private void mostraDocumento(int idmov) {
        modo = EnvJazz.AGG;
        mostradoc = true;
        scadmodif = true;
        rigacorr = -1;
        stato.setText("MODIFICA");
        datamov.setEnabled(false);
        buttonModNum.setEnabled(true);
        String qry = "SELECT * FROM movimentidoc" + esmov + " LEFT JOIN testatadoc" + esmov
                + " ON movdocid = testamovdocid "
                + " LEFT JOIN piededoc" + esmov + " ON movdocid = piemovdocid"
                + " WHERE movdocid = " + idmov;
        QueryInterrogazioneClient qm = new QueryInterrogazioneClient(connAz, qry);
        qm.apertura();
        Record rm = qm.successivo();
        qm.chiusura();

        if (idmov == movdocId) {
            numero.setText("" + rm.leggiIntero("movdocnumft"));
            String dt = rm.leggiStringa("movdocdtft");
            datamov.setText((new Data(dt, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
            datarif.visData(rm, "movdocdtre");
            numrif.setText(rm.leggiStringa("movdocnumre"));
        }

        if (!rm.nullo("testaanagid")) {
            String qry2 = "SELECT * FROM clienti WHERE cliid = " + rm.leggiIntero("testaanagid");
            QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connAz, qry2);
            qc.apertura();
            Record rc = qc.successivo();
            qc.chiusura();

            if (rc != null) {
                cliente.visualizzaDati(rc.leggiIntero("cliid"), rc.leggiStringa("clicod"));
            } else {
                cliente.svuota();
            }
        } else {
            cliente.svuota();
        }

        if (rm.leggiIntero("testadestid") != -1) {
            String qry2 = "SELECT * FROM consegne WHERE consid = " + rm.leggiIntero("testadestid");
            QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qry2);
            qd.apertura();
            if (qd.numeroRecord() > 0) {
                Record rd = qd.successivo();
                consegna.visualizzaDati(rd.leggiIntero("consid"), rd.leggiStringa("conscod"), rd.leggiStringa("consdenom"));
            }
            qd.chiusura();
        } else {
            consegna.svuota();
        }

        if (rm.leggiIntero("testapagid") != -1) {
            Record rpag = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "pagamenti", "pagid", rm.leggiIntero("testapagid"), "");
            pagamento.visualizzaDati(rpag.leggiIntero("pagid"), rpag.leggiStringa("pagcod"), rpag.leggiStringa("pagdescr"));
        }

        vrighe = new ArrayList();
        String dtcons = "";

        String qryncespiti = "SELECT * FROM cespitiven WHERE cespnumven=" + rm.leggiIntero("movdocnumft")
                + " AND cespdataven=\"" + rm.leggiStringa("movdocdtft") + "\" AND cespesercv=\"" + esmov + "\"";
        QueryInterrogazioneClient querycespitinn = new QueryInterrogazioneClient(connAz, qryncespiti);
        querycespitinn.apertura();
        int nrecordcespiti = 0;
        Record rcespiten = querycespitinn.successivo();        
//        tipologia= rcespiten.leggiIntero("cespvtiponc");
        while (rcespiten != null) {
            nrecordcespiti++;            
            rcespiten = querycespitinn.successivo();
        }
        querycespitinn.chiusura();

        if (rDoc.leggiIntero("doctipo") == 4) {
            String qry2 = "SELECT * FROM corpodocmag" + esmov + " INNER JOIN movimentidoc" + esmov + " ON corpomagmovdocid = movdocid LEFT JOIN articoli ON corpomagartid = artid "
                    + " WHERE movdocnumint2 > 0 AND movdocnumft = " + rm.leggiIntero("movdocnumft") + " AND movdocdtft = \"" + rm.leggiStringa("movdocdtft") + "\" AND movdocsufft = \"" + rm.leggiStringa("movdocsufft") + "\"";
            QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connAz, qry2);
            qc.apertura();
            for (int i = 0; i < qc.numeroRecord(); i++) {
                Record rc = qc.successivo();
                if (rc.leggiIntero("corpomagartid") > 0) {
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 0);
                    rx.insElem("artid", rc.leggiIntero("corpomagartid"));
                    rx.insElem("artcod", rc.leggiStringa("artcod"));
                    rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
                    rx.insElem("artum", rc.leggiStringa("corpomagum"));
                    rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                    rx.insElem("ivaid", rc.leggiIntero("corpomagivaid"));
                    Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", rc.leggiIntero("corpomagivaid"), "");
                    rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                    rx.insElem("colli", rc.leggiIntero("corpomagcolli"));
                    rx.insElem("qta", rc.leggiDouble("corpomagqta"));
                    rx.insElem("prezzo", rc.leggiDouble("corpomagprz"));
                    rx.insElem("scart1", rc.leggiDouble("corpomagartsc1"));
                    rx.insElem("scart2", rc.leggiDouble("corpomagartsc2"));
                    rx.insElem("scart3", rc.leggiDouble("corpomagartsc3"));
                    rx.insElem("sccli1", rc.leggiDouble("corpomagcfsc1"));
                    rx.insElem("sccli2", rc.leggiDouble("corpomagcfsc2"));
                    rx.insElem("sccli3", rc.leggiDouble("corpomagcfsc3"));
                    rx.insElem("lordo", rc.leggiDouble("corpomaglordo"));
                    rx.insElem("importo", rc.leggiDouble("corpomagnetto"));
                    rx.insElem("esclsctestata", rc.leggiDouble("corpomagquotaprovg"));
                    if (ri.leggiIntero("ivagest") == 1) {
                        rx.insElem("oma", 1);
                    } else {
                        rx.insElem("oma", 0);
                    }
                    Record rcfe = BusinessLogicDocumenti.leggiCorpoFatturaElettronica(connAz.leggiConnessione(), esmov, movdocId, rc.leggiIntero("corpomagid"));
                    if (rcfe != null) {
                        rx.insElem("serviziodatai", rcfe.leggiStringa("cfeserviziodatai"));
                        rx.insElem("serviziodataf", rcfe.leggiStringa("cfeserviziodataf"));
                    } else {
                        rx.insElem("serviziodatai", "0000-00-00");
                        rx.insElem("serviziodataf", "0000-00-00");
                    }

                    String qry3 = "SELECT * FROM relrighemagdoc WHERE rlrrigadestescont = \"" + esmov + "\" AND rlrrigadestid = " + rc.leggiIntero("corpomagid");
                    QueryInterrogazioneClient qrel = new QueryInterrogazioneClient(connAz, qry3);
                    qrel.apertura();
                    if (qrel.numeroRecord() > 0) {
                        Record rrel = qrel.successivo();
                        rx.insElem("rigaorigid", rrel.leggiIntero("rlrrigaorigid"));
                        rx.insElem("rigaorigescont", rrel.leggiStringa("rlrrigaorigescont"));
                    }
                    qrel.chiusura();
                    vrighe.add(rx);
                } else if (rc.leggiIntero("corpomagartid") == -1) {
                    // non codificato
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 1);
                    rx.insElem("artid", (int) -1);
                    rx.insElem("artcod", "");
                    rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
                    rx.insElem("artum", rc.leggiStringa("corpomagum"));
                    rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                    rx.insElem("ivaid", rc.leggiIntero("corpomagivaid"));
                    Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", rc.leggiIntero("corpomagivaid"), "");
                    rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                    rx.insElem("colli", rc.leggiIntero("corpomagcolli"));
                    rx.insElem("qta", rc.leggiDouble("corpomagqta"));
                    rx.insElem("prezzo", rc.leggiDouble("corpomagprz"));
                    rx.insElem("scart1", rc.leggiDouble("corpomagartsc1"));
                    rx.insElem("scart2", rc.leggiDouble("corpomagartsc2"));
                    rx.insElem("scart3", rc.leggiDouble("corpomagartsc3"));
                    rx.insElem("sccli1", rc.leggiDouble("corpomagcfsc1"));
                    rx.insElem("sccli2", rc.leggiDouble("corpomagcfsc2"));
                    rx.insElem("sccli3", rc.leggiDouble("corpomagcfsc3"));
                    rx.insElem("lordo", rc.leggiDouble("corpomaglordo"));
                    rx.insElem("importo", rc.leggiDouble("corpomagnetto"));
                    rx.insElem("esclsctestata", rc.leggiDouble("corpomagquotaprovg"));
                    rx.insElem("oma", 0);
                    rx.insElem("serviziodatai", "0000-00-00");
                    rx.insElem("serviziodataf", "0000-00-00");
                    vrighe.add(rx);
                } else {
                    // descrittivo
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 2);
                    rx.insElem("artid", (int) -1);
                    rx.insElem("artcod", "");
                    rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
                    rx.insElem("artum", "");
                    rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                    rx.insElem("ivaid", (int) -1);
                    rx.insElem("ivacod", "");
                    rx.insElem("colli", (int) 0);
                    rx.insElem("qta", (double) 0);
                    rx.insElem("prezzo", (double) 0);
                    rx.insElem("scart1", (double) 0);
                    rx.insElem("scart2", (double) 0);
                    rx.insElem("scart3", (double) 0);
                    rx.insElem("sccli1", (double) 0);
                    rx.insElem("sccli2", (double) 0);
                    rx.insElem("sccli3", (double) 0);
                    rx.insElem("lordo", (double) 0);
                    rx.insElem("importo", (double) 0);
                    rx.insElem("esclsctestata", (double) 0);
                    rx.insElem("oma", 0);
                    rx.insElem("serviziodatai", "0000-00-00");
                    rx.insElem("serviziodataf", "0000-00-00");
                    vrighe.add(rx);
                }

                if (!rc.leggiStringa("corpomagdtcons").equals("0000-00-00")) {
                    dtcons = rc.leggiStringa("corpomagdtcons");
                }
            }
            qc.chiusura();

            String esprec = BusinessLogicAmbiente.leggiEsercizioPrecedente(EnvJazz.connAmbiente.leggiConnessione(), azienda, esmov);
            if (!esprec.equals("")) {
                String qry3 = "SELECT * FROM corpodocmag" + esprec + " INNER JOIN movimentidoc" + esprec + " ON corpomagmovdocid = movdocid LEFT JOIN articoli ON corpomagartid = artid "
                        + " WHERE movdocnumint2 > 0 AND movdocnumft = " + rm.leggiIntero("movdocnumft") + " AND movdocdtft = \"" + rm.leggiStringa("movdocdtft") + "\" AND movdocsufft = \"" + rm.leggiStringa("movdocsufft") + "\"";
                qc = new QueryInterrogazioneClient(connAz, qry3);
                qc.apertura();
                for (int i = 0; i < qc.numeroRecord(); i++) {
                    Record rc = qc.successivo();
                    if (rc.leggiIntero("corpomagartid") > 0) {
                        Record rx = new Record();
                        rx.insElem("tiporiga", (int) 0);
                        rx.insElem("artid", rc.leggiIntero("corpomagartid"));
                        rx.insElem("artcod", rc.leggiStringa("artcod"));
                        rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
                        rx.insElem("artum", rc.leggiStringa("corpomagum"));
                        rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                        rx.insElem("ivaid", rc.leggiIntero("corpomagivaid"));
                        Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", rc.leggiIntero("corpomagivaid"), "");
                        rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                        rx.insElem("colli", rc.leggiIntero("corpomagcolli"));
                        rx.insElem("qta", rc.leggiDouble("corpomagqta"));
                        rx.insElem("prezzo", rc.leggiDouble("corpomagprz"));
                        rx.insElem("scart1", rc.leggiDouble("corpomagartsc1"));
                        rx.insElem("scart2", rc.leggiDouble("corpomagartsc2"));
                        rx.insElem("scart3", rc.leggiDouble("corpomagartsc3"));
                        rx.insElem("sccli1", rc.leggiDouble("corpomagcfsc1"));
                        rx.insElem("sccli2", rc.leggiDouble("corpomagcfsc2"));
                        rx.insElem("sccli3", rc.leggiDouble("corpomagcfsc3"));
                        rx.insElem("lordo", rc.leggiDouble("corpomaglordo"));
                        rx.insElem("importo", rc.leggiDouble("corpomagnetto"));
                        rx.insElem("esclsctestata", rc.leggiDouble("corpomagquotaprovg"));
                        if (ri.leggiIntero("ivagest") == 1) {
                            rx.insElem("oma", 1);
                        } else {
                            rx.insElem("oma", 0);
                        }

                        Record rcfe = BusinessLogicDocumenti.leggiCorpoFatturaElettronica(connAz.leggiConnessione(), esmov, movdocId, rc.leggiIntero("corpomagid"));
                        if (rcfe != null) {
                            rx.insElem("serviziodatai", rcfe.leggiStringa("cfeserviziodatai"));
                            rx.insElem("serviziodataf", rcfe.leggiStringa("cfeserviziodataf"));
                        } else {
                            rx.insElem("serviziodatai", "0000-00-00");
                            rx.insElem("serviziodataf", "0000-00-00");
                        }

                        String qryrighe = "SELECT * FROM relrighemagdoc WHERE rlrrigadestescont = \"" + esmov + "\" AND rlrrigadestid = " + rc.leggiIntero("corpomagid");
                        QueryInterrogazioneClient qrel = new QueryInterrogazioneClient(connAz, qryrighe);
                        qrel.apertura();
                        if (qrel.numeroRecord() > 0) {
                            Record rrel = qrel.successivo();
                            rx.insElem("rigaorigid", rrel.leggiIntero("rlrrigaorigid"));
                            rx.insElem("rigaorigescont", rrel.leggiStringa("rlrrigaorigescont"));
                        }
                        qrel.chiusura();
                        vrighe.add(rx);
                    } else if (rc.leggiIntero("corpomagartid") == -1) {
                        // non codificato
                        Record rx = new Record();
                        rx.insElem("tiporiga", (int) 1);
                        rx.insElem("artid", (int) -1);
                        rx.insElem("artcod", "");
                        rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
                        rx.insElem("artum", rc.leggiStringa("corpomagum"));
                        rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                        rx.insElem("ivaid", rc.leggiIntero("corpomagivaid"));
                        Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", rc.leggiIntero("corpomagivaid"), "");
                        rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                        rx.insElem("colli", rc.leggiIntero("corpomagcolli"));
                        rx.insElem("qta", rc.leggiDouble("corpomagqta"));
                        rx.insElem("prezzo", rc.leggiDouble("corpomagprz"));
                        rx.insElem("scart1", rc.leggiDouble("corpomagartsc1"));
                        rx.insElem("scart2", rc.leggiDouble("corpomagartsc2"));
                        rx.insElem("scart3", rc.leggiDouble("corpomagartsc3"));
                        rx.insElem("sccli1", rc.leggiDouble("corpomagcfsc1"));
                        rx.insElem("sccli2", rc.leggiDouble("corpomagcfsc2"));
                        rx.insElem("sccli3", rc.leggiDouble("corpomagcfsc3"));
                        rx.insElem("lordo", rc.leggiDouble("corpomaglordo"));
                        rx.insElem("importo", rc.leggiDouble("corpomagnetto"));
                        rx.insElem("esclsctestata", rc.leggiDouble("corpomagquotaprovg"));
                        rx.insElem("oma", 0);
                        rx.insElem("serviziodatai", "0000-00-00");
                        rx.insElem("serviziodataf", "0000-00-00");
                        vrighe.add(rx);
                    } else {
                        // descrittivo
                        Record rx = new Record();
                        rx.insElem("tiporiga", (int) 2);
                        rx.insElem("artid", (int) -1);
                        rx.insElem("artcod", "");
                        rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
                        rx.insElem("artum", "");
                        rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                        rx.insElem("ivaid", (int) -1);
                        rx.insElem("ivacod", "");
                        rx.insElem("colli", (int) 0);
                        rx.insElem("qta", (double) 0);
                        rx.insElem("prezzo", (double) 0);
                        rx.insElem("scart1", (double) 0);
                        rx.insElem("scart2", (double) 0);
                        rx.insElem("scart3", (double) 0);
                        rx.insElem("sccli1", (double) 0);
                        rx.insElem("sccli2", (double) 0);
                        rx.insElem("sccli3", (double) 0);
                        rx.insElem("lordo", (double) 0);
                        rx.insElem("importo", (double) 0);
                        rx.insElem("esclsctestata", (double) 0);
                        rx.insElem("oma", 0);
                        rx.insElem("serviziodatai", "0000-00-00");
                        rx.insElem("serviziodataf", "0000-00-00");
                        vrighe.add(rx);
                    }

                    if (!rc.leggiStringa("corpomagdtcons").equals("0000-00-00")) {
                        dtcons = rc.leggiStringa("corpomagdtcons");
                    }
                }
                qc.chiusura();
            }
        } else {
            String qry2 = "SELECT * FROM corpodocmag" + esmov + " LEFT JOIN articoli ON corpomagartid = artid "
                    + " WHERE corpomagmovdocid = " + idmov + " ORDER BY corpomagriga";
            QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connAz, qry2);
            qc.apertura();
            for (int i = 0; i < qc.numeroRecord(); i++) {
                Record rc = qc.successivo();
                if (rc.leggiIntero("corpomagartid") > 0) {
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 0);
                    rx.insElem("artid", rc.leggiIntero("corpomagartid"));
                    rx.insElem("artcod", rc.leggiStringa("artcod"));
                    rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
//                    rx.insElem("artum", rc.leggiStringa("corpomagum"));
                    rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                    rx.insElem("ivaid", rc.leggiIntero("corpomagivaid"));
                    Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", rc.leggiIntero("corpomagivaid"), "");
                    rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                    rx.insElem("colli", rc.leggiIntero("corpomagcolli"));
                    rx.insElem("qta", rc.leggiDouble("corpomagqta"));
                    rx.insElem("prezzo", rc.leggiDouble("corpomagprz"));
                    rx.insElem("scart1", rc.leggiDouble("corpomagartsc1"));
                    rx.insElem("scart2", rc.leggiDouble("corpomagartsc2"));
                    rx.insElem("scart3", rc.leggiDouble("corpomagartsc3"));
                    rx.insElem("sccli1", rc.leggiDouble("corpomagcfsc1"));
                    rx.insElem("sccli2", rc.leggiDouble("corpomagcfsc2"));
                    rx.insElem("sccli3", rc.leggiDouble("corpomagcfsc3"));
                    rx.insElem("lordo", rc.leggiDouble("corpomaglordo"));
                    rx.insElem("importo", rc.leggiDouble("corpomagnetto"));
                    rx.insElem("esclsctestata", rc.leggiDouble("corpomagquotaprovg"));
                    if (ri.leggiIntero("ivagest") == 1) {
                        rx.insElem("oma", 1);
                    } else {
                        rx.insElem("oma", 0);
                    }

                    Record rcfe = BusinessLogicDocumenti.leggiCorpoFatturaElettronica(connAz.leggiConnessione(), esmov, movdocId, rc.leggiIntero("corpomagid"));
                    if (rcfe != null) {
                        rx.insElem("serviziodatai", rcfe.leggiStringa("cfeserviziodatai"));
                        rx.insElem("serviziodataf", rcfe.leggiStringa("cfeserviziodataf"));
                    } else {
                        rx.insElem("serviziodatai", "0000-00-00");
                        rx.insElem("serviziodataf", "0000-00-00");
                    }

                    String qry3 = "SELECT * FROM relrighemagdoc WHERE rlrrigadestescont = \"" + esmov + "\" AND rlrrigadestid = " + rc.leggiIntero("corpomagid");
                    QueryInterrogazioneClient qrel = new QueryInterrogazioneClient(connAz, qry3);
                    qrel.apertura();
                    if (qrel.numeroRecord() > 0) {
                        Record rrel = qrel.successivo();
                        rx.insElem("rigaorigid", rrel.leggiIntero("rlrrigaorigid"));
                        rx.insElem("rigaorigescont", rrel.leggiStringa("rlrrigaorigescont"));
                    }
                    qrel.chiusura();
                    vrighe.add(rx);
                } else if (rc.leggiIntero("corpomagartid") == -1) {
                    // non codificato
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 1);
                    rx.insElem("artid", (int) -1);
                    rx.insElem("artcod", "");
                    rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
//                    rx.insElem("artum", rc.leggiStringa("corpomagum"));
                    rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                    rx.insElem("ivaid", rc.leggiIntero("corpomagivaid"));
                    Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", rc.leggiIntero("corpomagivaid"), "");
                    rx.insElem("ivacod", ri.leggiStringa("ivacod"));
                    rx.insElem("colli", rc.leggiIntero("corpomagcolli"));
                    rx.insElem("qta", rc.leggiDouble("corpomagqta"));
                    rx.insElem("prezzo", rc.leggiDouble("corpomagprz"));
                    rx.insElem("scart1", rc.leggiDouble("corpomagartsc1"));
                    rx.insElem("scart2", rc.leggiDouble("corpomagartsc2"));
                    rx.insElem("scart3", rc.leggiDouble("corpomagartsc3"));
                    rx.insElem("sccli1", rc.leggiDouble("corpomagcfsc1"));
                    rx.insElem("sccli2", rc.leggiDouble("corpomagcfsc2"));
                    rx.insElem("sccli3", rc.leggiDouble("corpomagcfsc3"));
                    rx.insElem("lordo", rc.leggiDouble("corpomaglordo"));
                    rx.insElem("importo", rc.leggiDouble("corpomagnetto"));
                    rx.insElem("esclsctestata", rc.leggiDouble("corpomagquotaprovg"));
                    rx.insElem("oma", 0);
                    rx.insElem("serviziodatai", "0000-00-00");
                    rx.insElem("serviziodataf", "0000-00-00");
                    vrighe.add(rx);

                    String qrycespiteven = "SELECT cespvcespid,vconsistenzac,vfondoc,vtotbenec,vtotfondoc,vplusc,cespqtav,cespimpvend FROM cespitiven WHERE cespnumven=" + rm.leggiIntero("movdocnumft")
                            + " AND cespdataven=\"" + rm.leggiStringa("movdocdtft") + "\" AND cespesercv=\"" + esmov + "\" AND cespnrigav=" + i;
                    QueryInterrogazioneClient querycespitiven = new QueryInterrogazioneClient(connAz, qrycespiteven);
                    querycespitiven.apertura();
                    Record rcespiteven = querycespitiven.successivo();
                    querycespitiven.chiusura();

                    if (rcespiteven != null) {
                        String qrycespite = "SELECT * FROM cespiti LEFT JOIN categoriebs ON cespcategid=catbsid WHERE cespid=" + rcespiteven.leggiIntero("cespvcespid");
                        QueryInterrogazioneClient querycespiti = new QueryInterrogazioneClient(connAz, qrycespite);
                        querycespiti.apertura();
                        Record rcespitesalvato = querycespiti.successivo();
                        querycespiti.chiusura();

                        rcespitesalvato.insElem("valoreparziale", rcespiteven.leggiDouble("vconsistenzac"));
                        rcespitesalvato.insElem("parzialefondo", rcespiteven.leggiDouble("vfondoc"));
                        rcespitesalvato.insElem("valorestorico", rcespiteven.leggiDouble("vtotbenec"));
                        rcespitesalvato.insElem("valorefondo", rcespiteven.leggiDouble("vtotfondoc"));
                        if (rcespiteven.leggiDouble("vplusc") < 0) {
                            rcespitesalvato.insElem("minusvalenza", rcespiteven.leggiDouble("vplusc") * (-1));
                            rcespitesalvato.insElem("plusvalenza", 0.00);
                        } else {
                            rcespitesalvato.insElem("minusvalenza", 0.00);
                            rcespitesalvato.insElem("plusvalenza", rcespiteven.leggiDouble("vplusc"));//minusvalenza = plusvalenza negativa
                        }
                        rcespitesalvato.insElem("minusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcminusid"));
                        rcespitesalvato.insElem("plusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcplusid"));
                        rcespitesalvato.insElem("immobilizzazionepdc", rcespitesalvato.leggiIntero("catbsimmpdcid"));
                        rcespitesalvato.insElem("fondopdc", rcespitesalvato.leggiIntero("catbsfondopdcid"));
                        rcespitesalvato.insElem("qta", rcespiteven.leggiDouble("cespqtav"));
                        rcespitesalvato.insElem("importo", rcespiteven.leggiDouble("cespimpvend"));
                        cespitidaregistrare.add(rcespitesalvato);
                    } else {
                        Record rcespitesalvato = new Record();
                        rcespitesalvato.insElem("cespid", -1);
                        cespitidaregistrare.add(rcespitesalvato);
                    }
                } else if (rc.leggiIntero("corpomagartid") == -2 && nrecordcespiti > 1) {
                    // descrittivo
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 2);
                    rx.insElem("artid", (int) -1);
                    rx.insElem("artcod", "");
                    rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
//                    rx.insElem("artum", "");
                    rx.insElem("lotto", rc.leggiStringa("corpomaglotto"));
                    rx.insElem("ivaid", (int) -1);
                    rx.insElem("ivacod", "");
                    rx.insElem("colli", (int) 0);
                    rx.insElem("qta", (double) 0);
                    rx.insElem("prezzo", (double) 0);
                    rx.insElem("scart1", (double) 0);
                    rx.insElem("scart2", (double) 0);
                    rx.insElem("scart3", (double) 0);
                    rx.insElem("sccli1", (double) 0);
                    rx.insElem("sccli2", (double) 0);
                    rx.insElem("sccli3", (double) 0);
                    rx.insElem("lordo", (double) 0);
                    rx.insElem("importo", (double) 0);
                    rx.insElem("esclsctestata", (double) 0);
                    rx.insElem("lotto", "");
                    rx.insElem("oma", 0);
                    rx.insElem("serviziodatai", "0000-00-00");
                    rx.insElem("serviziodataf", "0000-00-00");
                    vrighe.add(rx);

                    String qrycespiteven = "SELECT cespvcespid,vconsistenzac,vfondoc,vtotbenec,vtotfondoc,vplusc,cespqtav,cespimpvend FROM cespitiven WHERE cespnumven=" + rm.leggiIntero("movdocnumft")
                            + " AND cespdataven=\"" + rm.leggiStringa("movdocdtft") + "\" AND cespesercv=\"" + esmov + "\" AND cespnrigav=" + i;
                    QueryInterrogazioneClient querycespitiven = new QueryInterrogazioneClient(connAz, qrycespiteven);
                    querycespitiven.apertura();
                    Record rcespiteven = querycespitiven.successivo();
                    querycespitiven.chiusura();

                    if (rcespiteven != null) {
                        String qrycespite = "SELECT * FROM cespiti LEFT JOIN categoriebs ON cespcategid=catbsid WHERE cespid=" + rcespiteven.leggiIntero("cespvcespid");
                        QueryInterrogazioneClient querycespiti = new QueryInterrogazioneClient(connAz, qrycespite);
                        querycespiti.apertura();
                        Record rcespitesalvato = querycespiti.successivo();
                        querycespiti.chiusura();

                        rcespitesalvato.insElem("valoreparziale", rcespiteven.leggiDouble("vconsistenzac"));
                        rcespitesalvato.insElem("parzialefondo", rcespiteven.leggiDouble("vfondoc"));
                        rcespitesalvato.insElem("valorestorico", rcespiteven.leggiDouble("vtotbenec"));
                        rcespitesalvato.insElem("valorefondo", rcespiteven.leggiDouble("vtotfondoc"));
                        if (rcespiteven.leggiDouble("vplusc") < 0) {
                            rcespitesalvato.insElem("minusvalenza", rcespiteven.leggiDouble("vplusc") * (-1));
                            rcespitesalvato.insElem("plusvalenza", 0.00);
                        } else {
                            rcespitesalvato.insElem("minusvalenza", 0.00);
                            rcespitesalvato.insElem("plusvalenza", rcespiteven.leggiDouble("vplusc"));//minusvalenza = plusvalenza negativa
                        }
                        rcespitesalvato.insElem("minusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcminusid"));
                        rcespitesalvato.insElem("plusvalenzapdc", rcespitesalvato.leggiIntero("catbspdcplusid"));
                        rcespitesalvato.insElem("immobilizzazionepdc", rcespitesalvato.leggiIntero("catbsimmpdcid"));
                        rcespitesalvato.insElem("fondopdc", rcespitesalvato.leggiIntero("catbsfondopdcid"));
                        rcespitesalvato.insElem("qta", rcespiteven.leggiDouble("cespqtav"));
                        rcespitesalvato.insElem("importo", rcespiteven.leggiDouble("cespimpvend"));
                        cespitidaregistrare.add(rcespitesalvato);
                    } else {
                        Record rcespitesalvato = new Record();
                        rcespitesalvato.insElem("cespid", -1);
                        cespitidaregistrare.add(rcespitesalvato);
                    }
                } else {
                    // descrittivo
                    Record rx = new Record();
                    rx.insElem("tiporiga", (int) 2);
                    rx.insElem("artid", (int) -1);
                    rx.insElem("artcod", "");
                    rx.insElem("artdescr", rc.leggiStringa("corpomagdescr"));
//                    rx.insElem("artum", "");
                    rx.insElem("lotto", "");
                    rx.insElem("ivaid", (int) -1);
                    rx.insElem("ivacod", "");
                    rx.insElem("colli", (int) 0);
                    rx.insElem("qta", (double) 0);
                    rx.insElem("prezzo", (double) 0);
                    rx.insElem("scart1", (double) 0);
                    rx.insElem("scart2", (double) 0);
                    rx.insElem("scart3", (double) 0);
                    rx.insElem("sccli1", (double) 0);
                    rx.insElem("sccli2", (double) 0);
                    rx.insElem("sccli3", (double) 0);
                    rx.insElem("lordo", (double) 0);
                    rx.insElem("importo", (double) 0);
                    rx.insElem("esclsctestata", (double) 0);
                    rx.insElem("oma", 0);
                    rx.insElem("serviziodatai", "0000-00-00");
                    rx.insElem("serviziodataf", "0000-00-00");
                    vrighe.add(rx);

                    Record rcespitesalvato = new Record();
                    rcespitesalvato.insElem("cespid", -1);
                    cespitidaregistrare.add(rcespitesalvato);
                }

                if (!rc.leggiStringa("corpomagdtcons").equals("0000-00-00")) {
                    dtcons = rc.leggiStringa("corpomagdtcons");
                }
            }
            qc.chiusura();
            
            String qryacconto="SELECT * FROM ftacconto INNER JOIN ftacconto_saldi ON facmovdocftaccid=facsmovdocftaccid WHERE facsmovdocftevasid="+idmov
                    +" AND facsescontftevas=\""+esmov+"\"";
            QueryInterrogazioneClient queryacconto= new QueryInterrogazioneClient(connAz,qryacconto);
            queryacconto.apertura();
            Record ris=queryacconto.successivo();
            queryacconto.chiusura();
            
            if(ris!=null)
            {
                String qryddocm="SELECT movdocid,movdocsufft,movdocnumft,movdocdtft,doccod,docdescr,rieptotale,rieptotimpon FROM movimentidoc" + ris.leggiStringa("facescontftacc") + " INNER JOIN documenti ON movdocdocumid = docid " +
                        " INNER JOIN riepdoc" + ris.leggiStringa("facescontftacc") + " ON movdocid = riepmovdocid " +
                        " WHERE movdocid = " + ris.leggiIntero("facmovdocftaccid");
                Query qdocm = new Query(connAz,qryddocm);
                qdocm.apertura();
                Record rdocm = qdocm.successivo();
                qm.chiusura();
                if (rdocm != null)
                {
                    Record rx = new Record();
                    rx.insElem("movdocid", rdocm.leggiIntero("movdocid"));
                    rx.insElem("esmov", ris.leggiStringa("facescontftacc"));
                    rx.insElem("doccod", rdocm.leggiStringa("doccod"));
                    rx.insElem("docdescr", rdocm.leggiStringa("docdescr"));
                    rx.insElem("datadoc", rdocm.leggiStringa("movdocdtft"));
                    rx.insElem("numdoc", rdocm.leggiIntero("movdocnumft"));
                    rx.insElem("sezdoc", rdocm.leggiStringa("movdocsufft"));
                    rx.insElem("totimp", rdocm.leggiDouble("rieptotimpon"));
                    rx.insElem("totdoc", rdocm.leggiDouble("rieptotale"));
                    double impres = 0;
                    double impsaldi = 0;
                    Query qivadoc = new Query(connAz, "SELECT * FROM ivaproforma" + rdocm.leggiStringa("movdocdtft").substring(0, 4) + " WHERE ipmovdocid = " + rdocm.leggiIntero("movdocid") +
                            " AND ipescont = \"" + ris.leggiStringa("facescontftacc") + "\"");
                    qivadoc.apertura();
                    for (int k = 0; k < qivadoc.numeroRecord(); k++) {
                        Record rivadoc = qivadoc.successivo();
                        Query qsldprec = new Query(connAz, "SELECT * FROM ftacconto_saldi WHERE facsescontftacc = \"" + ris.leggiStringa("facescontftacc") +
                                "\" AND facsmovdocftaccid = " + ris.leggiIntero("facmovdocftaccid"));
                        qsldprec.apertura();
                        for (int j = 0; j < qsldprec.numeroRecord(); j++)
                        {
                            Record rsldprec = qsldprec.successivo();
                            Query qcrpprec = new Query(connAz, "SELECT SUM(corpomagnetto) AS tnetto FROM corpodocmag" + rsldprec.leggiStringa("facsescontftevas") + " WHERE corpomagmovdocid = " + rsldprec.leggiIntero("facsmovdocftevasid") + " AND corpomagivaid = " + rivadoc.leggiIntero("ipcodid") +
                                    " AND corpomagkey LIKE \"ACC_%\"");
                            qcrpprec.apertura();
                            Record rcrpprec = qcrpprec.successivo();
                            qcrpprec.chiusura();
                            if (rcrpprec != null)
                            {
                                impsaldi += Math.abs(rcrpprec.leggiDouble("tnetto"));
                            }
                        }
                        qsldprec.chiusura();
                    }
                    qivadoc.chiusura();
                    impsaldi = OpValute.arrotondaMat(impsaldi, 2);
                    impres = OpValute.arrotondaMat(rdocm.leggiDouble("rieptotimpon") - Math.abs(impsaldi), 2);
                    rx.insElem("impres", impres);
                    vfattacconto.add(rx);
                }
                mostraFattureAcconto();
            }
        }

        if (!dtcons.equals("")) {
            datacons.setText((new Data(dtcons, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/"));
        }

        vdettddt = new ArrayList();
        if (rDoc.leggiIntero("doctipo") == 4) {
            vdettddt = BusinessLogicDocumenti.leggiDDTFatturaDifferita(EnvJazz.connAmbiente.conn, connAz.leggiConnessione(), connCom.leggiConnessione(), azienda, esmov, idmov);
        }

        mostraCarrello();

        ArrayList<Record> vscadtmp = BusinessLogicDocumenti.leggiScadenzeDocumento(connAz.leggiConnessione(), esmov, idmov);
        if (rDoc.leggiIntero("doctipo") == 4 && vscadtmp.size() == 0) {
            scadmodif = false;
        }
        ricalcolaTotale();
        ricalcolaTotale();

        if (vscadtmp.size() > 0) {
            vscad = new ArrayList();
            for (int i = 0; i < vscadtmp.size(); i++) {
                Record rtmp = vscadtmp.get(i);
                Record rsc = Edit.copiaRecord(rtmp);
                vscad.add(rsc);
            }
        }
        mostraScadenze();

        if (!rm.nullo("piedeid") && !rm.nullo("piemovdocid") && !rm.nullo("piecausa")) {
            notepiede.setText(rm.leggiStringa("pienote1"));
            tipotrasporto.setSelectedIndex(rm.leggiIntero("piecura"));
            causaletrasporto.setSelectedItem(rm.leggiStringa("piecausa"));
            aspettobeni.setSelectedItem(rm.leggiStringa("pieaspe"));
            if (tipotrasporto.getSelectedIndex() == 2) {
                pvettore.setVisible(true);
            } else {
                pvettore.setVisible(false);
            }

            if (rm.leggiIntero("piesped1id") != -1) {
                Record rvett = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "vettori", "vettid", rm.leggiIntero("piesped1id"), "");
                if (rvett != null) {
                    vettore.visualizzaDati(rvett.leggiIntero("vettid"), rvett.leggiStringa("vettcod"), rvett.leggiStringa("vettanagdes"));
                } else {
                    vettore.svuota();
                }
            } else {
                vettore.svuota();
            }

            targa.setText(rm.leggiStringa("pienote12"));
            dataritiro.visData(rm, "piespeddt1");
            oraritiro.impostaOrario(rm.leggiStringa("piespedho1"));
            totcolli.visIntero(rm, "pienumcol");
            totpallets.visIntero(rm, "pienumpallet");
            totpesolordo.visValuta(rm, "piepesolordo");
            totpesonetto.visValuta(rm, "piepeso");
            datapart.visData(rm, "pieritdt");
            orapart.impostaOrario(rm.leggiStringa("pieritho"));
            if (rm.leggiIntero("pieportoid") != -1) {
                Record rporto = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "porto", "pid", rm.leggiIntero("pieportoid"), "");
                if (rporto != null) {
                    porto.visualizzaDati(rporto.leggiIntero("pid"), rporto.leggiStringa("pcod"), rporto.leggiStringa("pdescr"));
                } else {
                    porto.svuota();
                }
            } else {
                porto.svuota();
            }
        }

        String qry2 = "SELECT * FROM movfe WHERE mfeescontmov = \"" + esmov + "\" AND mfemovdocid = " + idmov;
        QueryInterrogazioneClient qfe = new QueryInterrogazioneClient(connAz, qry2);
        qfe.apertura();
        Record rfe = qfe.successivo();
        qfe.chiusura();
        if (rfe != null) {
            datiagg_bollovirtuale.setSelected(rfe.leggiIntero("mfebollovirtuale") == 1);
            datiagg_impbollo.visValuta(rfe, "mfeimpbollo");
        }

        vdatiagg = new ArrayList();
        qry = "SELECT * FROM movfe_datiagg WHERE mfedaescontmov = \"" + esmov + "\" AND mfedamovdocid = " + idmov
                + " ORDER BY mfedatiporec,mfedanumrec";
        QueryInterrogazioneClient qdagg = new QueryInterrogazioneClient(connAz, qry);
        qdagg.apertura();
        for (int i = 0; i < qdagg.numeroRecord(); i++) {
            Record rdagg = qdagg.successivo();
            rdagg.eliminaCampo("mfedaid");
            rdagg.eliminaCampo("mfedaescontmov");
            rdagg.eliminaCampo("mfedamovdocid");
            rdagg.eliminaCampo("mfedanumrec");
            vdatiagg.add(rdagg);
        }
        qdagg.chiusura();
        mostraDatiAggiuntivi();

        vallegati = new ArrayList();
        qry = "SELECT * FROM movfe_allegati WHERE mfeaescontmov = \"" + esmov + "\" AND mfeamovdocid = " + idmov
                + " ORDER BY mfeanome";
        QueryInterrogazioneClient qall = new QueryInterrogazioneClient(connAz, qry);
        qall.apertura();
        for (int i = 0; i < qall.numeroRecord(); i++) {
            Record rall = qall.successivo();
            rall.eliminaCampo("mfeaid");
            rall.eliminaCampo("mfeaescontmov");
            rall.eliminaCampo("mfeamovdocid");
            vallegati.add(rall);
        }
        qall.chiusura();
        mostraAllegati();

        if (idmov == movdocId) {
//            vord = BusinessLogicDocumenti.leggiOrdiniClientEvasiDaDocumento(connAz.leggiConnessione(), connCom.leggiConnessione(), esmov, idmov);

            vnotac = BusinessLogicDocumenti.leggiDocumentiLegati(connAz.leggiConnessione(), connCom.leggiConnessione(), esmov, idmov, true);
            
            mostraFattureNotaCredito();
            ptab.setSelectedIndex(TAB_CLIENTE);
        } else {
            modo = EnvJazz.INS;

            ptab.setSelectedIndex(TAB_CARRELLO);
        }
    }

    private void ricalcolaTotale() {
        hiva = new TreeMap();
        int tcolli = 0;
        double tpesol = 0, tpeson = 0;
        double timp = 0, tiva = 0, tdoc = 0, tnetto = 0;
        for (int i = 0; i < vrighe.size(); i++) {
            Record r = vrighe.get(i);
            tcolli += r.leggiIntero("colli");
            if (r.leggiIntero("artid") > 0) {
                String[] campiart
                        = {
                            "artpeso", "artpesolordo"
                        };
                Record rart = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "articoli", "artid", r.leggiIntero("artid"), "", campiart);
                tpeson += (r.leggiDouble("qta") * rart.leggiDouble("artpeso"));
                tpesol += (r.leggiDouble("qta") * rart.leggiDouble("artpesolordo"));
            }

            if (r.leggiIntero("ivaid") != -1) {
                String qryiva = "SELECT * FROM codiva WHERE ivaid = " + r.leggiIntero("ivaid");
                QueryInterrogazioneClient qiva = new QueryInterrogazioneClient(connCom, qryiva);
                qiva.apertura();
                Record ri = qiva.successivo();
                qiva.chiusura();

                if (hiva.get(ri.leggiStringa("ivacod")) == null) {
                    Record riva = new Record();
                    riva.insElem("id", ri.leggiIntero("ivaid"));
                    riva.insElem("descr", ri.leggiStringa("ivadescr"));
                    riva.insElem("imponibile", r.leggiDouble("importo"));
                    riva.insElem("aliquota", ri.leggiDouble("ivaaliq"));
                    hiva.put(ri.leggiStringa("ivacod"), riva);
                } else {
                    Record riva = hiva.get(ri.leggiStringa("ivacod"));
                    double impold = riva.leggiDouble("imponibile");
                    riva.eliminaCampo("imponibile");
                    riva.insElem("imponibile", OpValute.arrotondaMat(impold + r.leggiDouble("importo"), 2));
                }

                // omaggio
                if (ri.leggiIntero("ivaivagestid") > 0 && ri.leggiIntero("ivagest") == 1) {
                    String qry = "SELECT * FROM codiva WHERE ivaid = " + ri.leggiIntero("ivaivagestid");
                    QueryInterrogazioneClient qiva1 = new QueryInterrogazioneClient(connCom, qry);
                    qiva1.apertura();
                    Record ri1 = qiva1.successivo();
                    qiva1.chiusura();

                    if (hiva.get(ri1.leggiStringa("ivacod")) == null) {
                        Record riva = new Record();
                        riva.insElem("id", ri1.leggiIntero("ivaid"));
                        riva.insElem("descr", ri1.leggiStringa("ivadescr"));
                        riva.insElem("imponibile", -r.leggiDouble("importo"));
                        riva.insElem("aliquota", ri1.leggiDouble("ivaaliq"));
                        hiva.put(ri1.leggiStringa("ivacod"), riva);
                    } else {
                        Record riva = hiva.get(ri1.leggiStringa("ivacod"));
                        double impold = riva.leggiDouble("imponibile");
                        riva.eliminaCampo("imponibile");
                        riva.insElem("imponibile", OpValute.arrotondaMat(impold - r.leggiDouble("importo"), 2));
                    }
                }
            }
        }
        // cassa previdenza

        // calcolo iva per i vari codici iva
        ivasplitp = 0;
        Iterator it = hiva.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            Record riva = hiva.get(key);
            timp += riva.leggiDouble("imponibile");
            double valiva = OpValute.arrotondaMat(riva.leggiDouble("imponibile") * riva.leggiDouble("aliquota") / 100.0, 2);
            riva.insElem("iva", valiva);
            tiva += valiva;
            Record ri = BusinessLogic.leggiRecord(connCom.leggiConnessione(), "codiva", "ivaid", riva.leggiIntero("id"), "");
            if (ri.leggiIntero("ivacess") == 1) {
                ivasplitp += valiva;
            }
        }
        ivasplitp = OpValute.arrotondaMat(ivasplitp, 2);
        timp = OpValute.arrotondaMat(timp, 2);
        tiva = OpValute.arrotondaMat(tiva, 2);
        tdoc = OpValute.arrotondaMat(timp + tiva, 2);

        // ritenuta
        //double timprit = Formattazione.estraiDouble(totimp.getText());
        double taccr = OpValute.arrotondaMat(Formattazione.estraiDouble(totaccred.getText()) + Formattazione.estraiDouble(totabbuono.getText()), 2);
        tnetto = OpValute.arrotondaMat(tdoc - taccr, 2);
        totimp.setText(Formattazione.formValuta(timp, 12, 2, 0));
        totiva.setText(Formattazione.formValuta(tiva, 12, 2, 0));
        totdoc.setText(Formattazione.formValuta(tdoc, 12, 2, 0));
        totdoc2.setText(Formattazione.formValuta(tnetto, 12, 2, 2));
        totnetto.setText(Formattazione.formValuta(tnetto, 12, 2, 2));
        totcolli.setText("" + tcolli);
        totpesolordo.setText(Formattazione.formValuta(tpesol, 12, 3, 0));
        totpesonetto.setText(Formattazione.formValuta(tpeson, 12, 3, 0));
        if (!scadmodif) {
            vscad = new ArrayList();
            if (pagamento.leggiId() != -1 && datamov.getData() != null) {
                Record rpag = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "pagamenti", "pagid", pagamento.leggiId(), "");
                ArrayList<Record> vscadtmp = BusinessLogicDocumenti.calcolaScadenzeDocumento(connAz.leggiConnessione(), rpag, OpValute.arrotondaMat(tnetto - ivasplitp, 2), datamov.getData(), 0, 0, 0, 2);
                for (int i = 0; i < vscadtmp.size(); i++) {
                    Record rscadtmp = vscadtmp.get(i);
                    Record rsc = new Record();
                    rsc.insElem("data", rscadtmp.leggiStringa("data"));
                    rsc.insElem("importo", rscadtmp.leggiDouble("importo"));
                    //                rs.insElem("scadsegno", rDoc.leggiIntero("tdsegno"));
                    //                rsc.insElem("scadmodo", rpag.leggiIntero("pagtipo"));
                    vscad.add(rsc);
                }
            }
        }

        mostraScadenze();
        mostraRiepilogoIva();
        mostradoc = false;
    }

//    private double[] calcolaPrezzoScontiArticolo(int artId) {
//        double[] obj = new double[8];
//        double sclic1 = Formattazione.estraiDouble(sccli1.getText());
//        double sclic2 = Formattazione.estraiDouble(sccli2.getText());
//        double sclic3 = Formattazione.estraiDouble(sccli3.getText());
//        Query qa = new Query(connAz, "SELECT * FROM articoli WHERE artid = " + artId);
//        qa.apertura();
//        Record rArt = qa.successivo();
//        qa.chiusura();
//        Record rprz = BusinessLogicListiniVendita.calcoloPrezzoDocCliente(connAz.leggiConnessione(), connCom.leggiConnessione(), cliente.leggiId(), consegna.leggiId(),
//                rArt, sclic1, sclic2, sclic3, listinoId, 1, pagamento.leggiId(), datamov.getDataStringa(), true, 1);
//        obj[0] = rprz.leggiDouble("prezzolordo");
//        obj[1] = rprz.leggiDouble("scontoriga1");
//        obj[2] = rprz.leggiDouble("scontoriga2");
//        obj[3] = rprz.leggiDouble("scontoriga3");
//        obj[4] = rprz.leggiDouble("scontot1");
//        obj[5] = rprz.leggiDouble("scontot2");
//        obj[6] = rprz.leggiDouble("scontot3");
//        obj[7] = rprz.leggiIntero("escludiScontiTestata");
//        return obj;
//    }
//    private void visualizzaPrezzoScontiArticolo() {
//        if (articolo.leggiId() != -1) {
//            double[] obj = calcolaPrezzoScontiArticolo(articolo.leggiId());
//            prezzo.setText(Formattazione.formValuta(obj[0], 12, 5, 0));
//            sc1.setText(Formattazione.formValuta(obj[1], 3, 2, 0));
//            sc2.setText(Formattazione.formValuta(obj[2], 3, 2, 0));
//            sc3.setText(Formattazione.formValuta(obj[3], 3, 2, 0));
//            sclicalc1 = obj[4];
//            sclicalc2 = obj[5];
//            sclicalc3 = obj[6];
//            this.ricalcolaRiga();
//        }
//    }
    private void ricalcolaRiga() {
        double q = Formattazione.estraiDouble(qta.getText());
        double p = Formattazione.estraiDouble(prezzo.getText());
        double s1 = Formattazione.estraiDouble(sc1.getText());
//        double s2 = Formattazione.estraiDouble(sc2.getText());
//        double s3 = Formattazione.estraiDouble(sc3.getText());
        double val = q * p;

        if (s1 != 0) {
            val -= OpValute.arrotondaMat(val * s1 / 100, 6);
        }

//        if (s2 != 0)
//        {
//            val -= OpValute.arrotondaMat(val * s2 / 100, 6);
//        }
//        
//        if (s3 != 0) 
//        {
//            val -= OpValute.arrotondaMat(val * s3 / 100, 6);
//        }
        if (sclicalc1 != 0) {
            val -= OpValute.arrotondaMat(val * sclicalc1 / 100, 6);
        }

        if (sclicalc2 != 0) {
            val -= OpValute.arrotondaMat(val * sclicalc2 / 100, 6);
        }

        if (sclicalc3 != 0) {
            val -= OpValute.arrotondaMat(val * sclicalc3 / 100, 6);
        }

        val = OpValute.arrotondaMat(val, 2);
        imponibilealiq.setText(Formattazione.formValuta(val, 12, 2, 2));
//        importo.setText(Formattazione.formValuta(val, 12, 2, 0));
    }

    private void mostraDatiAggiuntivi() {
        final String[] names
                = {
                    COL_BUTTON1, COL_TIPORIGA, COL_NDOC, COL_DTDOC, COL_CODCOMMESSA, COL_CUP, COL_CIG
                };
        final Object[][] data = new Object[vdatiagg.size()][names.length];
        for (int i = 0; i < vdatiagg.size(); i++) {
            Record r = vdatiagg.get(i);
            data[i][0] = "";
            if (r.leggiIntero("mfedatiporec") == 0) {
                data[i][1] = "dati ordine acquisto";
            } else if (r.leggiIntero("mfedatiporec") == 1) {
                data[i][1] = "dati contratto";
            } else if (r.leggiIntero("mfedatiporec") == 2) {
                data[i][1] = "dati convenzione";
            } else {
                data[i][1] = "dati ricezione";
            }

            data[i][2] = r.leggiStringa("mfedanumdoc");
            if (!r.leggiStringa("mfedadatadoc").equals("0000-00-00")) {
                data[i][3] = (new Data(r.leggiStringa("mfedadatadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            } else {
                data[i][3] = "";
            }
            data[i][4] = r.leggiStringa("mfedacodcommessa");
            data[i][5] = r.leggiStringa("mfedacodcup");
            data[i][6] = r.leggiStringa("mfedacodcig");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tabdatiagg, COL_BUTTON1)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };

        tabdatiagg.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tabdatiagg, true);
        tabdatiagg.getColumn(COL_BUTTON1).setPreferredWidth(40);
        tabdatiagg.getColumn(COL_TIPORIGA).setPreferredWidth(200);
        tabdatiagg.getColumn(COL_NDOC).setPreferredWidth(200);
        tabdatiagg.getColumn(COL_DTDOC).setPreferredWidth(150);
        tabdatiagg.getColumn(COL_CODCOMMESSA).setPreferredWidth(200);
        tabdatiagg.getColumn(COL_CUP).setPreferredWidth(200);
        tabdatiagg.getColumn(COL_CIG).setPreferredWidth(200);

        TableColumn buttonColumn1 = tabdatiagg.getColumn(COL_BUTTON1);
        TablePopButton bCanc = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
            }
        });

        bCanc.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vdatiagg.get(row);
                vdatiagg.remove(row);
                mostraDatiAggiuntivi();
            }
        });
        buttonColumn1.setCellRenderer(bCanc);
        buttonColumn1.setCellEditor(bCanc);

        tabdatiagg.getColumn(COL_BUTTON1).setHeaderRenderer(headerRenderer);
    }

    @Override
    public void impostaListener(PannelloPopListener l) {
        listener = l;
    }

    public static String nextAN(String initAL) {
        ArrayList<Character> NUMBERSET = new ArrayList();
        NUMBERSET.add('0');
        NUMBERSET.add('1');
        NUMBERSET.add('2');
        NUMBERSET.add('3');
        NUMBERSET.add('4');
        NUMBERSET.add('5');
        NUMBERSET.add('6');
        NUMBERSET.add('7');
        NUMBERSET.add('8');
        NUMBERSET.add('9');
        NUMBERSET.add('A');
        NUMBERSET.add('B');
        NUMBERSET.add('C');
        NUMBERSET.add('D');
        NUMBERSET.add('E');
        NUMBERSET.add('F');
        NUMBERSET.add('G');
        NUMBERSET.add('H');
        NUMBERSET.add('I');
        NUMBERSET.add('J');
        NUMBERSET.add('K');
        NUMBERSET.add('L');
        NUMBERSET.add('M');
        NUMBERSET.add('N');
        NUMBERSET.add('O');
        NUMBERSET.add('P');
        NUMBERSET.add('Q');
        NUMBERSET.add('R');
        NUMBERSET.add('S');
        NUMBERSET.add('T');
        NUMBERSET.add('U');
        NUMBERSET.add('V');
        NUMBERSET.add('W');
        NUMBERSET.add('X');
        NUMBERSET.add('Y');
        NUMBERSET.add('Z');

        int BASE = NUMBERSET.size();
        char[] number = initAL.toCharArray();
        int spare = 0;
        int addition = 1;

        /*
   * Here what i'm doing is i'm looping the given string backwards and get
   * char by char
         */
        for (int i = number.length - 1; i >= 0; i--) {
            if (i != number.length - 1) {
                addition = 0;
            }
            int lastnumber = NUMBERSET.indexOf(number[i]);
            /*
    * abov i'm getting number associated with the last char, example
    * letter "A" means 10 and in the below line i'm adding +1, also if
    * there is number left in the previous calculation I'm adding it
    * too
             */

            int newnumb = lastnumber + addition + spare;

            /*
    * now i'm checking whether the new number is exceeding the base,
    * example, in normal number set is 9+1 = 10
             */
            if (newnumb >= BASE) {
                // calculate spare and the addition
                number[i] = NUMBERSET.get(newnumb % BASE);
                spare = newnumb / BASE;
            } else {
                /*
    if the addition is not exceeding the base then we can just
    add them
    and stop there
                 */
                number[i] = NUMBERSET.get(newnumb);
                break;
            }
            /*
   checkin a special situation, spare can be 1 but sometimes number
   might have end in the next iteration, in this case i'm adding the
   spare to the front
   and stoping
             */
            if ((spare > 0) && ((i - 1) < 0)) {
                number = (NUMBERSET.get(spare) + new String(number)).toCharArray();
                break;
            }
        }

        return new String(number);
    }

    private String convertStringToDate(Date indate) {
        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd");

        try {
            dateString = sdfr.format(indate);
        } catch (Exception ex) {
            System.out.println(ex);
        }

        return dateString;
    }

    private void aggiornaNumeroDocumento() {
        if (modo == EnvJazz.INS) {
            if (datamov.getData() != null) {
                numero.setText("" + BusinessLogicDocumenti.leggiNumeroDocumentoLibero(connAz.leggiConnessione(), rDoc, esmov, datamov.getDataStringa()));
            }
        }
    }

    public boolean documentoModificato() {
        return docModificato;
    }

    public int idMovimento() {
        return movdocId;
    }

    public String esContMovimento() {
        return esmov;
    }

    private java.util.ArrayList<Record> salvaDatiPagamento(java.util.List<DatiPagamentoType> lp) {
        java.util.ArrayList<Record> pagRecord = new ArrayList<Record>();
        if (!lp.isEmpty()) {
            Record rpag = new Record();
            for (DatiPagamentoType dpt : lp) {
                rpag.insElem("edpcondpag", dpt.getCondizioniPagamento().value());
                java.util.List<DettaglioPagamentoType> deptl = dpt.getDettaglioPagamento();
                for (DettaglioPagamentoType dept : deptl) {
                    if (dept.getBeneficiario() != null) {
                        rpag.insElem("edpbeneficiario", dept.getBeneficiario());
                    }

                    rpag.insElem("edpmodpag", dept.getModalitaPagamento().value());
                    if (dept.getDataRiferimentoTerminiPagamento() != null) {
                        rpag.insElem("edpdatariftermpag", convertStringToDate(asDate(dept.getDataRiferimentoTerminiPagamento())));
                    }

                    if (dept.getGiorniTerminiPagamento() != null) {
                        rpag.insElem("edpggtermpag", dept.getGiorniTerminiPagamento());
                    }

                    if (dept.getDataScadenzaPagamento() != null) {
                        rpag.insElem("edpdatascadpag", convertStringToDate(asDate(dept.getDataScadenzaPagamento())));
                    }
                    rpag.insElem("edpimportopag", dept.getImportoPagamento().doubleValue());

                    if (dept.getCodUfficioPostale() != null) {
                        rpag.insElem("edpcoduffpostale", dept.getCodUfficioPostale());
                    }

                    if (dept.getCognomeQuietanzante() != null) {
                        rpag.insElem("edpcognomequiet", dept.getCognomeQuietanzante());
                    }

                    if (dept.getNomeQuietanzante() != null) {
                        rpag.insElem("edpnomequiet", dept.getNomeQuietanzante());
                    }

                    if (dept.getCFQuietanzante() != null) {
                        rpag.insElem("edpcfquiet", dept.getCFQuietanzante());
                    }

                    if (dept.getTitoloQuietanzante() != null) {
                        rpag.insElem("edptitoloquiet", dept.getTitoloQuietanzante());
                    }

                    if (dept.getIstitutoFinanziario() != null) {
                        rpag.insElem("edpistfinanziario", dept.getIstitutoFinanziario());
                    }

                    if (dept.getIBAN() != null) {
                        rpag.insElem("edpiban", dept.getIBAN());
                    }

                    if (dept.getABI() != null) {
                        rpag.insElem("edpabi", dept.getABI());
                    }

                    if (dept.getCAB() != null) {
                        rpag.insElem("edpcab", dept.getCAB());
                    }

                    if (dept.getBIC() != null) {
                        rpag.insElem("edpbic", dept.getBIC());
                    }

                    if (dept.getScontoPagamentoAnticipato() != null) {
                        rpag.insElem("edpscontopaganticip", dept.getScontoPagamentoAnticipato().doubleValue());
                    }

                    if (dept.getDataLimitePagamentoAnticipato() != null) {
                        rpag.insElem("edpdatalimpaganticip", convertStringToDate(asDate(dept.getDataLimitePagamentoAnticipato())));
                    }

                    if (dept.getPenalitaPagamentiRitardati() != null) {
                        rpag.insElem("edppenpagritard", dept.getPenalitaPagamentiRitardati().doubleValue());
                    }

                    if (dept.getDataDecorrenzaPenale() != null) {
                        rpag.insElem("edpdatadecorrpen", convertStringToDate(asDate(dept.getDataDecorrenzaPenale())));
                    }

                    if (dept.getCodicePagamento() != null) {
                        rpag.insElem("edpcodpag", dept.getCodicePagamento());
                    }

                    pagRecord.add(rpag);
                }
            }
        }
        return pagRecord;
    }

    private void mostraAllegati() {
        final String[] names
                = {
                    "", "Nome", "Compressione", "Formato", "Descrizione", "Percorso file"
                };
        final Object[][] data = new Object[vallegati.size()][6];
        for (int i = 0; i < vallegati.size(); i++) {
            Record r = vallegati.get(i);
            data[i][0] = "";
            data[i][1] = r.leggiStringa("mfeanome");
            data[i][2] = r.leggiStringa("mfeaalgcompr");
            data[i][3] = r.leggiStringa("mfeaformato");
            data[i][4] = r.leggiStringa("mfeadescr");
            data[i][5] = r.leggiStringa("mfealinkdoc");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (row == 0) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };

        taball.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(taball, true);
        taball.getColumnModel().getColumn(0).setPreferredWidth(40);
        taball.getColumnModel().getColumn(1).setPreferredWidth(400);
        taball.getColumnModel().getColumn(2).setPreferredWidth(80);
        taball.getColumnModel().getColumn(3).setPreferredWidth(100);
        taball.getColumnModel().getColumn(4).setPreferredWidth(200);
        taball.getColumnModel().getColumn(5).setPreferredWidth(200);

        TableColumn buttonColumn1 = taball.getColumnModel().getColumn(0);
        TablePopButton bCanc = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
            }
        });

        bCanc.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vallegati.get(row);
                vallegati.remove(row);
                mostraAllegati();
            }
        });
        buttonColumn1.setCellRenderer(bCanc);
        buttonColumn1.setCellEditor(bCanc);
    }

    public int leggiIdMovimento() {
        return movdocId;
    }

    private void generaRigaPn(int movdocid, String escontabile, int riga, int pdcId, String descragg, double val, boolean dare, String scoperto) {
        Record rpn = new Record();
        rpn.insElem("mpnmovdocid", movdocid);
        rpn.insElem("mpnriga", riga);
        rpn.insElem("mpnpdcid", pdcId);
        rpn.insElem("mpncaucont", cauId);
        rpn.insElem("mpndescr", caucondescr);

        rpn.insElem("mpndescragg", descragg);
        if (dare) {
            rpn.insElem("mpndare", val);
            rpn.insElem("mpndarevaluta", val);
            rpn.insElem("mpnavere", (double) 0);
        } else {
            rpn.insElem("mpndare", (double) 0);
            rpn.insElem("mpnavere", val);
            rpn.insElem("mpnaverevaluta", val);
        }
        rpn.insElem("mpnvaluta", (int) 2);
        rpn.insElem("mpncambio", (double) 1);
        rpn.insElem("mpnscoperto", scoperto);

        QueryAggiornamentoClient query = new QueryAggiornamentoClient(connAz.conn, QueryAggiornamentoClient.INS, "primanota" + escontabile, "mpnid", rpn);
        query.esecuzione();
    }

    private void modificaPrimaNota(String escontabile, int movid) {
        String qry = "SELECT * FROM movimentidoc" + escontabile + " WHERE movdocid=" + movid;
        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
        query.apertura();
        Record fattura = query.successivo();
        query.chiusura();

        qry = "SELECT * FROM movimentidoc" + escontabile + " WHERE movdocnumft=" + fattura.leggiIntero("movdocnumft")
                + " AND movdocsufft=\"" + fattura.leggiStringa("movdocsufft")
                + "\" AND movdocsufiva=\"" + fattura.leggiStringa("movdocsufft") + "\" AND movdocnumiva=" + fattura.leggiIntero("movdocnumft");

        query = new QueryInterrogazioneClient(connAz, qry);
        query.apertura();
        Record contabilizzazione = query.successivo();
        query.chiusura();
        if (contabilizzazione != null) 
        {
            qry = "SELECT MAX(mpnriga) AS max FROM primanota" + escontabile + " WHERE mpnmovdocid=" + contabilizzazione.leggiIntero("movdocid");
            query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record rprimanota = query.successivo();
            query.chiusura();
            int max = rprimanota.leggiIntero("max");
            max++;

            String descrizione = "";
            double valore = 0.00;
            HashMap<Integer,Double> valorestorico= new HashMap<>();
            HashMap<Integer,Double> fondo= new HashMap<>();
            HashMap<Integer,Double> minusv= new HashMap<>();
            HashMap<Integer,Double> plusv= new HashMap<>();
            for (int i = 0; i < cespitidaregistrare.size(); i++) {
                Record rcesp = cespitidaregistrare.get(i);
                if (rcesp.leggiIntero("cespid") != -1) 
                {                 
                    if(valorestorico.containsKey(rcesp.leggiIntero("immobilizzazionepdc")))
                    {
                        valore=valorestorico.get(rcesp.leggiIntero("immobilizzazionepdc"));
                        valore+=rcesp.leggiDouble("valoreparziale");
                    }
                    else
                    {
                        valore=0.00;
                        valore+=rcesp.leggiDouble("valoreparziale");
                    }
                    valorestorico.put(rcesp.leggiIntero("immobilizzazionepdc"), valore);
                    
                    valore=0.00;
                    if(fondo.containsKey(rcesp.leggiIntero("fondopdc")))
                    {
                        valore=fondo.get(rcesp.leggiIntero("fondopdc"));
                        valore+=rcesp.leggiDouble("parzialefondo");
                    }
                    else
                    {
                        valore=0.00;
                        valore+=rcesp.leggiDouble("parzialefondo");
                    }
                    fondo.put(rcesp.leggiIntero("fondopdc"), valore);
                    
                    valore=0.00;
                    if(minusv.containsKey(rcesp.leggiIntero("minusvalenzapdc")))
                    {
                        valore=minusv.get(rcesp.leggiIntero("minusvalenzapdc"));
                        valore+=rcesp.leggiDouble("minusvalenza");
                    }
                    else
                    {
                        valore=0.00;
                        valore+=rcesp.leggiDouble("minusvalenza");
                    }
                    minusv.put(rcesp.leggiIntero("minusvalenzapdc"), valore);
                    
                    valore=0.00;
                    if(plusv.containsKey(rcesp.leggiIntero("plusvalenzapdc")))
                    {
                        valore=plusv.get(rcesp.leggiIntero("plusvalenzapdc"));
                        valore+=rcesp.leggiDouble("plusvalenza");
                    }
                    else
                    {
                        valore=0.00;
                        valore+=rcesp.leggiDouble("plusvalenza");
                    }
                    plusv.put(rcesp.leggiIntero("plusvalenzapdc"), valore);
                }
            }
            
            int immpdc=-1;
            Set<Integer> chiavi=valorestorico.keySet();
            Iterator it = chiavi.iterator();
            while (it.hasNext())
            {
                immpdc=(Integer) it.next();
                if (tipodoc == 0) 
                {  
                    String qryupdate = "UPDATE primanota" + escontabile + " SET mpnavere=" + valorestorico.get(immpdc)
                            + ", mpnaverevaluta=" + valorestorico.get(immpdc) + " WHERE mpnmovdocid=" + contabilizzazione.leggiIntero("movdocid")
                            + " AND mpnpdcid=" + immpdc;
                    QueryAggiornamentoClient queryagg = new QueryAggiornamentoClient(connAz, qryupdate);
                    queryagg.esecuzione();
                } else {
                    String qryupdate = "UPDATE primanota" + escontabile + " SET mpndare=" + valorestorico.get(immpdc)
                        + ", mpndarevaluta=" + valorestorico.get(immpdc) + " WHERE mpnmovdocid=" + contabilizzazione.leggiIntero("movdocid")
                        + " AND mpnpdcid=" + immpdc;
                    QueryAggiornamentoClient queryagg = new QueryAggiornamentoClient(connAz, qryupdate);
                    queryagg.esecuzione();                    
                }
            }
            
            chiavi=fondo.keySet();
            it = chiavi.iterator();
            int fondopdc=-1;
            while (it.hasNext())
            {
                fondopdc=(Integer) it.next();
                if (tipodoc == 0) 
                {                        
                    generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, fondopdc, descrizione,
                        fondo.get(fondopdc), true, "C");
                } else 
                {
                    generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, fondopdc, descrizione,
                        fondo.get(fondopdc), false, "C");                    
                }
                max++;
            }

            
            double newplus=0.00;
            double newminus=0.00;
            chiavi=plusv.keySet();
            it = chiavi.iterator();
            int pluspdc=-1;
            while (it.hasNext())
            {
                dif=0.00;
                pluspdc=(Integer) it.next();
                if (tipodoc == 0) 
                {
                    generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, pluspdc, descrizione,
                        plusv.get(pluspdc), false, "C");
                } else 
                {       
                    if(tipologia==0 || tipologia==1)
                    {
                        generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, pluspdc, descrizione,
                            plusv.get(pluspdc), true, "C");
                    }
                    else if(tipologia==2)
                    {
                        String qryx="SELECT * FROM movimentidoc"+esContabile+" WHERE movdocid="+vnotac.get(0).leggiIntero("movdocid");
                        QueryInterrogazioneClient queryx=new QueryInterrogazioneClient(connAz,qryx);
                        queryx.apertura();
                        Record rx=queryx.successivo();
                        queryx.chiusura();
                        qryx="SELECT * FROM movimentidoc"+esContabile+" WHERE movdocsufiva=\""+rx.leggiStringa("movdocsufft")+
                                "\" AND movdocnumiva="+rx.leggiIntero("movdocnumft")+" AND movdocdtiva=\""+rx.leggiStringa("movdocdtft")+"\"";
                        queryx=new QueryInterrogazioneClient(connAz,qryx);
                        queryx.apertura();
                        rx=queryx.successivo();
                        queryx.chiusura();
                        
                        
                        String qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" + contabilizzazione.leggiIntero("movdocid")+
                                " AND mpnriga=1";
                        QueryInterrogazioneClient querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        Record rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valnc=0.00;
                        if(rpdcid!=null)
                        {
                            valnc=rpdcid.leggiDouble("mpnavere");
                        }
                        
                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" + contabilizzazione.leggiIntero("movdocid")+
                                " AND mpnriga=3";
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valiva=0.00;
                        if(rpdcid!=null)
                        {
                            valiva=rpdcid.leggiDouble("mpndare");
                        }
                            
                        valnc-=valiva;
                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" + rx.leggiIntero("movdocid")+
                                " AND mpnpdcid="+immpdc;
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valimm=0.00;
                        if(rpdcid!=null)
                        {
                            valimm=rpdcid.leggiDouble("mpnavere");
                        }

                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" + rx.leggiIntero("movdocid")+
                                " AND mpnpdcid="+fondopdc;
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valf=0.00;
                        if(rpdcid!=null)
                        {
                            valf=rpdcid.leggiDouble("mpndare");
                        }


                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" + rx.leggiIntero("movdocid")+
                                " AND mpnpdcid="+pluspdc;
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valplus=0.00;
                        if(rpdcid!=null)
                        {
                            valplus=rpdcid.leggiDouble("mpnavere");
                        }
                        
                        if(valplus>0)
                        {
                            double res=valimm-valf;
                            double importovendita=valplus+res;
                            double nuovaplus=(importovendita-valnc)-res;
                            dif=valplus-nuovaplus;
                            if(dif>0)
                            {
                                generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, pluspdc, descrizione,
                                dif, true, "C");
                            }
                            else
                            {
                                newminus=dif;
                            }
                        }
                    }
                }
                max++;
            }   
            
            chiavi=minusv.keySet();
            it = chiavi.iterator();
            int minuspdc=-1;
            while (it.hasNext())
            {
                minuspdc=(Integer) it.next();
                if (tipodoc == 0) 
                {
                    generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, minuspdc, descrizione,
                        minusv.get(minuspdc), true, "C");
                } else 
                {
                    if(tipologia==0 || tipologia==1)
                    {
                        generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, minuspdc, descrizione,
                            minusv.get(minuspdc), false, "C");
                    }
                    else if( tipologia==2)
                    {
                        if(newminus!=0.00)
                        {
                            generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, minuspdc, descrizione,
                            newminus, false, "C");
                            max++;
                        }
                        
                        String qryx="SELECT * FROM movimentidoc"+esContabile+" WHERE movdocid="+vnotac.get(0).leggiIntero("movdocid");
                        QueryInterrogazioneClient queryx=new QueryInterrogazioneClient(connAz,qryx);
                        queryx.apertura();
                        Record rx=queryx.successivo();
                        queryx.chiusura();
                        qryx="SELECT * FROM movimentidoc"+esContabile+" WHERE movdocsufiva=\""+rx.leggiStringa("movdocsufft")+
                                "\" AND movdocnumiva="+rx.leggiIntero("movdocnumft")+" AND movdocdtiva=\""+rx.leggiStringa("movdocdtft")+"\"";
                        queryx=new QueryInterrogazioneClient(connAz,qryx);
                        queryx.apertura();
                        rx=queryx.successivo();
                        queryx.chiusura();        
                        
                        String qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" + contabilizzazione.leggiIntero("movdocid")+
                                " AND mpnriga=1";
                        QueryInterrogazioneClient querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        Record rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valnc=0.00;
                        if(rpdcid!=null)
                        {
                            valnc=rpdcid.leggiDouble("mpnavere");
                        }

                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" + contabilizzazione.leggiIntero("movdocid")+
                                " AND mpnriga=3";
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valiva=0.00;
                        if(rpdcid!=null)
                        {
                            valiva=rpdcid.leggiDouble("mpndare");
                        }
                        valnc-=valiva;
                        
                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" +  rx.leggiIntero("movdocid")+
                                " AND mpnpdcid="+immpdc;
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valimm=0.00;
                        if(rpdcid!=null)
                        {
                            valimm=rpdcid.leggiDouble("mpnavere");
                        }

                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" +  rx.leggiIntero("movdocid")+
                                " AND mpnpdcid="+fondopdc;
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valf=0.00;
                        if(rpdcid!=null)
                        {
                            valf=rpdcid.leggiDouble("mpndare");
                        }


                        qrypdcid="SELECT * FROM primanota"+esContabile+" WHERE mpnmovdocid=" +  rx.leggiIntero("movdocid")+
                                " AND mpnpdcid="+minuspdc;
                        querypdcid=new QueryInterrogazioneClient(connAz,qrypdcid);
                        querypdcid.apertura();
                        rpdcid=querypdcid.successivo();
                        querypdcid.chiusura();
                        double valminus=0.00;
                        if(rpdcid!=null)
                        {
                            valminus=rpdcid.leggiDouble("mpndare");
                        }
                        if(valminus>0)
                        {
                            double res=valimm-valf;
                            double importovendita=valminus+res;
                            double nuovaminus=(importovendita-valnc)-res;
                            dif=valminus-nuovaminus;
                            if(dif>0)
                            {
                                generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, minuspdc, descrizione,
                                dif, false, "C");
                            }
                            else
                            {
                                newplus=dif;
                                generaRigaPn(contabilizzazione.leggiIntero("movdocid"), escontabile, max, pluspdc, descrizione,
                                dif, true, "C");
                            }
                        }
                    }
                }
                max++;
            }   
        }
    }

   private void mostraFattureAcconto() {
        final String[] names
                = {
                    COL_DOC, COL_SEZ, COL_DATA, COL_NUM, COL_IMP, COL_TOT, COL_IMPRES,COL_ELIMINA
                };
        final Object[][] data = new Object[vfattacconto.size()][names.length];
        for (int i = 0; i < vfattacconto.size(); i++) {
            Record r = vfattacconto.get(i);
            data[i][0] = r.leggiStringa("docdescr");
            data[i][1] = r.leggiStringa("sezdoc");
            data[i][2] = r.leggiIntero("numdoc");
            data[i][3] = (new Data(r.leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][4] = new FDouble(-r.leggiDouble("totimp"), "###,###,###,##0.00", FDouble.SEGNO_DX);
            data[i][5] = new FDouble(-r.leggiDouble("totdoc"), "###,###,###,##0.00", FDouble.SEGNO_DX);
            data[i][6] = new FDouble(-r.leggiDouble("impres"), "###,###,###,##0.00", FDouble.SEGNO_DX);
            data[i][7] = "";
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if(col == FunzioniUtilita.getColumnIndex(tabnotac1, COL_ELIMINA))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tabnotac1.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tabnotac1, true);

        tabnotac1.getColumn(COL_DOC).setPreferredWidth(400);
        tabnotac1.getColumn(COL_SEZ).setPreferredWidth(100);
        tabnotac1.getColumn(COL_DATA).setPreferredWidth(100);
        tabnotac1.getColumn(COL_NUM).setPreferredWidth(100);
        tabnotac1.getColumn(COL_IMP).setPreferredWidth(100);
        tabnotac1.getColumn(COL_TOT).setPreferredWidth(100);
        tabnotac1.getColumn(COL_IMPRES).setPreferredWidth(100);
        tabnotac1.getColumn(COL_ELIMINA).setPreferredWidth(20);
        TableColumn buttonColumn1 = tabnotac1.getColumn(COL_ELIMINA);
        TablePopButton bCanc = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                
                String qryeft = "SELECT * FROM eft_archivio" + esContabile + " WHERE eftescontmov= \""+esContabile+"\" AND eftmovdocid = " +movdocId;
                QueryInterrogazioneClient queryeft = new QueryInterrogazioneClient(connAz, qryeft);
                queryeft.apertura();
                Record reft = queryeft.successivo();
                queryeft.chiusura();

                String stato = "Da inviare";
                if (reft != null)
                {
                    String qrynotifiche = "SELECT * FROM eft_notifiche" + esContabile + " WHERE notideft=" + reft.leggiIntero("eftid");
                    QueryInterrogazioneClient querynotf = new QueryInterrogazioneClient(connAz, qrynotifiche);
                    querynotf.apertura();
                    Record rnot = querynotf.successivo();
                    querynotf.chiusura();

                    if (rnot != null) {
                        if (reft.leggiIntero("notstato") == 0 || reft.leggiIntero("notstato") == 4) {
                            stato = "Da rigenerare";
                        }
                    } else {
                        if (reft.leggiIntero("eftimportato") == 1) {
                            stato = "Importato";
                        } else {
                            if (reft.leggiIntero("eftstato") == 0) {
                                stato = "Da inviare";
                            } else if (reft.leggiIntero("eftstato") == 1) {
                                stato = "Inviata allo SDI";
                            } else {
                                stato = "In transito";
                            }
                        }
                    }
                }
                
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                if(stato.equals("Da inviare")|| stato.equals("Da rigenerare") || stato.equals("Importata"))
                {
                    button.setEnabled(true);
                }
                else
                {
                    button.setEnabled(false);
                }
            }
        });

        bCanc.addHandler(
                new TablePopButton.TableButtonPressedHandler() {

            @Override
            public void onButtonPress(int row, int column) {
                
                int idfatacconto=vfattacconto.get(row).leggiIntero("movdocid");
                String esfatacconto=vfattacconto.get(row).leggiStringa("esmov");
                
                String descrriga =  "Detrazione anticipo Fattura n." + vfattacconto.get(row).leggiStringa("sezdoc") + "/" + 
                        vfattacconto.get(row).leggiIntero("numdoc") + " del "
                    + (new Data(vfattacconto.get(row).leggiStringa("datadoc"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                
                int indiceremove=-1;
                for(int i=0;i<vrighe.size();i++)
                {
                    if(vrighe.get(i).leggiStringa("artdescr").equals(descrriga));
                    {
                        indiceremove=i;
                    }
                } 
                
                vrighe.remove(indiceremove);
                mostraCarrello();
                ricalcolaTotale();                
                                
                vfattacconto.remove(row);
                String qryupdate="UPDATE ftacconto SET facchiusa= 0 WHERE facmovdocftaccid = " +  idfatacconto + " AND facescontftacc = \"" + esfatacconto + "\"";
                QueryAggiornamentoClient qupdate = new QueryAggiornamentoClient(connAz,qryupdate);
                qupdate.esecuzione();
                
                String qrydelete3="DELETE FROM ftacconto_saldi WHERE facsmovdocftevasid = " +  movdocId + " AND facsescontftevas = \"" + esmov + "\"";
                QueryAggiornamentoClient qdant = new QueryAggiornamentoClient(connAz,qrydelete3);
                qdant.esecuzione();
                mostraFattureAcconto();
            }
        }
        );
        buttonColumn1.setCellRenderer(bCanc);
        buttonColumn1.setCellEditor(bCanc);
    }
    
    private void mostraFattureNotaCredito() {
        final String[] names
                = {
                    "", "Data fattura", "Numero fattura", "Cliente", "Importo"
                };
        final Object[][] data = new Object[vnotac.size()][5];
        for (int i = 0; i < vnotac.size(); i++) {
            Record r = vnotac.get(i);
            data[i][0] = "";
            data[i][1] = (new Data(r.leggiStringa("datadocman"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][2] = r.leggiStringa("numerodocman");
            data[i][3] = r.leggiStringa("nomesogg");
            data[i][4] = Formattazione.formValuta(r.leggiDouble("totaledoc"), 12, 2, 0);
        }
        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == 0) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tabnotac.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tabnotac, true);
        tabnotac.getColumnModel().getColumn(0).setPreferredWidth(40);
        tabnotac.getColumnModel().getColumn(1).setPreferredWidth(80);
        tabnotac.getColumnModel().getColumn(2).setPreferredWidth(200);
        tabnotac.getColumnModel().getColumn(3).setPreferredWidth(700);
        tabnotac.getColumnModel().getColumn(4).setPreferredWidth(150);
        TableColumn buttonColumn1 = tabnotac.getColumnModel().getColumn(0);
        TablePopButton bCanc = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
            }
        });

        bCanc.addHandler(
                new TablePopButton.TableButtonPressedHandler() {

            @Override
            public void onButtonPress(int row, int column) {
                vnotac.remove(row);
                mostraFattureNotaCredito();
            }
        }
        );
        buttonColumn1.setCellRenderer(bCanc);
        buttonColumn1.setCellEditor(bCanc);
    }

}
