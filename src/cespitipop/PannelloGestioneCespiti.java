package cespitipop;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import nucleo.ConnessioneServer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;
import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.OpValute;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.StampaTabella;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogic;
import nucleo.businesslogic.BusinessLogicAmbiente;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloGestioneCespiti extends javax.swing.JPanel implements PannelloPop {

    private String COL_GRU = "G/S/C";
    private String COL_ANNO = "Data acquisto";
    private String COL_NORDINE = "N. Ordine";
    private String COL_DESCR = "Descrizione";
    private String COL_NOTE = "Nota";
    private String COL_VALORE = "Valore";
    private String COL_FONDO = "Fondo";
    private String COL_RESIDUO = "Residuo";
    private String COL_RIPRESA = "F.";
    private String COL_CAPO = "+";
    private String COL_CAT = "M.";
    private String COL_VENDITA = "V/D";
    private String COL_PN = "PN";
    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;
    private Window padre;
    private Frame formGen;
    private int idVis = -1;
    private int idSelez = -1;
    private String azienda = "";
    private String esContabile = "";
    private WaitPop wait;
    private int[] id;
    private int[] idC;
    ArrayList<Record> vcespiti = new ArrayList();
    ArrayList<Record> vcatbsid = new ArrayList();
    ArrayList<Record> vmcsSpec = new ArrayList();
    private boolean primavolta = true;
    public boolean dacategoria = false;
    private boolean[] flagfigli;
    String[] dtes = null;
    Data oggi = new Data();
    String precdescr = "";
    String precricanno = "";
    String precricordine = "";

    public PannelloGestioneCespiti() {
        this.initComponents();
        this.setSize(1120, 680);
        this.setPreferredSize(new Dimension(1120, 680));
    }

    public PannelloGestioneCespiti(Frame formGen) {
        this();
        this.formGen = formGen;
    }

    public PannelloGestioneCespiti(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.wait = wait;

        dtes = BusinessLogicAmbiente.leggiEsercizio(azienda, esContabile);
        waitloc.ferma();
        popolaComboCategorieBs("", "", "");
        popolaComboCategorieMin();
        popolaComboFornitori();
        comboCategorieBs.setSelectedIndex(0);
        comboCategorieMin.setSelectedIndex(0);
        comboFornitore.setSelectedIndex(0);
        descr.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (!primavolta) {
                    if (controlloFiltri()) {
                        interrogazione();
                    }
                }
            }
        });

        ricordine.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (!primavolta) {
                    if (controlloFiltri()) {
                        interrogazione();
                    }
                }
            }
        });

        ricanno.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (!primavolta) {
                    if (!ricanno.getText().trim().equals(precricanno)) {
                        precricanno = ricanno.getText().trim();
                        interrogazione();
                    }
                }
            }
        });

        comboStato.setSelectedIndex(0);
        if (idVis > 0) {
            String qry = "SELECT * FROM categoriebs WHERE catbsid=" + idVis;
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record rcat = query.successivo();
            query.chiusura();

            if (rcat != null) {
                comboCategorieBs.setSelectedItem(rcat.leggiStringa("catbscod") + "-" + rcat.leggiStringa("catbsdescr"));
            }

            qry = "SELECT catbsid,catbstipo,catbscod,catbsdescr,catbsgruppo,catbsspecie,catbssotto FROM categoriebs"
                    + " LEFT JOIN gruppibs ON catbsgruppo=gruppobsmastro AND catbsspecie=gruppobsconto AND catbsdescr=gruppobscateg WHERE catbsstato = 1"
                    + " AND catbsid = " + idVis;
            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
            q.apertura();
            Record rl = q.successivo();
            if (rl != null) {
                if (rl.leggiIntero("catbstipo") == 2) {
                    catbsspecie.setText("Immobilizzazioni MATERIALI");
                } else if (rl.leggiIntero("catbstipo") == 3) {
                    catbsspecie.setText("Immobilizzazioni IMMATERIALI");
                } else {
                    catbsspecie.setText("ASSENTE");
                }
                interrogazione();
            }
            q.chiusura();
        } else {
            interrogazione();
        }
        comboCategorieBs.requestFocus();

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        comboCategorieBs = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        descr = new ui.beans.CampoTestoPop();
        jLabel10 = new javax.swing.JLabel();
        comboFornitore = new javax.swing.JComboBox<>();
        comboStato = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        ricordine = new ui.beans.CampoTestoPop();
        comboCategorieMin = new javax.swing.JComboBox<>();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        ricanno = new ui.beans.CampoInteroPop();
        waitloc = new nucleo.WaitJazz();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovoCespite = new ui.beans.PopButton();
        catbsspecie = new javax.swing.JLabel();
        totaleresiduo = new javax.swing.JLabel();
        immobilizzazione = new javax.swing.JLabel();
        totalefondo = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        numerocespiti = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        buttonStampa1 = new ui.beans.PopButton();
        buttonProduzione = new ui.beans.PopButton();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 80));
        pFiltri.setLayout(null);

        comboCategorieBs.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        comboCategorieBs.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<   ..........   >" }));
        comboCategorieBs.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboCategorieBsItemStateChanged(evt);
            }
        });
        comboCategorieBs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategorieBsActionPerformed(evt);
            }
        });
        pFiltri.add(comboCategorieBs);
        comboCategorieBs.setBounds(130, 8, 460, 24);

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Categ. ministeriale");
        pFiltri.add(jLabel9);
        jLabel9.setBounds(600, 8, 124, 24);

        descr.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.add(descr);
        descr.setBounds(130, 44, 308, 24);

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Categoria interna");
        pFiltri.add(jLabel10);
        jLabel10.setBounds(8, 8, 110, 24);

        comboFornitore.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboFornitore.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<   fornitore   >" }));
        comboFornitore.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboFornitoreItemStateChanged(evt);
            }
        });
        pFiltri.add(comboFornitore);
        comboFornitore.setBounds(732, 44, 372, 24);

        comboStato.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboStato.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<   ........  >", "Nuovi", "Non ancora in funzione", "Totalmente ammortizzati", "Alienati", "Residuo attivo", "Cespiti con figli", "Figli di altri cespiti", "Beni usati", "Beni Leasing" }));
        comboStato.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboStatoItemStateChanged(evt);
            }
        });
        pFiltri.add(comboStato);
        comboStato.setBounds(1180, 8, 184, 24);

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Descrizione");
        pFiltri.add(jLabel11);
        jLabel11.setBounds(4, 44, 110, 24);

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Fornitore");
        pFiltri.add(jLabel12);
        jLabel12.setBounds(656, 44, 70, 24);

        ricordine.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.add(ricordine);
        ricordine.setBounds(1180, 44, 112, 24);

        comboCategorieMin.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        comboCategorieMin.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<    .........  >" }));
        comboCategorieMin.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboCategorieMinItemStateChanged(evt);
            }
        });
        pFiltri.add(comboCategorieMin);
        comboCategorieMin.setBounds(732, 8, 372, 24);

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("n. Ordine");
        pFiltri.add(jLabel13);
        jLabel13.setBounds(1100, 44, 72, 24);

        jLabel14.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Stato");
        pFiltri.add(jLabel14);
        jLabel14.setBounds(1132, 8, 40, 24);

        jLabel15.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Anno");
        pFiltri.add(jLabel15);
        jLabel15.setBounds(444, 44, 56, 24);
        pFiltri.add(ricanno);
        ricanno.setBounds(516, 44, 72, 24);
        pFiltri.add(waitloc);
        waitloc.setBounds(1328, 40, 36, 32);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 40));
        jPanel1.setLayout(null);

        buttonNuovoCespite.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_box_white_24dp.png"))); // NOI18N
        buttonNuovoCespite.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovoCespite.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovoCespite.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovoCespite.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovoCespite.setToolTipText("Nuova immobilizzazione");
        buttonNuovoCespite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovoCespiteActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovoCespite);
        buttonNuovoCespite.setBounds(20, 4, 48, 32);

        catbsspecie.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        catbsspecie.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        catbsspecie.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        catbsspecie.setOpaque(true);
        jPanel1.add(catbsspecie);
        catbsspecie.setBounds(150, 10, 440, 24);

        totaleresiduo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totaleresiduo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totaleresiduo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totaleresiduo.setOpaque(true);
        jPanel1.add(totaleresiduo);
        totaleresiduo.setBounds(1180, 10, 120, 24);

        immobilizzazione.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        immobilizzazione.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        immobilizzazione.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        immobilizzazione.setOpaque(true);
        jPanel1.add(immobilizzazione);
        immobilizzazione.setBounds(810, 10, 120, 24);

        totalefondo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totalefondo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totalefondo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totalefondo.setOpaque(true);
        jPanel1.add(totalefondo);
        totalefondo.setBounds(990, 10, 120, 24);

        jLabel16.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Residuo");
        jPanel1.add(jLabel16);
        jLabel16.setBounds(1110, 0, 60, 40);

        jLabel17.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Immobilizzazione");
        jPanel1.add(jLabel17);
        jLabel17.setBounds(690, 0, 108, 40);

        jLabel18.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("N.");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(590, 0, 24, 40);

        numerocespiti.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        numerocespiti.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        numerocespiti.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        numerocespiti.setOpaque(true);
        jPanel1.add(numerocespiti);
        numerocespiti.setBounds(630, 10, 60, 24);

        jLabel19.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Fondo");
        jPanel1.add(jLabel19);
        jLabel19.setBounds(930, 0, 52, 40);

        buttonStampa1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_print_white_24dp.png"))); // NOI18N
        buttonStampa1.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonStampa1.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonStampa1.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonStampa1.setToolTipText("Stampa alienazioni");
        buttonStampa1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonStampa1ActionPerformed(evt);
            }
        });
        jPanel1.add(buttonStampa1);
        buttonStampa1.setBounds(1310, 4, 44, 30);

        buttonProduzione.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_location_white_24dp.png"))); // NOI18N
        buttonProduzione.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonProduzione.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonProduzione.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonProduzione.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonProduzione.setToolTipText("Registra incremento cespite ottenuto da lavorazioni e beni interni");
        buttonProduzione.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonProduzioneActionPerformed(evt);
            }
        });
        jPanel1.add(buttonProduzione);
        buttonProduzione.setBounds(80, 4, 44, 32);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setMinimumSize(new java.awt.Dimension(8, 10));
        jPanel2.setPreferredSize(new java.awt.Dimension(8, 100));
        add(jPanel2, java.awt.BorderLayout.LINE_START);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setPreferredSize(new java.awt.Dimension(8, 100));
        add(jPanel3, java.awt.BorderLayout.LINE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];
            DialogAnagraficaCespite d = new DialogAnagraficaCespite(formGen, true, connAz, connCom, azienda, esContabile, idSelez);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.show();
        }
    }//GEN-LAST:event_tabMouseClicked

    private void buttonNuovoCespiteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovoCespiteActionPerformed
        Record rFattura = new Record();
        Record rFattura1 = new Record();
        Record rFattura2 = new Record();

        int catbsid = -1;
        if (comboCategorieBs.getSelectedIndex() > 0) {
            catbsid = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1).leggiIntero("catbsid");
        }
        int ris = BusinessLogicAmbiente.mostraConfirmDialog(formGen, "Conferma", "Stai registrando su esercizio: " + esContabile + " ?", JOptionPane.YES_NO_OPTION);
        if (ris == JOptionPane.YES_OPTION) {
            String qry = "SELECT * FROM caubs WHERE bscaumov = 0 AND bscaucod =\"fta\" AND bscaustato = 1";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            qRic.chiusura();
            if (r != null) {
                if (r.leggiStringa("bsdocjazzcod2").equals("")) {
                    if (r.leggiStringa("bsdocjazzcod1").equals("")) {
                        if (!r.leggiStringa("bsdocjazzcod").equals("")) {
                            String[] campi = {"docid", "doccod", "docdescr", "doccauconid"};
                            rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                            if (rFattura == null) {
                                JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                return;
                            }
                        } else {
                            JOptionPane.showMessageDialog(formGen, "Documenti acquisto NON parametrizzati.\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    } else {
                        String[] campi = {"docid", "doccod", "docdescr", "doccauconid"};
                        rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                        if (rFattura != null) {
                            rFattura1 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod1"), campi);
                            if (rFattura1 == null) {
                                JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod1") + "\nImpossibile proseguire!", "",
                                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                return;
                            }
                        } else {
                            JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    }
                } else {
                    String[] campi = {"docid", "doccod", "docdescr", "doccauconid"};
                    rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                    if (rFattura != null) {
                        rFattura1 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod1"), campi);
                        if (rFattura1 != null) {
                            rFattura2 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod2"), campi);
                            if (rFattura2 == null) {
                                JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod2") + "\nImpossibile proseguire!", "",
                                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                return;
                            }
                        } else {
                            JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod1") + "\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    } else {
                        JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    }
                }
                DialogGestAcqCespiteManuale d = new DialogGestAcqCespiteManuale(formGen, true, connAz, connCom, azienda, esContabile, -1, "Nuovo", catbsid);
                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                d.setVisible(true);

                primavolta = true;
                interrogazione();
            } else {
                JOptionPane.showMessageDialog(formGen, "Assente parametrizzazione causali acquisti.\nImpossibile proseguire!", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            }
        }
    }//GEN-LAST:event_buttonNuovoCespiteActionPerformed

    private void comboCategorieBsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboCategorieBsItemStateChanged

//        if (!primavolta) {
//            if (comboCategorieBs.getSelectedIndex() > 0) {
//                Record rcateg = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1);
//                if (rcateg.leggiIntero("catbstipo") == 2) {
//                    catbsspecie.setText("Immobilizzazioni MATERIALI");
//                } else if (rcateg.leggiIntero("catbstipo") == 3) {
//                    catbsspecie.setText("Immobilizzazioni IMMATERIALI");
//                } else {
//                    catbsspecie.setText("Immobilizzazioni FINANZIARIE");
//                }
//                interrogazione();
//            } else {
//                interrogazione();
//            }
//        }
    }//GEN-LAST:event_comboCategorieBsItemStateChanged

    private void comboCategorieMinItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboCategorieMinItemStateChanged
        if (!primavolta) {
            if (comboCategorieMin.getSelectedIndex() > 0) {
                Record rmin = vmcsSpec.get(comboCategorieMin.getSelectedIndex() - 1);
                popolaComboCategorieBs(rmin.leggiStringa("gruppobsmastro"), rmin.leggiStringa("gruppobsconto"), rmin.leggiStringa("gruppobssotto"));

                if (rmin.leggiStringa("gruppobsmastro").equals("30")) {
                    catbsspecie.setText("Immobilizzazioni IMMATERIALI");
                } else if (rmin.leggiStringa("gruppobsmastro").equals("40")) {
                    catbsspecie.setText("Immobilizzazioni FINANZIARIE");
                } else {
                    catbsspecie.setText("Immobilizzazioni MATERIALI");
                }
                interrogazione();
            } else {
                interrogazione();
            }
        }
    }//GEN-LAST:event_comboCategorieMinItemStateChanged

    private void comboStatoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboStatoItemStateChanged
        /*
        <   stato  > //esclude venduti
        Nuova formazione
        Non ancora in funzione
        Totalmente ammortizzati
        Alienati
        Residuo attivo
        Cespiti con figli
        Figli di altri cespiti
        Beni usati
        Beni Leasing
         */
        if (!primavolta) {
            interrogazione();
        }
    }//GEN-LAST:event_comboStatoItemStateChanged

    private void comboFornitoreItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboFornitoreItemStateChanged
        if (!primavolta) {
            interrogazione();
        }
    }//GEN-LAST:event_comboFornitoreItemStateChanged

    private void comboCategorieBsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategorieBsActionPerformed
        if (!primavolta) {
            if (comboCategorieBs.getSelectedIndex() > 0) {
                Record rcateg = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1);
                switch (rcateg.leggiIntero("catbstipo")) {
                    case 2:
                        catbsspecie.setText("Immobilizzazioni MATERIALI");
                        break;
                    case 3:
                        catbsspecie.setText("Immobilizzazioni IMMATERIALI");
                        break;
                    default:
                        catbsspecie.setText("ASSENTE");
                        break;
                }
                interrogazione();
            } else {
                interrogazione();
            }
        }
    }//GEN-LAST:event_comboCategorieBsActionPerformed

    private void buttonStampa1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonStampa1ActionPerformed
        boolean opz_solovend = true;
        //codrepSelez.substring(0, 4).equals("cspf")
        //stampaLibro();

        StampaTabella prn = new StampaTabella(tab); // stat è la jtable da stampare
        prn.Stampa();
        prn = null;
    }//GEN-LAST:event_buttonStampa1ActionPerformed

    private void buttonProduzioneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonProduzioneActionPerformed

        int ris = BusinessLogicAmbiente.mostraConfirmDialog(this, "Conferma", "Vuoi registrare immobilizzazioni prodotte all'interno dell'azienda su esercizio: " + esContabile + " ?", JOptionPane.YES_NO_OPTION);
        if (ris == JOptionPane.YES_OPTION) {

            int catbsid=-1;
            if(comboCategorieBs.getSelectedIndex()!=0)
            {
                catbsid=vcatbsid.get(comboCategorieBs.getSelectedIndex()).leggiIntero("catbsid");
            }
            
            DialogGestProdCespiteManuale d = new DialogGestProdCespiteManuale(formGen, true, connAz, connCom, azienda, esContabile, catbsid,2);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.show();
            this.requestFocus();
        }
        
    }//GEN-LAST:event_buttonProduzioneActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonNuovoCespite;
    private ui.beans.PopButton buttonProduzione;
    private ui.beans.PopButton buttonStampa1;
    private javax.swing.JLabel catbsspecie;
    private javax.swing.JComboBox<String> comboCategorieBs;
    private javax.swing.JComboBox<String> comboCategorieMin;
    private javax.swing.JComboBox<String> comboFornitore;
    private javax.swing.JComboBox<String> comboStato;
    private ui.beans.CampoTestoPop descr;
    private javax.swing.JLabel immobilizzazione;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel numerocespiti;
    private javax.swing.JPanel pFiltri;
    private ui.beans.CampoInteroPop ricanno;
    private ui.beans.CampoTestoPop ricordine;
    private javax.swing.JTable tab;
    private javax.swing.JLabel totalefondo;
    private javax.swing.JLabel totaleresiduo;
    private nucleo.WaitJazz waitloc;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void interrogazione() {
        waitloc.lancia();

        boolean scartacespite = false;
        vcespiti = new ArrayList();
        String dataregord = (String) oggi.formatta(Data.AAAA_MM_GG, "-");
        String[] dtesattu = BusinessLogicAmbiente.leggiEsercizio(azienda, esContabile);

        //filtri categorie
        int idcatbs = -1;
        String incateg = "";
        if (comboCategorieBs.getSelectedIndex() > 0) {
            Record rcateg = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1);
            idcatbs = rcateg.leggiIntero("catbsid");
        } else if (comboCategorieMin.getSelectedIndex() > 0) {
            Record rmin = vmcsSpec.get(comboCategorieMin.getSelectedIndex() - 1);
            String qry2 = "SELECT catbsid FROM categoriebs"
                    + " JOIN gruppibs ON catbsgruppo = gruppobsmastro AND catbsspecie = gruppobsconto AND catbssotto = gruppobssotto"
                    + " WHERE catbsgruppo = \"" + rmin.leggiStringa("gruppobsmastro") + "\""
                    + " AND catbsspecie = \"" + rmin.leggiStringa("gruppobsconto") + "\""
                    + " AND catbssotto = \"" + rmin.leggiStringa("gruppobssotto") + "\"";
            QueryInterrogazioneClient query2 = new QueryInterrogazioneClient(connAz, qry2);
            query2.apertura();
            Record r2 = query2.successivo();
            while (r2 != null) {
                incateg += "" + r2.leggiIntero("catbsid") + ",";
                r2 = query2.successivo();
            }
            incateg = incateg.substring(0, (incateg.length()) - 1);
            query2.chiusura();
        }

        String qry = "SELECT cespid,cespcategid,cespordine,cespdescriz1,cespdatafunz,cespdataalien,cespmoviniid,cespcapoid,cespeserc,catbsgruppo,catbsspecie,catbssotto,catbscod,catbscriterio1"
                + " FROM cespiti"
                + " INNER JOIN categoriebs ON cespcategid = catbsid"
                + " WHERE cespid > 0";
        if (idcatbs > 0) {
            qry += " AND catbsid = " + idcatbs;//una sola categoria
        }
        if (!incateg.equals("")) {
            qry += " AND catbsid IN(" + incateg + ")";//tutte le categorie apparteneti alla categ ministeriale
        }
        if (!ricordine.getText().trim().equals("")) {
            qry += " AND cespordine LIKE \"%" + (String) ricordine.getText().trim() + "%\"";
        }
        if (!descr.getText().trim().equals("")) {
            qry += " AND cespdescriz1 LIKE \"%" + (String) descr.getText().trim() + "%\"";
        }
        if (!ricanno.getText().trim().equals("")) {
            qry += " AND cespeserc = \"" + (String) ricanno.getText().trim() + (String) ricanno.getText().trim() + "\"";
        }
        if (!dacategoria && primavolta) {
            qry += " AND cespeserc = \"" + esContabile + "\"";
        }
        if (comboStato.getSelectedIndex() == 0 && ricordine.getText().trim().equals("") && ricanno.getText().trim().equals("")) {
            qry += " AND cespdataalien = \"0000-00-00\"";
        } else if (comboStato.getSelectedIndex() == 1) {//comprati nell'anno o entrano in funzione nell'anno
            qry += " AND cespdatafunz >= \"" + dtesattu[0] + "\" AND cespdatafunz <= \"" + dtesattu[1] + "\"";
        } else if (comboStato.getSelectedIndex() == 2) {//Non ancora in funzione
            qry += " AND cespdatafunz > \"" + dataregord + "\"";
        } else if (comboStato.getSelectedIndex() == 4) {//alienati
            qry += " AND cespdataalien <> \"0000-00-00\"";
        } else if (comboStato.getSelectedIndex() == 7) {//Figli di altri cespiti
            qry += " AND cespcapoid > 0 AND cespdataalien = \"0000-00-00\"";
        }
        qry += " ORDER BY catbsgruppo,catbsspecie,catbssotto,cespeserc DESC,cespdatafunz DESC,cespdescriz1";

        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();

        Record r = qRic.successivo();
        while (r != null) {
            scartacespite = false;
            Record rins = new Record();
            rins.insElem("cespid", r.leggiIntero("cespid"));
            rins.insElem("cespcategid", r.leggiIntero("cespcategid"));
            rins.insElem("cespeserc", r.leggiStringa("cespeserc"));
            rins.insElem("cespordine", r.leggiStringa("cespordine"));
            rins.insElem("cespdescriz1", r.leggiStringa("cespdescriz1"));
            rins.insElem("cespdatafunz", r.leggiStringa("cespdatafunz"));
            rins.insElem("cespdataalien", r.leggiStringa("cespdataalien"));
            rins.insElem("cespmoviniid", r.leggiIntero("cespmoviniid"));
            rins.insElem("cespcapoid", r.leggiIntero("cespcapoid"));
            rins.insElem("catbsgruppo", r.leggiStringa("catbsgruppo"));
            rins.insElem("catbsspecie", r.leggiStringa("catbsspecie"));
            rins.insElem("catbssotto", r.leggiStringa("catbssotto"));
            rins.insElem("catbscod", r.leggiStringa("catbscod"));
            rins.insElem("catbscriterio1", r.leggiIntero("catbscriterio1"));
            int criterio = r.leggiIntero("catbscriterio1");

            //Residuo attivo
            String qryiniz = "";
            if (r.leggiIntero("cespmoviniid") > 0) {
                qryiniz = "SELECT cespaid,cespvaloreacq, cespvaloreacqc,cespfornit,cespusato,cespmovajazzid FROM cespitiacq"
                        + " WHERE cespacespid = " + r.leggiIntero("cespid")
                        + " AND cespvaloreacqc > 0"
                        + " AND cespaid = " + r.leggiIntero("cespmoviniid");
            } else {
                qryiniz = "SELECT cespaid,cespvaloreacq, cespvaloreacqc,cespfornit,cespusato,cespmovajazzid FROM cespitiacq"
                        + " WHERE cespacespid = " + r.leggiIntero("cespid")
                        + " AND cespdataesterna <= \"" + dataregord + "\""
                        + " AND cespvaloreacqc > 0"
                        + " ORDER BY cespeserca,cespdataacq LIMIT 01";
            }
            QueryInterrogazioneClient qa = new QueryInterrogazioneClient(connAz, qryiniz);
            qa.apertura();
            Record recacq = qa.successivo();
            if (recacq != null) {
                if (comboFornitore.getSelectedIndex() > 0 && !((String) comboFornitore.getSelectedItem()).equals("Assente")) {
                    if (!recacq.leggiStringa("cespfornit").equals((String) comboFornitore.getSelectedItem()) || !r.leggiStringa("cespdataalien").equals("0000-00-00")) {
                        scartacespite = true;
                    }
                }
                if ((comboStato.getSelectedIndex() == 8 && recacq.leggiIntero("cespusato") != 1) || (comboStato.getSelectedIndex() == 8 && !r.leggiStringa("cespdataalien").equals("0000-00-00"))) {
                    scartacespite = true;
                }
                if ((comboStato.getSelectedIndex() == 9 && recacq.leggiIntero("cespusato") != 2) || (comboStato.getSelectedIndex() == 9 && !r.leggiStringa("cespdataalien").equals("0000-00-00"))) {
                    scartacespite = true;
                }
                rins.insElem("cespfornit", recacq.leggiStringa("cespfornit"));
                rins.insElem("movjazzid", recacq.leggiIntero("cespmovajazzid"));
            } else {
                rins.insElem("cespfornit", "");
                rins.insElem("movjazzid", -1);
            }
            if (!scartacespite) {
                int figli = 0;
                String nota = "";
                if (r.leggiIntero("cespcapoid") != -1) {
                    String qry3 = "SELECT cespordine,cespdescriz1 FROM cespiti WHERE cespid = " + r.leggiIntero("cespcapoid");
                    QueryInterrogazioneClient qs = new QueryInterrogazioneClient(connAz, qry3);
                    qs.apertura();
                    if (qs.numeroRecord() > 0) {
                        Record rs = qs.successivo();
                        nota = " - Figlio di: " + rs.leggiStringa("cespordine") + "/" + rs.leggiStringa("cespdescriz1");
                    } else {
                        nota = " - Errore assenti beni accessori";
                    }
                    qs.chiusura();
                } else {
                    String qry3 = "SELECT cespordine FROM cespiti WHERE cespcapoid = " + r.leggiIntero("cespid");
                    QueryInterrogazioneClient qs = new QueryInterrogazioneClient(connAz, qry3);
                    qs.apertura();
                    if (qs.numeroRecord() > 0) {
                        figli = qs.numeroRecord();
                        nota = "Presenti n." + figli + " cespiti figli";
                    } else {
                        figli = 0;
                    }
                    qs.chiusura();
                    if ((comboStato.getSelectedIndex() == 6 && figli == 0) || (comboStato.getSelectedIndex() == 6 && !r.leggiStringa("cespdataalien").equals("0000-00-00"))) {
                        scartacespite = true;
                    }
                }
                if (!scartacespite) {
                    rins.insElem("flagfigli", figli);
                    double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, r.leggiIntero("cespid"), esContabile, dataregord);

                    double totbene = OpValute.arrotondaMat(vv[1], 2);
                    double totfondo = OpValute.arrotondaMat(vv[3], 2);
                    double totresiduo = totbene - totfondo;
                    rins.insElem("totbene", totbene);
                    rins.insElem("totfondo", totfondo);
                    rins.insElem("totresiduo", totresiduo);
                    double totbeneF = OpValute.arrotondaMat(vv[0], 2);
                    double totfondoF = OpValute.arrotondaMat(vv[2], 2);
                    double totresiduoF = totbeneF - totfondoF;
                    rins.insElem("totbeneF", totbeneF);
                    rins.insElem("totfondoF", totfondoF);
                    rins.insElem("totresiduoF", totresiduoF);

                    double ammortamentoF = OpValute.arrotondaMat(vv[2], 2);
                    double ammortamento = OpValute.arrotondaMat(vv[3], 2);
                    double ripresa = ammortamento - ammortamentoF;
                    rins.insElem("ripresa", ripresa);

                    if (!r.leggiStringa("cespdataalien").equals("0000-00-00") && totbene != 0) {
                        //patch per correzioni jazz1
                        String qryupdate = "UPDATE cespiti SET cespdataalien = \"0000-00-00\" WHERE cespid = " + r.leggiIntero("cespid");
                        QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
                        qd.esecuzione();
                        r.eliminaCampo("cespdataalien");
                        r.insElem("cespdataalien", "0000-00-00");
                    }
                    if (totbene == 0 && totfondo == 0 && r.leggiStringa("cespdataalien").equals("")) {
                        //patch per correzioni jazz1
                        String qry2 = "SELECT cespdataven FROM cespitiven"
                                + " WHERE cespvcespid = " + r.leggiIntero("cespid")
                                + " ORDER BY cespdataven DESC"
                                + " LIMIT 01";
                        QueryInterrogazioneClient qvu = new QueryInterrogazioneClient(connAz, qry2);
                        qvu.apertura();
                        Record rvu = qvu.successivo();
                        qvu.chiusura();
                        if (rvu != null) {
                            String qryupdate = "UPDATE cespiti SET cespdataalien = \"" + rvu.leggiStringa("cespdataven") + "\" WHERE cespid = " + r.leggiIntero("cespid");
                            QueryAggiornamentoClient qm = new QueryAggiornamentoClient(connAz, qryupdate);
                            qm.esecuzione();
                        } else {
                            boolean errorev = ricercaErroreVendita(r.leggiIntero("cespid"));
                            if (!errorev) {
                                nota = "ERRORE su valore storico e/o fondo";
                            } else {
                                String qry3 = "SELECT cespdataalien FROM cespiti"
                                        + " WHERE cespid = " + r.leggiIntero("cespid");
                                QueryInterrogazioneClient qvc = new QueryInterrogazioneClient(connAz, qry3);
                                qvc.apertura();
                                Record rvc = qvc.successivo();
                                qvc.chiusura();
                                if (rvc != null) {
                                    r.eliminaCampo("cespdataalien");
                                    r.insElem("cespdataalien", rvc.leggiStringa("cespdataalien"));
                                }
                            }
                        }
                    }

                    if (nota.equals("")) {
                        if (totbene == 0) {
                            nota = "Venduto/Dismesso " + (new Data(r.leggiStringa("cespdataalien"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                        } else if (totresiduo == totresiduoF && totresiduo == 0) {
                            nota = "Ammortizzato";
                        } else if (totresiduo == 0) {
                            nota = "Ammort. civile diverso da fiscale";
                            if (totresiduoF < 0) {
                                lanciaPatchAllineaAmmortamenti(r.leggiIntero("cespid"));
                            }
                        } else if (totresiduoF > 0 && totresiduo == 0) {
                            nota = "Aperto Fisc.";
                        } else if (totresiduo > 0 && totresiduoF == 0) {
                            nota = "Aperto Civ.";
                        } else if (r.leggiStringa("cespdatafunz").compareTo(dataregord) > 0) {
                            nota = "Non Funzione";
                        } else if (criterio == 5) {
                            nota = "Non ammortizzabile";
                        } else if (totfondo == totfondoF && totfondo == 0 && r.leggiStringa("cespdataalien").equals("0000-00-00")) {
                            if (r.leggiStringa("cespdatafunz").compareTo(dtesattu[0]) >= 0 && r.leggiStringa("cespdatafunz").compareTo(dtesattu[1]) <= 0) {
                                nota = "Nuovo";
                            } else if (recacq != null) {
                                if (recacq.leggiIntero("cespusato") == 1) {
                                    nota = "Usato";
                                } else if (recacq.leggiIntero("cespusato") == 2) {
                                    nota = "Leasing";
                                } else if (recacq.leggiIntero("cespusato") == 3) {
                                    nota = "Noleggio";
                                } else if (recacq.leggiIntero("cespusato") == 4) {
                                    nota = "Comodato";
                                } else {
                                    nota = "";
                                }
                            } else {
                                nota = "";
                            }
                        } else {
                            nota = "";
                        }
                    }
                    rins.insElem("nota", nota);

                    if ((comboStato.getSelectedIndex() == 3 && totresiduo != 0.00) || (comboStato.getSelectedIndex() == 3 && !r.leggiStringa("cespdataalien").equals("0000-00-00"))) {
                        scartacespite = true;
                    }
                    if ((comboStato.getSelectedIndex() == 5 && totresiduo == 0.00) || (comboStato.getSelectedIndex() == 5 && !r.leggiStringa("cespdataalien").equals("0000-00-00"))) {
                        scartacespite = true;
                    }
                }
                if (!scartacespite) {
                    vcespiti.add(rins);
                }
            }
            r = qRic.successivo();
        }
        qRic.chiusura();
        waitloc.ferma();
        //
        dacategoria=false;
        mostraLista();
    }

    private void mostraLista() {
        primavolta = false;
        final String[] names
                = {
                    COL_GRU, COL_NORDINE, COL_DESCR, COL_ANNO, COL_NOTE, COL_VALORE, COL_FONDO, COL_RESIDUO, COL_RIPRESA, COL_CAPO, COL_CAT, COL_VENDITA
                };
        final Object[][] data = new Object[vcespiti.size()][names.length];

        id = new int[vcespiti.size()];
        idC = new int[vcespiti.size()];
        flagfigli = new boolean[vcespiti.size()];
        double beni = 0;
        double fondi = 0;
        double residui = 0;
        for (int i = 0; i < vcespiti.size(); i++) {
            Record r = vcespiti.get(i);
            data[i][0] = r.leggiStringa("catbsgruppo") + " " + r.leggiStringa("catbsspecie") + " " + r.leggiStringa("catbssotto") + " " + r.leggiStringa("catbscod");
            data[i][1] = r.leggiStringa("cespordine");
            data[i][2] = r.leggiStringa("cespdescriz1");
            data[i][3] = r.leggiStringa("cespeserc").substring(0, 4) + "  "
                    + (new Data(r.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][4] = r.leggiStringa("nota");

            data[i][5] = Formattazione.formValuta(r.leggiDouble("totbene"), 12, 2, 0);
            data[i][6] = Formattazione.formValuta(r.leggiDouble("totfondo"), 12, 2, 0);
            data[i][7] = Formattazione.formValuta(r.leggiDouble("totresiduo"), 12, 2, 0);

            beni += r.leggiDouble("totbene");
            fondi += r.leggiDouble("totfondo");

            if (r.leggiIntero("flagfigli") == 1) {
                flagfigli[i] = true;
            } else {
                flagfigli[i] = false;
            }
            id[i] = r.leggiIntero("cespid");
            idC[i] = r.leggiIntero("cespcategid");
        }
        residui = beni - fondi;

        immobilizzazione.setText(Formattazione.formValuta(beni, 12, 2, 0));
        totalefondo.setText(Formattazione.formValuta(fondi, 12, 2, 0));
        totaleresiduo.setText(Formattazione.formValuta(residui, 12, 2, 0));

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_CAT)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_CAPO)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_VENDITA)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_RIPRESA)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tab, true);

        TableColumn buttonColumn1 = tab.getColumn(COL_CAPO);
        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (flagfigli[row]) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_remove_red_eye_white_24dp.png")));
                    button.setToolTipText("Visualizza cespiti figli collegati");
                    button.setEnabled(true);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_visibility_off_white_24dp.png")));
                    button.setToolTipText("NON presenti figli");
                    button.setEnabled(false);
                }
            }
        });
        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (flagfigli[row]) {
                    DialogVisualizzaBeniAccessori d = new DialogVisualizzaBeniAccessori(formGen, true, connAz, azienda, id[row]);
                    UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                    d.show();

                    interrogazione();
                }
            }
        });
        buttonColumn1.setCellRenderer(button1);
        buttonColumn1.setCellEditor(button1);

        TableColumn buttonColumn2 = tab.getColumn(COL_CAT);
        TablePopButton button2 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_low_priority_white_24dp.png")));
                button.setToolTipText("Cambio di categoria");
            }
        });
        button2.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                DialogUtilitaSostituzioneCategoria d = new DialogUtilitaSostituzioneCategoria(formGen, true, connAz, azienda, idC[row], id[row]);
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();

                interrogazione();
            }
        });
        buttonColumn2.setCellRenderer(button2);
        buttonColumn2.setCellEditor(button2);

        TableColumn buttonColumn3 = tab.getColumn(COL_VENDITA);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {                
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_local_grocery_store_white_24dp.png")));
                button.setToolTipText("Alineazione/Dismissione");
                if(vcespiti.get(row).leggiStringa("nota").contains("Venduto/Dismesso"))
                {
                    button.setEnabled(false);
                }
                else
                {
                    button.setEnabled(true);
                }
            }
        });
        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                int ris = JOptionPane.showConfirmDialog(formGen, "Vuoi dismettere o chiudere il cespite?",
                        "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                if (ris == JOptionPane.YES_OPTION) {
                    double totbene = Formattazione.estraiDouble((String) tab.getValueAt(row, FunzioniUtilita.getColumnIndex(tab, COL_VALORE)));
                    double totfondo = Formattazione.estraiDouble((String) tab.getValueAt(row, FunzioniUtilita.getColumnIndex(tab, COL_FONDO)));
                    DialogCaricamentoVenditaManualeCespite d = new DialogCaricamentoVenditaManualeCespite(formGen, true, connAz, connCom, azienda, id[row],
                            totbene, totfondo);
                    UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                    d.show();

                    interrogazione();
                }
            }
        });
        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        TableColumn buttonColumn4 = tab.getColumn(COL_RIPRESA);
        TablePopButton button4 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("catbscriterio1") == 0) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_adjust_white_24dp.png")));
                    button.setToolTipText("Raccordo civilistico/fiscale");
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_adjust_white_24dp.png")));
                    button.setToolTipText("NON presente raccordo civile-fiscale");
                    button.setEnabled(false);
                }
            }
        });
        button4.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("catbscriterio1") == 0) {
                    idSelez = id[row];
                    DialogAnagraficaFiscaleCespite d = new DialogAnagraficaFiscaleCespite(formGen, true, connAz, connCom, azienda, esContabile, idSelez);
                    UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                    d.show();
                }
            }
        });
        buttonColumn4.setCellRenderer(button4);
        buttonColumn4.setCellEditor(button4);

//        TableColumn buttonColumn5 = tab.getColumn(COL_PN);
//        TablePopButton button5 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
//            public void customize(PopButton button, int row, int column) {
//                Record r = vcespiti.get(row);
//                if (r.leggiIntero("movjazzid") < 0) {
//                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_compare_arrows_white_24dp.png")));
//                    button.setToolTipText("Collega a contabilizzazione");
//                } else {
//                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_compare_arrows_white_24dp.png")));
//                    button.setToolTipText("Legame esistente");
//                    button.setEnabled(false);
//                }
//            }
//        });
//        button5.addHandler(new TablePopButton.TableButtonPressedHandler() {
//            @Override
//            public void onButtonPress(int row, int column) {
//                Record r = vcespiti.get(row);
//                if (r.leggiIntero("movjazzid") < 0) {
//                    idSelez = id[row];
//                    DialogListaVariazioniIncrementi d = new DialogListaVariazioniIncrementi(formGen, true, connAz, connCom, azienda, esContabile, idC[row], idSelez, "", "");
//                    UtilForm.posRelSchermo(d, UtilForm.CENTRO);
//                    d.show();
//                }
//            }
//        });
//        buttonColumn5.setCellRenderer(button5);
//        buttonColumn5.setCellEditor(button5);

        tab.getColumn(COL_GRU).setPreferredWidth(100);
        tab.getColumn(COL_NORDINE).setPreferredWidth(132);
        tab.getColumn(COL_DESCR).setPreferredWidth(480);
        tab.getColumn(COL_ANNO).setPreferredWidth(130);
        tab.getColumn(COL_NOTE).setPreferredWidth(200);
        tab.getColumn(COL_VALORE).setPreferredWidth(100);
        tab.getColumn(COL_FONDO).setPreferredWidth(100);
        tab.getColumn(COL_RESIDUO).setPreferredWidth(100);
        tab.getColumn(COL_RIPRESA).setPreferredWidth(30);
        tab.getColumn(COL_CAT).setPreferredWidth(30);
        tab.getColumn(COL_CAPO).setPreferredWidth(30);
        tab.getColumn(COL_VENDITA).setPreferredWidth(40);
//        tab.getColumn(COL_PN).setPreferredWidth(30);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_VALORE).setCellRenderer(rightRenderer);
        tab.getColumn(COL_FONDO).setCellRenderer(rightRenderer);
        tab.getColumn(COL_RESIDUO).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tab.getColumn(COL_ANNO).setCellRenderer(centerRenderer);

        numerocespiti.setText("" + tab.getRowCount());
    }

    public void popolaComboCategorieBs(String ms, String spec, String cod) {
        comboCategorieBs.removeAllItems();
        comboCategorieBs.addItem("<  ......  >");
        vcatbsid = new ArrayList();
        String qry = "";
        if (comboCategorieMin.getSelectedIndex() == 0 || (ms.equals("") && spec.equals("") && cod.equals(""))) {
            qry = "SELECT catbsid,catbstipo,catbscod,catbsdescr,catbsgruppo,catbsspecie,catbssotto FROM categoriebs"
                    + " LEFT JOIN gruppibs ON catbsgruppo=gruppobsmastro AND catbsspecie=gruppobsconto AND catbsdescr=gruppobscateg WHERE catbsstato = 1"
                    + " ORDER BY catbsgruppo,catbsspecie,catbstipo,catbssotto,gruppobscateg DESC,catbsdescr";
        } else {
            qry = "SELECT catbsid,catbstipo,catbscod,catbsdescr,catbsgruppo,catbsspecie,catbssotto FROM categoriebs"
                    + " LEFT JOIN gruppibs ON catbsgruppo=gruppobsmastro AND catbsspecie=gruppobsconto AND catbsdescr=gruppobscateg WHERE catbsstato = 1"
                    + " AND catbsgruppo = \"" + ms + "\""
                    + " AND catbsspecie = \"" + spec + "\""
                    + " AND catbssotto = \"" + cod + "\" AND catbscod<>\"" + spec + cod + "00" + "\" "
                    + " ORDER BY catbsgruppo,catbsspecie,catbstipo,catbssotto,gruppobscateg DESC,catbsdescr";
        }
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record rl = q.successivo();
        while (rl != null) {
            vcatbsid.add(rl);
            comboCategorieBs.addItem(rl.leggiStringa("catbscod") + "-" + rl.leggiStringa("catbsdescr"));
            rl = q.successivo();
        }
        q.chiusura();
    }

    public void popolaComboCategorieMin() {
        comboCategorieMin.removeAllItems();
        comboCategorieMin.addItem("<  .....  >");
        String qry = "SELECT DISTINCT gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsdescr,gruppobsnote,gruppobscateg FROM categoriebs"
                + " JOIN gruppibs ON catbsgruppo = gruppobsmastro AND catbsspecie = gruppobsconto AND catbssotto = gruppobssotto"
                + " ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            comboCategorieMin.addItem(r.leggiStringa("gruppobsmastro") + "/" + r.leggiStringa("gruppobsconto") + "/" + r.leggiStringa("gruppobssotto") + "-" + r.leggiStringa("gruppobscateg") + r.leggiStringa("gruppobsnote"));
            vmcsSpec.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
    }

    public void popolaComboFornitori() {
        comboFornitore.removeAllItems();
        comboFornitore.addItem("<  ......  >");

        String qry = "SELECT DISTINCT cespfornit FROM cespitiacq"
                + " ORDER BY cespfornit";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record rl = q.successivo();
        while (rl != null) {
            String nome = rl.leggiStringa("cespfornit");
            if (rl.leggiStringa("cespfornit").equals("")) {
                nome = "Assente";
            }
            comboFornitore.addItem(nome);
            rl = q.successivo();
        }
        q.chiusura();
    }

    final boolean controlloFiltri() {
        boolean ok = true;
        if (!ricordine.getText().trim().equals("") && !descr.getText().trim().equals("")) {
            ok = false;
            JOptionPane.showMessageDialog(this, "Filtro doppio, codice e descrizione", "",
                    JOptionPane.WARNING_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/warning.png")));
        }
        if ((comboCategorieBs.getSelectedIndex() > 0 || comboCategorieMin.getSelectedIndex() > 0 || comboStato.getSelectedIndex() > 0 || comboFornitore.getSelectedIndex() > 0) && !ricordine.getText().trim().equals("")) {
            ok = false;
            JOptionPane.showMessageDialog(this, "Ordine del cespite con filtri selezionati, non ammessa ricerca", "",
                    JOptionPane.WARNING_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/warning.png")));
        }
        if (ricordine.getText().trim().equals(precricordine) && descr.getText().trim().equals(precdescr)) {
            ok = false;
        } else {
            precricordine = ricordine.getText().trim();
            precdescr = descr.getText().trim();
        }
        return ok;
    }

    private void lanciaPatchAllineaAmmortamenti(int idcespite) {
        //patch per disallineamenti dovuti a conversione cespiti
        //Elimino ammortamenti fiscali
        QueryAggiornamentoClient qdel = new QueryAggiornamentoClient(connAz, "DELETE FROM movimentibs "
                + " WHERE mbscespid = " + idcespite
                + " AND mbstipomov = 2"
                + " AND mbstipocf = 1");
        qdel.esecuzione();

        //copio civile = a fiscale
        String qrymov = "SELECT * FROM movimentibs"
                + " WHERE mbscespid = " + idcespite
                + " AND mbstipomov = 2"
                + " AND mbstipocf = 2";
        QueryInterrogazioneClient qmov = new QueryInterrogazioneClient(connAz, qrymov);
        qmov.apertura();
        Record rmov = qmov.successivo();
        while (rmov != null) {
            rmov.eliminaCampo("mbsid");
            rmov.eliminaCampo("mbstipocf");
            rmov.insElem("mbstipocf", (int) 1);
            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "movimentibs", "mbsid", rmov);
            qins.esecuzione();
            rmov = qmov.successivo();
        }
        qmov.chiusura();
    }

    private boolean ricercaErroreVendita(int idcesp) {
        //avviene solo per i clienti storici, che provengono da 1' release
        //patch per uniformare a versione attuale

        boolean trovato = false;
        String qry = "SELECT DISTINCT cespid,cespdataalien,mbseserc,mbsdata,mbsimpbene,mbsimpofondo,mbsnote,mbsinsuss,mbsmovjazzid FROM cespiti"
                + " INNER JOIN movimentibs ON cespid = mbscespid"
                + " WHERE cespid = " + idcesp
                + " AND ((mbstipomov = 3 AND mbsimpbene != 0) OR (mbstipomov < 2 AND mbsimpbene < 0))";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        if (r != null) {
            Record rins = new Record();
            rins.insElem("cespvcespid", idcesp);
            rins.insElem("cespesercv", r.leggiStringa("mbseserc"));
            rins.insElem("cespsezv", "");
            rins.insElem("cespdataven", r.leggiStringa("mbsdata"));
            rins.insElem("cespnumven", "");
            rins.insElem("cespcliente", r.leggiStringa("mbsnote"));
            rins.insElem("cespnotev", "");
            rins.insElem("cespimpvend", 0.00);
            rins.insElem("cespivav", 0.00);
            rins.insElem("vplusc", 0.00);

            rins.insElem("vconsistenzac", r.leggiDouble("mbsimpbene"));
            rins.insElem("vfondoc", r.leggiDouble("mbsimpofondo"));
            double res = r.leggiDouble("mbsimpbene") - r.leggiDouble("mbsimpofondo");
            rins.insElem("vresiduoc", res);

            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "cespitiven", "cespvcespid", rins);
            qa.esecuzione();
            String qryupdate = "UPDATE cespiti SET cespdataalien = \"" + r.leggiStringa("mbsdata") + "\" WHERE cespid = " + idcesp;
            QueryAggiornamentoClient qm = new QueryAggiornamentoClient(connAz, qryupdate);
            qm.esecuzione();
            trovato = true;
        }

        return trovato;
    }
    
    public void settaDacategoria(boolean var)
    {
        dacategoria=var;
    }
}
