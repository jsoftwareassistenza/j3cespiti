package cespitipop;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import nucleo.ConnessioneServer;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;

import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloCausaliMovimentazioneCespiti extends javax.swing.JPanel implements PannelloPop {

    private final String COL_TIPO = "";
    private final String COL_CAU = "Causale";
    private final String COL_METODO = "Funzionalità";
    private final String COL_CRITERIO = "Criterio";
    private final String COL_SEGNO = "+/-";
    private final String COL_STATO = "Stato";    
    private final String COL_JDOC = "Docum.1";
    private final String COL_JDOC1 = "Docum.2";
    private final String COL_JDOC2 = "Docum.3";
    private final String COL_JD = "V.d";
    private final String COL_EL = "Elim.";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;
    private int idVis = -1;
    private int idSelez = -1;
    private String azienda = "";
    private String esContabile = "";

    private int[] id;
    String[] dtes = null;
    Data oggi = new Data();

    private boolean[] flagcaujazz = null;
    private boolean[] flagdocjazz = null;
    ArrayList<Record> vcatbsid = new ArrayList();

    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloCausaliMovimentazioneCespiti() {
        this.initComponents();
    }

    public PannelloCausaliMovimentazioneCespiti(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        caubsdescr.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
//                if (!caubsdescr.getText().trim().equals("") && caubsdescr.getText().trim().length() >= 2) {
                interrogazioneStd();
                mostraLista("STD");
                buttonNuovaCateg.setEnabled(true);
//                }
            }
        });
        //
        interrogazioneStd();
        mostraLista("STD");
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        caubsdescr = new ui.beans.CampoTestoPop();
        jLabel10 = new javax.swing.JLabel();
        buttonMostraDisattive = new ui.beans.PopButton();
        stato = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovaCateg = new ui.beans.PopButton();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 50));
        pFiltri.setLayout(null);

        caubsdescr.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.add(caubsdescr);
        caubsdescr.setBounds(140, 12, 280, 24);

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Stato");
        pFiltri.add(jLabel10);
        jLabel10.setBounds(510, 12, 88, 24);

        buttonMostraDisattive.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_lock_white_24dp.png"))); // NOI18N
        buttonMostraDisattive.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonMostraDisattive.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonMostraDisattive.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonMostraDisattive.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonMostraDisattive.setToolTipText("Mostra categorie automatiche per caricamento iniziale cespiti da altro software");
        buttonMostraDisattive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonMostraDisattiveActionPerformed(evt);
            }
        });
        pFiltri.add(buttonMostraDisattive);
        buttonMostraDisattive.setBounds(1010, 10, 32, 32);

        stato.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        stato.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Attivo", "Disattivo" }));
        stato.setToolTipText("");
        stato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                statoActionPerformed(evt);
            }
        });
        pFiltri.add(stato);
        stato.setBounds(610, 12, 100, 24);

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Descrizione");
        pFiltri.add(jLabel11);
        jLabel11.setBounds(44, 12, 88, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        tab.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tabKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 40));
        jPanel1.setLayout(null);

        buttonNuovaCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovaCateg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setToolTipText("Nuova categoria");
        buttonNuovaCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaCategActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovaCateg);
        buttonNuovaCateg.setBounds(8, 4, 56, 32);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];
            DialogCausaliMovCespiti d = new DialogCausaliMovCespiti(formGen, true, connAz, connCom, esContabile, idSelez);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();
            requestFocus();
        }
    }//GEN-LAST:event_tabMouseClicked

    private void tabKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tabKeyPressed

    }//GEN-LAST:event_tabKeyPressed

    private void buttonNuovaCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaCategActionPerformed
        DialogCausaliMovCespiti d = new DialogCausaliMovCespiti(formGen, true, connAz, connCom, esContabile, -1);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        requestFocus();
        interrogazioneStd();
        mostraLista("STD");
        
    }//GEN-LAST:event_buttonNuovaCategActionPerformed

    private void buttonMostraDisattiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonMostraDisattiveActionPerformed
        interrogazioneIni();
        mostraLista("INIZ");
        buttonNuovaCateg.setEnabled(false);
    }//GEN-LAST:event_buttonMostraDisattiveActionPerformed

    private void statoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_statoActionPerformed
        interrogazioneStd();
        mostraLista("STD");
    }//GEN-LAST:event_statoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonMostraDisattive;
    private ui.beans.PopButton buttonNuovaCateg;
    private ui.beans.CampoTestoPop caubsdescr;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pFiltri;
    private javax.swing.JComboBox stato;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void interrogazioneStd() {
        vcatbsid = new ArrayList();
        String qry = "SELECT * FROM caubs WHERE bscaumov = 0";
        if (!caubsdescr.getText().trim().equals("")) {
            qry += " AND bscaudescr LIKE \"%" + caubsdescr.getText() + "%\"";
        }
        if(stato.getSelectedItem().equals("Attivo"))
        {
            qry+=" AND bscaustato =1";
        }
        else
        {
            qry+=" AND bscaustato =0";
        }
        
        qry += " ORDER BY bscauraggr,bscausegno,bscaudescr";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vcatbsid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
    }

    private void interrogazioneIni() {
        vcatbsid = new ArrayList();
        String qry = "SELECT * FROM caubs WHERE bscaustato = 1"
                + " AND bscaumov = 1"
                + " ORDER BY bscauraggr,bscausegno,bscaudescr";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vcatbsid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
    }

    private void mostraLista(String std) {
        final String[] names = {
            COL_TIPO, COL_CAU, COL_METODO, COL_CRITERIO, COL_SEGNO, COL_STATO,  COL_JDOC, COL_JDOC1, COL_JDOC2, COL_JD, COL_EL
        };
        final Object[][] data = new Object[vcatbsid.size()][names.length];

        id = new int[vcatbsid.size()];
        flagdocjazz = new boolean[vcatbsid.size()];
        for (int i = 0; i < vcatbsid.size(); i++) {
            Record rec = vcatbsid.get(i);
            data[i][0] = std;
            data[i][1] = rec.leggiStringa("bscaucod") + " - " + rec.leggiStringa("bscaudescr");
            //metodo
            switch (rec.leggiIntero("bscauraggr")) {
                case 0:
                    data[i][2] = "Acquisizioni nuove e variazioni valore bene";
                    break;
                case 1:
                    data[i][2] = "Rivalutazioni e svalutazioni valore bene/fondo";
                    break;
                case 2:
                    data[i][2] = "Ammortamenti e relativo fondo";
                    break;
                default:
                    data[i][2] = "Vendite e Alienazioni";
                    break;
            }

            switch (rec.leggiIntero("bscaubinario")) {
                case 0:
                    data[i][3] = "applicata a Fiscale e Civilistico";
                    break;
                case 1:
                    data[i][3] = "applicata a solo Fiscale";
                    break;
                default:
                    data[i][3] = "applicata a solo Civilistico";
                    break;
            }

            switch (rec.leggiIntero("bscausegno")) {
                case 0:
                    data[i][4] = "negativa";
                    break;
                default:
                    data[i][4] = "positiva";
                    break;
            }

            switch (rec.leggiIntero("bscaustato")) {
                case 0:
                    data[i][5] = "disattiva";
                    break;

                default:
                    data[i][5] = "attiva";
                    break;
            }

            flagdocjazz[i] = false;
            if (!rec.leggiStringa("bsdocjazzcod").equals("")) 
            {
                String qry2 = "SELECT * FROM documenti WHERE doccod = \"" + rec.leggiStringa("bsdocjazzcod") + "\"";
                QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry2);
                qb.apertura();
                if (qb.numeroRecord() > 0) {
                    Record rb = qb.successivo();
                    data[i][6] = rb.leggiStringa("doccod") + " - " + rb.leggiStringa("docdescr");
                    flagdocjazz[i] = true;
                } else {
                    data[i][6] = "";                    
                }
                qb.chiusura();
            } else if (!rec.leggiStringa("bsncjazzcod").equals("")) {
                String qry2 = "SELECT * FROM documenti WHERE doccod = \"" + rec.leggiStringa("bsncjazzcod") + "\"";
                QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry2);
                qb.apertura();
                if (qb.numeroRecord() > 0) {
                    Record rb = qb.successivo();
                    data[i][6] = rb.leggiStringa("doccod") + " - " + rb.leggiStringa("docdescr");
                    flagdocjazz[i] = true;
                } else {
                    data[i][6] = "";
                }
                qb.chiusura();
            } else{
                data[i][6] = "";
            }

            if (!rec.leggiStringa("bsdocjazzcod1").equals("")) {
                String qry2 = "SELECT * FROM documenti WHERE doccod = \"" + rec.leggiStringa("bsdocjazzcod1") + "\"";
                QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry2);
                qb.apertura();
                if (qb.numeroRecord() > 0) {
                    Record rb = qb.successivo();
                    data[i][7] = rb.leggiStringa("doccod") + " - " + rb.leggiStringa("docdescr");
                    flagdocjazz[i] = true;
                } else {
                    data[i][7] = "";
                }
                qb.chiusura();
            } else if (!rec.leggiStringa("bsncjazzcod1").equals("")) 
            {
                String qry2 = "SELECT * FROM documenti WHERE doccod = \"" + rec.leggiStringa("bsncjazzcod1") + "\"";
                QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry2);
                qb.apertura();
                if (qb.numeroRecord() > 0) {
                    Record rb = qb.successivo();
                    data[i][7] = rb.leggiStringa("doccod") + " - " + rb.leggiStringa("docdescr");
                    flagdocjazz[i] = true;
                } else {
                    data[i][7] = "";
                }
                qb.chiusura();
            } else {
                data[i][7] = "";
            }
                        
            if (!rec.leggiStringa("bsdocjazzcod2").equals("")) {
                String qry2 = "SELECT * FROM documenti WHERE doccod = \"" + rec.leggiStringa("bsdocjazzcod2") + "\"";
                QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry2);
                qb.apertura();
                if (qb.numeroRecord() > 0) {
                    Record rb = qb.successivo();
                    data[i][8] = rb.leggiStringa("doccod") + " - " + rb.leggiStringa("docdescr");
                    flagdocjazz[i] = true;
                } else {
                    data[i][8] = "";
                }
                qb.chiusura();
            } else if (!rec.leggiStringa("bsncjazzcod2").equals("")) {
                String qry2 = "SELECT * FROM documenti WHERE doccod = \"" + rec.leggiStringa("bsncjazzcod2") + "\"";
                QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry2);
                qb.apertura();
                if (qb.numeroRecord() > 0) {
                    Record rb = qb.successivo();
                    data[i][8] = rb.leggiStringa("doccod") + " - " + rb.leggiStringa("docdescr");
                    flagdocjazz[i] = true;
                } else {
                    data[i][8] = "";
                }
                qb.chiusura();
            } else {
                data[i][8] = "";
            }

            id[i] = rec.leggiIntero("bscauid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_JD)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_EL)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tab, true);

        TableColumn buttonColumn1 = tab.getColumn(COL_JD);
        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (std.equals("STD")) {
                    if (flagdocjazz[row]) {
                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ICONA_VAR_24_BIANCA.png")));
                        button.setToolTipText("Legame con documento");
                    } else {
                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ICONA_VAR_24_ROSSA.png")));
                        button.setToolTipText("ASSENTE Legame con documento");
                    }
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_visibility_off_white_24dp.png")));
                    button.setToolTipText("Assente collegamento con altre procedure / disattivo");
                }
            }
        });
        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                idSelez = id[row];
                DialogCausaliMovCespiti d = new DialogCausaliMovCespiti(formGen, true, connAz, connCom, esContabile, idSelez);
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();
                interrogazioneStd();
                mostraLista("STD");
            }
        });
        buttonColumn1.setCellRenderer(button1);
        buttonColumn1.setCellEditor(button1);

//        TableColumn buttonColumn2 = tab.getColumn(COL_JC);
//        TablePopButton button2 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
//            public void customize(PopButton button, int row, int column) {
//                if (std.equals("STD")) {
//                    if (flagcaujazz[row]) {
//                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ICONA_VAR_24_ROSSA.png")));
//                        button.setToolTipText("Legame con contabilita'");
//                    } else {
//                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ICONA_VAR_24_BIANCA.png")));
//                        button.setToolTipText("ASSENTE Legame con contabilita'");
//                    }
//                } else {
//                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_visibility_off_white_24dp.png")));
//                    button.setToolTipText("Assente collegamento con altre procedure");
//                }
//            }
//        });
//        button2.addHandler(new TablePopButton.TableButtonPressedHandler() {
//            @Override
//            public void onButtonPress(int row, int column) {
//                DialogParCauCollegatoJazz d = new DialogParCauCollegatoJazz(formGen, true, connAz, azienda, id[row]);
//                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
//                d.show();
//                interrogazioneStd();
//                mostraLista("STD");
//            }
//        });
//        buttonColumn2.setCellRenderer(button2);
//        buttonColumn2.setCellEditor(button2);
        TableColumn buttonColumn3 = tab.getColumn(COL_EL);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (std.equals("STD")) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                    button.setToolTipText("Elimina causale");
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_visibility_off_white_24dp.png")));
                    button.setToolTipText("Assente collegamento con altre procedure");
                }
            }
        });
        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                boolean ko = chiediConfermaElim(id[row]);
                if (ko) {
                    interrogazioneStd();
                    mostraLista("STD");
                }
            }
        });
        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        tab.getColumn(COL_TIPO).setPreferredWidth(40);
        tab.getColumn(COL_CAU).setPreferredWidth(250);
        tab.getColumn(COL_METODO).setPreferredWidth(140);
        tab.getColumn(COL_CRITERIO).setPreferredWidth(180);
        tab.getColumn(COL_SEGNO).setPreferredWidth(30);
        tab.getColumn(COL_STATO).setPreferredWidth(60);
        tab.getColumn(COL_JDOC).setPreferredWidth(160);
        tab.getColumn(COL_JDOC1).setPreferredWidth(160);
        tab.getColumn(COL_JDOC2).setPreferredWidth(160);
        tab.getColumn(COL_JD).setPreferredWidth(40);
        tab.getColumn(COL_EL).setPreferredWidth(40);

        tab.getColumn(COL_EL).setHeaderRenderer(headerRenderer);
        tab.getColumn(COL_JD).setHeaderRenderer(headerRenderer);
    }

    final boolean chiediConfermaElim(int idelim) {
        boolean eli = false;
        
        String qry="SELECT * FROM movimentibs WHERE mbscaumovid="+idelim;
        QueryInterrogazioneClient query= new QueryInterrogazioneClient(connAz,qry);
        query.apertura();
        Record r= query.successivo();
        query.chiusura();
        if(r==null)
        {
            int ris = JOptionPane.showConfirmDialog(this, "Confermi l'eliminazione della causale?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (ris == JOptionPane.YES_OPTION) {
                String qrydelete = "DELETE FROM caubs  WHERE bscauid =" + idelim;
                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
                qd.esecuzione();

                eli = true;
                JOptionPane.showMessageDialog(this, "Causale ELIMINATA ", "Invio",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            }
        }
        else
        {
            int ris = JOptionPane.showConfirmDialog(this, "Confermi la disattivazione della causale?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (ris == JOptionPane.YES_OPTION) {
                String qryagg = "UPDATE caubs SET bscaustato=0 WHERE bscauid =" + idelim;
                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryagg);
                qd.esecuzione();

                eli = true;
                JOptionPane.showMessageDialog(this, "Causale DISATTIVATA ", "Invio",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            }
        }
        return eli;
    }
}
