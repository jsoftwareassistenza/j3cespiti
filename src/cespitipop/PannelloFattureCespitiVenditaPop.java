package cespitipop;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import nucleo.einvoice.FunzioniJBridge;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import jbridge.DialogVisualizzaFatturaBlob;
import nucleo.ConnessioneServer;
import nucleo.Data;
import nucleo.DialogReportJR;
import nucleo.EnvJazz;
import nucleo.Formattazione;
import nucleo.FunzFatt;
import nucleo.FunzioniDB;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.PannelloTabellaSelezionaTuttiListener;
import nucleo.ProcedureException;
import nucleo.Query;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SelectAllHeader;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogicAmbiente;
import nucleo.businesslogic.documenti.BusinessLogicDocumenti;
import nucleo.einvoice.Utilita;
import org.apache.commons.io.FileUtils;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

/**
 *
 * @author pgx71
 */
public class PannelloFattureCespitiVenditaPop extends javax.swing.JPanel implements PannelloPop, PannelloTabellaSelezionaTuttiListener {

    private final String COL_SEL = "Sel";
    private final String COL_TIPODOC = "Tipo Documento";
    private final String COL_NUMDOC = "Numero";
    private final String COL_DTDOC = "Data";
    private final String COL_SOGG = "Cliente";
    private final String COL_TOTDOC = "Totale";
    private final String COL_ELIM = "Elimina";
    private final String COL_STAMPA = "Stampa";

    private final String COL_DOC = "Documento";
    private final String COL_CAU = "Causale";

    private Frame formGen;
    private WaitPop wait;
    private boolean vaihome = true;
    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private String azienda = "";
    private String esContabile = "";

    String[] esContDoc;

    private PannelloPopListener listener = null;
    private ArrayList<Record> lista = null;
    private ArrayList<Record> vcoddoc = null;

    private int[] idMov;

    private int tipodocumento = -1;
    private int documentoid = -1;
    String[] dtes = null;

    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();
    SelectAllHeader select = null;

    int docfatturaId = -1;
    int docnotacreditoId = -1;
    Record rDoc = null;

    /**
     * Creates new form PannelloMarchePop
     */
    public PannelloFattureCespitiVenditaPop() {
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public PannelloFattureCespitiVenditaPop(Frame formGen) {
        this();
        this.formGen = formGen;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.wait = wait;

        dtes = BusinessLogicAmbiente.leggiEsercizio(azienda, esContabile);

        Record ri = new Record();
        ri.insElem("datai", dtes[0]);
        ri.insElem("dataf", dtes[1]);
        dataip.visData(ri, "datai");
        datafp.visData(ri, "dataf");

        switch (tipodocumento) 
        {                  
            // fattura di vendita
            case 3:
                buttonNuovoFt.setVisible(true);
                buttonNuovoNc.setVisible(false);
                labnome.setText("Cliente");
                docfatturaId = documentoid;
                break;
            case 5:
                buttonNuovoFt.setVisible(false);
                buttonNuovoNc.setVisible(true);
                labnome.setText("Cliente");
                docnotacreditoId = documentoid;
                break;
            default:
                buttonNuovoFt.setVisible(false);
                buttonNuovoNc.setVisible(false);
                break;
        }

//        buttonNuovoNc.setVisible(false);
        //
        anagrafica.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                interrogazioneDocumenti();
            }
        });

        numip.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                interrogazioneDocumenti();
            }
        });

        numfp.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                interrogazioneDocumenti();
            }
        });

        dataip.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                interrogazioneDocumenti();
            }
        });

        dataip.setEventoRicercaData(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                interrogazioneDocumenti();
            }
        });

        datafp.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                interrogazioneDocumenti();
            }
        });

        datafp.setEventoRicercaData(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                interrogazioneDocumenti();
            }
        });

        interrogazioneDocumenti();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        pannelloElenco = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        anagrafica = new ui.beans.CampoTestoPop();
        labnome = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        dataip = new ui.beans.CampoDataPop();
        jLabel3 = new javax.swing.JLabel();
        datafp = new ui.beans.CampoDataPop();
        jLabel5 = new javax.swing.JLabel();
        numip = new ui.beans.CampoTestoPop();
        jLabel2 = new javax.swing.JLabel();
        numfp = new ui.beans.CampoTestoPop();
        panpas = new javax.swing.JPanel();
        pScroll = new javax.swing.JScrollPane();
        tabp = new javax.swing.JTable();
        pcom = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        buttonNuovoFt = new ui.beans.PopButton();
        importButton = new ui.beans.PopButton();
        buttonNuovoNc = new ui.beans.PopButton();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        ndoc = new javax.swing.JLabel();

        setBackground(java.awt.Color.white);
        setMaximumSize(new java.awt.Dimension(700, 174));
        setLayout(new java.awt.BorderLayout());

        jPanel1.setLayout(new java.awt.BorderLayout());

        pannelloElenco.setBackground(new java.awt.Color(255, 255, 255));
        pannelloElenco.setPreferredSize(new java.awt.Dimension(1400, 700));
        pannelloElenco.setLayout(new java.awt.BorderLayout());

        jPanel2.setPreferredSize(new java.awt.Dimension(80, 45));
        jPanel2.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setPreferredSize(new java.awt.Dimension(400, 100));
        jPanel3.setLayout(null);
        jPanel3.add(anagrafica);
        anagrafica.setBounds(80, 8, 320, 24);

        labnome.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        labnome.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labnome.setText("Cliente");
        jPanel3.add(labnome);
        labnome.setBounds(10, 8, 68, 24);

        jPanel2.add(jPanel3, java.awt.BorderLayout.WEST);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setPreferredSize(new java.awt.Dimension(1500, 200));
        jPanel4.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel1.setText("Da data");
        jPanel4.add(jLabel1);
        jLabel1.setBounds(20, 8, 60, 24);
        jPanel4.add(dataip);
        dataip.setBounds(80, 8, 124, 24);

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel3.setText("A data");
        jPanel4.add(jLabel3);
        jLabel3.setBounds(230, 8, 60, 24);
        jPanel4.add(datafp);
        datafp.setBounds(280, 8, 120, 24);

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel5.setText("Da numero");
        jPanel4.add(jLabel5);
        jLabel5.setBounds(428, 8, 70, 24);
        jPanel4.add(numip);
        numip.setBounds(508, 8, 84, 24);

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setText("A numero");
        jPanel4.add(jLabel2);
        jLabel2.setBounds(604, 8, 70, 24);
        jPanel4.add(numfp);
        numfp.setBounds(676, 8, 84, 24);

        jPanel2.add(jPanel4, java.awt.BorderLayout.CENTER);

        pannelloElenco.add(jPanel2, java.awt.BorderLayout.NORTH);

        panpas.setPreferredSize(new java.awt.Dimension(10, 350));
        panpas.setLayout(new java.awt.BorderLayout());

        pScroll.setBackground(new java.awt.Color(255, 255, 255));

        tabp.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabpMouseClicked(evt);
            }
        });
        pScroll.setViewportView(tabp);

        panpas.add(pScroll, java.awt.BorderLayout.CENTER);

        pannelloElenco.add(panpas, java.awt.BorderLayout.CENTER);

        jPanel1.add(pannelloElenco, java.awt.BorderLayout.CENTER);

        add(jPanel1, java.awt.BorderLayout.CENTER);

        pcom.setBackground(java.awt.Color.white);
        pcom.setPreferredSize(new java.awt.Dimension(700, 44));
        pcom.setLayout(new java.awt.BorderLayout());

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setPreferredSize(new java.awt.Dimension(360, 100));
        jPanel6.setLayout(null);

        buttonNuovoFt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonNuovoFt.setText("FT");
        buttonNuovoFt.setToolTipText("Nuovo documento Fattura");
        buttonNuovoFt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovoFtActionPerformed(evt);
            }
        });
        jPanel6.add(buttonNuovoFt);
        buttonNuovoFt.setBounds(44, 4, 60, 36);

        importButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_file_upload_white_24dp.png"))); // NOI18N
        importButton.setLocation(new java.awt.Point(60, 30));
        importButton.setToolTipText("Importa");
        importButton.setVisible(false);
        importButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importButtonActionPerformed(evt);
            }
        });
        jPanel6.add(importButton);
        importButton.setBounds(60, 30, 36, 36);

        buttonNuovoNc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_remove_circle_white_24dp.png"))); // NOI18N
        buttonNuovoNc.setActionCommand("NC");
        buttonNuovoNc.setText("NC");
        buttonNuovoNc.setToolTipText("Nuovo documento Nota Credito");
        buttonNuovoNc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovoNcActionPerformed(evt);
            }
        });
        jPanel6.add(buttonNuovoNc);
        buttonNuovoNc.setBounds(116, 4, 60, 36);

        pcom.add(jPanel6, java.awt.BorderLayout.WEST);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(new java.awt.BorderLayout());

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setLayout(null);
        jPanel7.add(jPanel8, java.awt.BorderLayout.CENTER);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setPreferredSize(new java.awt.Dimension(500, 100));
        jPanel9.setLayout(null);

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Documenti Trovati");
        jPanel9.add(jLabel7);
        jLabel7.setBounds(68, 8, 136, 24);

        ndoc.setBackground(new java.awt.Color(202, 236, 221));
        ndoc.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        ndoc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        ndoc.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        ndoc.setMinimumSize(new java.awt.Dimension(200, 24));
        ndoc.setOpaque(true);
        ndoc.setPreferredSize(new java.awt.Dimension(200, 24));
        jPanel9.add(ndoc);
        ndoc.setBounds(216, 8, 116, 24);

        jPanel7.add(jPanel9, java.awt.BorderLayout.EAST);

        pcom.add(jPanel7, java.awt.BorderLayout.CENTER);

        add(pcom, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents


    private void importButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importButtonActionPerformed


    }//GEN-LAST:event_importButtonActionPerformed

    private void buttonNuovoFtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovoFtActionPerformed

        DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 0, rDoc);
        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
        d.setVisible(true);

        interrogazioneDocumenti();
   
    }//GEN-LAST:event_buttonNuovoFtActionPerformed

    private void tabpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabpMouseClicked
        if (evt.getClickCount() == 2 && tabp.getSelectedRow() >= 0) {
            int tipodoc = -1;

            if (docfatturaId > 0) {
                tipodoc = 0;
            }
            if (docnotacreditoId > 0) {
                tipodoc = 4;
            }

            DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, EnvJazz.connComune, EnvJazz.azienda, EnvJazz.esContabile, idMov[tabp.convertRowIndexToModel(tabp.getSelectedRow())], tipodoc);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.show();
            //
            interrogazioneDocumenti();
            //
        }
    }//GEN-LAST:event_tabpMouseClicked

    private void buttonNuovoNcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovoNcActionPerformed

        Object[] options
        = {
            "Nota credito per reso totale", "Nota credito per reso parziale","Nota credito per variazione importo"
        };
        Object selectedValue = JOptionPane.showOptionDialog(this, "Indicare il tipo di nota credito:", "Scelta tipo nota credito",
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")), options, options[0]);

        if (selectedValue.toString().equals("0")) {
            DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 4, rDoc);
            d.impostaModalitaNotaCredito(0);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.setVisible(true);
        }
        else if(selectedValue.toString().equals("1"))
        {
            DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 4, rDoc);
            d.impostaModalitaNotaCredito(1);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.setVisible(true);
        }
        else if(selectedValue.toString().equals("2"))
        {
            DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 4, rDoc);
            d.impostaModalitaNotaCredito(2);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.setVisible(true);
        }
        

        interrogazioneDocumenti();

    }//GEN-LAST:event_buttonNuovoNcActionPerformed

    public void mostraDocumenti() 
    {
        ndoc.setText("" + lista.size());

        if (lista.size() > 0) 
        {
            final String[] names
            = {
                COL_SEL, COL_TIPODOC, COL_NUMDOC, COL_DTDOC, COL_SOGG, COL_TOTDOC, COL_ELIM, COL_STAMPA
            };

            final Object[][] data = new Object[lista.size()][names.length];
            idMov = new int[lista.size()];
            esContDoc = new String[lista.size()];
            for (int i = 0; i < lista.size(); i++) {
                Record r = lista.get(i);
                data[i][0] = new Boolean(false);
                data[i][1] = r.leggiStringa("tipodoc");
                data[i][2] = "" + r.leggiIntero("numerodoc");
                data[i][3] = new Data(r.leggiStringa("datadoc"), Data.AAAA_MM_GG).formatta(Data.GG_MM_AAAA, "/");
                data[i][4] = r.leggiStringa("nome");
                data[i][5] = Formattazione.formatta(r.leggiDouble("totaledoc"), "######0.00", 3);

                idMov[i] = r.leggiIntero("id");
                esContDoc[i] = r.leggiStringa("datadoc").substring(0, 4).concat(r.leggiStringa("datadoc").substring(0, 4));

            }

            tabp.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    tabpMouseClicked(evt);
                }
            });

            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    if (col == FunzioniUtilita.getColumnIndex(tabp, COL_SEL)
                            || col == FunzioniUtilita.getColumnIndex(tabp, COL_ELIM)
                            || col == FunzioniUtilita.getColumnIndex(tabp, COL_STAMPA)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };

            pScroll.getViewport().remove(tabp);
            pScroll.getViewport().add(tabp);
            tabp.revalidate();
            tabp.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabp, true);

            tabp.getColumn(COL_SEL).setPreferredWidth(20);
            tabp.getColumn(COL_TIPODOC).setPreferredWidth(100);
            tabp.getColumn(COL_NUMDOC).setPreferredWidth(70);
            tabp.getColumn(COL_SOGG).setPreferredWidth(800);
            tabp.getColumn(COL_TOTDOC).setPreferredWidth(70);
            tabp.getColumn(COL_ELIM).setPreferredWidth(20);
            tabp.getColumn(COL_STAMPA).setPreferredWidth(20);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            tabp.getColumn(COL_TOTDOC).setCellRenderer(rightRenderer);

            tabp.getColumn(COL_ELIM).setHeaderRenderer(headerRenderer);
            tabp.getColumn(COL_STAMPA).setHeaderRenderer(headerRenderer);

            TableColumn tc = tabp.getColumn(COL_SEL);
            select = new SelectAllHeader(tabp, FunzioniUtilita.getColumnIndex(tabp, COL_SEL));
            select.impostaListener(this);
            tc.setHeaderRenderer(select);

            //Elimina Documento
           TableColumn  buttonColumn = tabp.getColumn(COL_ELIM);
            TablePopButton button = new TablePopButton(new TablePopButton.TableButtonCustomizer() {

                public void customize(PopButton button, int row, int column) {
                    boolean invsdi = BusinessLogicDocumenti.documentoInviatoSDI(connAz.leggiConnessione(), azienda, esContabile, idMov[row]);
                    if (invsdi) {
                        button.setEnabled(false);
                    }
                    else
                    {
                        button.setEnabled(true);
                    }
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                    button.setToolTipText("Elimina documento");
                }
            });

            button.addHandler(new TablePopButton.TableButtonPressedHandler() {
                @Override
                public void onButtonPress(int row, int column) {
                    int ris1 = JOptionPane.showConfirmDialog(formGen, "Confermare l'eliminazione del documento?",
                            "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                    boolean canc = false;
                    if (ris1 == JOptionPane.YES_OPTION)
                    {
                        String qrydoc = "SELECT * FROM movimentidoc" + esContabile
                                + " INNER JOIN documenti ON movdocdocumid = docid INNER JOIN sezionalinum ON docsuffnumid = sufxid WHERE movdocid = " + idMov[row];
                        QueryInterrogazioneClient qdoc = new QueryInterrogazioneClient(connAz, qrydoc);
                        qdoc.apertura();
                        Record rDoc = qdoc.successivo();
                        qdoc.chiusura();
                        
                        String qryeft = "SELECT * FROM eft_archivio" + esContabile + " WHERE eftescontmov= \""+esContabile+"\" AND eftmovdocid = " + idMov[row];
                        QueryInterrogazioneClient queryeft = new QueryInterrogazioneClient(connAz, qryeft);
                        queryeft.apertura();
                        Record reft = queryeft.successivo();
                        queryeft.chiusura();

                        String stato = "Da inviare";
                        if (reft != null)
                        {
                            String qrynotifiche = "SELECT * FROM eft_notifiche" + esContabile + " WHERE notideft=" + reft.leggiIntero("eftid");
                            QueryInterrogazioneClient querynotf = new QueryInterrogazioneClient(connAz, qrynotifiche);
                            querynotf.apertura();
                            Record rnot = querynotf.successivo();
                            querynotf.chiusura();

                            if (rnot != null) {
                                if (reft.leggiIntero("notstato") == 0 || reft.leggiIntero("notstato") == 4) {
                                    stato = "Da rigenerare";
                                }
                            } else {
                                if (reft.leggiIntero("eftimportato") == 1) {
                                    stato = "Importato";
                                } else {
                                    if (reft.leggiIntero("eftstato") == 0) {
                                        stato = "Da inviare";
                                    } else if (reft.leggiIntero("eftstato") == 1) {
                                        stato = "Inviata allo SDI";
                                    } else {
                                        stato = "In transito";
                                    }
                                }
                            }
                        }

                        if (rDoc != null) 
                        {
                            if (stato.equals("Da inviare") || stato.equals("Da rigenerare") || stato.equals("Importata"))
                            {
                                int ris = FunzFatt.eliminaDocumento(connAz, esContabile, rDoc, rDoc, idMov[row], false, formGen);
                                if (ris == FunzFatt.CANCELLATO)
                                {
                                    String qrydelete = "DELETE FROM eft_archivio" + esContabile + " WHERE eftid = " + reft.leggiIntero("eftid");
                                    QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
                                    qd.esecuzione();

                                    qrydelete = "UPDATE eft_notifiche" + esContabile + " SET notideft = -1 WHERE notideft = " + reft.leggiIntero("eftid");
                                    QueryAggiornamentoClient qaggnot = new QueryAggiornamentoClient(connAz, qrydelete);
                                    qaggnot.esecuzione();

                                    String qrycespiteven = "SELECT * FROM cespitiven INNER JOIN cespiti ON cespvcespid=cespid WHERE cespnumven=" + rDoc.leggiIntero("movdocnumft")
                                            + " AND cespdataven=\"" + rDoc.leggiStringa("movdocdtft") + "\" AND cespesercv=\"" + esContabile + "\"";
                                    QueryInterrogazioneClient querycespitiven = new QueryInterrogazioneClient(connAz, qrycespiteven);
                                    querycespitiven.apertura();
                                    for (int i = 0; i < querycespitiven.numeroRecord(); i++) {
                                        Record rcesp = querycespitiven.successivo();
                                        if(tipodocumento==3)
                                        {
                                            String qryupdate = "UPDATE cespiti SET cespdataalien = \"0000-00-00\",cespqtaattu = 0 WHERE cespid = " + rcesp.leggiIntero("cespid");
                                            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                                            qa.esecuzione();
                                        }
                                    }
                                    querycespitiven.chiusura();

                                    String qrydelete2 = "DELETE FROM cespitiven WHERE cespftjazzid = " + idMov[row];
                                    QueryAggiornamentoClient queryagg = new QueryAggiornamentoClient(connAz, qrydelete2);
                                    queryagg.esecuzione();

                                    qrydelete2 = "DELETE FROM movimentibs WHERE mbsmovjazzid = " + idMov[row];
                                    queryagg = new QueryAggiornamentoClient(connAz, qrydelete2);
                                    queryagg.esecuzione();
                                    
                                    String qrydelete3="DELETE FROM ftacconto_saldi WHERE facsmovdocftevasid = " +  idMov[row] + " AND facsescontftevas = \"" + esContabile + "\"";
                                    QueryAggiornamentoClient qdant = new QueryAggiornamentoClient(connAz,qrydelete3);
                                    qdant.esecuzione();
                                    
                                    JOptionPane.showMessageDialog(formGen, "Documento N. " + tabp.getValueAt(row, FunzioniUtilita.getColumnIndex(tabp, COL_NUMDOC)) + " eliminato", "Conferma",
                                            JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                                    
                                    interrogazioneDocumenti();
                                } else {
                                    JOptionPane.showMessageDialog(formGen, "Errore durante l'eliminazione", "Conferma",
                                            JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    return;
                                }                        
                            } else {
                                JOptionPane.showMessageDialog(formGen, "Almeno uno dei file selezionati e' gia' stato inviato al SDI. Non si può procedere con la cancellazione di questo file.", "",
                                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                return;
                            }
                        }                        
                    }
                    interrogazioneDocumenti();
                }
            });

            buttonColumn.setCellRenderer(button);
            buttonColumn.setCellEditor(button);

            //Stampa Documento
            buttonColumn = tabp.getColumn(COL_STAMPA);
            button = new TablePopButton(new TablePopButton.TableButtonCustomizer() {

                public void customize(PopButton button, int row, int column) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_print_white_24dp.png")));
                    button.setToolTipText("Stampa");
                }
            });
            button.addHandler(new TablePopButton.TableButtonPressedHandler() {

                @Override
                public void onButtonPress(int row, int column) {
                    Query qms = new Query(connAz, "SELECT * FROM movimentidoc" + esContDoc[row] + " LEFT JOIN documenti ON movdocdocumid = docid LEFT JOIN modellistampa ON docmodstid = modstid WHERE movdocid = " + idMov[row]);
                    qms.apertura();
                    Record r = qms.successivo();
                    qms.chiusura();
                    Query qeft = new Query(connAz, "SELECT a.*, eftxml, eftxmlsigned FROM eft_archivio" + esContDoc[row] + " a JOIN eft_archivioxml" + esContDoc[row] + " x ON x.eftid = a.eftid WHERE eftmovdocid = " + idMov[row] + " AND eftescontmov = \"" + esContDoc[row] + "\"");
                    qeft.apertura();
                    Record rb = qeft.successivo();
                    qeft.chiusura();
                    if (rb != null) {
                        try {
                            boolean deskSupp = Desktop.isDesktopSupported();
                            boolean deskBrowseSupp = Desktop.getDesktop().isSupported(Desktop.Action.BROWSE);
                            boolean deskOpenSupp = Desktop.getDesktop().isSupported(Desktop.Action.OPEN);
                            if (deskSupp && (deskBrowseSupp || deskOpenSupp)) {
                                String radiceNome = rb.leggiStringa("eftnomeorigfile").substring(0, rb.leggiStringa("eftnomeorigfile").indexOf("."));
                                String formatoxls = FunzioniJBridge.FOGLIO_STILE_FATTURA_ASSOSOFTWARE_PDF;
                                File filePdf = new File(radiceNome + ".pdf");
                                filePdf.deleteOnExit();
                                Utilita ut = new Utilita();
                                byte[] pdfByte = ut.creaPdf(FunzioniJBridge.TIPO_EXPORT_FATTURA, rb, getClass().getResourceAsStream("/resource/" + formatoxls), null);
                                FileUtils.writeByteArrayToFile(filePdf, pdfByte);
                                if (deskSupp && (deskBrowseSupp || deskOpenSupp)) {
                                    Desktop.getDesktop().open(filePdf);
                                }
                            } else {
                                DialogVisualizzaFatturaBlob d = new DialogVisualizzaFatturaBlob(formGen, true, rb, connAz, esContDoc[row]);
                                d.show();
                            }
                        } catch (Exception ex) {
                            System.out.println(ex.getMessage());
                            new ProcedureException(this.getClass().toString(), ex);
                        }
                    } else {
                        if (r.leggiIntero("docmodstid") != -1) {
                            stampa(r.leggiIntero("movdocid"), esContDoc[row], r);
                        } else {
                            JOptionPane.showMessageDialog(formGen, "Modello di stampa per il documento non configurato", "Errore",
                                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                        }
                    }
                }
            }
            );

            buttonColumn.setCellRenderer(button);
            buttonColumn.setCellEditor(button);
        } else {
            tabp.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{}
            ));
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.CampoTestoPop anagrafica;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private ui.beans.PopButton buttonNuovoFt;
    private ui.beans.PopButton buttonNuovoNc;
    private ui.beans.CampoDataPop datafp;
    private ui.beans.CampoDataPop dataip;
    private ui.beans.PopButton importButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JLabel labnome;
    private javax.swing.JLabel ndoc;
    private ui.beans.CampoTestoPop numfp;
    private ui.beans.CampoTestoPop numip;
    private javax.swing.JScrollPane pScroll;
    private javax.swing.JPanel pannelloElenco;
    private javax.swing.JPanel panpas;
    private javax.swing.JPanel pcom;
    private javax.swing.JTable tabp;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener l) {
        listener = l;
    }

    private void interrogazioneDocumenti() {
        // esegue interrogazione
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        ndoc.setText("");
        String dtpi = "", dtpf = "", dtdi = "", dtdf = "", dtri = "", dtrf = "";

        dtpi = "0000-00-00";
        dtpf = "0000-00-00";

        if (dataip.getText().trim().equals("")) {
            dtdi = "0000-00-00";
        } else {
            dtdi = (new Data(dataip.getText(), Data.GG_MM_AAAA)).formatta(Data.AAAA_MM_GG, "-");
        }
        if (datafp.getText().trim().equals("")) {
            dtdf = "0000-00-00";
        } else {
            dtdf = (new Data(datafp.getText(), Data.GG_MM_AAAA)).formatta(Data.AAAA_MM_GG, "-");
        }

        dtri = "0000-00-00";
        dtrf = "0000-00-00";

        ArrayList<Integer> docids = new ArrayList();
        if (docfatturaId != -1) {
            docids.add(docfatturaId);
        }
        if (docnotacreditoId != -1) {
            docids.add(docnotacreditoId);
        }

        if (!docids.isEmpty()) {
            lista = BusinessLogicDocumenti.ricercaDocumenti(EnvJazz.connAmbiente.conn, connAz.conn, connCom.conn, esContabile, "",
                    (int) 0, (int) 0,
                    dtpi, dtpf,
                    Formattazione.estraiIntero(numip.getText()), Formattazione.estraiIntero(numfp.getText()),
                    dtdi, dtdf,
                    "", "",
                    dtri, dtrf,
                    documentoid, -1, -1, -1, (int) 0, (int) 0,
                    (double) 0, "", "", "", (int) 0, -1, -1, -1, -1,
                    (int) 0, (int) 0, "", -1, -1, -1, -1, -1, -1, -1, true, anagrafica.getText().trim(), -1, -1, docids, -1, false, "", "", 0, 0, -1, -1, -1, -1, -1, "", -1, -1, -1, "", "");
        }
        else
        {
            lista = new ArrayList<Record>();
        }
        
        mostraDocumenti();
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

    }

    @Override
    public void azionemassiva(boolean stato) {
        if (stato) {
            for (int i = 0; i < tabp.getRowCount(); i++) {
                tabp.setValueAt(true, i, FunzioniUtilita.getColumnIndex(tabp, COL_SEL));
            }
            tabp.repaint();
        } else {
            for (int i = 0; i < tabp.getRowCount(); i++) {
                tabp.setValueAt(false, i, FunzioniUtilita.getColumnIndex(tabp, COL_SEL));
            }
            tabp.repaint();
        }

        select.cambiaIcona(stato);
    }

    private void stampa(int movdocId, String escont, Record rModStp) {

        if (!FunzioniDB.esisteReport(rModStp.leggiStringa("modstreport"))) {
            File fr = new File(rModStp.leggiStringa("modstreport"));
            if (!fr.exists()) {
                JOptionPane.showMessageDialog(formGen, "Report " + rModStp.leggiStringa("modstreport") + " non trovato", "Errore",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

                return;
            }
        }

        FunzioniDB.leggiReport(rModStp.leggiStringa("modstreport"));

        BusinessLogicDocumenti.stampaDocumento(connAz, movdocId, azienda, escont, rModStp);
        if (rModStp.leggiStringa("modstreport").toLowerCase().endsWith("jasper")) {
            DialogReportJR dr = new DialogReportJR(formGen, EnvJazz.crERW, rModStp.leggiStringa("modstreport"));
            UtilForm.posRelSchermo(dr, UtilForm.CENTRO);
            dr.show();
        }
    }

    public void passaTipoDocumento(ConnessioneServer connAz,int tipodocumento, int documentoid) 
    {
        this.connAz=connAz;
        this.tipodocumento = tipodocumento;
        this.documentoid = documentoid;
        String qry="SELECT * FROM  documenti WHERE docid = " + documentoid;
        QueryInterrogazioneClient qm = new QueryInterrogazioneClient(connAz, qry);
        qm.apertura();
        rDoc = qm.successivo();
        qm.chiusura();
    }

}
