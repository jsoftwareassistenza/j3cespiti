package cespitipop;

import java.awt.*;
import nucleo.ConnessioneServer;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogicAmbiente;

public class PannelloUtilitaRettificheManualiCesp extends javax.swing.JPanel implements PannelloPop {

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;

    private String gruppoSelez = "";
    private String specieSelez = "";
    private String azienda = "";
    private String esContabile = "";
    private int idVis = -1;

    public PannelloUtilitaRettificheManualiCesp() {
        this.initComponents();

    }

    public PannelloUtilitaRettificheManualiCesp(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        note.setText("La funzione inserisce un cespite di rettifica per quadrare i toltali di categoria con i saldi contabili\n"
                + "I valori di rettifica previsti sono:\n"
                + "Immobilizzazione (incremento e/o decremento)\n"
                + "Ammortamento senza intervento sul fondo (incremento e/o decremento)\n"
                + "Fondo(incremento e/o decremento)\n"
                + "La data di registrazione solitamente e' il 31/12/aaaa\n"
                + "Nel caso i successivi anni quadrano, si deve fare la registrazione di storno della rettifica in data 01/01/(aaaa+1)\n"
                + "Il cespite aggiunto risulta comunque NON CONGRUO ");
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        jLabel36 = new javax.swing.JLabel();
        jPasswordField1 = new javax.swing.JPasswordField();
        jLabel39 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        note = new javax.swing.JTextPane();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovaCateg = new ui.beans.PopButton();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 74));
        pFiltri.setLayout(null);

        jLabel36.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel36.setText("password");
        pFiltri.add(jLabel36);
        jLabel36.setBounds(308, 28, 192, 28);

        jPasswordField1.setText("jPasswordField1");
        pFiltri.add(jPasswordField1);
        jPasswordField1.setBounds(508, 28, 184, 28);

        jLabel39.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel39.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel39.setText("Utilita' accessibile solo operatore abilitato");
        pFiltri.add(jLabel39);
        jLabel39.setBounds(4, 28, 300, 28);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setViewportView(note);

        jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 50));
        jPanel1.setLayout(null);

        buttonNuovaCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_chevron_right_white_24dp.png"))); // NOI18N
        buttonNuovaCateg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setText("Prosegui");
        buttonNuovaCateg.setToolTipText("");
        buttonNuovaCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaCategActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovaCateg);
        buttonNuovaCateg.setBounds(508, 8, 184, 32);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonNuovaCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaCategActionPerformed
        if (!jPasswordField1.getText().equals("xxjazzyy")) {
            BusinessLogicAmbiente.mostraErrorMessage(formGen, "NON si puo' proseguire", "PASSWORD non riconosciuta");
        } else {
            DialogRettificaManualeQuadratura d = new DialogRettificaManualeQuadratura(formGen, true, connAz, azienda, esContabile);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();
        }

    }//GEN-LAST:event_buttonNuovaCategActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonNuovaCateg;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextPane note;
    private javax.swing.JPanel pFiltri;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

}
