package cespitipop;

import contabilita.pannellistart.PannelloGestioniPop;
import java.awt.*;
import nucleo.ConnessioneServer;
import javax.swing.JOptionPane;
import nucleo.EnvJazz;
import nucleo.Formattazione;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import ui.dialog.DialogUiPop;
import ui.eventi.GestoreEventiBeans;

public class PannelloParametroCespiti extends javax.swing.JPanel implements PannelloPop {

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;

    private String gruppoSelez = "";
    private String specieSelez = "";
    private String azienda = "";
    private String esContabile = "";
    private int idVis = -1;

    public PannelloParametroCespiti() {
        this.initComponents();
//        this.setSize(1120, 680);
//        this.setPreferredSize(new Dimension(1120, 680));
    }

    public PannelloParametroCespiti(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        buttonChiudiTest.setVisible(false);
//        jLabel4.setVisible(false);
        jCheckBox1.setVisible(false);
//        jCheckBox2.setVisible(false);
//        jCheckBox3.setVisible(false);
//        jCheckBox4.setVisible(false);
//        jCheckBox5.setVisible(false);
//        jCheckBox6.setVisible(false);

//        BusinessLogicImpostazioni.creazioneParametriDocumentiPop(connAz.conn, EnvJazz.connComune.conn, EnvJazz.connAmbiente.conn, EnvJazz.azienda);

        deposito.passaParametri(connAz, formGen, formGen, descrdeposito, false);
        puntoemdoc.passaParametri(connAz, formGen, formGen, descrped, false);

        annoinizjazz.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                int aaf = Formattazione.estraiIntero(annoinizjazz.getText().trim());
                annobilchiuso.setText("" + (aaf - 1));//primo anno              
            }
        });

        String prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "pem");
        String qry = "SELECT * FROM puntiemdoc WHERE pedcod = \"" + prop + "\"";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() == 1) {
            Record r = q.successivo();
            puntoemdoc.visualizzaDati(r.leggiIntero("pedid"), prop, r.leggiStringa("peddescr"));
        } else {
            puntoemdoc.svuota();
            descrped.setText("");
        }
        q.chiusura();

        annoinizjazz.setText(BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "biliniz"));

//        String qry1 = "SELECT cbseserc FROM chiusurebs ORDER BY cbseserc DESC LIMIT 01";
//        QueryInterrogazioneClient q1 = new QueryInterrogazioneClient(connAz, qry1);
//        q1.apertura();
//        Record r1 = q1.successivo();
//        if (r1 != null) {
//            annobilchiuso.setText(r1.leggiStringa("cbseserc").substring(4, 8));
//        } else {
        int aaf = Formattazione.estraiIntero(annoinizjazz.getText().trim());
        annobilchiuso.setText("" + (aaf - 1));//primo anno
//        }
//        q1.chiusura();

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "flagCriterioFiscale");
        if (prop.equals("0")) {
            flagCriterioFiscale.setSelected(false);
        } else {
            flagCriterioFiscale.setSelected(true);
        }

//        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "pnriepilogata");
//        if (prop.equals("S")) {
//            flagRiepPn.setSelected(true);
//        } else {
//            flagRiepPn.setSelected(false);
//        }
        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "art6c6");
        if (prop.equals("")) {
            art67c6.setText(prop);
        } else {
            art67c6.setText("516,46");
        }

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "jCheckBox1");
        if (prop.equals("S")) {
            jCheckBox1.setSelected(true);
        } else {
            jCheckBox1.setSelected(false);
        }

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "jCheckBox2");
        if (prop.equals("S")) {
            jCheckBox2.setSelected(true);
        } else {
            jCheckBox2.setSelected(false);
        }

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "jCheckBox3");
        if (prop.equals("S")) {
            jCheckBox3.setSelected(true);
        } else {
            jCheckBox3.setSelected(false);
        }

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "jCheckBox4");
        if (prop.equals("S")) {
            jCheckBox4.setSelected(true);
        } else {
            jCheckBox4.setSelected(false);
        }

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "jCheckBox5");
        if (prop.equals("S")) {
            jCheckBox5.setSelected(true);
        } else {
            jCheckBox5.setSelected(false);
        }

        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "jCheckBox6");
        if (prop.equals("S")) {
            jCheckBox6.setSelected(true);
        } else {
            jCheckBox6.setSelected(false);
        }

    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        jLabel33 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        annobilchiuso = new ui.beans.CampoTestoPop();
        annoinizjazz = new ui.beans.CampoTestoPop();
        jLabel37 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox6 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        jCheckBox5 = new javax.swing.JCheckBox();
        jCheckBox4 = new javax.swing.JCheckBox();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        puntoemdoc = new archivibasepop.beans.BeanGestPuntoEmDocPop();
        descrped = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        art67c6 = new ui.beans.CampoValutaPop();
        flagCriterioFiscale = new javax.swing.JCheckBox();
        jLabel38 = new javax.swing.JLabel();
        buttonMacchine = new ui.beans.PopButton();
        buttonGestioni = new ui.beans.PopButton();
        buttonAttrezzature = new ui.beans.PopButton();
        buttonAutoparco = new ui.beans.PopButton();
        buttonAttrezzature1 = new ui.beans.PopButton();
        jLabel103 = new javax.swing.JLabel();
        deposito = new archivibasepop.beans.BeanGestDepositoPop();
        descrdeposito = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        buttonSalva = new ui.beans.PopButton();
        buttonChiudiTest = new ui.beans.PopButton();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 74));
        pFiltri.setLayout(null);

        jLabel33.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel33.setText("(su proc. jazz)");
        pFiltri.add(jLabel33);
        jLabel33.setBounds(624, 8, 100, 24);

        jLabel36.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel36.setText("Primo esercizio attivato anno ");
        pFiltri.add(jLabel36);
        jLabel36.setBounds(48, 32, 200, 24);

        annobilchiuso.setBackground(new java.awt.Color(255, 255, 255));
        annobilchiuso.setEditable(false);
        pFiltri.add(annobilchiuso);
        annobilchiuso.setBounds(624, 32, 100, 24);

        annoinizjazz.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.add(annoinizjazz);
        annoinizjazz.setBounds(260, 32, 100, 24);

        jLabel37.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel37.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel37.setText("Ultimo esercizio chiuso manualmente ");
        pFiltri.add(jLabel37);
        jLabel37.setBounds(376, 32, 240, 24);

        jLabel34.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel34.setText("(su proc. jazz)");
        pFiltri.add(jLabel34);
        jLabel34.setBounds(260, 8, 100, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setLayout(null);

        jCheckBox2.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jCheckBox2.setText("Attrezzature fornite in comodato");
        jPanel7.add(jCheckBox2);
        jCheckBox2.setBounds(120, 390, 332, 27);

        jCheckBox1.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jCheckBox1.setText("Serbatoi gas in comodato");
        jPanel7.add(jCheckBox1);
        jCheckBox1.setBounds(120, 360, 292, 27);

        jCheckBox6.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox6.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jCheckBox6.setText("Gestisce centri costo/Gestioni");
        jCheckBox6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox6ActionPerformed(evt);
            }
        });
        jPanel7.add(jCheckBox6);
        jCheckBox6.setBounds(120, 330, 332, 27);

        jCheckBox3.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jCheckBox3.setText("Macchine agricole/impianti/macchinari");
        jPanel7.add(jCheckBox3);
        jCheckBox3.setBounds(120, 240, 292, 27);

        jCheckBox5.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jCheckBox5.setText("Gestisce ubicazione/Immobili diversificati");
        jPanel7.add(jCheckBox5);
        jCheckBox5.setBounds(120, 300, 292, 27);

        jCheckBox4.setBackground(new java.awt.Color(255, 255, 255));
        jCheckBox4.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jCheckBox4.setText("Gestisce automezzi/autoparco");
        jPanel7.add(jCheckBox4);
        jCheckBox4.setBounds(120, 270, 332, 27);

        jLabel4.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Applicazioni informatiche collegate  alla gestione cespiti");
        jPanel7.add(jLabel4);
        jLabel4.setBounds(44, 212, 320, 20);

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Punto em.doc. amministrativo");
        jPanel7.add(jLabel5);
        jLabel5.setBounds(40, 36, 204, 24);
        jPanel7.add(puntoemdoc);
        puntoemdoc.setBounds(260, 36, 144, 24);

        descrped.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrped.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrped.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrped.setOpaque(true);
        jPanel7.add(descrped);
        descrped.setBounds(412, 36, 312, 24);

        jLabel35.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel35.setText("Importo soglia beni 100% amm.li");
        jPanel7.add(jLabel35);
        jLabel35.setBounds(12, 76, 228, 24);

        art67c6.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        art67c6.setInteri(12);
        art67c6.setText("Immettere il valore della quota parziale che si vende");
        jPanel7.add(art67c6);
        art67c6.setBounds(260, 76, 116, 24);

        flagCriterioFiscale.setBackground(new java.awt.Color(255, 255, 255));
        flagCriterioFiscale.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        flagCriterioFiscale.setText("Tratta il binario civilistico come fiscale");
        flagCriterioFiscale.setToolTipText("Sconsigliato");
        jPanel7.add(flagCriterioFiscale);
        flagCriterioFiscale.setBounds(260, 140, 416, 27);

        jLabel38.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel38.setText("(trattamento fiscale)");
        jLabel38.setToolTipText("");
        jPanel7.add(jLabel38);
        jLabel38.setBounds(384, 76, 156, 24);

        buttonMacchine.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_build_white_20dp.png"))); // NOI18N
        buttonMacchine.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonMacchine.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonMacchine.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonMacchine.setPreferredSize(new java.awt.Dimension(92, 40));
        buttonMacchine.setText("Impianti");
        buttonMacchine.setToolTipText("Impianti e macchinari adibiti alla produzione");
        buttonMacchine.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonMacchineActionPerformed(evt);
            }
        });
        jPanel7.add(buttonMacchine);
        buttonMacchine.setBounds(452, 240, 108, 28);

        buttonGestioni.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_work_white_20dp.png"))); // NOI18N
        buttonGestioni.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonGestioni.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonGestioni.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonGestioni.setPreferredSize(new java.awt.Dimension(92, 40));
        buttonGestioni.setText("Gestioni");
        buttonGestioni.setToolTipText("Ripartizione in gestioni o centri di costo aziendali");
        buttonGestioni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGestioniActionPerformed(evt);
            }
        });
        jPanel7.add(buttonGestioni);
        buttonGestioni.setBounds(452, 330, 108, 28);

        buttonAttrezzature.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_vpn_key_white_20dp.png"))); // NOI18N
        buttonAttrezzature.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonAttrezzature.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonAttrezzature.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonAttrezzature.setPreferredSize(new java.awt.Dimension(92, 40));
        buttonAttrezzature.setText("Attrezzature");
        buttonAttrezzature.setToolTipText("Attivita' di fornitura attrezzature in comodato");
        buttonAttrezzature.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAttrezzatureActionPerformed(evt);
            }
        });
        jPanel7.add(buttonAttrezzature);
        buttonAttrezzature.setBounds(452, 390, 108, 28);

        buttonAutoparco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_notifications_white_20dp.png"))); // NOI18N
        buttonAutoparco.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonAutoparco.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonAutoparco.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonAutoparco.setPreferredSize(new java.awt.Dimension(92, 40));
        buttonAutoparco.setText("Autoparco");
        buttonAutoparco.setToolTipText("Gestione del parco automezzi");
        buttonAutoparco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAutoparcoActionPerformed(evt);
            }
        });
        jPanel7.add(buttonAutoparco);
        buttonAutoparco.setBounds(452, 270, 108, 28);

        buttonAttrezzature1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_vpn_key_white_20dp.png"))); // NOI18N
        buttonAttrezzature1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonAttrezzature1.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonAttrezzature1.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonAttrezzature1.setPreferredSize(new java.awt.Dimension(92, 40));
        buttonAttrezzature1.setText("Ubicazioni");
        buttonAttrezzature1.setToolTipText("Attivita' aziendali dislocate");
        buttonAttrezzature1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAttrezzature1ActionPerformed(evt);
            }
        });
        jPanel7.add(buttonAttrezzature1);
        buttonAttrezzature1.setBounds(452, 300, 108, 28);

        jLabel103.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel103.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel103.setText("Deposito");
        jPanel7.add(jLabel103);
        jLabel103.setBounds(100, 4, 140, 24);
        jPanel7.add(deposito);
        deposito.setBounds(260, 4, 148, 24);

        descrdeposito.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrdeposito.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrdeposito.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrdeposito.setOpaque(true);
        jPanel7.add(descrdeposito);
        descrdeposito.setBounds(412, 4, 312, 24);

        add(jPanel7, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 50));
        jPanel1.setLayout(null);

        buttonSalva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_save_white_24dp.png"))); // NOI18N
        buttonSalva.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonSalva.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonSalva.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonSalva.setPreferredSize(new java.awt.Dimension(92, 40));
        buttonSalva.setToolTipText("Salva impostazioni");
        buttonSalva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvaActionPerformed(evt);
            }
        });
        jPanel1.add(buttonSalva);
        buttonSalva.setBounds(32, 0, 48, 44);

        buttonChiudiTest.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_lock_white_24dp.png"))); // NOI18N
        buttonChiudiTest.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonChiudiTest.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonChiudiTest.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonChiudiTest.setPreferredSize(new java.awt.Dimension(92, 40));
        buttonChiudiTest.setText("chiudi test");
        buttonChiudiTest.setToolTipText("Terminato test passaggio cespiti da jazz2 a pop");
        buttonChiudiTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonChiudiTestActionPerformed(evt);
            }
        });
        jPanel1.add(buttonChiudiTest);
        buttonChiudiTest.setBounds(600, 0, 132, 44);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonSalvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvaActionPerformed
        aggiornaRecord();
        JOptionPane.showMessageDialog(this, "Salvataggio avvenuto con successo", "",
                JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

        requestFocus();
    }//GEN-LAST:event_buttonSalvaActionPerformed

    private void buttonChiudiTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonChiudiTestActionPerformed
        int ris1 = JOptionPane.showConfirmDialog(this, "Funzione valida solo per conversione da Jazz2. Prosegui?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris1 == JOptionPane.YES_OPTION) {
            //cancellazione campi OBSOLETI dalla tabella categoriebs  
//            String qry = "SELECT * FROM categoriebs LIMIT 01";
//            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
//            q.apertura();
//            Record r = q.successivo();
//            q.chiusura();
//            if (r != null) {
//                if (r.esisteCampo("catbspdcid1")) {
//                    try {
//                        String qryupdate = " ALTER TABLE categoriebs "
//                                + "DROP COLUMN catbspdcid,"
//                                + "DROP COLUMN catbspdcfondoid,"
//                                + "DROP COLUMN catbspdccostoid,"
//                                + "DROP COLUMN catbspdcid1,"
//                                + "DROP COLUMN catbspdcfondoid1";
//                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
//                        qa.esecuzione();
//                    } catch (Exception e1) {
//                        System.out.println("campi: catbspdcfondoid1,catbspdcid1 ....... non eliminati da tabella categoriebs");
//                    }
//                }
//            }
//
//            qry = "SELECT * FROM cespitiven LIMIT 01";
//            q = new QueryInterrogazioneClient(connAz, qry);
//            q.apertura();
//            r = q.successivo();
//            q.chiusura();
//
//            if (r != null) {
//                if (r.esisteCampo("vpercdediva")) {
//                    try {
//                        String qryupdate = " ALTER TABLE cespitiven "
//                                + "DROP COLUMN vpercdediva,"
//                                + "DROP COLUMN vquotaammfa,"
//                                + "DROP COLUMN cesppercFa,"
//                                + "DROP COLUMN vplusf,"
//                                + "DROP COLUMN vplusflagripf,"
//                                + "DROP COLUMN vtotammded,"
//                                + "DROP COLUMN vtotammdedt,"
//                                + "DROP COLUMN cespalienato,"
//                                + "DROP COLUMN cespinsuss";
//                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
//                        qa.esecuzione();
//                    } catch (Exception e1) {
//                        System.out.println("campi: vpercdediva,vquotaammfa etc. non eliminati da tabella cespitiven");
//                    }
//                }
//            }
        }
    }//GEN-LAST:event_buttonChiudiTestActionPerformed

    private void buttonMacchineActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonMacchineActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonMacchineActionPerformed

    private void buttonGestioniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGestioniActionPerformed
        DialogUiPop dx = new DialogUiPop(formGen, true, "Az: " + EnvJazz.azienda + "  Es.:" + EnvJazz.esContabile.substring(0, 4) + "/" + EnvJazz.esContabile.substring(4, 8) + "  Funzioni Centri Costo/Gestioni aziendali",
                "Immob. " + EnvJazz.azienda + EnvJazz.esContabile, Color.BLACK, Color.BLACK, jPanel1);
        PannelloGestioniPop pgestdoc = new PannelloGestioniPop(dx, formGen);
        dx.impostaPannello(pgestdoc);
        pgestdoc.inizializza(connAz, connCom, EnvJazz.azienda, EnvJazz.esContabile, -1, null, "Cespiti");
        dx.pack();
        UtilForm.posRelSchermo(dx, UtilForm.CENTRO);
        dx.setVisible(true);
    }//GEN-LAST:event_buttonGestioniActionPerformed

    private void buttonAttrezzatureActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAttrezzatureActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonAttrezzatureActionPerformed

    private void buttonAutoparcoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAutoparcoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonAutoparcoActionPerformed

    private void buttonAttrezzature1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAttrezzature1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_buttonAttrezzature1ActionPerformed

    private void jCheckBox6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox6ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.CampoTestoPop annobilchiuso;
    private ui.beans.CampoTestoPop annoinizjazz;
    private ui.beans.CampoValutaPop art67c6;
    private ui.beans.PopButton buttonAttrezzature;
    private ui.beans.PopButton buttonAttrezzature1;
    private ui.beans.PopButton buttonAutoparco;
    private ui.beans.PopButton buttonChiudiTest;
    private ui.beans.PopButton buttonGestioni;
    private ui.beans.PopButton buttonMacchine;
    private ui.beans.PopButton buttonSalva;
    private archivibasepop.beans.BeanGestDepositoPop deposito;
    private javax.swing.JLabel descrdeposito;
    private javax.swing.JLabel descrped;
    private javax.swing.JCheckBox flagCriterioFiscale;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JCheckBox jCheckBox4;
    private javax.swing.JCheckBox jCheckBox5;
    private javax.swing.JCheckBox jCheckBox6;
    private javax.swing.JLabel jLabel103;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel pFiltri;
    private archivibasepop.beans.BeanGestPuntoEmDocPop puntoemdoc;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void aggiornaRecord() {
        String tmp = "";
        if (jCheckBox1.isSelected()) {
            tmp = "S";
        } else {
            tmp = "N";
        }
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "jCheckBox1", tmp);

        if (jCheckBox2.isSelected()) {
            tmp = "S";
        } else {
            tmp = "N";
        }
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "jCheckBox2", tmp);

        if (jCheckBox3.isSelected()) {
            tmp = "S";
        } else {
            tmp = "N";
        }
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "jCheckBox3", tmp);

        if (jCheckBox4.isSelected()) {
            tmp = "S";
        } else {
            tmp = "N";
        }
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "jCheckBox4", tmp);

        if (jCheckBox5.isSelected()) {
            tmp = "S";
        } else {
            tmp = "N";
        }
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "jCheckBox5", tmp);

        if (jCheckBox6.isSelected()) {
            tmp = "S";
        } else {
            tmp = "N";
        }
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "jCheckBox6", tmp);

        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "art6c6", art67c6.getText().trim());

        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "biliniz", annoinizjazz.getText().trim());

//        //prima nota ammortamenti riepilogata sul conto
//        if (flagRiepPn.isSelected()) {
//            BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "pnriepilogata", "S");
//        } else {
//            BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "pnriepilogata", "N");
//        }

        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "docpop", "deposito", deposito.leggiCodice());
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "docpop", "puntoemdoc", puntoemdoc.leggiCodice());
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "pem", puntoemdoc.leggiCodice());

        if (flagCriterioFiscale.isSelected()) {
            BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "flagCriterioFiscale", "1");
        } else {
            BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "flagCriterioFiscale", "0");
        }

    }
}
