package cespitipop.disuso;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import nucleo.ConnessioneServer;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;

import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import ui.beans.PopButton;
import ui.beans.TablePopButton;

public class PannelloClassiDiManutenzione extends javax.swing.JPanel implements PannelloPop {

    private final String COL_COD = "Cod.";
    private final String COL_DTI = "Val. da";
    private final String COL_DTF = "a data";
    private final String COL_FABBR = "% ";
    private final String COL_MAX = "Massimale ";
    private final String COL_STATO = "Stato";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;

    private int idVis = -1;
    private int idSelez = -1;
    private String key = "";

    private String azienda = "";
    private String esContabile = "";
    boolean primavolta = true;
    private int[] id;

    String[] dtes = null;
    Data oggi = new Data();

    ArrayList<Record> vclasseid = new ArrayList();

    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloClassiDiManutenzione() {
        this.initComponents();
    }

     public PannelloClassiDiManutenzione(Frame formGen) {
        this();
        this.formGen = formGen;
    }
     
    public PannelloClassiDiManutenzione(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.key = key;

        interrogazione(idVis, key);
        primavolta = false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovaCateg = new ui.beans.PopButton();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 40));
        jPanel1.setLayout(null);

        buttonNuovaCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovaCateg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setToolTipText("Nuova sottocategoria con eccezioni rispetto allo standard");
        buttonNuovaCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaCategActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovaCateg);
        buttonNuovaCateg.setBounds(32, 4, 56, 32);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];

            DialogClassiManutenzioniCespiti d = new DialogClassiManutenzioniCespiti(formGen, true, connAz, connCom, azienda, esContabile, idSelez);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();
            requestFocus();

            interrogazione(idVis, key);
        }
    }//GEN-LAST:event_tabMouseClicked

    private void buttonNuovaCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaCategActionPerformed
        DialogClassiManutenzioniCespiti d = new DialogClassiManutenzioniCespiti(formGen, true, connAz, connCom, azienda, esContabile, -1);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        requestFocus();

        interrogazione(idVis, key);
    }//GEN-LAST:event_buttonNuovaCategActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonNuovaCateg;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void interrogazione(int idVis, String key) {
        vclasseid = new ArrayList();
        String qry = "SELECT * FROM bsclassimanut WHERE bsclmanutstato = 1";
        if (!key.equals("")) {
            qry += " AND bsclmanutcod = \"" + key + "\"";
        }
        if (idVis > 0) {
            qry += " AND bsclmanutid = " + idVis;
        }
        qry += " ORDER BY bsclmanutcod";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vclasseid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        
        mostraLista();
    }

    private void mostraLista() {

        final String[] names = {
            COL_COD, COL_DTI, COL_DTF, COL_FABBR, COL_MAX, COL_STATO
        };
        final Object[][] data = new Object[vclasseid.size()][names.length];

        id = new int[vclasseid.size()];

        for (int i = 0; i < vclasseid.size(); i++) {
            Record rec = vclasseid.get(i);
            data[i][0] = rec.leggiStringa("bsclmanutcod");

            data[i][1] = (new Data(rec.leggiStringa("bsclmanutdtiniz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][2] = (new Data(rec.leggiStringa("bsclmanutdtfine"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");

            data[i][3] = Formattazione.formValuta(rec.leggiDouble("bsclmanutquota"), 3, 2, 0);
            data[i][4] = Formattazione.formValuta(rec.leggiDouble("bsclmanutmassimale"), 8, 2, 0);

            id[i] = rec.leggiIntero("bsclmanutid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_STATO)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tab, true);

        TableColumn buttonColumn3 = tab.getColumn(COL_STATO);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                button.setToolTipText("Disattiva/elimina classe manutenzione");
            }
        });

        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                boolean ko = chiediConfermaElim(id[row]);
                if (ko) {
                    interrogazione(-1, key);
                }
            }
        });
        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        tab.getColumn(COL_COD).setPreferredWidth(60);
        tab.getColumn(COL_DTI).setPreferredWidth(80);
        tab.getColumn(COL_DTF).setPreferredWidth(80);
        tab.getColumn(COL_FABBR).setPreferredWidth(52);
        tab.getColumn(COL_MAX).setPreferredWidth(90);
        tab.getColumn(COL_STATO).setPreferredWidth(30);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_MAX).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tab.getColumn(COL_DTI).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DTF).setCellRenderer(centerRenderer);
        tab.getColumn(COL_FABBR).setCellRenderer(centerRenderer);

        tab.getColumn(COL_STATO).setHeaderRenderer(headerRenderer);

    }

    final boolean chiediConfermaElim(int idelim) {
        boolean eli = false;
        int ris = JOptionPane.showConfirmDialog(this, "Confermi l'eliminazione definitiva della classe?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) 
        {
            String qrydelete = "DELETE FROM bsclassimanut WHERE bsclmanutid =" + idelim;
            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
            qd.esecuzione();

            eli = true;
            JOptionPane.showMessageDialog(this, "Classe ELIMINATA ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        } else {
            ris = JOptionPane.showConfirmDialog(this, "Confermi la disattivazione della classe?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (ris == JOptionPane.YES_OPTION) {
                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, "UPDATE bsclassimanut SET bsclmanutstato = 0 WHERE bsclmanutid =" + idelim);
                qd.esecuzione();
                JOptionPane.showMessageDialog(formGen, "Classe disattivata ", "Invio",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                eli = true;
            }
        }
        return eli;
    }

}
