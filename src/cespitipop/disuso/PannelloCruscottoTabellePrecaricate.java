/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cespitipop.disuso;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.swing.JLabel;
import javax.swing.JPanel;
import nucleo.ConnessioneServer;
import nucleo.EnvJazz;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.Record;
import nucleo.WaitPop;
import ui.beans.GestTabPop;
import ui.beans.PannelloMenuPop;
import ui.beans.PannelloMenuPopListener;
import ui.beans.PopButton;
import ui.dialog.DialogUiPop;

/**
 *
 * @author pgx71
 */
public class PannelloCruscottoTabellePrecaricate extends javax.swing.JPanel implements PannelloPop, PannelloMenuPopListener, PannelloPopListener
{
    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;      
    
    public Window padre;
    private Frame formGen;
    private JLabel labelWait;
    private WaitPop wait;
    private PannelloPopListener listener = null;
    static public HashMap<String, JPanel> hpannelli = null;
    
    private String azienda="";
    private String esContabile="";
    private String pwdok = "";    
    private String key = "";
    
    private Record rposfisc = null;
    private Record rstab = null;
    
    public static final int UI_MODOCRUSCOTTO_SCHERMOINTERO = 0;
    public static final int UI_MODOCRUSCOTTO_BARRAMENU = 1;
    public int ui_modocruscotto = UI_MODOCRUSCOTTO_SCHERMOINTERO;
    private String utente = "";

    /**
     * Creates new form PannelloAnnullamentiEadPop
     */
    public PannelloCruscottoTabellePrecaricate()
    {
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public PannelloCruscottoTabellePrecaricate(Frame formGen)
    {
        this();
        this.formGen = formGen;
    }

    public PannelloCruscottoTabellePrecaricate(Window padre, Frame formGen)
    {
        this();
        this.padre = padre;
        this.formGen = formGen;
    }

    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key)
    {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.wait = wait;
        this.key = key;

        hpannelli = new HashMap();
        pmenu.impostaListener(this);
        
        
        inizializzaMenuBase();
    }

    
    private void inizializzaMenuBase() {
        int gfont = grandezzaFontMenu();

        pmenu.impostaListener(this);

        PopButton bm1 = new PopButton();
        bm1.setText("Gruppi/specie cespiti");
        bm1.setBackground(new Color(0, 77, 106));
        bm1.setDefaultBackground(new Color(0, 77, 106));
        bm1.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm1.setForeground(Color.WHITE);
        bm1.setHighlightedColor(new Color(7, 110, 149));
        bm1.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm1.setFocusPainted(false);
        bm1.setActionKey("gruppispeciecesp");
        ArrayList<PopButton> vSottoMenum1 = new ArrayList();
        pmenu.aggiungiPopButton(bm1, vSottoMenum1);
        
        PopButton bm2 = new PopButton();
        bm2.setText("Categorie precaricate cespiti");
        bm2.setBackground(new Color(0, 77, 106));
        bm2.setDefaultBackground(new Color(0, 77, 106));
        bm2.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm2.setForeground(Color.WHITE);
        bm2.setHighlightedColor(new Color(7, 110, 149));
        bm2.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm2.setFocusPainted(false);
        bm2.setActionKey("categspecialicesp");
        ArrayList<PopButton> vSottoMenum2 = new ArrayList();
        pmenu.aggiungiPopButton(bm2, vSottoMenum2);
        
        PopButton bm3 = new PopButton();
        bm3.setText("Classi manutenzione cespiti");
        bm3.setBackground(new Color(0, 77, 106));
        bm3.setDefaultBackground(new Color(0, 77, 106));
        bm3.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm3.setForeground(Color.WHITE);
        bm3.setHighlightedColor(new Color(7, 110, 149));
        bm3.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm3.setFocusPainted(false);
        bm3.setActionKey("classimanutcesp");
        ArrayList<PopButton> vSottoMenum3 = new ArrayList();
        pmenu.aggiungiPopButton(bm3, vSottoMenum3);
        
        PopButton bm4 = new PopButton();
        bm4.setText("Collegamento a bilancio Cee");
        bm4.setBackground(new Color(0, 77, 106));
        bm4.setDefaultBackground(new Color(0, 77, 106));
        bm4.setDisabledBackgroundColor(Color.DARK_GRAY);
        bm4.setForeground(Color.WHITE);
        bm4.setHighlightedColor(new Color(7, 110, 149));
        bm4.setFont(new java.awt.Font("Dialog", Font.BOLD, gfont));
        bm4.setFocusPainted(false);
        bm4.setActionKey("bilanciocesp");
        ArrayList<PopButton> vSottoMenum4 = new ArrayList();
        pmenu.aggiungiPopButton(bm4, vSottoMenum4);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ptesta = new javax.swing.JPanel();
        buttonTuttoSchermo = new ui.beans.PopButton();
        pmenu = new ui.beans.PannelloMenuPop();
        pwork = new javax.swing.JPanel();
        pTab = new javax.swing.JTabbedPane();

        setMaximumSize(new java.awt.Dimension(900, 660));
        setMinimumSize(new java.awt.Dimension(900, 660));
        setPreferredSize(new java.awt.Dimension(900, 660));
        setLayout(new java.awt.BorderLayout());

        ptesta.setBackground(java.awt.Color.white);
        ptesta.setPreferredSize(new java.awt.Dimension(900, 64));
        ptesta.setLayout(new java.awt.BorderLayout(4, 0));

        buttonTuttoSchermo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_zoom_out_map_black_24dp.png"))); // NOI18N
        buttonTuttoSchermo.setBackground(java.awt.Color.white);
        buttonTuttoSchermo.setBorderPainted(false);
        buttonTuttoSchermo.setFocusPainted(false);
        buttonTuttoSchermo.setHighlightedColor(new java.awt.Color(246, 246, 246));
        buttonTuttoSchermo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        buttonTuttoSchermo.setPreferredSize(new java.awt.Dimension(40, 25));
        buttonTuttoSchermo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonTuttoSchermoActionPerformed(evt);
            }
        });
        ptesta.add(buttonTuttoSchermo, java.awt.BorderLayout.WEST);
        ptesta.add(pmenu, java.awt.BorderLayout.CENTER);

        add(ptesta, java.awt.BorderLayout.NORTH);

        pwork.setLayout(new java.awt.GridLayout(1, 1));
        pwork.add(pTab);

        add(pwork, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonTuttoSchermoActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_buttonTuttoSchermoActionPerformed
    {//GEN-HEADEREND:event_buttonTuttoSchermoActionPerformed
        if (padre != null)
        {
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            padre.setSize((int) d.getWidth(), (int) d.getHeight() - 36);
            padre.setLocation(0, 0);
            padre.paintAll(padre.getGraphics());
        }
    }//GEN-LAST:event_buttonTuttoSchermoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonTuttoSchermo;
    private javax.swing.JTabbedPane pTab;
    private ui.beans.PannelloMenuPop pmenu;
    private javax.swing.JPanel ptesta;
    private javax.swing.JPanel pwork;
    // End of variables declaration//GEN-END:variables

    public void impostaListener(PannelloPopListener l)
    {
        listener = l;
    }

    @Override
    public void notificaClickPulsanteMenuPop(PannelloMenuPop origine, String key)
    {
        System.out.println("EVENTO MENU:" + key);
        int dimschermo = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        int maxalt = dimschermo - ((int) this.getLocationOnScreen().getY() + 64 + 64);
        if (key.equals("gruppispeciecesp"))
        {
            PannelloManutenzioneGruppiSpecie p = new PannelloManutenzioneGruppiSpecie(formGen);
            p.inizializza(connAz, connCom, EnvJazz.azienda, esContabile, -1, wait, "");
            //plistaordini.impostaParametri(pwdok);
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO)
            {
                hpannelli.put(keyass, p);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Gruppi e specie cespiti", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) p, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else
            {
                DialogUiPop dx = new DialogUiPop(formGen, false, p, "Gruppi e specie cespiti", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(p);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt)
                    dx.setSize(dx.getWidth(), maxalt);
                dx.setVisible(true);
            }
        }
        else if (key.equals("categspecialicesp"))
        {
            PannelloCategorieCespitiPrecaricate p = new PannelloCategorieCespitiPrecaricate(formGen);
            p.inizializza(connAz, connCom, EnvJazz.azienda, esContabile, -1, null, "");
            //plistaordini.impostaParametri(pwdok);
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO)
            {
                hpannelli.put(keyass, p);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Categorie precaricate cespiti", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) p, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else
            {
                DialogUiPop dx = new DialogUiPop(formGen, false, p, "Categorie precaricate cespiti", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(p);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt)
                    dx.setSize(dx.getWidth(), maxalt);
                dx.setVisible(true);
            }
        }else if (key.equals("classimanutcesp"))
        {
            PannelloClassiDiManutenzione p = new PannelloClassiDiManutenzione(formGen);
            p.inizializza(connAz, connCom, EnvJazz.azienda, esContabile, -1, null, "");
             //plistaordini.impostaParametri(pwdok);
            String keyass = calcolaChiavePannello(key, true);
            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO)
            {
                hpannelli.put(keyass, p);
                int ind = pTab.getTabCount();
                GestTabPop pgesttab = new GestTabPop(formGen, "Classi costi manutenzione", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
                pTab.add((JPanel) p, ind);
                pTab.setTabComponentAt(ind, pgesttab);
                pTab.setSelectedIndex(ind);
                pTab.paintAll(pTab.getGraphics());
            } else
            {
                DialogUiPop dx = new DialogUiPop(formGen, false, p, "Classi costi manutenzione", keyass, new Color(0, 77, 106), Color.BLACK);
                dx.getContentPane().add(p);
                dx.pack();
                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
                if (dx.getHeight() > maxalt)
                    dx.setSize(dx.getWidth(), maxalt);
                dx.setVisible(true);
            }
        }else if (key.equals("bilanciocesp"))
        {
//            PannelloBilancioCeeCesp p = new PannelloBilancioCeeCesp(formGen);
//            p.inizializza(connAz, connCom, azienda, esContabile);
//            String keyass = calcolaChiavePannello(key, true);
//            if (ui_modocruscotto == UI_MODOCRUSCOTTO_SCHERMOINTERO)
//            {
//                hpannelli.put(keyass, p);
//                int ind = pTab.getTabCount();
//                GestTabPop pgesttab = new GestTabPop(formGen, "Collegamento a bilancio Cee", pTab, ind, keyass, hpannelli, true, true, new Color(0, 77, 106), Color.BLACK);
//                pTab.add((JPanel) p, ind);
//                pTab.setTabComponentAt(ind, pgesttab);
//                pTab.setSelectedIndex(ind);
//                pTab.paintAll(pTab.getGraphics());
//            } else
//            {
//                DialogUiPop dx = new DialogUiPop(formGen, false, p, "Collegamento a bilancio Cee", keyass, new Color(0, 77, 106), Color.BLACK);
//                dx.getContentPane().add(p);
//                dx.pack();
//                dx.setLocation((int) this.getLocationOnScreen().getX(), (int) this.getLocationOnScreen().getY() + 68);
//                if (dx.getHeight() > maxalt)
//                    dx.setSize(dx.getWidth(), maxalt);
//                dx.setVisible(true);
//            }    
        }        
    }

    private int grandezzaFontMenu() {
        int gfont = 16;
        if (EnvJazz.risoluzioneX > 1440) {
            gfont = 16;
        }
        if (EnvJazz.risoluzioneX > 1024) {
            gfont = 14;
        } else {
            gfont = 12;
        }
        return gfont;
    }

    @Override
    public void notificaChiusura(PannelloPop origine, String key)
    {
        System.out.println("CHIUSURA PANNELLO " + key);
        if (hpannelli.get(key) != null)
        {
            for (int i = 0; i < pTab.getComponentCount(); i++)
            {
                if (pTab.getComponentAt(i).equals(origine))
                {
                    pTab.remove(i);
                    break;
                }
            }
            hpannelli.remove(key);
        }
//
//        xpwork.removeAll();
//        this.paintAll(this.getGraphics());
//        repaint();
    }

    @Override
    public void notificaCambioTitolo(PannelloPop origine, String key, String nuovoTitolo)
    {
    }

    private String calcolaChiavePannello(String key, boolean nuovatab)
    {
        String keyass = key;
        if (nuovatab)
        {
            boolean keyok = false;
            int keyprog = 0;
            while (!keyok)
            {
                keyprog++;
                String k = key + "_" + keyprog;
                boolean ktrovata = false;
                Iterator it = hpannelli.keySet().iterator();
                while (it.hasNext())
                {
                    String k2 = (String) it.next();
                    if (k2.equals(k))
                    {
                        ktrovata = true;
                    }
                }
                if (!ktrovata)
                {
                    keyass = k;
                    keyok = true;
                }
            }
        }
        return keyass;
    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo)
    {
    }
    
     @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo,Color coloresfondo,int idVis)
    {
    }
}
