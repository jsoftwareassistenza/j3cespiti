package cespitipop.disuso;

/*
 * DialogCategCespiti.java
 *
 * Created on 8 novembre 2006, 15.21
 */
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import nucleo.ConnessioneServer;
import nucleo.EnvJazz;
import nucleo.Formattazione;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.UtilForm;
import ui.eventi.GestoreEventiBeans;

/**
 *
 * @author boss
 */
public class DialogManutenzioneGruppiSpecie extends javax.swing.JDialog {

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    Frame formGen = null;

    private String esContabile = "";
    private String azienda = "";

    private int[] idGru;
    private int[] idSpec;
    private int[] idCat;
    private String[] mcsSpec;
    private String[] mcsGru;
    private String[] mcsCat;
    private String eccezione = "";
    private int idVis = -1;
    private int pwd = -1;
    ArrayList<Record> vcatbsid = new ArrayList();
    ArrayList<Record> vcee = new ArrayList();
    int esito = EnvJazz.PASSWORD_ERRATA;
    Record rcateg = new Record();
    boolean duplicazione = false;

    /**
     * Creates new form DialogCategCespiti
     */
    public DialogManutenzioneGruppiSpecie(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public DialogManutenzioneGruppiSpecie(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom,
            String azienda, String esContabile, int idVis, int pwd) {
        this(parent, modal);
        this.formGen = parent;
        this.connAz = connAz;
        this.connCom = connAz;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.pwd = pwd;

        password.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                esito = EnvJazz.PASSWORD_ERRATA;
                if (new String(password.getText()).equals("xxjazzyy")) {
                    esito = EnvJazz.SUCCESSO;
                }

                if (esito != EnvJazz.SUCCESSO) {
                    buttonSalva.setVisible(false);
                } else {
                    buttonSalva.setVisible(true);
                }
            }
        });

        bsmastro.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (idVis < 1 && !bsmastro.getText().trim().equals("")) {
                    String qry = "SELECT gruppobsid,gruppobsdescr,gruppobsbil FROM gruppibs WHERE gruppobsid > 0"
                            + " AND gruppobsmastro = \"" + bsmastro.getText().trim() + "\""
                            + " AND gruppobsconto =  \"" + bsconto.getText().trim() + "\""
                            + " AND gruppobssotto =  \"" + bssotto.getText().trim() + "\""
                            + " AND gruppobsd = \"\" AND gruppobsecc = \"\""
                            + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                            + " ORDER BY gruppobsmcs DESC LIMIT 01";
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record r = qRic.successivo();
                    if (r != null) {
                        mostraRecord(r.leggiIntero("gruppobsid"));
                        comboGruppi.removeAllItems();
                        comboGruppi.addItem(r.leggiStringa("gruppobsbil") + " - " + r.leggiStringa("gruppobsdescr"));
                        comboGruppi.setSelectedIndex(0);
                        comboGruppi.setEnabled(false);
                        buttonInsGruppo.setEnabled(false);
                    }
                    qRic.chiusura();

                }
            }
        });

        bsconto.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (idVis < 1 && !bsmastro.getText().trim().equals("") && !bsconto.getText().trim().equals("")) {
                    String qry = "SELECT gruppobsid,gruppobsdescr FROM gruppibs WHERE gruppobsid > 0"
                            + " AND gruppobsmastro = \"" + bsmastro.getText().trim() + "\""
                            + " AND gruppobsconto =  \"" + bsconto.getText().trim() + "\""
                            + " AND gruppobssotto =  \"" + bssotto.getText().trim() + "\""
                            + " AND gruppobsd = \"\" AND gruppobsecc = \"\""
                            + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                            + " ORDER BY gruppobsmcs DESC LIMIT 01";
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record r = qRic.successivo();
                    if (r != null) {
                        mostraRecord(r.leggiIntero("gruppobsid"));
                        comboSpecie.removeAllItems();
                        comboSpecie.addItem(r.leggiStringa("gruppobsdescr"));
                        comboSpecie.setSelectedIndex(0);
                        comboSpecie.setEnabled(false);
                        buttonInsSpecie.setEnabled(false);
                    }
                    qRic.chiusura();
                }
            }
        });

        bssotto.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (idVis < 1 && !bsmastro.getText().trim().equals("") && !bsconto.getText().trim().equals("") && !bssotto.getText().trim().equals("")) {
                    String qry = "SELECT gruppobsid,gruppobscateg FROM gruppibs WHERE gruppobsid > 0"
                            + " AND gruppobsmastro = \"" + bsmastro.getText().trim() + "\""
                            + " AND gruppobsconto =  \"" + bsconto.getText().trim() + "\""
                            + " AND gruppobssotto =  \"" + bssotto.getText().trim() + "\""
                            + " AND gruppobsd = \"\" AND gruppobsecc = \"\""
                            + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                            + " ORDER BY gruppobsmcs DESC LIMIT 01";
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record r = qRic.successivo();
                    if (r != null) {
                        mostraRecord(r.leggiIntero("gruppobsid"));
                        combocategorie.removeAllItems();
                        combocategorie.addItem(r.leggiStringa("gruppobscateg"));
                        combocategorie.setSelectedIndex(0);
                        combocategorie.setEnabled(false);
                        buttonInsCateg.setEnabled(false);
                    }
                    qRic.chiusura();
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        bsmastro = new ui.beans.CampoTestoPop();
        bsconto = new ui.beans.CampoTestoPop();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        bsdescr = new ui.beans.CampoTestoPop();
        comboGruppi = new javax.swing.JComboBox();
        comboSpecie = new javax.swing.JComboBox();
        buttonInsSpecie = new ui.beans.PopButton();
        buttonInsGruppo = new ui.beans.PopButton();
        catbstipo = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        bsnote = new ui.beans.CampoTestoPop();
        jLabel17 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        bsaliq = new ui.beans.CampoValutaPop();
        bsstato = new javax.swing.JCheckBox();
        jLabel19 = new javax.swing.JLabel();
        bssotto = new ui.beans.CampoTestoPop();
        jLabel11 = new javax.swing.JLabel();
        bssottod = new ui.beans.CampoTestoPop();
        buttonInsCateg = new ui.beans.PopButton();
        buttonInsSottocat = new ui.beans.PopButton();
        bscee = new ui.beans.CampoTestoPop();
        combocee = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        combocategorie = new javax.swing.JComboBox<>();
        nota_1 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        bscateg = new ui.beans.CampoTestoPop();
        labeccezioni = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        buttonVisPrecaricate = new ui.beans.PopButton();
        jPanel8 = new javax.swing.JPanel();
        buttonSalva = new ui.beans.PopButton();
        password = new ui.beans.CampoPasswordPop();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("IMMOBILIZZAZIONI: Categorie civilistiche + categorie interne");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Titolo", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 200));
        jPanel1.setLayout(null);

        bsmastro.setBackground(new java.awt.Color(255, 255, 255));
        bsmastro.setCaratteri(20);
        jPanel1.add(bsmastro);
        bsmastro.setBounds(164, 68, 80, 24);

        bsconto.setBackground(new java.awt.Color(255, 255, 255));
        bsconto.setCaratteri(4);
        jPanel1.add(bsconto);
        bsconto.setBounds(164, 104, 52, 24);

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("------------>");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(76, 20, 72, 24);

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Specie");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(72, 104, 72, 24);

        jLabel18.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setText("Descrizione Gruppo/Specie");
        jPanel1.add(jLabel18);
        jLabel18.setBounds(16, 140, 200, 24);

        bsdescr.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(bsdescr);
        bsdescr.setBounds(16, 164, 768, 24);

        comboGruppi.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboGruppi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGruppiActionPerformed(evt);
            }
        });
        jPanel1.add(comboGruppi);
        comboGruppi.setBounds(292, 68, 492, 24);

        comboSpecie.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboSpecie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSpecieActionPerformed(evt);
            }
        });
        jPanel1.add(comboSpecie);
        comboSpecie.setBounds(256, 104, 528, 24);

        buttonInsSpecie.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_add_white_20dp.png"))); // NOI18N
        buttonInsSpecie.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonInsSpecie.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonInsSpecie.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonInsSpecie.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonInsSpecie.setToolTipText("Inserisci nuova specie");
        buttonInsSpecie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsSpecieActionPerformed(evt);
            }
        });
        jPanel1.add(buttonInsSpecie);
        buttonInsSpecie.setBounds(224, 104, 24, 24);

        buttonInsGruppo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_add_white_20dp.png"))); // NOI18N
        buttonInsGruppo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonInsGruppo.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonInsGruppo.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonInsGruppo.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonInsGruppo.setToolTipText("Inserisci un nuovo gruppo");
        buttonInsGruppo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsGruppoActionPerformed(evt);
            }
        });
        jPanel1.add(buttonInsGruppo);
        buttonInsGruppo.setBounds(256, 68, 24, 24);

        catbstipo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        catbstipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Materiali", "Immateriali", "Finanziarie", "<  .....  >", " " }));
        catbstipo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                catbstipoItemStateChanged(evt);
            }
        });
        jPanel1.add(catbstipo);
        catbstipo.setBounds(164, 20, 132, 24);

        jLabel20.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("Gruppo");
        jPanel1.add(jLabel20);
        jLabel20.setBounds(76, 68, 72, 24);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Categoria", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        jPanel2.setLayout(null);

        bsnote.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.add(bsnote);
        bsnote.setBounds(16, 232, 764, 24);

        jLabel17.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel17.setText("Descizione dettaglio");
        jPanel2.add(jLabel17);
        jLabel17.setBounds(16, 208, 168, 24);

        jLabel31.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel31.setText("Coefficiente ministeriale %");
        jPanel2.add(jLabel31);
        jLabel31.setBounds(68, 280, 168, 24);

        bsaliq.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        bsaliq.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        bsaliq.setInteri(3);
        jPanel2.add(bsaliq);
        bsaliq.setBounds(256, 280, 60, 24);

        bsstato.setBackground(new java.awt.Color(255, 255, 255));
        bsstato.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        bsstato.setText("Attiva");
        jPanel2.add(bsstato);
        bsstato.setBounds(336, 280, 136, 27);

        jLabel19.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Categoria");
        jPanel2.add(jLabel19);
        jLabel19.setBounds(168, 60, 72, 24);

        bssotto.setBackground(new java.awt.Color(255, 255, 255));
        bssotto.setCaratteri(2);
        jPanel2.add(bssotto);
        bssotto.setBounds(256, 60, 60, 24);

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Sottocategoria dettaglio");
        jPanel2.add(jLabel11);
        jLabel11.setBounds(68, 176, 168, 24);

        bssottod.setBackground(new java.awt.Color(255, 255, 255));
        bssottod.setCaratteri(2);
        jPanel2.add(bssottod);
        bssottod.setBounds(256, 176, 60, 24);

        buttonInsCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_add_white_20dp.png"))); // NOI18N
        buttonInsCateg.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonInsCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonInsCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonInsCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonInsCateg.setToolTipText("Inserisci nuova categoria (DUPLICA da richiamata)");
        buttonInsCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsCategActionPerformed(evt);
            }
        });
        jPanel2.add(buttonInsCateg);
        buttonInsCateg.setBounds(336, 60, 24, 24);

        buttonInsSottocat.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_add_white_20dp.png"))); // NOI18N
        buttonInsSottocat.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonInsSottocat.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonInsSottocat.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonInsSottocat.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonInsSottocat.setToolTipText("Inserisci nuova sottocategoria di dettaglio (DUPLICA da richiamata)");
        buttonInsSottocat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsSottocatActionPerformed(evt);
            }
        });
        jPanel2.add(buttonInsSottocat);
        buttonInsSottocat.setBounds(336, 176, 24, 24);

        bscee.setBackground(new java.awt.Color(255, 255, 255));
        bscee.setCaratteri(20);
        bscee.setEditable(false);
        jPanel2.add(bscee);
        bscee.setBounds(256, 24, 104, 24);

        combocee.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        combocee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboceeActionPerformed(evt);
            }
        });
        jPanel2.add(combocee);
        combocee.setBounds(372, 24, 408, 24);

        jLabel15.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Codice voce CEE");
        jPanel2.add(jLabel15);
        jLabel15.setBounds(112, 24, 128, 24);

        combocategorie.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        combocategorie.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                combocategorieItemStateChanged(evt);
            }
        });
        combocategorie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                combocategorieActionPerformed(evt);
            }
        });
        jPanel2.add(combocategorie);
        combocategorie.setBounds(372, 60, 408, 24);

        nota_1.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        nota_1.setText("(duplica per creare un dettaglio)");
        jPanel2.add(nota_1);
        nota_1.setBounds(376, 176, 184, 24);

        jLabel21.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel21.setText("Descizione categoria");
        jPanel2.add(jLabel21);
        jLabel21.setBounds(16, 84, 180, 24);

        bscateg.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.add(bscateg);
        bscateg.setBounds(16, 108, 764, 24);

        labeccezioni.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        labeccezioni.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labeccezioni.setText("Visualizza precaricate");
        jPanel2.add(labeccezioni);
        labeccezioni.setBounds(580, 276, 140, 24);

        jLabel4.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        jLabel4.setText("Sottocategorie libere");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(12, 148, 120, 24);

        buttonVisPrecaricate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_search_white_20dp.png"))); // NOI18N
        buttonVisPrecaricate.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonVisPrecaricate.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonVisPrecaricate.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonVisPrecaricate.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonVisPrecaricate.setToolTipText("Inserisci nuova sottocategoria di dettaglio (DUPLICA da richiamata)");
        buttonVisPrecaricate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonVisPrecaricateActionPerformed(evt);
            }
        });
        jPanel2.add(buttonVisPrecaricate);
        buttonVisPrecaricate.setBounds(728, 276, 52, 24);

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setPreferredSize(new java.awt.Dimension(100, 50));
        jPanel8.setLayout(null);

        buttonSalva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_save_white_20dp.png"))); // NOI18N
        buttonSalva.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonSalva.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonSalva.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonSalva.setPreferredSize(new java.awt.Dimension(80, 40));
        buttonSalva.setToolTipText("Salva le informazioni");
        buttonSalva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvaActionPerformed(evt);
            }
        });
        jPanel8.add(buttonSalva);
        buttonSalva.setBounds(4, 4, 40, 40);

        password.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
        password.setToolTipText("parola chiave accesso associata all'operatore");
        password.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                passwordFocusLost(evt);
            }
        });
        jPanel8.add(password);
        password.setBounds(256, 12, 256, 28);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Password");
        jPanel8.add(jLabel3);
        jLabel3.setBounds(144, 12, 88, 28);

        getContentPane().add(jPanel8, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(833, 621));
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        password.setText("");
        password.requestFocus();
        esito = pwd;

        if (esito == EnvJazz.SUCCESSO) {
            password.setText("xxjazzyy");
            buttonSalva.setVisible(true);
        } else {
            buttonSalva.setVisible(false);
        }

        popolaVociCee();

        if (idVis > 0) {
            mostraRecord(idVis);

            catbstipo.setEnabled(false);
            comboGruppi.setEnabled(false);
            comboSpecie.setEnabled(false);
            combocategorie.setEnabled(false);
            bsmastro.setEnabled(false);
            bsconto.setEnabled(false);
            bssotto.setEnabled(false);
            buttonInsGruppo.setEnabled(false);
            buttonInsSpecie.setEnabled(false);
            buttonInsCateg.setEnabled(false);
        } else {
            popolaComboGruppi();
            bsstato.setSelected(true);
        }
    }//GEN-LAST:event_formWindowOpened

    private void buttonSalvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvaActionPerformed
        switch (catbstipo.getSelectedIndex()) {
            case 3:
                JOptionPane.showMessageDialog(this, "Tipo immobilizzazione assente", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            case 0:
                if (Formattazione.estraiIntero(bsmastro.getText().trim()) < 1 || Formattazione.estraiIntero(bsmastro.getText().trim()) > 29) {
                    JOptionPane.showMessageDialog(this, "Codifica gruppo errata, ammessa >=0 e <30 ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }
                break;
            case 1:
                if (Formattazione.estraiIntero(bsmastro.getText().trim()) < 30 || Formattazione.estraiIntero(bsmastro.getText().trim()) > 39) {
                    JOptionPane.showMessageDialog(this, "Codifica gruppo errata, ammessa >=30 e <40 ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }
                break;
            case 2:
                if (Formattazione.estraiIntero(bsmastro.getText().trim()) < 40 || Formattazione.estraiIntero(bsmastro.getText().trim()) > 49) {
                    JOptionPane.showMessageDialog(this, "Codifica gruppo errata, ammessa >=40 e <50 ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }
                break;
            default:
                if (Formattazione.estraiIntero(bsmastro.getText().trim()) < 1 || Formattazione.estraiIntero(bsmastro.getText().trim()) > 29) {
                    JOptionPane.showMessageDialog(this, "Codifica gruppo errata, ammessa >=0 e <30 ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }
                catbstipo.setSelectedIndex(1);
                break;
        }

        if (bssotto.getText().trim().equals("") && (!bssottod.getText().trim().equals(""))) {
            JOptionPane.showMessageDialog(this, "Codice categoria Assente ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (bssotto.getText().trim().equals("") && bssottod.getText().trim().equals("") && bsdescr.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Descrizione Gruppo Specie Assente ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (!bssotto.getText().trim().equals("") && bssottod.getText().trim().equals("") && bscateg.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Descrizione Categoria  Assente ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

//        if ((!bssottod.getText().trim().equals("") || !bsecc.getText().trim().equals("")) && bsnote.getText().trim().equals("")) {
//            JOptionPane.showMessageDialog(this, "Descrizione note  Assente ", "Invio",
//                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
//            return;
//        }
//        if (!bssotto.getText().trim().equals("") && bssottod.getText().trim().equals("") && bsecc.getText().trim().equals("") && !bsnote.getText().trim().equals("")) {
//            JOptionPane.showMessageDialog(this, "Descrizione note impropria ", "Invio",
//                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
//            return;
//        }
//        if ((!bssotto.getText().trim().equals("") || !bssottod.getText().trim().equals("") || !bsecc.getText().trim().equals("")) && bsdescr.getText().trim().equals("")) {
//            JOptionPane.showMessageDialog(this, "Descrizione gruppo e specie impropria ", "Invio",
//                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
//            return;
//        }
        if (bsaliq.getValore() == 0.00 && !bssotto.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Coefficienti Assenti ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }
        if (bscee.getText().trim().equals("") && !bssotto.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "Codice voce cee Assente ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        int ok = aggiornaRecord();

        if (ok == EnvJazz.SUCCESSO) {
            JOptionPane.showMessageDialog(this, "Informazioni salvate ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            popolaComboGruppi();
            popolaComboSpecie(bsmastro.getText().trim());
            popolaComboCategorie(bsmastro.getText().trim(), bsconto.getText().trim());
        }

    }//GEN-LAST:event_buttonSalvaActionPerformed

    private void comboGruppiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGruppiActionPerformed
        if (comboGruppi.getSelectedIndex() > 0) {
            popolaComboSpecie(mcsGru[comboGruppi.getSelectedIndex() - 1]);
            bsmastro.setText(mcsGru[comboGruppi.getSelectedIndex() - 1]);
        } else if (!bsmastro.getText().trim().equals("")) {
            popolaComboSpecie(bsmastro.getText().trim());
        } else {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            combocategorie.removeAllItems();
            combocategorie.addItem("<  .....  >");
        }
    }//GEN-LAST:event_comboGruppiActionPerformed

    private void comboSpecieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSpecieActionPerformed
        if (comboGruppi.getSelectedIndex() > 0 && comboSpecie.getSelectedIndex() > 0) {
            popolaComboCategorie(mcsGru[comboGruppi.getSelectedIndex() - 1], mcsSpec[comboSpecie.getSelectedIndex() - 1]);
            bsconto.setText(mcsSpec[comboSpecie.getSelectedIndex() - 1]);
            bsdescr.setEnabled(false);
        } else if (!bsmastro.getText().trim().equals("") && !bsconto.getText().trim().equals("")) {
            popolaComboCategorie(bsmastro.getText().trim(), bsconto.getText().trim());
            bsdescr.setEnabled(false);
        } else {
            combocategorie.removeAllItems();
            combocategorie.addItem("<  .....  >");
        }
    }//GEN-LAST:event_comboSpecieActionPerformed

    private void buttonInsSpecieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsSpecieActionPerformed

        if (catbstipo.getSelectedIndex() < 3) {
            if (comboGruppi.getSelectedIndex() > 0) {
                bsmastro.setEnabled(false);
                String qry = "SELECT DISTINCT gruppobsconto FROM gruppibs WHERE gruppobssotto = \"\""
                        + " AND gruppobsmcs = \"" + bsmastro.getText().trim() + "\""
                        + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                        + " ORDER BY gruppobsmcs DESC LIMIT 01";
                QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                qRic.apertura();
                Record r = qRic.successivo();
                if (r != null) {
                    if (!r.leggiStringa("gruppobsconto").equals("")) {
                        Integer num = (new Integer(r.leggiStringa("gruppobsconto"))) + 1;
                        bsconto.setText(Formattazione.formIntero(num, 2, "0"));
                    } else {
                        Integer num = 1;
                        bsconto.setText(Formattazione.formIntero(num, 2, "0"));
                    }
                } else {
                    return;
                }
                qRic.chiusura();

                bscee.setText("");
                bssotto.setText("");
                bssottod.setText("");
                bsnote.setText("");
                bsaliq.setText("");

                bsstato.setSelected(true);
                jPanel2.setVisible(false);
                combocee.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(this, "Scegli tipo di GRUPPO azienda ", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        } else {
            JOptionPane.showMessageDialog(this, "Scegli tipo di IMMOBILIZZAZIONE ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }

    }//GEN-LAST:event_buttonInsSpecieActionPerformed

    private void buttonInsGruppoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsGruppoActionPerformed
        if (catbstipo.getSelectedIndex() < 3) {
            String qry = "SELECT DISTINCT gruppobsmcs FROM gruppibs WHERE gruppobsconto = \"\""
                    + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                    + " ORDER BY gruppobsmcs DESC LIMIT 01";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            if (r != null) {
                Integer num = (new Integer(r.leggiStringa("gruppobsmcs"))) + 1;
                String cod = Formattazione.formIntero(num, 2, "0");
                boolean ris = controllaEsistenzaCodice(cod);
                while (ris) {
                    num++;
                    cod = Formattazione.formIntero(num, 2, "0");
                    ris = controllaEsistenzaCodice(cod);
                }

                bsmastro.setText(cod);
//                bsmastro.setEnabled(false);
            } else {
                return;
            }
            qRic.chiusura();

            bsconto.setText("");
            bsconto.setEnabled(false);

            bscee.setText("");
            bssotto.setText("");
            bssottod.setText("");
            bsnote.setText("");
            bsaliq.setText("");

            bsstato.setSelected(true);
            comboGruppi.setEnabled(false);
            bsconto.setEnabled(false);
            buttonInsGruppo.setEnabled(false);
            comboSpecie.setEnabled(false);
            jPanel2.setVisible(false);
            combocee.setEnabled(false);
        } else {
            JOptionPane.showMessageDialog(this, "Scegli tipo di IMMOBILIZZAZIONE ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }


    }//GEN-LAST:event_buttonInsGruppoActionPerformed

    private void passwordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_passwordFocusLost

    }//GEN-LAST:event_passwordFocusLost

    private void buttonInsCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsCategActionPerformed
        duplicazione = false;
        if (catbstipo.getSelectedIndex() < 3) {
            if (!bsmastro.getText().trim().equals("")) {
                if (!bsconto.getText().trim().equals("")) {
                    bsmastro.setEnabled(false);
                    bsconto.setEnabled(false);
                    String qry = "SELECT DISTINCT gruppobssotto FROM gruppibs WHERE gruppobsd = \"\""
                            + " AND gruppobsmastro = \"" + bsmastro.getText().trim() + "\""
                            + " AND gruppobsconto =  \"" + bsconto.getText().trim() + "\""
                            + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                            + " ORDER BY gruppobsmcs DESC LIMIT 01";
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record r = qRic.successivo();
                    if (r != null) {
                        if (!r.leggiStringa("gruppobssotto").equals("")) {
                            Integer num = (new Integer(r.leggiStringa("gruppobssotto"))) + 1;
                            bssotto.setText(Formattazione.formIntero(num, 2, "0"));
                        } else {
                            Integer num = 1;
                            bssotto.setText(Formattazione.formIntero(num, 2, "0"));
                        }
                        bssotto.setEnabled(false);
                    } else {
                        return;
                    }
                    qRic.chiusura();

                    bscateg.setText("");
                    bssottod.setText("");
                    bsnote.setText("");
                    bsaliq.setText("");
                    bsstato.setSelected(true);

                    buttonInsSottocat.setEnabled(false);
                    bssottod.setEnabled(false);
                    bsnote.setEnabled(false);
                    if (idVis > 0) {
                        duplicazione = true;
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Scegli SPECIE azienda ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            } else {
                JOptionPane.showMessageDialog(this, "Scegli tipo di GRUPPO azienda ", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        } else {
            JOptionPane.showMessageDialog(this, "Scegli tipo di IMMOBILIZZAZIONE ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
    }//GEN-LAST:event_buttonInsCategActionPerformed

    private void buttonInsSottocatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsSottocatActionPerformed
        duplicazione = false;
        if (catbstipo.getSelectedIndex() < 3) {
            if (!bsmastro.getText().trim().equals("")) {
                if (!bsconto.getText().trim().equals("")) {
                    if (!bssotto.getText().trim().equals("")) {
                        bsmastro.setEnabled(false);
                        bsconto.setEnabled(false);
                        bssotto.setEnabled(false);
                        bssottod.setEnabled(true);
                        bsnote.setEnabled(true);
                        String qry = "SELECT DISTINCT gruppobsd FROM gruppibs WHERE gruppobsecc = \"\""
                                + " AND gruppobsmastro = \"" + bsmastro.getText().trim() + "\""
                                + " AND gruppobsconto =  \"" + bsconto.getText().trim() + "\""
                                + " AND gruppobssotto =  \"" + bssotto.getText().trim() + "\""
                                + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                                + " ORDER BY gruppobsmcs DESC LIMIT 01";
                        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                        qRic.apertura();
                        Record r = qRic.successivo();
                        if (r != null) {
                            if (!r.leggiStringa("gruppobsd").equals("")) {
                                Integer num = (new Integer(r.leggiStringa("gruppobsd"))) + 1;
                                bssottod.setText(Formattazione.formIntero(num, 2, "0"));
                            } else {
                                Integer num = 1;
                                bssottod.setText(Formattazione.formIntero(num, 2, "0"));
                            }
//                            bssottod.setEnabled(false);
                        } else {
                            return;
                        }
                        qRic.chiusura();

                        bsnote.setText("");
//                        bsaliq.setText("");
                        bsstato.setSelected(true);
                        if (idVis > 0) {
                            duplicazione = true;
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Scegli CATEGORIA appartenete alla specie azienda ", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Scegli SPECIE azienda ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            } else {
                JOptionPane.showMessageDialog(this, "Scegli tipo di GRUPPO azienda ", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        } else {
            JOptionPane.showMessageDialog(this, "Scegli tipo di IMMOBILIZZAZIONE ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
    }//GEN-LAST:event_buttonInsSottocatActionPerformed

    private void catbstipoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_catbstipoItemStateChanged
        popolaComboGruppi();
        if (catbstipo.getSelectedIndex() > 0) {
            if (comboGruppi.getSelectedIndex() > 0) {
                popolaComboSpecie(mcsGru[comboGruppi.getSelectedIndex() - 1]);
                if (comboSpecie.getSelectedIndex() > 0) {
                    popolaComboCategorie(mcsGru[comboGruppi.getSelectedIndex() - 1], mcsSpec[comboSpecie.getSelectedIndex() - 1]);
                } else {
                    combocategorie.removeAllItems();
                    combocategorie.addItem("<  .....  >");
                }
            } else {
                comboSpecie.removeAllItems();
                comboSpecie.addItem("<  .....  >");
                combocategorie.removeAllItems();
                combocategorie.addItem("<  .....  >");
            }
        } else {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            combocategorie.removeAllItems();
            combocategorie.addItem("<  .....  >");
        }
        popolaVociCee();
    }//GEN-LAST:event_catbstipoItemStateChanged

    private void combocategorieItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_combocategorieItemStateChanged

    }//GEN-LAST:event_combocategorieItemStateChanged

    private void combocategorieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_combocategorieActionPerformed
        if (combocategorie.getSelectedIndex() > 0) {
            String qry = "SELECT gruppobsid FROM gruppibs WHERE gruppobsid > 0";
            if (!bsmastro.getText().trim().equals("")) {
                qry += " AND gruppobsmastro = \"" + bsmastro.getText().trim() + "\"";
            }

            if (!bsconto.getText().trim().equals("")) {
                qry += " AND gruppobsconto =  \"" + bsconto.getText().trim() + "\"";
            }

            if (!bssotto.getText().trim().equals("")) {
                qry += " AND gruppobssotto =  \"" + bssotto.getText().trim() + "\"";
            }

            qry += " AND gruppobsd = \"\" AND gruppobsecc = \"\""
                    + " AND gruppobstipo = " + (catbstipo.getSelectedIndex() + 2)
                    + " ORDER BY gruppobsmcs DESC LIMIT 01";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            if (r != null) {
                mostraRecord(r.leggiIntero("gruppobsid"));
            }
            qRic.chiusura();
        }
    }//GEN-LAST:event_combocategorieActionPerformed

    private void comboceeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboceeActionPerformed
        if (combocee.getSelectedIndex() > 0) {
            String combo = (String) combocee.getSelectedItem();
            String codice = (combo).substring(0, combo.indexOf("-"));
            bscee.setText(codice);
        }
    }//GEN-LAST:event_comboceeActionPerformed

    private void buttonVisPrecaricateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonVisPrecaricateActionPerformed
        if (!eccezione.equals("")) {
            DialogListaCategoriePrecaricate d = new DialogListaCategoriePrecaricate(formGen, true, connAz, eccezione);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();
        }
    }//GEN-LAST:event_buttonVisPrecaricateActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DialogManutenzioneGruppiSpecie(new javax.swing.JFrame(), true).show();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.CampoValutaPop bsaliq;
    private ui.beans.CampoTestoPop bscateg;
    private ui.beans.CampoTestoPop bscee;
    private ui.beans.CampoTestoPop bsconto;
    private ui.beans.CampoTestoPop bsdescr;
    private ui.beans.CampoTestoPop bsmastro;
    private ui.beans.CampoTestoPop bsnote;
    private ui.beans.CampoTestoPop bssotto;
    private ui.beans.CampoTestoPop bssottod;
    private javax.swing.JCheckBox bsstato;
    private ui.beans.PopButton buttonInsCateg;
    private ui.beans.PopButton buttonInsGruppo;
    private ui.beans.PopButton buttonInsSottocat;
    private ui.beans.PopButton buttonInsSpecie;
    private ui.beans.PopButton buttonSalva;
    private ui.beans.PopButton buttonVisPrecaricate;
    private javax.swing.JComboBox catbstipo;
    private javax.swing.JComboBox comboGruppi;
    private javax.swing.JComboBox comboSpecie;
    private javax.swing.JComboBox<String> combocategorie;
    private javax.swing.JComboBox<String> combocee;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JLabel labeccezioni;
    private javax.swing.JLabel nota_1;
    private ui.beans.CampoPasswordPop password;
    // End of variables declaration//GEN-END:variables

    private void mostraRecord(int idsel) {
        String qry = "SELECT * FROM gruppibs WHERE gruppobsid = " + idsel;
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            Record r = q.successivo();
            if (r.leggiIntero("gruppobstipo") == 2) {
                catbstipo.setSelectedIndex(0);
            } else if (r.leggiIntero("gruppobstipo") == 3) {
                catbstipo.setSelectedIndex(1);
            } else {
                catbstipo.setSelectedIndex(2);
            }

            bsmastro.visTesto(r, "gruppobsmastro");
            bsconto.visTesto(r, "gruppobsconto");
            bssotto.visTesto(r, "gruppobssotto");
            bssottod.visTesto(r, "gruppobsd");
            bscee.visTesto(r, "gruppobsbil");

            bsdescr.visTesto(r, "gruppobsdescr");
            bscateg.visTesto(r, "gruppobscateg");
            bsnote.visTesto(r, "gruppobsnote");
            bsaliq.visValuta(r, "gruppobsaliq");

            if (r.leggiIntero("gruppobsstato") == 1) {
                bsstato.setSelected(true);
            } else {
                bsstato.setSelected(false);
            }
            if (r.leggiStringa("gruppobssotto").equals("")) {
                bscee.setEnabled(false);
                bsnote.setEnabled(false);
//                bsaliq.setEnabled(false);
                bssottod.setEnabled(false);
//                bscateg.setEnabled(false);
                buttonInsSottocat.setEnabled(false);
                nota_1.setVisible(false);
                bssottod.setEnabled(false);
                bsnote.setEnabled(false);
            } else {
                String qry2 = "SELECT gruppobsbil,gruppobsdescr FROM gruppibs WHERE gruppobsmcs = \"" + r.leggiStringa("gruppobsmastro") + "\"";
                QueryInterrogazioneClient qRic2 = new QueryInterrogazioneClient(connAz, qry2);
                qRic2.apertura();
                Record r2 = qRic2.successivo();
                qRic2.chiusura();
                comboGruppi.removeAllItems();
                comboGruppi.addItem(r2.leggiStringa("gruppobsbil") + " - " + r2.leggiStringa("gruppobsdescr"));
                comboGruppi.setSelectedIndex(0);
                String qry3 = "SELECT gruppobsdescr FROM gruppibs WHERE gruppobsmcs = \"" + r.leggiStringa("gruppobsmastro") + r.leggiStringa("gruppobsconto") + "\"";
                QueryInterrogazioneClient qRic3 = new QueryInterrogazioneClient(connAz, qry3);
                qRic3.apertura();
                Record r3 = qRic3.successivo();
                qRic3.chiusura();
                comboSpecie.removeAllItems();
                comboSpecie.addItem(r3.leggiStringa("gruppobsdescr"));
                comboSpecie.setSelectedIndex(0);
                bsdescr.setEnabled(false);
                nota_1.setVisible(true);
                bssottod.setEnabled(false);
                bsnote.setEnabled(false);
                String qry4 = "SELECT gruppobscateg FROM gruppibs WHERE gruppobsmcs = \"" + r.leggiStringa("gruppobsmastro") + r.leggiStringa("gruppobsconto") + r.leggiStringa("gruppobssotto") + "\"";
                QueryInterrogazioneClient qRic4 = new QueryInterrogazioneClient(connAz, qry4);
                qRic4.apertura();
                Record r4 = qRic4.successivo();
                qRic4.chiusura();
                combocategorie.removeAllItems();
                combocategorie.addItem(r4.leggiStringa("gruppobscateg"));
                combocategorie.setSelectedIndex(0);
                for (int y = 0; y < vcee.size(); y++) {
                    Record rc = vcee.get(y);
                    if (rc.leggiStringa("pdcceecodalfa").equals(bscee.getText().trim())) {
                        combocee.setSelectedIndex(y + 1);
                    }
                }
            }
            if (!r.leggiStringa("gruppobsecc").equals("")) {
                labeccezioni.setVisible(true);
                buttonVisPrecaricate.setVisible(true);
                eccezione = r.leggiStringa("gruppobsecc");
            } else {
                labeccezioni.setVisible(false);
                buttonVisPrecaricate.setVisible(false);
                eccezione = "";
            }

        }
        q.chiusura();
    }

    private int aggiornaRecord() {
        int ris = EnvJazz.SUCCESSO;
        Record r = new Record();
        switch (catbstipo.getSelectedIndex()) {
            case 0:
                r.insElem("gruppobstipo", (int) 2);
                break;
            case 1:
                r.insElem("gruppobstipo", (int) 3);
                break;
            default:
                r.insElem("gruppobstipo", (int) 4);
                break;
        }
        bsmastro.insTesto(r, "gruppobsmastro");
        if (bsconto.getText().trim().equals("") && bssotto.getText().trim().equals("")) {
            r.insElem("gruppobsbil", "GRUPPO " + bsmastro.getText().trim());
        } else if (bssotto.getText().trim().equals("")) {
            r.insElem("gruppobsbil", "");
        } else {
            bscee.insTesto(r, "gruppobsbil");
        }
        bsconto.insTesto(r, "gruppobsconto");
        bssotto.insTesto(r, "gruppobssotto");
        bssottod.insTesto(r, "gruppobsd");
        r.insElem("gruppobsmcs", bsmastro.getText().trim() + bsconto.getText().trim() + bssotto.getText().trim() + bssottod.getText().trim());
        if (!bssottod.getText().trim().equals("")) {
            r.insElem("gruppobsdescr", "");
            r.insElem("gruppobscateg", "");
            bsnote.insTesto(r, "gruppobsnote");
        } else if (!bssotto.getText().trim().equals("")) {
            r.insElem("gruppobsdescr", "");
            bscateg.insTesto(r, "gruppobscateg");
            r.insElem("gruppobsnote", "");
        } else {
            bsdescr.insTesto(r, "gruppobsdescr");
            r.insElem("gruppobscateg", "");
            r.insElem("gruppobsnote", "");
        }
        bsaliq.insValuta(r, "gruppobsaliq");

        if (bsstato.isSelected()) {
            r.insElem("gruppobsstato", (int) 1);
        } else {
            r.insElem("gruppobsstato", (int) 0);
        }

        if (idVis > 0 && !duplicazione) {
            r.insElem("gruppobsid", idVis);
            QueryAggiornamentoClient qAgg = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "gruppibs", "gruppobsid", r);
            qAgg.esecuzione();
        } else {
            String qry = "SELECT gruppobsid FROM gruppibs "
                    + " WHERE  gruppobsmastro = \"" + bsmastro.getText().trim() + "\""
                    + " AND   gruppobsconto = \"" + bsconto.getText().trim() + "\""
                    + " AND   gruppobssotto = \"" + bssotto.getText().trim() + "\""
                    + " AND   gruppobsd = \"" + bssottod.getText().trim() + "\"";
//                    + " AND   gruppobsecc = \"" + bsecc.getText().trim() + "\"";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            int num = qRic.numeroRecord();
            if (num > 0) {
                qRic.chiusura();
                JOptionPane.showMessageDialog(this, "Codifica GIA' ESISTENTE ", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                ris = EnvJazz.FALLIMENTO;
                return ris;
            }
            qRic.chiusura();

            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "gruppibs", "gruppobsid", r);
            qins.esecuzione();
            idVis = qins.getID();
        }
        return ris;
    }

    private void popolaComboGruppi() {
        int grtipo = catbstipo.getSelectedIndex();
        comboGruppi.removeAllItems();
        comboGruppi.addItem("<  .....  >");
        String qry = "SELECT gruppobsid,gruppobsmcs,gruppobsdescr FROM gruppibs WHERE gruppobsconto = \"\"";
        if (grtipo > 0) {
            qry += " AND gruppobstipo = " + (grtipo + 2);
        }
        qry += " ORDER BY gruppobsmcs";

        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        int num = qRic.numeroRecord();
        if (num > 0) {
            idGru = new int[qRic.numeroRecord()];
            mcsGru = new String[qRic.numeroRecord()];
            for (int i = 0; i < qRic.numeroRecord(); i++) {
                Record r = qRic.successivo();
                comboGruppi.addItem(r.leggiStringa("gruppobsmcs") + " - " + r.leggiStringa("gruppobsdescr"));
                idGru[i] = r.leggiIntero("gruppobsid");
                mcsGru[i] = r.leggiStringa("gruppobsmcs");
            }
        } else {
            idGru = new int[0];
            mcsGru = new String[0];
        }
        qRic.chiusura();
    }

    private void popolaComboSpecie(String codgru) {
        if (!codgru.equals("")) {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            String qry = "SELECT gruppobsid,gruppobsconto,gruppobsdescr FROM gruppibs"
                    + " WHERE gruppobsconto <> \"\""
                    + " AND gruppobssotto = \"\""
                    + " AND gruppobsmastro = \"" + codgru + "\" ORDER BY gruppobsmcs";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            int num = qRic.numeroRecord();
            if (num > 0) {
                idSpec = new int[qRic.numeroRecord()];
                mcsSpec = new String[qRic.numeroRecord()];
                for (int i = 0; i < qRic.numeroRecord(); i++) {
                    Record r = qRic.successivo();
                    comboSpecie.addItem(r.leggiStringa("gruppobsconto") + " - " + r.leggiStringa("gruppobsdescr"));
                    idSpec[i] = r.leggiIntero("gruppobsid");
                    mcsSpec[i] = r.leggiStringa("gruppobsconto");
                }
            }
            qRic.chiusura();
        } else {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            idSpec = new int[0];
            mcsSpec = new String[0];
        }
    }

    private void popolaComboCategorie(String codgru, String codspec) {
        if (!codgru.equals("") && !codspec.equals("")) {
            combocategorie.removeAllItems();
            combocategorie.addItem("<  .....  >");
            String qry = "SELECT gruppobsid,gruppobsmcs,gruppobscateg FROM gruppibs WHERE gruppobssotto <> \"\""
                    + " AND gruppobsmastro = \"" + codgru + "\""
                    + " AND gruppobsconto = \"" + codspec + "\""
                    + " ORDER BY gruppobsmcs";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            int num = qRic.numeroRecord();
            if (num > 0) {
                idCat = new int[qRic.numeroRecord()];
                mcsCat = new String[qRic.numeroRecord()];
                for (int i = 0; i < qRic.numeroRecord(); i++) {
                    Record r = qRic.successivo();
                    combocategorie.addItem(r.leggiStringa("gruppobscateg"));
                    idCat[i] = r.leggiIntero("gruppobsid");
                    mcsCat[i] = r.leggiStringa("gruppobsmcs");
                }
            }
            qRic.chiusura();
        } else {
            combocategorie.removeAllItems();
            combocategorie.addItem("<  .....  >");
            idCat = new int[0];
            mcsCat = new String[0];
        }
    }

    private void popolaVociCee() {
        combocee.removeAllItems();
        combocee.addItem("<  .....  >");
        String qry = "SELECT DISTINCT pdcceecodalfa,pdcceedescrvoce FROM bilanciobs WHERE pdcceemastro = \"\""
                + " AND pdcceetipovoce = \"V\" AND pdcceebs < 4 AND pdcceesegno = 1";
        if (catbstipo.getSelectedIndex() == 0) {
            qry += " AND pdcceecodalfa LIKE(\"1.B.II.%\")";
        }
        if (catbstipo.getSelectedIndex() == 1) {
            qry += " AND pdcceecodalfa LIKE(\"1.B.I.%\")";
        }
        if (catbstipo.getSelectedIndex() == 2) {
            qry += " AND pdcceecodalfa LIKE(\"1.B.III.%\")";
        }
        qry += " ORDER BY pdcceecodalfa";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            combocee.addItem(r.leggiStringa("pdcceecodalfa") + " - " + r.leggiStringa("pdcceedescrvoce"));
            vcee.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
    }

    final boolean controllaEsistenzaCodice(String cod) {
        boolean ris = false;
        String qry2 = "SELECT * FROM gruppibs WHERE gruppobsconto = \"\""
                + " AND gruppobsmcs = \"" + cod + "\"";

        QueryInterrogazioneClient qRic2 = new QueryInterrogazioneClient(connAz, qry2);
        qRic2.apertura();
        Record r2 = qRic2.successivo();
//        if (r2 != null && catbstipo.getSelectedIndex() == 1 && cod.compareTo("39") > 0) {
//            //errore
//            y = 2;
//        } else if (r2 != null && catbstipo.getSelectedIndex() == 0 && cod.compareTo("29") > 0) {
//            //errore
//            y = 2;
        if (r2 != null) {
            ris = true;
        } else {
            ris = false;
        }
        qRic2.chiusura();

        return ris;
    }

}
