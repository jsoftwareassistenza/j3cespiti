package cespitipop.disuso;

/*
 * DialogCategCespiti.java
 *
 * Created on 8 novembre 2006, 15.21
 */
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import nucleo.ConnessioneServer;
import nucleo.Data;
import nucleo.EnvJazz;
import nucleo.FDouble;
import nucleo.FunzioniComuniServer;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import ui.eventi.GestoreEventiBeans;

/**
 *
 * @author boss
 */
public class DialogGestCategoriePrecaricateCespiti extends javax.swing.JDialog {

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;
    
    Frame formGen = null;
    
    private String esContabile = "";
    private String azienda = "";
    private String key = "";

    private int idVis = -1;    

    ArrayList<Record> vcatecc = new ArrayList();
    ArrayList<Record> vclrip = new ArrayList();

    Record rcateg = new Record();
    Data oggi = new Data();

    int esito = EnvJazz.PASSWORD_ERRATA;
    /**
     * Creates new form DialogCategCespiti
     */
    public DialogGestCategoriePrecaricateCespiti(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public DialogGestCategoriePrecaricateCespiti(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom,
            String azienda, String esContabile, int idVis, String key) {
        this(parent, modal);
        this.formGen = parent;
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.key = key;        
        
        flagAttiva.setSelected(true);

        catsscod.setEventoFocusLost(new GestoreEventiBeans() 
        {
            @Override
            public void lancia(KeyEvent evento) 
            {
                controllaEsistenzaCodice();
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        catbsgruppo = new javax.swing.JLabel();
        grucat = new javax.swing.JLabel();
        catsscod = new ui.beans.CampoTestoPop();
        catssdescr = new ui.beans.CampoTestoPop();
        jLabel24 = new javax.swing.JLabel();
        codnote = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        quotacoeff = new ui.beans.CampoValutaPop();
        jPanel2 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        cespclasserip = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        catssquotaagevolata = new ui.beans.CampoValutaPop();
        jLabel30 = new javax.swing.JLabel();
        catssquotacosto = new ui.beans.CampoValutaPop();
        jLabel32 = new javax.swing.JLabel();
        catssquotaiva = new ui.beans.CampoValutaPop();
        jLabel18 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        catssmaxcosto = new ui.beans.CampoValutaPop();
        catssmaxnolo = new ui.beans.CampoValutaPop();
        jLabel16 = new javax.swing.JLabel();
        catssquotafabbr = new ui.beans.CampoValutaPop();
        jLabel31 = new javax.swing.JLabel();
        catssbinario = new javax.swing.JComboBox();
        catssdtfine = new ui.beans.CampoDataPop();
        catssdtiniz = new ui.beans.CampoDataPop();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        flagAttiva = new javax.swing.JCheckBox();
        codiva = new archivibasepop.beans.BeanGestIvaPop();
        descraliq = new ui.beans.PopDescrLabelLineBorder();
        label_um2 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        buttonSalva = new ui.beans.PopButton();
        jLabel3 = new javax.swing.JLabel();
        password = new ui.beans.CampoPasswordPop();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("IMMOBILIZZAZIONI: Categorie civilistiche + categorie interne");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 100));
        jPanel1.setLayout(null);

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Descrizione");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(192, 68, 96, 24);

        catbsgruppo.setBackground(new java.awt.Color(202, 236, 221));
        catbsgruppo.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        catbsgruppo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        catbsgruppo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        catbsgruppo.setOpaque(true);
        jPanel1.add(catbsgruppo);
        catbsgruppo.setBounds(292, 4, 520, 24);

        grucat.setBackground(new java.awt.Color(202, 236, 221));
        grucat.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        grucat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        grucat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        grucat.setOpaque(true);
        jPanel1.add(grucat);
        grucat.setBounds(92, 4, 96, 24);

        catsscod.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(catsscod);
        catsscod.setBounds(92, 68, 96, 24);

        catssdescr.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(catssdescr);
        catssdescr.setBounds(296, 68, 516, 24);

        jLabel24.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel24.setText("sub. Cod.");
        jPanel1.add(jLabel24);
        jLabel24.setBounds(12, 68, 72, 24);

        codnote.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        codnote.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        codnote.setText("sottocategoria");
        jPanel1.add(codnote);
        codnote.setBounds(12, 36, 104, 24);

        jLabel33.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel33.setText("Coefficiente ministeriale %");
        jPanel1.add(jLabel33);
        jLabel33.setBounds(272, 36, 188, 24);

        quotacoeff.setEditable(false);
        quotacoeff.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        quotacoeff.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        quotacoeff.setInteri(3);
        jPanel1.add(quotacoeff);
        quotacoeff.setBounds(468, 36, 60, 24);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Manutenzioni", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 12))); // NOI18N
        jPanel7.setLayout(null);

        cespclasserip.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel7.add(cespclasserip);
        cespclasserip.setBounds(92, 24, 260, 24);

        jLabel20.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("Classe");
        jPanel7.add(jLabel20);
        jLabel20.setBounds(20, 24, 64, 24);

        jPanel2.add(jPanel7);
        jPanel7.setBounds(292, 304, 368, 64);

        catssquotaagevolata.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        catssquotaagevolata.setInteri(12);
        jPanel2.add(catssquotaagevolata);
        catssquotaagevolata.setBounds(296, 104, 60, 24);

        jLabel30.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel30.setText("al");
        jPanel2.add(jLabel30);
        jLabel30.setBounds(452, 0, 56, 24);

        catssquotacosto.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        catssquotacosto.setInteri(12);
        jPanel2.add(catssquotacosto);
        catssquotacosto.setBounds(296, 144, 60, 24);

        jLabel32.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel32.setText("Agevolazione   %");
        jPanel2.add(jLabel32);
        jLabel32.setBounds(164, 104, 120, 24);

        catssquotaiva.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        catssquotaiva.setInteri(12);
        jPanel2.add(catssquotaiva);
        catssquotaiva.setBounds(296, 184, 60, 24);

        jLabel18.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Detraibilita' Iva %");
        jPanel2.add(jLabel18);
        jLabel18.setBounds(132, 184, 148, 24);

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Massimale acquisto");
        jPanel2.add(jLabel13);
        jLabel13.setBounds(136, 224, 144, 24);

        jLabel14.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Massimale noleggio");
        jPanel2.add(jLabel14);
        jLabel14.setBounds(136, 260, 144, 24);

        catssmaxcosto.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        catssmaxcosto.setInteri(12);
        jPanel2.add(catssmaxcosto);
        catssmaxcosto.setBounds(296, 224, 96, 24);

        catssmaxnolo.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        catssmaxnolo.setInteri(12);
        jPanel2.add(catssmaxnolo);
        catssmaxnolo.setBounds(296, 260, 96, 24);

        jLabel16.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Fabbricati/Terreni  % non ammortizzabile");
        jPanel2.add(jLabel16);
        jLabel16.setBounds(12, 64, 268, 24);

        catssquotafabbr.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        catssquotafabbr.setInteri(12);
        jPanel2.add(catssquotafabbr);
        catssquotafabbr.setBounds(296, 64, 60, 24);

        jLabel31.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel31.setText("Deducibilita' limitata %");
        jPanel2.add(jLabel31);
        jLabel31.setBounds(68, 144, 212, 24);

        catssbinario.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        catssbinario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "applicata a Fiscale e Civilistico", "applicata a solo Fiscale", "applicata a solo Civilistico" }));
        jPanel2.add(catssbinario);
        catssbinario.setBounds(392, 64, 248, 24);

        catssdtfine.setToolTipText("Se il bene strumentale è quiescente il relativo costo è indeducibile. A stabilirlo è la Corte di Cassazione con l’ordinanza del 5 febbraio 2020");
        jPanel2.add(catssdtfine);
        catssdtfine.setBounds(456, 24, 120, 24);

        catssdtiniz.setToolTipText("Se il bene strumentale è quiescente il relativo costo è indeducibile. A stabilirlo è la Corte di Cassazione con l’ordinanza del 5 febbraio 2020");
        jPanel2.add(catssdtiniz);
        catssdtiniz.setBounds(296, 24, 120, 24);

        jLabel34.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel34.setText("Validita' ");
        jPanel2.add(jLabel34);
        jLabel34.setBounds(232, 24, 56, 24);

        jLabel35.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel35.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel35.setText("dal");
        jPanel2.add(jLabel35);
        jLabel35.setBounds(292, 0, 56, 24);

        flagAttiva.setBackground(new java.awt.Color(255, 255, 255));
        flagAttiva.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        flagAttiva.setText("ATTIVA");
        jPanel2.add(flagAttiva);
        flagAttiva.setBounds(152, 320, 90, 23);
        jPanel2.add(codiva);
        codiva.setBounds(530, 144, 68, 24);

        descraliq.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        jPanel2.add(descraliq);
        descraliq.setBounds(600, 144, 200, 24);

        label_um2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_um2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        label_um2.setText("Aliquota");
        label_um2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        label_um2.setMaximumSize(new java.awt.Dimension(95, 16));
        label_um2.setMinimumSize(new java.awt.Dimension(95, 16));
        label_um2.setPreferredSize(new java.awt.Dimension(95, 16));
        jPanel2.add(label_um2);
        label_um2.setBounds(450, 144, 52, 24);

        getContentPane().add(jPanel2, java.awt.BorderLayout.CENTER);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setPreferredSize(new java.awt.Dimension(100, 50));
        jPanel8.setLayout(null);

        buttonSalva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_save_white_20dp.png"))); // NOI18N
        buttonSalva.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        buttonSalva.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonSalva.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonSalva.setPreferredSize(new java.awt.Dimension(80, 40));
        buttonSalva.setToolTipText("Salva le informazioni");
        buttonSalva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvaActionPerformed(evt);
            }
        });
        jPanel8.add(buttonSalva);
        buttonSalva.setBounds(15, 4, 40, 40);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Password");
        jPanel8.add(jLabel3);
        jLabel3.setBounds(204, 12, 88, 28);

        password.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
        password.setToolTipText("parola chiave accesso associata all'operatore");
        jPanel8.add(password);
        password.setBounds(304, 12, 256, 28);

        getContentPane().add(jPanel8, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(847, 568));
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened

        codiva.passaParametri(connCom, formGen, formGen, descraliq, false, "", -1);
        
        password.setEventoFocusLost(new GestoreEventiBeans() 
        {
            @Override
            public void lancia() 
            {
                esito = EnvJazz.PASSWORD_ERRATA;
                if (new String(password.getText()).equals("xxjazzyy")) 
                {
                    esito = EnvJazz.SUCCESSO;
                }
                
                if (esito != EnvJazz.SUCCESSO)
                {
                    buttonSalva.setEnabled(false);
                } else 
                {
                    buttonSalva.setEnabled(true);
                }
            }
        });
        
        vcatecc = new ArrayList();
        String qry = "SELECT DISTINCT gruppobsecc,gruppobssotto,gruppobscateg,gruppobsaliq FROM gruppibs"
                + " WHERE gruppobsecc <> \"\""
                + " AND gruppobssotto <> \"\""
                + " ORDER BY gruppobsmcs,gruppobsecc";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record rl = q.successivo();
        while (rl != null) 
        {
            vcatecc.add(rl);
            if (rl.leggiStringa("gruppobsecc").equals(key))
            {
                grucat.setText(key);
                catbsgruppo.setText(rl.leggiStringa("gruppobscateg"));
                quotacoeff.setText("" + new FDouble(rl.leggiDouble("gruppobsaliq"), "##0.00", 0));
            }
            rl = q.successivo();
        }
        q.chiusura();
        
        if (key.equals("")) 
        {
            JOptionPane.showMessageDialog(this, "Avvio non possibile assente la tematica di inserimento", "Avviso",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            catsscod.setEnabled(false);
            buttonSalva.setEnabled(false);
        }

        cespclasserip.removeAllItems();
        cespclasserip.addItem("<  scegli classe manutenzioni  >");
        vclrip = new ArrayList();
        
        qry = "SELECT * FROM bsclassimanut ORDER BY bsclmanutcod";
        QueryInterrogazioneClient qCl = new QueryInterrogazioneClient(connAz, qry);
        qCl.apertura();
        Record rcl = qCl.successivo();
        while (rcl != null)
        {
            vclrip.add(rcl);
            cespclasserip.addItem("Classe " + rcl.leggiStringa("bsclmanutcod") + " - " + rcl.leggiDouble("bsclmanutquota"));
            rcl = qCl.successivo();
        }
        qCl.chiusura();

        if (idVis > 0) 
        {
            qry = "SELECT * FROM categspeciali WHERE catssid = " + idVis;
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            rcateg = qRic.successivo();
            qRic.chiusura();
            if (rcateg != null) 
            {
                mostraRecord();

                if(esito != EnvJazz.SUCCESSO)
                {
                    buttonSalva.setEnabled(true);
                }
                else
                {
                    buttonSalva.setEnabled(false);
                }
            }
        } else {
            String prog = FunzioniComuniServer.progressivoTabella(connAz, "categspeciali", "catssid", "catsscod");
            catsscod.setText(prog);
            buttonSalva.setEnabled(false);
        }
    }//GEN-LAST:event_formWindowOpened

    private void buttonSalvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvaActionPerformed
        if (catsscod.getText().trim().equals("")) 
        {
            JOptionPane.showMessageDialog(this, "Codice nuova categoria assente", "Avviso",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }
        if (catssdescr.getText().trim().equals("")) 
        {
            JOptionPane.showMessageDialog(this, "Descrizione nuova categoria assente", "Avviso",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }
        
        if (catssquotaiva.getValore() == 0 && catssquotacosto.getValore() == 0
                && catssquotafabbr.getValore() == 0 && catssquotaagevolata.getValore() == 0
                && catssmaxcosto.getValore() == 0 && catssmaxnolo.getValore() == 0) 
        {
            JOptionPane.showMessageDialog(this, "Assenti le limitazioni/agevolazioni", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        aggiornaRecord();

    }//GEN-LAST:event_buttonSalvaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DialogGestCategoriePrecaricateCespiti(new javax.swing.JFrame(), true).show();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonSalva;
    private javax.swing.JLabel catbsgruppo;
    private javax.swing.JComboBox catssbinario;
    private ui.beans.CampoTestoPop catsscod;
    private ui.beans.CampoTestoPop catssdescr;
    private ui.beans.CampoDataPop catssdtfine;
    private ui.beans.CampoDataPop catssdtiniz;
    private ui.beans.CampoValutaPop catssmaxcosto;
    private ui.beans.CampoValutaPop catssmaxnolo;
    private ui.beans.CampoValutaPop catssquotaagevolata;
    private ui.beans.CampoValutaPop catssquotacosto;
    private ui.beans.CampoValutaPop catssquotafabbr;
    private ui.beans.CampoValutaPop catssquotaiva;
    private javax.swing.JComboBox cespclasserip;
    private archivibasepop.beans.BeanGestIvaPop codiva;
    private javax.swing.JLabel codnote;
    private ui.beans.PopDescrLabelLineBorder descraliq;
    private javax.swing.JCheckBox flagAttiva;
    private javax.swing.JLabel grucat;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JLabel label_um2;
    private ui.beans.CampoPasswordPop password;
    private ui.beans.CampoValutaPop quotacoeff;
    // End of variables declaration//GEN-END:variables

    private void mostraRecord()
    {
        catsscod.visTesto(rcateg, "catsscod");
        catsscod.setEditable(false);
        catssdescr.visTesto(rcateg, "catssdescr");

        catssdtiniz.visData(rcateg, "catssdtiniz");
        catssdtfine.visData(rcateg, "catssdtfine");

        catssquotafabbr.visValuta(rcateg, "catssdetrfabbr");
        catssbinario.setSelectedIndex(rcateg.leggiIntero("catssbinario"));
        catssquotaagevolata.visValuta(rcateg, "catssquotaage");
        catssquotaiva.visValuta(rcateg, "catssdetriva");
        catssquotacosto.visValuta(rcateg, "catssdetrcosto");

        catssmaxcosto.visValuta(rcateg, "catssmaxcosto");
        catssmaxnolo.visValuta(rcateg, "catssmaxnolo");
        
        String qry="SELECT ivaid,ivacod,ivadescr FROM codiva WHERE ivaid="+rcateg.leggiIntero("catbsivaiddetrab");
        QueryInterrogazioneClient q1 = new QueryInterrogazioneClient(connCom, qry);
        q1.apertura();
        Record rcodiva=q1.successivo();
        q1.chiusura();
        if(rcodiva!=null)
        {
            codiva.visualizzaDati(rcodiva.leggiIntero("ivaid"), rcodiva.leggiStringa("ivacod"), rcodiva.leggiStringa("ivadescr"));
        }

        if (rcateg.leggiIntero("catssclmanutid") > 0)
        {
            String query = "SELECT * FROM bsclassimanut WHERE bsclmanutid = " + rcateg.leggiIntero("catssclmanutid");
            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, query);
            q.apertura();
            Record rm = q.successivo();
            if (rm != null) 
            {
                for (int i = 0; i < vclrip.size(); i++) 
                {
                    Record rcl = vclrip.get(i);
                    if (rcl.leggiIntero("bsclmanutid") == rcateg.leggiIntero("catssclmanutid")) {
                        cespclasserip.setSelectedIndex(i + 1);
                        if ((new Data(rcl.leggiStringa("bsclmanutdtiniz"), Data.AAAA_MM_GG)).maggiore(oggi) || (new Data(rcl.leggiStringa("bsclmanutdtfine"), Data.AAAA_MM_GG)).minore(oggi))
                        {
                            JOptionPane.showMessageDialog(this, "ATTENZIONE la classe manutenzioni non e' valida ad oggi", "Avviso",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        }
                    }
                }
            } else 
            {
                //errore;
                cespclasserip.setSelectedIndex(0);
            }
        } else {
            cespclasserip.setSelectedIndex(0);
        }

        if (rcateg.leggiIntero("catssstato") == 1) 
        {
            flagAttiva.setSelected(true);
        } else 
        {
            flagAttiva.setSelected(false);
        }
    }

    private int aggiornaRecord() 
    {
        int ris = EnvJazz.SUCCESSO;
        Record rec = new Record();

        rec.insElem("catsscodecc",grucat.getText());
        rec.insElem("catsscod", catsscod.getText().trim());
        catssdescr.insTesto(rec, "catssdescr");

        catssquotacosto.insValuta(rec, "catssdetrcosto");
        catssmaxcosto.insValuta(rec, "catssmaxcosto");
        catssmaxnolo.insValuta(rec, "catssmaxnolo");
        catssquotaiva.insValuta(rec, "catssdetriva");
        catssquotaagevolata.insValuta(rec, "catssquotaage");

        catssquotafabbr.insValuta(rec, "catssdetrfabbr");
        rec.insElem("catssbinario", catssbinario.getSelectedIndex());

        Record rcl = vclrip.get(cespclasserip.getSelectedIndex());
        rec.insElem("catssclmanutid", rcl.leggiIntero("bsclmanutid"));
        if (!catssdtiniz.getText().trim().equals(""))
        {
            catssdtiniz.insData(rec, "catssdtiniz");
        } else 
        {
            rec.insElem("catssdtiniz", "0001-01-01");
        }
        
        if (!catssdtfine.getText().trim().equals("")) 
        {
            catssdtfine.insData(rec, "catssdtfine");
        } else 
        {
            rec.insElem("catssdtfine", "9999-12-31");
        }
        
        if (flagAttiva.isSelected()) 
        {
            rec.insElem("catssstato", (int) 1);
        } else 
        {
            rec.insElem("catssstato", (int) 0);
        }
        
        rec.insElem("catbsivaiddetrab",codiva.leggiId());
        rec.convalida(true);

        if (idVis > 0) 
        {
            rec.insElem("catssid", idVis);
            QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "categspeciali", "catssid", rec);
            qc.esecuzione();
            
            JOptionPane.showMessageDialog(this, "Sottocategoria aggiornata", "Avviso",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            
        } else 
        {
            QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "categspeciali", "catssid", rec);
            qc.esecuzione();
            int newid = qc.getID();

            JOptionPane.showMessageDialog(this, "Sottocategoria inserita", "Avviso",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

        }
        return ris;
    }

    private boolean controllaEsistenzaCodice() 
    {
        boolean ok = true;
        if (!catsscod.getText().trim().equals("")) 
        {
            String qry2 = "SELECT catssid FROM categspeciali WHERE catsscod = \"" + catsscod.getText().trim() + "\"";
            QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connAz, qry2);
            qc.apertura();
            Record r = qc.successivo();
            qc.chiusura();
            if (r != null) 
            {
                JOptionPane.showMessageDialog(this, "Codice sottocategoria gia' esistente", "Errore",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                catsscod.setText("");
                ok = false;
            }
        }
        return ok;
    }
}
