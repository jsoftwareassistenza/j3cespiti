package cespitipop.disuso;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import nucleo.ConnessioneServer;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;
import nucleo.EnvJazz;

import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloManutenzioneGruppiSpecie extends javax.swing.JPanel implements PannelloPop {

    private final String COL_COD = "Cod.";
    private final String COL_ECC = "E.";
    private final String COL_DESCR = "Gruppo/Specie";
    private final String COL_CATEG = "Categoria";
    private final String COL_D = "D.";
    private final String COL_NOTE = "Dettaglio";
    private final String COL_Q = "% ";
    private final String COL_AGGECC = "c.std";
    private final String COL_NEWECC = "+ ";
    private final String COL_ELIECC = "- ";
    private final String COL_INCR = "SU";
    private final String COL_STATO = "stato";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;
    private Window padre;
    private Frame formGen;

    private int idVis = -1;
    private int idSelez = -1;
    private String azienda = "";
    private String esContabile = "";

    private int[] id;
    private String[] cod;
    private int[] idGru;
    private String[] mcsGru;
    private int[] idSpec;
    private String[] mcsSpec;
    boolean[] flagbutton;

    String[] dtes = null;
    Data oggi = new Data();
    int esito = EnvJazz.PASSWORD_ERRATA;
    ArrayList<Record> vgruppi = new ArrayList();
    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloManutenzioneGruppiSpecie() {
        this.initComponents();
    }

    public PannelloManutenzioneGruppiSpecie(Frame formGen) {
        this();
        this.formGen = formGen;
    }

    public PannelloManutenzioneGruppiSpecie(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        password.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                esito = EnvJazz.PASSWORD_ERRATA;
                if (password.getText().trim().length() == 8) {
                    if (new String(password.getText().trim()).equals("xxjazzyy")) {
                        esito = EnvJazz.SUCCESSO;
                    }
                }
            }
        });

        catbsdescr.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                interrogazione();
            }
        });

        password.setText("");
        password.requestFocus();

        popolaComboGruppi((int) 0);
        popolaComboSpecie("");

        interrogazione();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        catbsdescr = new ui.beans.CampoTestoPop();
        jLabel10 = new javax.swing.JLabel();
        catbstipo = new javax.swing.JComboBox();
        comboGruppi = new javax.swing.JComboBox();
        comboSpecie = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovaCateg = new ui.beans.PopButton();
        jLabel3 = new javax.swing.JLabel();
        password = new ui.beans.CampoPasswordPop();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 60));
        pFiltri.setLayout(null);

        catbsdescr.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.add(catbsdescr);
        catbsdescr.setBounds(896, 16, 192, 24);

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Descrizione");
        pFiltri.add(jLabel10);
        jLabel10.setBounds(796, 16, 88, 24);

        catbstipo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        catbstipo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .....  >", "Materiali", "Immateriali", "Finanziarie" }));
        catbstipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                catbstipoActionPerformed(evt);
            }
        });
        pFiltri.add(catbstipo);
        catbstipo.setBounds(12, 16, 116, 24);

        comboGruppi.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboGruppi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .....  >" }));
        comboGruppi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGruppiActionPerformed(evt);
            }
        });
        pFiltri.add(comboGruppi);
        comboGruppi.setBounds(140, 16, 324, 24);

        comboSpecie.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboSpecie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .....  >" }));
        comboSpecie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSpecieActionPerformed(evt);
            }
        });
        pFiltri.add(comboSpecie);
        comboSpecie.setBounds(476, 16, 320, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 60));
        jPanel1.setLayout(null);

        buttonNuovaCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovaCateg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setToolTipText("Nuova categoria o duplicazione");
        buttonNuovaCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaCategActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovaCateg);
        buttonNuovaCateg.setBounds(20, 12, 40, 32);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Password");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(204, 12, 88, 28);

        password.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
        password.setToolTipText("parola chiave accesso associata all'operatore");
        jPanel1.add(password);
        password.setBounds(304, 12, 256, 28);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];
            DialogManutenzioneGruppiSpecie d = new DialogManutenzioneGruppiSpecie(formGen, true, connAz, connCom, azienda, esContabile, idSelez, esito);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();

            interrogazione();
            requestFocus();
        }
    }//GEN-LAST:event_tabMouseClicked

    private void buttonNuovaCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaCategActionPerformed
        DialogManutenzioneGruppiSpecie d = new DialogManutenzioneGruppiSpecie(formGen, true, connAz, connCom, azienda, esContabile, -1, esito);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();

        interrogazione();
        requestFocus();
    }//GEN-LAST:event_buttonNuovaCategActionPerformed

    private void comboSpecieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSpecieActionPerformed
        interrogazione();
    }//GEN-LAST:event_comboSpecieActionPerformed

    private void comboGruppiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGruppiActionPerformed
        if (comboGruppi.getSelectedIndex() > 0) {
            popolaComboSpecie(mcsGru[comboGruppi.getSelectedIndex() - 1]);
        } else {
            comboSpecie.setSelectedIndex(0);
        }

        interrogazione();
    }//GEN-LAST:event_comboGruppiActionPerformed

    private void catbstipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_catbstipoActionPerformed
        if (catbstipo.getSelectedIndex() > 0) {
            int tipo = catbstipo.getSelectedIndex();
            popolaComboGruppi(tipo);
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
        } else {
            comboGruppi.setSelectedIndex(0);
            comboSpecie.setSelectedIndex(0);
        }

        interrogazione();
    }//GEN-LAST:event_catbstipoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonNuovaCateg;
    private ui.beans.CampoTestoPop catbsdescr;
    private javax.swing.JComboBox catbstipo;
    private javax.swing.JComboBox comboGruppi;
    private javax.swing.JComboBox comboSpecie;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pFiltri;
    private ui.beans.CampoPasswordPop password;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void popolaComboGruppi(int grtipo) {
        comboGruppi.removeAllItems();
        comboGruppi.addItem("<  .....  >");
        String qry = "SELECT gruppobsid,gruppobsmcs,gruppobsdescr FROM gruppibs WHERE gruppobsconto = \"\" AND gruppobsstato=1";
        if (grtipo > 0) {
            qry += " AND gruppobstipo = " + (grtipo + 1);
        }
        qry += " ORDER BY gruppobsmcs";

        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        int num = qRic.numeroRecord();
        if (num > 0) {
            idGru = new int[qRic.numeroRecord()];
            mcsGru = new String[qRic.numeroRecord()];
            for (int i = 0; i < qRic.numeroRecord(); i++) {
                Record r = qRic.successivo();
                comboGruppi.addItem(r.leggiStringa("gruppobsmcs") + " - " + r.leggiStringa("gruppobsdescr"));
                idGru[i] = r.leggiIntero("gruppobsid");
                mcsGru[i] = r.leggiStringa("gruppobsmcs");
            }
        } else {
            idGru = new int[0];
            mcsGru = new String[0];
        }
        qRic.chiusura();
    }

    private void popolaComboSpecie(String codgru) {
        if (!codgru.equals("")) {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            String qry = "SELECT gruppobsid,gruppobsconto,gruppobsdescr FROM gruppibs WHERE gruppobssotto = \"\""
                    + " AND gruppobsmastro = \"" + codgru + "\"  AND gruppobsstato=1 ORDER BY gruppobsmcs,gruppobsecc";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            int num = qRic.numeroRecord();
            if (num > 0) {
                idSpec = new int[qRic.numeroRecord()];
                mcsSpec = new String[qRic.numeroRecord()];
                for (int i = 0; i < qRic.numeroRecord(); i++) {
                    Record r = qRic.successivo();
                    comboSpecie.addItem(r.leggiStringa("gruppobsconto") + " - " + r.leggiStringa("gruppobsdescr"));
                    idSpec[i] = r.leggiIntero("gruppobsid");
                    mcsSpec[i] = r.leggiStringa("gruppobsconto");
                }
            }
            qRic.chiusura();
        } else {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            idSpec = new int[0];
            mcsSpec = new String[0];
        }
    }

    private void interrogazione() {
        vgruppi = new ArrayList();
        String qry = "SELECT * FROM gruppibs WHERE gruppobsstato = 1";
        if (catbstipo.getSelectedIndex() == 1) {
            qry += " AND gruppobstipo = 2";
        }

        if (catbstipo.getSelectedIndex() == 2) {
            qry += " AND gruppobstipo = 3";
        }

        if (catbstipo.getSelectedIndex() == 3) {
            qry += " AND gruppobstipo = 4";
        }

        if (comboSpecie.getSelectedIndex() > 0) {
            qry += " AND  gruppobsmastro = \"" + mcsGru[comboGruppi.getSelectedIndex() - 1] + "\"";
            qry += " AND  gruppobsconto = \"" + mcsSpec[comboSpecie.getSelectedIndex() - 1] + "\"";
        } else {
            if (comboGruppi.getSelectedIndex() > 0) {
                qry += " AND  gruppobsmastro = \"" + mcsGru[comboGruppi.getSelectedIndex() - 1] + "\"";
            }
        }

        if (!catbsdescr.getText().trim().equals("")) {
            qry += " AND (gruppobscateg LIKE \"%" + catbsdescr.getText() + "%\" OR gruppobsdescr LIKE \"%" + catbsdescr.getText() + "%\")";
        }

        qry += " ORDER BY gruppobsmcs,gruppobsecc";

        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vgruppi.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        //
        mostraLista();
    }

    private void mostraLista() {
        final String[] names
                = {
                    COL_COD, COL_ECC, COL_DESCR, COL_CATEG, COL_D, COL_NOTE, COL_Q, COL_AGGECC, COL_NEWECC, COL_ELIECC, COL_INCR, COL_STATO
                };
        final Object[][] data = new Object[vgruppi.size()][names.length];

        id = new int[vgruppi.size()];
        cod = new String[vgruppi.size()];
        flagbutton = new boolean[vgruppi.size()];
        for (int i = 0; i < vgruppi.size(); i++) {
            Record rec = vgruppi.get(i);
            data[i][0] = rec.leggiStringa("gruppobsmcs");
            data[i][1] = rec.leggiStringa("gruppobsecc");
            data[i][2] = rec.leggiStringa("gruppobsdescr");
            data[i][3] = rec.leggiStringa("gruppobscateg");
            data[i][4] = rec.leggiStringa("gruppobsd");
            data[i][5] = rec.leggiStringa("gruppobsnote");
            if (rec.leggiStringa("gruppobsdescr").equals("")) {
                data[i][6] = Formattazione.formValuta(rec.leggiDouble("gruppobsaliq"), 3, 2, 0);
            } else {
                data[i][6] = "";
            }
            if (rec.leggiStringa("gruppobssotto").equals("")) {
                flagbutton[i] = false;
            } else {
                flagbutton[i] = true;
            }
            cod[i] = rec.leggiStringa("gruppobsmcs");
            id[i] = rec.leggiIntero("gruppobsid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_STATO)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_AGGECC)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_NEWECC)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_ELIECC)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_INCR)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tab, true);

        TableColumn buttonColumnA = tab.getColumn(COL_AGGECC);
        TablePopButton buttonA = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (!flagbutton[row]) {
                    button.setEnabled(false);
                }
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_check_circle_white_24dp.png")));
                button.setToolTipText("Assegna una etichetta di categorie precaricate");
            }
        });
        buttonA.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (esito == EnvJazz.SUCCESSO) {
                    String qry = "SELECT * FROM gruppibs WHERE gruppobsid = " + id[row];
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record rgru = qRic.successivo();
                    qRic.chiusura();
                    if (!rgru.leggiStringa("gruppobsconto").equals("") && !rgru.leggiStringa("gruppobssotto").equals("")
                            && rgru.leggiStringa("gruppobsd").equals("") && rgru.leggiStringa("gruppobsecc").equals("")) {
                        DialogListaEtichetteCategoriePrecaricate d = new DialogListaEtichetteCategoriePrecaricate(formGen, true, connAz);
                        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                        d.show();
                        if (!d.leggiCodice().equals("")) {
                            String qryup = "UPDATE gruppibs SET gruppobsecc = \"" + d.leggiCodice() + "\""
                                    + " WHERE gruppobsid = " + id[row];

                            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryup);
                            qd.esecuzione();
                            interrogazione();
                        }
                    } else {
                        if (!rgru.leggiStringa("gruppobsecc").equals("")) {
                            JOptionPane.showMessageDialog(formGen, "La categoria ministeriale ha gia' etichetta di categorie precaricate", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        } else {
                            JOptionPane.showMessageDialog(formGen, "Funzione prevista SOLO per categoria ministeriale", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(formGen, "Funzione NON AMMESSA ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            }
        });
        buttonColumnA.setCellRenderer(buttonA);
        buttonColumnA.setCellEditor(buttonA);

        TableColumn buttonColumnB = tab.getColumn(COL_ELIECC);
        TablePopButton buttonB = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (!flagbutton[row]) {
                    button.setEnabled(false);
                }
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_remove_white_24dp.png")));
                button.setToolTipText("Toglie l'etichetta di categorie precaricate");
            }
        });
        buttonB.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (esito == EnvJazz.SUCCESSO) {
                    String qry = "SELECT * FROM gruppibs WHERE gruppobsid = " + id[row];
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record rgru = qRic.successivo();
                    qRic.chiusura();
                    if (!rgru.leggiStringa("gruppobsconto").equals("") && !rgru.leggiStringa("gruppobssotto").equals("")
                            && rgru.leggiStringa("gruppobsd").equals("") && !rgru.leggiStringa("gruppobsecc").equals("")) {
                        String qryup = "UPDATE gruppibs SET gruppobsecc = \"\""
                                + " WHERE gruppobsid = " + id[row];

                        QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryup);
                        JOptionPane.showMessageDialog(formGen, "Eliminata etichetta", "",
                                JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                        qd.esecuzione();
                        interrogazione();
                    } else {
                        JOptionPane.showMessageDialog(formGen, "Funzione prevista SOLO per categoria ministeriale o assente etichetta da eliminare", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    }
                } else {
                    JOptionPane.showMessageDialog(formGen, "Funzione NON AMMESSA ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            }
        });
        buttonColumnB.setCellRenderer(buttonB);
        buttonColumnB.setCellEditor(buttonB);

        TableColumn buttonColumn1 = tab.getColumn(COL_NEWECC);
        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (!flagbutton[row]) {
                    button.setEnabled(false);
                }
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png")));
                button.setToolTipText("Crea nuova etichetta di categorie precaricate");
            }
        });
        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (esito == EnvJazz.SUCCESSO) {
                    String qry = "SELECT * FROM gruppibs WHERE gruppobsid = " + id[row];
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record rgru = qRic.successivo();
                    qRic.chiusura();

                    if (!rgru.leggiStringa("gruppobsconto").equals("") && !rgru.leggiStringa("gruppobssotto").equals("")
                            && rgru.leggiStringa("gruppobsd").equals("") && rgru.leggiStringa("gruppobsecc").equals("")) {
                        //
                        String codx = "";
                        qry = "SELECT catrscodecc FROM bstitoloecc"
                                + " ORDER BY catrscodecc DESC LIMIT 01";
                        qRic = new QueryInterrogazioneClient(connAz, qry);
                        qRic.apertura();
                        Record r = qRic.successivo();
                        if (r != null) {
                            Integer num = (new Integer(r.leggiStringa("catrscodecc"))) + 1;
                            codx = Formattazione.formIntero(num, 2, "0");

                            DialogGestEtichetteCategoriePrecaricate d = new DialogGestEtichetteCategoriePrecaricate(formGen, true, connAz, codx);
                            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                            d.show();

                            if (d.leggiCodice().equals(codx)) {
                                String qryup = "UPDATE gruppibs SET gruppobsecc = \"" + codx + "\""
                                        + " WHERE gruppobsmastro = \"" + rgru.leggiStringa("gruppobsmastro") + "\""
                                        + " AND    gruppobsconto = \"" + rgru.leggiStringa("gruppobsconto") + "\""
                                        + " AND    gruppobssotto = \"" + rgru.leggiStringa("gruppobssotto") + "\""
                                        + " AND    gruppobsd = \"" + rgru.leggiStringa("gruppobsd") + "\"";
                                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryup);
                                qd.esecuzione();
                            }

                        }
                        qRic.chiusura();
                        interrogazione();
                    } else {
                        JOptionPane.showMessageDialog(formGen, "Funzione prevista SOLO per categoria ministeriale", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    }
                } else {
                    JOptionPane.showMessageDialog(formGen, "Funzione NON AMMESSA ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            }
        });
        buttonColumn1.setCellRenderer(button1);
        buttonColumn1.setCellEditor(button1);

        TableColumn buttonColumn2 = tab.getColumn(COL_INCR);
        TablePopButton button2 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png")));
                button.setToolTipText("Incrementa codice per far spazio a nuova riga");
            }
        });
        button2.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (esito == EnvJazz.SUCCESSO) {
                    String qry = "SELECT * FROM gruppibs WHERE gruppobsid = " + id[row];
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record rgru = qRic.successivo();
                    qRic.chiusura();
                    if (!rgru.leggiStringa("gruppobsconto").equals("") || !rgru.leggiStringa("gruppobsecc").equals("")) {
                        spostaInAltoCodice(rgru);
                        interrogazione();
                    } else {
                        JOptionPane.showMessageDialog(formGen, "Funzione NON prevista per GRUPPO azienda o cat. precaricate ", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    }
                } else {
                    JOptionPane.showMessageDialog(formGen, "Funzione NON AMMESSA ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            }
        });
        buttonColumn2.setCellRenderer(button2);
        buttonColumn2.setCellEditor(button2);

        TableColumn buttonColumn3 = tab.getColumn(COL_STATO);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                button.setToolTipText("Elimina riga");
            }
        });
        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (esito == EnvJazz.SUCCESSO) {
                    String qry = "SELECT * FROM gruppibs WHERE gruppobsid = " + id[row];
                    QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
                    qRic.apertura();
                    Record rgru = qRic.successivo();
                    qRic.chiusura();
                    if (rgru.leggiStringa("gruppobsconto").equals("")) {
                        int ris = JOptionPane.showConfirmDialog(formGen, "Confermi la disattivazione di TUTTO il gruppo?",
                                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                        if (ris == JOptionPane.YES_OPTION) {
                            String qryupdate = "UPDATE gruppibs SET gruppobsstato = 0 WHERE gruppobsmastro  = \"" + rgru.leggiStringa("gruppobsmastro") + "\"";
                            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
                            qd.esecuzione();
                            JOptionPane.showMessageDialog(formGen, "GRUPPO disattivato ", "",
                                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                            interrogazione();
                        }
                    } else if (!rgru.leggiStringa("gruppobssotto").equals("")) {
                        int ris = JOptionPane.showConfirmDialog(formGen, "Confermi la disattivazione di TUTTA la specie?",
                                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                        if (ris == JOptionPane.YES_OPTION) {
                            String qryupdate = "UPDATE gruppibs SET gruppobsstato = 0 WHERE gruppobsmastro  = \"" + rgru.leggiStringa("gruppobsmastro") + "\""
                                    + " AND gruppobsconto  = \"" + rgru.leggiStringa("gruppobsconto") + "\"";
                            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
                            qd.esecuzione();
                            JOptionPane.showMessageDialog(formGen, "Gruppo/sepcie disattivato ", "",
                                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                            interrogazione();
                        }
                    } else{
                        int ris = JOptionPane.showConfirmDialog(formGen, "Confermi la disattivazione della categoria?",
                                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                        if (ris == JOptionPane.YES_OPTION) {
                            String qryupdate = "UPDATE gruppibs SET gruppobsstato = 0 WHERE gruppobsid =" + id[row];
                            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
                            qd.esecuzione();
                            JOptionPane.showMessageDialog(formGen, "Categoria disattivata ", "",
                                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                            interrogazione();
                        }
                    }
                } else {
                    JOptionPane.showMessageDialog(formGen, "Funzione NON AMMESSA ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            }
        });

        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        tab.getColumn(COL_COD).setPreferredWidth(80);
        tab.getColumn(COL_ECC).setPreferredWidth(30);
        tab.getColumn(COL_DESCR).setPreferredWidth(360);
        tab.getColumn(COL_CATEG).setPreferredWidth(400);
        tab.getColumn(COL_NOTE).setPreferredWidth(360);
        tab.getColumn(COL_Q).setPreferredWidth(50);
        tab.getColumn(COL_D).setPreferredWidth(30);
        tab.getColumn(COL_AGGECC).setPreferredWidth(36);
        tab.getColumn(COL_NEWECC).setPreferredWidth(36);
        tab.getColumn(COL_ELIECC).setPreferredWidth(36);
        tab.getColumn(COL_INCR).setPreferredWidth(36);
        tab.getColumn(COL_STATO).setPreferredWidth(36);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_Q).setCellRenderer(rightRenderer);

        tab.getColumn(COL_STATO).setHeaderRenderer(headerRenderer);
        tab.getColumn(COL_AGGECC).setHeaderRenderer(headerRenderer);
        tab.getColumn(COL_NEWECC).setHeaderRenderer(headerRenderer);
        tab.getColumn(COL_ELIECC).setHeaderRenderer(headerRenderer);
        tab.getColumn(COL_INCR).setHeaderRenderer(headerRenderer);
    }

    private void spostaInAltoCodice(Record rgru) {
        int cod = 0;
        String testo = "";
        if (!rgru.leggiStringa("gruppobsd").equals("")) {
            cod = Formattazione.estraiIntero(rgru.leggiStringa("gruppobsd"));
            testo = "Sottocategoria dettaglio";
        }

        if (!rgru.leggiStringa("gruppobssotto").equals("")) {
            cod = Formattazione.estraiIntero(rgru.leggiStringa("gruppobssotto"));
            testo = "Categoria";
        }

        if (!rgru.leggiStringa("gruppobsconto").equals("")) {
            cod = Formattazione.estraiIntero(rgru.leggiStringa("gruppobsconto").substring(0, 2));
            testo = "Specie";
        }
        cod++;
        String codx = Formattazione.formIntero(cod, 2, "0");
        int ris = JOptionPane.showConfirmDialog(this, "Confermi di spostare la riga " + testo + " sul codice " + codx + " ?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            String qry = "SELECT gruppobsid FROM gruppibs "
                    + " WHERE  gruppobsmastro = \"" + rgru.leggiStringa("gruppobsmastro") + "\"";
            if (testo.equals("Specie")) {
                qry += " AND gruppobsconto = \"" + codx + "\"";
            } else {
                qry += " AND gruppobsconto = \"" + rgru.leggiStringa("gruppobsconto") + "\"";
            }

            if (testo.equals("Categoria")) {
                qry += " AND gruppobssotto = \"" + codx + "\"";
            } else {
                qry += " AND gruppobssotto = \"" + rgru.leggiStringa("gruppobssotto") + "\"";
            }

            if (testo.equals("Sottocategoria dettaglio")) {
                qry += " AND gruppobsd = \"" + codx + "\"";
            } else {
                qry += " AND gruppobsd = \"" + rgru.leggiStringa("gruppobsd") + "\"";
            }
            qry += " AND    gruppobsecc = \"" + rgru.leggiStringa("gruppobsecc") + "\"";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            int num = qRic.numeroRecord();
            if (num > 0) {
                JOptionPane.showMessageDialog(this, "Codifica GIA' ESISTENTE ", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            }
            qRic.chiusura();

            String qryup = "";

            if (testo.equals("Specie")) {
                qryup = "UPDATE gruppibs SET gruppobsconto = \"" + codx + "\",gruppobsmcs = CONCAT(gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsd)"
                        + " WHERE gruppobsmastro = \"" + rgru.leggiStringa("gruppobsmastro") + "\""
                        + " AND    gruppobsconto = \"" + rgru.leggiStringa("gruppobsconto") + "\"";
            }

            if (testo.equals("Categoria")) {
                qryup = "UPDATE gruppibs SET gruppobssotto = \"" + codx + "\",gruppobsmcs = CONCAT(gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsd)"
                        + " WHERE gruppobsmastro = \"" + rgru.leggiStringa("gruppobsmastro") + "\""
                        + " AND    gruppobsconto = \"" + rgru.leggiStringa("gruppobsconto") + "\""
                        + " AND    gruppobssotto = \"" + rgru.leggiStringa("gruppobssotto") + "\"";
            }

            if (testo.equals("Sottocategoria dettaglio")) {
                qryup = "UPDATE gruppibs SET gruppobsd = \"" + codx + "\",gruppobsmcs = CONCAT(gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsd)"
                        + " WHERE gruppobsmastro = \"" + rgru.leggiStringa("gruppobsmastro") + "\""
                        + " AND    gruppobsconto = \"" + rgru.leggiStringa("gruppobsconto") + "\""
                        + " AND    gruppobssotto = \"" + rgru.leggiStringa("gruppobssotto") + "\""
                        + " AND    gruppobsd = \"" + rgru.leggiStringa("gruppobsd") + "\"";
            }

            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryup);
            qd.esecuzione();

            JOptionPane.showMessageDialog(this, "Spostamento di UNA posizione EFFETTUATO! ", "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        }
    }
}
