package cespitipop.disuso;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import nucleo.ConnessioneServer;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;
import nucleo.EnvJazz;

import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloCategorieCespitiPrecaricate extends javax.swing.JPanel implements PannelloPop {

    private final String COL_ECC = "I";
    private final String COL_COD = "Cod.";
    private final String COL_NOME = "Sottocategoria";
    private final String COL_DTI = "Val. da";
    private final String COL_DTF = "a data";
    private final String COL_AGEV = "Agev.";
    private final String COL_DETRIVA = "D. iva";
    private final String COL_DETRC = "Detr.C.";
    private final String COL_FABBR = "% NO amm.";
    private final String COL_CF = "Binario";
    private final String COL_MAXC = "Massimale";
    private final String COL_MAXN = "Mass. nolo";
    private final String COL_CLRIP = "Cl. rip.";
    private final String COL_ELIMINA = "Elimina";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;

    private int idVis = -1;
    private int idSelez = -1;
    private int esito = EnvJazz.PASSWORD_ERRATA;
    
    private int[] id;
    
    private String key = "";
    private String azienda = "";
    private String esContabile = "";
    
    String[] dtes = null;
    
    boolean primavolta = true;

    Data oggi = new Data();

    ArrayList<Record> vcatbsid = new ArrayList();
    ArrayList<Record> vcatecc = new ArrayList();


//    ArrayList<Record> vdisattiveid = new ArrayList();
    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloCategorieCespitiPrecaricate() {
        this.initComponents();
    }

    public PannelloCategorieCespitiPrecaricate(Frame formGen) {
        this();
        this.formGen = formGen;
    }

    public PannelloCategorieCespitiPrecaricate(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.key = key;

        password.setEventoFocusLost(new GestoreEventiBeans()
        {
            @Override
            public void lancia()
            {
                esito = EnvJazz.PASSWORD_ERRATA;
                if (password.getText().trim().length() == 8) 
                {
                    if (new String(password.getText().trim()).equals("xxjazzyy")) 
                    {
                        esito = EnvJazz.SUCCESSO;
                    }
                }
            }
        });

        comboCategorieBs.removeAllItems();
        comboCategorieBs.addItem("<  scegli sottocategoria  >");
        vcatecc = new ArrayList();
        String qry = "SELECT catrscodecc,catrsdescr FROM bstitoloecc ORDER BY catrscodecc";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record rl = q.successivo();
        while (rl != null) 
        {
            vcatecc.add(rl);
            comboCategorieBs.addItem(rl.leggiStringa("catrscodecc") + "-" + rl.leggiStringa("catrsdescr"));
            rl = q.successivo();
        }
        q.chiusura();

        if (!key.equals("")) 
        {
            interrogazione(key);
            primavolta = false;
        } else 
        {
            comboCategorieBs.requestFocus();
            primavolta = false;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        grucat = new javax.swing.JLabel();
        catbsgruppo = new javax.swing.JLabel();
        comboCategorieBs = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovaCateg = new ui.beans.PopButton();
        jLabel3 = new javax.swing.JLabel();
        password = new ui.beans.CampoPasswordPop();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 40));
        pFiltri.setLayout(null);

        grucat.setBackground(new java.awt.Color(202, 236, 221));
        grucat.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        grucat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        grucat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        grucat.setOpaque(true);
        pFiltri.add(grucat);
        grucat.setBounds(312, 8, 40, 24);

        catbsgruppo.setBackground(new java.awt.Color(202, 236, 221));
        catbsgruppo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        catbsgruppo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        catbsgruppo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        catbsgruppo.setOpaque(true);
        pFiltri.add(catbsgruppo);
        catbsgruppo.setBounds(364, 8, 480, 24);

        comboCategorieBs.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboCategorieBs.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboCategorieBsItemStateChanged(evt);
            }
        });
        comboCategorieBs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategorieBsActionPerformed(evt);
            }
        });
        pFiltri.add(comboCategorieBs);
        comboCategorieBs.setBounds(36, 8, 260, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 45));
        jPanel1.setLayout(null);

        buttonNuovaCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovaCateg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setToolTipText("Nuova sottocategoria con eccezioni rispetto allo standard");
        buttonNuovaCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaCategActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovaCateg);
        buttonNuovaCateg.setBounds(32, 4, 40, 40);

        jLabel3.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Password");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(204, 8, 88, 28);

        password.setFont(new java.awt.Font("Monospaced", 1, 16)); // NOI18N
        password.setToolTipText("parola chiave accesso associata all'operatore");
        jPanel1.add(password);
        password.setBounds(304, 8, 256, 28);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0)
        {
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];

            DialogGestCategoriePrecaricateCespiti d = new DialogGestCategoriePrecaricateCespiti(formGen, true, connAz, connCom, azienda, esContabile, idSelez, key);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();
            requestFocus();

            interrogazione(key);
        }
    }//GEN-LAST:event_tabMouseClicked

    private void buttonNuovaCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaCategActionPerformed
        if (comboCategorieBs.getSelectedIndex() > 0)
        {
            DialogGestCategoriePrecaricateCespiti d = new DialogGestCategoriePrecaricateCespiti(formGen, true, connAz, connCom, azienda, esContabile, -1, key);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();
            requestFocus();

            interrogazione(key);
        } else 
        {
            JOptionPane.showMessageDialog(this, "Assente selezione del gruppo ", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
    }//GEN-LAST:event_buttonNuovaCategActionPerformed

    private void comboCategorieBsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboCategorieBsItemStateChanged
        
    }//GEN-LAST:event_comboCategorieBsItemStateChanged

    private void comboCategorieBsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategorieBsActionPerformed
        if (!primavolta) 
        {
            if (comboCategorieBs.getSelectedIndex() > 0) 
            {
                Record r = vcatecc.get(comboCategorieBs.getSelectedIndex() - 1);

                grucat.setText(r.leggiStringa("catrscodecc"));
                catbsgruppo.setText(r.leggiStringa("catrsdescr"));
                key = r.leggiStringa("catrscodecc");
                interrogazione(key);
            } else 
            {
                JOptionPane.showMessageDialog(this, "Scegli un gruppo alla volta ", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            }
        }
    }//GEN-LAST:event_comboCategorieBsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonNuovaCateg;
    private javax.swing.JLabel catbsgruppo;
    private javax.swing.JComboBox<String> comboCategorieBs;
    private javax.swing.JLabel grucat;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pFiltri;
    private ui.beans.CampoPasswordPop password;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void interrogazione(String keyi) 
    {
        vcatbsid = new ArrayList();
        String qry = "SELECT * FROM categspeciali WHERE catssstato = 1";

        if (!keyi.equals("")) {
            qry += " AND catsscodecc = \"" + keyi + "\"";
        }
        qry += " ORDER BY catsscod,catssdescr";
        
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) 
        {
            vcatbsid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        //
        mostraLista();
    }

    private void mostraLista() 
    {
        final String[] names = 
        {
            COL_ECC, COL_COD, COL_NOME, COL_DTI, COL_DTF, COL_AGEV, COL_DETRIVA, COL_DETRC, COL_FABBR, COL_CF, COL_MAXC, COL_MAXN, COL_CLRIP, COL_ELIMINA
        };
        final Object[][] data = new Object[vcatbsid.size()][names.length];

        id = new int[vcatbsid.size()];

        for (int i = 0; i < vcatbsid.size(); i++) 
        {
            Record rec = vcatbsid.get(i);
            data[i][0] = rec.leggiStringa("catsscodecc");
            data[i][1] = rec.leggiStringa("catsscod");
            data[i][2] = rec.leggiStringa("catssdescr");

            data[i][3] = (new Data(rec.leggiStringa("catssdtiniz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][4] = (new Data(rec.leggiStringa("catssdtfine"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            data[i][5] = Formattazione.formValuta(rec.leggiDouble("catssquotaage"), 3, 2, 0);
            data[i][6] = Formattazione.formValuta(rec.leggiDouble("catssdetriva"), 3, 2, 0);
            data[i][7] = Formattazione.formValuta(rec.leggiDouble("catssdetrcosto"), 3, 2, 0);
            data[i][8] = Formattazione.formValuta(rec.leggiDouble("catssdetrfabbr"), 3, 2, 0);
            if (rec.leggiIntero("catssbinario") == 0)
            {
                data[i][9] = "C e F";
            } else if (rec.leggiIntero("catssbinario") == 1)
            {
                data[i][9] = "solo F";
            } else 
            {
                data[i][9] = "solo C";
            }
            data[i][10] = Formattazione.formValuta(rec.leggiDouble("catssmaxcosto"), 8, 2, 0);
            data[i][11] = Formattazione.formValuta(rec.leggiDouble("catssmaxnolo"), 8, 2, 0);
            String query = "SELECT bsclmanutcod,bsclmanutquota FROM bsclassimanut"
                    + " WHERE bsclmanutid = " + rec.leggiIntero("catssclmanutid");
            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, query);
            q.apertura();
            Record rm = q.successivo();
            if (rm != null) 
            {
                data[i][12] = rm.leggiStringa("bsclmanutcod") + " : " + rm.leggiDouble("bsclmanutquota");
            } else
            {
                data[i][12] = "";
            }

            id[i] = rec.leggiIntero("catssid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_ELIMINA)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tab, true);

        TableColumn buttonColumn3 = tab.getColumn(COL_ELIMINA);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                button.setToolTipText("Disattiva/elimina categoria");
            }
        });

        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (esito == EnvJazz.SUCCESSO)
                {
                    boolean ko = chiediConfermaElim(id[row]);
                    if (ko)
                    {
                        interrogazione(key);
                    }
                } else
                {
                    JOptionPane.showMessageDialog(formGen, "Funzione NON AMMESSA ", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                }
            }
        });
        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        tab.getColumn(COL_ECC).setPreferredWidth(28);
        tab.getColumn(COL_COD).setPreferredWidth(60);
        tab.getColumn(COL_NOME).setPreferredWidth(260);
        tab.getColumn(COL_DTI).setPreferredWidth(80);
        tab.getColumn(COL_DTF).setPreferredWidth(80);
        tab.getColumn(COL_AGEV).setPreferredWidth(72);
        tab.getColumn(COL_DETRIVA).setPreferredWidth(72);
        tab.getColumn(COL_DETRC).setPreferredWidth(72);
        tab.getColumn(COL_FABBR).setPreferredWidth(72);
        tab.getColumn(COL_CF).setPreferredWidth(72);
        tab.getColumn(COL_MAXC).setPreferredWidth(90);
        tab.getColumn(COL_MAXN).setPreferredWidth(90);
        tab.getColumn(COL_CLRIP).setPreferredWidth(60);
        tab.getColumn(COL_ELIMINA).setPreferredWidth(30);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_MAXC).setCellRenderer(rightRenderer);
        tab.getColumn(COL_MAXN).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tab.getColumn(COL_DTI).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DTF).setCellRenderer(centerRenderer);
        tab.getColumn(COL_AGEV).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DETRIVA).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DETRC).setCellRenderer(centerRenderer);
        tab.getColumn(COL_FABBR).setCellRenderer(centerRenderer);

        tab.getColumn(COL_ELIMINA).setHeaderRenderer(headerRenderer);
    }

    final boolean chiediConfermaElim(int idelim)
    {
        boolean eli = false;
        int ris = JOptionPane.showConfirmDialog(this, "Confermi l'eliminazione definitiva della categoria?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) 
        {
            String qrydelete = "DELETE FROM categspeciali  WHERE catssid =" + idelim;
            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
            qd.esecuzione();

            eli = true;
            JOptionPane.showMessageDialog(this, "Categoria ELIMINATA ", "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        } else
        {
            ris = JOptionPane.showConfirmDialog(this, "Confermi la disattivazione della categoria?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (ris == JOptionPane.YES_OPTION) 
            {
                String qryupdate = "UPDATE categspeciali SET catssstato = 0 WHERE catssid =" + idelim;
                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
                qd.esecuzione();
                JOptionPane.showMessageDialog(this, "Categoria disattivata ", "Invio",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                eli = true;
            }
        }
        return eli;
    }

}
