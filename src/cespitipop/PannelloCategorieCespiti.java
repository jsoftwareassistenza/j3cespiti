package cespitipop;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JLabel;
import nucleo.ConnessioneServer;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;
import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloCategorieCespiti extends javax.swing.JPanel implements PannelloPop, PannelloPopListener {

    private final String COL_TIPO = "Codice";
    private final String COL_NOME = "Categoria";
    private final String COL_CATPADRE = "Codice min.";
    private final String COL_METODO = "Metodo";
    private final String COL_ECC = "I";
    private final String COL_FABBR = "% No ammort.";
    private final String COL_QC = "% Civile";
    private final String COL_Q1 = "% 1'anno";
    private final String COL_QF = "% Fiscale";
    private final String COL_CRITERIO = "Criterio";
    private final String COL_MAX = "Massimale";
    private final String COL_AGEV = "Agev.";
    private final String COL_DETRI = "Detr. iva";
    private final String COL_DETRC = "Detr. Conto";
    private final String COL_PDC = "P.D.C";
    private final String COL_CESP = "Lista";
    private final String COL_GEST = "Gestioni";
    private final String COL_STATO = "Stato";

//    public JTabbedPane pTab;
    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;
    private PannelloPopListener listener = null;

    private Window padre;
    private Frame formGen;
    private PannelloCruscottoCespiti cruscotto;

    private int idVis = -1;
    private int idSelez = -1;

    private String azienda = "";
    private String esContabile = "";
    boolean primavolta = true;
    private int[] id;

    String[] dtes = null;
    Data oggi = new Data();

    ArrayList<Record> vmcsGru = new ArrayList();
    ArrayList<Record> vmcsSpec = new ArrayList();
    ArrayList<Record> vmcsCateg = new ArrayList();

    String gruppoSelez = "";
    String specieSelez = "";
    String categSelez = "";
//    private String[] mcsSpec;

    private boolean[] flaggestioni = null;
    private boolean[] flagerrorepdc = null;
    private boolean[] flagnocesp = null;
    private boolean flagCriterioFiscale = false;

    boolean erroregrave = false;

    ArrayList<Record> vcatbsid = new ArrayList();
    ArrayList<Record> vdisattiveid = new ArrayList();
    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloCategorieCespiti() {
        this.initComponents();
    }

    public PannelloCategorieCespiti(Window padrea, Frame formGen, PannelloCruscottoCespiti cruscotto) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
        this.cruscotto = cruscotto;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        waitloc.ferma();
        headerRenderer.setColor(new Color(255,255,255));
    
        impostaListener(cruscotto);       
        

        catbsdescr.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia(KeyEvent evento) {
                if (!catbsdescr.getText().trim().equals("") && catbsdescr.getText().trim().length() >= 2) {
                    interrogazione();
                }
            }
        });

        buttonManutenzione.setVisible(false);

        String prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "flagCriterioFiscale");
        if (prop.equals("0")) {
            flagCriterioFiscale = (false);
        } else {
            flagCriterioFiscale = (true);
        }

        popolaComboGruppi();
        
        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "gruppo");
        for (int i = 0; i < vmcsGru.size(); i++) {
            Record r = vmcsGru.get(i);
            if (r.leggiStringa("gruppobsmcs").equals(prop)) {
                comboGruppi.setSelectedIndex(i + 1);
                grucat.setText(prop);
                gruppoSelez = prop;
                popolaComboSpecie(prop);
            }
        }

        primavolta = true;
        prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "specie");
        for (int i = 0; i < vmcsSpec.size(); i++) {
            Record r = vmcsSpec.get(i);
            if (r.leggiStringa("gruppobsconto").equals(prop)) {
                comboSpecie.setSelectedIndex(i + 1);
                speccat.setText(prop);
                specieSelez = prop;
                popolaComboCategorie(grucat.getText(), prop);
            }
        }

        interrogazione();
        primavolta = false;
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        catbsdescr = new ui.beans.CampoTestoPop();
        jLabel10 = new javax.swing.JLabel();
        grucat = new javax.swing.JLabel();
        speccat = new javax.swing.JLabel();
        buttonMostraDisattive = new ui.beans.PopButton();
        comboGruppi = new javax.swing.JComboBox();
        comboSpecie = new javax.swing.JComboBox();
        comboCategorieMin = new javax.swing.JComboBox<>();
        labattivedisattive = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        buttonNuovaCateg = new ui.beans.PopButton();
        buttonManutenzione = new ui.beans.PopButton();
        buttonUtility = new ui.beans.PopButton();
        buttonPrendiStandard = new ui.beans.PopButton();
        buttonCodici = new ui.beans.PopButton();
        errorMessage = new javax.swing.JLabel();
        buttonManutenzione1 = new ui.beans.PopButton();
        labquadra = new javax.swing.JLabel();
        buttonQuadratura = new ui.beans.PopButton();
        waitloc = new nucleo.WaitJazzBianco();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 65));
        pFiltri.setLayout(null);

        catbsdescr.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.add(catbsdescr);
        catbsdescr.setBounds(670, 35, 280, 24);

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Specie");
        pFiltri.add(jLabel10);
        jLabel10.setBounds(680, 4, 88, 24);

        grucat.setBackground(new java.awt.Color(202, 236, 221));
        grucat.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        grucat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        grucat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        grucat.setOpaque(true);
        pFiltri.add(grucat);
        grucat.setBounds(120, 4, 40, 24);

        speccat.setBackground(new java.awt.Color(202, 236, 221));
        speccat.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        speccat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        speccat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        speccat.setOpaque(true);
        pFiltri.add(speccat);
        speccat.setBounds(780, 4, 40, 24);

        buttonMostraDisattive.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_lock_white_20dp.png"))); // NOI18N
        buttonMostraDisattive.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonMostraDisattive.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonMostraDisattive.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonMostraDisattive.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonMostraDisattive.setToolTipText("Mostra categorie disattive");
        buttonMostraDisattive.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonMostraDisattiveActionPerformed(evt);
            }
        });
        pFiltri.add(buttonMostraDisattive);
        buttonMostraDisattive.setBounds(1224, 30, 52, 28);

        comboGruppi.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboGruppi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .........  >" }));
        comboGruppi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGruppiActionPerformed(evt);
            }
        });
        pFiltri.add(comboGruppi);
        comboGruppi.setBounds(170, 4, 550, 24);

        comboSpecie.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboSpecie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .........  >" }));
        comboSpecie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSpecieActionPerformed(evt);
            }
        });
        pFiltri.add(comboSpecie);
        comboSpecie.setBounds(830, 4, 350, 24);

        comboCategorieMin.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboCategorieMin.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "<   ..........    >" }));
        comboCategorieMin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategorieMinActionPerformed(evt);
            }
        });
        pFiltri.add(comboCategorieMin);
        comboCategorieMin.setBounds(120, 35, 450, 24);

        labattivedisattive.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        labattivedisattive.setText("visualizzate le sole categorie ATTIVE");
        pFiltri.add(labattivedisattive);
        labattivedisattive.setBounds(970, 35, 240, 28);

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Descrizione");
        pFiltri.add(jLabel11);
        jLabel11.setBounds(570, 35, 88, 24);

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Gruppo");
        pFiltri.add(jLabel12);
        jLabel12.setBounds(20, 4, 88, 24);

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Categoria min.");
        pFiltri.add(jLabel13);
        jLabel13.setBounds(8, 35, 100, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 40));
        jPanel1.setLayout(null);

        buttonNuovaCateg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovaCateg.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaCateg.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaCateg.setToolTipText("Nuova sottocategoria");
        buttonNuovaCateg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaCategActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovaCateg);
        buttonNuovaCateg.setBounds(20, 4, 56, 32);

        buttonManutenzione.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_build_white_24dp.png"))); // NOI18N
        buttonManutenzione.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonManutenzione.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonManutenzione.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonManutenzione.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonManutenzione.setToolTipText("Funzione di utilita' (disattiva categorie non utilizzate)");
        buttonManutenzione.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonManutenzioneActionPerformed(evt);
            }
        });
        jPanel1.add(buttonManutenzione);
        buttonManutenzione.setBounds(444, 4, 56, 32);

        buttonUtility.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_low_priority_white_24dp.png"))); // NOI18N
        buttonUtility.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonUtility.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonUtility.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonUtility.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonUtility.setToolTipText("Funzione di utilita' (sostituisce, sui cespiti interessati, una categoria disattiva con una attiva)");
        buttonUtility.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonUtilityActionPerformed(evt);
            }
        });
        jPanel1.add(buttonUtility);
        buttonUtility.setBounds(264, 4, 56, 32);

        buttonPrendiStandard.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_get_app_white_24dp.png"))); // NOI18N
        buttonPrendiStandard.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonPrendiStandard.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonPrendiStandard.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonPrendiStandard.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonPrendiStandard.setToolTipText("Preleva categorie da tabella ministeriale o tabelle precaricate");
        buttonPrendiStandard.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrendiStandardActionPerformed(evt);
            }
        });
        jPanel1.add(buttonPrendiStandard);
        buttonPrendiStandard.setBounds(176, 4, 56, 32);

        buttonCodici.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_list_white_20dp.png"))); // NOI18N
        buttonCodici.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonCodici.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonCodici.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonCodici.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonCodici.setToolTipText("Unifica ai codici ministeriali");
        buttonCodici.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCodiciActionPerformed(evt);
            }
        });
        jPanel1.add(buttonCodici);
        buttonCodici.setBounds(356, 4, 56, 32);

        errorMessage.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        errorMessage.setForeground(new java.awt.Color(204, 51, 0));
        errorMessage.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        errorMessage.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jPanel1.add(errorMessage);
        errorMessage.setBounds(620, 6, 408, 28);

        buttonManutenzione1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_alert_white_24dp.png"))); // NOI18N
        buttonManutenzione1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonManutenzione1.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonManutenzione1.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonManutenzione1.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonManutenzione1.setToolTipText("Dettaglio collegamento a piano dei conti");
        buttonManutenzione1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonManutenzione1ActionPerformed(evt);
            }
        });
        jPanel1.add(buttonManutenzione1);
        buttonManutenzione1.setBounds(1040, 4, 56, 32);

        labquadra.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        labquadra.setText("effettua quadratura con bilancio");
        jPanel1.add(labquadra);
        labquadra.setBounds(1180, 4, 216, 28);

        buttonQuadratura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_euro_symbol_white_20dp.png"))); // NOI18N
        buttonQuadratura.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonQuadratura.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonQuadratura.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonQuadratura.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonQuadratura.setToolTipText("Confronta totali con saldi contabili");
        buttonQuadratura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonQuadraturaActionPerformed(evt);
            }
        });
        jPanel1.add(buttonQuadratura);
        buttonQuadratura.setBounds(1110, 4, 52, 32);
        jPanel1.add(waitloc);
        waitloc.setBounds(530, 0, 36, 40);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
            boolean errore = false;
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];
            String codsel = (String) tab.getValueAt(tab.getSelectedRow(), FunzioniUtilita.getColumnIndex(tab, COL_TIPO));
            if (codsel.equals("ERRORE tab.min.")) {
                errore = true;
            }

            DialogCategorieCespiti d = new DialogCategorieCespiti(formGen, true, connAz, connCom, azienda, esContabile, idSelez, errore);
            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
            d.show();
            requestFocus();

            interrogazione();
        }
    }//GEN-LAST:event_tabMouseClicked

    private void buttonNuovaCategActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaCategActionPerformed
        DialogCategorieCespiti d = new DialogCategorieCespiti(formGen, true, connAz, connCom, azienda, esContabile, -1, false);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        requestFocus();

        interrogazione();
    }//GEN-LAST:event_buttonNuovaCategActionPerformed

    private void buttonMostraDisattiveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonMostraDisattiveActionPerformed
        if (buttonMostraDisattive.getToolTipText().equals("Mostra categorie ATTIVE")) {
            buttonMostraDisattive.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_lock_white_24dp.png")));
            buttonMostraDisattive.setToolTipText("Mostra categorie disattive");
            labattivedisattive.setText("visualizzate le sole categorie ATTIVE");
            interrogazione();
            return;
        }

        labattivedisattive.setText("visualizzate le sole categorie DISATTIVE");
        caricaDisattivate();
    }//GEN-LAST:event_buttonMostraDisattiveActionPerformed

    private void buttonManutenzioneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonManutenzioneActionPerformed
        int ris = JOptionPane.showConfirmDialog(this, "La funzione disattiva le categorie senza cespiti associati.\nPROSEGUI?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            for (int i = 0; i < tab.getRowCount(); i++) {
                if (((String) tab.getValueAt(i, FunzioniUtilita.getColumnIndex(tab, COL_TIPO))).equals("NON ha cespiti")) {
                    String qryupdate = "UPDATE categoriebs SET catbsstato = 0 WHERE catbsid =" + id[i];
                    QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
                    qd.esecuzione();
                }
            }
            JOptionPane.showMessageDialog(formGen, "Categorie senza cespiti disattivate ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

            interrogazione();
//            buttonUtility.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ICONA_VAR_24_ROSSA.png")));
//            interrogazione();
        }
    }//GEN-LAST:event_buttonManutenzioneActionPerformed

    private void buttonUtilityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUtilityActionPerformed
        DialogUtilitaSostituzioneCategoriaDisattiva d = new DialogUtilitaSostituzioneCategoriaDisattiva(formGen, true, connAz);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        interrogazione();
    }//GEN-LAST:event_buttonUtilityActionPerformed

    private void buttonPrendiStandardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrendiStandardActionPerformed
        DialogInserisceCategorie d = new DialogInserisceCategorie(formGen, true, connAz);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        interrogazione();
    }//GEN-LAST:event_buttonPrendiStandardActionPerformed

    private void buttonCodiciActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCodiciActionPerformed
        DialogAllineaCodiciCategGruppi d = new DialogAllineaCodiciCategGruppi(formGen, true, connAz);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        interrogazione();
    }//GEN-LAST:event_buttonCodiciActionPerformed

    private void comboGruppiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGruppiActionPerformed
        if (!primavolta) {
            if (comboGruppi.getSelectedIndex() > 0) {
                Record r = vmcsGru.get(comboGruppi.getSelectedIndex() - 1);
                popolaComboSpecie(r.leggiStringa("gruppobsmcs"));
            }
//            else {
//                comboSpecie.setSelectedIndex(0);
//            }

            interrogazione();
        }
    }//GEN-LAST:event_comboGruppiActionPerformed

    private void comboSpecieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSpecieActionPerformed
        if (!primavolta) {
            if (comboSpecie.getSelectedIndex() > 0) {
                Record r = vmcsSpec.get(comboSpecie.getSelectedIndex() - 1);
                popolaComboCategorie(r.leggiStringa("gruppobsmastro"), r.leggiStringa("gruppobsconto"));
            }
//            else {
//                comboCategorieMin.setSelectedIndex(0);
//            }        
            interrogazione();
        }
    }//GEN-LAST:event_comboSpecieActionPerformed

    private void comboCategorieMinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategorieMinActionPerformed
        if (!primavolta) {
            interrogazione();
        }
    }//GEN-LAST:event_comboCategorieMinActionPerformed

    private void buttonQuadraturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonQuadraturaActionPerformed
        DialogQuadraturaContabile d = new DialogQuadraturaContabile(formGen, true, connAz, azienda);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        requestFocus();

//        interrogazione();
    }//GEN-LAST:event_buttonQuadraturaActionPerformed

    private void buttonManutenzione1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonManutenzione1ActionPerformed
       DialogEsponiLegamiConPdc d = new DialogEsponiLegamiConPdc(formGen, true, connAz, azienda, true);
       UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
       d.show();
    }//GEN-LAST:event_buttonManutenzione1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonCodici;
    private ui.beans.PopButton buttonManutenzione;
    private ui.beans.PopButton buttonManutenzione1;
    private ui.beans.PopButton buttonMostraDisattive;
    private ui.beans.PopButton buttonNuovaCateg;
    private ui.beans.PopButton buttonPrendiStandard;
    private ui.beans.PopButton buttonQuadratura;
    private ui.beans.PopButton buttonUtility;
    private ui.beans.CampoTestoPop catbsdescr;
    private javax.swing.JComboBox<String> comboCategorieMin;
    private javax.swing.JComboBox comboGruppi;
    private javax.swing.JComboBox comboSpecie;
    private javax.swing.JLabel errorMessage;
    private javax.swing.JLabel grucat;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labattivedisattive;
    private javax.swing.JLabel labquadra;
    private javax.swing.JPanel pFiltri;
    private javax.swing.JLabel speccat;
    private javax.swing.JTable tab;
    private nucleo.WaitJazzBianco waitloc;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener l) {
        listener = l;
    }

    private void popolaComboGruppi() {
        comboGruppi.removeAllItems();
        comboGruppi.addItem("<  .....  >");
        String qry = "SELECT DISTINCT gruppobsid,gruppobsmcs,gruppobsdescr FROM categoriebs JOIN gruppibs ON catbsgruppo = gruppobsmcs WHERE gruppobsconto = \"\""
                + " ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            if(!r.leggiStringa("gruppobsmcs").equals("30") && !r.leggiStringa("gruppobsmcs").equals("40"))
            {
                comboGruppi.addItem(r.leggiStringa("gruppobsmcs") + " - " + r.leggiStringa("gruppobsdescr") +" - IMMOBILIZZAZIONI MATERIALI" );
                vmcsGru.add(r);
                r = qRic.successivo();
            }
            else
            {
                comboGruppi.addItem(r.leggiStringa("gruppobsmcs") + " - " + r.leggiStringa("gruppobsdescr"));
                vmcsGru.add(r);
                r = qRic.successivo();
            }
        }
        qRic.chiusura();
    }

    private void popolaComboSpecie(String codgru) {
        if (!codgru.equals("")) {
            primavolta = true;
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            vmcsSpec.clear();
            String qry = "SELECT DISTINCT gruppobsid,gruppobsmcs,gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsdescr,gruppobsnote FROM categoriebs"
                    + " JOIN gruppibs ON catbsgruppo = gruppobsmastro AND catbsspecie = gruppobsconto"
                    + " WHERE gruppobssotto = \"\""
                    + " AND gruppobsmastro = \"" + codgru + "\""
                    + " AND gruppobsconto <> \"\""
                    + " ORDER BY gruppobsmcs";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            while (r != null) {
                comboSpecie.addItem(r.leggiStringa("gruppobsconto") + " - " + r.leggiStringa("gruppobsdescr") + r.leggiStringa("gruppobsnote"));
                vmcsSpec.add(r);
                r = qRic.successivo();
            }
            qRic.chiusura();
            primavolta = false;
        } else {
            primavolta = true;
            comboSpecie.removeAllItems();
            primavolta = false;
        }
    }

    private void popolaComboCategorie(String codgru, String codspecie) {
        if (!codgru.equals("") && !codspecie.equals("")) {
            primavolta = true;
            comboCategorieMin.removeAllItems();
            comboCategorieMin.addItem("<  .....  >");
            String qry = "SELECT DISTINCT gruppobsid,gruppobsmcs,gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsdescr,gruppobscateg,gruppobsnote FROM categoriebs"
                    + " JOIN gruppibs ON catbsgruppo = gruppobsmastro AND catbsspecie = gruppobsconto"
                    + " WHERE catbsstato=1 AND gruppobssotto <> \"\""
                    + " AND gruppobsmastro = \"" + codgru + "\""
                    + " AND gruppobsconto = \"" + codspecie + "\""
                    + " ORDER BY gruppobsmcs";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            while (r != null) {
                comboCategorieMin.addItem(r.leggiStringa("gruppobssotto") + " - " + r.leggiStringa("gruppobscateg") + r.leggiStringa("gruppobsnote"));
                vmcsCateg.add(r);
                r = qRic.successivo();
            }
            qRic.chiusura();
            primavolta = false;
        } else {
            primavolta = true;
            comboCategorieMin.removeAllItems();
            primavolta = false;
        }
    }

    private void interrogazione() {
        vcatbsid = new ArrayList();
        String qry = "SELECT categoriebs.*,gruppobsid FROM categoriebs LEFT JOIN gruppibs ON catbsgruppo=gruppobsmastro AND catbsspecie=gruppobsconto AND catbsdescr=gruppobscateg WHERE catbsstato = 1";
        
        if (comboGruppi.getSelectedIndex() > 0) {
            Record r = vmcsGru.get(comboGruppi.getSelectedIndex() - 1);
            qry += " AND catbsgruppo = \"" + r.leggiStringa("gruppobsmcs") + "\"";
        }

        if (comboSpecie.getSelectedIndex() > 0) {
            Record r = vmcsSpec.get(comboSpecie.getSelectedIndex() - 1);
            qry += " AND catbsspecie = \"" + r.leggiStringa("gruppobsconto") + "\"";
        }

        if (comboCategorieMin.getSelectedIndex() > 0) {
            Record r = vmcsCateg.get(comboCategorieMin.getSelectedIndex() - 1);
            qry += " AND catbssotto = \"" + r.leggiStringa("gruppobssotto") + "\"";
        }

        if (!catbsdescr.getText().trim().equals("")) {
            qry += " AND catbsdescr LIKE \"%" + catbsdescr.getText() + "%\"";
        }

        qry += " ORDER BY catbsgruppo,catbsspecie,catbssotto,gruppobsid DESC";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vcatbsid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        //
        mostraLista();
    }

    private void mostraLista() {
        boolean attivaManutenzionePerc = false;
        boolean attivaManutenzionePerf = false;
        boolean attivaManutenzionePer1 = false;
        boolean messaggioErrore = false;
        final String[] names = {
            COL_CATPADRE, COL_TIPO, COL_NOME, COL_METODO,COL_CRITERIO,COL_QC, COL_QF,COL_FABBR,COL_MAX, COL_Q1, COL_AGEV, COL_DETRC, COL_DETRI, COL_PDC, COL_CESP,/* COL_GEST, */COL_STATO
        };
        final Object[][] data = new Object[vcatbsid.size()][names.length];

        id = new int[vcatbsid.size()];
        flagerrorepdc = new boolean[vcatbsid.size()];
        flaggestioni = new boolean[vcatbsid.size()];
        flagnocesp = new boolean[vcatbsid.size()];

        for (int i = 0; i < vcatbsid.size(); i++) {
            Record rec = vcatbsid.get(i);
            String qry = "SELECT gruppobsbil,gruppobscateg,gruppobsmcs FROM gruppibs"
                    + " WHERE gruppobsmastro = \"" + rec.leggiStringa("catbsgruppo") + "\""
                    + " AND gruppobsconto = \"" + rec.leggiStringa("catbsspecie") + "\""
                    + " AND gruppobssotto = \"" + rec.leggiStringa("catbssotto") + "\" ORDER BY gruppobscateg DESC";
            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
            q.apertura();
            Record rd = q.successivo();
            q.chiusura();
            flagnocesp[i] = false;
            if (rd != null) {
                String qry2 = "SELECT cespcategid FROM cespiti WHERE cespcategid = " + rec.leggiIntero("catbsid") + " LIMIT 01";
                QueryInterrogazioneClient q2 = new QueryInterrogazioneClient(connAz, qry2);
                q2.apertura();
                Record r2 = q2.successivo();
                q2.chiusura();
                if (r2 == null) {
                    flagnocesp[i] = true;
                    buttonManutenzione.setVisible(true);
                } 
                
                String codice = rec.leggiStringa("catbscod");
                      

                if (rec.leggiIntero("gruppobsid") == 0) {
                    data[i][0] = rec.leggiStringa("catbssotto") + "-" + rd.leggiStringa("gruppobsmcs");
                    data[i][1] = rec.leggiStringa("catbssotto") + "-" + codice;          
                } else {
                    data[i][0] = rec.leggiStringa("catbssotto") + "-" + codice;
                    data[i][1]="";
                }
            } else {
                flagnocesp[i] = true;
                data[i][1] = "ERRORE tab.min.";
                data[i][0] = "";
                erroregrave = true;
            }

            data[i][2] = rec.leggiStringa("catbsdescr");

            //metodo
            switch (rec.leggiIntero("catbsflagtipoammc")) {
                case 0:
                    switch (rec.leggiIntero("catbscriterio1")) {
                        case 7:
                            data[i][3] = "Irregolare o ragionato";
                            break;
                        default:
                            data[i][3] = ".....";
                            break;
                    }
                case 1:
                    data[i][3] = "In base all'utilizzo";
                    break;
                default:
                    data[i][3] = "Irregolare o ragionato";
                    break;
            }
            
            switch (rec.leggiIntero("catbscriterio1")) {
                case 0:
                    data[i][4] = "Quota ordinaria";
                    break;
                case 1:
                    data[i][4] = "Quote in mesi";
                    break;
                case 2:
                    data[i][4] = "Quote in giorni";
                    break;
                case 3:
                    data[i][4] = "Accellerato (2 x Aliq.)";
                    break;
                case 4:
                    data[i][4] = "Ritardato (50% Aliq)";
                    break;
                case 5:
                    data[i][4] = "Nessun ammortamento";
                    break;
                case 6:
                    data[i][4] = "Totale (100%)";
                    break;
                default:
                    data[i][4] = "%  libera";
                    break;
            }

            data[i][5] = Formattazione.formValuta(rec.leggiDouble("catbsquotac"), 3, 2, 0);
            if (rec.leggiDouble("catbsquotac") == 0 && rec.leggiIntero("catbscriterio1") != 5) {
                attivaManutenzionePerc = true;
            }

            data[i][6] = Formattazione.formValuta(rec.leggiDouble("catbsquotaf"), 3, 2, 0);
            if (rec.leggiDouble("catbsquotaf") == 0 && rec.leggiIntero("catbscriterio1") != 5) {
                attivaManutenzionePerf = true;
            }

            double massimoval = 0;
            if (rec.leggiDouble("catbsmaxcosto") > 0) {
                massimoval = rec.leggiDouble("catbsmaxcosto");
            }

            if (rec.leggiDouble("catbsmaxnolo") > 0) {
                massimoval = rec.leggiDouble("catbsmaxnolo");
            }

            data[i][7]=rec.leggiDouble("catbsfabbrnoamm");            
            data[i][8] = Formattazione.formValuta(massimoval, 8, 2, 0);
            data[i][9] = Formattazione.formValuta(rec.leggiDouble("catbsquota1"), 3, 2, 0);
            if (rec.leggiIntero("catbscriterio1") != 1 && rec.leggiIntero("catbscriterio1") != 2 && rec.leggiIntero("catbscriterio1") != 5
                    && rec.leggiDouble("catbsquota1") == 0) {
                attivaManutenzionePer1 = true;
            }

            data[i][10] = Formattazione.formValuta(rec.leggiDouble("catbsquotaage"), 3, 2, 0);
            data[i][11] = Formattazione.formValuta(rec.leggiDouble("catbsdetrcosto"), 3, 2, 0);
            data[i][12] = Formattazione.formValuta(rec.leggiDouble("catbsdetriva"), 3, 2, 0);

            flagerrorepdc[i] = false;
            ArrayList<Record> vrisposta = BusinessLogicCespiti.verificaLegamePianoConti(connAz.conn, rec.leggiIntero("catbsimmpdcid"), rec.leggiIntero("catbsfondopdcid"), rec.leggiIntero("catbscostopdcid"));
            if (!vrisposta.isEmpty()) {
                Record rb = vrisposta.get(0);
                if (rb != null) {
                    if (!rb.leggiStringa("errore").equals("") && !flagnocesp[i]) {
                        messaggioErrore = true;
                        flagerrorepdc[i] = true;
                    }
                }
                rb = vrisposta.get(1);
                if (rb != null) {
                    if (!rb.leggiStringa("errore").equals("") && !flagnocesp[i]) {
                        messaggioErrore = true;
                        flagerrorepdc[i] = true;
                    }
                }
                rb = vrisposta.get(2);
                if (rb != null) {
                    if (!rb.leggiStringa("errore").equals("") && !flagnocesp[i]) {
                        messaggioErrore = true;
                        flagerrorepdc[i] = true;
                    }
                }
            } else {
                flagerrorepdc[i] = true;
            }

            flaggestioni[i] = false;

            if (rec.leggiStringa("catbsserb").equals("S")
                    || rec.leggiStringa("catbsconv").equals("S")
                    || rec.leggiStringa("catbsmac").equals("S")
                    || rec.leggiStringa("catbsauto").equals("S")
                    || rec.leggiStringa("catbsubic").equals("S")
                    || rec.leggiStringa("catbscata").equals("S")) {
                flaggestioni[i] = true;
            }
            id[i] = rec.leggiIntero("catbsid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_PDC)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_CESP)
//                        || col == FunzioniUtilita.getColumnIndex(tab, COL_GEST)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_STATO)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tab, true);

        TableColumn buttonColumn0 = tab.getColumn(COL_CESP);
        TablePopButton button0 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_list_white_20dp.png")));
                    if (!flagnocesp[row]) 
                    {
                        button.setEnabled(true);
                        button.setToolTipText("Presenti immobilizzazioni collegate");
                    }
                    else {
                        button.setEnabled(false);
                        button.setToolTipText("Non sono presenti immobilizzazioni");
                    }
            }
        });
        button0.addHandler(new TablePopButton.TableButtonPressedHandler() {

            @Override
            public void onButtonPress(int row, int column) {
//                if (listener != null && !flagnocesp[row]) {
                if (!flagnocesp[row]) {
                    waitloc.lancia();
                    PannelloGestioneCespiti p = new PannelloGestioneCespiti(formGen);
//                    p.inizializza(connAz, connCom, azienda, esContabile, id[row], null, "Ricerca Cespiti");
                    listener.notificaAperturaPannello(null, "Categorie", p, "Ricerca Cespiti", "Immobilizzazioni", new Color(245, 145, 0),id[row]);                    
                    waitloc.ferma();
                }
            }
        });
        buttonColumn0.setCellRenderer(button0);
        buttonColumn0.setCellEditor(button0);

//        TableColumn buttonColumn1 = tab.getColumn(COL_GEST);
//        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
//            public void customize(PopButton button, int row, int column) {
//                if (flaggestioni[row]) {
//                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_remove_red_eye_white_24dp.png")));
//                    button.setToolTipText("Presente collegamento con altre procedure");
//                } else {
//                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_phonelink_off_white_24dp.png")));
//                    button.setToolTipText("Assente collegamento con altre procedure");
//                }
//            }
//        });
//        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
//            @Override
//            public void onButtonPress(int row, int column) {
//                DialogProcedureCollegateCespite d = new DialogProcedureCollegateCespite(formGen, true, connAz, azienda, id[row]);
//                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
//                d.show();
//                interrogazione();
//            }
//        });
//        buttonColumn1.setCellRenderer(button1);
//        buttonColumn1.setCellEditor(button1);

        TableColumn buttonColumn2 = tab.getColumn(COL_PDC);
        TablePopButton button2 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (flagerrorepdc[row]) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ICONA_VAR_24_ROSSA.png")));
                    button.setToolTipText("ASSENTE Legame con piano dei conti");
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ICONA_VAR_24_BIANCA.png")));
                    button.setToolTipText("Legame con piano dei conti");
                }
            }
        });
        button2.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                DialogPdcCollegatoCespiti d = new DialogPdcCollegatoCespiti(formGen, true, connAz, connCom, azienda, id[row]);
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();
                interrogazione();
            }
        });
        buttonColumn2.setCellRenderer(button2);
        buttonColumn2.setCellEditor(button2);

        TableColumn buttonColumn3 = tab.getColumn(COL_STATO);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                button.setToolTipText("Elimina / Disattiva categoria");
            }
        });

        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                String qry2 = "SELECT cespcategid FROM cespiti WHERE cespcategid = " + id[row];
                QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry2);
                q.apertura();
                Record r = q.successivo();
                q.chiusura();

                if (r == null) 
                {
                    String qry3 = "SELECT * FROM categoriebs INNER JOIN gruppibs ON (catbsgruppo=gruppobsmastro AND\n" +
                    " catbsspecie=gruppobsconto AND catbssotto=gruppobssotto AND catbscod=CONCAT(gruppobsconto,gruppobssotto,\"00\")) WHERE catbsid =" + id[row];
                    QueryInterrogazioneClient q3 = new QueryInterrogazioneClient(connAz, qry3);
                    q3.apertura();
                    Record r3 = q3.successivo();
                    q3.chiusura();
                    
                    if(r3==null)
                    {
                        boolean ok = chiediConfermaElim(id[row]);
                        if (ok) {
                            interrogazione();
                        }
                    }
                    else
                    {
                        
                        JOptionPane.showMessageDialog(formGen, "E' una categoria ministeriale non si può eliminare", "Invio",
                            JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                        
                    }
                } else {
                    boolean ok = chiediConfermaDisattiva(id[row]);
                    if (ok) {
                        JOptionPane.showMessageDialog(formGen, "Categoria disattivata ", "Invio",
                                JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                    }
                    interrogazione();
                    buttonUtility.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_link_white_24dp.png")));
                }
            }
        });
        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        tab.getColumn(COL_TIPO).setPreferredWidth(100);
        tab.getColumn(COL_CATPADRE).setPreferredWidth(100);
        tab.getColumn(COL_NOME).setPreferredWidth(340);
        tab.getColumn(COL_METODO).setPreferredWidth(120);
        tab.getColumn(COL_FABBR).setPreferredWidth(90);
        tab.getColumn(COL_QC).setPreferredWidth(64);
        tab.getColumn(COL_QF).setPreferredWidth(74);
        tab.getColumn(COL_CRITERIO).setPreferredWidth(168);
        tab.getColumn(COL_MAX).setPreferredWidth(90);
        tab.getColumn(COL_Q1).setPreferredWidth(80);
        tab.getColumn(COL_AGEV).setPreferredWidth(56);
        tab.getColumn(COL_DETRC).setPreferredWidth(100);
        tab.getColumn(COL_DETRI).setPreferredWidth(90);
        tab.getColumn(COL_CESP).setPreferredWidth(30);
        tab.getColumn(COL_PDC).setPreferredWidth(30);
        tab.getColumn(COL_STATO).setPreferredWidth(30);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_MAX).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tab.getColumn(COL_FABBR).setCellRenderer(centerRenderer);
        tab.getColumn(COL_QC).setCellRenderer(centerRenderer);
        tab.getColumn(COL_Q1).setCellRenderer(centerRenderer);
        tab.getColumn(COL_QF).setCellRenderer(centerRenderer);
        tab.getColumn(COL_AGEV).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DETRC).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DETRI).setCellRenderer(centerRenderer);

        tab.getColumn(COL_CESP).setHeaderRenderer(headerRenderer);
        tab.getColumn(COL_PDC).setHeaderRenderer(headerRenderer);
//        tab.getColumn(COL_GEST).setHeaderRenderer(headerRenderer);
        tab.getColumn(COL_STATO).setHeaderRenderer(headerRenderer);
        if ((attivaManutenzionePerc || attivaManutenzionePerf || attivaManutenzionePer1) && primavolta) {
            if (attivaManutenzionePerc) {
                DialogUtilitaManutenzioneCategoria d = new DialogUtilitaManutenzioneCategoria(formGen, true, connAz, 1);
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();
            }

            if (attivaManutenzionePerf) {
                DialogUtilitaManutenzioneCategoria d = new DialogUtilitaManutenzioneCategoria(formGen, true, connAz, 2);
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();
            }

            if (attivaManutenzionePer1) {
                DialogUtilitaManutenzioneCategoria d = new DialogUtilitaManutenzioneCategoria(formGen, true, connAz, 3);
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();
            }
            primavolta = false;
        }

        if (erroregrave) {
            errorMessage.setText("Ci sono categorie NON previste su gruppi e specie");
            JOptionPane.showMessageDialog(formGen, "ERRORE grave, ci sono categorie NON previste su gruppi e specie", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        } else if (messaggioErrore) {
            errorMessage.setText("Categorie non legate al piano dei conti");
            JOptionPane.showMessageDialog(formGen, "ERRORE, Ci sono categorie NON legate al piano dei conti", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));            
            requestFocus();

        } else {
            errorMessage.setText("");
        }
    }

    final boolean chiediConfermaElim(int idelim) {
        boolean eli = false;
        int ris = JOptionPane.showConfirmDialog(this, "SI: confermi l'eliminazione definitiva della categoria\n"
                + "NO: disattivi la categoria",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            String qrydelete = "DELETE FROM categoriebs  WHERE catbsid =" + idelim;
            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
            qd.esecuzione();

            eli = true;
            JOptionPane.showMessageDialog(this, "Categoria ELIMINATA ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        } else {
            String qryupdate = "UPDATE categoriebs SET catbsstato = 0 WHERE catbsid =" + idelim;
            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
            qd.esecuzione();
        }
        return eli;
    }

    final boolean chiediConfermaDisattiva(int idelim) {
        boolean eli = false;
        int ris = JOptionPane.showConfirmDialog(this, "SI: confermi la disattivazione della categoria",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            String qryupdate = "UPDATE categoriebs SET catbsstato = 0 WHERE catbsid =" + idelim;
            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
            qd.esecuzione();
        }
        return eli;
    }

    @Override
    public void notificaChiusura(PannelloPop origine, String key) {

    }

    @Override
    public void notificaCambioTitolo(PannelloPop origine, String key, String nuovoTitolo) {

    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo) {

    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo, Color coloresfondo, int idVis) {
        
    }

    private void caricaDisattivate() {
        vdisattiveid = new ArrayList();
        String qry = "SELECT * FROM categoriebs WHERE catbsstato = 0"
                + " ORDER BY catbsgruppo,catbsspecie,catbssotto,catbstipo,catbsdescr";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vdisattiveid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();

        mostraTabellaDisattivate();
    }

    private void mostraTabellaDisattivate() {
        final String[] names
                = {
                    COL_TIPO, COL_NOME, COL_ECC, COL_METODO, COL_QC, COL_CRITERIO, COL_QF, COL_MAX, COL_Q1, COL_AGEV, COL_DETRC, COL_DETRI, COL_STATO
                };
        final Object[][] data = new Object[vdisattiveid.size()][names.length];

        id = new int[vdisattiveid.size()];

        for (int i = 0; i < vdisattiveid.size(); i++) {
            Record rec = vdisattiveid.get(i);

            String qry = "SELECT gruppobscateg,gruppobsbil,gruppobsmastro,gruppobsconto FROM gruppibs WHERE gruppobsmastro = \"" + rec.leggiStringa("catbsgruppo")
                    + "\" AND gruppobsconto = \"" + rec.leggiStringa("catbsspecie") + "\" AND gruppobssotto = \"" + rec.leggiStringa("catbssotto") + "\"";
            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
            q.apertura();
            Record rd = q.successivo();
            q.chiusura();

            if (rd != null) {
                String qry2 = "SELECT cespcategid FROM cespiti WHERE cespcategid = " + rec.leggiIntero("catbsid") + " LIMIT 01";
                QueryInterrogazioneClient q2 = new QueryInterrogazioneClient(connAz, qry2);
                q2.apertura();
                Record r2 = q2.successivo();
                q2.chiusura();
                if (r2 != null) {
                    data[i][0] = "ERRORE Cespiti associati";
                    buttonUtility.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_link_white_24dp.png")));
                    errorMessage.setText("ATTENZIONE errore,  ci sono cespiti legati alla categoria disattivata");
                } else {
                    String codice = rec.leggiStringa("catbscod");
                    data[i][0] = rec.leggiStringa("catbssotto") + " - " + codice;
                }
            } else {
                data[i][0] = "ERRORE tab.minist.";
            }
            data[i][1] = rec.leggiStringa("catbsdescr");
            data[i][2] = rec.leggiStringa("catbsecc");
            //metodo
            switch (rec.leggiIntero("catbsflagtipoammc")) {
                case 0:
                    data[i][3] = ".....";
                    break;
                case 1:
                    data[i][3] = "In base all'utilizzo";
                    break;
                default:
                    data[i][3] = "Irregolare o ragionato";
                    break;
            }

            data[i][4] = Formattazione.formValuta(rec.leggiDouble("catbsquotac"), 3, 2, 0);

            switch (rec.leggiIntero("catbscriterio1")) {
                case 0:
                    data[i][5] = "Quota ordinaria";
                    break;
                case 1:
                    data[i][5] = "Quote in mesi";
                    break;
                case 2:
                    data[i][5] = "Quote in giorni";
                    break;
                case 3:
                    data[i][5] = "Accellerato (2 x Aliq.)";
                    break;
                case 4:
                    data[i][5] = "Ritardato (50% Aliq)";
                    break;
                case 5:
                    data[i][5] = "Nessun ammortamento";
                    break;
                case 6:
                    data[i][5] = "Totale (100%)";
                    break;
                default:
                    data[i][5] = "%  libera";
                    break;
            }
            data[i][6] = Formattazione.formValuta(rec.leggiDouble("catbsquotaf"), 3, 2, 0);

            double massimoval = 0;
            if (rec.leggiDouble("catbsmaxcosto") > 0) {
                massimoval = rec.leggiDouble("catbsmaxcosto");
            }

            if (rec.leggiDouble("catbsmaxnolo") > 0) {
                massimoval = rec.leggiDouble("catbsmaxnolo");
            }
            data[i][7] = Formattazione.formValuta(massimoval, 8, 2, 0);
            data[i][8] = Formattazione.formValuta(rec.leggiDouble("catbsquota1"), 3, 2, 0);
            data[i][9] = Formattazione.formValuta(rec.leggiDouble("catbsquotaage"), 3, 2, 0);
            data[i][10] = Formattazione.formValuta(rec.leggiDouble("catbsdetrcosto"), 3, 2, 0);
            data[i][11] = Formattazione.formValuta(rec.leggiDouble("catbsdetriva"), 3, 2, 0);

            id[i] = rec.leggiIntero("catbsid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_STATO)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tab, true);

        TableColumn buttonColumn3 = tab.getColumn(COL_STATO);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                button.setToolTipText("RIPRISTINA categoria");
            }
        });
        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                String qryupdate = "UPDATE categoriebs SET catbsstato = 1 WHERE catbsid =" + id[row];
                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryupdate);
                qd.esecuzione();
                JOptionPane.showMessageDialog(formGen, "Categoria ripristinata ", "Invio",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                caricaDisattivate();
            }
        });

        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        tab.getColumn(COL_TIPO).setPreferredWidth(70);
        tab.getColumn(COL_NOME).setPreferredWidth(250);
        tab.getColumn(COL_METODO).setPreferredWidth(120);
        tab.getColumn(COL_ECC).setPreferredWidth(28);
        tab.getColumn(COL_QC).setPreferredWidth(46);
        tab.getColumn(COL_Q1).setPreferredWidth(46);
        tab.getColumn(COL_CRITERIO).setPreferredWidth(168);
        tab.getColumn(COL_QF).setPreferredWidth(46);
        tab.getColumn(COL_MAX).setPreferredWidth(70);
        tab.getColumn(COL_AGEV).setPreferredWidth(80);
        tab.getColumn(COL_DETRI).setPreferredWidth(60);
        tab.getColumn(COL_DETRC).setPreferredWidth(46);
        tab.getColumn(COL_STATO).setPreferredWidth(30);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_MAX).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tab.getColumn(COL_QC).setCellRenderer(centerRenderer);
        tab.getColumn(COL_Q1).setCellRenderer(centerRenderer);
        tab.getColumn(COL_QF).setCellRenderer(centerRenderer);

        tab.getColumn(COL_STATO).setHeaderRenderer(headerRenderer);

        buttonMostraDisattive.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_lock_open_white_24dp.png")));
        buttonMostraDisattive.setToolTipText("Mostra categorie ATTIVE");
    }
}
