package cespitipop;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import nucleo.ConnessioneServer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;
import nucleo.EnvJazz;
import nucleo.ExecGestioni;
import nucleo.Formattazione;
import nucleo.FunzFatt;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.Query;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogic;
import nucleo.businesslogic.BusinessLogicAmbiente;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloGestioneIncrementi extends javax.swing.JPanel implements PannelloPop {

    private String COL_DESCR = "Descrizione";
    private String COL_ANNO = "Anno inizio";
    private String COL_DOC = "Protocollo";
    private String COL_DTACQ = "Del";
    private String COL_FORNIT = "Fornitore";
    private String COL_RIF = "Riferimento";
    private String COL_VALORE = "Valore";
    private String COL_FISCALE = "Val. fiscale";
    private String COL_NCF = "Nc";
    private String COL_MOD = "M.";
    private String COL_COLLPN = "PN";
    private String COL_PRIMANOTA = "Apri PN";
    private String COL_ELIMINA = "Elimina";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;
    private Window padre;
    private Frame formGen;
    private String azienda = "";
    private String esContabile = "";
    private int[] id;
    private int[] idC;
    private int[] vcatbsid;
    private boolean[] flagmodificabile;
    private String esercSelez = "";
    private String modalita = "";

    private String esercizio = "";
    private int idCesp;
    private int idCat;
    private String[] vcau;
    private boolean cambia = false;
    ArrayList<Record> vcespiti = new ArrayList();
    
    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloGestioneIncrementi() {
        this.initComponents();
    }

    public PannelloGestioneIncrementi(Window padrea, Frame formGena) {
        this();
        this.padre = padrea;
        this.formGen = formGena;
    }

    public Window getPadre() {
        return padre;
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        tiporicerca = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        annoacq = new ui.beans.CampoTestoPop();
        jLabel12 = new javax.swing.JLabel();
        comboCategorieBs = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        pPulsanti = new javax.swing.JPanel();
        buttonRettifiche = new ui.beans.PopButton();
        buttonStampa = new ui.beans.PopButton();
        buttonNuovoCespite = new ui.beans.PopButton();
        jLabelEs = new javax.swing.JLabel();
        comboEsercizi = new archivibasepop.beans.BeanGestEserciziPop();
        buttonProduzione = new ui.beans.PopButton();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 48));
        pFiltri.setLayout(null);

        tiporicerca.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        tiporicerca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "...............", "Nuovi acquisti", "Incrementi" }));
        tiporicerca.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                tiporicercaItemStateChanged(evt);
            }
        });
        tiporicerca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tiporicercaActionPerformed(evt);
            }
        });
        pFiltri.add(tiporicerca);
        tiporicerca.setBounds(812, 12, 160, 24);

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Anno acq.");
        pFiltri.add(jLabel9);
        jLabel9.setBounds(540, 12, 76, 24);

        jLabel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Categoria");
        pFiltri.add(jLabel11);
        jLabel11.setBounds(8, 12, 68, 24);

        annoacq.setCaratteri(4);
        pFiltri.add(annoacq);
        annoacq.setBounds(624, 12, 64, 24);

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Tipo");
        pFiltri.add(jLabel12);
        jLabel12.setBounds(692, 12, 108, 24);

        comboCategorieBs.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        comboCategorieBs.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboCategorieBsItemStateChanged(evt);
            }
        });
        comboCategorieBs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategorieBsActionPerformed(evt);
            }
        });
        pFiltri.add(comboCategorieBs);
        comboCategorieBs.setBounds(84, 12, 440, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pPulsanti.setBackground(new java.awt.Color(255, 255, 255));
        pPulsanti.setPreferredSize(new java.awt.Dimension(800, 40));
        pPulsanti.setLayout(null);

        buttonRettifiche.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_shuffle_white_24dp.png"))); // NOI18N
        buttonRettifiche.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonRettifiche.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonRettifiche.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonRettifiche.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonRettifiche.setToolTipText("Registra svalutazioni e rivalutazioni. Rettifiche di raccordo procedura cespiti e contabilità. ");
        buttonRettifiche.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRettificheActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonRettifiche);
        buttonRettifiche.setBounds(136, 4, 44, 32);

        buttonStampa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_print_white_24dp.png"))); // NOI18N
        buttonStampa.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonStampa.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonStampa.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonStampa.setToolTipText("Riepilogo ammortamenti per specie");
        buttonStampa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonStampaActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonStampa);
        buttonStampa.setBounds(196, 4, 44, 32);

        buttonNuovoCespite.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovoCespite.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovoCespite.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovoCespite.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovoCespite.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovoCespite.setToolTipText("Registra nuovo cespite, accessorio di cespite esistente o  incremento di cespite esistente");
        buttonNuovoCespite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovoCespiteActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonNuovoCespite);
        buttonNuovoCespite.setBounds(20, 4, 48, 32);

        jLabelEs.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabelEs.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelEs.setText("Esercizio contab.");
        pPulsanti.add(jLabelEs);
        jLabelEs.setBounds(264, 8, 108, 24);
        pPulsanti.add(comboEsercizi);
        comboEsercizi.setBounds(388, 8, 144, 24);

        buttonProduzione.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_location_white_24dp.png"))); // NOI18N
        buttonProduzione.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonProduzione.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonProduzione.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonProduzione.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonProduzione.setToolTipText("Registra incremento cespite ottenuto da lavorazioni e beni interni");
        buttonProduzione.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonProduzioneActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonProduzione);
        buttonProduzione.setBounds(80, 4, 44, 32);

        add(pPulsanti, java.awt.BorderLayout.PAGE_END);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(8, 100));
        add(jPanel1, java.awt.BorderLayout.LINE_END);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(8, 100));
        add(jPanel2, java.awt.BorderLayout.LINE_START);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonRettificheActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRettificheActionPerformed
        if (esercSelez.compareTo(esContabile) == 0) {
            int ris = BusinessLogicAmbiente.mostraConfirmDialog(this, "Conferma", "Vuoi registrare Svalutazioni e rivalutazioni su esercizio: " + esContabile + " ?", JOptionPane.YES_NO_OPTION);
            if (ris == JOptionPane.YES_OPTION) {
                int finoanno = Formattazione.estraiIntero(esContabile.substring(4, 8));

                DialogVariazioniRettificheAltre d = new DialogVariazioniRettificheAltre(formGen, true, connAz, connCom, azienda, esContabile, 1, finoanno);
                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                d.show();
                this.requestFocus();
            }
        } else {
            int ris = BusinessLogicAmbiente.mostraConfirmDialog(formGen, "Conferma", "Vuoi effettuare rettifiche su esercizio: " + esercSelez + " ?", JOptionPane.YES_NO_OPTION);
            if (ris == JOptionPane.YES_OPTION) {

                int finoanno = Formattazione.estraiIntero(esercSelez.substring(4, 8));

                DialogVariazioniRettificheAltre d = new DialogVariazioniRettificheAltre(formGen, true, connAz, connCom, azienda, esercSelez, 0, finoanno);
                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                d.show();
                this.requestFocus();
            }
        }
    }//GEN-LAST:event_buttonRettificheActionPerformed

    private void tiporicercaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_tiporicercaItemStateChanged

    }//GEN-LAST:event_tiporicercaItemStateChanged

    private void buttonStampaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonStampaActionPerformed
        //boolean opz_soloacq = true;
        //codrepSelez.substring(0, 4).equals("cspf")
        //stampaLibro();
    }//GEN-LAST:event_buttonStampaActionPerformed

    private void buttonNuovoCespiteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovoCespiteActionPerformed
        Record rFattura = new Record();
        Record rFattura1 = new Record();
        Record rFattura2 = new Record();

        int catbsid = -1;
        if (comboCategorieBs.getSelectedIndex() > 0) {
            catbsid = vcatbsid[comboCategorieBs.getSelectedIndex() - 1];
        }
        int ris = BusinessLogicAmbiente.mostraConfirmDialog(this, "Conferma", "Vuoi registrare su esercizio: " + esercSelez + " ?", JOptionPane.YES_NO_OPTION);
        if (ris == JOptionPane.YES_OPTION) {
            String qry = "SELECT * FROM caubs WHERE bscaumov = 0 AND bscaucod =\"fta\" AND bscaustato = 1";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            qRic.chiusura();
            if (r != null) {
                if (r.leggiStringa("bsdocjazzcod2").equals("")) {
                    if (r.leggiStringa("bsdocjazzcod1").equals("")) {
                        if (!r.leggiStringa("bsdocjazzcod").equals("")) {
                            String[] campi = {"docid", "doccod", "docdescr", "doccauconid"};
                            rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                            if (rFattura == null) {
                                JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                return;
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Documenti acquisto NON parametrizzati.\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    } else {
                        String[] campi = {"docid", "doccod", "docdescr", "doccauconid"};
                        rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                        if (rFattura != null) {
                            rFattura1 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod1"), campi);
                            if (rFattura1 == null) {
                                JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod1") + "\nImpossibile proseguire!", "",
                                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                return;
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    }
                } else {
                    String[] campi = {"docid", "doccod", "docdescr", "doccauconid"};
                    rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                    if (rFattura != null) {
                        rFattura1 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod1"), campi);
                        if (rFattura1 != null) {
                            rFattura2 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod2"), campi);
                            if (rFattura2 == null) {
                                JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod2") + "\nImpossibile proseguire!", "",
                                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                return;
                            }
                        } else {
                            JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod1") + "\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    }
                }
                DialogGestAcqCespiteManuale d = new DialogGestAcqCespiteManuale(formGen, true, connAz, connCom, azienda, esContabile, -1, "Nuovo", catbsid);
                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                d.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(this, "Assente parametrizzazione causali acquisti.\nImpossibile proseguire!", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            }
        }
    }//GEN-LAST:event_buttonNuovoCespiteActionPerformed

    private void tiporicercaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tiporicercaActionPerformed
        if (cambia) {
            if (tiporicerca.getSelectedIndex() == 0) {
                modalita = "";
            } else if (tiporicerca.getSelectedIndex() == 1) {
                modalita = "NUOVI";
            } else if (tiporicerca.getSelectedIndex() == 2) {
                modalita = "INCREMENTO";
            }

            if (comboCategorieBs.getSelectedIndex() > 0) {
                interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
            } else {
                interrogazione(-1, -1);
            }
        }
    }//GEN-LAST:event_tiporicercaActionPerformed

    private void comboCategorieBsItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboCategorieBsItemStateChanged
        
    }//GEN-LAST:event_comboCategorieBsItemStateChanged

    private void buttonProduzioneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonProduzioneActionPerformed

        if (esercSelez.compareTo(esContabile) == 0) {
            int ris = BusinessLogicAmbiente.mostraConfirmDialog(this, "Conferma", "Vuoi registrare immobilizzazioni prodotte all'interno dell'azienda su esercizio: " + esContabile + " ?", JOptionPane.YES_NO_OPTION);
            if (ris == JOptionPane.YES_OPTION) {
                int catbsid=-1;
                if(comboCategorieBs.getSelectedIndex()!=0)
                {
                    catbsid=vcatbsid[comboCategorieBs.getSelectedIndex()];
                }
                DialogGestProdCespiteManuale d = new DialogGestProdCespiteManuale(formGen, true, connAz, connCom, azienda, esContabile, catbsid,1);
                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                d.show();
                this.requestFocus();
            }
        } else {
            int ris = BusinessLogicAmbiente.mostraConfirmDialog(this, "Conferma", "Vuoi registrare immobilizzazioni prodotte all'interno dell'azienda su esercizio: " + esercSelez + " ?", JOptionPane.YES_NO_OPTION);
            if (ris == JOptionPane.YES_OPTION) {
                int catbsid=-1;
                if(comboCategorieBs.getSelectedIndex()==0)
                {
                    catbsid=vcatbsid[comboCategorieBs.getSelectedIndex()];
                }
                DialogGestProdCespiteManuale d = new DialogGestProdCespiteManuale(formGen, true, connAz, connCom, azienda, esercSelez,catbsid,1);
                UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                d.show();
                this.requestFocus();
            }
        }
    }//GEN-LAST:event_buttonProduzioneActionPerformed

    private void comboCategorieBsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategorieBsActionPerformed
       if (cambia) {
            if (comboCategorieBs.getSelectedIndex() > 0) {
                interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
            } else {
                interrogazione(-1, -1);
            }
        }
    }//GEN-LAST:event_comboCategorieBsActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.CampoTestoPop annoacq;
    private ui.beans.PopButton buttonNuovoCespite;
    private ui.beans.PopButton buttonProduzione;
    private ui.beans.PopButton buttonRettifiche;
    private ui.beans.PopButton buttonStampa;
    private javax.swing.JComboBox<String> comboCategorieBs;
    private archivibasepop.beans.BeanGestEserciziPop comboEsercizi;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelEs;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pFiltri;
    private javax.swing.JPanel pPulsanti;
    private javax.swing.JTable tab;
    private javax.swing.JComboBox<String> tiporicerca;
    // End of variables declaration//GEN-END:variables

    public void impostaParametri(int idCat, int idCesp, String esercizio, String modalita) {
        this.idCat = idCat;
        this.idCesp = idCesp;
        this.esercizio = esercizio;
        this.modalita = modalita;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;

        cambia = false;

        buttonNuovoCespite.setEnabled(false);
        buttonRettifiche.setEnabled(false);
        buttonStampa.setVisible(false);

        //carica causali acquisti e storni
        String qry = "SELECT bscauid FROM caubs WHERE"
                + " (bscaucod = \"fta\""
                + " OR bscaucod = \"ftault\""
                + " OR bscaucod = \"bsriv\""
                + " OR bscaucod = \"acqx\""
                + " OR bscaucod = \"brvar\""
                + ") AND bscaumov = 0 ";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            vcau = new String[q.numeroRecord()];
            for (int i = 0; i < q.numeroRecord(); i++) {
                Record r = q.successivo();
                vcau[i] = "" + r.leggiIntero("bscauid");
            }
        }
        q.chiusura();

        annoacq.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (comboCategorieBs.getSelectedIndex() > 0) {
                    interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
                } else {
                    interrogazione(-1, -1);
                }
            }
        });

        comboEsercizi.passaParametri(azienda, formGen, formGen);
        CambioEsercizio ce = new CambioEsercizio();
        comboEsercizi.gestioneAggiuntiva(ce);
        comboEsercizi.impostaEsercizio(esContabile);

        popolaComboCategorieBs();

        tiporicerca.setSelectedItem("<...............>");

        cambia = true;

        if (idCat > 0 || idCesp > 0 || !esercizio.equals("")) {
            pFiltri.setVisible(false);
            pPulsanti.setVisible(false);
            esercSelez = esercizio;
            interrogazione(idCat, idCesp);
        }

    }

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void interrogazione(int catbsid, int cespbsid) {

        if (cambia) {
            vcespiti = new ArrayList();

            double totaleanno = 0;
            double totalegenerale = 0;
            double totaleannof = 0;
            double totalegeneralef = 0;

            String cespeserc = "";//appoggio per rottura esercizi

            String qry = "SELECT cespid,cespcategid,cespordine,cespdescriz1,cespdatafunz,cespdataalien,cespcapoid,cespitiacq.* FROM cespiti"
                    + " INNER JOIN cespitiacq ON cespiti.cespid = cespitiacq.cespacespid"
                    + " INNER JOIN categoriebs ON cespcategid = catbsid"
                    + " WHERE cespid > 0 ";
            if (catbsid > 0) {
                qry += " AND cespcategid = " + catbsid;
            }
            if (cespbsid > 0) {
                qry += " AND cespid = " + cespbsid;
            }
            if (!annoacq.getText().trim().equals("")) {
                qry += " AND (cespeserca = \"" + annoacq.getText().trim() + annoacq.getText().trim() + "\""
                        + " OR cespeserca = \"" + (Formattazione.estraiIntero(annoacq.getText().trim()) - 1) + annoacq.getText().trim() + "\")";
            }

            qry += " ORDER BY cespeserca DESC,cespdatainizio DESC,catbsgruppo,catbsspecie,catbssotto,catbstipo,catbscod,cespordine";
            //        System.out.println(qry);
            Query qRic = new Query(connAz, qry);
            qRic.apertura();
            int num = qRic.numeroRecord();
            if (num > 0) {
                for (int i = 0; i < num; i++) {
                    Record r = qRic.successivo();
                    if (i == 0) {
                        cespeserc = r.leggiStringa("cespeserca");
                    }
                    if (!r.leggiStringa("cespeserca").equals(cespeserc)) {
                        Record rins = new Record();
                        rins.insElem("cespid", -1);
                        rins.insElem("cespcategid", -1);
                        rins.insElem("cespordine", "");
                        rins.insElem("cespdescriz1", "");
                        rins.insElem("cespdatafunz", "");
                        rins.insElem("cespdataalien", "");
                        rins.insElem("cespaid", -1);
                        rins.insElem("cespacespid", -1);
                        rins.insElem("cespeserca", "");                        
                        rins.insElem("cespseza", "");
                        rins.insElem("cespdataacq", "");
                        rins.insElem("cespnumacq", "");
                        rins.insElem("cespnriga", 0);
                        rins.insElem("cespdataesterna", "");
                        rins.insElem("cespnumesterno", "");
                        rins.insElem("cespfornit", "TOTALE anno " + cespeserc.substring(0, 4));
                        cespeserc = r.leggiStringa("cespeserca");
                        rins.insElem("cespfornpiva", "");
                        rins.insElem("cespnotea", "");
                        rins.insElem("cespimponibile", 0.00);
                        rins.insElem("cespivanodetr", 0.00);
                        rins.insElem("cespvalorenoamm", 0.00);
                        rins.insElem("cespvaloreacq", 0.00);
                        rins.insElem("cespvaloreacqc", totaleanno);
                        rins.insElem("cespusato", 0);
                        rins.insElem("cespvalriscatto", 0.00);
                        rins.insElem("cespannileasing", 0);
                        rins.insElem("cespqta", 0);
                        rins.insElem("cesppercdeduc1", 100.00);
                        rins.insElem("cespmassimale", 0.00);
                        rins.insElem("cespquotadeduc", totaleannof);
                        rins.insElem("cespimpnetto", 0.00);
                        rins.insElem("cespmovajazzid", -1);

                        totaleanno = 0;
                        totaleannof = 0;
                        vcespiti.add(rins);
                    }
                    
                    String qrymin="SELECT MIN(cespaid) AS min FROM cespitiacq WHERE cespacespid="+r.leggiIntero("cespid");
                    QueryInterrogazioneClient querymin=new QueryInterrogazioneClient(connAz, qrymin);
                    querymin.apertura();
                    Record recmin=querymin.successivo();
                    querymin.chiusura();
                    
                    if (modalita.equals("INCREMENTO")) 
                    {                        
                        if(recmin!=null)
                        {
                            if(recmin.leggiIntero("min")!=r.leggiIntero("cespaid"))
                            {
                                vcespiti.add(r);
                                totaleanno += r.leggiDouble("cespvaloreacqc");
                                totalegenerale += r.leggiDouble("cespvaloreacqc");
                                if (r.leggiDouble("cespquotadeduc") == 0) {
                                    totaleannof += r.leggiDouble("cespvaloreacqc");
                                    totalegeneralef += r.leggiDouble("cespvaloreacqc");
                                } else {
                                    totaleannof += r.leggiDouble("cespquotadeduc");
                                    totalegeneralef += r.leggiDouble("cespquotadeduc");
                                }                              
                            }
                        } 
                    } else if (modalita.equals("NUOVI")) {
                        if(recmin!=null)
                        {
                            if(recmin.leggiIntero("min")==r.leggiIntero("cespaid"))
                            {
                                vcespiti.add(r);
                                totaleanno += r.leggiDouble("cespvaloreacqc");
                                totalegenerale += r.leggiDouble("cespvaloreacqc");
                                if (r.leggiDouble("cespquotadeduc") == 0) {
                                    totaleannof += r.leggiDouble("cespvaloreacqc");
                                    totalegeneralef += r.leggiDouble("cespvaloreacqc");
                                } else {
                                    totaleannof += r.leggiDouble("cespquotadeduc");
                                    totalegeneralef += r.leggiDouble("cespquotadeduc");
                                }                              
                            }
                        } 
                    } else {
                        vcespiti.add(r);

                        String qryx = vcau[0];
                        for (int k = 1; k < vcau.length; k++) {
                            qryx += "," + vcau[k];
                        }
                        
                        String qryincr = "SELECT mbseserc,mbsdata,mbsmovjazzid,mbsnote,mbscespid,mbsid,mbsimpbene,mbstipocf,mbscaumovid,bscaudescr FROM movimentibs"
                                + " LEFT JOIN caubs ON mbscaumovid = bscauid"
                                + " WHERE mbscespid=" + r.leggiIntero("cespid") + " AND "
                                + "((mbscaumovid IN(" + qryx + ")) AND mbstipocf = 2) AND mbseserc=\""+annoacq.getText().trim()+annoacq.getText().trim()+"\"";
                        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qryincr);
                        query.apertura();
                        Record rincre = query.successivo();
                        while (rincre != null) {
                            Record rec = new Record();
                            rec.insElem("cespid", r.leggiIntero("cespid"));
                            rec.insElem("cespcategid", r.leggiIntero("cespcategid"));
                            rec.insElem("cespordine", r.leggiStringa("cespordine"));
                            rec.insElem("cespdescriz1", r.leggiStringa("cespdescriz1"));
                            rec.insElem("cespdatafunz", r.leggiStringa("cespdatafunz"));
                            rec.insElem("cespdataalien", r.leggiStringa("cespdataalien"));
                            rec.insElem("cespaid", 0);//proviene da movimentibs anziche' cespitiacq
                            rec.insElem("cespacespid", r.leggiIntero("cespid"));
                            rec.insElem("cespeserca", rincre.leggiStringa("mbseserc"));
                            rec.insElem("cespdataacq", rincre.leggiStringa("mbsdata"));
                            rec.insElem("cespseza", "");
                            rec.insElem("cespnumacq", "");
                            rec.insElem("cespnriga", 0);
                            rec.insElem("cespmovajazzid", rincre.leggiIntero("mbsmovjazzid"));
                            rec.insElem("cespdataesterna", rincre.leggiStringa("mbsdata"));
                            if (rincre.leggiIntero("mbscaumovid") > 0) {
                                rec.insElem("cespnumesterno", rincre.leggiStringa("bscaudescr"));
                            } else {
                                rec.insElem("cespnumesterno", "");
                            }
                            rec.insElem("cespfornit", rincre.leggiStringa("mbsnote"));
                            rec.insElem("cespfornpiva", "");

                            if (rincre.leggiIntero("mbstipocf") == 1) {
                                rec.insElem("cespvaloreacqc", rincre.leggiDouble("mbsimpbene"));
                                rec.insElem("cespquotadeduc", 0.00);
                            } else {
                                rec.insElem("cespvaloreacqc", rincre.leggiDouble("mbsimpbene"));
                                rec.insElem("cespquotadeduc", rincre.leggiDouble("mbsimpbene"));
                            }
                            if (rincre.leggiIntero("mbstipocf") == 1) {
                                totaleanno += rincre.leggiDouble("mbsimpbene");
                                totalegenerale += rincre.leggiDouble("mbsimpbene");
                            } else {
                                totaleannof += rincre.leggiDouble("mbsimpbene");
                                totalegeneralef += rincre.leggiDouble("mbsimpbene");
                            }
                            rec.insElem("cespnotea", "");
                            rec.insElem("cespimponibile", 0.00);
                            rec.insElem("cespivanodetr", 0.00);
                            rec.insElem("cespvalorenoamm", 0.00);
                            rec.insElem("cespvaloreacq", 0.00);
                            rec.insElem("cespusato", 0);
                            rec.insElem("cespvalriscatto", 0.00);
                            rec.insElem("cespannileasing", 0);
                            rec.insElem("cespqta", 0);
                            rec.insElem("cesppercdeduc1", 100.00);
                            rec.insElem("cespmassimale", 0.00);
                            rec.insElem("cespimpnetto", 0.00);
                            vcespiti.add(rec);
                            rincre = query.successivo();
                        }

                        totaleanno += r.leggiDouble("cespvaloreacqc");
                        totalegenerale += r.leggiDouble("cespvaloreacqc");
                        if (r.leggiDouble("cespquotadeduc") == 0) {
                            totaleannof += r.leggiDouble("cespvaloreacqc");
                            totalegeneralef += r.leggiDouble("cespvaloreacqc");
                        } else {
                            totaleannof += r.leggiDouble("cespquotadeduc");
                            totalegeneralef += r.leggiDouble("cespquotadeduc");
                        }
                    }
                }
                qRic.chiusura();

                qry = "SELECT cespid,cespcategid,cespordine,cespdescriz1,cespdatafunz,cespdataalien,cespcapoid,cespitiven.* FROM cespiti"
                    + " INNER JOIN cespitiven ON cespiti.cespid = cespitiven.cespvcespid"
                    + " INNER JOIN categoriebs ON cespcategid = catbsid"
                    + " WHERE cespid > 0 AND (vconsistenzac < 0 OR cespimpvend<0)";
                    if (catbsid > 0) {
                        qry += " AND cespcategid = " + catbsid;
                    }
                    if (cespbsid > 0) {
                        qry += " AND cespid = " + cespbsid;
                    }
                    if (!annoacq.getText().trim().equals("")) {
                        qry += " AND (cespesercv = \"" + annoacq.getText().trim() + annoacq.getText().trim() + "\""
                                + " OR cespesercv = \"" + (Formattazione.estraiIntero(annoacq.getText().trim()) - 1) + annoacq.getText().trim() + "\")";
                    }

                qry += " ORDER BY cespesercv DESC,cespdatainizio DESC,catbsgruppo,catbsspecie,catbssotto,catbstipo,catbscod,cespordine";
                //        System.out.println(qry);
                qRic = new Query(connAz, qry);
                qRic.apertura();
                num = qRic.numeroRecord();
                if (num > 0) {
                    for (int i = 0; i < num; i++) {
                        Record r = qRic.successivo();
                        if (i == 0) {
                            cespeserc = r.leggiStringa("cespesercv");
                        }
                        if (!r.leggiStringa("cespesercv").equals(cespeserc))
                        {
                            Record rins = new Record();
                            rins.insElem("cespid", -1);
                            rins.insElem("cespcategid", -1);
                            rins.insElem("cespordine", "");
                            rins.insElem("cespdescriz1", "");
                            rins.insElem("cespdatafunz", "");
                            rins.insElem("cespdataalien", "");
                            rins.insElem("cespaid", -1);
                            rins.insElem("cespacespid", -1);
                            rins.insElem("cespeserca", "");                        
                            rins.insElem("cespseza", "");
                            rins.insElem("cespdataacq", "");
                            rins.insElem("cespnumacq", "");
                            rins.insElem("cespnriga", 0);
                            rins.insElem("cespdataesterna", "");
                            rins.insElem("cespnumesterno", "");
                            rins.insElem("cespfornit", "TOTALE anno " + cespeserc.substring(0, 4));
                            cespeserc = r.leggiStringa("cespesercv");
                            rins.insElem("cespfornpiva", "");
                            rins.insElem("cespnotea", "");
                            rins.insElem("cespimponibile", 0.00);
                            rins.insElem("cespivanodetr", 0.00);
                            rins.insElem("cespvalorenoamm", 0.00);
                            rins.insElem("cespvaloreacq", 0.00);
                            rins.insElem("cespvaloreacqc", totaleanno);
                            rins.insElem("cespusato", 0);
                            rins.insElem("cespvalriscatto", 0.00);
                            rins.insElem("cespannileasing", 0);
                            rins.insElem("cespqta", 0);
                            rins.insElem("cesppercdeduc1", 100.00);
                            rins.insElem("cespmassimale", 0.00);
                            rins.insElem("cespquotadeduc", totaleannof);
                            rins.insElem("cespimpnetto", 0.00);
                            rins.insElem("cespmovajazzid", -1);
                            rins.insElem("cespmovvjazzid", -1);
                            rins.insElem("cespvid", -1);

                            totaleanno = 0;
                            totaleannof = 0;
                            vcespiti.add(rins);
                        
                        }
                        if (modalita.equals("INCREMENTO")) 
                        {
                            if (r.leggiStringa("cespdatafunz").compareTo(r.leggiStringa("cespdataesterna")) < 0) {
                            vcespiti.add(r);
                            totaleanno -= r.leggiDouble("cespvaloreacqc");
                            totalegenerale -= r.leggiDouble("cespvaloreacqc");
                            if (r.leggiDouble("cespquotadeduc") == 0) {
                                totaleannof -= r.leggiDouble("cespvaloreacqc");
                                totalegeneralef -= r.leggiDouble("cespvaloreacqc");
                            } else {
                                totaleannof -= r.leggiDouble("cespquotadeduc");
                                totalegeneralef -= r.leggiDouble("cespquotadeduc");
                            }
                        } else if (r.leggiStringa("cespdatafunz").compareTo(r.leggiStringa("cespdataesterna")) == 0) {
                            if (r.leggiIntero("cespcapoid") != -1) {
                                vcespiti.add(r);
                                totaleanno -= r.leggiDouble("cespvaloreacqc");
                                totalegenerale -= r.leggiDouble("cespvaloreacqc");
                                if (r.leggiDouble("cespquotadeduc") == 0) {
                                    totaleannof -= r.leggiDouble("cespvaloreacqc");
                                    totalegeneralef -= r.leggiDouble("cespvaloreacqc");
                                } else {
                                    totaleannof += r.leggiDouble("cespquotadeduc");
                                    totalegeneralef -= r.leggiDouble("cespquotadeduc");
                                }
                            }
                        }
                    } else if (modalita.equals("NUOVI")) {
                        if (r.leggiStringa("cespdatafunz").compareTo(r.leggiStringa("cespdataesterna")) >= 0 && r.leggiIntero("cespcapoid") == -1) {
                            vcespiti.add(r);
                            totaleanno -= r.leggiDouble("cespvaloreacqc");
                            totalegenerale -= r.leggiDouble("cespvaloreacqc");
                            if (r.leggiDouble("cespquotadeduc") == 0) {
                                totaleannof -= r.leggiDouble("cespvaloreacqc");
                                totalegeneralef -= r.leggiDouble("cespvaloreacqc");
                            } else {
                                totaleannof -= r.leggiDouble("cespquotadeduc");
                                totalegeneralef -= r.leggiDouble("cespquotadeduc");
                            }
                        }

                    } else {
                            Record rec = new Record();
                            rec.insElem("cespid", r.leggiIntero("cespid"));
                            rec.insElem("cespcategid", r.leggiIntero("cespcategid"));
                            rec.insElem("cespordine", r.leggiStringa("cespordine"));
                            rec.insElem("cespdescriz1", r.leggiStringa("cespdescriz1"));
                            rec.insElem("cespdatafunz", r.leggiStringa("cespdatafunz"));
                            rec.insElem("cespdataalien", r.leggiStringa("cespdataalien"));
                            rec.insElem("cespaid", 0);//proviene da movimentibs anziche' cespitiacq
                            rec.insElem("cespacespid", r.leggiIntero("cespid"));
                            rec.insElem("cespeserca", r.leggiStringa("cespesercv"));
                            rec.insElem("cespdataacq", r.leggiStringa("cespdataven"));
                            rec.insElem("cespseza", "");
                            rec.insElem("cespnumacq", "");
                            rec.insElem("cespnriga", 0);
                            rec.insElem("cespmovajazzid", -1);
                            rec.insElem("cespdataesterna", r.leggiStringa("cespdataven"));
                            rec.insElem("cespnumesterno", r.leggiStringa("cespnumven"));                            
                            rec.insElem("cespfornit", "");
                            rec.insElem("cespfornpiva", "");

                            rec.insElem("cespvaloreacqc", -r.leggiDouble("vconsistenzac"));
                            rec.insElem("cespquotadeduc", 0.00);                            
                            
                            rec.insElem("cespnotea", "");
                            rec.insElem("cespimponibile", 0.00);
                            rec.insElem("cespivanodetr", 0.00);
                            rec.insElem("cespvalorenoamm", 0.00);
                            rec.insElem("cespvaloreacq", 0.00);
                            rec.insElem("cespusato", 0);
                            rec.insElem("cespvalriscatto", 0.00);
                            rec.insElem("cespannileasing", 0);
                            rec.insElem("cespqta", 0);
                            rec.insElem("cesppercdeduc1", 100.00);
                            rec.insElem("cespmassimale", 0.00);
                            rec.insElem("cespimpnetto", 0.00);
                            rec.insElem("cespmovvjazzid", r.leggiIntero("cespmovvjazzid"));
                            rec.insElem("cespvid", r.leggiIntero("cespvid"));
                            vcespiti.add(rec);

                        String qryx = vcau[0];
                        for (int k = 1; k < vcau.length; k++) {
                            qryx += "," + vcau[k];
                        }

                        String qryincr = "SELECT mbseserc,mbsdata,mbsmovjazzid,mbsnote,mbscespid,mbsid,mbsimpbene,mbstipocf,mbscaumovid,bscaudescr FROM movimentibs"
                                + " LEFT JOIN caubs ON mbscaumovid = bscauid"
                                + " WHERE mbscespid=" + r.leggiIntero("cespid") + " AND "
                                + "((mbscaumovid IN(" + qryx + ")) AND mbstipocf = 2) AND mbseserc=\""+annoacq.getText().trim()+annoacq.getText().trim()+"\"";
                        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qryincr);
                        query.apertura();
                        Record rincre = query.successivo();
                        while (rincre != null) {
                            rec = new Record();
                            rec.insElem("cespid", r.leggiIntero("cespid"));
                            rec.insElem("cespcategid", r.leggiIntero("cespcategid"));
                            rec.insElem("cespordine", r.leggiStringa("cespordine"));
                            rec.insElem("cespdescriz1", r.leggiStringa("cespdescriz1"));
                            rec.insElem("cespdatafunz", r.leggiStringa("cespdatafunz"));
                            rec.insElem("cespdataalien", r.leggiStringa("cespdataalien"));
                            rec.insElem("cespaid", 0);//proviene da movimentibs anziche' cespitiacq
                            rec.insElem("cespacespid", r.leggiIntero("cespid"));
                            rec.insElem("cespeserca", rincre.leggiStringa("mbseserc"));
                            rec.insElem("cespdataacq", rincre.leggiStringa("mbsdata"));
                            rec.insElem("cespseza", "");
                            rec.insElem("cespnumacq", "");
                            rec.insElem("cespnriga", 0);
                            rec.insElem("cespmovajazzid", rincre.leggiIntero("mbsmovjazzid"));
                            rec.insElem("cespdataesterna", rincre.leggiStringa("mbsdata"));
                            if (rincre.leggiIntero("mbscaumovid") > 0) {
                                rec.insElem("cespnumesterno", rincre.leggiStringa("bscaudescr"));
                            } else {
                                rec.insElem("cespnumesterno", "");
                            }
                            rec.insElem("cespfornit", rincre.leggiStringa("mbsnote"));
                            rec.insElem("cespfornpiva", "");

                            if (rincre.leggiIntero("mbstipocf") == 1) {
                                rec.insElem("cespvaloreacqc", rincre.leggiDouble("mbsimpbene"));
                                rec.insElem("cespquotadeduc", 0.00);
                            } else {
                                rec.insElem("cespvaloreacqc", rincre.leggiDouble("mbsimpbene"));
                                rec.insElem("cespquotadeduc", rincre.leggiDouble("mbsimpbene"));
                            }
                            if (rincre.leggiIntero("mbstipocf") == 1) {
                                totaleanno -= rincre.leggiDouble("mbsimpbene");
                                totalegenerale -= rincre.leggiDouble("mbsimpbene");
                            } else {
                                totaleannof -= rincre.leggiDouble("mbsimpbene");
                                totalegeneralef -= rincre.leggiDouble("mbsimpbene");
                            }
                            rec.insElem("cespnotea", "");
                            rec.insElem("cespimponibile", 0.00);
                            rec.insElem("cespivanodetr", 0.00);
                            rec.insElem("cespvalorenoamm", 0.00);
                            rec.insElem("cespvaloreacq", 0.00);
                            rec.insElem("cespusato", 0);
                            rec.insElem("cespvalriscatto", 0.00);
                            rec.insElem("cespannileasing", 0);
                            rec.insElem("cespqta", 0);
                            rec.insElem("cesppercdeduc1", 100.00);
                            rec.insElem("cespmassimale", 0.00);
                            rec.insElem("cespimpnetto", 0.00);
                            rec.insElem("cespmovvjazzid", r.leggiIntero("cespmovvjazzid"));
                            rec.insElem("cespvid", r.leggiIntero("cespvid"));
                            vcespiti.add(rec);
                            rincre = query.successivo();
                        }

                        totaleanno -= r.leggiDouble("vconsistenzac");
                        totalegenerale -= r.leggiDouble("vconsistenzac");
//                        if (r.leggiDouble("cespquotadeduc") == 0) {
                            totaleannof -= r.leggiDouble("vconsistenzac");
                            totalegeneralef -= r.leggiDouble("vconsistenzac");
//                        }
//                        else {
//                            totaleannof -= r.leggiDouble("cespquotadeduc");
//                            totalegeneralef -= r.leggiDouble("cespquotadeduc");
//                        }
                    }
                }
                qRic.chiusura();
                
                Record rins = new Record();
                rins.insElem("cespid", -1);
                rins.insElem("cespcategid", -1);
                rins.insElem("cespordine", "");
                rins.insElem("cespdescriz1", "");
                rins.insElem("cespdatafunz", "");
                rins.insElem("cespdataalien", "");
                rins.insElem("cespaid", -1);
                rins.insElem("cespacespid", -1);
                rins.insElem("cespeserca", "");
                rins.insElem("cespseza", "");
                rins.insElem("cespdataacq", "");
                rins.insElem("cespnumacq", "");
                rins.insElem("cespnriga", 0);
                rins.insElem("cespdataesterna", "");
                rins.insElem("cespnumesterno", "");
                rins.insElem("cespfornit", "TOTALE anno " + cespeserc.substring(0, 4));
                rins.insElem("cespfornpiva", "");
                rins.insElem("cespnotea", "");
                rins.insElem("cespimponibile", 0.00);
                rins.insElem("cespivanodetr", 0.00);
                rins.insElem("cespvalorenoamm", 0.00);
                rins.insElem("cespvaloreacq", 0.00);
                rins.insElem("cespvaloreacqc", totaleanno);
                rins.insElem("cespusato", 0);
                rins.insElem("cespvalriscatto", 0.00);
                rins.insElem("cespannileasing", 0);
                rins.insElem("cespqta", 0);
                rins.insElem("cesppercdeduc1", 100.00);
                rins.insElem("cespmassimale", 0.00);
                rins.insElem("cespquotadeduc", totaleannof);
                rins.insElem("cespimpnetto", 0.00);
                rins.insElem("cespmovajazzid", -1);

                vcespiti.add(rins);
                rins = new Record();
                rins.insElem("cespid", -1);
                rins.insElem("cespcategid", -1);
                rins.insElem("cespordine", "");
                rins.insElem("cespdescriz1", "");
                rins.insElem("cespdatafunz", "");
                rins.insElem("cespdataalien", "");
                rins.insElem("cespaid", -1);
                rins.insElem("cespacespid", -1);
                rins.insElem("cespeserca", "");
                rins.insElem("cespseza", "");
                rins.insElem("cespdataacq", "");
                rins.insElem("cespnumacq", "");
                rins.insElem("cespnriga", 0);
                rins.insElem("cespdataesterna", "");
                rins.insElem("cespnumesterno", "");
                rins.insElem("cespfornit", "TOTALE GENERALE");
                rins.insElem("cespfornpiva", "");
                rins.insElem("cespnotea", "");
                rins.insElem("cespimponibile", 0.00);
                rins.insElem("cespivanodetr", 0.00);
                rins.insElem("cespvalorenoamm", 0.00);
                rins.insElem("cespvaloreacq", 0.00);
                rins.insElem("cespvaloreacqc", totalegenerale);
                rins.insElem("cespusato", 0);
                rins.insElem("cespvalriscatto", 0.00);
                rins.insElem("cespannileasing", 0);
                rins.insElem("cespqta", 0);
                rins.insElem("cesppercdeduc1", 100.00);
                rins.insElem("cespmassimale", 0.00);
                rins.insElem("cespquotadeduc", totalegeneralef);
                rins.insElem("cespimpnetto", 0.00);
                rins.insElem("cespmovajazzid", -1);

                vcespiti.add(rins);
            } 
            mostraLista();
        }
        }
    }

    private void mostraLista() {
        String[] dtes = BusinessLogicAmbiente.leggiEsercizio(azienda, esContabile);
        Record r = null;
        int i = 0;
        Data oggi = new Data();
        String dataregord = (String) oggi.formatta(Data.AAAA_MM_GG);

        final String[] names = {
            COL_DESCR, COL_ANNO, COL_DOC, COL_DTACQ, COL_FORNIT, COL_RIF, COL_VALORE, COL_FISCALE, COL_NCF, COL_MOD, COL_COLLPN,COL_PRIMANOTA,COL_ELIMINA
        };
        final Object[][] data = new Object[vcespiti.size()][names.length];
        id = new int[vcespiti.size()];
        idC = new int[vcespiti.size()];
        flagmodificabile = new boolean[vcespiti.size()];

        for (i = 0; i < vcespiti.size(); i++) {
            r = vcespiti.get(i);
            if (r.leggiIntero("cespaid") >= 0) {
                data[i][0] = r.leggiStringa("cespordine") + " - " + r.leggiStringa("cespdescriz1");
                data[i][1] = r.leggiStringa("cespeserca").substring(0, 4) + "  "
                        + (new Data(r.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");

                if (r.leggiIntero("cespmovajazzid") > 0) {
                    String qrydoc="SELECT * FROM movimentidoc"+r.leggiStringa("cespeserca")+" WHERE movdocid="+r.leggiIntero("cespmovajazzid");
                    QueryInterrogazioneClient query=new QueryInterrogazioneClient(connAz,qrydoc);
                    query.apertura();
                    Record rmd=query.successivo();
                    query.chiusura();
                    if(rmd!=null)
                    {
                        data[i][2] = rmd.leggiIntero("movdocnumiva")+ " del "+ (new Data(rmd.leggiStringa("movdocdtiva"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    }
                    else
                    {
                        data[i][2] = "";
                    }
                }
                else
                {
                    data[i][2] = "";
                }
                data[i][3] = (new Data(r.leggiStringa("cespdataacq"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");

                String legamejazz = "";
                if (r.leggiIntero("cespmovajazzid") > 0) {
                    legamejazz = "Reg. SI ";
                } else if (r.esisteCampo("cespmovvjazzid"))
                {
                    if(r.leggiIntero("cespmovvjazzid") > 0)
                    {
                        legamejazz = "Reg. SI ";
                    }
                    else
                    {
                        legamejazz = "Manuale ";
                    }
                }else {
                    legamejazz = "Manuale ";
                }
                if (r.leggiDouble("cespvaloreacqc") >= 0) {
                    if (!r.leggiStringa("cespdataesterna").equals("") && r.leggiIntero("cespaid") > 0) {
                        data[i][5] = legamejazz + "FT n." + r.leggiStringa("cespnumesterno") + " del "
                                + (new Data(r.leggiStringa("cespdataesterna"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    } else if (r.leggiIntero("cespaid") == 0 && !r.leggiStringa("cespdataesterna").equals("")) {
                        data[i][5] = legamejazz + "NC. " + r.leggiStringa("cespnumesterno") + " del "
                                + (new Data(r.leggiStringa("cespdataesterna"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");;
                    } else {
                        data[i][5] = "";
                    }
                } else {
                    if (!r.leggiStringa("cespdataesterna").equals("") && r.leggiIntero("cespaid") > 0) {
                        data[i][5] = legamejazz + "NC n." + r.leggiStringa("cespnumesterno") + " del "
                                + (new Data(r.leggiStringa("cespdataesterna"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    } else if (r.leggiIntero("cespaid") == 0 && !r.leggiStringa("cespdataesterna").equals("")) {
                        data[i][5] = legamejazz + "NC. " + r.leggiStringa("cespnumesterno") + " del "
                                + (new Data(r.leggiStringa("cespdataesterna"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");;
                    } else {
                        data[i][5] = "";
                    }
                }
                data[i][6] = Formattazione.formValuta(r.leggiDouble("cespvaloreacqc"), 12, 2, 2);
                if (r.leggiDouble("cespquotadeduc") == 0) {
                    if(!r.nullo("cespmovvjazzid"))
                    {
                        data[i][7] = Formattazione.formValuta(r.leggiDouble("cespvaloreacqc"), 12, 2, 2);
                    }
                } else {
                    data[i][7] = Formattazione.formValuta(r.leggiDouble("cespquotadeduc"), 12, 2, 2);
                }
                data[i][8] = "";
//                double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, r.leggiIntero("cespid"),esContabile , dataregord);
//                double totbene = OpValute.arrotondaMat(vv[1], 2);
//                double totfondo = OpValute.arrotondaMat(vv[3], 2);
//                double totresiduo = totbene - totfondo;
                String note = "";
                if (r.leggiStringa("cespdatafunz").compareTo(dataregord) > 0) {
                    note = "Non ancora in funzione";
                    flagmodificabile[i] = true;
                } else if (r.leggiStringa("cespdataalien").compareTo("0000-00-00") != 0) {
                    note = "Venduto";
                    flagmodificabile[i] = false;
//                } else if (totresiduo == 0) {      //tolto per velocizzare
//                    note = "Ammortizzato";
//                    flagmodificabile[i] = false;
                } else {
                    note = "";
                    flagmodificabile[i] = true;
                }
                data[i][4] = note + " " + r.leggiStringa("cespfornit");
            } else {
                data[i][0] = "";
                data[i][1] = r.leggiStringa("cespeserca");
                data[i][2] = "";
                data[i][3] = "";
                data[i][4] = r.leggiStringa("cespfornit");
                data[i][5] = "";
                data[i][6] = Formattazione.formValuta(r.leggiDouble("cespvaloreacqc"), 12, 2, 2);
                data[i][7] = Formattazione.formValuta(r.leggiDouble("cespquotadeduc"), 12, 2, 2);
                flagmodificabile[i] = false;
                data[i][8]="";
            }

            id[i] = r.leggiIntero("cespid");
            idC[i] = r.leggiIntero("cespcategid");

        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_MOD)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_COLLPN)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_PRIMANOTA)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_NCF)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_ELIMINA)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tab, true);

        tab.getColumn(COL_DESCR).setPreferredWidth(380);
        tab.getColumn(COL_ANNO).setPreferredWidth(120);
        tab.getColumn(COL_DOC).setPreferredWidth(120);
        tab.getColumn(COL_DTACQ).setPreferredWidth(76);
        tab.getColumn(COL_FORNIT).setPreferredWidth(300);
        tab.getColumn(COL_RIF).setPreferredWidth(220);
        tab.getColumn(COL_VALORE).setPreferredWidth(88);
        tab.getColumn(COL_FISCALE).setPreferredWidth(88);
        tab.getColumn(COL_NCF).setPreferredWidth(32);
        tab.getColumn(COL_MOD).setPreferredWidth(32);
        tab.getColumn(COL_COLLPN).setPreferredWidth(32);
        tab.getColumn(COL_PRIMANOTA).setPreferredWidth(32);
        tab.getColumn(COL_ELIMINA).setPreferredWidth(32);        
        
        tab.getColumn(COL_ELIMINA).setHeaderRenderer(headerRenderer);
        
        TableColumn buttonColumn = tab.getColumn(COL_NCF);
        TablePopButton button = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespaid") > 0 && r.leggiDouble("cespvaloreacqc") > 0) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_remove_circle_white_24dp.png")));
                    button.setToolTipText("Registra Nota credito fornitore su riga documento acquisto");
                    button.setEnabled(true);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_remove_circle_white_24dp.png")));
                    button.setToolTipText("");
                    button.setEnabled(false);
                }
            }
        });
        button.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespaid") > 0 && r.leggiDouble("cespvaloreacqc") > 0) {
                    int modo = 1;
                    int tipo = 2;
                    DialogUtilitaModificaAcquisto d = new DialogUtilitaModificaAcquisto(formGen, true, connAz, azienda, r, modo, tipo,-1,-1,id[row]);
                    UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                    d.show();
                    if (comboCategorieBs.getSelectedIndex() > 0) {
                        interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
                    } else if(idCat>0)
                    {
                        interrogazione(idCat, idCesp);
                    }
                    else{
                        interrogazione(-1, -1);
                    }
                }
            }
        });
        buttonColumn.setCellRenderer(button);
        buttonColumn.setCellEditor(button);

        TableColumn buttonColumn1 = tab.getColumn(COL_MOD);
        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (flagmodificabile[row] && r.leggiIntero("cespaid") > 0) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_create_white_20dp.png")));
                    button.setToolTipText("Modifica valori senza interagire con prima nota. ATTENZIONE!");
                    button.setEnabled(true);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_create_white_20dp.png")));
                    button.setToolTipText("NON modificabile");
                    button.setEnabled(false);
                }
            }
        });
        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                if (flagmodificabile[row] && r.leggiIntero("cespaid") >= 0) {
                    int modo = 0;
                    int tipo = -1;
                    if(r.leggiDouble("cespvaloreacqc") >= 0)
                    {
                        tipo = 1;
                    }
                    else
                    {
                        tipo = 2;
                    }
                    if (r.leggiIntero("cespaid") == 0) {
                        modo = 1;//solo movimentobs
                    } else {
                        modo = 2;
                    }
                    DialogUtilitaModificaAcquisto d = new DialogUtilitaModificaAcquisto(formGen, true, connAz, azienda, r, modo, tipo,-1,-1,id[row]);
                    UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                    d.show();
                    if (comboCategorieBs.getSelectedIndex() > 0) {
                        interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
                    } else if(idCat>0)
                    {
                        interrogazione(idCat, idCesp);
                    }
                    else{
                        interrogazione(-1, -1);
                    }
                }
            }
        });
        buttonColumn1.setCellRenderer(button1);
        buttonColumn1.setCellEditor(button1);

        TableColumn buttonColumn2 = tab.getColumn(COL_COLLPN);
        TablePopButton button2 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespmovajazzid") < 1 && r.leggiIntero("cespaid") >= 0) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_compare_arrows_white_24dp.png")));
                    button.setToolTipText("Collega a contabilizzazione");
                    button.setEnabled(true);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_compare_arrows_white_24dp.png")));
                    button.setToolTipText("Legame esistente id del documento: " + r.leggiIntero("cespmovajazzid"));
                    button.setEnabled(false);
                }
            }
        });
        button2.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespmovajazzid") < 1 && r.leggiIntero("cespaid") >= 0) {
                    int modo = 0;
                    if (r.leggiIntero("cespaid") == 0) {
                        modo = 1;//solo movimentobs
                    } else {
                        modo = 2;
                    }
                    DialogUtilitaLegaMovimentoJazz d = new DialogUtilitaLegaMovimentoJazz(formGen, true, connAz, azienda, r, modo);
                    UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                    d.show();
                    if (comboCategorieBs.getSelectedIndex() > 0) {
                        interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
                    } else if(idCat>0)
                    {
                        interrogazione(idCat, idCesp);
                    }
                    else{
                        interrogazione(-1, -1);
                    }
                }
            }
        });
        buttonColumn2.setCellRenderer(button2);
        buttonColumn2.setCellEditor(button2);

        TableColumn buttonColumn3 = tab.getColumn(COL_PRIMANOTA);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespmovajazzid") < 1 ) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ESTRATTO_CONTO_NEW.png")));
                    button.setToolTipText("");
                    button.setEnabled(false);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ESTRATTO_CONTO_NEW.png")));
                    button.setToolTipText("Apri prima nota");
                    button.setEnabled(true);
                }
            }
        });
        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespmovajazzid") > -1 && r.leggiIntero("cespaid") >= 0) {
                                        
                    String qrydoc="SELECT * FROM movimentidoc"+r.leggiStringa("cespeserca")+" WHERE movdocid="+r.leggiIntero("cespmovajazzid");
                    QueryInterrogazioneClient query=new QueryInterrogazioneClient(connAz,qrydoc);
                    query.apertura();
                    Record rmd=query.successivo();
                    query.chiusura();
                    if (rmd != null) 
                    {
                        Object[] cpar = new Object[12];
                        cpar[0] = formGen;
                        cpar[1] = new Boolean(false);
                        cpar[2] = connAz;
                        cpar[3] = connCom;
                        cpar[4] = azienda;
                        cpar[5] = r.leggiStringa("cespeserca");
                        cpar[6] = new Integer(rmd.leggiIntero("movdocdepid"));
                        cpar[7] = new Integer(rmd.leggiIntero("movdocpedid"));
                        cpar[8] = new Integer(rmd.leggiIntero("movdocdocumid"));
                        cpar[9] = (new Data(rmd.leggiStringa("movdocdtiva"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                        cpar[10] = new Integer(rmd.leggiIntero("movdocid"));
                        cpar[11] = new Integer(EnvJazz.VISUALIZZAZIONE);
                        Object d = FunzioniUtilita.creaOggetto("documenti.dialogstart.DialogDocMagCog", cpar);
                        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                        FunzioniUtilita.invocaMetodo(d, "show", null);
                    }
                    
                    if (comboCategorieBs.getSelectedIndex() > 0) {
                        interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
                    } else if(idCat>0)
                    {
                        interrogazione(idCat, idCesp);
                    }
                    else{
                        interrogazione(-1, -1);
                    }
                }
            }
        });
        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);
        
        TableColumn buttonColumn4 = tab.getColumn(COL_ELIMINA);
        TablePopButton button4 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiStringa("cespdatafunz").compareTo(r.leggiStringa("cespdataesterna")) <= 0) 
                {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                    button.setToolTipText("Elimina");
                    button.setEnabled(true);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                    button.setEnabled(false);
                }
            }
        });
        button4.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                int risp = JOptionPane.showConfirmDialog(formGen, "Si desidera eliminare l'incremento?", "Avviso",
                                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                if (risp == JOptionPane.YES_OPTION) 
                {   if(r.leggiIntero("cespmovajazzid")>0)
                    {
                        String qrydelete = "DELETE FROM cespitiacq WHERE cespaid = " + r.leggiIntero("cespaid");
                        QueryAggiornamentoClient qi1 = new QueryAggiornamentoClient(connAz, qrydelete);
                        qi1.esecuzione();   

                        int ris = JOptionPane.showConfirmDialog(formGen, "E' collegato anche il documento di contabilità. Eliminare anche la contabilità?", "PROCEDO ?",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                        if (ris == JOptionPane.YES_OPTION) 
                        {
                            String qrydoc="SELECT movdocdocumid FROM movimentidoc"+r.leggiStringa("cespeserca")+" WHERE movdocid="+r.leggiIntero("cespmovajazzid");
                            QueryInterrogazioneClient qn = new QueryInterrogazioneClient(connAz,qrydoc);
                            qn.apertura();
                            Record rdocum = qn.successivo();
                            qn.chiusura();
                            
                            Record rdoc = new Record();
                            if(rdocum!=null)
                            {
                                String qrydoc2="SELECT * FROM documenti WHERE docid="+rdocum.leggiIntero("movdocdocumid");
                                qn = new QueryInterrogazioneClient(connAz,qrydoc2);
                                qn.apertura();
                                rdoc = qn.successivo();
                                qn.chiusura();
                            }
                            
                            String qry="SELECT sufxcod FROM sezionalinum WHERE sufxid = " + rdoc.leggiIntero("docsuffnumid");
                            qn = new QueryInterrogazioneClient(connAz,qry);
                            qn.apertura();
                            Record rSez = qn.successivo();
                            qn.chiusura();

                            String annoInizio = esContabile.substring(0, 4);
                            String annoFine = esContabile.substring(4, 8);
                            String qrysuf="SELECT sufxid,sufxtipo,sufxcod,sufxnum FROM sezionalinum WHERE sufxcod = \"" + rSez.leggiStringa("sufxcod") + "\" AND "
                                    + "sufxannoinizio = \"" + annoInizio + "\" AND "
                                    + "sufxannofine = \"" + annoFine + "\"";
                            qn = new QueryInterrogazioneClient(connAz,qrysuf);
                            qn.apertura();
                            Record rsez = qn.successivo();
                            qn.chiusura();
                            
                            FunzFatt.eliminaDocumento(connAz, r.leggiStringa("cespeserca"), rdoc, rsez, r.leggiIntero("cespmovajazzid"), false, formGen);

                        }
                    }
                    else if(!r.nullo("cespmovvjazzid") && r.leggiIntero("cespmovvjazzid")>0)
                    {
                        String qrydelete = "DELETE FROM cespitiven WHERE cespvid = " + r.leggiIntero("cespvid");
                        QueryAggiornamentoClient qi1 = new QueryAggiornamentoClient(connAz, qrydelete);
                        qi1.esecuzione();   

                        int ris = JOptionPane.showConfirmDialog(formGen, "E' collegato anche il documento di contabilità. Eliminare anche la contabilità?", "PROCEDO ?",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                        if (ris == JOptionPane.YES_OPTION) 
                        {
                            String qrydoc="SELECT movdocdocumid FROM movimentidoc"+r.leggiStringa("cespeserca")+" WHERE movdocid="+r.leggiIntero("cespmovvjazzid");
                            QueryInterrogazioneClient qn = new QueryInterrogazioneClient(connAz,qrydoc);
                            qn.apertura();
                            Record rdocum = qn.successivo();
                            qn.chiusura();
                            
                            String qrydoc2="SELECT * FROM documenti WHERE docid="+rdocum.leggiIntero("movdocdocumid");
                            qn = new QueryInterrogazioneClient(connAz,qrydoc2);
                            qn.apertura();
                            Record rdoc = qn.successivo();
                            qn.chiusura();
                            
                            String qry="SELECT sufxcod FROM sezionalinum WHERE sufxid = " + rdoc.leggiIntero("docsuffnumid");
                            qn = new QueryInterrogazioneClient(connAz,qry);
                            qn.apertura();
                            Record rSez = qn.successivo();
                            qn.chiusura();

                            String annoInizio = esContabile.substring(0, 4);
                            String annoFine = esContabile.substring(4, 8);
                            String qrysuf="SELECT sufxid,sufxtipo,sufxcod,sufxnum FROM sezionalinum WHERE sufxcod = \"" + rSez.leggiStringa("sufxcod") + "\" AND "
                                    + "sufxannoinizio = \"" + annoInizio + "\" AND "
                                    + "sufxannofine = \"" + annoFine + "\"";
                            qn = new QueryInterrogazioneClient(connAz,qrysuf);
                            qn.apertura();
                            Record rsez = qn.successivo();
                            qn.chiusura();
                            
                            FunzFatt.eliminaDocumento(connAz, r.leggiStringa("cespeserca"), rdoc, rsez, r.leggiIntero("cespmovvjazzid"), false, formGen);
                        }
                    }
                    else if(r.leggiIntero("cespaid")>0)
                    {
                        String qrydelete = "DELETE FROM cespitiacq WHERE cespaid = " + r.leggiIntero("cespaid");
                        QueryAggiornamentoClient qi1 = new QueryAggiornamentoClient(connAz, qrydelete);
                        qi1.esecuzione();                    
                    }      
                    else if(r.leggiIntero("cespvid")>0)
                    {
                        String qrydelete = "DELETE FROM cespitiven WHERE cespvid = " + r.leggiIntero("cespvid");
                        QueryAggiornamentoClient qi1 = new QueryAggiornamentoClient(connAz, qrydelete);
                        qi1.esecuzione();    
                    }
                
                    if (comboCategorieBs.getSelectedIndex() > 0) {
                        interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
                    } else if(idCat>0)
                    {
                        interrogazione(idCat, idCesp);
                    }
                    else{
                        interrogazione(-1, -1);
                    }
                }
            }
        });
        buttonColumn4.setCellRenderer(button4);
        buttonColumn4.setCellEditor(button4);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_VALORE).setCellRenderer(rightRenderer);
        tab.getColumn(COL_FISCALE).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tab.getColumn(COL_DTACQ).setCellRenderer(centerRenderer);

    }

    public void popolaComboCategorieBs() {
        comboCategorieBs.removeAllItems();
        comboCategorieBs.addItem("< ............... >");
        String qry = "SELECT catbsid,catbscod,catbsdescr FROM categoriebs"
                + " WHERE catbsstato = 1"
                + " ORDER BY catbsgruppo,catbsspecie,catbssotto,catbscod";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            vcatbsid = new int[q.numeroRecord() + 1];
            vcatbsid[0] = -1;
            for (int i = 0; i < q.numeroRecord(); i++) {
                Record rl = q.successivo();
                vcatbsid[i + 1] = rl.leggiIntero("catbsid");
                comboCategorieBs.addItem(rl.leggiStringa("catbscod") + "-" + rl.leggiStringa("catbsdescr"));
            }
        }
    }

    class CambioEsercizio implements ExecGestioni {

        public void lancia() {
            esercSelez = comboEsercizi.leggiEsercizio();
            if (esercSelez.compareTo(esContabile) == 0) {
                buttonNuovoCespite.setEnabled(true);
                buttonRettifiche.setEnabled(true);
            } else {
                buttonNuovoCespite.setEnabled(false);
                buttonRettifiche.setEnabled(true);
                annoacq.setText(esercSelez.substring(4, 8));
            }
            if (comboCategorieBs.getSelectedIndex() > 0) {
                interrogazione(vcatbsid[comboCategorieBs.getSelectedIndex()], -1);
            } else {
                interrogazione(-1, -1);
            }
        }
    }
}
