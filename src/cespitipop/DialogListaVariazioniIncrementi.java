package cespitipop;

/*
 * DialogCanali.java
 *
 * Created on 27 settembre 2006, 14.00
 */
import nucleo.ConnessioneServer;
import java.awt.*;

/**
 *
 * @author svilupp
 */
public class DialogListaVariazioniIncrementi extends javax.swing.JDialog {

    PannelloGestioneIncrementi pp = null;
    private ConnessioneServer connAz = null, connCom = null;
    private String esContabile = "";
    private String esercizio = "";
    private int idCesp;
    private int idCateg;
    private String azienda;
    private String modalita = "";

    /**
     * Creates new form DialogCanali
     */
    public DialogListaVariazioniIncrementi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
        pp = new PannelloGestioneIncrementi(this, parent);
        getContentPane().add(pp);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Visualizzazione incrementi e variazioni immobilizzazioni");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        setSize(new java.awt.Dimension(807, 532));
    }// </editor-fold>//GEN-END:initComponents

    public DialogListaVariazioniIncrementi(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom, String azienda,
            String esContabile, int idCateg, int idCesp, String esercizio, String modalita) {
        this(parent, modal);
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.esercizio = esercizio;
        this.idCateg = idCateg;
        this.idCesp = idCesp;
        this.modalita = modalita;
    }


    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        pp.impostaParametri(idCateg, idCesp, esercizio, modalita);
        pp.inizializza(connAz, connCom, azienda, esContabile, -1, null, "");

    }//GEN-LAST:event_formWindowOpened

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DialogListaVariazioniIncrementi(new javax.swing.JFrame(), true).show();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
