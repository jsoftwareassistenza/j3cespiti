package cespitipop;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import nucleo.ConnessioneServer;
import nucleo.EnvJazz;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;


public class PannelloGestioneAccessiCespiti extends javax.swing.JPanel implements PannelloPop {

    private final String COL_UTENTE = "Utente";
    private final String COL_UTENTE2 = "Utente";

    public Window padre;
    public Frame formGen;
    
    public ConnessioneServer connCom;
    public ConnessioneServer connAz;
    private ConnessioneServer connAmb = EnvJazz.connAmbiente;
    
    private boolean ric = false;
    private boolean dlg = false;
    private boolean azione = false;
    
    private int idVis;
    private int operazione;
    
    private String azienda = "";
    private String user = "";
    private String esContabile = "";    
    private String utente = "";
    private String password = "";
    
    private int[] idpal;
    
    private ArrayList<Record> vopecespiti = null;
    private ArrayList<Record> vopefirma = null;
    private ArrayList<Record> vope = null;
    private ArrayList<Record> vlis = null;
    
    private boolean flagInserimento = false;
    private boolean admin;

    /**
     * Creates new form
     */
    public PannelloGestioneAccessiCespiti(Window padre, Frame formGen) {
        this();
        this.padre = padre;
        this.formGen = formGen;
    }

    public PannelloGestioneAccessiCespiti() {

        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.idVis = idVis;
        this.operazione = operazione;
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;

        passwordaccessi.setVisible(false);
        passwordaccessi1.setVisible(false);
        label_passwordaccessi1.setVisible(false);
        label_passwordaccessi.setVisible(false);
        //operazione=1 primo accesso abilita le scheramte degli operatori
        //altrimenti verificare se l'utente e' un administrator oppure no
        utente = EnvJazz.recOperatore.leggiStringa("opelogin");

        if (BusinessLogicCespiti.verificaAccessoUtenteCespiti(connAz.conn, utente)) {
            if (!BusinessLogicCespiti.verificaUtenteAdminCespiti(connAz.conn, utente)) {
                // operatore NON ammesso
                JOptionPane.showMessageDialog(this,
                        "NON abilitato alle funzioni.\nImpossibile proseguire.",
                        "", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                padre.dispose();
            } else {
                // l'operatore e' un admin
                admin = true;
                passwordaccessi.setVisible(true);
                passwordaccessi1.setVisible(true);
                label_passwordaccessi.setVisible(true);
                label_passwordaccessi1.setVisible(true);
            }
        } else {
            if (operazione == 1) {
                //l'operatoreun admin di sistema
                admin = true;
                passwordaccessi.setVisible(true);
                label_passwordaccessi.setVisible(true);
            } else {
                // operatore generico
                admin = false;
                // operatore NON ammesso
                JOptionPane.showMessageDialog(this,
                        "NON abilitato alle funzioni.\nImpossibile proseguire.",
                        "", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                padre.dispose();
            }
        }


        String prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cruscottocespiti", "tipoaccesso");
        if (prop.equals("pwd")) {
            opz_accessopassword.setSelected(true);
            opz_accessolibero.setSelected(false);
            label_passwordaccessi.setVisible(true);
            label_passwordaccessi1.setVisible(true);
            passwordaccessi.setVisible(true);
            passwordaccessi1.setVisible(true);
        } else {
            opz_accessopassword.setSelected(false);
            opz_accessolibero.setSelected(true);
            label_passwordaccessi.setVisible(false);
            passwordaccessi.setVisible(false);
            label_passwordaccessi1.setVisible(false);
            passwordaccessi1.setVisible(false);
        }
        opegestionicespiti.combo.removeAllItems();
        opeaccessi.combo.removeAllItems();
        vope = new ArrayList();

        String qry = "SELECT * FROM operatori ORDER BY opelogin";
        QueryInterrogazioneClient qop = new QueryInterrogazioneClient(EnvJazz.connAmbiente, qry);
        qop.apertura();
        for (int i = 0; i < qop.numeroRecord(); i++) {
            Record rop = qop.successivo();
            vope.add(rop);
            opegestionicespiti.combo.addItem(rop.leggiStringa("opelogin"));
            opeaccessi.combo.addItem(rop.leggiStringa("opelogin"));
        }
        qop.chiusura();

        visualizzaUtentiProrietaGestCespiti();
        visualizzaUtentiProrietaOpAccessi();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        ppar = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabopaccessi = new javax.swing.JTable();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        buttonInsOpeAccessi = new ui.beans.PopButton();
        buttonCancOpeAccessi = new ui.beans.PopButton();
        opeaccessi = new ui.beans.ComboBoxEditable();
        label_passwordaccessi = new javax.swing.JLabel();
        JLabel9 = new javax.swing.JLabel();
        passwordaccessi = new javax.swing.JPasswordField();
        jPanel4 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jPanel17 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabopgestListini = new javax.swing.JTable();
        jPanel18 = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        buttonInsOpeGestCespiti = new ui.beans.PopButton();
        buttonCancOpeGestCespiti = new ui.beans.PopButton();
        opegestionicespiti = new ui.beans.ComboBoxEditable();
        JLabel13 = new javax.swing.JLabel();
        label_passwordaccessi1 = new javax.swing.JLabel();
        passwordaccessi1 = new javax.swing.JPasswordField();
        jPanel1 = new javax.swing.JPanel();
        opz_accessopassword = new javax.swing.JRadioButton();
        opz_accessolibero = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        buttonSalvaGestioneAccessi = new ui.beans.PopButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setOpaque(false);
        setPreferredSize(new java.awt.Dimension(1000, 610));
        setLayout(new java.awt.BorderLayout());

        ppar.setBackground(new java.awt.Color(255, 255, 255));
        ppar.setPreferredSize(new java.awt.Dimension(1000, 650));
        ppar.setLayout(null);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setPreferredSize(new java.awt.Dimension(500, 100));
        jPanel3.setLayout(new java.awt.BorderLayout());

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setPreferredSize(new java.awt.Dimension(100, 200));
        jPanel6.setLayout(new java.awt.BorderLayout());

        jScrollPane3.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Supervisore per controllo operatori abilitati", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N
        jScrollPane3.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N

        tabopaccessi.setBackground(new java.awt.Color(255, 255, 232));
        tabopaccessi.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        tabopaccessi.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabopaccessi.setGridColor(new java.awt.Color(204, 204, 204));
        tabopaccessi.setRowHeight(20);
        jScrollPane3.setViewportView(tabopaccessi);

        jPanel6.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setPreferredSize(new java.awt.Dimension(20, 100));
        jPanel6.add(jPanel9, java.awt.BorderLayout.LINE_END);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setPreferredSize(new java.awt.Dimension(20, 100));
        jPanel6.add(jPanel10, java.awt.BorderLayout.LINE_START);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));
        jPanel11.setPreferredSize(new java.awt.Dimension(100, 20));
        jPanel6.add(jPanel11, java.awt.BorderLayout.PAGE_START);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.add(jPanel12, java.awt.BorderLayout.PAGE_END);

        jPanel3.add(jPanel6, java.awt.BorderLayout.CENTER);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.add(jPanel7, java.awt.BorderLayout.NORTH);

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setPreferredSize(new java.awt.Dimension(100, 150));
        jPanel8.setLayout(null);

        buttonInsOpeAccessi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonInsOpeAccessi.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonInsOpeAccessi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsOpeAccessiActionPerformed(evt);
            }
        });
        jPanel8.add(buttonInsOpeAccessi);
        buttonInsOpeAccessi.setBounds(118, 110, 36, 36);

        buttonCancOpeAccessi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png"))); // NOI18N
        buttonCancOpeAccessi.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonCancOpeAccessi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancOpeAccessiActionPerformed(evt);
            }
        });
        jPanel8.add(buttonCancOpeAccessi);
        buttonCancOpeAccessi.setBounds(164, 110, 36, 36);
        jPanel8.add(opeaccessi);
        opeaccessi.setBounds(118, 10, 350, 24);

        label_passwordaccessi.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_passwordaccessi.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_passwordaccessi.setText("Password:");
        label_passwordaccessi.setMaximumSize(new java.awt.Dimension(120, 16));
        label_passwordaccessi.setMinimumSize(new java.awt.Dimension(120, 16));
        label_passwordaccessi.setPreferredSize(new java.awt.Dimension(120, 16));
        jPanel8.add(label_passwordaccessi);
        label_passwordaccessi.setBounds(30, 40, 80, 24);

        JLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel9.setText("Operatore:");
        JLabel9.setMaximumSize(new java.awt.Dimension(120, 16));
        JLabel9.setMinimumSize(new java.awt.Dimension(120, 16));
        JLabel9.setPreferredSize(new java.awt.Dimension(120, 16));
        jPanel8.add(JLabel9);
        JLabel9.setBounds(30, 10, 80, 24);

        passwordaccessi.setText("jPasswordField1");
        jPanel8.add(passwordaccessi);
        passwordaccessi.setBounds(118, 40, 350, 24);

        jPanel3.add(jPanel8, java.awt.BorderLayout.PAGE_END);

        ppar.add(jPanel3);
        jPanel3.setBounds(2, 6, 512, 438);

        jPanel4.setLayout(new java.awt.BorderLayout());

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setPreferredSize(new java.awt.Dimension(100, 200));
        jPanel13.setLayout(new java.awt.BorderLayout());

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setPreferredSize(new java.awt.Dimension(20, 100));
        jPanel13.add(jPanel14, java.awt.BorderLayout.LINE_END);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setPreferredSize(new java.awt.Dimension(20, 100));
        jPanel13.add(jPanel15, java.awt.BorderLayout.LINE_START);

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setPreferredSize(new java.awt.Dimension(100, 20));
        jPanel16.setLayout(null);
        jPanel13.add(jPanel16, java.awt.BorderLayout.PAGE_START);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.add(jPanel17, java.awt.BorderLayout.PAGE_END);

        jScrollPane4.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Operatori abilitati gestione CESPITI", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 14))); // NOI18N
        jScrollPane4.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N

        tabopgestListini.setBackground(new java.awt.Color(255, 255, 232));
        tabopgestListini.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        tabopgestListini.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabopgestListini.setRowHeight(20);
        jScrollPane4.setViewportView(tabopgestListini);

        jPanel13.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        jPanel4.add(jPanel13, java.awt.BorderLayout.CENTER);

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.add(jPanel18, java.awt.BorderLayout.NORTH);

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setPreferredSize(new java.awt.Dimension(100, 150));
        jPanel19.setLayout(null);

        buttonInsOpeGestCespiti.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png"))); // NOI18N
        buttonInsOpeGestCespiti.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonInsOpeGestCespiti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonInsOpeGestCespitiActionPerformed(evt);
            }
        });
        jPanel19.add(buttonInsOpeGestCespiti);
        buttonInsOpeGestCespiti.setBounds(184, 110, 36, 36);

        buttonCancOpeGestCespiti.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png"))); // NOI18N
        buttonCancOpeGestCespiti.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonCancOpeGestCespiti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancOpeGestCespitiActionPerformed(evt);
            }
        });
        jPanel19.add(buttonCancOpeGestCespiti);
        buttonCancOpeGestCespiti.setBounds(230, 110, 36, 36);

        opegestionicespiti.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel19.add(opegestionicespiti);
        opegestionicespiti.setBounds(184, 10, 256, 24);

        JLabel13.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        JLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        JLabel13.setText("Operatore:");
        JLabel13.setMaximumSize(new java.awt.Dimension(120, 16));
        JLabel13.setMinimumSize(new java.awt.Dimension(120, 16));
        JLabel13.setPreferredSize(new java.awt.Dimension(120, 16));
        jPanel19.add(JLabel13);
        JLabel13.setBounds(64, 10, 90, 24);

        label_passwordaccessi1.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        label_passwordaccessi1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_passwordaccessi1.setText("Password:");
        label_passwordaccessi1.setMaximumSize(new java.awt.Dimension(120, 16));
        label_passwordaccessi1.setMinimumSize(new java.awt.Dimension(120, 16));
        label_passwordaccessi1.setPreferredSize(new java.awt.Dimension(120, 16));
        jPanel19.add(label_passwordaccessi1);
        label_passwordaccessi1.setBounds(64, 40, 90, 24);

        passwordaccessi1.setText("jPasswordField1");
        jPanel19.add(passwordaccessi1);
        passwordaccessi1.setBounds(184, 40, 256, 24);

        jPanel4.add(jPanel19, java.awt.BorderLayout.PAGE_END);

        ppar.add(jPanel4);
        jPanel4.setBounds(512, 6, 482, 438);

        add(ppar, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(0, 100));
        jPanel1.setLayout(null);

        opz_accessopassword.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(opz_accessopassword);
        opz_accessopassword.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        opz_accessopassword.setText("Password utente");
        opz_accessopassword.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opz_accessopasswordActionPerformed(evt);
            }
        });
        jPanel1.add(opz_accessopassword);
        opz_accessopassword.setBounds(68, 58, 148, 20);

        opz_accessolibero.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(opz_accessolibero);
        opz_accessolibero.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        opz_accessolibero.setText("Accesso libero");
        opz_accessolibero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                opz_accessoliberoActionPerformed(evt);
            }
        });
        jPanel1.add(opz_accessolibero);
        opz_accessolibero.setBounds(68, 24, 184, 20);

        jLabel1.setFont(new java.awt.Font("Dialog", 2, 14)); // NOI18N
        jLabel1.setText("Necessaria per funzioni significative quali gestione ammortamenti, chiusure esercizio");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(256, 58, 718, 19);

        add(jPanel1, java.awt.BorderLayout.NORTH);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(100, 60));
        jPanel2.setLayout(null);

        buttonSalvaGestioneAccessi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_save_white_24dp.png"))); // NOI18N
        buttonSalvaGestioneAccessi.setActionCommand("Salva");
        buttonSalvaGestioneAccessi.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonSalvaGestioneAccessi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvaGestioneAccessiActionPerformed(evt);
            }
        });
        jPanel2.add(buttonSalvaGestioneAccessi);
        buttonSalvaGestioneAccessi.setBounds(20, 12, 36, 36);

        add(jPanel2, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void buttonSalvaGestioneAccessiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvaGestioneAccessiActionPerformed
        String qry = "DELETE FROM proprieta WHERE pprog = \"cruscottocespiti\" AND pprop = \"tipoaccesso\"";

        QueryAggiornamentoClient qagg = new QueryAggiornamentoClient(connAz, qry);
        qagg.esecuzione();
        
        Record r = new Record();
        r.insElem("pprog", "cruscottocespiti");
        r.insElem("pprop", "tipoaccesso");
        if (opz_accessopassword.isSelected()) {
            r.insElem("pval", "pwd");
        } else {
            r.insElem("pval", "libero");
        }

        QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "proprieta", "pid", r);
        int ris = qins.esecuzione();

        if (ris == EnvJazz.SUCCESSO) {
            JOptionPane.showMessageDialog(this, "Salvataggio eseguito correttamente",
                    "", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        } else {
            JOptionPane.showMessageDialog(this, "Ci sono stati errori nel salvataggio",
                    "", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
    }//GEN-LAST:event_buttonSalvaGestioneAccessiActionPerformed

    private void buttonInsOpeGestCespitiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsOpeGestCespitiActionPerformed

        if (!verificaDuplicatoUtente(opegestionicespiti.combo.getSelectedItem().toString())) {

            if (opz_accessopassword.isSelected()) {
                Record c = new Record();
                c.insElem("pprog", "cruscottocespiti");
                c.insElem("pprop", opegestionicespiti.combo.getSelectedItem().toString());
                String attivazione = new String(passwordaccessi1.getPassword());
                if (attivazione.equals("") || attivazione.equals("jPasswordField1")) {
                    int ris = JOptionPane.showConfirmDialog(this, "Operatore SENZA autorizzazione funzioni significative. PROSEGUO?",
                            "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                    if (ris == JOptionPane.NO_OPTION) {
                        return;
                    }
                }

                c.insElem("pval", attivazione);
                QueryAggiornamentoClient qins2 = new QueryAggiornamentoClient(connAz,
                        QueryAggiornamentoClient.INS, "proprieta", "pid", c);
                qins2.esecuzione();
            }

            Record r = new Record();
            r.insElem("pprog", "cruscottocespiti");
            r.insElem("pprop", "opecespiti");
            r.insElem("pval", opegestionicespiti.combo.getSelectedItem().toString());

            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "proprieta", "pid", r);
            qins.esecuzione();

            visualizzaUtentiProrietaGestCespiti();
        }
    }//GEN-LAST:event_buttonInsOpeGestCespitiActionPerformed

    private void buttonCancOpeGestCespitiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancOpeGestCespitiActionPerformed
        if (tabopgestListini.getSelectedRow() >= 0) {
            int ris = JOptionPane.showConfirmDialog(this, "Confermi eliminazione utente selezionata?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));

            if (ris == JOptionPane.YES_OPTION) {
                String qrydelete = "DELETE FROM proprieta WHERE pprog = \'cruscottocespiti\' AND pval = \'" + (String) tabopgestListini.getValueAt(tabopgestListini.getSelectedRow(), 0) + "\'";
                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
                qd.esecuzione();

                qrydelete = "DELETE FROM proprieta WHERE pprog = \'cruscottocespiti\' AND pprop = \'" + (String) tabopgestListini.getValueAt(tabopgestListini.getSelectedRow(), 0) + "\'";
                QueryAggiornamentoClient qpwd = new QueryAggiornamentoClient(connAz, qrydelete);
                qpwd.esecuzione();

                JOptionPane.showMessageDialog(this, "Utente eliminato",
                        "", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                visualizzaUtentiProrietaGestCespiti();
            }
        }
    }//GEN-LAST:event_buttonCancOpeGestCespitiActionPerformed

    private void opz_accessoliberoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opz_accessoliberoActionPerformed
        if (opz_accessolibero.isSelected()) {
            opz_accessopassword.setSelected(false);
            passwordaccessi.setVisible(false);
            passwordaccessi1.setVisible(false);
            label_passwordaccessi1.setVisible(false);
            label_passwordaccessi.setVisible(false);
        }
    }//GEN-LAST:event_opz_accessoliberoActionPerformed

    private void opz_accessopasswordActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_opz_accessopasswordActionPerformed
        if (opz_accessopassword.isSelected()) {
            opz_accessolibero.setSelected(false);
            if (admin == true || operazione == 1) {
                passwordaccessi.setText("");
                passwordaccessi.setVisible(true);
                passwordaccessi1.setText("");
                passwordaccessi1.setVisible(true);
                label_passwordaccessi1.setVisible(true);
                label_passwordaccessi.setVisible(true);
            }
        }
    }//GEN-LAST:event_opz_accessopasswordActionPerformed

    private void buttonInsOpeAccessiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonInsOpeAccessiActionPerformed

        if (!verificaDuplicatoUtente(opeaccessi.combo.getSelectedItem().toString())) {
            if (opz_accessopassword.isSelected()) {
                Record c = new Record();
                c.insElem("pprog", "cruscottocespiti");
                c.insElem("pprop", opeaccessi.combo.getSelectedItem().toString());
                String attivazione = new String(passwordaccessi.getPassword());

                if (attivazione.equals("") || attivazione.equals("jPasswordField1")) {
                    JOptionPane.showMessageDialog(this, "Inserire una Password corretta!",
                            "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }

                c.insElem("pval", attivazione);
                QueryAggiornamentoClient qins2 = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "proprieta", "pid", c);
                qins2.esecuzione();
            }

            Record r = new Record();
            r.insElem("pprog", "cruscottocespiti");
            r.insElem("pprop", "opecespitiadmin");
            r.insElem("pval", opeaccessi.combo.getSelectedItem().toString());
            QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz,
                    QueryAggiornamentoClient.INS, "proprieta", "pid", r);
            qins.esecuzione();

            visualizzaUtentiProrietaOpAccessi();

            passwordaccessi.setText("");
        }
    }//GEN-LAST:event_buttonInsOpeAccessiActionPerformed

    private void buttonCancOpeAccessiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancOpeAccessiActionPerformed
        if (tabopaccessi.getSelectedRow() >= 0) {

            int ris = JOptionPane.showConfirmDialog(this, "Confermi eliminazione utente selezionato?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (ris == JOptionPane.YES_OPTION) {
                String qrydelete = "DELETE FROM proprieta WHERE pprog = \'cruscottocespiti\' AND pval = \'" + (String) tabopaccessi.getValueAt(tabopaccessi.getSelectedRow(), 0) + "\'";
                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
                qd.esecuzione();

                qrydelete = "DELETE FROM proprieta WHERE pprog = \'cruscottocespiti\' AND pprop = \'" + (String) tabopaccessi.getValueAt(tabopaccessi.getSelectedRow(), 0) + "\'";
                QueryAggiornamentoClient qpwd = new QueryAggiornamentoClient(connAz, qrydelete);
                qpwd.esecuzione();

                JOptionPane.showMessageDialog(this, "Utente eliminato",
                        "", JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                visualizzaUtentiProrietaOpAccessi();
            }
        }
    }//GEN-LAST:event_buttonCancOpeAccessiActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JLabel13;
    private javax.swing.JLabel JLabel9;
    private ui.beans.PopButton buttonCancOpeAccessi;
    private ui.beans.PopButton buttonCancOpeGestCespiti;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private ui.beans.PopButton buttonInsOpeAccessi;
    private ui.beans.PopButton buttonInsOpeGestCespiti;
    private ui.beans.PopButton buttonSalvaGestioneAccessi;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel label_passwordaccessi;
    private javax.swing.JLabel label_passwordaccessi1;
    private ui.beans.ComboBoxEditable opeaccessi;
    private ui.beans.ComboBoxEditable opegestionicespiti;
    private javax.swing.JRadioButton opz_accessolibero;
    private javax.swing.JRadioButton opz_accessopassword;
    private javax.swing.JPasswordField passwordaccessi;
    private javax.swing.JPasswordField passwordaccessi1;
    private javax.swing.JPanel ppar;
    private javax.swing.JTable tabopaccessi;
    private javax.swing.JTable tabopgestListini;
    // End of variables declaration//GEN-END:variables

    public void visualizzaUtentiProrietaGestCespiti() {
        String qry = "SELECT * FROM proprieta WHERE pprog = \"cruscottocespiti\" AND (pprop = \"opecespiti\")";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();

        final String[] names
        = {
            COL_UTENTE
        };
        final Object[][] data = new Object[q.numeroRecord()][names.length];

        for (int i = 0; i < q.numeroRecord(); i++) {
            Record r = q.successivo();
            data[i][0] = r.leggiStringa("pval");
        }
        q.chiusura();

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tabopgestListini, COL_UTENTE2)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };

        tabopgestListini.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tabopgestListini, true);

        tabopgestListini.getColumn(COL_UTENTE).setPreferredWidth(400);
    }

    public void visualizzaUtentiProrietaOpAccessi() {

        ArrayList<Record> vproprieta = BusinessLogicImpostazioni.ricercaProprieta(connAz.conn, "cruscottocespiti", "opecespitiadmin", "");

        final String[] names
        = {
            COL_UTENTE2
        };
        final Object[][] data = new Object[vproprieta.size()][names.length];

        for (int i = 0; i < vproprieta.size(); i++) {
            Record r = vproprieta.get(i);
            data[i][0] = r.leggiStringa("pval");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                return false;
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };

        tabopaccessi.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tabopaccessi, true);

        tabopaccessi.getColumn(COL_UTENTE2).setPreferredWidth(400);
    }

    public boolean verificaDuplicatoUtente(String utente) {
       
        // utente nome dell'utente
        ArrayList<Record> vproprieta = BusinessLogicImpostazioni.ricercaProprieta(connAz.conn, "cruscottocespiti", "", utente);

        if (vproprieta.size() > 0) {
            JOptionPane.showMessageDialog(this, "Utente gia' esistente",
                    "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return true;
        }

        return false;

    }


    @Override
    public void impostaListener(PannelloPopListener listener) {
    
    }

}
