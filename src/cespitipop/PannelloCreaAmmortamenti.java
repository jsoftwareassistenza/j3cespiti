package cespitipop;

/*
 * JpanelCreaAmmortamenti.java
 *
 * Created on 9 novembre 2006, 11.37
 */
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.ConnessioneServer;
import nucleo.Data;
import nucleo.DialogExportSuFile;
import nucleo.DialogReportJR;
import nucleo.Edit;
import nucleo.EnvJazz;
import nucleo.FDouble;
import nucleo.Formattazione;
import nucleo.FunzioniDB;
import nucleo.FunzioniUtilita;
import nucleo.OpValute;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.PannelloTabellaSelezionaTuttiListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SelectAllHeader;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogic;
import nucleo.businesslogic.BusinessLogicAmbiente;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.businesslogic.archivibase.BusinessLogicArchiviBase;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;
import popcontabilita.dialogstart.DialogRicercaPnAmmortamenti;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.dialog.DialogExportSuFilePop;
import ui.eventi.GestoreEventiBeans;

/**
 *
 * @author svilupp
 */
public class PannelloCreaAmmortamenti extends javax.swing.JPanel implements PannelloPop, PannelloTabellaSelezionaTuttiListener {

    public Window padre;
    public Frame formGen;
    public ConnessioneServer connCom;
    public ConnessioneServer connAz;
    private boolean ric = false;
    private boolean dlg = false;
    private boolean azione = false;

    private int idVis;
    private int operazione;
    private int sel = 0;
    private int totSel = 0;

    public String azienda = "";
    private String esContabile = "";
    private String msgErrore = "";
    private String descrcau1 = "";
    private String descrcau2 = "";
    private String descrcau3 = "";
    private String descrcau4 = "";
    private String descrcau5 = "";
    private String dtCalcolo = "";
    private String annobilchiuso = "";
    private String esercizioScelto = "";

    private Record raz = new Record();

    private int[] id;
    private int[] idM;
    private int[] idCesp;
    private int[] gg;
    private boolean[] modif;
    private double[] fondoc;
    private double[] fondof;
    private String[] dtes = null;

    private int idCau1 = -1;
    private int idCau2 = -1;
    private int idCau3 = -1;
    private int idCau4 = -1;
    private int idCau5 = -1;
    private int idJcau1 = -1;
    private int idJcau2 = -1;
    private int idJcau3 = -1;
    private int idJcau4 = -1;
    private int idJcau5 = -1;
    private int idDoc1 = -1;
    private int idDoc2 = -1;
    private int idDoc3 = -1;
    private int idDoc4 = -1;
    private int idDoc5 = -1;
    private int idpdcF = -1;// pdc fondo
    private int idpdcA = -1;// pdc costo ammortamento
    private int idpdcR = -1;// riprese positive di bilancio
    private int idpdcC = -1;// riprese negative di bilancio
    private int puntoemdocid = -1;

    private Record rCat = new Record();

    private double immobattualedbl = 0;
    private double fondoattualedbl = 0;
    private WaitPop wait;

    public ArrayList<String> ves;
    private ArrayList<Record> vlog;
    private ArrayList<Record> vcateg;
    private ArrayList<Record> vcesp;

    ArrayList<Record> vmcsGru = new ArrayList();
    ArrayList<Record> vmcsSpec = new ArrayList();

    private boolean importiamm = false;
    private boolean elaboraStampaControlloCostoAmmortamenti = false;
    private boolean elaboraControlloFondoAmmortamentiCateg = false;

    SelectAllHeader select = null;
    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();
    private static final int BOOLEAN_COL = 0;

    //tabella categorie
    String COL_GRU = "C. Min.";
    String COL_COD = "Cod.";
    String COL_CAT = "Categoria";
    String COL_PC = "% C.";
    String COL_PF = "% F.";
    String COL_DETR = "% detraib.";
    String COL_AGE = "% agev.";

    String COL_DT = "Data";
    String COL_IMPC = "amm. Civ.";
    String COL_IMPF = "amm. Fis.";
    String COL_IMPD = "amm. Detr.";
    String COL_DEL = "Elim.";
    String COL_REG = "P.N.";
    String COL_PRINT = "St.";
    String COL_LEGA = "Lega";
    String COL_DOCUM = "Documento";

    //tabella calcolo
    String COL_SEL = "Sel.";
    String COL_COEFF = "% Civ.";
    String COL_BENE = "Valore bene";
    String COL_IMPAMM = "Amm. civ.";
    String COL_CRIT = "Criterio";
    String COL_PERIODO = "da-a";
    String COL_GIORNI = "N. gg";
    String COL_PFISC = "% Fisc.";
    String COL_FISCALE = "Imponib. Fisc.";
    String COL_AMMFISC = "Amm. fisc.";
    String COL_DEDUC = "Deducibile";
    String COL_DTACQ = "Anno acq.";
    String COL_DESCR = "Descrizione";
    //
    String COL_conto = "Conto";
    String COL_voce = "Voce ammortamenti";
    String COL_aciv = "Amm.civ";
    String COL_saldo = "Saldo";
    String COL_ripresa = "Riprese";
    String COL_categ = "Categoria";
    String COL_catciv = "Civ.";
    String COL_catfis = "Fis.";
    String COL_catdetr = "Detr.";
    String COL_diff = "Diff.";
    String COL_nota = "nota";

    String COL_catmin = "Cat. ministeriale";
    String COL_costo = "C.to amm.";
    String COL_catpre = "Anno P.";
    String COL_catamm = "Amm.nto";
    String COL_catvar = "Variaz.";
    String COL_catsva = "Svalut.";
    String COL_tot = "Totale";

    String COL_CESPITI = "Numero cespiti";

    private HashMap<Integer, Double> totali = new HashMap<Integer, Double>();

    /**
     * Creates new form JpanelCreaAmmortamenti
     */
    public PannelloCreaAmmortamenti() {

        initComponents();
        nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public PannelloCreaAmmortamenti(Window padre, Frame formGen) {
        this();
        this.padre = padre;
        this.formGen = formGen;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        editpercciv = new javax.swing.JTextField();
        documentoValuta1 = new halleytech.models.DocumentoValuta();
        editpercfis = new javax.swing.JTextField();
        documentoValuta2 = new halleytech.models.DocumentoValuta();
        editutilizzo = new javax.swing.JTextField();
        documentoNumero1 = new halleytech.models.DocumentoNumero();
        jPanel1 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelCateg = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jComboBeni = new javax.swing.JComboBox();
        jLabel53 = new javax.swing.JLabel();
        jComboEsercizi = new javax.swing.JComboBox<>();
        buttonRICALCOLA = new ui.beans.PopButton();
        comboGruppi = new javax.swing.JComboBox();
        comboSpecie = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabellaRisultato = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        buttonStampa = new ui.beans.PopButton();
        jPanelFondo = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabellaCosti = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jComboBoxfondo = new javax.swing.JComboBox<>();
        jPanelCespiti = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        descrcategoria = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        dataregamm = new ui.beans.CampoDataPop();
        buttonCalcola = new ui.beans.PopButton();
        quotaamm = new ui.beans.CampoValutaPop();
        jLabel13 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        anchefiscale = new javax.swing.JCheckBox();
        jLabel20 = new javax.swing.JLabel();
        note = new ui.beans.CampoTestoPop();
        ggamm = new ui.beans.CampoInteroPop();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabellaAmmo = new javax.swing.JTable();
        jPanel5 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        scostamento = new javax.swing.JLabel();
        buttonReg = new ui.beans.PopButton();
        jLabel15 = new javax.swing.JLabel();
        totaleciv = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        totalefis = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        totalededuc = new javax.swing.JLabel();
        buttonEsportaE = new ui.beans.PopButton();

        editpercciv.setDocument(documentoValuta1);
        editpercciv.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        editpercciv.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        editpercciv.setNextFocusableComponent(tabellaAmmo);
        editpercciv.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                editperccivFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                editperccivFocusLost(evt);
            }
        });

        documentoValuta1.scriviInteri(3);

        editpercfis.setDocument(documentoValuta2);
        editpercfis.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        editpercfis.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        editpercfis.setNextFocusableComponent(tabellaAmmo);
        editpercfis.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                editpercfisFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                editpercfisFocusLost(evt);
            }
        });

        documentoValuta2.scriviInteri(3);

        editutilizzo.setDocument(documentoNumero1);
        editutilizzo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        editutilizzo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        editutilizzo.setNextFocusableComponent(tabellaAmmo);
        editutilizzo.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                editutilizzoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                editutilizzoFocusLost(evt);
            }
        });

        setLayout(new java.awt.BorderLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setPreferredSize(new java.awt.Dimension(400, 4));
        jPanel1.setLayout(null);
        add(jPanel1, java.awt.BorderLayout.NORTH);

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N

        jPanelCateg.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelCateg.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanelCateg.setPreferredSize(new java.awt.Dimension(456, 380));
        jPanelCateg.setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel2.setPreferredSize(new java.awt.Dimension(100, 32));
        jPanel2.setLayout(null);

        jComboBeni.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jComboBeni.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Materiali", "Immateriali" }));
        jComboBeni.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBeniItemStateChanged(evt);
            }
        });
        jComboBeni.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBeniActionPerformed(evt);
            }
        });
        jPanel2.add(jComboBeni);
        jComboBeni.setBounds(0, 0, 200, 32);

        jLabel53.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel53.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel53.setText("Esercizi elaborati");
        jPanel2.add(jLabel53);
        jLabel53.setBounds(628, 8, 128, 20);

        jComboEsercizi.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jComboEsercizi.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboEserciziItemStateChanged(evt);
            }
        });
        jComboEsercizi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboEserciziActionPerformed(evt);
            }
        });
        jPanel2.add(jComboEsercizi);
        jComboEsercizi.setBounds(768, 0, 252, 32);

        buttonRICALCOLA.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/baseline_call_merge_white_24dp.png"))); // NOI18N
        buttonRICALCOLA.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonRICALCOLA.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonRICALCOLA.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonRICALCOLA.setToolTipText("Ricalcola totale ammortamento per utte le categorie effettuato nell' intero esercizio");
        buttonRICALCOLA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRICALCOLAActionPerformed(evt);
            }
        });
        jPanel2.add(buttonRICALCOLA);
        buttonRICALCOLA.setBounds(1032, 4, 56, 24);

        comboGruppi.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboGruppi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "< ....gruppo.... >" }));
        comboGruppi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGruppiActionPerformed(evt);
            }
        });
        jPanel2.add(comboGruppi);
        comboGruppi.setBounds(216, 0, 200, 32);

        comboSpecie.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboSpecie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "< ....specie.... >" }));
        comboSpecie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSpecieActionPerformed(evt);
            }
        });
        jPanel2.add(comboSpecie);
        comboSpecie.setBounds(432, 0, 200, 32);

        jPanelCateg.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jScrollPane1.setPreferredSize(new java.awt.Dimension(470, 500));

        tabellaRisultato.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabellaRisultato.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tabellaRisultato.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabellaRisultatoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabellaRisultato);

        jPanelCateg.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setPreferredSize(new java.awt.Dimension(100, 44));
        jPanel4.setLayout(null);

        buttonStampa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_print_white_24dp.png"))); // NOI18N
        buttonStampa.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonStampa.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonStampa.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonStampa.setToolTipText("Riepilogo ammortamenti per specie");
        buttonStampa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonStampaActionPerformed(evt);
            }
        });
        jPanel4.add(buttonStampa);
        buttonStampa.setBounds(16, 0, 64, 40);

        jPanelCateg.add(jPanel4, java.awt.BorderLayout.PAGE_END);

        jTabbedPane1.addTab("Dettaglio", jPanelCateg);

        jPanelFondo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanelFondo.setLayout(new java.awt.BorderLayout());

        tabellaCosti.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabellaCosti.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane3.setViewportView(tabellaCosti);

        jPanelFondo.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setPreferredSize(new java.awt.Dimension(100, 32));
        jPanel6.setLayout(new javax.swing.BoxLayout(jPanel6, javax.swing.BoxLayout.LINE_AXIS));

        jComboBoxfondo.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jComboBoxfondo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Costi di ammortamento", "Costi di ammortamento riepilogati per categoria ministeriale", "Fondi di ammortamento", "Fondi di ammortamento riepilogati per categoria ministeriale", "Fondi di svalutazione", "Fondi di svalutazione riepilogati per categoria ministeriale" }));
        jComboBoxfondo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBoxfondoItemStateChanged(evt);
            }
        });
        jComboBoxfondo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxfondoActionPerformed(evt);
            }
        });
        jPanel6.add(jComboBoxfondo);

        jPanelFondo.add(jPanel6, java.awt.BorderLayout.PAGE_START);

        jTabbedPane1.addTab("Fondo", jPanelFondo);

        jPanelCespiti.setBackground(new java.awt.Color(255, 255, 255));
        jPanelCespiti.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanelCespiti.setMinimumSize(new java.awt.Dimension(600, 30));
        jPanelCespiti.setPreferredSize(new java.awt.Dimension(600, 40));
        jPanelCespiti.setLayout(new java.awt.BorderLayout());

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setPreferredSize(new java.awt.Dimension(100, 75));
        jPanel3.setLayout(null);

        descrcategoria.setBackground(new java.awt.Color(202, 236, 221));
        descrcategoria.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrcategoria.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrcategoria.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrcategoria.setOpaque(true);
        jPanel3.add(descrcategoria);
        descrcategoria.setBounds(4, 4, 468, 40);

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Note ammortamento");
        jPanel3.add(jLabel12);
        jLabel12.setBounds(710, 40, 120, 24);
        jPanel3.add(dataregamm);
        dataregamm.setBounds(970, 4, 120, 24);

        buttonCalcola.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_create_white_24dp.png"))); // NOI18N
        buttonCalcola.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonCalcola.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonCalcola.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonCalcola.setToolTipText("Calcola ammortamenti per categoria");
        buttonCalcola.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCalcolaActionPerformed(evt);
            }
        });
        jPanel3.add(buttonCalcola);
        buttonCalcola.setBounds(1108, 4, 88, 24);

        quotaamm.setHintDim(new java.awt.Font("Default", 0, 14)); // NOI18N
        quotaamm.setInteri(3);
        jPanel3.add(quotaamm);
        quotaamm.setBounds(600, 4, 60, 24);

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Data reg. ammort.");
        jPanel3.add(jLabel13);
        jLabel13.setBounds(840, 4, 124, 24);

        jLabel19.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Nuova perc.");
        jPanel3.add(jLabel19);
        jLabel19.setBounds(490, 4, 90, 24);

        anchefiscale.setBackground(new java.awt.Color(255, 255, 255));
        anchefiscale.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        anchefiscale.setLabel("Anche fiscale");
        jPanel3.add(anchefiscale);
        anchefiscale.setBounds(596, 40, 100, 25);

        jLabel20.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel20.setText("Nuovi gg");
        jPanel3.add(jLabel20);
        jLabel20.setBounds(675, 4, 90, 24);

        note.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.add(note);
        note.setBounds(840, 40, 360, 24);
        jPanel3.add(ggamm);
        ggamm.setBounds(770, 4, 60, 24);

        jPanelCespiti.add(jPanel3, java.awt.BorderLayout.PAGE_START);

        tabellaAmmo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tabellaAmmo.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tabellaAmmo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabellaAmmoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabellaAmmo);

        jPanelCespiti.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setPreferredSize(new java.awt.Dimension(100, 44));
        jPanel5.setLayout(null);

        jLabel14.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Ripresa");
        jPanel5.add(jLabel14);
        jLabel14.setBounds(980, 12, 84, 24);

        scostamento.setBackground(new java.awt.Color(202, 236, 221));
        scostamento.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        scostamento.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        scostamento.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        scostamento.setOpaque(true);
        jPanel5.add(scostamento);
        scostamento.setBounds(1072, 12, 96, 24);

        buttonReg.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_save_white_24dp.png"))); // NOI18N
        buttonReg.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonReg.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonReg.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonReg.setToolTipText("Calcola ammortamenti per categoria");
        buttonReg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonRegActionPerformed(evt);
            }
        });
        jPanel5.add(buttonReg);
        buttonReg.setBounds(20, -10, 64, 36);

        jLabel15.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Totale civ.");
        jPanel5.add(jLabel15);
        jLabel15.setBounds(436, 12, 76, 24);

        totaleciv.setBackground(new java.awt.Color(202, 236, 221));
        totaleciv.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totaleciv.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totaleciv.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totaleciv.setOpaque(true);
        jPanel5.add(totaleciv);
        totaleciv.setBounds(516, 12, 96, 24);

        jLabel16.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Totale Fisc.");
        jPanel5.add(jLabel16);
        jLabel16.setBounds(612, 12, 84, 24);

        totalefis.setBackground(new java.awt.Color(202, 236, 221));
        totalefis.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totalefis.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totalefis.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totalefis.setOpaque(true);
        jPanel5.add(totalefis);
        totalefis.setBounds(700, 12, 96, 24);

        jLabel17.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Deducibile");
        jPanel5.add(jLabel17);
        jLabel17.setBounds(796, 12, 84, 24);

        totalededuc.setBackground(new java.awt.Color(202, 236, 221));
        totalededuc.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        totalededuc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        totalededuc.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        totalededuc.setOpaque(true);
        jPanel5.add(totalededuc);
        totalededuc.setBounds(884, 12, 96, 24);

        buttonEsportaE.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_open_in_new_white_24dp.png"))); // NOI18N
        buttonEsportaE.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonEsportaE.setText("Esporta");
        buttonEsportaE.setToolTipText("");
        buttonEsportaE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEsportaEActionPerformed(evt);
            }
        });
        jPanel5.add(buttonEsportaE);
        buttonEsportaE.setBounds(96, 4, 120, 36);

        jPanelCespiti.add(jPanel5, java.awt.BorderLayout.PAGE_END);

        jTabbedPane1.addTab("Categorie", jPanelCespiti);

        add(jTabbedPane1, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void tabellaRisultatoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabellaRisultatoMouseClicked
        if (evt.getClickCount() > 0 && tabellaRisultato.getSelectedRow() >= 0
                && (int) tabellaRisultato.getValueAt(tabellaRisultato.getSelectedRow(), FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_CESPITI)) != 0) {
            String querycat = "SELECT * FROM categoriebs WHERE catbsid = " + id[tabellaRisultato.getSelectedRow()];
            QueryInterrogazioneClient qcat = new QueryInterrogazioneClient(connAz, querycat);
            qcat.apertura();
            rCat = qcat.successivo();
            qcat.chiusura();

            if (rCat.leggiIntero("catbscostopdcid") < 1) {
                JOptionPane.showMessageDialog(this, "Assente CONTO AMMORTAMENTI su categoria", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                idpdcA = -1;
            } else {
                idpdcA = rCat.leggiIntero("catbscostopdcid");
            }

            if (rCat.leggiIntero("catbsfondopdcid") < 1) {
                JOptionPane.showMessageDialog(this, "Assente CONTO FONDO AMMORTAMENTI su categoria", "",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                idpdcF = -1;
            } else {
                idpdcF = rCat.leggiIntero("catbsfondopdcid");
            }
            if (idpdcA > 0 && idpdcF > 0 && rCat.leggiIntero("catbsimmpdcid") > 0) {
//                controlloContabilita();
                buttonCalcola.setEnabled(true);
                buttonReg.setEnabled(true);
                buttonStampa.setEnabled(true);
            } else {
                buttonCalcola.setEnabled(false);
                buttonReg.setEnabled(false);
                buttonStampa.setEnabled(false);
            }

            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=\"5.1\"";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            if (r != null) {
                idpdcC = r.leggiIntero("pdcceejazzid");
            }
            query.chiusura();

            qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=\"5.2\"";
            query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            r = query.successivo();
            if (r != null) {
                idpdcR = r.leggiIntero("pdcceejazzid");
            }
            query.chiusura();
        } else {
            JOptionPane.showMessageDialog(this, "In questa categoria non ci sono cespiti", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));

        }

        if (evt.getClickCount() > 1 && tabellaRisultato.getSelectedRow() >= 0) {
            tabellaAmmo.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{}));

            if (((String) tabellaRisultato.getValueAt(tabellaRisultato.getSelectedRow(), FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT))).equals("")
                    || ((String) tabellaRisultato.getValueAt(tabellaRisultato.getSelectedRow(), FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT))).equals("00/00/0000")) {
                dtCalcolo = "";
            } else {
                String dt = (String) tabellaRisultato.getValueAt(tabellaRisultato.getSelectedRow(), FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT));
                dtCalcolo = (new Data(dt, Data.GG_MM_AAAA)).formatta(Data.AAAA_MM_GG, "-");
            }

            descrcategoria.setText((String) tabellaRisultato.getValueAt(tabellaRisultato.getSelectedRow(), FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_CAT)));

//            double[] immob = BusinessLogicCespiti.calcoloImmobilizzazioniCategoria(connAz, azienda, id[tabellaRisultato.getSelectedRow()], esContabile, "");
//            double[] fondo = BusinessLogicCespiti.calcoloFondoCategoria(connAz, azienda, id[tabellaRisultato.getSelectedRow()], esContabile, "");
//            immobattuale.setText("" + new FDouble(immob[1], "###,###,##0.00", 0));
//            immobattualedbl = immob[1];
//            fondoattuale.setText("" + new FDouble(fondo[1], "###,###,##0.00", 0));
//            fondoattualedbl = fondo[1];            
            if (idM[tabellaRisultato.getSelectedRow()] > 0) {
                JOptionPane.showMessageDialog(this, "Categoria gia' elaborata e registrata in prima nota", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                buttonCalcola.setEnabled(false);
                buttonReg.setEnabled(false);
                mostraAmmortamenti();
            } else if (idM[tabellaRisultato.getSelectedRow()] < 1
                    && !dtCalcolo.equals("")) {
                JOptionPane.showMessageDialog(this, "Categoria gia' elaborata in prova", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                buttonCalcola.setEnabled(false);
                buttonReg.setEnabled(false);
                mostraAmmortamenti();
            } else {
                buttonCalcola.setEnabled(true);
                buttonReg.setEnabled(true);
            }
            jTabbedPane1.setSelectedIndex(2);
        } else {
            immobattualedbl = 0;
            fondoattualedbl = 0;
        }
    }//GEN-LAST:event_tabellaRisultatoMouseClicked

    private void jComboBeniItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBeniItemStateChanged

    }//GEN-LAST:event_jComboBeniItemStateChanged

    private void controlloContabilita() {
        double saldoCont = 0;
        String conto = "";

        //CALCOLO VALORE IMMOBILIZZAZIONE
        String query3 = "SELECT SUM(mpndare) AS dare,SUM(mpnavere) AS avere,pdcmastro,pdcconto,pdcsottoconto FROM primanota" + esContabile
                + " LEFT JOIN pianoconti ON primanota" + esContabile + ".mpnpdcid = pianoconti.pdcid"
                + " LEFT JOIN caucon ON primanota" + esContabile + ".mpncaucont = caucon.cauconid"
                + " WHERE pdcid = " + rCat.leggiIntero("catbsimmpdcid") + " AND caucongest <> 7";
        QueryInterrogazioneClient q3 = new QueryInterrogazioneClient(connAz, query3);
        q3.apertura();
        Record r3 = q3.successivo();
        if (r3 != null) {
            saldoCont = r3.leggiDouble("dare") - r3.leggiDouble("avere");
            conto = r3.leggiStringa("pdcmastro") + "." + r3.leggiStringa("pdcconto") + "." + r3.leggiStringa("pdcsottoconto");
        }
        q3.chiusura();

        //controllo immobilizzazione
        if (saldoCont != immobattualedbl) {
            JOptionPane.showMessageDialog(this, "ATTENZIONE! il saldo contabile IMMOBILIZZAZIONE non corrisponde al totale categoria sulla procedura cespiti.\n"
                    + " CONTO:" + conto + " SALDO CONTABILE: " + saldoCont + "; TOTALE CATEGORIA: " + immobattualedbl, "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        }
        //--------------   FONDO
        saldoCont = 0;
        query3 = "SELECT SUM(mpndare) AS dare,SUM(mpnavere) AS avere,pdcmastro,pdcconto,pdcsottoconto FROM primanota" + esContabile
                + " LEFT JOIN pianoconti ON primanota" + esContabile + ".mpnpdcid = pianoconti.pdcid"
                + " LEFT JOIN caucon ON primanota" + esContabile + ".mpncaucont = caucon.cauconid"
                + " WHERE pdcid = " + rCat.leggiIntero("catbsfondopdcid")
                + " AND caucongest <> 7";
        q3 = new QueryInterrogazioneClient(connAz, query3);
        q3.apertura();
        r3 = q3.successivo();
        if (r3 != null) {
            saldoCont = r3.leggiDouble("avere") - r3.leggiDouble("dare");
            conto = r3.leggiStringa("pdcmastro") + "." + r3.leggiStringa("pdcconto") + "." + r3.leggiStringa("pdcsottoconto");
        }
        q3.chiusura();

        if (saldoCont != fondoattualedbl) {
            JOptionPane.showMessageDialog(this, "ATTENZIONE! il saldo contabile del FONDO non corrisponde al totale categoria sulla procedura cespiti.\n CONTO:" + conto + " SALDO CONTABILE: " + saldoCont + "; TOTALE CATEGORIA: " + fondoattualedbl, "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        }

    }
    private void editpercfisFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editpercfisFocusGained
        Edit.selComponenteValuta(editpercfis);
}//GEN-LAST:event_editpercfisFocusGained

    private void editpercfisFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editpercfisFocusLost
        Edit.deselComponenteValuta(editpercfis);

        int j = tabellaAmmo.getSelectedRow();
        if (!modif[j]) {
            JOptionPane.showMessageDialog(this, "Campo non modificabile", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
    }//GEN-LAST:event_editpercfisFocusLost

    private void editutilizzoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editutilizzoFocusGained
        Edit.selComponenteValuta(editutilizzo);
    }//GEN-LAST:event_editutilizzoFocusGained

    private void editutilizzoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editutilizzoFocusLost
        Edit.deselComponenteValuta(editutilizzo);

        int j = tabellaAmmo.getSelectedRow();
        if (!modif[j]) {
            JOptionPane.showMessageDialog(this, "Campo non modificabile", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
    }//GEN-LAST:event_editutilizzoFocusLost

    private void jComboBeniActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBeniActionPerformed
        jTabbedPane1.setSelectedIndex(0);
        caricaCategorie("", "");
    }//GEN-LAST:event_jComboBeniActionPerformed

    private void buttonCalcolaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCalcolaActionPerformed

        jLabel19.setVisible(false);
        jLabel20.setVisible(false);
        jLabel12.setVisible(false);
        anchefiscale.setVisible(false);
        note.setVisible(false);
        quotaamm.setVisible(false);
        quotaamm.setText("");
        jLabel12.setVisible(false);
        ggamm.setVisible(false);
        ggamm.setText("");

        if (dataregamm.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "data Registrazione obbligatoria", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (dataregamm.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "data Registrazione obbligatoria", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        String dtelab = "";
        if (!dataregamm.getDataStringa().equals(dtes[1])) {
            dtelab = dataregamm.getDataStringa();
        } else {
            dtelab = dtes[1];
        }

        ves = BusinessLogicAmbiente.leggiEserciziPeriodo(EnvJazz.connAmbiente.leggiConnessione(), azienda, dtelab, dtelab);
        String esmov = ves.get(0);
        if (esContabile.compareTo(esmov) != 0) {
            JOptionPane.showMessageDialog(this, "La data non corrisponde all'esercizio contabile", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
        //
        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        //
        CalcolaAmmortamenti(dtelab);
        //
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        JOptionPane.showMessageDialog(this, "Calcolo AMMORTAMENTI terminato", "",
                JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

        buttonReg.setEnabled(true);
    }//GEN-LAST:event_buttonCalcolaActionPerformed

    private void buttonRegActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRegActionPerformed
        int aap = Formattazione.estraiIntero(annobilchiuso);
        int aaa = Formattazione.estraiIntero((dataregamm.getText().trim()).substring(6, 10));

        if (aap + 1 != aaa) {
            JOptionPane.showMessageDialog(this, "Esercizio non progressivo all'ultimo anno di chiusura", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));

            return;
        }

        if (dataregamm.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this, "data Registrazione obbligatoria", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        if (tabellaAmmo.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "Elabora prima la categoria", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }

        String dtelab = "";

        if (!dataregamm.getDataStringa().equals(dtes[1])) {
            dtelab = new Data(dataregamm.getText().trim(), Data.GG_MM_AAAA).formatta(Data.AAAA_MM_GG, "-");
        } else {
            dtelab = dtes[1];
            dataregamm.setText(new Data(dtelab, Data.AAAA_MM_GG).formatta(Data.GG_MM_AAAA, "/"));
        }
        int ris = JOptionPane.showConfirmDialog(this, "Confermi la REGISTRAZIONE degli ammortamenti ?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            this.setCursor(new Cursor(Cursor.WAIT_CURSOR));

            eliminaTutteSimulazioni();
            registrazioneAmmortamenti();

            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            JOptionPane.showMessageDialog(this, "Registrazione AMMORTAMENTI terminata", "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

            buttonReg.setEnabled(false);
            //
            jTabbedPane1.setSelectedIndex(0);
            caricaCategorie("", "");
        }

    }//GEN-LAST:event_buttonRegActionPerformed

    private void buttonStampaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonStampaActionPerformed
        if (jComboEsercizi.getSelectedIndex() > 0) {
            Record rlog = vlog.get(jComboEsercizi.getSelectedIndex() - 1);
            dtCalcolo = rlog.leggiStringa("logamm_data");
        } else {
            dtCalcolo = dataregamm.getDataStringa();
        }
        HashMap par = new HashMap();
        par.put("azienda", azienda + ": " + raz.leggiStringa("nome") + "  P.Iva " + raz.leggiStringa("partitaiva"));
        par.put("escontabile", esercizioScelto);
        par.put("datastampa", (new Data().formatta(Data.AAAA_MM_GG, "-")));
        par.put("dataelab", dtCalcolo);

        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        //
        BusinessLogicCespiti.elaboraStampaAmmortamenti(connAz.conn, azienda, esercizioScelto, dtCalcolo, (int) -1, (int) 0);

        if (!FunzioniDB.esisteReport("logammDettStampanuova.jasper")) {
            JOptionPane.showMessageDialog(this,
                    "Report logammDettStampanuova.jasper non trovato", "Errore", JOptionPane.ERROR_MESSAGE);
            return;
        }
        FunzioniDB.leggiReport("logammDettStampanuova.jasper");

        DialogReportJR dr = new DialogReportJR(formGen, EnvJazz.crERW, "logammDettStampanuova.jasper", par);
        UtilForm.posRelSchermo(dr, UtilForm.CENTRO);
        dr.show();
//        
//        rep = "logammContStampa.jasper";
//        dr = new DialogReportJrNew(formGen, connAz.conn, rep, par);
//        UtilForm.posRelSchermo(dr, UtilForm.CENTRO);
//        dr.show();

        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }//GEN-LAST:event_buttonStampaActionPerformed

    private void editperccivFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editperccivFocusGained
        Edit.selComponenteValuta(editpercciv);
    }//GEN-LAST:event_editperccivFocusGained

    private void editperccivFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editperccivFocusLost
        Edit.deselComponenteValuta(editpercciv);

        int j = tabellaAmmo.getSelectedRow();
        if (!modif[j]) {
            JOptionPane.showMessageDialog(this, "Campo non modificabile", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        }
    }//GEN-LAST:event_editperccivFocusLost

    private void jComboEserciziItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboEserciziItemStateChanged

    }//GEN-LAST:event_jComboEserciziItemStateChanged

    private void jComboBoxfondoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBoxfondoItemStateChanged

    }//GEN-LAST:event_jComboBoxfondoItemStateChanged

    private void tabellaAmmoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabellaAmmoMouseClicked
        if (evt.getClickCount() == 1) {
            sel = 0;
            for (int i = 0; i < tabellaAmmo.getRowCount(); i++) {
                if ((Boolean) tabellaAmmo.getValueAt(i, 0)) {
                    sel++;
                }
            }
            totSel = sel;

            if (sel >= 1) {
                jLabel19.setVisible(true);
                jLabel20.setVisible(true);
                jLabel12.setVisible(true);
                anchefiscale.setVisible(true);
                note.setVisible(true);
                quotaamm.setVisible(true);
                jLabel12.setVisible(true);
                ggamm.setVisible(true);
            } else {
                jLabel19.setVisible(false);
                jLabel20.setVisible(false);
                jLabel12.setVisible(false);
                anchefiscale.setVisible(false);
                note.setVisible(false);
                quotaamm.setVisible(false);
                quotaamm.setText("");
                jLabel12.setVisible(false);
                ggamm.setVisible(false);
                ggamm.setText("");
            }
        }
    }//GEN-LAST:event_tabellaAmmoMouseClicked

    private void jComboEserciziActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboEserciziActionPerformed
        jTabbedPane1.setEnabledAt(1, false);
        if (jComboEsercizi.getSelectedIndex() > 0) {
            Record rlog = vlog.get(jComboEsercizi.getSelectedIndex() - 1);
            esercizioScelto = rlog.leggiStringa("logamm_eserc");
            buttonCalcola.setVisible(false);
            buttonReg.setVisible(false);
            if (!vcateg.isEmpty()) {
                jTabbedPane1.setEnabledAt(1, true);
                caricaCategorie("", "");
                caricaValoriContabiliCosto();
            }
        } else {
            esercizioScelto = esContabile;
            buttonCalcola.setVisible(true);
            buttonReg.setVisible(true);
            caricaCategorie("", "");
        }
        elaboraStampaControlloCostoAmmortamenti = false;
        elaboraControlloFondoAmmortamentiCateg = false;
    }//GEN-LAST:event_jComboEserciziActionPerformed

    private void jComboBoxfondoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxfondoActionPerformed
        switch (jComboBoxfondo.getSelectedIndex()) {
            case 0:
                caricaValoriContabiliCosto();
                break;
            case 1:
                caricaValoriContabiliCostoRiep();
                break;
            case 2:
                caricaValoriContabiliFondo();
                break;
            case 3:
                caricaValoriContabiliFondoRiep();
                break;
            case 4:
                break;
            default:
                break;
        }
    }//GEN-LAST:event_jComboBoxfondoActionPerformed

    private void buttonEsportaEActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEsportaEActionPerformed
        int risatt = FunzioniDB.controllaAttivazionePlugin(EnvJazz.azienda, "EXPORTTAB");
        if (risatt == EnvJazz.FALLIMENTO) {
            JOptionPane.showMessageDialog(this, "Chiave attivazione per plugin EXPORT TABELLE mancante o non valida", "Errore",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        } else {
            if (tabellaAmmo.getRowCount() < 1) {
                JOptionPane.showMessageDialog(this, "Prima esegui interrogazione", "Errore",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            }

            DialogExportSuFilePop d = new DialogExportSuFilePop((Frame) formGen, true, connCom);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.show();
            if (d.leggiScelta() == DialogExportSuFile.OK) {
                String qual = "";
                if (d.leggiQualificatore() == DialogExportSuFile.DOPPIOAPICE) {
                    qual = "\"";
                } else {
                    qual = "\'";
                }
                switch (d.leggiTipoExport()) {
                    case DialogExportSuFile.TCSV:
                        FunzioniUtilita.esportaJtableCsv(tabellaAmmo, d.leggiFile(), d.leggiSeparatore(), qual, true);
                        break;
                    case DialogExportSuFile.THTML:
                        FunzioniUtilita.esportaJtableHtml(tabellaAmmo, d.leggiFile(), true);
                        break;
                    default:
                        FunzioniUtilita.esportaJtableXlsXml_2003(tabellaAmmo, d.leggiFile(), true);
                        break;
                }
            }
            JOptionPane.showMessageDialog(this, "Esportazione effettuata", "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        }
        this.requestFocus();
    }//GEN-LAST:event_buttonEsportaEActionPerformed

    private void buttonRICALCOLAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonRICALCOLAActionPerformed

        ArrayList<Record> esercizivuoti = new ArrayList();
        if (jComboEsercizi.getSelectedIndex() == 0) {
            int ris = JOptionPane.showConfirmDialog(this, "Vuoi il ricalcolo degli esercizi ASSENTI ?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (ris == JOptionPane.NO_OPTION) {
                return;
            }
            String query = "SELECT DISTINCT mbseserc FROM movimentibs ORDER BY mbseserc DESC";
            QueryInterrogazioneClient qlog = new QueryInterrogazioneClient(connAz, query);
            qlog.apertura();
            Record rc = qlog.successivo();
            while (rc != null) {
                Record rlog = new Record();
                String qrya = "SELECT logamm_eserc FROM log_amm WHERE logamm_eserc = \"" + rc.leggiStringa("mbseserc") + "\" LIMIT 01";
                QueryInterrogazioneClient q1 = new QueryInterrogazioneClient(connAz, qrya);
                q1.apertura();
                if (q1.numeroRecord() == 0) {
                    rlog.insElem("anno", rc.leggiStringa("mbseserc"));
                    esercizivuoti.add(rlog);
                }
                q1.chiusura();
                rc = qlog.successivo();
            }
            qlog.chiusura();

        } else {
            int riss = JOptionPane.showConfirmDialog(this, "Attenzione il ricalcolo " + esercizioScelto + " puo' impiegare del tempo",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (riss == JOptionPane.NO_OPTION) {
                return;
            }
            Record rlog = new Record();
            rlog.insElem("anno", esercizioScelto);
            esercizivuoti.add(rlog);
        }
        //crea nuova tabella totali ammortamento
        if (BusinessLogic.esisteTabella(connAz.conn, "log_amm2")) {
            String drop = "DROP TABLE log_amm2";
            QueryAggiornamentoClient querydrop = new QueryAggiornamentoClient(connAz, drop);
            querydrop.esecuzione();
        }
        String qry = "CREATE TABLE log_amm2"
                + " SELECT catbsid,mbstipocf,mbsdata,SUM(mbsammortamento) AS ammort,SUM(mbsammdeducib) AS deduc,SUM(mbsammindeducib) AS nondeduc"
                + " FROM movimentibs JOIN cespiti ON mbscespid = cespid JOIN categoriebs ON cespcategid = catbsid"
                + " GROUP BY catbsid,mbstipocf,mbsdata"
                + " ORDER BY catbsid,mbstipocf,mbsdata";
        QueryAggiornamentoClient querycrea = new QueryAggiornamentoClient(connAz, qry);
        querycrea.esecuzione();

        for (int a = 0; a < esercizivuoti.size(); a++) {
            Record rec = esercizivuoti.get(a);
            esercizioScelto = rec.leggiStringa("anno");
            //inserisce righe vuote categoria,data per esercizio scelto
            if (BusinessLogic.esisteTabella(connAz.conn, "log_amm1")) {
                String drop = "DROP TABLE log_amm1";
                QueryAggiornamentoClient querydrop = new QueryAggiornamentoClient(connAz, drop);
                querydrop.esecuzione();
            }
            qry = "CREATE TABLE log_amm1"
                    + " SELECT * from log_amm"
                    + " WHERE logamm_eserc = \"" + esercizioScelto + "\"";
            querycrea = new QueryAggiornamentoClient(connAz, qry);
            querycrea.esecuzione();

            String qrydrop = "DELETE FROM log_amm"
                    + " WHERE logamm_eserc = \"" + esercizioScelto + "\"";
            QueryAggiornamentoClient queryagg = new QueryAggiornamentoClient(connAz, qrydrop);
            queryagg.esecuzione();

            qry = "INSERT INTO log_amm(logamm_eserc,logamm_catid,logamm_data,logamm_fisc,logamm_civ,logamm_chiuso,logamm_importoc,logamm_importof,logamm_importod,logamm_movjazzid,logamm_movdoccg)"
                    + "(SELECT CONCAT(SUBSTRING(mbsdata,1,4),SUBSTRING(mbsdata,1,4)),catbsid,mbsdata,0,0,0,0.00,0.00,0.00,-1,\"\"  FROM movimentibs"
                    + " JOIN cespiti ON mbscespid = cespid JOIN categoriebs ON cespcategid = catbsid"
                    + " WHERE mbseserc = \"" + esercizioScelto + "\" and mbstipomov=2"
                    + " GROUP BY catbsid,mbsdata"
                    + " ORDER BY catbsid,mbsdata)";
            QueryAggiornamentoClient queryins = new QueryAggiornamentoClient(connAz, qry);
            queryins.esecuzione();
            //aggiorna i totali
            qry = "SELECT logamm_catid,logamm_data,logamm_id FROM log_amm WHERE logamm_eserc = \"" + esercizioScelto + "\"";
            QueryInterrogazioneClient q1 = new QueryInterrogazioneClient(connAz, qry);
            q1.apertura();
            if (q1.numeroRecord() > 0) {
                for (int i = 0; i < q1.numeroRecord(); i++) {
                    Record rup = q1.successivo();
                    String qryup = "SELECT ammort FROM log_amm2 WHERE catbsid = " + rup.leggiIntero("logamm_catid")
                            + " AND mbsdata = \"" + rup.leggiStringa("logamm_data") + "\""
                            + " AND mbstipocf = 2";
                    QueryInterrogazioneClient q2 = new QueryInterrogazioneClient(connAz, qryup);
                    q2.apertura();
                    Record rciv = q2.successivo();
                    q2.chiusura();
                    if (rciv != null) {
                        QueryAggiornamentoClient queryup = new QueryAggiornamentoClient(connAz, "UPDATE log_amm SET logamm_civ = 1,logamm_importoc = " + rciv.leggiDouble("ammort")
                                + " WHERE logamm_id = " + rup.leggiIntero("logamm_id"));
                        queryup.esecuzione();
                    }
                    qryup = "SELECT deduc,nondeduc FROM log_amm2 WHERE catbsid = " + rup.leggiIntero("logamm_catid")
                            + " AND mbsdata = \"" + rup.leggiStringa("logamm_data") + "\""
                            + " AND mbstipocf = 1";
                    q2 = new QueryInterrogazioneClient(connAz, qryup);
                    q2.apertura();
                    Record rfis = q2.successivo();
                    q2.chiusura();
                    if (rfis != null) {
                        QueryAggiornamentoClient queryup = new QueryAggiornamentoClient(connAz, "UPDATE log_amm SET logamm_fisc = 1,logamm_chiuso = 1,"
                                + "logamm_importof = " + rfis.leggiDouble("deduc") + ","
                                + "logamm_importod = " + rfis.leggiDouble("nondeduc")
                                + " WHERE logamm_id = " + rup.leggiIntero("logamm_id"));
                        queryup.esecuzione();
                    }
                    qryup = "SELECT logamm_movjazzid,logamm_movdoccg FROM log_amm1 WHERE logamm_catid = " + rup.leggiIntero("logamm_catid")
                            + " AND logamm_data = \"" + rup.leggiStringa("logamm_data") + "\"";
                    q2 = new QueryInterrogazioneClient(connAz, qryup);
                    q2.apertura();
                    Record r1 = q2.successivo();
                    q2.chiusura();
                    if (r1 != null) {
                        QueryAggiornamentoClient queryup = new QueryAggiornamentoClient(connAz, "UPDATE log_amm SET logamm_movjazzid = " + r1.leggiIntero("logamm_movjazzid")
                                + ",logamm_movdoccg = \"" + r1.leggiStringa("logamm_movdoccg") + "\""
                                + " WHERE logamm_id = " + rup.leggiIntero("logamm_id"));
                        queryup.esecuzione();
                    }
                }
            }
            q1.chiusura();
        }

        jComboEsercizi.removeAllItems();
        jComboEsercizi.addItem("<  ......  >");
        vlog = new ArrayList();
        String query = "SELECT DISTINCT logamm_eserc,logamm_data FROM log_amm ORDER BY logamm_eserc DESC,logamm_data DESC";
        QueryInterrogazioneClient qlog = new QueryInterrogazioneClient(connAz, query);
        qlog.apertura();
        Record rlog = qlog.successivo();
        int j = 0;
        int indice = 0;
        while (rlog != null) {
            j++;
            vlog.add(rlog);
            jComboEsercizi.addItem(rlog.leggiStringa("logamm_eserc") + "  -  " + rlog.leggiStringa("logamm_data"));
            if (rlog.leggiStringa("logamm_eserc").equals(esercizioScelto)) {
                indice = j;
            }
            rlog = qlog.successivo();
        }
        qlog.chiusura();
        jComboEsercizi.setSelectedIndex(indice);
    }//GEN-LAST:event_buttonRICALCOLAActionPerformed

    private void comboGruppiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGruppiActionPerformed
//        if (!primavolta) {
        if (comboGruppi.getSelectedIndex() > 0) {
            Record r = vmcsGru.get(comboGruppi.getSelectedIndex() - 1);
            popolaComboSpecie(r.leggiStringa("gruppobsmcs"));
        }
//        }
    }//GEN-LAST:event_comboGruppiActionPerformed

    private void comboSpecieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSpecieActionPerformed
//        if (!primavolta) {
        if (comboSpecie.getSelectedIndex() > 0) {
            Record r = vmcsSpec.get(comboSpecie.getSelectedIndex() - 1);
            caricaCategorie(r.leggiStringa("gruppobsmastro"), r.leggiStringa("gruppobsconto"));
        }
//        }
    }//GEN-LAST:event_comboSpecieActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox anchefiscale;
    private ui.beans.PopButton buttonCalcola;
    private ui.beans.PopButton buttonEsportaE;
    private ui.beans.PopButton buttonRICALCOLA;
    private ui.beans.PopButton buttonReg;
    private ui.beans.PopButton buttonStampa;
    private javax.swing.JComboBox comboGruppi;
    private javax.swing.JComboBox comboSpecie;
    private ui.beans.CampoDataPop dataregamm;
    private javax.swing.JLabel descrcategoria;
    private halleytech.models.DocumentoNumero documentoNumero1;
    private halleytech.models.DocumentoValuta documentoValuta1;
    private halleytech.models.DocumentoValuta documentoValuta2;
    private javax.swing.JTextField editpercciv;
    private javax.swing.JTextField editpercfis;
    private javax.swing.JTextField editutilizzo;
    private ui.beans.CampoInteroPop ggamm;
    private javax.swing.JComboBox jComboBeni;
    private javax.swing.JComboBox<String> jComboBoxfondo;
    private javax.swing.JComboBox<String> jComboEsercizi;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanelCateg;
    private javax.swing.JPanel jPanelCespiti;
    private javax.swing.JPanel jPanelFondo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private ui.beans.CampoTestoPop note;
    private ui.beans.CampoValutaPop quotaamm;
    private javax.swing.JLabel scostamento;
    private javax.swing.JTable tabellaAmmo;
    private javax.swing.JTable tabellaCosti;
    private javax.swing.JTable tabellaRisultato;
    private javax.swing.JLabel totaleciv;
    private javax.swing.JLabel totalededuc;
    private javax.swing.JLabel totalefis;
    // End of variables declaration//GEN-END:variables

    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.idVis = idVis;
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.wait = wait;
        raz = BusinessLogicArchiviBase.leggiDatiAzienda(EnvJazz.connAmbiente.conn, connCom.conn, azienda);
        //carica esercizi elaborati
        jComboEsercizi.addItem("<  ......  >");
        vlog = new ArrayList();

        jLabel19.setVisible(false);
        jLabel20.setVisible(false);
        jLabel12.setVisible(false);
        anchefiscale.setVisible(false);
        note.setVisible(false);
        quotaamm.setVisible(false);
        jLabel12.setVisible(false);
        ggamm.setVisible(false);

        String prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "art6c6");
        double soglia = Formattazione.estraiDouble(prop);

        quotaamm.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (!quotaamm.getText().equals("")) {
                    int sel = 0;
                    for (int i = 0; i < tabellaAmmo.getRowCount(); i++) {
                        if (((Boolean) tabellaAmmo.getValueAt(i, 0)).booleanValue()) {
                            sel++;
                        }
                    }

                    if (sel > 0) {
                        for (int i = 0; i < tabellaAmmo.getRowCount(); i++) {
                            if (((Boolean) tabellaAmmo.getValueAt(i, 0)).booleanValue()) {
                                if (modif[i]) {
                                    double totamm = Formattazione.estraiDouble(totaleciv.getText().trim());

                                    totamm += (Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM))) * (-1));
                                    double valoreciv = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_BENE)));
                                    double vfondociv = fondoc[i];

                                    double ggcalc = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_GIORNI)));
                                    int gganno = -1;
                                    int annoelab = Formattazione.estraiIntero(esContabile.substring(4, 8));
                                    String qrycat = "SELECT * FROM categoriebs WHERE catbsid = " + vcesp.get(i).leggiIntero("cespcategid");
                                    QueryInterrogazioneClient qcat = new QueryInterrogazioneClient(connAz, qrycat);
                                    qcat.apertura();
                                    Record reccateg = qcat.successivo();
                                    qcat.chiusura();
                                    if (reccateg.leggiIntero("catbscriterio1") == 1) {
                                        gganno = 12;
                                    } else {
                                        if (Data.annoBisestile(annoelab)) {
                                            gganno = 366;
                                        } else {
                                            gganno = 365;
                                        }
                                    }
                                    double quota = ((double) ggcalc / gganno);
                                    double residuo1C = OpValute.arrotondaMat(valoreciv - vfondociv, 2);

                                    double perc = quotaamm.getValore();
                                    //                                if (valoreciv <= soglia && rCat.leggiIntero("catbstipo") == 2) // solo per beni materiali inferiori alla soglia
                                    //                                {
                                    //                                    if (rCat.leggiIntero("catbscriterio2") == 0) {
                                    //                                        perc = 100.00;
                                    //                                    }
                                    //                                }
                                    double valammciv = OpValute.arrotondaMat(valoreciv * (perc / 100) * quota, 2);
                                    if (valammciv > residuo1C && residuo1C > 0.00) {
                                        valammciv = residuo1C;
                                    }

                                    if (anchefiscale.isSelected()) {
                                        double totammfis = Formattazione.estraiDouble(totalefis.getText().trim());
                                        totammfis += (Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC))) * (-1));
                                        double valoreF = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_FISCALE)));
                                        double vfondof = fondof[i];
                                        double residuo1F = OpValute.arrotondaMat(valoreF - vfondof, 2);
                                        double valammF = OpValute.arrotondaMat(valoreF * (perc / 100) * quota, 2);
                                        if (valammF > residuo1F && residuo1F > 0.00) {
                                            valammF = residuo1F;
                                        }
                                        totammfis += valammF;
                                        tabellaAmmo.setValueAt("" + new FDouble(valammF, "##0.00", 0), i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC));
                                        totalefis.setText("" + new FDouble(totammfis, "###,###,##0.00", 0));
                                        totalededuc.setText("" + new FDouble(valammF, "###,###,##0.00", 0));
                                        tabellaAmmo.setValueAt("" + new FDouble(perc, "##0.00", 0), i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_PFISC));
                                    }

                                    tabellaAmmo.setValueAt("" + new FDouble(valammciv, "##0.00", 0), i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM));
                                    tabellaAmmo.setValueAt("" + new FDouble(perc, "##0.00", 0), i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_COEFF));
                                    totamm += valammciv;

                                    totaleciv.setText("" + new FDouble(totamm, "###,###,##0.00", 0));
                                    double totammfisc = Formattazione.estraiDouble(totalededuc.getText().trim());
                                    double ripresa = totamm - totammfisc;
                                    scostamento.setText("" + new FDouble(ripresa, "###,###,##0.00", 2));

                                    repaint();
                                }
                            }
                        }
                    }
                }
            }
        });

        ggamm.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (!ggamm.getText().equals("")) {
                    int sel = 0;
                    for (int i = 0; i < tabellaAmmo.getRowCount(); i++) {
                        if (((Boolean) tabellaAmmo.getValueAt(i, 0)).booleanValue()) {
                            sel++;
                        }
                    }

                    if (sel > 0) {
                        for (int i = 0; i < tabellaAmmo.getRowCount(); i++) {
                            if (((Boolean) tabellaAmmo.getValueAt(i, 0)).booleanValue()) {
                                if (modif[i]) {
                                    double totammciv = Formattazione.estraiDouble(totaleciv.getText().trim());
                                    totammciv += (Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM))) * (-1));
                                    double valoreciv = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_BENE)));
                                    double vfondociv = fondoc[i];
                                    double totammfis = Formattazione.estraiDouble(totalefis.getText().trim());
                                    totammfis += (Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC))) * (-1));
                                    double vfondofisc = fondof[i];
                                    
                                    int ggcalc = ggamm.getValoreIntero();
                                    int annoelab = Formattazione.estraiIntero(esContabile.substring(4, 8));
                                    if (ggcalc != 0) {
                                        int gganno = -1;
                                        String qrycat = "SELECT * FROM categoriebs WHERE catbsid = " + vcesp.get(i).leggiIntero("cespcategid");
                                        QueryInterrogazioneClient qcat = new QueryInterrogazioneClient(connAz, qrycat);
                                        qcat.apertura();
                                        Record reccateg = qcat.successivo();
                                        qcat.chiusura();
                                        if (reccateg.leggiIntero("catbscriterio1") == 1) {
                                            gganno = 12;
                                        } else {
                                            if (Data.annoBisestile(annoelab)) {
                                                gganno = 366;
                                            } else {
                                                gganno = 365;
                                            }
                                        }
                                        double quota = ((double) ggcalc / gganno);

                                        double residuo1C = OpValute.arrotondaMat(valoreciv - vfondociv, 2);
                                        double percC = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_COEFF)));
                                        double valammciv = OpValute.arrotondaMat(valoreciv * (percC / 100) * quota, 2);
                                        if (valammciv > residuo1C) {
                                            valammciv = residuo1C;
                                        }
                                        totammciv += valammciv;

                                        if (anchefiscale.isSelected()) {
                                            double valoreF = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_FISCALE)));
                                            double vfondof = fondof[i];
                                            double residuo1F = OpValute.arrotondaMat(valoreF - vfondof, 2);
                                            double percF = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_PFISC)));

                                            double valammF = OpValute.arrotondaMat(valoreF * (percF / 100) * quota, 2);
                                            if (valammF > residuo1F) {
                                                valammF = residuo1F;
                                            }
                                            totammfis += valammF;
                                            tabellaAmmo.setValueAt("" + new FDouble(valammF, "##0.00", 0), i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC));
                                        } else {
                                            double valoreF = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_FISCALE)));
                                            double vfondof = fondof[i];
                                            double residuoF = OpValute.arrotondaMat(valoreF - vfondof, 2);
                                            double percf = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_PFISC)));

                                            int ggcalcf = gg[i];
                                            gganno = gg[i];
                                            quota = ((double) ggcalcf / gganno);
                                            double valammfis = OpValute.arrotondaMat(valoreF * (percf / 100) * quota, 2);
                                            if (valammfis > residuoF) {
                                                valammfis = residuoF;
                                            }
                                            totammfis += valammfis;
                                            tabellaAmmo.setValueAt("" + new FDouble(valammfis, "##0.00", 0), i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC));
                                        }

                                        tabellaAmmo.setValueAt("" + new FDouble(valammciv, "##0.00", 0), i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM));
                                        tabellaAmmo.setValueAt("" + ggcalc, i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_GIORNI));

                                        totaleciv.setText("" + new FDouble(totammciv, "###,###,##0.00", 0));
                                        totalefis.setText("" + new FDouble(totammfis, "###,###,##0.00", 0));

                                        double ripresa = totammciv - totammfis;
                                        scostamento.setText("" + new FDouble(ripresa, "###,###,##0.00", 2));
                                    }
                                    repaint();
                                }
                            }
                        }
                    }
                }
            }
        });

        String query = "SELECT DISTINCT logamm_eserc,logamm_data FROM log_amm ORDER BY logamm_eserc DESC,logamm_data DESC";
        QueryInterrogazioneClient qlog = new QueryInterrogazioneClient(connAz, query);
        qlog.apertura();
        Record rlog = qlog.successivo();
        while (rlog != null) {
            vlog.add(rlog);
            jComboEsercizi.addItem(rlog.leggiStringa("logamm_eserc") + "  -  " + rlog.leggiStringa("logamm_data"));
            rlog = qlog.successivo();
        }
        qlog.chiusura();
        esercizioScelto = esContabile;
        //causali bs ammortamenti
        ArrayList<Record> vcatbsid = new ArrayList();
        String qry = "SELECT * FROM caubs WHERE bscaustato = 1 AND bscaumov = 0 AND bscauraggr = 2 AND bscausegno = 1 ORDER BY bscauid";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vcatbsid.add(r);
            if (r.leggiStringa("bscaucod").equals("aamm")) {//ordinario
                idCau1 = r.leggiIntero("bscauid");
                descrcau1 = " AMMORTAMENTO ORDINARIO";
                String qryd = "SELECT docid,doccauconid FROM documenti WHERE doccod = \"" + r.leggiStringa("bsdocjazzcod") + "\"";
                QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qryd);
                qd.apertura();
                if (qd.numeroRecord() == 1) {
                    Record rd = qd.successivo();
                    idDoc1 = rd.leggiIntero("docid");
                    idJcau1 = rd.leggiIntero("doccauconid");
                } else {
                    JOptionPane.showMessageDialog(this, "Assente documento ammortamenti su parametri", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }
                qd.chiusura();
            } else if (r.leggiStringa("bscaucod").equals("amm1")) {//1'anno
                idCau2 = r.leggiIntero("bscauid");
                descrcau2 = " AMMORTAMENTO 1' ANNO";
                String qryd = "SELECT docid,doccauconid FROM documenti WHERE doccod = \"" + r.leggiStringa("bsdocjazzcod") + "\"";
                QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qryd);
                qd.apertura();
                if (qd.numeroRecord() == 1) {
                    Record rd = qd.successivo();
                    idDoc2 = rd.leggiIntero("docid");
                    idJcau2 = rd.leggiIntero("doccauconid");
                } else {
                    if (idDoc1 < 1) {
                        JOptionPane.showMessageDialog(this, "Assente documento ammortamenti su parametri", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    } else {
                        idDoc2 = idDoc1;
                        idJcau2 = idJcau1;
                    }
                }
                qd.chiusura();
            } else if (r.leggiStringa("bscaucod").equals("arit")) {//ritardato
                idCau3 = r.leggiIntero("bscauid");
                descrcau3 = " AMMORTAMENTO RITARDATO";
                String qryd = "SELECT docid,doccauconid FROM documenti WHERE doccod = \"" + r.leggiStringa("bsdocjazzcod") + "\"";
                QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qryd);
                qd.apertura();
                if (qd.numeroRecord() == 1) {
                    Record rd = qd.successivo();
                    idDoc3 = rd.leggiIntero("docid");
                    idJcau3 = rd.leggiIntero("doccauconid");
                } else {
                    if (idDoc1 < 1) {
                        JOptionPane.showMessageDialog(this, "Assente documento ammortamenti su parametri", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    } else {
                        idDoc3 = idDoc1;
                        idJcau3 = idJcau1;
                    }
                }
                qd.chiusura();
            } else if (r.leggiStringa("bscaucod").equals("aacc")) {//accellerato
                idCau4 = r.leggiIntero("bscauid");
                descrcau4 = " AMMORTAMENTO ACCELLERATO";
                String qryd = "SELECT docid,doccauconid FROM documenti WHERE doccod = \"" + r.leggiStringa("bsdocjazzcod") + "\"";
                QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qryd);
                qd.apertura();
                if (qd.numeroRecord() == 1) {
                    Record rd = qd.successivo();
                    idDoc4 = rd.leggiIntero("docid");
                    idJcau4 = rd.leggiIntero("doccauconid");
                } else {
                    if (idDoc1 < 1) {
                        JOptionPane.showMessageDialog(this, "Assente documento ammortamenti su parametri", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    } else {
                        idDoc4 = idDoc1;
                        idJcau4 = idJcau1;
                    }
                }
                qd.chiusura();
            } else {
                idCau5 = r.leggiIntero("bscauid");//ragionato
                descrcau5 = " AMMORTAMENTO RAGIONATO";
                String qryd = "SELECT docid,doccauconid FROM documenti WHERE doccod = \"" + r.leggiStringa("bsdocjazzcod") + "\"";
                QueryInterrogazioneClient qd = new QueryInterrogazioneClient(connAz, qryd);
                qd.apertura();
                if (qd.numeroRecord() == 1) {
                    Record rd = qd.successivo();
                    idDoc5 = rd.leggiIntero("docid");
                    idJcau5 = rd.leggiIntero("doccauconid");
                } else {
                    if (idDoc1 < 1) {
                        JOptionPane.showMessageDialog(this, "Assente documento ammortamenti su parametri", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    } else {
                        idDoc5 = idDoc1;
                        idJcau5 = idJcau1;
                    }
                }
                qd.chiusura();
            }
            r = qRic.successivo();
        }
        qRic.chiusura();

        String annoinizjazz = (BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "biliniz"));
        qry = "SELECT cbseserc FROM chiusurebs ORDER BY cbseserc DESC LIMIT 01";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        r = q.successivo();
        if (r != null) {
            annobilchiuso = r.leggiStringa("cbseserc").substring(4, 8);
        } else {
            annobilchiuso = "" + (Formattazione.estraiIntero(annoinizjazz) - 1);
        }
        q.chiusura();

        Data oggi = new Data();
        dtes = BusinessLogicAmbiente.leggiEsercizio(azienda, esContabile);
        if (oggi.maggioreuguale(new Data(dtes[0], Data.AAAA_MM_GG)) && oggi.minoreuguale(new Data(dtes[1], Data.AAAA_MM_GG))) {
            dataregamm.setText(oggi.formatta(Data.GG_MM_AAAA, "/"));
        } else {
            dataregamm.setText(new Data(dtes[1], Data.AAAA_MM_GG).formatta(Data.GG_MM_AAAA, "/"));
        }

        if ((esContabile.substring(4, 8)).compareTo(annobilchiuso) <= 0) {
            JOptionPane.showMessageDialog(this, "ESISTE la chiusura del libro cespiti. AMMESSA SOLO CONSULTAZIONE", "Invio",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            buttonCalcola.setVisible(false);
            buttonReg.setVisible(false);
        }

        if ((Formattazione.estraiIntero(esContabile.substring(4, 8)) - (Formattazione.estraiIntero(annobilchiuso))) > 1) {
            JOptionPane.showMessageDialog(this, "Assente la chiusura del libro cespiti, degli anni precedenti.\nAmmesso solo calcolo per consultazione", "Invio",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            buttonReg.setVisible(false);
            caricaCategorie("", "");
        } else {
            buttonReg.setEnabled(false);
            caricaCategorie("", "");
        }

        popolaComboGruppi();
    }

    private void caricaCategorie(String gruppo, String specie) {
        //2 = materiali
        //3 = immateriali
        //4 = Finanziarie 
        vcateg = new ArrayList();
        String qry = "SELECT catbsid,catbsgruppo,catbsspecie,catbssotto,catbscod,catbsdescr,catbsquotac,catbsquotaf,catbsquotaage,catbsdetrcosto,catbscriterio1"
                + ",catbsfondopdcid,catbscostopdcid FROM categoriebs"
                + " WHERE catbstipo = " + (jComboBeni.getSelectedIndex() + 2)
                + " AND catbsstato = 1";
        if (!gruppo.equals("") && !specie.equals("")) {
            qry += " AND catbsgruppo = \"" + gruppo + "\" AND catbsspecie = \"" + specie + "\"";
        }
        qry += " ORDER BY catbsgruppo,catbsspecie,catbssotto,catbscod";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();

        if (q.numeroRecord() > 0) {
            int num = q.numeroRecord();

            final String[] names
                    = {
                        COL_CESPITI, COL_GRU, COL_COD, COL_CAT, COL_PC, COL_CRIT, COL_PF, COL_DETR, COL_AGE, COL_DT, COL_IMPC, COL_IMPF, COL_IMPD, COL_PRINT, COL_DEL, COL_REG, COL_LEGA, COL_DOCUM
                    };
            final Object[][] data = new Object[num][names.length];

            id = new int[num];
            idM = new int[num];
            for (int i = 0; i < num; i++) {
                Record r = q.successivo();

                String qry1 = "SELECT COUNT(cespid) AS num FROM cespiti WHERE cespcategid=" + r.leggiIntero("catbsid");
                QueryInterrogazioneClient q1 = new QueryInterrogazioneClient(connAz, qry1);
                q1.apertura();
                Record r1 = q1.successivo();
                q1.chiusura();
                if (r1 != null) {
                    data[i][0] = r1.leggiIntero("num");
                } else {
                    data[i][0] = "";
                }
                data[i][1] = r.leggiStringa("catbsgruppo") + "." + r.leggiStringa("catbsspecie") + "." + r.leggiStringa("catbssotto");
                data[i][2] = r.leggiStringa("catbscod");
                data[i][3] = r.leggiStringa("catbsdescr");
                if (r.leggiDouble("catbsquotac") > 0) {
                    data[i][4] = "" + new FDouble(r.leggiDouble("catbsquotac"), "###,###,##0.00", 0);
                } else {
                    data[i][4] = "";
                }
                switch (r.leggiIntero("catbscriterio1")) {
                    case 0:
                        data[i][5] = "Quota ordinaria";
                        break;
                    case 1:
                        data[i][5] = "Quote in mesi";
                        break;
                    case 2:
                        data[i][5] = "Quote in giorni";
                        break;
                    case 3:
                        data[i][5] = "Accellerato (2 x Aliq.)";
                        break;
                    case 4:
                        data[i][5] = "Ritardato (50% Aliq)";
                        break;
                    case 5:
                        data[i][5] = "Nessun ammortamento";
                        break;
                    case 6:
                        data[i][5] = "Totale (100%)";
                        break;
                    default:
                        data[i][5] = "%  libera";
                        break;
                }

                if (r.leggiDouble("catbsquotaf") > 0) {
                    data[i][6] = "" + new FDouble(r.leggiDouble("catbsquotaf"), "###,###,##0.00", 0);
                } else {
                    data[i][6] = "";
                }
                if (r.leggiDouble("catbsdetrcosto") > 0) {
                    data[i][7] = "" + new FDouble(r.leggiDouble("catbsdetrcosto"), "###,###,##0.00", 0);
                } else {
                    data[i][7] = "";
                }
                if (r.leggiDouble("catbsquotaage") > 0) {
                    data[i][8] = "" + new FDouble(r.leggiDouble("catbsquotaage"), "###,###,##0.00", 0);
                } else {
                    data[i][8] = "";
                }
//                int idlogamm = -1;
                String qry2 = "SELECT * FROM log_amm WHERE logamm_eserc = \"" + esercizioScelto + "\" AND logamm_catid = " + r.leggiIntero("catbsid");
                QueryInterrogazioneClient qe = new QueryInterrogazioneClient(connAz, qry2);
                qe.apertura();
                Record re = qe.successivo();
                qe.chiusura();
                if (re == null) {
                    data[i][9] = "";
                    data[i][10] = "";
                    data[i][11] = "";
                    data[i][12] = "";
                    data[i][17] = "";
                    r.insElem("logamm_data", "00/00/0000");
                    r.insElem("logamm_importoc", (double) 0.00);
                    r.insElem("logamm_importof", (double) 0.00);
                    r.insElem("logamm_importod", (double) 0.00);
                    idM[i] = -1;
                    r.insElem("logamm_movjazzid", idM[i]);
                    r.insElem("logamm_id", -1);
                } else {
                    String dt = (new Data(re.leggiStringa("logamm_data"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    data[i][9] = dt;
                    data[i][10] = "" + new FDouble(re.leggiDouble("logamm_importoc"), "###,###,##0.00", 0);
                    data[i][11] = "" + new FDouble(re.leggiDouble("logamm_importof"), "###,###,##0.00", 0);
                    data[i][12] = "" + new FDouble(re.leggiDouble("logamm_importod"), "###,###,##0.00", 0);
                    data[i][17] = re.leggiStringa("logamm_movdoccg");
                    r.insElem("logamm_data", dt);
                    r.insElem("logamm_importoc", re.leggiDouble("logamm_importoc"));
                    r.insElem("logamm_importof", re.leggiDouble("logamm_importof"));
                    r.insElem("logamm_importod", re.leggiDouble("logamm_importod"));
                    idM[i] = re.leggiIntero("logamm_movjazzid");
                    r.insElem("logamm_movjazzid", idM[i]);
                    r.insElem("logamm_id", re.leggiIntero("logamm_id"));
                }

                vcateg.add(r);
                id[i] = r.leggiIntero("catbsid");

            }

            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    if (col == FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DEL)
                            || col == FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_REG)
                            || col == FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_LEGA)
                            || col == FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_PRINT)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };
            tabellaRisultato.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabellaRisultato, true);

            TableColumn buttonColumn14 = tabellaRisultato.getColumn(COL_LEGA);
            TablePopButton button14;
            button14 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                @Override
                public void customize(PopButton button, int row, int column) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_vpn_key_white_20dp.png")));
                    button.setBackground(new Color(0, 77, 106));
                    dtCalcolo = (String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT));
                    importiamm = true;
                    if (((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPC))).equals("")
                            || ((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPC))).equals("0,00")
                            || ((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPF))).equals("")
                            || ((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPF))).equals("0,00")) {
                        importiamm = false;
                    }
                    if (!dtCalcolo.equals("") && !dtCalcolo.equals("00/00/0000") && importiamm && idM[row] < 1) {
                        button.setToolTipText("Associa Doc. P.N.");
                        button.setEnabled(true);
                    } else {
                        button.setToolTipText("Funzione NON disponibile");
                        button.setEnabled(false);
                    }
                }
            });
            button14.addHandler(new TablePopButton.TableButtonPressedHandler() {
                @Override
                public void onButtonPress(int row, int column) {

                    String querycat = "SELECT * FROM categoriebs WHERE catbsid = " + id[tabellaRisultato.getSelectedRow()];
                    QueryInterrogazioneClient qcat = new QueryInterrogazioneClient(connAz, querycat);
                    qcat.apertura();
                    rCat = qcat.successivo();
                    qcat.chiusura();

                    if (rCat.leggiIntero("catbscostopdcid") > -1) {
                        idpdcA = rCat.leggiIntero("catbscostopdcid");
                    }

                    DialogRicercaPnAmmortamenti ddoc = new DialogRicercaPnAmmortamenti(formGen, true, connAz, esercizioScelto, idpdcA, -1);
                    UtilForm.posRelSchermo(ddoc, UtilForm.CENTRO);
                    ddoc.show();
                    Record r = vcateg.get(row);
                    if (ddoc.leggiId() > 0) {
                        String qrx = "UPDATE log_amm SET logamm_movjazzid = " + ddoc.leggiId()
                                + " WHERE logamm_id = " + r.leggiIntero("logamm_id");
                        QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrx);
                        qd.esecuzione();

                        String documento = cercaDocumento(ddoc.leggiId(), esercizioScelto);
                        qrx = "UPDATE log_amm SET logamm_movdoccg = \"" + documento + "\""
                                + " WHERE logamm_id = " + r.leggiIntero("logamm_id");
                        qd = new QueryAggiornamentoClient(connAz, qrx);
                        qd.esecuzione();
                        caricaCategorie(gruppo, specie);
                    }
                }
            });
            buttonColumn14.setCellRenderer(button14);
            buttonColumn14.setCellEditor(button14);

            TableColumn buttonColumn10 = tabellaRisultato.getColumn(COL_PRINT);
            TablePopButton button10 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                public void customize(PopButton button, int row, int column) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_print_white_20dp.png")));
                    button.setBackground(new Color(0, 77, 106));
                    dtCalcolo = (String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT));
                    if (!dtCalcolo.equals("") && !dtCalcolo.equals("00/00/0000")) {
                        button.setToolTipText("Stampa");
                        button.setEnabled(true);
                    } else {
                        button.setToolTipText("Funzione NON disponibile");
                        button.setEnabled(false);
                    }
                }
            });
            button10.addHandler(new TablePopButton.TableButtonPressedHandler() {
                @Override
                public void onButtonPress(int row, int column) {
                    String dt = (String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT));
                    dtCalcolo = (new Data(dt, Data.GG_MM_AAAA)).formatta(Data.AAAA_MM_GG, "-");
                    stampaAmmo(row);

                }
            });
            buttonColumn10.setCellRenderer(button10);
            buttonColumn10.setCellEditor(button10);

            TableColumn buttonColumn12 = tabellaRisultato.getColumn(COL_DEL);
            TablePopButton button12 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                public void customize(PopButton button, int row, int column) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_delete_white_20dp.png")));
                    button.setBackground(new Color(0, 77, 106));
                    if (esContabile.equals(esercizioScelto)) {
                        dtCalcolo = (String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT));
                        if (!dtCalcolo.equals("") && !dtCalcolo.equals("00/00/0000")) {
                            button.setToolTipText("Elimina calcolo ammortamenti");
                            button.setEnabled(true);
                        } else {
                            button.setToolTipText("Funzione NON disponibile");
                            button.setEnabled(false);
                        }
                    } else {
                        button.setToolTipText("Funzione NON disponibile");
                        button.setEnabled(false);
                    }
                }
            });
            button12.addHandler(new TablePopButton.TableButtonPressedHandler() {
                @Override
                public void onButtonPress(int row, int column) {
                    if (esContabile.equals(esercizioScelto)) {
                        int risp = JOptionPane.showConfirmDialog(formGen, "Si desidera eliminare gli ammortamenti?", "Avviso",
                                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                        if (risp == JOptionPane.YES_OPTION) {
                            String dt = (String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT));
                            dtCalcolo = (new Data(dt, Data.GG_MM_AAAA)).formatta(Data.AAAA_MM_GG, "-");
                            cancellaCalcoloAmmortamento(row, dtCalcolo);
                            caricaCategorie(gruppo, specie);
                        }
                    }
                }
            });
            buttonColumn12.setCellRenderer(button12);
            buttonColumn12.setCellEditor(button12);

            TableColumn buttonColumn13 = tabellaRisultato.getColumn(COL_REG);
            TablePopButton button13 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                public void customize(PopButton button, int row, int column) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_create_white_20dp.png")));
                    button.setBackground(new Color(0, 77, 106));
                    if (esContabile.equals(esercizioScelto)) {
                        dtCalcolo = (String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_DT));
                        importiamm = true;
                        if (((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPC))).equals("")
                                || ((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPC))).equals("0,00")
                                || ((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPF))).equals("")
                                || ((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPF))).equals("0,00")) {
                            importiamm = false;
                        }
                        if (!dtCalcolo.equals("") && !dtCalcolo.equals("00/00/0000") && importiamm) {
                            if (idM[row] < 1) {
                                button.setToolTipText("Registra Prima Nota ammortamenti");
                                button.setEnabled(true);
                            } else {
                                button.setToolTipText("Prima Nota ammortamenti PRESENTE");
                                button.setEnabled(false);
                            }
                        } else {
                            button.setToolTipText("Funzione NON disponibile");
                            button.setEnabled(false);
                        }
                    } else {
                        button.setToolTipText("Funzione NON disponibile");
                        button.setEnabled(false);
                    }
                }
            });
            button13.addHandler(new TablePopButton.TableButtonPressedHandler() {
                @Override
                public void onButtonPress(int row, int column) {
                    String querycat = "SELECT * FROM categoriebs WHERE catbsid = " + id[tabellaRisultato.getSelectedRow()];
                    QueryInterrogazioneClient qcat = new QueryInterrogazioneClient(connAz, querycat);
                    qcat.apertura();
                    rCat = qcat.successivo();
                    qcat.chiusura();

                    if (rCat.leggiIntero("catbscostopdcid") > -1) {
                        idpdcA = rCat.leggiIntero("catbscostopdcid");
                    }

                    if (rCat.leggiIntero("catbsfondopdcid") > -1) {
                        idpdcF = rCat.leggiIntero("catbsfondopdcid");
                    }

                    String dtelab = "";
                    if (!dataregamm.getDataStringa().equals(dtes[1])) {
                        dtelab = new Data(dataregamm.getText().trim(), Data.GG_MM_AAAA).formatta(Data.AAAA_MM_GG, "-");
                    } else {
                        dtelab = dtes[1];
                    }

                    totali = new HashMap();
                                        double fondoc = Formattazione.estraiDouble((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPC)));
                    double fondof = Formattazione.estraiDouble((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPF)));
                    double scostamento = Formattazione.estraiDouble((String) tabellaRisultato.getValueAt(row, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_IMPD)));

                    //controllo se ha gestioni
                    String qrygestioni = "SELECT * FROM gestioni WHERE gestioneid > 0";
                    QueryInterrogazioneClient querygest = new QueryInterrogazioneClient(connAz, qrygestioni);
                    querygest.apertura();
                    int numgest = querygest.numeroRecord();
                    querygest.chiusura();
                    if (numgest > 0) {
                        String qrycespiticateg = "SELECT * FROM cespiti WHERE cespcategid=" + id[tabellaRisultato.getSelectedRow()];
                        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qrycespiticateg);
                        query.apertura();
                        for (int i = 0; i < query.numeroRecord(); i++) {
                            Record r = query.successivo();
                            String qry = "SELECT * FROM  cespitiripartiti"
                                    + " INNER JOIN gestioni ON ceripgestioneid=gestioneid "
                                    + " WHERE ceripordineid = " + r.leggiIntero("cespid") + " AND ceripperc>0"
                                    + " ORDER BY gestioneid,ceripordineid";
                            QueryInterrogazioneClient qrip = new QueryInterrogazioneClient(connAz, qry);
                            qrip.apertura();
                            for (int j = 0; j < qrip.numeroRecord(); j++) {
                                Record rip = qrip.successivo();
                                String qry2 = "SELECT * FROM movimentibs WHERE mbscespid=" + r.leggiIntero("cespid") + " AND "
                                        + " mbseserc=" + esContabile + " AND mbstipomov=2 AND mbstipocf=1 AND mbsdata=\"" + dtelab + "\"";
                                QueryInterrogazioneClient query2 = new QueryInterrogazioneClient(connAz, qry2);
                                query2.apertura();
                                Record r2 = query2.successivo();
                                query2.chiusura();
                                if (r2 != null) {
                                    double am = OpValute.arrotondaMat(r2.leggiDouble("mbsammortamento") * rip.leggiDouble("ceripperc") / 100, 2);
                                    if (totali.containsKey(rip.leggiIntero("gestionepdcammid"))) {
                                        double val = (double) totali.get(rip.leggiIntero("gestionepdcammid"));
                                        val += am;
                                        totali.remove(rip.leggiIntero("gestionepdcammid"));
                                        totali.put(rip.leggiIntero("gestionepdcammid"), val);
                                    } else {
                                        totali.put(rip.leggiIntero("gestionepdcammid"), am);
                                    }
                                }
                            }
                        }
                    }

                    if (fondoc > 0) {
                        registraPrimaNota(totali, fondoc, fondof, scostamento);
                    }
                    caricaCategorie(gruppo, specie);
                }
            });
            buttonColumn13.setCellRenderer(button13);
            buttonColumn13.setCellEditor(button13);

            tabellaRisultato.getColumn(COL_GRU).setPreferredWidth(60);
            tabellaRisultato.getColumn(COL_COD).setPreferredWidth(90);
            tabellaRisultato.getColumn(COL_CAT).setPreferredWidth(400);
            tabellaRisultato.getColumn(COL_PC).setPreferredWidth(60);
            tabellaRisultato.getColumn(COL_CRIT).setPreferredWidth(120);
            tabellaRisultato.getColumn(COL_PF).setPreferredWidth(60);
            tabellaRisultato.getColumn(COL_DETR).setPreferredWidth(84);
            tabellaRisultato.getColumn(COL_AGE).setPreferredWidth(70);

            tabellaRisultato.getColumn(COL_DT).setPreferredWidth(80);
            tabellaRisultato.getColumn(COL_IMPC).setPreferredWidth(88);
            tabellaRisultato.getColumn(COL_IMPF).setPreferredWidth(88);
            tabellaRisultato.getColumn(COL_IMPD).setPreferredWidth(88);

            tabellaRisultato.getColumn(COL_DEL).setPreferredWidth(36);
            tabellaRisultato.getColumn(COL_REG).setPreferredWidth(36);
            tabellaRisultato.getColumn(COL_PRINT).setPreferredWidth(36);
            tabellaRisultato.getColumn(COL_LEGA).setPreferredWidth(36);
            tabellaRisultato.getColumn(COL_DOCUM).setPreferredWidth(240);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            tabellaRisultato.getColumn(COL_IMPC).setCellRenderer(rightRenderer);
            tabellaRisultato.getColumn(COL_IMPF).setCellRenderer(rightRenderer);
            tabellaRisultato.getColumn(COL_IMPD).setCellRenderer(rightRenderer);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            tabellaRisultato.getColumn(COL_DT).setCellRenderer(centerRenderer);
            tabellaRisultato.getColumn(COL_PC).setCellRenderer(centerRenderer);
            tabellaRisultato.getColumn(COL_PF).setCellRenderer(centerRenderer);
            tabellaRisultato.getColumn(COL_DETR).setCellRenderer(centerRenderer);
            tabellaRisultato.getColumn(COL_AGE).setCellRenderer(centerRenderer);

            tabellaRisultato.getColumn(COL_DEL).setHeaderRenderer(headerRenderer);
            tabellaRisultato.getColumn(COL_REG).setHeaderRenderer(headerRenderer);
            tabellaRisultato.getColumn(COL_PRINT).setHeaderRenderer(headerRenderer);
            tabellaRisultato.getColumn(COL_LEGA).setHeaderRenderer(headerRenderer);
        } else {
            tabellaRisultato.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{}));
        }
        if (tabellaRisultato.getRowCount() > 0) {
            tabellaRisultato.requestFocus();
            tabellaRisultato.setRowSelectionInterval(0, 0);
        }
        q.chiusura();
    }

    private void registrazioneAmmortamenti() {
        Record ra = null;
        String dtelab = "";

        if (!dataregamm.getDataStringa().equals(dtes[1])) {
            dtelab = new Data(dataregamm.getText().trim(), Data.GG_MM_AAAA).formatta(Data.AAAA_MM_GG, "-");
        } else {
            dtelab = dtes[1];
        }

        for (int i = 0; i < tabellaAmmo.getRowCount(); i++) {

            if (modif[i]) {
                String qry = "SELECT catbscriterio1,catbsquotac,catbsquotaf,catbsquota1,catbsquotaage,catbsdetrcosto FROM cespiti INNER JOIN categoriebs ON cespcategid = catbsid WHERE cespid = " + idCesp[i];
                QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
                q.apertura();
                Record rec = q.successivo();
                double importo = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC)));

                if (importo > 0) {
                    //FISCALE
                    double quota = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_PFISC)));
                    double catbsquotaf = rec.leggiDouble("catbsquotaf");
                    ra = new Record();
                    ra.insElem("mbseserc", esContabile);
                    ra.insElem("mbscespid", idCesp[i]);
                    ra.insElem("mbstipocf", (int) 1); //(1=Fiscale)
                    ra.insElem("mbsdata", dtelab);
                    ra.insElem("mbstipomov", (int) 2); //ammortamento

                    if (quota == catbsquotaf) {
                        ra.insElem("mbscriterio", (int) 0);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento ordinario");
                        } else {
                            String notedescr = "Ammortamento ordinario - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau1);
                    } else if (quota == (catbsquotaf * 2)) {
                        ra.insElem("mbscriterio", (int) 1);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento anticipato");
                        } else {
                            String notedescr = "Ammortamento anticipato - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau3);
                    } else if (quota == (double) 100) {
                        ra.insElem("mbscriterio", (int) 6);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento immediato");
                        } else {
                            String notedescr = "Ammortamento immediato - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau2);
                    } else if (quota > catbsquotaf) {
                        ra.insElem("mbscriterio", (int) 2);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento accelerato");
                        } else {
                            String notedescr = "Ammortamento accelerato - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau4);
                    } else if (quota == (catbsquotaf / 2)) {
                        ra.insElem("mbscriterio", (int) 0);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento 1' anno");
                        } else {
                            String notedescr = "Ammortamento 1' anno - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau2);
                    } else if (quota == 0 && catbsquotaf == 0) {
                        ra.insElem("mbscriterio", (int) 5);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "NON ammortizzabile");
                        } else {
                            String notedescr = "NON ammortizzabile - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau2);
                    } else {
                        ra.insElem("mbscriterio", (int) 3);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento ritardato");
                        } else {
                            String notedescr = "Ammortamento ritardato - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau5);
                    }

                    ra.insElem("mbsquotaord", catbsquotaf);
                    ra.insElem("mbsquotaappl", quota);
                    ra.insElem("mbsutilizzo", Formattazione.estraiIntero((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_GIORNI))));
                    double imponibile = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_FISCALE)));
                    ra.insElem("mbsimporto", imponibile);
                    ra.insElem("mbsimpbene", (double) 0);
                    ra.insElem("mbsqta", (double) 0);
                    ra.insElem("mbsammortamento", importo);
                    double costodetraib = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_DEDUC)));
                    double costonodetraib = importo - costodetraib;
                    ra.insElem("mbsammdeducib", costodetraib);
                    ra.insElem("mbsammindeducib", costonodetraib);
                    ra.insElem("mbsimpofondo", importo);
                    ra.insElem("mbsmovjazzid", (int) -1);
                    ra.convalida(true);
                    QueryAggiornamentoClient qi = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "movimentibs", "mbsid", ra);
                    qi.esecuzione();
                }

                importo = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM)));
                if (importo > 0) {
                    //CIVILISTICO
                    ra = new Record();

                    double quota = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_COEFF)));
                    double cespquotac = rec.leggiDouble("catbsquotac");
                    ra.insElem("mbseserc", esContabile);
                    ra.insElem("mbscespid", idCesp[i]);
                    ra.insElem("mbstipocf", (int) 2); //(2=Civile)
                    ra.insElem("mbsdata", dtelab);
                    ra.insElem("mbstipomov", (int) 2); //ammortamento
                    if (rec.leggiIntero("catbscriterio1") == 1 || rec.leggiIntero("catbscriterio1") == 2) {
                        ra.insElem("mbscriterio", rec.leggiIntero("catbscriterio1"));
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento ordinario");
                        } else {
                            String notedescr = "Ammortamento ordinario - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau1);
                    } else if (rec.leggiIntero("catbscriterio1") == 0) {
                        ra.insElem("mbscriterio", (int) 0);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento come fiscale");
                        } else {
                            String notedescr = "Ammortamento come fiscale - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau1);
                    } else if (rec.leggiIntero("catbscriterio1") == 3) {
                        ra.insElem("mbscriterio", (int) 3);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento accelerato");
                        } else {
                            String notedescr = "Ammortamento accelerato - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau4);
                    } else if (rec.leggiIntero("catbscriterio1") == 4) {
                        ra.insElem("mbscriterio", (int) 4);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento ritardato");
                        } else {
                            String notedescr = "Ammortamento ritardato - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau5);
                    } else if (rec.leggiIntero("catbscriterio1") == 5) {
                        ra.insElem("mbscriterio", (int) 5);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "NON ammortizzabile");
                        } else {
                            String notedescr = "NON ammortizzabile - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", -1);
                    } else {
                        ra.insElem("mbscriterio", (int) 6);
                        if (note.getText().equals("")) {
                            ra.insElem("mbsnote", "Ammortamento immediato");
                        } else {
                            String notedescr = "Ammortamento immediato - " + note.getText();
                            if (notedescr.length() > 255) {
                                notedescr = notedescr.substring(0, 255);
                            }
                            ra.insElem("mbsnote", notedescr);
                        }
                        ra.insElem("mbscaumovid", idCau2);
                    }

                    ra.insElem("mbsquotaord", cespquotac);
                    ra.insElem("mbsquotaappl", quota);
                    ra.insElem("mbsutilizzo", Formattazione.estraiIntero((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_GIORNI))));
                    double imponibile = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_BENE)));
                    ra.insElem("mbsimporto", imponibile);
                    ra.insElem("mbsimpbene", (double) 0);
                    ra.insElem("mbsqta", (double) 0);
                    ra.insElem("mbsammortamento", importo);
                    ra.insElem("mbsammdeducib", (double) 0);
                    ra.insElem("mbsammindeducib", (double) 0);
                    ra.insElem("mbsimpofondo", importo);
                    ra.insElem("mbsmovjazzid", (int) -1);
                    ra.convalida(true);
                    QueryAggiornamentoClient qi = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "movimentibs", "mbsid", ra);
                    qi.esecuzione();
                }
            }
        }

        double fondoc = Formattazione.estraiDouble(totaleciv.getText().trim());
        double fondof = Formattazione.estraiDouble(totalefis.getText().trim());
        double scost = Formattazione.estraiDouble(scostamento.getText().trim());
        if (fondoc > 0) {
            int ris = JOptionPane.showConfirmDialog(this, "Registro Prima Nota ammortamenti in contabilita ?",
                    "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
            if (ris == JOptionPane.YES_OPTION) {
                totali = new HashMap();
                //controllo se ha gestioni
                String qrygestioni = "SELECT * FROM gestioni WHERE gestioneid > 0";
                QueryInterrogazioneClient querygest = new QueryInterrogazioneClient(connAz, qrygestioni);
                querygest.apertura();
                int numgest = querygest.numeroRecord();
                querygest.chiusura();
                if (numgest > 0) {
                    for (int i = 0; i < tabellaAmmo.getRowCount(); i++) {
                        Record r = vcesp.get(i);
                        String qry = "SELECT * FROM  cespitiripartiti"
                                + " INNER JOIN gestioni ON ceripgestioneid = gestioneid "
                                + " WHERE ceripordineid = " + r.leggiIntero("cespid") + " AND ceripperc > 0"
                                + " ORDER BY gestioneid,ceripordineid";
                        QueryInterrogazioneClient qrip = new QueryInterrogazioneClient(connAz, qry);
                        qrip.apertura();
                        for (int j = 0; j < qrip.numeroRecord(); j++) {
                            Record rip = qrip.successivo();
                            double am = OpValute.arrotondaMat(Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM))) * rip.leggiDouble("ceripperc") / 100, 2);
                            if (totali.containsKey(rip.leggiIntero("gestionepdcammid"))) {
                                double val = (double) totali.get(rip.leggiIntero("gestionepdcammid"));
                                val += am;
                                totali.remove(rip.leggiIntero("gestionepdcammid"));
                                totali.put(rip.leggiIntero("gestionepdcammid"), val);
                            } else {
                                totali.put(rip.leggiIntero("gestionepdcammid"), am);
                            }
                        }
                    }
                    registraPrimaNota(totali, fondoc, fondof, scost);
                    return;
                } else {
                    registraPrimaNota(totali, fondoc, fondof, scost);
                    return;
                }
            }
        }
        String qry2 = "SELECT logamm_id FROM log_amm WHERE logamm_eserc = \"" + esContabile + "\" AND logamm_catid = " + rCat.leggiIntero("catbsid");
        QueryInterrogazioneClient qe = new QueryInterrogazioneClient(connAz, qry2);
        qe.apertura();
        Record re = qe.successivo();
        qe.chiusura();

        Record r = new Record();
        r.insElem("logamm_eserc", esContabile);
        r.insElem("logamm_catid", rCat.leggiIntero("catbsid"));
        r.insElem("logamm_data", dtelab);
        r.insElem("logamm_fisc", (int) 1);
        r.insElem("logamm_civ", (int) 1);
        r.insElem("logamm_importoc", Formattazione.estraiDouble(totaleciv.getText().trim()));
        r.insElem("logamm_importof", Formattazione.estraiDouble(totalefis.getText().trim()));
        r.insElem("logamm_importod", Formattazione.estraiDouble(totalededuc.getText().trim()));
        r.insElem("logamm_movjazzid", (int) -1);
        r.insElem("logamm_chiuso", (int) 0); //simulazione            
        r.insElem("logamm_movdoccg", "");
        r.convalida(true);
        if (re != null) {
            r.insElem("logamm_id", re.leggiIntero("logamm_id"));
            QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "log_amm", "logamm_id", r);
            qa.esecuzione();
        } else {
            QueryAggiornamentoClient qi = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "log_amm", "logamm_id", r);
            qi.esecuzione();
        }

    }

    private void CalcolaAmmortamenti(String dataregord) {
        double totammciv = 0;
        double totammfis = 0;
        double totammded = 0;
        int nrighe = 0;
        String qry = "SELECT cespdatainizio,cespid,cespordine,cespdescriz1,cespdatafunz,cespdataalien,catbscriterio1,cespcategid FROM cespiti"
                + " INNER JOIN categoriebs ON cespcategid = catbsid"
                + " WHERE cespeserc <= \"" + esContabile + "\""
                + " AND cespcategid = " + rCat.leggiIntero("catbsid")
                + " ORDER BY cespeserc,cespdatainizio,cespordine";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        nrighe = q.numeroRecord();
        if (nrighe > 0) {
            idCesp = new int[nrighe];
            gg = new int[nrighe];
            modif = new boolean[nrighe];
            fondoc = new double[nrighe];
            fondof = new double[nrighe];
            vcesp = new ArrayList<Record>();

            final String[] names = {
                COL_SEL, COL_DTACQ, COL_DESCR, COL_GIORNI, COL_COEFF, COL_BENE, COL_IMPAMM, COL_PERIODO, COL_CRIT, COL_PFISC, COL_FISCALE, COL_AMMFISC, COL_DEDUC
            };
            final Object[][] data = new Object[nrighe][names.length];

            for (int i = 0; i < nrighe; i++) {
                Record r = q.successivo();
                vcesp.add(r);
                data[i][0] = new Boolean(false);

                String dies = "";
                if (!r.leggiStringa("cespdatafunz").equals("0000-00-00")) {
                    dies = (new Data(r.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                } else {
                    dies = (new Data(r.leggiStringa("cespdatainizio"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                }
                data[i][1] = dies;
                data[i][2] = r.leggiStringa("cespordine") + " / " + r.leggiStringa("cespdescriz1");
                double vamm[] = BusinessLogicCespiti.calcolaAmmortamento(connAz.conn, azienda, esContabile, null, null, r.leggiIntero("cespid"), dataregord);

                if (vamm[8] == 7) {
                    //effettuato calcolo
                    data[i][3] = "" + (int) vamm[4];
                    data[i][4] = "" + new FDouble(vamm[2], "##0.00", 0);
                    data[i][5] = "" + new FDouble(vamm[0], "###,###,##0.00", 2);
                    data[i][6] = "" + new FDouble(vamm[6], "###,###,##0.00", 0);

                    String difi = (new Data(dataregord, Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                    String[] dtf = BusinessLogicAmbiente.leggiEsercizio(azienda, esercizioScelto);
                    String datainizioanno = "01/01/" + dtf[1].substring(0, 4);
                    if (dies.compareTo(datainizioanno) < 0) {
                        data[i][7] = "" + datainizioanno + "     " + difi;
                    } else {
                        data[i][7] = "" + dies + "     " + difi;
                    }
                    switch (r.leggiIntero("catbscriterio1")) {
                        case 0:
                            data[i][8] = "Amm. quota ordinaria";
                            break;
                        case 1:
                            data[i][8] = "Amm. a quote costanti in mesi";
                            break;
                        case 2:
                            data[i][8] = "Amm. a quote costanti in giorni";
                            break;
                        case 3:
                            data[i][8] = "Amm. accellerato (2 x Aliq.)";
                            break;
                        case 4:
                            data[i][8] = "Amm. (50% Aliq)";
                            break;
                        case 5:
                            data[i][8] = "Nessun ammortamento";
                            break;
                        case 6:
                            data[i][8] = "Amm. Totale (100%)";
                            break;
                        default:
                            data[i][8] = "ragionato Aliq. libera";
                            break;
                    }

                    data[i][9] = "" + new FDouble(vamm[3], "##0.00", 0);
                    data[i][10] = "" + new FDouble(vamm[1], "###,###,##0.00", 0);
                    data[i][11] = "" + new FDouble(vamm[7], "###,###,##0.00", 0);
                    data[i][12] = "" + new FDouble(vamm[9], "###,###,##0.00", 0);

                    gg[i] = (int) vamm[4];
                    modif[i] = true;
                    fondoc[i] = vamm[10];
                    fondof[i] = vamm[11];
                    totammciv += vamm[6];
                    totammfis += vamm[7];
                    totammded += vamm[9];
                    //                    valimmobiliz += vamm[0];
                } else if (vamm[8] == 1) {
                    //non ancora in funzione 
                    data[i][3] = "";
                    data[i][4] = "" + new FDouble(vamm[2], "##0.00", 0);
                    data[i][5] = "" + new FDouble(vamm[0], "###,###,##0.00", 2);
                    data[i][6] = "";
                    dies = (new Data(r.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, ".");
                    data[i][7] = "In funzione dal " + dies;
                    data[i][8] = "Non ancora in funzione";
                    data[i][9] = "" + new FDouble(vamm[3], "##0.00", 0);
                    data[i][10] = "" + new FDouble(vamm[1], "###,###,##0.00", 0);
                    data[i][11] = "";
                    data[i][12] = "";
                    //                    valimmobiliz += vamm[0];
                    gg[i] = 0;
                    fondoc[i] = 0;
                    fondof[i] = 0;
                    modif[i] = false;
                } else if (vamm[8] == 2 || vamm[8] == 3) {
                    //venduto
                    data[i][3] = "";
                    data[i][4] = "";
                    data[i][5] = "";
                    data[i][6] = "";
                    dies = (new Data(r.leggiStringa("cespdataalien"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, ".");
                    data[i][7] = "Venduto/dismesso il " + dies;
                    if (vamm[8] == 3) {
                        data[i][8] = "Variazione nell'anno";
                    } else {
                        data[i][8] = "";
                    }

                    data[i][9] = "";
                    data[i][10] = "";
                    data[i][11] = "";
                    data[i][12] = "";

                    gg[i] = 0;
                    fondoc[i] = 0;
                    fondof[i] = 0;
                    modif[i] = false;
                } else if (vamm[8] == 4) {
                    //non ammortizzabile (esempio terreni)
                    data[i][3] = "";
                    data[i][4] = "0.00";
                    data[i][5] = "" + new FDouble(vamm[0], "###,###,##0.00", 2);
                    data[i][6] = "";
                    data[i][7] = "";
                    data[i][8] = "NON ammortizzabile";
                    data[i][9] = "0.00";
                    data[i][10] = "" + new FDouble(vamm[1], "###,###,##0.00", 2);
                    data[i][11] = "";
                    data[i][12] = "";

                    gg[i] = 0;
                    fondoc[i] = 0;
                    fondof[i] = 0;
                    modif[i] = false;
//                    valimmobiliz += vamm[0];
                } else if (vamm[8] == 5) {
                    //ammortizzato
                    data[i][3] = "";
                    data[i][4] = "0.00";
                    data[i][5] = "" + new FDouble(vamm[0], "###,###,##0.00", 2);
                    data[i][6] = "";
                    data[i][7] = "";
                    data[i][8] = "AMMORTIZZATO";
                    data[i][9] = "0.00";
                    data[i][10] = "" + new FDouble(vamm[1], "###,###,##0.00", 2);
                    data[i][11] = "";
                    data[i][12] = "";

                    gg[i] = 0;
                    fondoc[i] = 0;
                    fondof[i] = 0;
                    modif[i] = false;
//                    valimmobiliz += vamm[0];
                } else {
                    //errore
                    data[i][3] = "";
                    data[i][4] = "0.00";
                    data[i][5] = "" + new FDouble(vamm[0], "###,###,##0.00", 2);
                    data[i][6] = "";
                    data[i][7] = "Correggere";
                    data[i][8] = "ERRORE";
                    data[i][9] = "0.00";
                    data[i][10] = "" + new FDouble(vamm[1], "###,###,##0.00", 2);
                    data[i][11] = "";
                    data[i][12] = "";

                    gg[i] = 0;
                    fondoc[i] = 0;
                    fondof[i] = 0;
                    modif[i] = false;
                }

                idCesp[i] = r.leggiIntero("cespid");
            }
            q.chiusura();
            totaleciv.setText("" + new FDouble(totammciv, "###,###,##0.00", 0));
            totalefis.setText("" + new FDouble(totammfis, "###,###,##0.00", 0));
            totalededuc.setText("" + new FDouble(totammded, "###,###,##0.00", 0));
            double ripresa = totammciv - totammded;
            scostamento.setText("" + new FDouble(ripresa, "###,###,##0.00", 2));

            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    if (col == FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_SEL)
                            || col == FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_COEFF)
                            || col == FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_GIORNI)
                            || col == FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_PFISC)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };
            tabellaAmmo.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabellaAmmo, true);

            DefaultCellEditor ced = new DefaultCellEditor(editpercfis);
            ced.addCellEditorListener(new CambioTabFisc());
            tabellaAmmo.getColumn(COL_PFISC).setCellEditor(ced);

            DefaultCellEditor civ = new DefaultCellEditor(editpercciv);
            civ.addCellEditorListener(new CambioTabCiv());
            tabellaAmmo.getColumn(COL_COEFF).setCellEditor(civ);

            DefaultCellEditor cid = new DefaultCellEditor(editutilizzo);
            cid.addCellEditorListener(new CambioTabGg());
            tabellaAmmo.getColumn(COL_GIORNI).setCellEditor(cid);

            tabellaAmmo.getColumn(COL_COEFF).setPreferredWidth(60);
            tabellaAmmo.getColumn(COL_BENE).setPreferredWidth(108);
            tabellaAmmo.getColumn(COL_IMPAMM).setPreferredWidth(80);
            tabellaAmmo.getColumn(COL_PERIODO).setPreferredWidth(150);
            tabellaAmmo.getColumn(COL_GIORNI).setPreferredWidth(48);
            tabellaAmmo.getColumn(COL_CRIT).setPreferredWidth(200);
            tabellaAmmo.getColumn(COL_PFISC).setPreferredWidth(64);
            tabellaAmmo.getColumn(COL_FISCALE).setPreferredWidth(108);
            tabellaAmmo.getColumn(COL_AMMFISC).setPreferredWidth(80);
            tabellaAmmo.getColumn(COL_DEDUC).setPreferredWidth(100);
            tabellaAmmo.getColumn(COL_DTACQ).setPreferredWidth(90);
            tabellaAmmo.getColumn(COL_DESCR).setPreferredWidth(410);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            tabellaAmmo.getColumn(COL_COEFF).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_BENE).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_IMPAMM).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_PFISC).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_FISCALE).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_AMMFISC).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_DEDUC).setCellRenderer(rightRenderer);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            tabellaAmmo.getColumn(COL_GIORNI).setCellRenderer(centerRenderer);
            tabellaAmmo.getColumn(COL_DTACQ).setCellRenderer(centerRenderer);

            TableColumn tc = tabellaAmmo.getColumnModel().getColumn(BOOLEAN_COL);
            if (tabellaAmmo.getRowCount() != 0) {
                select = new SelectAllHeader(tabellaAmmo, BOOLEAN_COL);
                select.impostaListener(this);
                tc.setHeaderRenderer(select);
            }
        }
    }

    private void mostraAmmortamenti() {
        double totammciv = 0;
        double totammfis = 0;
        double totammded = 0;
        vcesp = new ArrayList();
        String qry = "SELECT DISTINCT cespordine,cespdescriz1,cespdatainizio,cespid,cespdatafunz FROM cespiti"
                + " INNER JOIN categoriebs ON cespcategid = catbsid"
                + " INNER JOIN movimentibs ON cespiti.cespid = movimentibs.mbscespid"
                + " WHERE mbstipomov = 2"
                + " AND mbseserc = \"" + esercizioScelto + "\""
                + " AND cespcategid = " + rCat.leggiIntero("catbsid")
                + " ORDER BY cespeserc,cespdatainizio,cespordine";

        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record r = q.successivo();
        while (r != null) {
            vcesp.add(r);
            r = q.successivo();
        }
        q.chiusura();

        if (!vcesp.isEmpty()) {
            final String[] names = {
                COL_SEL, COL_DTACQ, COL_DESCR, COL_GIORNI, COL_COEFF, COL_BENE, COL_IMPAMM, COL_PERIODO, COL_CRIT, COL_PFISC, COL_FISCALE, COL_AMMFISC, COL_DEDUC
            };
            final Object[][] data = new Object[vcesp.size()][names.length];

            modif = new boolean[vcesp.size()];
            boolean trovatof = false;
            boolean trovatoc = false;
            for (int i = 0; i < vcesp.size(); i++) {
                Record rcesp = vcesp.get(i);
                trovatof = false;
                trovatoc = false;

                data[i][0] = new Boolean(false);

                String dies = "";
                if (!rcesp.leggiStringa("cespdatafunz").equals("0000-00-00")) {
                    dies = (new Data(rcesp.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                } else {
                    dies = (new Data(rcesp.leggiStringa("cespdatainizio"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                }
                data[i][1] = dies;
                data[i][2] = rcesp.leggiStringa("cespordine") + " - " + rcesp.leggiStringa("cespdescriz1");
                String query = "SELECT SUM(mbsammortamento) AS mbsammortamento,SUM(mbsammdeducib) AS mbsammdeducib,mbstipocf,mbsimporto,mbsquotaappl,mbsutilizzo,mbscriterio,mbsdata FROM movimentibs"
                        + " WHERE mbstipomov = 2"
                        + " AND mbseserc = \"" + esercizioScelto + "\""
                        + " AND mbscespid = " + rcesp.leggiIntero("cespid")
                        + " GROUP BY mbstipocf,mbsimporto";
                QueryInterrogazioneClient qbs = new QueryInterrogazioneClient(connAz, query);
                qbs.apertura();
                Record rbs = qbs.successivo();
                while (rbs != null) {
                    if (rbs.leggiIntero("mbstipocf") == 1) {
                        trovatof = true;
                        switch (rbs.leggiIntero("mbscriterio")) {
                            case 0:
                                data[i][8] = "Amm. quota ordinaria";
                                break;
                            case 1:
                                data[i][8] = "Amm. a quote costanti in mesi";
                                break;
                            case 2:
                                data[i][8] = "Amm. a quote costanti in giorni";
                                break;
                            case 3:
                                data[i][8] = "Amm. accellerato (2 x Aliq.)";
                                break;
                            case 4:
                                data[i][8] = "Amm. (50% Aliq)";
                                break;
                            case 5:
                                data[i][8] = "Nessun ammortamento";
                                break;
                            case 6:
                                data[i][8] = "Amm. Totale (100%)";
                                break;
                            default:
                                data[i][8] = "ragionato Aliq. libera";
                                break;
                        }
                        data[i][9] = "" + new FDouble(rbs.leggiDouble("mbsquotaappl"), "##0.00", 0);
                        data[i][10] = "" + new FDouble(rbs.leggiDouble("mbsimporto"), "###,###,##0.00", 0);
                        data[i][11] = "" + new FDouble(rbs.leggiDouble("mbsammortamento"), "###,###,##0.00", 0);
                        data[i][12] = "" + new FDouble(rbs.leggiDouble("mbsammdeducib"), "###,###,##0.00", 0);

                        totammfis += rbs.leggiDouble("mbsammortamento");
                        totammded += rbs.leggiDouble("mbsammdeducib");
                    } else {
                        trovatoc = true;
                        data[i][3] = "" + rbs.leggiIntero("mbsutilizzo");
                        data[i][4] = "" + new FDouble(rbs.leggiDouble("mbsquotaappl"), "##0.00", 0);
                        data[i][5] = "" + new FDouble(rbs.leggiDouble("mbsimporto"), "###,###,##0.00", 0);
                        data[i][6] = "" + new FDouble(rbs.leggiDouble("mbsammortamento"), "###,###,##0.00", 0);

                        String[] dtf = BusinessLogicAmbiente.leggiEsercizio(azienda, esercizioScelto);
                        String datainizioanno = dtf[1].substring(0, 4)+"-01-01";
                        String difi = (new Data(rbs.leggiStringa("mbsdata"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                        if ((new Data(dies, Data.GG_MM_AAAA).formatta(Data.AAAA_MM_GG, "-")).compareTo(datainizioanno) < 0) {
                            data[i][7] = "" + new Data(datainizioanno,Data.AAAA_MM_GG).formatta(Data.GG_MM_AAAA,"/") + "     " + difi;
                        } else {
                            data[i][7] = "" + dies + "     " + difi;
                        }
                        totammciv += rbs.leggiDouble("mbsammortamento");
                    }
                    if (!trovatof) {
                        data[i][8] = "";
                        data[i][9] = "";
                        data[i][10] = "";
                        data[i][11] = "";
                        data[i][12] = "";
                    }
                    if (!trovatoc) {
                        data[i][3] = "";
                        data[i][4] = "";
                        data[i][5] = "";
                        data[i][6] = "";
                        data[i][7] = "";
                    }
                    rbs = qbs.successivo();

                }
                qbs.chiusura();
                modif[i] = false;
            }
            //
            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    if (col == FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_SEL)) {
                        return true;
                    } else {
                        return false;
                    }
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };

            tabellaAmmo.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabellaAmmo, true);

            tabellaAmmo.getColumn(COL_COEFF).setPreferredWidth(60);
            tabellaAmmo.getColumn(COL_BENE).setPreferredWidth(108);
            tabellaAmmo.getColumn(COL_IMPAMM).setPreferredWidth(80);
            tabellaAmmo.getColumn(COL_PERIODO).setPreferredWidth(150);
            tabellaAmmo.getColumn(COL_GIORNI).setPreferredWidth(48);
            tabellaAmmo.getColumn(COL_CRIT).setPreferredWidth(200);
            tabellaAmmo.getColumn(COL_PFISC).setPreferredWidth(64);
            tabellaAmmo.getColumn(COL_FISCALE).setPreferredWidth(108);
            tabellaAmmo.getColumn(COL_AMMFISC).setPreferredWidth(80);
            tabellaAmmo.getColumn(COL_DEDUC).setPreferredWidth(100);
            tabellaAmmo.getColumn(COL_DTACQ).setPreferredWidth(90);
            tabellaAmmo.getColumn(COL_DESCR).setPreferredWidth(410);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            tabellaAmmo.getColumn(COL_COEFF).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_BENE).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_IMPAMM).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_PFISC).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_FISCALE).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_AMMFISC).setCellRenderer(rightRenderer);
            tabellaAmmo.getColumn(COL_DEDUC).setCellRenderer(rightRenderer);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            tabellaAmmo.getColumn(COL_GIORNI).setCellRenderer(centerRenderer);
            tabellaAmmo.getColumn(COL_DTACQ).setCellRenderer(centerRenderer);

            TableColumn tc = tabellaAmmo.getColumnModel().getColumn(BOOLEAN_COL);
            if (tabellaAmmo.getRowCount() != 0) {
                select = new SelectAllHeader(tabellaAmmo, BOOLEAN_COL);
                select.impostaListener(this);
                tc.setHeaderRenderer(select);
            }

        } else {
            tabellaAmmo.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{}));
        }

        totaleciv.setText("" + new FDouble(totammciv, "###,###,##0.00", 0));
        totalefis.setText("" + new FDouble(totammfis, "###,###,##0.00", 0));
        totalededuc.setText("" + new FDouble(totammded, "###,###,##0.00", 0));
        double ripresa = totammciv - totammded;
        scostamento.setText("" + new FDouble(ripresa, "###,###,##0.00", 2));

    }

    private void registraPrimaNota(HashMap totali, double fondoc, double fondof, double scostamento) {

        String querycat = "SELECT * FROM categoriebs WHERE catbsid = " + id[tabellaRisultato.getSelectedRow()];
        QueryInterrogazioneClient qcat = new QueryInterrogazioneClient(connAz, querycat);
        qcat.apertura();
        rCat = qcat.successivo();
        qcat.chiusura();

        //prende parametri
        String prop = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "pem");
        String qry = "SELECT * FROM puntiemdoc WHERE pedcod = \"" + prop + "\"";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() == 1) {
            Record r = q.successivo();
            puntoemdocid = r.leggiIntero("pedid");
        } else {
            JOptionPane.showMessageDialog(this, "Assente punto emissione documenti su parametri", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }
        q.chiusura();

        String dtelab = "";

        if (!dataregamm.getDataStringa().equals(dtes[1])) {
            dtelab = new Data(dataregamm.getText().trim(), Data.GG_MM_AAAA).formatta(Data.AAAA_MM_GG, "-");
        } else {
            dtelab = dtes[1];
        }
        // salvataggio documento
        Record rmdi = new Record();
        rmdi.insElem("movdocnumpe", (int) 0);
        rmdi.insElem("movdocdtpe", "0000-00-00");
        rmdi.insElem("movdocnumrpe", (int) 0);
        rmdi.insElem("movdocdtrpe", "0000-00-00");
        rmdi.insElem("movdocnumre", "");
        rmdi.insElem("movdocdtre", "0000-00-00");
        rmdi.insElem("movdocnumpu", (int) 0);
        rmdi.insElem("movdocdtpu", "0000-00-00");
        rmdi.insElem("movdocsufint1", "");
        rmdi.insElem("movdocnumint1", (int) 0);
        rmdi.insElem("movdocdtint1", "0000-00-00");
        rmdi.insElem("movdocsufint2", "");
        rmdi.insElem("movdocnumint2", (int) 0);
        rmdi.insElem("movdocdtint2", "0000-00-00");
        rmdi.insElem("movdocsufft", "");
        rmdi.insElem("movdocnumft", (int) 0);
        rmdi.insElem("movdocdtft", "0000-00-00");
        rmdi.insElem("movdocsufiva", "");
        rmdi.insElem("movdocnumiva", (int) 0);
        rmdi.insElem("movdocdtiva", "0000-00-00");

        Vector righePn = new Vector();
        int documentoid = idDoc1;
        int depid = -1;
        String qrydep = "SELECT docdepid FROM documenti WHERE docid=" + idDoc1;
        QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qrydep);
        query.apertura();
        Record rdep = query.successivo();
        query.chiusura();
        if (rdep != null) {
            if (rdep.leggiIntero("docdepid") != -1) {
                depid = rdep.leggiIntero("docdepid");
            }
        }

        if (depid == -1) {
            String qrydep2 = "SELECT depid,deptipo,deptiponew FROM depositi WHERE deptipo=6 OR deptiponew=1";
            QueryInterrogazioneClient query2 = new QueryInterrogazioneClient(connAz, qrydep2);
            query2.apertura();
            Record r2 = query2.successivo();
            query2.chiusura();

            if (r2 != null) {
                depid = r2.leggiIntero("depid");
            }
        }

        int numeroriga = 1;
//        if (flagPnRiep) {
//        double ammdeducib = Formattazione.estraiDouble(totalededuc.getText().trim());
        if (fondoc > 0) {
            if (totali.size() == 0) {
                Record rpn = new Record();
                rpn.insElem("mpnriga", numeroriga);
                rpn.insElem("mpnpdcid", idpdcA);
                rpn.insElem("mpncaucont", idJcau1);
                rpn.insElem("mpndescr", "AMMORTAMENTO ESERCIZIO");
                rpn.insElem("mpndescragg", "");
                rpn.insElem("mpndare", fondoc);
                rpn.insElem("mpnavere", (double) 0);
                rpn.insElem("mpndarev", fondoc);
                rpn.insElem("mpnaverev", (double) 0);
                rpn.insElem("mpnvaluta", (int) 2);
                rpn.insElem("mpncambio", (double) 1);
                rpn.insElem("mpnscoperto", "C");
                righePn.addElement(rpn);
                numeroriga++;

                //            if (ripresabil > 0) {//diminuisce i costi
                //                rpn = new Record();
                //                rpn.insElem("mpnriga", numeroriga);
                //                rpn.insElem("mpnpdcid", idpdcC);
                //                rpn.insElem("mpncaucont", idJcau1);
                //                rpn.insElem("mpndescr", "RIPRESA AMMORTAMENTO NON DEDUCIB.");
                //                rpn.insElem("mpndescragg", "");
                //                rpn.insElem("mpndare", ripresabil);
                //                rpn.insElem("mpnavere", (double) 0);
                //                rpn.insElem("mpndarev", ripresabil);
                //                rpn.insElem("mpnaverev", (double) 0);
                //                rpn.insElem("mpnvaluta", (int) 2);
                //                rpn.insElem("mpncambio", (double) 1);
                //                rpn.insElem("mpnscoperto", "C");
                //                righePn.addElement(rpn);
                //                numeroriga++;
                //            }
                //            if (ripresabil < 0) {//aumenta i ricavi
                //                rpn = new Record();
                //                rpn.insElem("mpnriga", numeroriga);
                //                rpn.insElem("mpnpdcid", idpdcR);
                //                rpn.insElem("mpncaucont", idJcau1);
                //                rpn.insElem("mpndescr", "RIPRESA AMMORTAMENTO AGEVOLATO");
                //                rpn.insElem("mpndescragg", "");
                //                rpn.insElem("mpndare", (double) 0);
                //                rpn.insElem("mpnavere", ripresabil * (-1));
                //                rpn.insElem("mpndarev", (double) 0);
                //                rpn.insElem("mpnaverev", ripresabil * (-1));
                //                rpn.insElem("mpnvaluta", (int) 2);
                //                rpn.insElem("mpncambio", (double) 1);
                //                rpn.insElem("mpnscoperto", "C");
                //                righePn.addElement(rpn);
                //                numeroriga++;
                //            }
                rpn = new Record();
                rpn.insElem("mpnriga", numeroriga);
                
                if(rCat.leggiIntero("catbsamminconto")==1)
                {
                    rpn.insElem("mpnpdcid", rCat.leggiIntero("catbsimmpdcid"));
                }
                else
                {                    
                    rpn.insElem("mpnpdcid", idpdcF);
                }
                rpn.insElem("mpncaucont", idJcau1);
                rpn.insElem("mpndescr", "AMMORTAMENTO ESERCIZIO");
                rpn.insElem("mpndescragg", "");
                rpn.insElem("mpndare", (double) 0);
                rpn.insElem("mpnavere", fondoc);
                rpn.insElem("mpndarev", (double) 0);
                rpn.insElem("mpnaverev", fondoc);
                rpn.insElem("mpnvaluta", (int) 2);
                rpn.insElem("mpncambio", (double) 1);
                rpn.insElem("mpnscoperto", "C");
                righePn.addElement(rpn);
                numeroriga++;
            } else if (totali.size() > 0) {
                double tot = 0.00;
                double dif = 0.00;
                Record rpn = new Record();
                Iterator it = totali.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();

                    tot += (Double) entry.getValue();

                    if (numeroriga == totali.size()) {
                        dif = fondoc - tot;
                    }
                    rpn = new Record();
                    rpn.insElem("mpnriga", numeroriga);
                    rpn.insElem("mpnpdcid", (Integer) entry.getKey());
                    rpn.insElem("mpncaucont", idJcau1);
                    rpn.insElem("mpndescr", "AMMORTAMENTO ESERCIZIO");
                    rpn.insElem("mpndescragg", "");
                    rpn.insElem("mpndare", (Double) entry.getValue() + dif);
                    rpn.insElem("mpnavere", (double) 0);
                    rpn.insElem("mpndarev", (Double) entry.getValue() + dif);
                    rpn.insElem("mpnaverev", (double) 0);
                    rpn.insElem("mpnvaluta", (int) 2);
                    rpn.insElem("mpncambio", (double) 1);
                    rpn.insElem("mpnscoperto", "C");
                    righePn.addElement(rpn);
                    numeroriga++;
                }

                rpn = new Record();
                rpn.insElem("mpnriga", numeroriga);
                rpn.insElem("mpnpdcid", idpdcF);
                rpn.insElem("mpncaucont", idJcau1);
                rpn.insElem("mpndescr", "AMMORTAMENTO ESERCIZIO");
                rpn.insElem("mpndescragg", "");
                rpn.insElem("mpndare", (double) 0);
                rpn.insElem("mpnavere", fondoc);
                rpn.insElem("mpndarev", (double) 0);
                rpn.insElem("mpnaverev", fondoc);
                rpn.insElem("mpnvaluta", (int) 2);
                rpn.insElem("mpncambio", (double) 1);
                rpn.insElem("mpnscoperto", "C");
                righePn.addElement(rpn);
            }
        }

        Record risdoc = FunzioniDB.salvaDocumento(connAz, connCom, esContabile,
                dtelab, documentoid, depid, puntoemdocid,
                -1, -1, -1, rmdi, new Record(), new Vector(), new Vector(),
                new Record(), new Record(), righePn, new Vector(), EnvJazz.INS, 2, new Vector(), new Vector(), new Vector(), new Vector(), new Vector(), 0);
        //prende ID
        if (risdoc.leggiIntero("esito") == EnvJazz.SUCCESSO) {
            int idmovJazz = risdoc.leggiIntero("mdid");
            JOptionPane.showMessageDialog(this, "Registrazione PRIMA NOTA effettuata !", "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

            qry = "SELECT mbsid FROM movimentibs"
                    + " INNER JOIN cespiti ON movimentibs.mbscespid = cespiti.cespid"
                    + " INNER JOIN categoriebs ON cespiti.cespcategid = categoriebs.catbsid"
                    + " WHERE cespcategid = " + rCat.leggiIntero("catbsid")
                    + " AND mbsammortamento != 0"
                    + " AND mbstipomov = 2"
                    + " AND mbsdata = \"" + dtelab + "\"";
            q = new QueryInterrogazioneClient(connAz, qry);
            q.apertura();
            for (int i = 0; i < q.numeroRecord(); i++) {
                Record rv = q.successivo();
                QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, "UPDATE movimentibs SET mbsmovjazzid = " + idmovJazz
                        + " WHERE mbsid = " + rv.leggiIntero("mbsid"));
                qa.esecuzione();
            }
            q.chiusura();
            //
            String qry2 = "SELECT logamm_id FROM log_amm WHERE logamm_eserc = \"" + esContabile + "\" AND logamm_catid = " + rCat.leggiIntero("catbsid");
            QueryInterrogazioneClient qe = new QueryInterrogazioneClient(connAz, qry2);
            qe.apertura();
            Record re = qe.successivo();
            qe.chiusura();

            Record r = new Record();
            r.insElem("logamm_eserc", esContabile);
            r.insElem("logamm_catid", rCat.leggiIntero("catbsid"));
            r.insElem("logamm_data", dtelab);
            r.insElem("logamm_fisc", (int) 1);
            r.insElem("logamm_civ", (int) 1);
            r.insElem("logamm_importoc", fondoc);
            r.insElem("logamm_importof", fondof);
            r.insElem("logamm_importod", scostamento);
            r.insElem("logamm_movjazzid", idmovJazz);
            r.insElem("logamm_chiuso", (int) 0); //simulazione
            if (idmovJazz > 0) {
                String documento = "";
                documento = cercaDocumento(idmovJazz, esContabile);
                r.insElem("logamm_movdoccg", documento);
            } else {
                r.insElem("logamm_movdoccg", "");
            }
            r.convalida(true);
            if (re != null) {
                r.insElem("logamm_id", re.leggiIntero("logamm_id"));
                QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "log_amm", "logamm_id", r);
                qa.esecuzione();
            } else {
                QueryAggiornamentoClient qi = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "log_amm", "logamm_id", r);
                qi.esecuzione();
            }
        }
    }

    private void eliminazioneAmmortamenti(int idcateg, String dt, int idjazz) {
        Record r = new Record();
        String qry = "SELECT cespid FROM cespiti WHERE cespcategid = " + idcateg;
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            for (int i = 1; i <= q.numeroRecord(); i++) {
                r = q.successivo();
                int cespid = r.leggiIntero("cespid");
                String qryd = "DELETE FROM movimentibs"
                        + " WHERE mbsdata = \"" + dt + "\""
                        + " AND mbseserc = \"" + esContabile + "\""
                        + " AND mbscespid = " + cespid
                        + " AND mbstipomov = 2";

                QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qryd);
                qd.esecuzione();
            }
        }
        q.chiusura();
        //
        String qrx = "DELETE FROM log_amm WHERE logamm_data = \"" + dt + "\""
                + " AND logamm_catid = " + idcateg;
        QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrx);
        qd.esecuzione();

        if (idjazz > 0) {
            QueryAggiornamentoClient qm = new QueryAggiornamentoClient(connAz, "DELETE FROM primanota" + esContabile + " WHERE mpnmovdocid = " + idjazz);
            qm.esecuzione();
            qm = new QueryAggiornamentoClient(connAz, "DELETE FROM movimentidoc" + esContabile + " WHERE movdocid = " + idjazz);
            qm.esecuzione();

        }
    }

    private void eliminaTutteSimulazioni() {
        //elimina tutte le simulazioni di ammortamento eseguite durante l'esercio 
        String qry = "SELECT * FROM log_amm WHERE logamm_eserc = \"" + esContabile + "\""
                + " AND logamm_catid = " + rCat.leggiIntero("catbsid")
                + " AND logamm_fisc = 1 AND logamm_civ = 1"
                + " AND logamm_movjazzid < 1";
        QueryInterrogazioneClient qe = new QueryInterrogazioneClient(connAz, qry);
        qe.apertura();
        if (qe.numeroRecord() > 0) {
            for (int i = 1; i <= qe.numeroRecord(); i++) {
                Record re = qe.successivo();
                String dt = re.leggiStringa("logamm_data");
                eliminazioneAmmortamenti(rCat.leggiIntero("catbsid"), dt, -1);
            }
        }
        qe.chiusura();
    }

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    class CambioTabCiv implements CellEditorListener {

        public void editingCanceled(javax.swing.event.ChangeEvent changeEvent) {
        }

        public void editingStopped(javax.swing.event.ChangeEvent changeEvent) {

            int j = tabellaAmmo.getSelectedRow();
            if (modif[j]) {
                double totamm = Formattazione.estraiDouble(totaleciv.getText().trim());
                totamm += (Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM))) * (-1));
                double valoreciv = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_BENE)));
                double vfondociv = fondoc[j];
                double ggcalc = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_GIORNI)));
                int gganno = gg[j];
                double quota = ((double) ggcalc / gganno);
                double residuo1C = OpValute.arrotondaMat(valoreciv - vfondociv, 2);
                double perc = Formattazione.estraiDouble(editpercciv.getText().trim());
                double valammciv = OpValute.arrotondaMat(valoreciv * (perc / 100) * quota, 2);
                if (valammciv > residuo1C && residuo1C > 0.00) {
                    valammciv = residuo1C;
                }
                tabellaAmmo.setValueAt("" + new FDouble(valammciv, "##0.00", 0), j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM));
                totamm += valammciv;
                totaleciv.setText("" + new FDouble(totamm, "###,###,##0.00", 0));
                double totammfisc = Formattazione.estraiDouble(totalededuc.getText().trim());
                double ripresa = totamm - totammfisc;
                scostamento.setText("" + new FDouble(ripresa, "###,###,##0.00", 2));

                repaint();
            }
        }
    }

    class CambioTabFisc implements CellEditorListener {

        public void editingCanceled(javax.swing.event.ChangeEvent changeEvent) {
        }

        public void editingStopped(javax.swing.event.ChangeEvent changeEvent) {

            int j = tabellaAmmo.getSelectedRow();
            if (modif[j]) {
                double totamm = Formattazione.estraiDouble(totalefis.getText().trim());
                double totdeduc = Formattazione.estraiDouble(totalededuc.getText().trim());
                double exvalore = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC)));
                totamm += exvalore * (-1);
                double exdeducib = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_DEDUC)));
                totdeduc += exdeducib * (-1);
                double valorefis = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_FISCALE)));
                double vfondofis = fondof[j];
                double residuoF = OpValute.arrotondaMat(valorefis - vfondofis, 2);
                double perc = Formattazione.estraiDouble(editpercfis.getText().trim());
                double valammfis = OpValute.arrotondaMat(valorefis * (perc / 100), 2);
                if (valammfis > residuoF && residuoF > 0.00) {
                    valammfis = residuoF;
                }
                tabellaAmmo.setValueAt("" + new FDouble(valammfis, "##0.00", 0), j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC));
                //percentuale = (exdeducib*100)/exvalore
                double valdeducib = ((valammfis * ((exdeducib * 100) / exvalore)) / 100);
                tabellaAmmo.setValueAt("" + new FDouble(valdeducib, "##0.00", 0), j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_DEDUC));
                totamm += valammfis;
                totdeduc += valdeducib;
                totalefis.setText("" + new FDouble(totamm, "###,###,##0.00", 0));
                totalededuc.setText("" + new FDouble(totdeduc, "###,###,##0.00", 0));
                double totammciv = Formattazione.estraiDouble(totaleciv.getText().trim());
                double ripresa = totammciv - totdeduc;
                scostamento.setText("" + new FDouble(ripresa, "###,###,##0.00", 2));

                repaint();
            }
        }
    }

    class CambioTabGg implements CellEditorListener {

        public void editingCanceled(javax.swing.event.ChangeEvent changeEvent) {
        }

        public void editingStopped(javax.swing.event.ChangeEvent changeEvent) {

            int j = tabellaAmmo.getSelectedRow();
            if (modif[j]) {
                double totammciv = Formattazione.estraiDouble(totaleciv.getText().trim());
                totammciv += (Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM))) * (-1));
                double valoreciv = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_BENE)));
                double vfondociv = fondoc[j];

                double totammfis = Formattazione.estraiDouble(totalefis.getText().trim());                
                totammfis += (Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC))) * (-1));
                double valorefisc = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_FISCALE)));
                double vfondofisc = fondof[j];

                double ggcalc = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_GIORNI)));
                int gganno = gg[j];
                double quota = ((double) ggcalc / gganno);

                double residuo1C = OpValute.arrotondaMat(valoreciv - vfondociv, 2);
                double percC = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_COEFF)));
                double valammciv = OpValute.arrotondaMat(valoreciv * (percC / 100) * quota, 2);
                if (valammciv > residuo1C && residuo1C > 0.00) {
                    valammciv = residuo1C;
                }
                totammciv += valammciv;

                double residuo1F = OpValute.arrotondaMat(valorefisc - vfondofisc, 2);
                double percF = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_PFISC)));
                double valammfisc = OpValute.arrotondaMat(valorefisc * (percF / 100) * quota, 2);
                if (valammfisc > residuo1F && residuo1F > 0.00) {
                    valammfisc = residuo1F;
                }
                totammfis += valammfisc;
                
                double totdeduc = Formattazione.estraiDouble(totalededuc.getText().trim());
                double exvalore = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC)));
                double exdeducib = Formattazione.estraiDouble((String) tabellaAmmo.getValueAt(j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_DEDUC)));
                totdeduc += exdeducib * (-1);
                double valdeducib = ((valammfisc * ((exdeducib * 100) / exvalore)) / 100);
                totdeduc += valdeducib;
                
                tabellaAmmo.setValueAt("" + new FDouble(valammciv, "##0.00", 0), j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_IMPAMM));
                tabellaAmmo.setValueAt("" + new FDouble(valammfisc, "##0.00", 0), j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_AMMFISC));
                tabellaAmmo.setValueAt("" + new FDouble(valdeducib, "##0.00", 0), j, FunzioniUtilita.getColumnIndex(tabellaAmmo, COL_DEDUC));
                
                totaleciv.setText("" + new FDouble(totammciv, "###,###,##0.00", 0));
                totalefis.setText("" + new FDouble(totammfis, "###,###,##0.00", 0));
                totalededuc.setText("" + new FDouble(totdeduc, "###,###,##0.00", 0));

                double ripresa = totammciv - totdeduc;
                scostamento.setText("" + new FDouble(ripresa, "###,###,##0.00", 2));
                repaint();
            }
        }
    }

    private String cercaDocumento(int idmov, String es) {
        String doc = "";
        String qry = "SELECT doccod,movdocsufiva,movdocnumiva,movdocdtiva FROM movimentidoc" + es
                + " INNER JOIN documenti ON movimentidoc" + es + ".movdocdocumid = documenti.docid"
                + " WHERE movdocid = " + idmov;
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            Record r = q.successivo();
            String dt = (new Data(r.leggiStringa("movdocdtiva"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "-");

            doc = "Doc. " + r.leggiStringa("doccod")
                    + " sez. " + r.leggiStringa("movdocsufiva")
                    + "/" + r.leggiIntero("movdocnumiva")
                    + " del " + dt;
        }
        q.chiusura();
        return doc;
    }

    private void cancellaCalcoloAmmortamento(int riga, String dtCalc) {
        if ((esContabile.substring(4, 8)).compareTo(annobilchiuso) <= 0) {
            JOptionPane.showMessageDialog(this, "Il libro cespiti e' chiuso. Per continuare devi eliminare le chiusure!", "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            return;
        }

        if (idM[riga] > 0) {
            String qry = "SELECT movdocstampaboll FROM movimentidoc" + esContabile + " WHERE movdocid = " + idM[riga];
            QueryInterrogazioneClient q1 = new QueryInterrogazioneClient(connAz, qry);
            q1.apertura();
            Record r1 = null;
            if (q1.numeroRecord() > 0) {
                r1 = q1.successivo();
            }
            q1.chiusura();

            if (r1 != null && r1.leggiIntero("movdocstampaboll") == 1) {
                JOptionPane.showMessageDialog(this, "Impossibile eliminare gli ammortamenti, la stampa del libro giornale e' gia' stata effettuata!", "",
                        JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                return;
            } else {
                String documento = "";
                documento = cercaDocumento(idM[riga], esContabile);
                if (documento.equals("")) {
                    JOptionPane.showMessageDialog(this, "ERRORE il documento eliminato in contabilita' (id) " + idM[riga], "",
                            JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
                    return;
                }
                int ris = JOptionPane.showConfirmDialog(this, "Gli ammortamenti sono registrati in contabilita'.\n"
                        + " " + documento
                        + " Per continuare devo eliminare il documento contabile!", "PROCEDO ?",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
                if (ris == JOptionPane.NO_OPTION) {
                    return;
                }
            }
        }

        int ris = JOptionPane.showConfirmDialog(this, "Confermi la CANCELLAZIONE degli ammortamenti ?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            this.setCursor(new Cursor(Cursor.WAIT_CURSOR));

            eliminazioneAmmortamenti(id[riga], dtCalc, idM[riga]);

            this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            JOptionPane.showMessageDialog(this, "Eliminazione terminata", "",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));

            tabellaAmmo.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{}));

        }
    }

    private void stampaAmmo(int riga) {
        HashMap par = new HashMap();

        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        par.put("azienda", azienda + ": " + raz.leggiStringa("nome") + "  P.Iva " + raz.leggiStringa("partitaiva"));
        par.put("escontabile", esercizioScelto.substring(4, 8));
        par.put("datastampa", (new Data().formatta(Data.AAAA_MM_GG, "-")));

        if (jComboBeni.getSelectedIndex() == 0) {
            par.put("tipobeni", "IMMOBILIZZAZIONI MATERIALI");
        } else if (jComboBeni.getSelectedIndex() == 1) {
            par.put("tipobeni", "IMMOBILIZZAZIONI IMMATERIALI");
        } else if (jComboBeni.getSelectedIndex() == 2) {
            par.put("tipobeni", "IMMOBILIZZAZIONI FINANZIARIE");
        } else {
            par.put("tipobeni", "");
        }

        par.put("metodo", "AMMORTAMENTO ");

        if (!dataregamm.getDataStringa().equals(dtes[1])) {
            par.put("tipobil", "AMMORTAMENTO DI VERIFICA");
        } else {
            par.put("tipobil", "AMMORTAMENTO DI CHIUSURA");
        }
        par.put("categoria", descrcategoria.getText().trim());

        BusinessLogicCespiti.elaboraStampaAmmortamenti(connAz.conn, azienda, esercizioScelto, dtCalcolo, id[riga], (int) 0);

        if (!FunzioniDB.esisteReport("cspammcategnuova.jasper")) {
            JOptionPane.showMessageDialog(this,
                    "Report cspammcategnuova.jasper non trovato", "Errore", JOptionPane.ERROR_MESSAGE);
            return;
        }
        FunzioniDB.leggiReport("cspammcategnuova.jasper");

        DialogReportJR dr = new DialogReportJR(formGen, EnvJazz.crERW, "cspammcategnuova.jasper", par);
        UtilForm.posRelSchermo(dr, UtilForm.CENTRO);
        dr.show();

        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    private void caricaValoriContabiliCosto() {
        if (!elaboraStampaControlloCostoAmmortamenti) {
            BusinessLogicCespiti.elaboraStampaControlloCostoAmmortamenti(connAz.conn, azienda, esercizioScelto, vcateg);
            elaboraStampaControlloCostoAmmortamenti = true;
        }
        String chiave = "..";
        String chiaveattu = "";
        double saldo = 0;
        double civile = 0;
        double residuo = 0;
        double differenza = 0;
        String qry = "SELECT * FROM tmpammpdccosto LEFT JOIN tmpammcateg ON costoid = catcostoid"
                + " WHERE catcostoid > 0 OR ((catammcivdbl>0 OR catammfisdbl>0 OR catammdetrdbl > 0) AND catcostoid =-1)"
                + " ORDER BY costopdc,catmin,catcod";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(EnvJazz.connReport, qry);

        q.apertura();

        if (q.numeroRecord() > 0) {
            final String[] names = {COL_conto, COL_voce, COL_aciv, COL_saldo, COL_ripresa, COL_categ, COL_catciv, COL_catfis, COL_catdetr, COL_diff, COL_nota};
            final Object[][] data = new Object[q.numeroRecord()][names.length];
            for (int i = 0; i < q.numeroRecord(); i++) {
                Record r = q.successivo();
                chiaveattu = r.leggiStringa("costopdc");
                if (!chiave.equals(chiaveattu)) {
                    if (i != 0) {
                        if (differenza != 0) {
                            data[i - 1][9] = "" + new FDouble(differenza, "###,###,##0.00", 2);
                            data[i - 1][10] = "Errore";
                        } else {
                            data[i - 1][9] = "";
                            data[i - 1][10] = "Ok";
                        }
                    } else {
                        data[i][9] = "";
                        data[i][10] = "";
                    }
                    data[i][0] = r.leggiStringa("costopdc");
                    if (r.leggiStringa("costopdc").equals("")) {
                        data[i][1] = "ASSENTE";
                    } else {
                        data[i][1] = r.leggiStringa("costodescr");
                    }
                    data[i][2] = r.leggiStringa("costoammcivx");
                    civile = r.leggiDouble("costoammciv");
                    data[i][3] = r.leggiStringa("costosaldox");
                    saldo = r.leggiDouble("costosaldo");
                    residuo = r.leggiDouble("costoammciv") - r.leggiDouble("costoammdetr");
                    differenza = civile - saldo;
                    data[i][4] = "" + new FDouble(residuo, "###,###,##0.00", 2);
                    data[i][5] = r.leggiStringa("catcod") + " - " + r.leggiStringa("categ");
                    data[i][6] = r.leggiStringa("catammcivx");
                    data[i][7] = r.leggiStringa("catammfisx");
                    data[i][8] = r.leggiStringa("catammdetrx");
                    chiave = chiaveattu;
                } else {
                    data[i][0] = "";
                    data[i][1] = "";
                    data[i][2] = "";
                    data[i][3] = "";
                    data[i][4] = "";

                    data[i][5] = r.leggiStringa("catcod") + " - " + r.leggiStringa("categ");
                    data[i][6] = r.leggiStringa("catammcivx");
                    data[i][7] = r.leggiStringa("catammfisx");
                    data[i][8] = r.leggiStringa("catammdetrx");
                    data[i][9] = "";
                    data[i][10] = "";
                    if (i == (tabellaCosti.getRowCount() - 1)) {
                        if (differenza != 0) {
                            data[tabellaCosti.getRowCount() - 1][9] = "" + new FDouble(differenza, "###,###,##0.00", 2);
                            data[tabellaCosti.getRowCount() - 1][10] = "Errore";
                        } else {
                            data[tabellaCosti.getRowCount() - 1][9] = "";
                            data[tabellaCosti.getRowCount() - 1][10] = "Ok";
                        }
                    }
                }
            }

            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    return false;
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };

            tabellaCosti.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabellaCosti, true);

            tabellaCosti.getColumn(COL_conto).setPreferredWidth(140);
            tabellaCosti.getColumn(COL_voce).setPreferredWidth(280);
            tabellaCosti.getColumn(COL_aciv).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_saldo).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_ripresa).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_categ).setPreferredWidth(380);
            tabellaCosti.getColumn(COL_catciv).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_catfis).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_catdetr).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_diff).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_nota).setPreferredWidth(80);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            tabellaCosti.getColumn(COL_aciv).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_saldo).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_ripresa).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catciv).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catfis).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catdetr).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_diff).setCellRenderer(rightRenderer);

        }

        q.chiusura();
    }

    private void caricaValoriContabiliCostoRiep() {
        String chiave = "..";
        String chiaveattu = "";

        ArrayList<Record> vr = new ArrayList();

        String qry = "SELECT catmin,catmindescr,costopdc,costodescr,costoammcivx,costoammciv,costosaldox,costosaldo,SUM(catammcivdbl) AS catammcivdbl,SUM(catammfisdbl) AS catammfisdbl,SUM(catammdetrdbl) AS catammdetrdbl"
                + " FROM tmpammpdccosto"
                + " LEFT JOIN tmpammcateg ON costoid = catcostoid"
                + " WHERE catcostoid > 0 OR ((catammcivdbl > 0 OR catammfisdbl > 0 OR catammdetrdbl > 0) AND catcostoid = -1)"
                + " GROUP BY catmin,costopdc"
                + " ORDER BY catmin,costopdc";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(EnvJazz.connReport, qry);
        q.apertura();
        Record rr = q.successivo();
        while (rr != null) {
            vr.add(rr);
            rr = q.successivo();
        }
        q.chiusura();

        if (!vr.isEmpty()) {
            final String[] names = {COL_catmin, COL_catciv, COL_catdetr, COL_conto, COL_voce, COL_saldo};
            final Object[][] data = new Object[vr.size()][names.length];
            for (int i = 0; i < vr.size(); i++) {
                Record r = vr.get(i);
                chiaveattu = r.leggiStringa("catmin");
                if (!chiave.equals(chiaveattu)) {
                    data[i][0] = r.leggiStringa("catmin") + " - " + r.leggiStringa("catmindescr");
                    data[i][1] = "" + new FDouble(r.leggiDouble("catammcivdbl"), "###,###,##0.00", 0);
                    data[i][2] = "" + new FDouble(r.leggiDouble("catammdetrdbl"), "###,###,##0.00", 0);

                    data[i][3] = r.leggiStringa("costopdc");
                    if (r.leggiStringa("costopdc").equals("")) {
                        data[i][4] = "ASSENTE";
                    } else {
                        data[i][4] = r.leggiStringa("costodescr");
                    }
                    data[i][5] = r.leggiStringa("costosaldox");
                    chiave = chiaveattu;
                } else {
                    data[i][0] = "";
                    data[i][1] = "";
                    data[i][2] = "";
                    data[i][3] = r.leggiStringa("costopdc");
                    if (r.leggiStringa("costopdc").equals("")) {
                        data[i][4] = "ASSENTE";
                    } else {
                        data[i][4] = r.leggiStringa("costodescr");
                    }
                    data[i][5] = r.leggiStringa("costosaldox");
                }
            }
            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    return false;
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };
            tabellaCosti.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabellaCosti, true);

            tabellaCosti.getColumn(COL_catmin).setPreferredWidth(420);
            tabellaCosti.getColumn(COL_catciv).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_catdetr).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_conto).setPreferredWidth(148);
            tabellaCosti.getColumn(COL_voce).setPreferredWidth(340);
            tabellaCosti.getColumn(COL_saldo).setPreferredWidth(80);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            tabellaCosti.getColumn(COL_saldo).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catciv).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catdetr).setCellRenderer(rightRenderer);

        }
    }

    private void caricaValoriContabiliFondo() {
        //
        BusinessLogicCespiti.elaboraControlloFondoAmmortamenti(connAz.conn, azienda, esercizioScelto);
        elaboraControlloFondoAmmortamentiCateg = true;
        //
        String chiave = "..";
        String chiaveattu = "";
        double saldo = 0;
        double differenza = 0;
//        ArrayList<Record> vfondi = new ArrayList();
        String qry = "SELECT tmpammpdcfondo.*,catid,catcod,categ,catcostoid,fondoprecx,fondoaammx,fondovarx,fondosvax,fondototx,fondotot"
                + " FROM tmpammpdcfondo"
                + " LEFT JOIN tmpammcateg ON pdcfondoid = catfondoid"
                + " LEFT JOIN tmpammcatfondo ON catid = fondocategid "
                + " WHERE catfondoid > 0 OR ((catammcivdbl > 0 OR catammfisdbl > 0 OR catammdetrdbl > 0) AND catfondoid =-1)"
                + " ORDER BY pdcfondopdc,catmin,catcostoid,catcod";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(EnvJazz.connReport, qry);
        q.apertura();

        if (q.numeroRecord() > 0) {
            final String[] names = {COL_conto, COL_voce, COL_saldo, COL_categ, COL_costo, COL_catpre, COL_catamm, COL_catvar, COL_catsva, COL_tot, COL_diff, COL_nota};
            final Object[][] data = new Object[q.numeroRecord()][names.length];
            for (int i = 0; i < q.numeroRecord(); i++) {
                Record r = q.successivo();
                chiaveattu = r.leggiStringa("pdcfondopdc");
                if (!chiave.equals(chiaveattu)) {
                    if (i != 0) {
                        if (differenza != 0) {
                            data[i - 1][10] = "" + new FDouble(differenza, "###,###,##0.00", 2);
                            data[i - 1][11] = "Errore";
                        } else {
                            data[i - 1][10] = "";
                            data[i - 1][11] = "Ok";
                        }
                    } else {
                        data[i][10] = "";
                        data[i][11] = "";
                    }
                    data[i][0] = r.leggiStringa("pdcfondopdc");
                    if (r.leggiStringa("pdcfondopdc").equals("")) {
                        data[i][1] = "ASSENTE";
                    } else {
                        data[i][1] = r.leggiStringa("pdcfondodescr");
                    }
                    data[i][2] = r.leggiStringa("pdcfondosaldox");
                    saldo = r.leggiDouble("pdcfondosaldo");
                    data[i][3] = r.leggiStringa("catcod") + " - " + r.leggiStringa("categ");
                    if (r.leggiIntero("catcostoid") > 0) {
                        String qryp = "SELECT pdcid,pdcmcs,pdcsottoconto FROM pianoconti WHERE pdcid = " + r.leggiIntero("catcostoid");
                        QueryInterrogazioneClient qp = new QueryInterrogazioneClient(connAz, qryp);
                        qp.apertura();
                        if (qp.numeroRecord() > 0) {
                            Record rp = qp.successivo();
                            if (rp.leggiStringa("pdcsottoconto").equals("")) {
                                data[i][4] = "ELIMINATO";
                            } else {
                                data[i][4] = rp.leggiStringa("pdcmcs");
                            }
                        }
                    } else {
                        data[i][4] = "ASSENTE";
                    }
                    data[i][5] = r.leggiStringa("fondoprecx");
                    data[i][6] = r.leggiStringa("fondoaammx");
                    data[i][7] = r.leggiStringa("fondovarx");
                    data[i][8] = r.leggiStringa("fondosvax");
                    data[i][9] = r.leggiStringa("fondototx");
//                    //esercizio precedente
//                    String esprec = BusinessLogicAmbiente.leggiEsercizioPrecedente(EnvJazz.connAmbiente.leggiConnessione(), azienda, esercizioScelto);
//                    String[] dtesp = BusinessLogicAmbiente.leggiEsercizio(azienda, esprec);
//                    double[] fondop = BusinessLogicCespiti.calcoloFondoCategoria(connAz, azienda, r.leggiIntero("catid"), esprec, dtesp[1]);
//                    double fondoprec = fondop[1];
//                    data[i][5] = "" + new FDouble(fondoprec, "###,###,##0.00", 0);
//                    //esercizio scelto
//                    double[] fondoatt = BusinessLogicCespiti.calcoloFondoCategoria(connAz, azienda, r.leggiIntero("catid"), esercizioScelto, "");
//                    double fondoaamm = fondoatt[2];
//                    data[i][6] = "" + new FDouble(fondoaamm, "###,###,##0.00", 0);
//                    double fondoavar = fondoatt[3];
//                    data[i][7] = "" + new FDouble(fondoavar, "###,###,##0.00", 0);
//                    double fondoasva = fondoatt[4];
//                    data[i][8] = "" + new FDouble(fondoasva, "###,###,##0.00", 0);
//                    double fondoa = fondoatt[1];
//                    data[i][9] = "" + new FDouble(fondoa, "###,###,##0.00", 0);
                    differenza = r.leggiDouble("fondotot") - saldo;
                    chiave = chiaveattu;
                } else {
                    data[i][0] = "";
                    data[i][1] = "";
                    data[i][2] = "";
                    data[i][3] = r.leggiStringa("catcod") + " - " + r.leggiStringa("categ");
                    if (r.leggiIntero("catcostoid") > 0) {
                        String qryp = "SELECT pdcid,pdcmcs,pdcsottoconto FROM pianoconti WHERE pdcid = " + r.leggiIntero("catcostoid");
                        QueryInterrogazioneClient qp = new QueryInterrogazioneClient(connAz, qryp);
                        qp.apertura();
                        if (qp.numeroRecord() > 0) {
                            Record rp = qp.successivo();
                            if (rp.leggiStringa("pdcsottoconto").equals("")) {
                                data[i][4] = "ELIMINATO";
                            } else {
                                data[i][4] = rp.leggiStringa("pdcmcs");
                            }
                        }
                    } else {
                        data[i][4] = "ASSENTE";
                    }
                    //esercizio precedente
//                    String esprec = BusinessLogicAmbiente.leggiEsercizioPrecedente(EnvJazz.connAmbiente.leggiConnessione(), azienda, esercizioScelto);
//                    String[] dtesp = BusinessLogicAmbiente.leggiEsercizio(azienda, esprec);
//                    double[] fondop = BusinessLogicCespiti.calcoloFondoCategoria(connAz, azienda, r.leggiIntero("catid"), esprec, dtesp[1]);
//                    double fondoprec = fondop[1];
//                    data[i][5] = "" + new FDouble(fondoprec, "###,###,##0.00", 0);
//                    //esercizio scelto
//                    double[] fondoatt = BusinessLogicCespiti.calcoloFondoCategoria(connAz, azienda, r.leggiIntero("catid"), esercizioScelto, "");
//                    double fondoaamm = fondoatt[2];
//                    data[i][6] = "" + new FDouble(fondoaamm, "###,###,##0.00", 0);
//                    double fondoavar = fondoatt[3];
//                    data[i][7] = "" + new FDouble(fondoavar, "###,###,##0.00", 0);
//                    double fondoasva = fondoatt[4];
//                    data[i][8] = "" + new FDouble(fondoasva, "###,###,##0.00", 0);
//                    double fondoa = fondoatt[1];
//                    data[i][9] = "" + new FDouble(fondoa, "###,###,##0.00", 0);
                    data[i][5] = r.leggiStringa("fondoprecx");
                    data[i][6] = r.leggiStringa("fondoaammx");
                    data[i][7] = r.leggiStringa("fondovarx");
                    data[i][8] = r.leggiStringa("fondosvax");
                    data[i][9] = r.leggiStringa("fondototx");
                    if (i == (tabellaCosti.getRowCount() - 1)) {
                        if (differenza != 0) {
                            data[tabellaCosti.getRowCount() - 1][10] = "" + new FDouble(differenza, "###,###,##0.00", 2);
                            data[tabellaCosti.getRowCount() - 1][11] = "Errore";
                        } else {
                            data[tabellaCosti.getRowCount() - 1][10] = "";
                            data[tabellaCosti.getRowCount() - 1][11] = "Ok";
                        }
                    }
                }
            }

            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    return false;
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };

            tabellaCosti.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabellaCosti, true);

            tabellaCosti.getColumn(COL_conto).setPreferredWidth(140);
            tabellaCosti.getColumn(COL_voce).setPreferredWidth(280);
            tabellaCosti.getColumn(COL_saldo).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_categ).setPreferredWidth(380);
            tabellaCosti.getColumn(COL_costo).setPreferredWidth(130);
            tabellaCosti.getColumn(COL_catpre).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_catamm).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_catvar).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_catsva).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_tot).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_diff).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_nota).setPreferredWidth(80);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

            tabellaCosti.getColumn(COL_saldo).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catsva).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catpre).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catamm).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catvar).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_tot).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_diff).setCellRenderer(rightRenderer);
        }
        q.chiusura();
    }

    private void caricaValoriContabiliFondoRiep() {
        String chiave = "..";
        String chiaveattu = "";

        ArrayList<Record> vr = new ArrayList();
        if (!elaboraControlloFondoAmmortamentiCateg) {
            BusinessLogicCespiti.elaboraControlloFondoAmmortamentiCateg(connAz.conn, azienda, esercizioScelto);
            elaboraControlloFondoAmmortamentiCateg = true;
        }
        String qry = "SELECT catmin,catmindescr,pdcfondopdc,pdcfondodescr,pdcfondosaldox,pdcfondosaldo,fondoprecx,fondoaammx,fondoaamm,fondovarx,fondosvax,fondototx"
                + " FROM tmpammpdcfondo"
                + " LEFT JOIN tmpammcateg ON pdcfondoid = catfondoid"
                + " LEFT JOIN tmpammcatfondo ON fondoid = catfondoid "
                + " WHERE catfondoid > 0 OR ((catammcivdbl > 0 OR catammfisdbl > 0 OR catammdetrdbl > 0) AND catfondoid = -1)"
                + " GROUP BY catmin,pdcfondopdc"
                + " ORDER BY catmin,pdcfondopdc";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(EnvJazz.connReport, qry);
        q.apertura();
        Record rr = q.successivo();
        while (rr != null) {
            vr.add(rr);
            rr = q.successivo();
        }
        q.chiusura();

        if (!vr.isEmpty()) {
            final String[] names = {COL_catmin, COL_catpre, COL_catamm, COL_catvar, COL_catsva, COL_tot, COL_conto, COL_voce, COL_saldo};
            final Object[][] data = new Object[vr.size()][names.length];
            for (int i = 0; i < vr.size(); i++) {
                Record r = vr.get(i);
                chiaveattu = r.leggiStringa("catmin");
                if (!chiave.equals(chiaveattu)) {
                    data[i][0] = r.leggiStringa("catmin") + " - " + r.leggiStringa("catmindescr");
                    if (!r.nullo("fondoprecx")) {
                        data[i][1] = r.leggiStringa("fondoprecx");
                    } else {
                        data[i][1] = "";
                    }

                    if (!r.nullo("fondoaammx")) {
                        data[i][2] = r.leggiStringa("fondoaammx");
                    } else {
                        data[i][2] = "";
                    }

                    if (!r.nullo("fondovarx")) {
                        data[i][3] = r.leggiStringa("fondovarx");
                    } else {
                        data[i][3] = "";
                    }

                    if (!r.nullo("fondosvax")) {
                        data[i][4] = r.leggiStringa("fondosvax");
                    } else {
                        data[i][4] = "";
                    }

                    if (!r.nullo("fondototx")) {
                        data[i][5] = r.leggiStringa("fondototx");
                    } else {
                        data[i][5] = "";
                    }

                    data[i][6] = r.leggiStringa("pdcfondopdc");
                    if (r.leggiStringa("pdcfondopdc").equals("")) {
                        data[i][7] = "ASSENTE";
                    } else {
                        data[i][7] = r.leggiStringa("pdcfondodescr");
                    }
                    data[i][8] = r.leggiStringa("pdcfondosaldox");
                    chiave = chiaveattu;
                } else {
                    data[i][0] = "";
                    data[i][1] = "";
                    data[i][2] = "";
                    data[i][3] = "";
                    data[i][4] = "";
                    data[i][5] = "";
                    data[i][6] = r.leggiStringa("pdcfondopdc");
                    if (r.leggiStringa("pdcfondopdc").equals("")) {
                        data[i][7] = "ASSENTE";
                    } else {
                        data[i][7] = r.leggiStringa("pdcfondodescr");
                    }
                    data[i][8] = r.leggiStringa("pdcfondosaldox");
                }
            }
            TableModel dataModel = new AbstractTableModel() {
                public int getColumnCount() {
                    return names.length;
                }

                public int getRowCount() {
                    return data.length;
                }

                public Object getValueAt(int row, int col) {
                    return data[row][col];
                }

                public String getColumnName(int column) {
                    return names[column];
                }

                public Class getColumnClass(int col) {
                    return getValueAt(0, col).getClass();
                }

                public boolean isCellEditable(int row, int col) {
                    return false;
                }

                public void setValueAt(Object aValue, int row, int column) {
                    data[row][column] = aValue;
                }
            };
            tabellaCosti.setModel(dataModel);
            FunzioniUtilita.impostaTabellaPopNoColor(tabellaCosti, true);

            tabellaCosti.getColumn(COL_catmin).setPreferredWidth(420);
            tabellaCosti.getColumn(COL_catpre).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_catamm).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_catvar).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_catsva).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_tot).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_conto).setPreferredWidth(148);
            tabellaCosti.getColumn(COL_voce).setPreferredWidth(340);
            tabellaCosti.getColumn(COL_saldo).setPreferredWidth(80);

            DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
            rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
            tabellaCosti.getColumn(COL_saldo).setCellRenderer(rightRenderer);
            tabellaCosti.getColumn(COL_catpre).setPreferredWidth(88);
            tabellaCosti.getColumn(COL_catamm).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_catvar).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_catsva).setPreferredWidth(80);
            tabellaCosti.getColumn(COL_tot).setPreferredWidth(88);

        }
    }

    @Override
    public void azionemassiva(boolean stato) {
        if (stato) {
            jLabel19.setVisible(true);
            jLabel20.setVisible(true);
            jLabel12.setVisible(true);
            anchefiscale.setVisible(true);
            note.setVisible(true);
            quotaamm.setVisible(true);
            jLabel12.setVisible(true);
            ggamm.setVisible(true);
        } else {
            jLabel19.setVisible(false);
            jLabel20.setVisible(false);
            jLabel12.setVisible(false);
            anchefiscale.setVisible(false);
            note.setVisible(false);
            quotaamm.setVisible(false);
            quotaamm.setText("");
            jLabel12.setVisible(false);
            ggamm.setVisible(false);
            ggamm.setText("");
        }

        if (select != null) {
            select.cambiaIcona(stato);
        }
    }

    private void popolaComboGruppi() {
        comboGruppi.removeAllItems();
        comboGruppi.addItem("< ....gruppo.... >");
        String qry = "SELECT DISTINCT gruppobsid,gruppobsmcs,gruppobsdescr FROM categoriebs JOIN gruppibs ON catbsgruppo = gruppobsmcs WHERE gruppobsconto = \"\""
                + " ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            if (!r.leggiStringa("gruppobsmcs").equals("30") && !r.leggiStringa("gruppobsmcs").equals("40")) {
                comboGruppi.addItem(r.leggiStringa("gruppobsmcs") + " - " + r.leggiStringa("gruppobsdescr") + " - IMMOBILIZZAZIONI MATERIALI");
                vmcsGru.add(r);
                r = qRic.successivo();
            } else {
                comboGruppi.addItem(r.leggiStringa("gruppobsmcs") + " - " + r.leggiStringa("gruppobsdescr"));
                vmcsGru.add(r);
                r = qRic.successivo();
            }
        }
        qRic.chiusura();
    }

    private void popolaComboSpecie(String codgru) {
        if (!codgru.equals("")) {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("< ....specie.... >");
            vmcsSpec.clear();
            String qry = "SELECT DISTINCT gruppobsid,gruppobsmcs,gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsdescr,gruppobsnote FROM categoriebs"
                    + " JOIN gruppibs ON catbsgruppo = gruppobsmastro AND catbsspecie = gruppobsconto"
                    + " WHERE gruppobssotto = \"\""
                    + " AND gruppobsmastro = \"" + codgru + "\""
                    + " AND gruppobsconto <> \"\""
                    + " ORDER BY gruppobsmcs";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            while (r != null) {
                comboSpecie.addItem(r.leggiStringa("gruppobsconto") + " - " + r.leggiStringa("gruppobsdescr") + r.leggiStringa("gruppobsnote"));
                vmcsSpec.add(r);
                r = qRic.successivo();
            }
            qRic.chiusura();
        } else {
            comboSpecie.removeAllItems();
        }
    }
}
