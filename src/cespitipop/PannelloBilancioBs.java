package cespitipop;

import java.awt.*;
import java.util.ArrayList;
import nucleo.ConnessioneServer;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.ExecGestioni;
import nucleo.FunzioniUtilita;

import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import ui.beans.PopButton;
import ui.beans.TablePopButton;

public class PannelloBilancioBs extends javax.swing.JPanel implements PannelloPop {

    private final String COL_COD = "Cod.";
    private final String COL_VOCE = "Voce di bilancio";
    private final String COL_MM = "Mastro";
    private final String COL_CC = "Conto";
    private final String COL_SS = "Sottoconto";
    private final String COL_DETT = "Dettaglio";
    private final String COL_INS = "+";
    private final String COL_DEL = "-";

    private Window padre;
    private Frame formGen;

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private int idVis = -1;

    private String azienda = "";
    private String esContabile = "";

//    public JTable immobImm = new javax.swing.JTable();
//    public JTable immobMat = new javax.swing.JTable();
//    public JTable immobFin = new javax.swing.JTable();
//    public JTable costiProd = new javax.swing.JTable();
    private int idpdcPlus = -1;
    private int idpdcPlusR = -1;
    private int idpdcMinus = -1;
    private int idpdcC1 = -1;
    private int idpdcC2 = -1;
    private int idpdcRR = -1;
    private int idpdcRC = -1;

    ArrayList<Record> righe0 = new ArrayList();
    ArrayList<Record> righe1 = new ArrayList();
    ArrayList<Record> righe2 = new ArrayList();
    ArrayList<Record> righe3 = new ArrayList();
    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloBilancioBs() {
        this.initComponents();
    }

    public PannelloBilancioBs(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

//        jScrollPane1.setVisible(false);
//        jScrollPane2.setVisible(false);
//        jScrollPane3.setVisible(false);
//        jScrollPane4.setVisible(false);
//        pTab.remove(jPanel3);
//        pTab.remove(jPanel10);
//        pTab.remove(jPanel11);
//        pTab.remove(jPanel12);

// 
        beanGestContoPlus.passaParametri(connAz, formGen, formGen, descrplus, 3, 3);
        CambioContoPlus cbs6 = new CambioContoPlus();
        beanGestContoPlus.gestioneAggiuntiva(cbs6);

        beanGestContoPlusR.passaParametri(connAz, formGen, formGen, descrplusr, 3, 3);
        CambioContoPlusR cbs7 = new CambioContoPlusR();
        beanGestContoPlusR.gestioneAggiuntiva(cbs7);

        beanGestContoMinus.passaParametri(connAz, formGen, formGen, descrminus, 3, 2);
        CambioContoMinus cbs8 = new CambioContoMinus();
        beanGestContoMinus.gestioneAggiuntiva(cbs8);

        beanGestContoAmm1.passaParametri(connAz, formGen, formGen, descrcosto1, 3, 2);
        CambioContoMinus cbs1 = new CambioContoMinus();
        beanGestContoAmm1.gestioneAggiuntiva(cbs1);

        beanGestContoAmm2.passaParametri(connAz, formGen, formGen, descrcosto2, 3, 2);
        CambioContoMinus cbs2 = new CambioContoMinus();
        beanGestContoAmm2.gestioneAggiuntiva(cbs2);

        beanGestContoRicavi.passaParametri(connAz, formGen, formGen, descrricavi, 3, 3);
        CambioContoMinus cbs3 = new CambioContoMinus();
        beanGestContoRicavi.gestioneAggiuntiva(cbs3);

        beanGestContoCosti.passaParametri(connAz, formGen, formGen, descrcosti, 3, 2);
        CambioContoMinus cbs4 = new CambioContoMinus();
        beanGestContoCosti.gestioneAggiuntiva(cbs4);

        visualizzaTutto();

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        pTab = new javax.swing.JTabbedPane();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        immobImm = new javax.swing.JTable();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        immobMat = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        immobFin = new javax.swing.JTable();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        costiAmm = new javax.swing.JTable();
        jPanel14 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        beanGestContoPlus = new archivibasepop.beans.BeanGestContoPop();
        descrplus = new javax.swing.JLabel();
        codcontoP = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        beanGestContoPlusR = new archivibasepop.beans.BeanGestContoPop();
        descrplusr = new javax.swing.JLabel();
        codcontoPr = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        beanGestContoMinus = new archivibasepop.beans.BeanGestContoPop();
        descrminus = new javax.swing.JLabel();
        codcontoM = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        beanGestContoAmm1 = new archivibasepop.beans.BeanGestContoPop();
        descrcosto1 = new javax.swing.JLabel();
        codcontoInd = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        beanGestContoAmm2 = new archivibasepop.beans.BeanGestContoPop();
        descrcosto2 = new javax.swing.JLabel();
        codcontoPded = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        beanGestContoRicavi = new archivibasepop.beans.BeanGestContoPop();
        beanGestContoCosti = new archivibasepop.beans.BeanGestContoPop();
        descrricavi = new javax.swing.JLabel();
        descrcosti = new javax.swing.JLabel();
        codcontoRiprr = new javax.swing.JLabel();
        codcontoRiprc = new javax.swing.JLabel();
        buttonModificaLegami = new ui.beans.PopButton();
        jPanel1 = new javax.swing.JPanel();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 30));
        pFiltri.setLayout(null);
        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        pTab.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        pTab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pTabMouseClicked(evt);
            }
        });

        jPanel3.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel3.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setPreferredSize(new java.awt.Dimension(200, 200));

        immobImm.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(immobImm);

        jPanel3.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pTab.addTab("I. Immob. Immateriali", jPanel3);

        jPanel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel10.setLayout(new java.awt.BorderLayout());

        immobMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(immobMat);

        jPanel10.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        pTab.addTab("II. Immob. Materiali", jPanel10);

        jPanel11.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel11.setLayout(new java.awt.BorderLayout());

        immobFin.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(immobFin);

        jPanel11.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        pTab.addTab("III. Immob. Finanziarie", jPanel11);

        jPanel12.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel12.setLayout(new java.awt.BorderLayout());

        costiAmm.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane4.setViewportView(costiAmm);

        jPanel12.add(jScrollPane4, java.awt.BorderLayout.CENTER);

        pTab.addTab("Ammortamenti  B) Costi della produzione", jPanel12);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jPanel14.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel1.setText("Conti di raccordo riprese di bilancio fiscale");
        jPanel14.add(jLabel1);
        jLabel1.setBounds(36, 380, 744, 14);

        jLabel17.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Plusvalenze");
        jPanel14.add(jLabel17);
        jLabel17.setBounds(68, 60, 172, 24);
        jPanel14.add(beanGestContoPlus);
        beanGestContoPlus.setBounds(256, 60, 248, 24);

        descrplus.setBackground(new java.awt.Color(202, 236, 221));
        descrplus.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrplus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrplus.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrplus.setOpaque(true);
        jPanel14.add(descrplus);
        descrplus.setBounds(508, 60, 400, 24);

        codcontoP.setBackground(new java.awt.Color(202, 236, 221));
        codcontoP.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        codcontoP.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codcontoP.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        codcontoP.setOpaque(true);
        jPanel14.add(codcontoP);
        codcontoP.setBounds(916, 60, 88, 24);

        jLabel18.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Plusvalenze rateizzabili");
        jPanel14.add(jLabel18);
        jLabel18.setBounds(68, 96, 172, 24);
        jPanel14.add(beanGestContoPlusR);
        beanGestContoPlusR.setBounds(256, 96, 248, 24);

        descrplusr.setBackground(new java.awt.Color(202, 236, 221));
        descrplusr.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrplusr.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrplusr.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrplusr.setOpaque(true);
        jPanel14.add(descrplusr);
        descrplusr.setBounds(508, 96, 400, 24);

        codcontoPr.setBackground(new java.awt.Color(202, 236, 221));
        codcontoPr.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        codcontoPr.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codcontoPr.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        codcontoPr.setOpaque(true);
        jPanel14.add(codcontoPr);
        codcontoPr.setBounds(916, 96, 88, 24);

        jLabel19.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Minusvalenze");
        jPanel14.add(jLabel19);
        jLabel19.setBounds(68, 132, 168, 24);
        jPanel14.add(beanGestContoMinus);
        beanGestContoMinus.setBounds(256, 132, 248, 24);

        descrminus.setBackground(new java.awt.Color(202, 236, 221));
        descrminus.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrminus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrminus.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrminus.setOpaque(true);
        jPanel14.add(descrminus);
        descrminus.setBounds(508, 132, 400, 24);

        codcontoM.setBackground(new java.awt.Color(202, 236, 221));
        codcontoM.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        codcontoM.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codcontoM.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        codcontoM.setOpaque(true);
        jPanel14.add(codcontoM);
        codcontoM.setBounds(916, 132, 88, 24);

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel2.setText("Conti di appoggio per vendite, alienazioni e dismissione dei beni");
        jPanel14.add(jLabel2);
        jLabel2.setBounds(28, 32, 744, 14);

        jLabel13.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Costo indeducibile");
        jPanel14.add(jLabel13);
        jLabel13.setBounds(68, 256, 176, 24);
        jPanel14.add(beanGestContoAmm1);
        beanGestContoAmm1.setBounds(256, 256, 248, 24);

        descrcosto1.setBackground(new java.awt.Color(202, 236, 221));
        descrcosto1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrcosto1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrcosto1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrcosto1.setOpaque(true);
        jPanel14.add(descrcosto1);
        descrcosto1.setBounds(508, 256, 400, 24);

        codcontoInd.setBackground(new java.awt.Color(202, 236, 221));
        codcontoInd.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        codcontoInd.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codcontoInd.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        codcontoInd.setOpaque(true);
        jPanel14.add(codcontoInd);
        codcontoInd.setBounds(916, 256, 88, 24);

        jLabel14.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Costo parzialmente deducibile");
        jPanel14.add(jLabel14);
        jLabel14.setBounds(44, 292, 200, 24);
        jPanel14.add(beanGestContoAmm2);
        beanGestContoAmm2.setBounds(256, 292, 248, 24);

        descrcosto2.setBackground(new java.awt.Color(202, 236, 221));
        descrcosto2.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrcosto2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrcosto2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrcosto2.setOpaque(true);
        jPanel14.add(descrcosto2);
        descrcosto2.setBounds(508, 292, 400, 24);

        codcontoPded.setBackground(new java.awt.Color(202, 236, 221));
        codcontoPded.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        codcontoPded.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codcontoPded.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        codcontoPded.setOpaque(true);
        jPanel14.add(codcontoPded);
        codcontoPded.setBounds(916, 292, 88, 24);

        jLabel3.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel3.setText("Conti di dettaglio costi ammortamento non o parzialmente detraibili");
        jPanel14.add(jLabel3);
        jLabel3.setBounds(32, 228, 744, 14);

        jLabel15.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel15.setText("Riprese ricavi");
        jPanel14.add(jLabel15);
        jLabel15.setBounds(68, 412, 176, 24);

        jLabel16.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Riprese costi");
        jPanel14.add(jLabel16);
        jLabel16.setBounds(44, 448, 200, 24);
        jPanel14.add(beanGestContoRicavi);
        beanGestContoRicavi.setBounds(256, 412, 248, 24);
        jPanel14.add(beanGestContoCosti);
        beanGestContoCosti.setBounds(256, 448, 248, 24);

        descrricavi.setBackground(new java.awt.Color(202, 236, 221));
        descrricavi.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrricavi.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrricavi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrricavi.setOpaque(true);
        jPanel14.add(descrricavi);
        descrricavi.setBounds(508, 412, 400, 24);

        descrcosti.setBackground(new java.awt.Color(202, 236, 221));
        descrcosti.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        descrcosti.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        descrcosti.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        descrcosti.setOpaque(true);
        jPanel14.add(descrcosti);
        descrcosti.setBounds(508, 448, 400, 24);

        codcontoRiprr.setBackground(new java.awt.Color(202, 236, 221));
        codcontoRiprr.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        codcontoRiprr.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codcontoRiprr.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        codcontoRiprr.setOpaque(true);
        jPanel14.add(codcontoRiprr);
        codcontoRiprr.setBounds(916, 412, 88, 24);

        codcontoRiprc.setBackground(new java.awt.Color(202, 236, 221));
        codcontoRiprc.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        codcontoRiprc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        codcontoRiprc.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        codcontoRiprc.setOpaque(true);
        jPanel14.add(codcontoRiprc);
        codcontoRiprc.setBounds(916, 448, 88, 24);

        buttonModificaLegami.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_arrow_forward_white_20dp.png"))); // NOI18N
        buttonModificaLegami.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonModificaLegami.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonModificaLegami.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonModificaLegami.setPreferredSize(new java.awt.Dimension(80, 36));
        buttonModificaLegami.setText("Lega");
        buttonModificaLegami.setToolTipText("Lega il cespite al bilancio CEE e piano dei conti");
        buttonModificaLegami.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonModificaLegamiActionPerformed(evt);
            }
        });
        jPanel14.add(buttonModificaLegami);
        buttonModificaLegami.setBounds(30, 480, 80, 36);

        pTab.addTab("Ricavi, Dismissioni, Raccordo civile-fiscale", jPanel14);

        add(pTab, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 40));
        jPanel1.setLayout(null);
        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void pTabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pTabMouseClicked
//        pTab.
//        visualizzaTutto();

    }//GEN-LAST:event_pTabMouseClicked

    private void buttonModificaLegamiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonModificaLegamiActionPerformed
        Record rec = new Record();

        if (beanGestContoPlus.leggiId() > 0) {
            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.1";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();

            rec.insElem("pdcceecodalfa", "5.1");
            rec.insElem("pdcceemastro", beanGestContoPlus.leggiMastro());
            rec.insElem("pdcceeconto", beanGestContoPlus.leggiConto());
            rec.insElem("pdcceesotto", beanGestContoPlus.leggiSottoconto());
            rec.insElem("pdcceejazzid", beanGestContoPlus.leggiId());
            rec.insElem("pdcceedescr", descrplus.getText().trim());
            rec.insElem("pdcceedescrvoce", jLabel17.getText().trim());
            rec.convalida(true);
            if (r != null) {
                rec.insElem("pdcceeid", r.leggiIntero("pdcceeid"));
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            } else {
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            }
        }

        if (beanGestContoPlusR.leggiId() > 0) {

            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.2";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();

            rec = new Record();
            rec.insElem("pdcceecodalfa", "5.2");
            rec.insElem("pdcceemastro", beanGestContoPlusR.leggiMastro());
            rec.insElem("pdcceeconto", beanGestContoPlusR.leggiConto());
            rec.insElem("pdcceesotto", beanGestContoPlusR.leggiSottoconto());
            rec.insElem("pdcceejazzid", beanGestContoPlusR.leggiId());
            rec.insElem("pdcceedescr", descrplusr.getText().trim());
            rec.insElem("pdcceedescrvoce", jLabel18.getText().trim());
            rec.convalida(true);
            if (r != null) {
                rec.insElem("pdcceeid", r.leggiIntero("pdcceeid"));
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            } else {
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            }
        }

        if (beanGestContoMinus.leggiId() > 0) {
            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=5.3";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();

            rec = new Record();
            rec.insElem("pdcceecodalfa", "5.3");
            rec.insElem("pdcceemastro", beanGestContoMinus.leggiMastro());
            rec.insElem("pdcceeconto", beanGestContoMinus.leggiConto());
            rec.insElem("pdcceesotto", beanGestContoMinus.leggiSottoconto());
            rec.insElem("pdcceejazzid", beanGestContoMinus.leggiId());
            rec.insElem("pdcceedescr", descrminus.getText().trim());
            rec.insElem("pdcceedescrvoce", jLabel19.getText().trim());
            rec.convalida(true);

            if (r != null) {
                rec.insElem("pdcceeid", r.leggiIntero("pdcceeid"));
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            } else {
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            }
        }

        if (beanGestContoAmm1.leggiId() > 0) {
            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=6.1";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();

            rec = new Record();
            rec.insElem("pdcceecodalfa", "6.1");
            rec.insElem("pdcceemastro", beanGestContoAmm1.leggiMastro());
            rec.insElem("pdcceeconto", beanGestContoAmm1.leggiConto());
            rec.insElem("pdcceesotto", beanGestContoAmm1.leggiSottoconto());
            rec.insElem("pdcceejazzid", beanGestContoAmm1.leggiId());
            rec.insElem("pdcceedescr", descrcosto1.getText().trim());
            rec.insElem("pdcceedescrvoce", jLabel13.getText().trim());
            rec.convalida(true);

            if (r != null) {
                rec.insElem("pdcceeid", r.leggiIntero("pdcceeid"));
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            } else {
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            }
        }

        if (beanGestContoAmm2.leggiId() > 0) {
            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=6.2";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();

            rec = new Record();
            rec.insElem("pdcceecodalfa", "6.2");
            rec.insElem("pdcceemastro", beanGestContoAmm2.leggiMastro());
            rec.insElem("pdcceeconto", beanGestContoAmm2.leggiConto());
            rec.insElem("pdcceesotto", beanGestContoAmm2.leggiSottoconto());
            rec.insElem("pdcceejazzid", beanGestContoAmm2.leggiId());
            rec.insElem("pdcceedescr", descrcosto2.getText().trim());
            rec.insElem("pdcceedescrvoce", jLabel14.getText().trim());
            rec.convalida(true);

            if (r != null) {
                rec.insElem("pdcceeid", r.leggiIntero("pdcceeid"));
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            } else {
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            }
        }

        if (beanGestContoRicavi.leggiId() > 0) {
            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=7.1";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();

            rec = new Record();
            rec.insElem("pdcceecodalfa", "7.1");
            rec.insElem("pdcceemastro", beanGestContoRicavi.leggiMastro());
            rec.insElem("pdcceeconto", beanGestContoRicavi.leggiConto());
            rec.insElem("pdcceesotto", beanGestContoRicavi.leggiSottoconto());
            rec.insElem("pdcceejazzid", beanGestContoRicavi.leggiId());
            rec.insElem("pdcceedescr", descrricavi.getText().trim());
            rec.insElem("pdcceedescrvoce", jLabel15.getText().trim());
            rec.convalida(true);

            if (r != null) {
                rec.insElem("pdcceeid", r.leggiIntero("pdcceeid"));
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            } else {
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            };
        }

        if (beanGestContoCosti.leggiId() > 0) {
            String qry = "SELECT * FROM bilanciobs WHERE pdcceecodalfa=7.2";
            QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry);
            query.apertura();
            Record r = query.successivo();
            query.chiusura();

            rec = new Record();
            rec.insElem("pdcceecodalfa", "7.2");
            rec.insElem("pdcceemastro", beanGestContoCosti.leggiMastro());
            rec.insElem("pdcceeconto", beanGestContoCosti.leggiConto());
            rec.insElem("pdcceesotto", beanGestContoCosti.leggiSottoconto());
            rec.insElem("pdcceejazzid", beanGestContoCosti.leggiId());
            rec.insElem("pdcceedescr", descrcosti.getText().trim());
            rec.insElem("pdcceedescrvoce", jLabel16.getText().trim());
            rec.convalida(true);

            if (r != null) {
                rec.insElem("pdcceeid", r.leggiIntero("pdcceeid"));
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.AGG, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            } else {
                QueryAggiornamentoClient qc = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "bilanciobs", "pdcceeid", rec);
                qc.esecuzione();
            };
        }

        JOptionPane.showMessageDialog(this, "Legame con contabilita' eseguito", "Errore",
                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        visualizzaTutto();

    }//GEN-LAST:event_buttonModificaLegamiActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private archivibasepop.beans.BeanGestContoPop beanGestContoAmm1;
    private archivibasepop.beans.BeanGestContoPop beanGestContoAmm2;
    private archivibasepop.beans.BeanGestContoPop beanGestContoCosti;
    private archivibasepop.beans.BeanGestContoPop beanGestContoMinus;
    private archivibasepop.beans.BeanGestContoPop beanGestContoPlus;
    private archivibasepop.beans.BeanGestContoPop beanGestContoPlusR;
    private archivibasepop.beans.BeanGestContoPop beanGestContoRicavi;
    private ui.beans.PopButton buttonModificaLegami;
    private javax.swing.JLabel codcontoInd;
    private javax.swing.JLabel codcontoM;
    private javax.swing.JLabel codcontoP;
    private javax.swing.JLabel codcontoPded;
    private javax.swing.JLabel codcontoPr;
    private javax.swing.JLabel codcontoRiprc;
    private javax.swing.JLabel codcontoRiprr;
    private javax.swing.JTable costiAmm;
    private javax.swing.JLabel descrcosti;
    private javax.swing.JLabel descrcosto1;
    private javax.swing.JLabel descrcosto2;
    private javax.swing.JLabel descrminus;
    private javax.swing.JLabel descrplus;
    private javax.swing.JLabel descrplusr;
    private javax.swing.JLabel descrricavi;
    private javax.swing.JTable immobFin;
    private javax.swing.JTable immobImm;
    private javax.swing.JTable immobMat;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JPanel pFiltri;
    private javax.swing.JTabbedPane pTab;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void visualizzaTutto() {
        pTab.setVisible(true);
        final String colonne[]
                = {
                    COL_COD, COL_VOCE, COL_MM, COL_CC, COL_SS, COL_DETT, COL_INS, COL_DEL
                };
        int i = pTab.getSelectedIndex();

        pTab.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N

        switch (i) {
            //TABELLA IMMOBILIZZAZIONI IMMATERIALI
            case 0:
                final Object[][] data = popolaTabella(i);
                TableModel dataModel0 = new AbstractTableModel() {

                    public int getRowCount() {
                        return data.length;
                    }

                    public int getColumnCount() {
                        int columnCount = colonne.length;
                        return columnCount;
                    }

                    public Object getValueAt(int row, int col) {
                        return data[row][col];
                    }

                    public String getColumnName(int i) {
                        String name = colonne[i];
                        return name;
                    }

                    public void setValueAt(Object aValue, int row, int column) {
                        data[row][column] = aValue;
                    }

                    public boolean isCellEditable(int row, int col) {
                        if (col == FunzioniUtilita.getColumnIndex(immobImm, COL_INS)
                                || col == FunzioniUtilita.getColumnIndex(immobImm, COL_DEL)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                immobImm.setModel(dataModel0);

                FunzioniUtilita.impostaTabellaPop(immobImm, true);
                immobImm.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
                immobImm.getTableHeader().setReorderingAllowed(false);

                TableColumn buttonColumn1 = immobImm.getColumn(COL_INS);
                TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                    public void customize(PopButton button, int row, int column) {
                        Record r = righe0.get(row);
                        if (r.leggiStringa("pdcceedescr").equals("") && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1") > 0 && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1.c") < 0) {
                            button.setEnabled(true);
                        } else {
                            button.setEnabled(false);
                        }
                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png")));
                        button.setToolTipText("Lega il conto del piano conti interno alla voce di bilancio per nota integrativa");
                    }
                });

                button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
                    @Override
                    public void onButtonPress(int row, int column) {
                        Record r = righe0.get(row);
                        if (r.leggiStringa("pdcceedescr").equals("") && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1") > 0 && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1.c") < 0) {

                            DialogPdcCollegatoCee d = new DialogPdcCollegatoCee(formGen, true, connAz, azienda, r.leggiIntero("pdcceeid"), (int) 2);
                            UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                            d.show();

                            visualizzaTutto();
                        }
                    }
                });
                buttonColumn1.setCellRenderer(button1);
                buttonColumn1.setCellEditor(button1);

                TableColumn buttonColumn2 = immobImm.getColumn(COL_DEL);
                TablePopButton button2 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
                    public void customize(PopButton button, int row, int column) {
                        Record r = righe0.get(row);
                        if (r.leggiStringa("pdcceedescr").equals("") && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1") > 0 && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1.c") < 0) {
                            button.setEnabled(true);
                        } else {
                            button.setEnabled(false);
                        }
                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                        button.setToolTipText("Slega il sottoconto associato alla voce di bilancio");
                    }
                });

                button2.addHandler(new TablePopButton.TableButtonPressedHandler() {
                    @Override
                    public void onButtonPress(int row, int column) {
                        boolean ok = false;
                        Record r = righe0.get(row);
                        if (r.leggiStringa("pdcceedescr").equals("") && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1") > 0 && r.leggiStringa("pdcceecodalfa").compareTo("1.B.1.c") < 0) {
                            chiediConfermaElim(r.leggiIntero("pdcceeid"));
                            if (ok) {
                                visualizzaTutto();
                            }
                        }
                    }
                });

                buttonColumn2.setCellRenderer(button2);
                buttonColumn2.setCellEditor(button2);

                immobImm.getColumn(COL_COD).setPreferredWidth(90);
                immobImm.getColumn(COL_VOCE).setPreferredWidth(400);
                immobImm.getColumn(COL_MM).setPreferredWidth(80);
                immobImm.getColumn(COL_CC).setPreferredWidth(80);
                immobImm.getColumn(COL_SS).setPreferredWidth(90);
                immobImm.getColumn(COL_DETT).setPreferredWidth(340);
                immobImm.getColumn(COL_INS).setPreferredWidth(40);
                immobImm.getColumn(COL_DEL).setPreferredWidth(40);

                immobImm.getColumn(COL_INS).setHeaderRenderer(headerRenderer);
                immobImm.getColumn(COL_DEL).setHeaderRenderer(headerRenderer);
                break;

            //TABELLA IMMOBILIZZAZIONI MATERIALI
            case 1:
                final Object[][] data1 = popolaTabella(i);
                TableModel dataModel1 = new AbstractTableModel() {
                    public int getRowCount() {
                        return data1.length;
                    }

                    public int getColumnCount() {
                        int columnCount = colonne.length;
                        return columnCount;
                    }

                    public Object getValueAt(int row, int col) {
                        return data1[row][col];
                    }

                    public String getColumnName(int i) {
                        String name = colonne[i];
                        return name;
                    }

                    public void setValueAt(Object aValue, int row, int column) {
                        data1[row][column] = aValue;
                    }

                    public boolean isCellEditable(int row, int col) {
                        if (col == FunzioniUtilita.getColumnIndex(immobMat, COL_INS)
                                || col == FunzioniUtilita.getColumnIndex(immobMat, COL_DEL)) {
                            return true;
                        } else {
                            return false;
                        }
                    }

                };
                immobMat.setModel(dataModel1);

                break;

            //IMMOBILIZZAZIONI FINANZIARIE
            case 2:
                final Object[][] data2 = popolaTabella(i);
                TableModel dataModel2 = new AbstractTableModel() {
                    public int getRowCount() {
                        return data2.length;
                    }

                    public int getColumnCount() {
                        int columnCount = colonne.length;
                        return columnCount;
                    }

                    public Object getValueAt(int row, int col) {
                        return data2[row][col];
                    }

                    public String getColumnName(int i) {
                        String name = colonne[i];
                        return name;
                    }

                    public void setValueAt(Object aValue, int row, int column) {
                        data2[row][column] = aValue;
                    }

                    public boolean isCellEditable(int row, int col) {
                        if (col == FunzioniUtilita.getColumnIndex(immobFin, COL_INS)
                                || col == FunzioniUtilita.getColumnIndex(immobFin, COL_DEL)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                immobFin.setModel(dataModel2);

                break;

            //COSTI DI PRODUZIONE
            case 3:
                final Object[][] data3 = popolaTabella(i);
                TableModel dataModel3 = new AbstractTableModel() {
                    public int getRowCount() {
                        return data3.length;
                    }

                    public int getColumnCount() {
                        int columnCount = colonne.length;
                        return columnCount;
                    }

                    public Object getValueAt(int row, int col) {
                        return data3[row][col];
                    }

                    public String getColumnName(int i) {
                        String name = colonne[i];
                        return name;
                    }

                    public void setValueAt(Object aValue, int row, int column) {
                        data3[row][column] = aValue;
                    }
//

                    public boolean isCellEditable(int row, int col) {
                        if (col == FunzioniUtilita.getColumnIndex(costiAmm, COL_INS)
                                || col == FunzioniUtilita.getColumnIndex(costiAmm, COL_DEL)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                };
                costiAmm.setModel(dataModel3);
                break;
//
            //VALORI PRODUZIONE
            case 4:
//              
                String query = "SELECT * FROM bilanciobs "
                        + " WHERE pdcceecodalfa > \"5\" AND pdcceebs > 0 ORDER BY pdcceecodalfa,pdcceemastro";
                QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, query);
                q.apertura();
                if (q.numeroRecord() > 0) {
                    for (int y = 0; y < q.numeroRecord(); y++) {
                        Record rec = q.successivo();
                        if (rec.leggiIntero("pdcceejazzid") > 0) {
                            String qry2 = "SELECT pdcid,pdctipo,pdcspec,pdcdescr,pdcmastro,pdcconto,pdcsottoconto FROM pianoconti "
                                    + " WHERE pdcid = " + rec.leggiIntero("pdcceejazzid");
                            QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry2);
                            qb.apertura();
                            Record rb = qb.successivo();
                            qb.chiusura();
                            if (rb != null) {
                                if (rec.leggiStringa("pdcceecodalfa").equals("5.1")) {
                                    beanGestContoPlus.visualizzaDati(rb.leggiIntero("pdcid"), rb.leggiStringa("pdcmastro"), rb.leggiStringa("pdcconto"), rb.leggiStringa("pdcsottoconto"), rb.leggiStringa("pdcdescr"));
                                    if (rec.leggiStringa("pdcceecodalfa") != null) {
                                        codcontoP.setText(rec.leggiStringa("pdcceecodalfa"));
                                    }
                                    if (rb.leggiIntero("pdctipo") != 5) {
                                        JOptionPane.showMessageDialog(this, "ERRORE il conto non e' un Provento", "Errore",
                                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    }
                                    idpdcPlus = rec.leggiIntero("pdcceejazzid");
                                } else if (rec.leggiStringa("pdcceecodalfa").equals("5.2")) {
                                    beanGestContoPlusR.visualizzaDati(rb.leggiIntero("pdcid"), rb.leggiStringa("pdcmastro"), rb.leggiStringa("pdcconto"), rb.leggiStringa("pdcsottoconto"), rb.leggiStringa("pdcdescr"));
                                    if (rec.leggiStringa("pdcceecodalfa") != null) {
                                        codcontoPr.setText(rec.leggiStringa("pdcceecodalfa"));
                                    }
                                    if (rb.leggiIntero("pdctipo") != 5) {
                                        JOptionPane.showMessageDialog(this, "ERRORE il conto non e' un Ricavo-Provento", "Errore",
                                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    }
                                    idpdcPlusR = rec.leggiIntero("pdcceejazzid");
                                } else if (rec.leggiStringa("pdcceecodalfa").equals("5.3")) {
                                    beanGestContoMinus.visualizzaDati(rb.leggiIntero("pdcid"), rb.leggiStringa("pdcmastro"), rb.leggiStringa("pdcconto"), rb.leggiStringa("pdcsottoconto"), rb.leggiStringa("pdcdescr"));
                                    if (rec.leggiStringa("pdcceecodalfa") != null) {
                                        codcontoM.setText(rec.leggiStringa("pdcceecodalfa"));
                                    }
                                    if (rb.leggiIntero("pdctipo") != 4) {
                                        JOptionPane.showMessageDialog(this, "ERRORE il conto non e' un Costo-Onere", "Errore",
                                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    }
                                    idpdcMinus = rec.leggiIntero("pdcceejazzid");
                                } else if (rec.leggiStringa("pdcceecodalfa").equals("6.1")) {
                                    beanGestContoAmm1.visualizzaDati(rb.leggiIntero("pdcid"), rb.leggiStringa("pdcmastro"), rb.leggiStringa("pdcconto"), rb.leggiStringa("pdcsottoconto"), rb.leggiStringa("pdcdescr"));
                                    if (rec.leggiStringa("pdcceecodalfa") != null) {
                                        codcontoInd.setText(rec.leggiStringa("pdcceecodalfa"));
                                    }
                                    if (rb.leggiIntero("pdctipo") != 4) {
                                        JOptionPane.showMessageDialog(this, "ERRORE il conto non e' un Costo Ammortamento", "Errore",
                                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    }
                                    idpdcC1 = rec.leggiIntero("pdcceejazzid");
                                } else if (rec.leggiStringa("pdcceecodalfa").equals("6.2")) {
                                    beanGestContoAmm2.visualizzaDati(rb.leggiIntero("pdcid"), rb.leggiStringa("pdcmastro"), rb.leggiStringa("pdcconto"), rb.leggiStringa("pdcsottoconto"), rb.leggiStringa("pdcdescr"));
                                    if (rec.leggiStringa("pdcceecodalfa") != null) {
                                        codcontoPded.setText(rec.leggiStringa("pdcceecodalfa"));
                                    }
                                    if (rb.leggiIntero("pdctipo") != 4) {
                                        JOptionPane.showMessageDialog(this, "ERRORE il conto non e' un Costo Ammortamento", "Errore",
                                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    }
                                    idpdcC2 = rec.leggiIntero("pdcceejazzid");
                                } else if (rec.leggiStringa("pdcceecodalfa").equals("7.1")) {
                                    beanGestContoRicavi.visualizzaDati(rb.leggiIntero("pdcid"), rb.leggiStringa("pdcmastro"), rb.leggiStringa("pdcconto"), rb.leggiStringa("pdcsottoconto"), rb.leggiStringa("pdcdescr"));
                                    if (rec.leggiStringa("pdcceecodalfa") != null) {
                                        codcontoRiprr.setText(rec.leggiStringa("pdcceecodalfa"));
                                    }
                                    if (rb.leggiIntero("pdctipo") != 4) {
                                        JOptionPane.showMessageDialog(this, "ERRORE il conto non e' una Ripresa Ricavi", "Errore",
                                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    }
                                    idpdcRR = rec.leggiIntero("pdcceejazzid");
                                } else if (rec.leggiStringa("pdcceecodalfa").equals("7.2")) {
                                    beanGestContoCosti.visualizzaDati(rb.leggiIntero("pdcid"), rb.leggiStringa("pdcmastro"), rb.leggiStringa("pdcconto"), rb.leggiStringa("pdcsottoconto"), rb.leggiStringa("pdcdescr"));
                                    if (rec.leggiStringa("pdcceecodalfa") != null) {
                                        codcontoRiprc.setText(rec.leggiStringa("pdcceecodalfa"));
                                    }
                                    if (rb.leggiIntero("pdctipo") != 4) {
                                        JOptionPane.showMessageDialog(this, "ERRORE il conto non e' una Ripresa Costo", "Errore",
                                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                    }
                                    idpdcRC = rec.leggiIntero("pdcceejazzid");
                                }
                            } else {
                                descrplus.setText("");
                                codcontoP.setText("");
                                descrplusr.setText("");
                                codcontoPr.setText("");
                                descrminus.setText("");
                                codcontoM.setText("");
                                codcontoInd.setText("");
                                descrcosto1.setText("");
                                codcontoPded.setText("");
                                descrcosto2.setText("");

                                codcontoRiprr.setText("");
                                descrricavi.setText("");
                                codcontoRiprc.setText("");
                                descrcosti.setText("");
                            }
                        } else {
                            descrplus.setText("");
                            codcontoP.setText("");
                            descrplusr.setText("");
                            codcontoPr.setText("");
                            descrminus.setText("");
                            codcontoM.setText("");
                            codcontoInd.setText("");
                            descrcosto1.setText("");
                            codcontoPded.setText("");
                            descrcosto2.setText("");

                            codcontoRiprr.setText("");
                            descrricavi.setText("");
                            codcontoRiprc.setText("");
                            descrcosti.setText("");
                        }
                    }
                }

                break;
            default:
                break;
        }
    }

//QUERY CHE POPOLA LE VARIE TABELLE
    private Object[][] popolaTabella(int indice) {
        righe0 = new ArrayList();
        righe1 = new ArrayList();
        righe2 = new ArrayList();
        righe3 = new ArrayList();

        String query = "SELECT pdcceecodalfa,pdcceedescr,pdcid,pdcspec,pdcmastro,pdcconto,pdcsottoconto,pdcdescr FROM comunejazz.pianoconticee"
                + " LEFT JOIN pianoconti ON pdcceeid = pdccodcee ";
        String where = "";
        if (indice == 0) {
            where = "WHERE pdcceecodalfa > \"1.B.I\" AND pdcceecodalfa < \"1.B.II\" AND pdcceestato > 0 AND pdcstato = 0 ORDER BY pdcceecodalfa,pdcmastro,pdcconto"; //immobilizzazioni immateriali
        } else if (indice == 1) {
            where = "WHERE pdcceecodalfa > \"1.B.II\" AND pdcceecodalfa < \"1.B.III\" AND pdcceestato > 0 AND pdcstato = 0 ORDER BY pdcceecodalfa,pdcmastro,pdcconto"; //immobilizzazioni materiali
        } else if (indice == 2) {
            where = "WHERE pdcceecodalfa > \"1.B.III\" AND pdcceecodalfa < \"1.B.IV\" AND pdcceestato > 0 AND pdcstato = 0 ORDER BY pdcceecodalfa,pdcmastro,pdcconto"; //immobilizzazioni finanziarie
        } else if (indice == 3) {
            where = "pdcceecodalfa = \"4.B.10.a\" OR pdcceecodalfa = \"4.B.10.b\" OR pdcceecodalfa = \"4.B.10.c\" AND pdcceebs > 0 AND pdcstato = 0 ORDER BY pdcceecodalfa,pdcmastro,pdcconto"; //ammortamenti
        }

        String qry = query + where;
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        final Object[][] data = new Object[q.numeroRecord()][8];
        if (q.numeroRecord() > 0) {

            for (int i = 0; i < q.numeroRecord(); i++) {
                Record r = q.successivo();
                data[i][0] = r.leggiStringa("pdcceecodalfa");
                String dd = r.leggiStringa("pdcceedescr");
                if ((r.leggiStringa("pdcceedescr")).length() > 15) {
                    dd = (r.leggiStringa("pdcceedescr")).substring(14, (r.leggiStringa("pdcceedescr")).length());
                }
                data[i][1] = dd;
                if (r.nullo("pdcmastro")) {
                    data[i][2] = "";
                    data[i][3] = "";
                    data[i][4] = "";
                    data[i][5] = "";
                } else {
                    data[i][2] = r.leggiStringa("pdcmastro");
                    data[i][3] = r.leggiStringa("pdcconto");
                    data[i][4] = r.leggiStringa("pdcsottoconto");
                    data[i][5] = r.leggiStringa("pdcdescr");
                    //patch aggiornamento pianoconti jazz

                    if (indice == 0 && r.leggiIntero("pdcspec") != 3) {
                        String qryupdate = "UPDATE pianoconti SET pdcspec = 3 WHERE pdcid =" + r.leggiIntero("pdcid");
                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                        qa.esecuzione();
                    }

                    if (indice == 1 && r.leggiIntero("pdcspec") != 2) {
                        String qryupdate = "UPDATE pianoconti SET pdcspec = 2 WHERE pdcid =" + r.leggiIntero("pdcid");
                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                        qa.esecuzione();
                    }

                    if (indice == 2 && r.leggiIntero("pdcspec") != 4) {
                        String qryupdate = "UPDATE pianoconti SET pdcspec = 4 WHERE pdcid =" + r.leggiIntero("pdcid");
                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                        qa.esecuzione();
                    }
                    if (indice == 3 && r.leggiStringa("pdcceecodalfa").equals("4.B.10.a") && r.leggiIntero("pdcspec") != 0) {
                        String qryupdate = "UPDATE pianoconti SET pdcspec = 2 WHERE pdcid =" + r.leggiIntero("pdcid");
                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                        qa.esecuzione();
                    }
                    if (indice == 3 && r.leggiStringa("pdcceecodalfa").equals("4.B.10.b") && r.leggiIntero("pdcspec") != 0) {
                        String qryupdate = "UPDATE pianoconti SET pdcspec = 3 WHERE pdcid =" + r.leggiIntero("pdcid");
                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                        qa.esecuzione();
                    }
                    if (indice == 3 && r.leggiStringa("pdcceecodalfa").equals("4.B.10.c") && r.leggiIntero("pdcspec") != 0) {
                        String qryupdate = "UPDATE pianoconti SET pdcspec = 4 WHERE pdcid =" + r.leggiIntero("pdcid");
                        QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                        qa.esecuzione();
                    }
                }
                //
                if (indice == 0) {
                    righe0.add(r);
                } else if (indice == 1) {
                    righe1.add(r);
                } else if (indice == 2) {
                    righe2.add(r);
                } else if (indice == 3) {
                    righe3.add(r);
                }
            }
        }
        q.chiusura();

        return data;
    }

    final boolean chiediConfermaElim(int idelim) {
        boolean eli = false;
        int ris = JOptionPane.showConfirmDialog(this, "Confermi l'eliminazione dell'associazione ?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            String qrydelete = "UPDATE pianoconti SET pdccodcee = -1  WHERE pdcid  =" + idelim;
            QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qrydelete);
            qd.esecuzione();

            eli = true;
            JOptionPane.showMessageDialog(this, "Associazione ELIMINATA ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            visualizzaTutto();
        }
        return eli;
    }

    class CambioContoPlus implements ExecGestioni {

        public void lancia() {
            if (beanGestContoPlus.leggiId() > 0) {
                idpdcPlus = beanGestContoPlus.leggiId();
            } else {
                idpdcPlus = -1;
            }
        }
    }

    class CambioContoPlusR implements ExecGestioni {

        public void lancia() {
            if (beanGestContoPlusR.leggiId() > 0) {
                idpdcPlusR = beanGestContoPlusR.leggiId();
            } else {
                idpdcPlusR = -1;
            }
        }
    }

    class CambioContoMinus implements ExecGestioni {

        public void lancia() {
            if (beanGestContoMinus.leggiId() > 0) {
                idpdcMinus = beanGestContoMinus.leggiId();
            } else {
                idpdcMinus = -1;
            }
        }
    }

    class CambioContoAmm1 implements ExecGestioni {

        public void lancia() {
            if (beanGestContoAmm1.leggiId() > 0) {
                idpdcC1 = beanGestContoAmm1.leggiId();
            } else {
                idpdcC1 = -1;
            }
        }
    }

    class CambioContoAmm2 implements ExecGestioni {

        public void lancia() {
            if (beanGestContoAmm2.leggiId() > 0) {
                idpdcC2 = beanGestContoAmm2.leggiId();
            } else {
                idpdcC2 = -1;
            }
        }
    }

    class CambioContoRicavi implements ExecGestioni {

        public void lancia() {
            if (beanGestContoRicavi.leggiId() > 0) {
                idpdcRR = beanGestContoRicavi.leggiId();
            } else {
                idpdcRR = -1;
            }
        }
    }

    class CambioContoCosti implements ExecGestioni {

        public void lancia() {
            if (beanGestContoCosti.leggiId() > 0) {
                idpdcRC = beanGestContoCosti.leggiId();
            } else {
                idpdcRC = -1;
            }
        }
    }
}
