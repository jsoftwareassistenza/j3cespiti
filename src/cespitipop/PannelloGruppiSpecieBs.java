package cespitipop;

/*
 * JpanelCategCespiti.java
 *
 * Created on 8 novembre 2006, 13.24
 */
import java.awt.Frame;
import java.awt.Window;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.*;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import ui.beans.PopButton;
import ui.beans.TablePopButton;

/**
 *
 * @author svilupp
 */
public class PannelloGruppiSpecieBs extends javax.swing.JPanel implements PannelloPop {

    private final String COL_SEL = "S.";
    private final String COL_ID = "ID";
    private final String COL_GRU = "Gruppo";
    private final String COL_COD = "Codice";
    private final String COL_DES = "Tipologia";

    private final String COL_SEL1 = "S.";
    private final String COL_ID1 = "ID";
    private final String COL_COD1 = "Codice";
    private final String COL_DES1 = "Tipologia";

    private final String COL_ID2 = "ID";
    private final String COL_COD2 = "Categoria";
    private final String COL_DES2 = "Descrizione";
    private final String COL_ALIQ2 = "Quota";
    private final String COL_INS2 = " ins ";

    private Window padre;
    private Frame formGen;

    private ConnessioneServer connCom = null;
    private ConnessioneServer connAz = null;

    private String azienda = "";
    private String esContabile = "";
    private String gruppoSelez = "";
    private String specieSelez = "";

    private int idVis;
    private String[] codEcc = null;
    String flagCriterioFiscale = "0";
    private boolean modif = true;

//    private int[] id;
    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    /**
     * Creates new form JpanelCategCespiti
     */
    public PannelloGruppiSpecieBs() {
//        this.setSize(590, 312);
//        this.setPreferredSize(new Dimension(590, 312));
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public PannelloGruppiSpecieBs(Window padre, Frame formGen) {
        this();
        this.padre = padre;
        this.formGen = formGen;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connCom = connCom;
        this.connAz = connAz;        
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        flagCriterioFiscale = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "flagCriterioFiscale");
        //ricerca inizializzazione
        gruppoSelez = "";
        specieSelez = "";
        String qry = "SELECT pval FROM proprieta WHERE pprog = \"cespiti_jazz\" AND pprop = \"gruppo\"";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record r = q.successivo();
        q.chiusura();
        if (r != null) {
            gruppoSelez = r.leggiStringa("pval");
        }

        qry = "SELECT pval FROM proprieta WHERE pprog = \"cespiti_jazz\" AND pprop = \"specie\"";
        q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        r = q.successivo();
        q.chiusura();
        if (r != null) {
            specieSelez = r.leggiStringa("pval");
        }

        mostraListaGruppi();

        if (!gruppoSelez.equals("") && !specieSelez.equals("")) {
            mostraCategorie();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        speccat = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabellaRisultato = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lista = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tabcat = new javax.swing.JTable();
        pannelloPulsanti = new javax.swing.JPanel();
        buttonSalva = new ui.beans.PopButton();
        help1 = new javax.swing.JLabel();
        buttonElimina = new ui.beans.PopButton();

        setLayout(new java.awt.BorderLayout());

        jPanel3.setPreferredSize(new java.awt.Dimension(100, 300));
        jPanel3.setLayout(new java.awt.BorderLayout());

        jPanel5.setPreferredSize(new java.awt.Dimension(100, 24));
        jPanel5.setLayout(new java.awt.BorderLayout());

        speccat.setBackground(new java.awt.Color(202, 236, 221));
        speccat.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        speccat.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        speccat.setText("Indica la tipologia di impresa");
        speccat.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        speccat.setOpaque(true);
        speccat.setPreferredSize(new java.awt.Dimension(202, 24));
        jPanel5.add(speccat, java.awt.BorderLayout.PAGE_START);

        jPanel3.add(jPanel5, java.awt.BorderLayout.NORTH);

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setPreferredSize(new java.awt.Dimension(620, 300));
        jPanel1.setLayout(new java.awt.BorderLayout());

        tabellaRisultato.setBackground(new java.awt.Color(255, 255, 232));
        tabellaRisultato.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tabellaRisultato.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabellaRisultatoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tabellaRisultato);

        jPanel1.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jPanel3.add(jPanel1, java.awt.BorderLayout.WEST);

        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel2.setPreferredSize(new java.awt.Dimension(560, 70));
        jPanel2.setLayout(new java.awt.BorderLayout());

        lista.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        lista.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        lista.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lista);

        jPanel2.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel3.add(jPanel2, java.awt.BorderLayout.CENTER);

        add(jPanel3, java.awt.BorderLayout.NORTH);

        jPanel4.setLayout(new java.awt.BorderLayout());

        tabcat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane3.setViewportView(tabcat);

        jPanel4.add(jScrollPane3, java.awt.BorderLayout.CENTER);

        add(jPanel4, java.awt.BorderLayout.CENTER);

        pannelloPulsanti.setBackground(new java.awt.Color(255, 255, 255));
        pannelloPulsanti.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pannelloPulsanti.setPreferredSize(new java.awt.Dimension(600, 52));
        pannelloPulsanti.setLayout(null);

        buttonSalva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_save_white_24dp.png"))); // NOI18N
        buttonSalva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonSalva.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonSalva.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonSalva.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonSalva.setToolTipText("Salva catteristiche azienda e categorie");
        buttonSalva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvaActionPerformed(evt);
            }
        });
        pannelloPulsanti.add(buttonSalva);
        buttonSalva.setBounds(16, 4, 45, 45);

        help1.setBackground(new java.awt.Color(202, 236, 221));
        help1.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        help1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        help1.setText("ATTENZIONE la scelta errata di gruppo e specie di azienda comporta il rifacimento delle categorie");
        help1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        help1.setOpaque(true);
        help1.setPreferredSize(new java.awt.Dimension(202, 24));
        pannelloPulsanti.add(help1);
        help1.setBounds(170, 10, 780, 32);

        buttonElimina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png"))); // NOI18N
        buttonElimina.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonElimina.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonElimina.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonElimina.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonElimina.setToolTipText("Elimina categorie inserite");
        buttonElimina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEliminaActionPerformed(evt);
            }
        });
        pannelloPulsanti.add(buttonElimina);
        buttonElimina.setBounds(80, 4, 45, 45);

        add(pannelloPulsanti, java.awt.BorderLayout.SOUTH);
    }// </editor-fold>//GEN-END:initComponents

    private void listaMouseClicked(java.awt.event.MouseEvent evt)//GEN-FIRST:event_listaMouseClicked
    {//GEN-HEADEREND:event_listaMouseClicked
        if (modif) {
            if (evt.getClickCount() > 0 && lista.getSelectedRow() >= 0) {
                int prog = 0;
                for (int i = 0; i < lista.getRowCount(); i++) {
                    if ((Boolean) lista.getValueAt(i, FunzioniUtilita.getColumnIndex(lista, COL_SEL1))) {
                        if (specieSelez.equals("")) {
                            prog++;
                            specieSelez = (String) lista.getValueAt(i, FunzioniUtilita.getColumnIndex(lista, COL_COD1));
                        }
                    }
                }

                if (prog == 1) {
                    mostraCategorie();
                    buttonSalva.setEnabled(true);
                    help1.setVisible(true);
                } else {
                    JOptionPane.showMessageDialog(this, "Si puo' scegliere una sola specie di azienda", "Errore",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    specieSelez = "";

                }
            }
        }

    }//GEN-LAST:event_listaMouseClicked

    private void tabellaRisultatoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabellaRisultatoMouseClicked
        if (modif) {
            if (evt.getClickCount() > 0 && tabellaRisultato.getSelectedRow() >= 0) {
                int prog = 0;
                for (int i = 0; i < tabellaRisultato.getRowCount(); i++) {
                    if ((Boolean) tabellaRisultato.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_SEL))) {
                        if (gruppoSelez.equals("")) {
                            prog++;
                            gruppoSelez = (String) tabellaRisultato.getValueAt(i, FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_COD));
                        }
                    }
                }

                if (prog == 1) {
                    mostraListaSpecie(gruppoSelez);
                    repaint();
                } else {
                    JOptionPane.showMessageDialog(this, "Si puo' scegliere un solo gruppo di azienda", "Errore",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    gruppoSelez = "";
                }
            }
        }
    }//GEN-LAST:event_tabellaRisultatoMouseClicked

    private void buttonSalvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvaActionPerformed

        int ris = JOptionPane.showConfirmDialog(this, "Confermi la creazione automatica delle categorie?",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "gruppo", gruppoSelez);
            BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "specie", specieSelez);
            buttonSalva.setEnabled(false);

            //trasferisce su categorie BS dell'azienda
            String qry = "SELECT * FROM gruppibs"
                    + " WHERE ((gruppobsmastro = \"" + gruppoSelez + "\" AND gruppobsconto = \"" + specieSelez + "\")"
                    + " OR (gruppobsmastro = \"30\" AND gruppobsconto <> \"\") OR (gruppobsmastro = \"40\" AND gruppobsconto <> \"\"))"
                    + " AND gruppobssotto <> \"\""
                    + " AND gruppobsstato = 1 ORDER BY gruppobsmcs";
            QueryInterrogazioneClient qb = new QueryInterrogazioneClient(connAz, qry);
            qb.apertura();
            for (int i = 0; i < qb.numeroRecord(); i++) {
                Record rb = qb.successivo();
                Record rins = new Record();
                rins.insElem("catbsgruppo", rb.leggiStringa("gruppobsmastro"));
                rins.insElem("catbsspecie", rb.leggiStringa("gruppobsconto"));
                rins.insElem("catbssotto", rb.leggiStringa("gruppobssotto"));
                String codice = "";
                if (rb.leggiStringa("gruppobsd").equals("")) {
                    codice = rb.leggiStringa("gruppobsconto") + rb.leggiStringa("gruppobssotto") + "00";
                } else {
                    codice = rb.leggiStringa("gruppobsconto") + rb.leggiStringa("gruppobssotto") + rb.leggiStringa("gruppobsd");
                }
                rins.insElem("catbscod", codice);
                rins.insElem("catbsecc", rb.leggiStringa("gruppobsecc"));
                rins.insElem("catbsdescr", rb.leggiStringa("gruppobscateg")  + rb.leggiStringa("gruppobsnote"));
                rins.insElem("catbstipo", rb.leggiIntero("gruppobstipo"));
                if (rb.leggiStringa("gruppobsd").equals("")) {
                    rins.insElem("catbsquotac", rb.leggiDouble("gruppobsaliq"));
                    rins.insElem("catbsquotaf", rb.leggiDouble("gruppobsaliq"));
                } else {
                    rins.insElem("catbsquotac", rb.leggiDouble("gruppobsaliq2"));
                    rins.insElem("catbsquotaf", rb.leggiDouble("gruppobsaliq2"));
                }

                double quota = rins.leggiDouble("catbsquotaf") / 2;
                rins.insElem("catbsquota1", quota);

                rins.insElem("catbsflagtipoammc", 1);

                if (flagCriterioFiscale.equals("0")) {
                    rins.insElem("catbscriterio1", 2);
                    rins.insElem("catbscriterio2", (int) 1);
                } else {
                    rins.insElem("catbscriterio1", 0);
                    rins.insElem("catbscriterio2", (int) 0);
                }

                rins.insElem("catbsdetriva", (double) 100);
                rins.insElem("catbsdetrcosto", (double) 100);
                rins.insElem("catbsfabbrnoamm", (double) 0);
                rins.insElem("catbsnoammcf", 0);
                rins.insElem("catbsmaxcosto", (double) 0);
                rins.insElem("catbsmaxnolo", (double) 0);

                rins.insElem("catbsbilancio", rb.leggiStringa("gruppobsbil"));
                rins.insElem("catbsintero", (int) 0);
                rins.insElem("catbsstato", (int) 1);
                rins.convalida(true);

                String qry2 = "SELECT catbscod FROM categoriebs WHERE catbscod = \"" + rins.leggiStringa("catbscod") + "\"";
                QueryInterrogazioneClient qc = new QueryInterrogazioneClient(connAz, qry2);
                qc.apertura();
                if (qc.numeroRecord() < 1) {
                    QueryAggiornamentoClient qins = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "categoriebs", "catbsid", rins);
                    qins.esecuzione();                                       
                }
                qc.chiusura();
            }
            qb.chiusura();

            BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "plugjazz", "S");
            JOptionPane.showMessageDialog(this, "Categorie automatiche, generate. Installazione iniziale effettuata! ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            //
            mostraCategorie();
        }

    }//GEN-LAST:event_buttonSalvaActionPerformed

    private void buttonEliminaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEliminaActionPerformed
        String qry = "SELECT cespid FROM cespiti LIMIT 01";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            JOptionPane.showMessageDialog(this, "Esistono cespiti caricati. OCCORRE UN INTERVENTO DI ASSISTENZA ", "Invio",
                    JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            return;
        }
        q.chiusura();

        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "gruppo", "");
        BusinessLogicImpostazioni.scriviProprieta(connAz.conn, "cespiti_jazz", "specie", "");
        qry = "DELETE FROM categoriebs";
        QueryAggiornamentoClient qd = new QueryAggiornamentoClient(connAz, qry);
        qd.esecuzione();

        gruppoSelez = "";
        specieSelez = "";
        modif = true;

        String qrydelete = "DELETE FROM proprieta WHERE pprog=\"cespiti_jazz\" AND pprop=\"plugjazz\"";
        QueryAggiornamentoClient query = new QueryAggiornamentoClient(connAz, qrydelete);
        query.esecuzione();
        
        mostraCategorie();

        JOptionPane.showMessageDialog(this, "Categorie eliminate, riparti dalla scelta della tipologia e specie ", "Invio",
                JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        mostraListaGruppi();
        lista.setModel(new javax.swing.table.DefaultTableModel(
                new Object[][]{},
                new String[]{}
        ));

    }//GEN-LAST:event_buttonEliminaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonElimina;
    private ui.beans.PopButton buttonSalva;
    private javax.swing.JLabel help1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable lista;
    private javax.swing.JPanel pannelloPulsanti;
    private javax.swing.JLabel speccat;
    private javax.swing.JTable tabcat;
    private javax.swing.JTable tabellaRisultato;
    // End of variables declaration//GEN-END:variables

    private void mostraListaSpecie(String gruppo) {
        modif=true;
        String qry = "SELECT * FROM gruppibs"
                + " WHERE gruppobsmastro = \"" + gruppo + "\""
                + " AND gruppobsconto <> \"\""
                + " AND gruppobssotto = \"\""
                + " ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        int nRec = qRic.numeroRecord();
        // crea modello tabella
        final String[] names
                = {
                    COL_SEL1, COL_ID1, COL_COD1, COL_DES1
                };
        final Object[][] data = new Object[nRec][names.length];

        for (int i = 0; i < nRec; i++) {
            Record r = qRic.successivo();
            if (r.leggiStringa("gruppobsconto").equals(specieSelez)) {
                data[i][0] = new Boolean(true);
                modif = false;
            } else {
                data[i][0] = new Boolean(false);
            }
            data[i][1] = r.leggiIntero("gruppobsid");
            data[i][2] = r.leggiStringa("gruppobsconto");
            data[i][3] = r.leggiStringa("gruppobsdescr");
        }
        qRic.chiusura();

        TableModel modello = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(lista, COL_SEL1) && modif) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };

        if (!modif) {
            mostraCategorie();
            speccat.setText("Azienda appartiene a gruppo " + gruppoSelez + " specie " + specieSelez);
        } else {
            speccat.setText("Indica la tipologia di impresa");

        }
        
        lista.setModel(modello);
        FunzioniUtilita.impostaTabellaPopNoColor(lista, true);

        lista.getColumn(COL_SEL1).setPreferredWidth(50);
        lista.getColumn(COL_ID1).setPreferredWidth(50);
        lista.getColumn(COL_COD1).setPreferredWidth(70);
        lista.getColumn(COL_DES1).setPreferredWidth(700);

//        lista.getColumn(COL_SEL1).setHeaderRenderer(headerRenderer);
    }

    private void mostraListaGruppi() 
    {
        String qry = "SELECT * FROM gruppibs WHERE gruppobsconto = \"\" AND gruppobstipo = 2 ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        int nRec = qRic.numeroRecord();

        // crea modello tabella
        final String[] namesg
                = {
                    COL_SEL, COL_ID, COL_GRU, COL_COD, COL_DES
                };
        final Object[][] datag = new Object[nRec][namesg.length];

        for (int i = 0; i < nRec; i++) {
            Record r = qRic.successivo();
            if (r.leggiStringa("gruppobsmcs").equals(gruppoSelez)) {
                datag[i][0] = new Boolean(true);
                modif = false;
            } else {
                datag[i][0] = new Boolean(false);
            }
            datag[i][1] = r.leggiIntero("gruppobsid");
            datag[i][2] = r.leggiStringa("gruppobsmastro");
            datag[i][3] = r.leggiStringa("gruppobsmcs");
            datag[i][4] = r.leggiStringa("gruppobsdescr");
        }
        qRic.chiusura();

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return namesg.length;
            }

            public int getRowCount() {
                return datag.length;
            }

            public Object getValueAt(int row, int col) {
                return datag[row][col];
            }

            public String getColumnName(int column) {
                return namesg[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tabellaRisultato, COL_SEL) && modif) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                datag[row][column] = aValue;
            }
        };
        tabellaRisultato.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tabellaRisultato, true);

        tabellaRisultato.getColumn(COL_SEL).setPreferredWidth(40);
        tabellaRisultato.getColumn(COL_ID).setPreferredWidth(40);
        tabellaRisultato.getColumn(COL_GRU).setPreferredWidth(90);
        tabellaRisultato.getColumn(COL_COD).setPreferredWidth(70);
        tabellaRisultato.getColumn(COL_DES).setPreferredWidth(750);

//        tabellaRisultato.getColumn(COL_SEL).setHeaderRenderer(headerRenderer);
        if (!modif) {
            mostraListaSpecie(gruppoSelez);
            speccat.setText("Azienda appartiene a gruppo " + gruppoSelez + " specie " + specieSelez);
        } else {
            speccat.setText("Indica la tipologia di impresa");

        }
        help1.setVisible(false);
        buttonSalva.setEnabled(true);
    }

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void mostraCategorie() {
        String qry = "SELECT * FROM gruppibs"
                + " WHERE gruppobsmastro = \"" + gruppoSelez + "\""
                + " AND gruppobsconto = \"" + specieSelez + "\""
                + " AND gruppobssotto <> \"\""
                + " ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        int nRec = qRic.numeroRecord();
        codEcc = new String[nRec];
        final String[] names
                = {
                    COL_ID2, COL_COD2, COL_DES2, COL_ALIQ2, COL_INS2
                };
        final Object[][] data = new Object[nRec][names.length];

        codEcc = new String[nRec];
        for (int i = 0; i < nRec; i++) {
            Record r = qRic.successivo();

            data[i][0] = r.leggiIntero("gruppobsid");
            data[i][1] = r.leggiStringa("gruppobssotto");
            data[i][2] = r.leggiStringa("gruppobscateg");
            data[i][3] = Formattazione.formValuta(r.leggiDouble("gruppobsaliq"), 3, 2, 0);

            codEcc[i] = r.leggiStringa("gruppobsecc");
        }
        qRic.chiusura();

        TableModel modello = new AbstractTableModel() {
            @Override
            public int getColumnCount() {
                return names.length;
            }

            @Override
            public int getRowCount() {
                return data.length;
            }

            @Override
            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            @Override
            public String getColumnName(int column) {
                return names[column];
            }

            @Override
            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            @Override
            public boolean isCellEditable(int row, int col) {
                return col == FunzioniUtilita.getColumnIndex(tabcat, COL_INS2);
            }

            @Override
            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };

        tabcat.setModel(modello);
        FunzioniUtilita.impostaTabellaPopNoColor(tabcat, true);

        TableColumn buttonColumn1 = tabcat.getColumn(COL_INS2);
        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                if (!codEcc[row].equals("")) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_white_24dp.png")));
                    button.setToolTipText("Presenti categorie con eccezioni. Inserisce su tabella categorie aziendali.");
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_label_white_24dp.png")));
                    button.setToolTipText("Funzione NON prevista.");
                }

            }
        });
        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                if (!codEcc[row].equals("")) {
                    DialogVisualizzaCategorieSTD d = new DialogVisualizzaCategorieSTD(formGen, true, connAz, azienda, codEcc[row]);
                    UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                    d.show();
                }
            }
        });
        buttonColumn1.setCellRenderer(button1);
        buttonColumn1.setCellEditor(button1);

        tabcat.getColumn(COL_ID2).setPreferredWidth(50);
        tabcat.getColumn(COL_COD2).setPreferredWidth(90);
        tabcat.getColumn(COL_DES2).setPreferredWidth(400);
        tabcat.getColumn(COL_ALIQ2).setPreferredWidth(50);

        tabcat.getColumn(COL_INS2).setHeaderRenderer(headerRenderer);
    }
}
