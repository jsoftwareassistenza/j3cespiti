package cespitipop;

/*
 * BeanGestCanale.java
 *
 * Created on 8 giugno 2006, 10.58
 */
import nucleo.QueryInterrogazioneClient;
import nucleo.ConnessioneServer;
import nucleo.UtilForm;
import nucleo.Record;
import javax.swing.*;
import java.awt.*;
import nucleo.ExecGestioni;
import ui.eventi.GestoreEventiBeans;

/**
 *
 * @author svilupp
 */
public class GestRicCategoriaCespitiPop extends javax.swing.JPanel {

    ConnessioneServer connAz = null;
    ConnessioneServer connCom = null;
    int catId = -1;
    boolean dlg = false, flag = false;
    DialogRicercaCategorieCespitiPop dlc = null;
    boolean ric = false;
    JLabel descr = null;
    Frame formGen = null;
    Window padre = null;
    boolean ins;
    boolean msgErrore = true;
    ExecGestioni proc2 = null;

    /**
     * Creates new form BeanGestCanale
     */
    public GestRicCategoriaCespitiPop() {
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());

        categoria.setEventoFocusLost(new GestoreEventiBeans() {
            public void lancia() {
                QueryInterrogazioneClient qRic = null;
                int nRec;
                Record r = null;
                String qry = "";

                if (!flag) {
                    flag = true;
                    //Edit.deselComponenteTesto(canale);
                    if (!ric) {
                        if (categoria.getText().trim().equals("")) {
                            catId = -1;
                            categoria.setText("");
                            if (descr != null) {
                                descr.setText("");
                            }
                        } else {
                            // ricerca esatta
                            qry = "SELECT * FROM categoriebs WHERE catbscod = \""
                                    + categoria.getText().trim() + "\" OR catbsdescr=\"" + categoria.getText().trim() + "\"";
                            qRic = new QueryInterrogazioneClient(connAz, qry);
                            qRic.apertura();
                            nRec = qRic.numeroRecord();
                            if (nRec == 1) {
                                // trovata corrispondenza esatta
                                r = qRic.successivo();
                                catId = r.leggiIntero("catbsid");
                                if (descr != null) {
                                    descr.setText(r.leggiStringa("catbsdescr"));
                                    categoria.setText(r.leggiStringa("catbscod"));
                                }
                                qRic.chiusura();
                            } else {
                                // ricerca corrispondenza parziale
                                qRic.chiusura();
                                qry = "SELECT * FROM categoriebs WHERE catbscod LIKE \"%" + categoria.getText().trim() + "%\" OR catbsdescr LIKE \"%" + categoria.getText().trim() + "%\"";
                                qRic = new QueryInterrogazioneClient(connAz, qry);
                                qRic.apertura();
                                nRec = qRic.numeroRecord();
                                if (nRec == 0) {
                                    // nessun punto em.doc. trovato
                                    qRic.chiusura();
                                    if (msgErrore) {
                                        if (!dlg) {
                                            dlg = true;
                                            JOptionPane.showMessageDialog(formGen, "Non esistono categorie contenenti il testo specificato",
                                                    "Errore", JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                                            categoria.requestFocus();
                                            svuota();
                                            dlg = false;
                                        }
                                    } else {
                                        if (descr != null) {
                                            descr.setText("");
                                        }
                                        catId = -1;
                                    }
                                } else {
                                    if (dlc == null) {
                                        // trovate corrispondenze => apre lista
                                        dlc = new DialogRicercaCategorieCespitiPop(formGen, true, connAz, connCom, categoria.getText().trim());
                                        UtilForm.posRelSchermo(dlc, UtilForm.CENTRO);
                                        dlc.setSize(820, 400);
                                        dlc.setVisible(true);
                                        padre.requestFocus();
                                        categoria.setText(dlc.leggiCodice());
                                        if (descr != null) {
                                            descr.setText(dlc.leggiDescrizione());
                                        }
                                        catId = dlc.leggiId();
                                        dlc = null;
                                    }
                                }
                            }
                        }
                    }
                    if (proc2 != null) {
                        proc2.lancia();
                    }
                    flag = false;
                }
            }
        });

        categoria.setEventoFocusGained(new GestoreEventiBeans() {
            public void lancia() {
                ric = false;
            }
        });

        categoria.setEventoKeyPressed(new GestoreEventiBeans() {
            public void lancia() {
                if (categoria.getText().length() > 3) {
                    ric = true;
                    ricCan.doClick();
                }
            }
        });

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        documentoTesto1 = new halleytech.models.DocumentoTesto();
        ricCan = new ui.beans.PopButton();
        categoria = new ui.beans.CampoTestoPop();

        documentoTesto1.scriviLung(10);

        setBackground(new java.awt.Color(255, 255, 255));
        setLayout(null);

        ricCan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_search_white_20dp.png"))); // NOI18N
        ricCan.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 255, 255)));
        ricCan.setFocusable(false);
        ricCan.setFont(new java.awt.Font("Lucida Sans", 0, 12)); // NOI18N
        ricCan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ricCanActionPerformed(evt);
            }
        });
        add(ricCan);
        ricCan.setBounds(120, 0, 24, 24);
        add(categoria);
        categoria.setBounds(0, 0, 116, 24);
    }// </editor-fold>//GEN-END:initComponents

    private void ricCanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ricCanActionPerformed
        DialogRicercaCategorieCespitiPop d = null;
        int id;
        String nome;

        d = new DialogRicercaCategorieCespitiPop(formGen, true, connAz, connCom, "");
        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
        d.setVisible(true);
        id = d.leggiId();
        if (id != -1) {
            categoria.setText(d.leggiCodice());
            if (descr != null) {
                descr.setText(d.leggiDescrizione());
            }
            catId = id;
        }
        ric = false;
        if (proc2 != null) {
            proc2.lancia();
        }
        padre.requestFocus();
    }//GEN-LAST:event_ricCanActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.CampoTestoPop categoria;
    private halleytech.models.DocumentoTesto documentoTesto1;
    private ui.beans.PopButton ricCan;
    // End of variables declaration//GEN-END:variables

    public void passaParametri(ConnessioneServer connAz, Window padre, Frame formGen) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.padre = padre;
        this.formGen = formGen;
        this.ins = true;

    }

    public void passaParametri(ConnessioneServer connAz, ConnessioneServer connCom,
            Window padre, Frame formGen, JLabel descr, boolean ins) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.padre = padre;
        this.formGen = formGen;
        this.descr = descr;
        this.ins = ins;

    }

    /**
     * Visualizza il nominativo nel campo
     *
     *
     */
    public void visualizzaDati(String codice) {
        categoria.setText(codice);
    }

    /**
     * Visualizza il nominativo nel campo e memorizza il'id dell'anagrafica
     * associata.
     *
     */
    public void visualizzaDati(int id, String codice) {
        catId = id;
        visualizzaDati(codice);
        if (proc2 != null) {
            proc2.lancia();
        }
    }

    public void visualizzaDati(int id, String codice, String ds) {
        catId = id;
        categoria.setText(codice);
        if (descr != null) {
            descr.setText(ds);
        }
        if (proc2 != null) {
            proc2.lancia();
        }
    }

    public void gestioneAggiuntiva(ExecGestioni eg) {
        this.proc2 = eg;
    }

    /**
     * Restituisce un intero che rappresenta l'ID del record anagrafica corrente
     * dell'oggetto
     */
    public int leggiId() {
        return catId;
    }

    /**
     * reinizializza l'oggetto
     */
    public void svuota() {
        catId = -1;
        categoria.setText("");
        if (descr != null) {
            descr.setText("");
        }
        if (proc2 != null) {
            proc2.lancia();
        }
    }

    /**
     * permette di specificare la lunghezza in pixel della casella di testo che
     * contiene il nominativo
     *
     * @param l intero lunghezza
     */
    public void lunghezza(int l) {
        categoria.setSize(l, 20);
        ricCan.setLocation(l + 4, 0);
        this.setSize(l + 48, 20);
    }

    /**
     * abilita o disabilita testo e pulsanti dell'oggetto
     *
     * @param b True o False
     */
    public void abilita(boolean b) {
        categoria.setEnabled(b);
        ricCan.setEnabled(b);

    }

    public void fuoco() {
        categoria.requestFocus();
    }

    public String leggiCodice() {
        return categoria.getText().trim();
    }
}
