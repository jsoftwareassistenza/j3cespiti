package cespitipop;

/*
 * DialogStampaDoc.java
 *
 * Created on 1 giugno 2006, 11.35
 */
//import documenti.ausili.PannelloStampaDoc;
import nucleo.ListaFrames;
import nucleo.ConnessioneServer;
import java.awt.*;
import java.util.ArrayList;
import nucleo.Record;
import nucleo.UtilForm;

/**
 *
 * @author svilupp
 */
public class DialogGestVenditaCespitiPop extends javax.swing.JDialog {

    PannelloGestVenditaCespiti pp = null;
    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;
    
    private ListaFrames lf;
    
    private String nome="";
    private String azienda="";    
    private String esContabile = "";
    
    private int idVis;
    private int tipodoc = 0;    
    private int operazione;
    private int tipodett = 0;
    private int tipologia=-1;
    
    private Record rDoc = null;
    private Record rCli = null;
    private ArrayList<Record> vpref = null;
    private ArrayList<Record> vcarrello = null;

    /**
     * Creates new form DialogStampaDoc
     */
    public DialogGestVenditaCespitiPop(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
        pp = new PannelloGestVenditaCespiti(this, parent);
        getContentPane().add(pp);
        UtilForm.ridimensionaPercentuale(this, 80);
        //this.getContentPane().setPreferredSize(new Dimension(1250, 734));
        //this.pack();
    }

    public DialogGestVenditaCespitiPop(Frame parent, boolean modal, ConnessioneServer connAz, String azienda,
            String esContabile, String titolo, int idVis, int operazione) {
        this(parent, modal);
        this.setTitle(titolo);
        this.connAz = connAz;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.operazione = operazione;
    }

    public DialogGestVenditaCespitiPop(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom,
            String azienda, String esContabile, int idVis, int operazione, ListaFrames lf, String nome) {
        this(parent, modal);
        this.setTitle("Az: " + azienda + "    Documento vendita cespite");
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.operazione = operazione;
        this.lf = lf;
        this.nome = nome;
    }

    public DialogGestVenditaCespitiPop(Frame parent, boolean modal, ConnessioneServer connAz,
            String esContabile, String titolo, int idVis, int operazione, ListaFrames lf, String nome) {
        this(parent, modal);
        this.setTitle(titolo);
        this.connAz = connAz;
        this.esContabile = esContabile;
        this.idVis = idVis;
        this.operazione = operazione;
        this.lf = lf;
        this.nome = nome;
    }

    public DialogGestVenditaCespitiPop(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom,
            String azienda, String esContabile, int movdocId, int tipodoc) {
        this(parent, modal);
        if(tipodoc==1)
        {            
            this.setTitle("Az: " + azienda + "   Documento nota credito cespite");
        }
        else
        {               
            this.setTitle("Az: " + azienda + "   Documento vendita cespite");
        }
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = movdocId;
        this.tipodoc = tipodoc;
    }

    public DialogGestVenditaCespitiPop(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom,
            String azienda, String esContabile, int movdocId, int tipodoc, Record rDoc) {
        this(parent, modal);
        if(tipodoc==1)
        {            
            this.setTitle("Az: " + azienda + "   Documento nota credito cespite");
        }
        else
        {               
            this.setTitle("Az: " + azienda + "   Documento vendita cespite");
        }
        
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = movdocId;
        this.tipodoc = tipodoc;
        this.rDoc = rDoc;
    }

    public DialogGestVenditaCespitiPop(Frame parent, boolean modal, ConnessioneServer connAz, ConnessioneServer connCom,
            String azienda, String esContabile, int movdocId, int tipodoc, Record rDoc, Record rCli, ArrayList<Record> vpref,ArrayList<Record> vcarrello, int tipodett) {
        this(parent, modal);
        this.setTitle("Az: " + azienda + "    Documento vendita cespite");
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = movdocId;
        this.tipodoc = tipodoc;
        this.rDoc = rDoc;
        this.rCli = rCli;
        this.vpref = vpref;
        this.vcarrello=vcarrello;
        this.tipodett = tipodett;
    }

    /**
     * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this method is always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Vendita cespiti");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));
        getAccessibleContext().setAccessibleName("Vendita cespiti");

        setSize(new java.awt.Dimension(1266, 773));
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened

        pp.impostaParametri(tipodoc);
        pp.impostaRecDoc(rDoc);
        pp.impostaModalitaNotaCredito(tipologia);
        pp.inizializza(connAz, connCom, azienda, esContabile, idVis, null, "");
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
    }//GEN-LAST:event_formWindowClosed

    public void abilitaSalva(boolean stato) {
//        pp.buttonStampa.setEnabled(stato);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    public boolean documentoModificato() {
        return pp.documentoModificato();
    }

    public int idMovimento() {
        return pp.idMovimento();
    }

    public String esContMovimento() {
        return pp.esContMovimento();
    }
    
    public void impostaModalitaNotaCredito(int tipologia) {
        this.tipologia = tipologia;
    }
}
