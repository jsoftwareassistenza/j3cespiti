package cespitipop;

import java.awt.*;
import java.util.ArrayList;
import nucleo.ConnessioneServer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.Query;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogicImpostazioni;

public class PannelloCategorieHome extends javax.swing.JPanel implements PannelloPop, PannelloPopListener 
{
    private final String COL_CATPADRE = "Cod.";
    private final String COL_NOME = "Categoria";
    private final String COL_TIPO = "Bil. cee";

    private final String COL_NELEM = "N. beni";
    private final String COL_VALORE = "Valore";
    private final String COL_FONDO = "Fondo";
    private final String COL_RESIDUO = "Residuo";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;
    private PannelloPopListener listener = null;
    private Window padre;
    private Frame formGen;
    private int idVis = -1;
    private String azienda = "";
    private String esContabile = "";

    private int[] id;
    private int idSelez = -1;

    String gruppoSelez = "";
    String specieSelez = "";

    ArrayList<Record> vcatbsid = new ArrayList();
    ArrayList<Record> vcatint = new ArrayList();
//    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    public PannelloCategorieHome() {
        this.initComponents();
    }

    public PannelloCategorieHome(Window padre, Frame formGen) {
        this();
        this.padre = padre;
        this.formGen = formGen;

    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        gruppoSelez = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "gruppo");
        
        String qry2 = "SELECT gruppobsmcs,gruppobsdescr FROM gruppibs WHERE gruppobsmcs = \"" + gruppoSelez + "\"";
        Query q2 = new Query(connAz, qry2);
        q2.apertura();
        if (q2.numeroRecord() > 0) {
            Record r2 = q2.successivo();
            catbsgruppo.setText(r2.leggiStringa("gruppobsdescr"));
            gru.setText(gruppoSelez);
        }
        q2.chiusura();

        specieSelez = BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "specie");
        String gs = gruppoSelez + specieSelez;
        String qry3 = "SELECT gruppobsconto,gruppobsdescr FROM gruppibs WHERE gruppobsmcs = \"" + gs + "\"";
        Query q3 = new Query(connAz, qry3);
        q3.apertura();
        if (q3.numeroRecord() > 0) {
            Record r3 = q3.successivo();
            catbsspecie.setText(r3.leggiStringa("gruppobsdescr"));
            spec.setText(specieSelez);
        }
        q3.chiusura();

        labvalore.setVisible(false);
        labfondo.setVisible(false);
        labresiduo.setVisible(false);
        valorex.setVisible(false);
        fondox.setVisible(false);
        residuox.setVisible(false);
        
        interrogazione();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        gru = new javax.swing.JLabel();
        catbsgruppo = new javax.swing.JLabel();
        spec = new javax.swing.JLabel();
        catbsspecie = new javax.swing.JLabel();
        pCentro = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tabcat = new javax.swing.JTable();
        pPiede = new javax.swing.JPanel();
        labvalore = new javax.swing.JLabel();
        valorex = new javax.swing.JLabel();
        labfondo = new javax.swing.JLabel();
        fondox = new javax.swing.JLabel();
        labresiduo = new javax.swing.JLabel();
        residuox = new javax.swing.JLabel();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 40));
        pFiltri.setLayout(null);

        gru.setBackground(new java.awt.Color(202, 236, 221));
        gru.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        gru.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        gru.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        gru.setOpaque(true);
        pFiltri.add(gru);
        gru.setBounds(20, 8, 40, 24);

        catbsgruppo.setBackground(new java.awt.Color(202, 236, 221));
        catbsgruppo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        catbsgruppo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        catbsgruppo.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        catbsgruppo.setOpaque(true);
        pFiltri.add(catbsgruppo);
        catbsgruppo.setBounds(68, 8, 472, 24);

        spec.setBackground(new java.awt.Color(202, 236, 221));
        spec.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        spec.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        spec.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        spec.setOpaque(true);
        pFiltri.add(spec);
        spec.setBounds(552, 8, 40, 24);

        catbsspecie.setBackground(new java.awt.Color(202, 236, 221));
        catbsspecie.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        catbsspecie.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        catbsspecie.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        catbsspecie.setOpaque(true);
        pFiltri.add(catbsspecie);
        catbsspecie.setBounds(600, 8, 500, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        pCentro.setBackground(new java.awt.Color(255, 255, 255));
        pCentro.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setBackground(new java.awt.Color(255, 255, 255));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(540, 402));

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        pCentro.add(jScrollPane1, java.awt.BorderLayout.WEST);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(12, 100));
        jPanel1.add(jPanel2, java.awt.BorderLayout.WEST);

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));

        tabcat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane2.setViewportView(tabcat);

        jPanel1.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        pCentro.add(jPanel1, java.awt.BorderLayout.CENTER);

        add(pCentro, java.awt.BorderLayout.CENTER);

        pPiede.setBackground(new java.awt.Color(255, 255, 255));
        pPiede.setPreferredSize(new java.awt.Dimension(800, 40));
        pPiede.setLayout(null);

        labvalore.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        labvalore.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labvalore.setText("Totale Valore");
        pPiede.add(labvalore);
        labvalore.setBounds(552, 8, 100, 24);

        valorex.setBackground(new java.awt.Color(202, 236, 221));
        valorex.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        valorex.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        valorex.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        valorex.setOpaque(true);
        pPiede.add(valorex);
        valorex.setBounds(660, 8, 96, 24);

        labfondo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        labfondo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labfondo.setText("Fondo");
        pPiede.add(labfondo);
        labfondo.setBounds(756, 8, 72, 24);

        fondox.setBackground(new java.awt.Color(202, 236, 221));
        fondox.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        fondox.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        fondox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        fondox.setOpaque(true);
        pPiede.add(fondox);
        fondox.setBounds(836, 8, 96, 24);

        labresiduo.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        labresiduo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        labresiduo.setText("Residuo");
        pPiede.add(labresiduo);
        labresiduo.setBounds(932, 8, 72, 24);

        residuox.setBackground(new java.awt.Color(202, 236, 221));
        residuox.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        residuox.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        residuox.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        residuox.setOpaque(true);
        pPiede.add(residuox);
        residuox.setBounds(1012, 8, 96, 24);

        add(pPiede, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];

            interrogazioneInterne(tab.convertRowIndexToModel(tab.getSelectedRow()));
        } else {
            labvalore.setVisible(false);
            labfondo.setVisible(false);
            labresiduo.setVisible(false);
            valorex.setVisible(false);
            fondox.setVisible(false);
            residuox.setVisible(false);
            tabcat.setModel(new javax.swing.table.DefaultTableModel(
                    new Object[][]{},
                    new String[]{}
            ));
        }
    }//GEN-LAST:event_tabMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel catbsgruppo;
    private javax.swing.JLabel catbsspecie;
    private javax.swing.JLabel fondox;
    private javax.swing.JLabel gru;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labfondo;
    private javax.swing.JLabel labresiduo;
    private javax.swing.JLabel labvalore;
    private javax.swing.JPanel pCentro;
    private javax.swing.JPanel pFiltri;
    private javax.swing.JPanel pPiede;
    private javax.swing.JLabel residuox;
    private javax.swing.JLabel spec;
    private javax.swing.JTable tab;
    private javax.swing.JTable tabcat;
    private javax.swing.JLabel valorex;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener l) {
        listener = l;
    }

    private void interrogazione() 
    {
        //
        vcatbsid = new ArrayList();
        String qry = "SELECT DISTINCT gruppobsid,gruppobsmcs,gruppobsmastro,gruppobsconto,gruppobssotto,gruppobsdescr,gruppobscateg,gruppobsnote,gruppobsbil FROM categoriebs"
                + " LEFT JOIN gruppibs ON (catbsgruppo = gruppobsmastro AND catbsspecie = gruppobsconto AND catbssotto = gruppobssotto) WHERE catbsstato = 1"
                + " AND NOT ISNULL(gruppobsmcs) ORDER BY catbsgruppo,catbsspecie,catbssotto,catbscod";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vcatbsid.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        //
        mostraLista();
    }

    private void interrogazioneInterne(int riga) {
        Record rec = vcatbsid.get(riga);
        vcatint = new ArrayList();
        String qry = "SELECT * FROM categoriebs  WHERE catbsstato = 1"
                + " AND catbsgruppo = \"" + rec.leggiStringa("gruppobsmastro") + "\" AND catbsspecie = \"" + rec.leggiStringa("gruppobsconto") + "\" AND catbssotto = \"" + rec.leggiStringa("gruppobssotto") + "\""
                + " ORDER BY catbsgruppo,catbsspecie,catbssotto,catbscod";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            vcatint.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        //
        mostraListaCatInterne();
    }

    private void mostraListaCatInterne() {

        final String[] names = {
            COL_CATPADRE, COL_NOME, COL_NELEM, COL_VALORE, COL_FONDO, COL_RESIDUO
        };
        final Object[][] data = new Object[vcatint.size()][names.length];
        double valore = 0;
        double fondo = 0;
        double residuo = 0;

        for (int i = 0; i < vcatint.size(); i++) {
            Record rec = vcatint.get(i);

            data[i][0] = rec.leggiStringa("catbscod");
            data[i][1] = rec.leggiStringa("catbsdescr");
            String qry2 = "SELECT COUNT(cespid) as nn FROM cespiti WHERE cespcategid = " + rec.leggiIntero("catbsid") +" AND cespdataalien = \"0000-00-00\"";
            QueryInterrogazioneClient q2 = new QueryInterrogazioneClient(connAz, qry2);
            q2.apertura();
            Record r2 = q2.successivo();
            q2.chiusura();
            if (r2 == null) {
                data[i][2] = "";
            } else {
                data[i][2] = "" + r2.leggiIntero("nn");
            }
            String qry4 = "SELECT  cbsbenitot,cbsfondotot,cbsresiduo FROM chiusurebs WHERE cbscategid = " + rec.leggiIntero("catbsid")
                    + " ORDER BY cbseserc DESC LIMIT 01";
            QueryInterrogazioneClient q4 = new QueryInterrogazioneClient(connAz, qry4);
            q4.apertura();
            Record r4 = q4.successivo();
            q4.chiusura();
            if (r4 != null) 
            {                                    
                data[i][3] =r4.leggiStringa("cbsbenitot");               
                valore += Formattazione.estraiDouble(r4.leggiStringa("cbsbenitot"));
                data[i][4] = r4.leggiStringa("cbsfondotot");                
                fondo += Formattazione.estraiDouble(r4.leggiStringa("cbsfondotot"));                
                data[i][5] = r4.leggiStringa("cbsresiduo");                
                residuo += Formattazione.estraiDouble(r4.leggiStringa("cbsresiduo"));
            } else {
                data[i][3] = "";
                data[i][4] = "";
                data[i][5] = "";
            }

        }
        labvalore.setVisible(true);
        labfondo.setVisible(true);
        labresiduo.setVisible(true);
        valorex.setVisible(true);
        fondox.setVisible(true);
        residuox.setVisible(true);
        valorex.setText(Formattazione.formValuta(valore, 10, 2, 0));
        fondox.setText(Formattazione.formValuta(fondo, 10, 2, 0));
        residuox.setText(Formattazione.formValuta(residuo, 10, 2, 0));

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                return false;
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tabcat.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tabcat, true);

        tabcat.getColumn(COL_CATPADRE).setPreferredWidth(60);
        tabcat.getColumn(COL_NOME).setPreferredWidth(360);
        tabcat.getColumn(COL_NELEM).setPreferredWidth(50);
        tabcat.getColumn(COL_VALORE).setPreferredWidth(110);
        tabcat.getColumn(COL_FONDO).setPreferredWidth(110);
        tabcat.getColumn(COL_RESIDUO).setPreferredWidth(110);

    }

    private void mostraLista() {

        final String[] names = {
            COL_CATPADRE, COL_NOME, COL_TIPO
        };
        final Object[][] data = new Object[vcatbsid.size()][names.length];

        id = new int[vcatbsid.size()];
        for (int i = 0; i < vcatbsid.size(); i++) {
            Record rec = vcatbsid.get(i);

            data[i][0] = rec.leggiStringa("gruppobsconto") + "." + rec.leggiStringa("gruppobssotto");
            data[i][1] = rec.leggiStringa("gruppobscateg") + rec.leggiStringa("gruppobsnote");
            data[i][2] = rec.leggiStringa("gruppobsbil");

            id[i] = rec.leggiIntero("gruppobsid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                return false;
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tab, true);

        tab.getColumn(COL_CATPADRE).setPreferredWidth(60);
        tab.getColumn(COL_NOME).setPreferredWidth(340);
        tab.getColumn(COL_TIPO).setPreferredWidth(100);

//        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
//        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
//        tab.getColumn(COL_MAX).setCellRenderer(rightRenderer);
//        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
//        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
    }

    @Override
    public void notificaChiusura(PannelloPop origine, String key) {

    }

    @Override
    public void notificaCambioTitolo(PannelloPop origine, String key, String nuovoTitolo) {

    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo) {

    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo, Color coloresfondo, int idVis) {

    }
}
