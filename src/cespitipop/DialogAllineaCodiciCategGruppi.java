package cespitipop;

/*
 * DialogListaCespiti.java
 *
 * Created on 8 novembre 2006, 11.07
 */
import java.awt.Frame;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableModel;
import nucleo.*;



/**
 *
 * @author svilupp
 */
public class DialogAllineaCodiciCategGruppi extends javax.swing.JDialog {

    private final String COL_sel = "S";
    private final String COL_gru = "Gruppo";
    private final String COL_spec = "Specie";
    private final String COL_categ = "Categ. ministeriale";
    private final String COL_CAT = "Categ. INTERNA";
    private final String COL_Q = "% ";

    private ConnessioneServer connAz;
    private int[] id;
    
    ArrayList<Record> vgruppi = new ArrayList();
    ArrayList<Record> vmcsGru = new ArrayList();
    
    private String gruppoSelez = "";
    private String specieSelez = "";
    
    private String[] mcsSpec;
    private String[] mcsCat;

    /**
     * Creates new form DialogListaCespiti
     */
    public DialogAllineaCodiciCategGruppi(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();nucleo.FunzioniUtilita.memorizzaClasseUtilizzata(this.getClass().getName());
    }

    public DialogAllineaCodiciCategGruppi(Frame parent, boolean modal, ConnessioneServer connAz) {
        this(parent, modal);
        this.connAz = connAz;

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        comboGruppi = new javax.swing.JComboBox();
        comboSpecie = new javax.swing.JComboBox();
        comboCategorie = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        buttonSalva = new ui.beans.PopButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Unifica ai codici ministeriali");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(100, 80));
        jPanel1.setLayout(null);

        comboGruppi.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboGruppi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .Gruppo.  >" }));
        comboGruppi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboGruppiActionPerformed(evt);
            }
        });
        jPanel1.add(comboGruppi);
        comboGruppi.setBounds(12, 16, 484, 24);

        comboSpecie.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboSpecie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .Specie.  >" }));
        comboSpecie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSpecieActionPerformed(evt);
            }
        });
        jPanel1.add(comboSpecie);
        comboSpecie.setBounds(504, 16, 488, 24);

        comboCategorie.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        comboCategorie.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<  .categoria.  >" }));
        jPanel1.add(comboCategorie);
        comboCategorie.setBounds(12, 48, 980, 24);

        getContentPane().add(jPanel1, java.awt.BorderLayout.PAGE_START);

        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jScrollPane1.setViewportView(tab);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setPreferredSize(new java.awt.Dimension(100, 40));
        jPanel2.setLayout(null);

        buttonSalva.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_save_white_20dp.png"))); // NOI18N
        buttonSalva.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonSalva.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonSalva.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonSalva.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonSalva.setToolTipText("");
        buttonSalva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalvaActionPerformed(evt);
            }
        });
        jPanel2.add(buttonSalva);
        buttonSalva.setBounds(20, 4, 44, 32);

        jLabel1.setFont(new java.awt.Font("Dialog", 2, 12)); // NOI18N
        jLabel1.setText("Controllare che la sottocategoria interna corrisponda alla categoria ministeriale, altrimenti correggere");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(144, 8, 660, 28);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_END);

        setSize(new java.awt.Dimension(1023, 694));
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        popolaComboGruppi();
        interrogazione();
    }//GEN-LAST:event_formWindowOpened

    private void comboGruppiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboGruppiActionPerformed
        if (comboGruppi.getSelectedIndex() > 0) 
        {
            Record r = vmcsGru.get(comboGruppi.getSelectedIndex() - 1);
            gruppoSelez = r.leggiStringa("gruppobsmcs");            
            popolaComboSpecie(gruppoSelez);
        } else
        {
            comboSpecie.setSelectedIndex(0);
        }
    }//GEN-LAST:event_comboGruppiActionPerformed

    private void comboSpecieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSpecieActionPerformed
        if (comboGruppi.getSelectedIndex() > 0 && comboSpecie.getSelectedIndex() > 0)
        {
            popolaComboCategorie(gruppoSelez, mcsSpec[comboSpecie.getSelectedIndex() - 1]);
        } else 
        {
            comboCategorie.setSelectedIndex(0);
        }
    }//GEN-LAST:event_comboSpecieActionPerformed

    private void buttonSalvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalvaActionPerformed
        if (comboGruppi.getSelectedIndex() > 0 && comboSpecie.getSelectedIndex() > 0 && comboCategorie.getSelectedIndex() > 0)
        {
            for (int i = 0; i < tab.getRowCount(); i++)
            {
                if (((Boolean) tab.getValueAt(i, 0)).booleanValue()) {
                    String qry = "";

                    if (((String) tab.getValueAt(i, FunzioniUtilita.getColumnIndex(tab, COL_gru))).compareTo("29") > 0) {
                        qry = "UPDATE categoriebs SET "
                                + "catbsgruppo = \"" + (String) tab.getValueAt(i, FunzioniUtilita.getColumnIndex(tab, COL_gru)) + "\","
                                + "catbsspecie = \"" + (String) tab.getValueAt(i, FunzioniUtilita.getColumnIndex(tab, COL_gru)) + "\","
                                + "catbssotto = \"" + mcsCat[comboCategorie.getSelectedIndex() - 1] + "\""
                                + " WHERE catbsid = " + id[i];
                    } else {
                        qry = "UPDATE categoriebs SET "
                                + "catbsgruppo = \"" + gruppoSelez + "\","
                                + "catbsspecie = \"" + mcsSpec[comboSpecie.getSelectedIndex() - 1] + "\","
                                + "catbssotto = \"" + mcsCat[comboCategorie.getSelectedIndex() - 1] + "\""
                                + " WHERE catbsid = " + id[i];
                    }
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qry);
                    qa.esecuzione();
                }
            }
            interrogazione();
        }
    }//GEN-LAST:event_buttonSalvaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        new DialogAllineaCodiciCategGruppi(new javax.swing.JFrame(), true).show();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.PopButton buttonSalva;
    private javax.swing.JComboBox comboCategorie;
    private javax.swing.JComboBox comboGruppi;
    private javax.swing.JComboBox comboSpecie;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables

    private void popolaComboGruppi() 
    {
        comboGruppi.removeAllItems();
        comboGruppi.addItem("<  .....  >");
        String qry = "SELECT DISTINCT gruppobsid,gruppobsmcs,gruppobsdescr FROM categoriebs JOIN gruppibs ON catbsgruppo = gruppobsmcs WHERE gruppobsconto = \"\""
                + " ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            comboGruppi.addItem(r.leggiStringa("gruppobsmcs") + " - " + r.leggiStringa("gruppobsdescr"));
            vmcsGru.add(r);
            r = qRic.successivo();            
        }
        qRic.chiusura();
    }

    private void popolaComboSpecie(String codgru) 
    {
        if (!codgru.equals("")) {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            String qry = "SELECT gruppobsid,gruppobsconto,gruppobsdescr FROM gruppibs WHERE gruppobssotto = \"\""
                    + " AND gruppobsmastro = \"" + codgru + "\" ORDER BY gruppobsmcs,gruppobsecc";
            QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
            qRic.apertura();
            int num = qRic.numeroRecord();
            if (num > 0) {
                mcsSpec = new String[qRic.numeroRecord()];
                for (int i = 0; i < qRic.numeroRecord(); i++) {
                    Record r = qRic.successivo();
                    comboSpecie.addItem(r.leggiStringa("gruppobsconto") + " - " + r.leggiStringa("gruppobsdescr"));
                    mcsSpec[i] = r.leggiStringa("gruppobsconto");
                }
            }
            qRic.chiusura();
        } else {
            comboSpecie.removeAllItems();
            comboSpecie.addItem("<  .....  >");
            mcsSpec = new String[0];
        }
    }

    private void popolaComboCategorie(String codgru, String codspec)
    {
        comboCategorie.removeAllItems();
        comboCategorie.addItem("<  .....  >");
        String qry = "SELECT * FROM gruppibs"
                + " WHERE ("
                + "(gruppobsmastro = \"" + codgru + "\" AND gruppobsconto = \"" + codspec + "\")"
                + " OR (gruppobsmastro > \"29\" AND gruppobsconto <> \"\"))"
                + " AND gruppobssotto <> \"\""
                + " ORDER BY gruppobsmcs";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        int nRec = qRic.numeroRecord();
        mcsCat = new String[nRec];
        for (int i = 0; i < nRec; i++) {
            Record r = qRic.successivo();
            if (r.leggiStringa("gruppobsd").equals("")) {
                comboCategorie.addItem(r.leggiStringa("gruppobssotto") + " - " + r.leggiStringa("gruppobscateg"));
            } else {
                comboCategorie.addItem(r.leggiStringa("gruppobssotto") + "/" + r.leggiStringa("gruppobsd") + " - " + r.leggiStringa("gruppobsnote"));
            }
            mcsCat[i] = r.leggiStringa("gruppobssotto");
        }
        qRic.chiusura();
    }

    private void interrogazione() 
    {
        vgruppi = new ArrayList();
        String qry = "SELECT * FROM categoriebs WHERE catbsstato = 1"
                + " ORDER BY catbsgruppo,catbsspecie,catbssotto,catbstipo,catbscod";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        while (r != null) {
            qry = "SELECT gruppobsmcs FROM gruppibs"
                    + " WHERE gruppobsmcs = \"" + r.leggiStringa("catbsgruppo") + "\""
                    + " AND gruppobsconto = \"\"";

            QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
            q.apertura();
            Record rd = q.successivo();
            q.chiusura();
            
            if (rd != null) 
            {
                r.insElem("gruppo", r.leggiStringa("catbsgruppo"));
                qry = "SELECT gruppobsdescr FROM gruppibs"
                        + " WHERE gruppobsmastro = \"" + r.leggiStringa("catbsgruppo") + "\""
                        + " AND gruppobsconto = \"" + r.leggiStringa("catbsspecie") + "\""
                        + " AND gruppobssotto = \"\"";
                q = new QueryInterrogazioneClient(connAz, qry);
                q.apertura();
                Record rsp = q.successivo();
                q.chiusura();
                if (rsp != null) 
                {
                    r.insElem("specie", rsp.leggiStringa("gruppobsdescr"));
                    qry = "SELECT gruppobscateg FROM gruppibs"
                            + " WHERE gruppobsmastro = \"" + r.leggiStringa("catbsgruppo") + "\""
                            + " AND gruppobsconto = \"" + r.leggiStringa("catbsspecie") + "\""
                            + " AND gruppobssotto = \"" + r.leggiStringa("catbssotto") + "\""
                            + " AND gruppobsd = \"\"";
                    q = new QueryInterrogazioneClient(connAz, qry);
                    q.apertura();
                    Record rs = q.successivo();
                    q.chiusura();
                    if (rs != null) {
                        r.insElem("categ", rs.leggiStringa("gruppobscateg"));
                    } else {
                        r.insElem("categ", "assente");
                    }
                } else {
                    r.insElem("specie", "errata");
                    r.insElem("categ", "assente");
                }
            } else {
                r.insElem("gruppo", "errato");
                r.insElem("specie", "errata");
                r.insElem("categ", "assente");
            }

            vgruppi.add(r);
            r = qRic.successivo();
        }
        qRic.chiusura();
        //
        mostraLista();
    }

    private void mostraLista() {
        final String[] names
                = {
                    COL_sel, COL_gru, COL_spec, COL_categ, COL_CAT, COL_Q
                };
        final Object[][] data = new Object[vgruppi.size()][names.length];

        id = new int[vgruppi.size()];

        for (int i = 0; i < vgruppi.size(); i++) {
            Record rec = vgruppi.get(i);
            data[i][0] = new Boolean(false);
            data[i][1] = rec.leggiStringa("gruppo");
            data[i][2] = rec.leggiStringa("specie");
            data[i][3] = rec.leggiStringa("categ");
            data[i][4] = rec.leggiStringa("catbsdescr");
            data[i][5] = Formattazione.formValuta(rec.leggiDouble("catbsquotaf"), 3, 2, 0);

            id[i] = rec.leggiIntero("catbsid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                return col == FunzioniUtilita.getColumnIndex(tab, COL_sel);
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPop(tab, true);
        tab.getColumn(COL_sel).setPreferredWidth(30);
        tab.getColumn(COL_gru).setPreferredWidth(72);
        tab.getColumn(COL_spec).setPreferredWidth(240);
        tab.getColumn(COL_categ).setPreferredWidth(260);
        tab.getColumn(COL_CAT).setPreferredWidth(300);
        tab.getColumn(COL_Q).setPreferredWidth(50);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_Q).setCellRenderer(rightRenderer);
    }
}
