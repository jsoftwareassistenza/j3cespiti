    package cespitipop;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import nucleo.ConnessioneServer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;
import nucleo.ExecGestioni;
import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.OpValute;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.Query;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.StampaTabella;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogic;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloGestioneAlienazioni extends javax.swing.JPanel implements PannelloPop, PannelloPopListener {

    private String COL_CODICE = "Codice";
    private String COL_DESCR = "Descrizione";
    private String COL_ANNO = "Anno inizio";
    private String COL_CLIENTE = "Cliente";
    private String COL_DOC = "Protocollo";
    private String COL_IMPVE = "Imp. ven.";

    private String COL_RESIDUO = "Residuo";
    private String COL_VBENE = "Var. Imm.";
    private String COL_PLU = "Plus/Min";
    private String COL_VFONDO = "Var. Fondo";
    private String COL_RETT = "Var";
    private String COL_NC = "Nc";
    private String COL_PN = "Pn";
    private String COL_DTALIEN = "Alienato";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;

    private String azienda = "";
    private String esContabile = "";
    private String esercSelez = "";

    private String esercizio = "";
    private int idCesp;
    private int idCat;

    private int[] id;
    private int[] idC;
    private int[] catbsid;
    private String[] vcau;

    double totbene = 0.00;
    double totfondo = 0.00;

    private PannelloPopListener listener = null;
    private PannelloCruscottoCespiti cruscotto;

    ArrayList<Record> vcespiti = new ArrayList();
    private boolean primavolta = true;
    private boolean cambia = false;

    public PannelloGestioneAlienazioni() {
        this.initComponents();
        this.setSize(1020, 680);
        this.setPreferredSize(new Dimension(1020, 680));
    }

    public PannelloGestioneAlienazioni(Window padrea, Frame formGena, PannelloCruscottoCespiti cruscotto) {
        this();
        this.padre = padrea;
        this.formGen = formGena;
        this.cruscotto = cruscotto;
    }

    public Window getPadre() {
        return padre;
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        comboCategorieBs = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        annovend = new ui.beans.CampoTestoPop();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        pPulsanti = new javax.swing.JPanel();
        buttonVenditaNoContab = new ui.beans.PopButton();
        buttonStampa1 = new ui.beans.PopButton();
        buttonNuovaVendita = new ui.beans.PopButton();
        jLabelEs = new javax.swing.JLabel();
        comboEsercizi = new archivibasepop.beans.BeanGestEserciziPop();
        buttonDocumentiVendita = new ui.beans.PopButton();
        buttonDocumentiNota = new ui.beans.PopButton();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 48));
        pFiltri.setLayout(null);

        comboCategorieBs.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        comboCategorieBs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategorieBsActionPerformed(evt);
            }
        });
        pFiltri.add(comboCategorieBs);
        comboCategorieBs.setBounds(88, 12, 440, 24);

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Categoria");
        pFiltri.add(jLabel9);
        jLabel9.setBounds(12, 12, 68, 24);

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Anno vendita");
        pFiltri.add(jLabel10);
        jLabel10.setBounds(540, 12, 100, 24);

        annovend.setCaratteri(4);
        pFiltri.add(annovend);
        annovend.setBounds(650, 12, 64, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pPulsanti.setBackground(new java.awt.Color(255, 255, 255));
        pPulsanti.setPreferredSize(new java.awt.Dimension(800, 40));
        pPulsanti.setLayout(null);

        buttonVenditaNoContab.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_to_queue_white_24dp.png"))); // NOI18N
        buttonVenditaNoContab.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonVenditaNoContab.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonVenditaNoContab.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonVenditaNoContab.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonVenditaNoContab.setToolTipText("Alienazione o vendita scollegata da contabilita', anche su esercizi contabili chiusi. Da utilizzare per raccordo procedura cespiti e contabilità.");
        buttonVenditaNoContab.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonVenditaNoContabActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonVenditaNoContab);
        buttonVenditaNoContab.setBounds(88, 4, 44, 32);

        buttonStampa1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_print_white_24dp.png"))); // NOI18N
        buttonStampa1.setFont(new java.awt.Font("Dialog", 1, 16)); // NOI18N
        buttonStampa1.setMaximumSize(new java.awt.Dimension(163, 28));
        buttonStampa1.setMinimumSize(new java.awt.Dimension(163, 28));
        buttonStampa1.setToolTipText("Stampa alienazioni");
        buttonStampa1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonStampa1ActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonStampa1);
        buttonStampa1.setBounds(260, 4, 44, 32);

        buttonNuovaVendita.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovaVendita.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovaVendita.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovaVendita.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovaVendita.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovaVendita.setToolTipText("Nuova fattura vendita/ o dismissione bene");
        buttonNuovaVendita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovaVenditaActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonNuovaVendita);
        buttonNuovaVendita.setBounds(20, 4, 48, 32);

        jLabelEs.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabelEs.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelEs.setText("Esercizio contab.");
        pPulsanti.add(jLabelEs);
        jLabelEs.setBounds(580, 4, 108, 24);
        pPulsanti.add(comboEsercizi);
        comboEsercizi.setBounds(690, 4, 144, 24);

        buttonDocumentiVendita.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_list_white_20dp.png"))); // NOI18N
        buttonDocumentiVendita.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonDocumentiVendita.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonDocumentiVendita.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonDocumentiVendita.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonDocumentiVendita.setToolTipText("Documenti di vendita");
        buttonDocumentiVendita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDocumentiVenditaActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonDocumentiVendita);
        buttonDocumentiVendita.setBounds(150, 4, 44, 32);

        buttonDocumentiNota.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_list_white_20dp.png"))); // NOI18N
        buttonDocumentiNota.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        buttonDocumentiNota.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonDocumentiNota.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonDocumentiNota.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonDocumentiNota.setToolTipText("Documenti Nota di credito");
        buttonDocumentiNota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDocumentiNotaActionPerformed(evt);
            }
        });
        pPulsanti.add(buttonDocumentiNota);
        buttonDocumentiNota.setBounds(200, 4, 44, 32);

        add(pPulsanti, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void comboCategorieBsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategorieBsActionPerformed
        if (!primavolta) {
            if (comboCategorieBs.getSelectedIndex() > 0) {
                interrogazione(catbsid[comboCategorieBs.getSelectedIndex()], -1);
            } else {
                interrogazione(-1, -1);
            }
        }

    }//GEN-LAST:event_comboCategorieBsActionPerformed

    private void buttonVenditaNoContabActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonVenditaNoContabActionPerformed

        DialogCaricamentoVenditaManualeCespite d = new DialogCaricamentoVenditaManualeCespite(formGen, true, connAz, connCom, azienda, -1, 0.00, 0.00);
        d.show();
        this.requestFocus();

    }//GEN-LAST:event_buttonVenditaNoContabActionPerformed

    private void buttonStampa1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonStampa1ActionPerformed
        boolean opz_solovend = true;
        //codrepSelez.substring(0, 4).equals("cspf")
        //stampaLibro();

        StampaTabella prn = new StampaTabella(tab); // stat è la jtable da stampare
        prn.Stampa();
        prn = null;
    }//GEN-LAST:event_buttonStampa1ActionPerformed

    private void buttonNuovaVenditaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovaVenditaActionPerformed
        Record rFattura = new Record();
        Record rFattura1 = new Record();
        Record rFattura2 = new Record();

        String qry = "SELECT * FROM caubs WHERE bscaumov = 0 AND bscaucod =\"ftven\"";
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        Record r = qRic.successivo();
        qRic.chiusura();
        if (r != null) {
            if (r.leggiStringa("bsdocjazzcod2").equals("")) {
                if (r.leggiStringa("bsdocjazzcod1").equals("")) {
                    if (!r.leggiStringa("bsdocjazzcod").equals("")) {
                        String[] campi = {"docid", "doccod", "docdescr", "doccauconid", "docsuffnumid", "doccliid", "doctipo", "doccaumagid","tdsegno"};
                        rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                        if (rFattura != null) {
                            DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 0, rFattura);
                            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                            d.setVisible(true);
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Documenti acquisto NON parametrizzati.\nImpossibile proseguire!", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    }
                } else {
                    String[] campi = {"docid", "doccod", "docdescr", "doccauconid", "docsuffnumid", "doccliid", "doctipo", "doccaumagid"};
                    rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                    if (rFattura != null) {
                        rFattura1 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod1"), campi);
                        if (rFattura1 != null) {

                        } else {
                            JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod1") + "\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    } else {
                        JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    }
                    Object[] options
                            = {
                                r.leggiStringa("bsdocjazzcod") + "  " + rFattura.leggiStringa("docdescr"), r.leggiStringa("bsdocjazzcod1") + "  " + rFattura1.leggiStringa("docdescr")
                            };
                    Object selectedValue = JOptionPane.showOptionDialog(this, "Indica il documento da utilizzare", "Avviso",
                            JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")), options, options[0]);
                    if (selectedValue.toString().equals("0")) {
                        DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 0, rFattura);
                        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                        d.setVisible(true);
                    } else if (selectedValue.toString().equals("1")) {
                        DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 0, rFattura);
                        UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                        d.setVisible(true);
                    } else {
                        rFattura = new Record();
                        rFattura1 = new Record();
                        return;
                    }
                }
            } else {
                String[] campi = {"docid", "doccod", "docdescr", "doccauconid", "docsuffnumid", "doccliid", "doctipo", "doccaumagid"};
                rFattura = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod"), campi);
                if (rFattura != null) {
                    rFattura1 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod1"), campi);
                    if (rFattura1 != null) {
                        rFattura2 = BusinessLogic.leggiRecord(connAz.leggiConnessione(), "documenti", "doccod", -1, r.leggiStringa("bsdocjazzcod2"), campi);
                        if (rFattura1 == null) {
                            JOptionPane.showMessageDialog(formGen, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod2") + "\nImpossibile proseguire!", "",
                                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                            return;
                        }
                    } else {
                        JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod1") + "\nImpossibile proseguire!", "",
                                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                        return;
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Errore NON ritrovo documento" + r.leggiStringa("bsdocjazzcod") + "\nImpossibile proseguire!", "",
                            JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
                    return;
                }
                Object[] options
                        = {
                            r.leggiStringa("bsdocjazzcod") + "  " + rFattura.leggiStringa("docdescr"), r.leggiStringa("bsdocjazzcod1") + "  " + rFattura1.leggiStringa("docdescr"), r.leggiStringa("bsdocjazzcod2") + "  " + rFattura2.leggiStringa("docdescr")
                        };
                Object selectedValue = JOptionPane.showOptionDialog(this, "Indica il documento da utilizzare", "Avviso",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")), options, options[0]);
                if (selectedValue.toString().equals("0")) {
                    DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 0, rFattura);
                    UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                    d.setVisible(true);
                } else if (selectedValue.toString().equals("1")) {
                    DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 0, rFattura);
                    UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                    d.setVisible(true);
                } else if (selectedValue.toString().equals("2")) {
                    DialogGestVenditaCespitiPop d = new DialogGestVenditaCespitiPop(formGen, true, connAz, connCom, azienda, esContabile, -1, 0, rFattura);
                    UtilForm.posRelSchermo(d, UtilForm.CENTRO);
                    d.setVisible(true);
                } else {
                    rFattura = new Record();
                    rFattura1 = new Record();
                    rFattura2 = new Record();
                    return;
                }
            }
        } else {
            JOptionPane.showMessageDialog(formGen, "Assente parametrizzazione causali vendite.\nImpossibile proseguire!", "",
                    JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
            return;
        }
    }//GEN-LAST:event_buttonNuovaVenditaActionPerformed

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
//            if (tab.getValueAt(tab.getSelectedRow(), FunzioniUtilita.getColumnIndex(tab, COL_DTALIEN)).equals("DA CONVERTIRE")) {
            Record r = vcespiti.get(tab.getSelectedRow());
            if (r.leggiIntero("cespvid") >= 0) {
                if (r.leggiIntero("cespvid") == 0) {
//                    lanciaPatchVenditaAssente(r);
                }
                else
                {
                    if(r.leggiIntero("cespid")!=-1)
                    {
                        String esercizioprece = "" + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4)) - 1)
                        + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4)) - 1);
                        String datacalcolo = "" + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4))) + "-12-31";
                        double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, r.leggiIntero("cespid"), esercizioprece, datacalcolo);
                        totbene = OpValute.arrotondaMat(vv[1], 2);
                        totfondo = OpValute.arrotondaMat(vv[3], 2);
                    }
                }
                
                if (r.leggiStringa("cespdataalien").equals("0000-00-00") && totbene == 0) 
                {                    
                    String qryupdate = "UPDATE cespiti SET cespdataalien = \"" + r.leggiStringa("mbsdata") + "\""
                            + " WHERE cespid = " + r.leggiIntero("cespid");
                    QueryAggiornamentoClient qa = new QueryAggiornamentoClient(connAz, qryupdate);
                    qa.esecuzione();
                }

                DialogRettificaConversioneVendita d = new DialogRettificaConversioneVendita(formGen, true, connAz, azienda, r.leggiIntero("cespid"));
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();
                if (comboCategorieBs.getSelectedIndex() > 0) {
                    interrogazione(catbsid[comboCategorieBs.getSelectedIndex()], -1);
                } else if (idCat > 0) {
                    interrogazione(idCat, idCesp);
                } else {
                    interrogazione(-1, -1);
                }
            }
//            }
        }
    }//GEN-LAST:event_tabMouseClicked

    private void buttonDocumentiVenditaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDocumentiVenditaActionPerformed

//        waitloc.lancia();
        PannelloRegistrazioneDocumenti p = new PannelloRegistrazioneDocumenti(formGen);
//                    p.inizializza(connAz, connCom, azienda, esContabile, id[row], null, "Ricerca Cespiti");
        listener.notificaAperturaPannello(null, "Alienazioni", p, "Documenti", "Documenti vendita", new Color(0, 77, 106), 1);
//        waitloc.ferma();
    }//GEN-LAST:event_buttonDocumentiVenditaActionPerformed

    private void buttonDocumentiNotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDocumentiNotaActionPerformed

//        waitloc.lancia();
        PannelloRegistrazioneDocumenti p = new PannelloRegistrazioneDocumenti(formGen);
//                    p.inizializza(connAz, connCom, azienda, esContabile, id[row], null, "Ricerca Cespiti");
        listener.notificaAperturaPannello(null, "Alienazioni", p, "Documenti", "Documenti nota di credito", new Color(0, 77, 106), 2);
//        waitloc.ferma();
    }//GEN-LAST:event_buttonDocumentiNotaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.CampoTestoPop annovend;
    private ui.beans.PopButton buttonDocumentiNota;
    private ui.beans.PopButton buttonDocumentiVendita;
    private ui.beans.PopButton buttonNuovaVendita;
    private ui.beans.PopButton buttonStampa1;
    private ui.beans.PopButton buttonVenditaNoContab;
    private javax.swing.JComboBox<String> comboCategorieBs;
    private archivibasepop.beans.BeanGestEserciziPop comboEsercizi;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelEs;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pFiltri;
    private javax.swing.JPanel pPulsanti;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables

    public void impostaParametri(int idCat, int idCesp, String esercizio) {
        this.idCat = idCat;
        this.idCesp = idCesp;
        this.esercizio = esercizio;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;

        impostaListener(cruscotto);

        buttonVenditaNoContab.setEnabled(false);
        buttonNuovaVendita.setEnabled(false);
        comboEsercizi.setVisible(false);
        jLabelEs.setVisible(false);

//        buttonStampa1.setVisible(false);
        String qryupdate = "UPDATE cespitiven SET vplusc=-vminusc WHERE vminusc>0.00";
        QueryAggiornamentoClient queryagg = new QueryAggiornamentoClient(connAz, qryupdate);
        queryagg.esecuzione();

        qryupdate = "UPDATE cespitiven SET vplusc=vminusc WHERE vminusc<0.00";
        queryagg = new QueryAggiornamentoClient(connAz, qryupdate);
        queryagg.esecuzione();

        qryupdate = "UPDATE cespitiven SET vminusc=0.00";
        queryagg = new QueryAggiornamentoClient(connAz, qryupdate);
        queryagg.esecuzione();

        annovend.setEventoFocusLost(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (comboCategorieBs.getSelectedIndex() > 0) {
                    interrogazione(catbsid[comboCategorieBs.getSelectedIndex()], -1);
                } else {
                    interrogazione(-1, -1);
                }
            }
        });

        //carica causali vendite e storni
        String qry = "SELECT bscauid FROM caubs WHERE"
                + " bscaucod = \"ftven\""
                + " OR bscaucod = \"bsven\""
                + " OR bscaucod = \"bseli\"";
//                + " OR bscauraggr = 3";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            vcau = new String[q.numeroRecord()];
            for (int i = 0; i < q.numeroRecord(); i++) {
                Record r = q.successivo();
                vcau[i] = "" + r.leggiIntero("bscauid");
            }
        }
        q.chiusura();

        primavolta = true;

        comboEsercizi.passaParametri(azienda, formGen, formGen);
        CambioEsercizio ce = new CambioEsercizio();
        comboEsercizi.gestioneAggiuntiva(ce);
        comboEsercizi.impostaEsercizio(esContabile);

        popolaComboCategorieBs();

        comboCategorieBs.setSelectedItem("<...............>");

        primavolta = false;

//        if (idCat > 0 || idCesp > 0 || !esContabile.equals("")) {
        if (idCat > 0 || idCesp > 0) {
            pFiltri.setVisible(false);
            pPulsanti.setVisible(false);
            esercSelez = esContabile;
            interrogazione(idCat, idCesp);
        }
    }

    @Override
    public void impostaListener(PannelloPopListener l) {
        listener = l;
    }

    private void interrogazione(int catbsid, int cespbsid) {
        if (!primavolta) {
            vcespiti = new ArrayList();

            String cespeserc = "";

            double totaleanno = 0;
            double totalefondo = 0;
            double totaleplus = 0;
            double totaleminus = 0;

            String qryx = vcau[0];
            for (int i = 1; i < vcau.length; i++) {
                qryx += "," + vcau[i];
            }
            //
            String qry = "SELECT DISTINCT cespid,cespeserc,cespcategid,cespordine,cespdescriz1,cespdatafunz,cespdataalien,cespmoviniid,mbseserc,mbsimporto,mbsdata,mbsimpbene,mbsimpofondo,mbsnote,mbsmovjazzid FROM cespiti"
                    + " INNER JOIN categoriebs ON cespcategid = catbsid"
                    + " INNER JOIN movimentibs ON cespid = mbscespid"
                    + " WHERE cespid > 0"
                    + " AND mbstipocf = 2"
                    // + " AND ((mbstipomov = 3) OR (mbstipomov < 2 AND mbsimpbene < 0) OR (mbscaumovid IN(" + qryx + ")))";
                    /*
                    (mbstipomov < 2 AND (mbsimpbene < 0 OR mbsimpofondo < 0)) sono casi della versione jazz1 
                    nella nuova versione sono svalutazioni e rivalutazioni
                    se cespdataalien = mbsdata corrisponde al caso jazz1 dobbiamo lanciare la patch di aggiustamento
                     */
                    + " AND ((mbstipomov = 3) OR (mbscaumovid IN(" + qryx + "))) AND mbsimpbene<0";
            if (catbsid > 0) {
                qry += " AND cespcategid = " + catbsid;
            }
            if (cespbsid > 0) {
                qry += " AND cespid = " + cespbsid;
            }
            if (!annovend.getText().trim().equals("")) {
                qry += " AND mbseserc = \"" + annovend.getText().trim() + annovend.getText().trim() + "\"";
            }
            qry += " ORDER BY mbseserc DESC,mbsdata,catbstipo,catbsgruppo,catbsspecie,catbssotto,catbscod,cespordine ASC";
            //        System.out.println(qry);
            // fare patch che per alienazione esista una sola riga con mbsimpbene,mbsimpofondo NON VA BENE separate
            Query qRic = new Query(connAz, qry);
            qRic.apertura();
            Record r = qRic.successivo();
            if (r != null) {
                cespeserc = r.leggiStringa("mbseserc");
            }
            while (r != null) {
                String esercizioprece = "" + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4)) - 1)
                        + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4)) - 1);
                String datacalcolo = "" + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4)) - 1) + "-12-31";
                if (!r.leggiStringa("mbseserc").equals(cespeserc)) {
                    Record rins = new Record();
                    rins.insElem("cespid", -1);
                    rins.insElem("cespcategid", -1);
                    rins.insElem("cespordine", "");
                    rins.insElem("cespdescriz1", "");
                    rins.insElem("cespdatafunz", "");
                    rins.insElem("cespdataalien", "");
                    rins.insElem("tipodoc", "");
                    rins.insElem("cespesercv", "");
                    rins.insElem("cespsezv", "");
                    rins.insElem("cespdataven", "");
                    rins.insElem("cespnumven", "");
                    rins.insElem("cespnumestv", "");
                    rins.insElem("cespcliente", "TOTALE anno " + cespeserc.substring(0, 4));
                    rins.insElem("cespnotev", "");
                    rins.insElem("cespimpvend", 0.00);
                    rins.insElem("cespqtav", 0.00);
                    rins.insElem("cespmovvjazzid", -1);
                    rins.insElem("mbsdata", "");
                    rins.insElem("mbsimpbene", totaleanno * (-1));
                    rins.insElem("mbsimpofondo", totalefondo * (-1));
                    rins.insElem("mbsnote", "");
                    rins.insElem("vplusc", totaleplus);
                    rins.insElem("minusvalenza", totaleminus);
                    rins.insElem("cespvid", -1);

                    cespeserc = r.leggiStringa("mbseserc");
                    totaleanno = 0;
                    totalefondo = 0;
                    totaleplus = 0;
                    totaleminus = 0;
                    vcespiti.add(rins);
                }

                totaleanno += (r.leggiDouble("mbsimpbene") * (-1));
                totalefondo += (r.leggiDouble("mbsimpofondo") * (-1));

                if (r.leggiIntero("mbsmovjazzid") > 0) {
                    double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, r.leggiIntero("cespid"), esercizioprece, datacalcolo);
                    totbene = OpValute.arrotondaMat(vv[1], 2);
                    totfondo = OpValute.arrotondaMat(vv[3], 2);
                    double totresiduo = totbene - totfondo;

                    String qry2 = "SELECT cespvid,vplusc,cespcliente,cespesercv,cespsezv,cespimpvend,cespnumven,cespnumestv,cespdataven,vconsistenzac,vfondoc,vresiduoc,cespnotev,cespqtav,cespmovvjazzid FROM cespitiven"
                            + " WHERE (cespftjazzid = " + r.leggiIntero("mbsmovjazzid") + " OR cespmovvjazzid = " + r.leggiIntero("mbsmovjazzid") + ")"
                            + " AND cespesercv = \"" + r.leggiStringa("mbseserc") + "\""
                            + " AND cespimpvend = " + r.leggiDouble("mbsimporto")
                            + " AND cespvcespid = " + r.leggiIntero("cespid") + " AND vconsistenzac>0";
                    QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry2);
                    query.apertura();
                    Record r2 = query.successivo();
                    query.chiusura();
                    if (r2 != null) {
                        r.insElem("cespvid", r2.leggiIntero("cespvid"));
                        r.insElem("cespesercv", r2.leggiStringa("cespesercv"));
                        r.insElem("cespsezv", r2.leggiStringa("cespsezv"));
                        r.insElem("cespdataven", r2.leggiStringa("cespdataven"));
                        r.insElem("cespnumestv", r2.leggiStringa("cespnumestv"));
                        r.insElem("cespnumven", r2.leggiStringa("cespnumven"));
                        r.insElem("cespcliente", r2.leggiStringa("cespcliente"));
                        r.insElem("cespnotev", r2.leggiStringa("cespnotev"));
                        r.insElem("cespimpvend", r2.leggiDouble("cespimpvend"));
                        r.insElem("vconsistenzac", r2.leggiDouble("vconsistenzac"));
                        r.insElem("vfondoc", r2.leggiDouble("vfondoc"));
                        r.insElem("cespmovvjazzid", r2.leggiIntero("cespmovvjazzid"));
                        double residuo = r2.leggiDouble("vconsistenzac") - r2.leggiDouble("vfondoc");
                        double plusvalenza = OpValute.arrotondaMat(r2.leggiDouble("cespimpvend") - residuo, 2);
                        if (r2.leggiDouble("vplusc") != plusvalenza) {
                            lanciaPatchPlusvalenza(r2, plusvalenza);
                        }
//                        if (totbene == r2.leggiDouble("vconsistenzac") && r2.leggiDouble("vconsistenzac") > 0) {
//                            //vendita totale                            
//                            if (r2.leggiDouble("vconsistenzac") > 0) {
//                                plusvalenza = OpValute.arrotondaMat(r2.leggiDouble("cespimpvend") - totresiduo, 2);
//                                if (r2.leggiDouble("vplusc") != plusvalenza) {
////                                    lanciaPatchPlusvalenza(r.leggiStringa("cespordine"));
//                                }
//                            }


//                        if (r.leggiStringa("cespdataalien").equals("0000-00-00")) {
//                            lanciaPatchDataVendita(r.leggiIntero("cespid"), r2.leggiStringa("cespdataven"));
//                        } //RICONTROLLA!!!
//                        }

                        totbene -= r2.leggiDouble("vconsistenzac");
                        totfondo -= r2.leggiDouble("vfondoc");

                        if (r2.leggiDouble("vplusc") > 0) {
                            r.insElem("vplusc", r2.leggiDouble("vplusc"));
                            r.insElem("minusvalenza", 0.00);
                            totaleplus += r2.leggiDouble("vplusc");
                        } else {
                            r.insElem("vplusc", 0.00);
                            r.insElem("minusvalenza", r2.leggiDouble("vplusc"));
                            totaleminus += r2.leggiDouble("vplusc");
                        }
                        r.insElem("vresiduoc", r2.leggiDouble("vresiduoc"));
                        r.insElem("cespqtav", r2.leggiDouble("cespqtav"));

                        String documento = "";
                        if (r2.leggiDouble("vconsistenzac") > 0) {
                            if (r2.leggiDouble("cespimpvend") != 0) {
                                documento = "FAT.";
                            } else {
                                documento = "P.N.";
                            }
                        } else {
                            documento = "N.C.";
                        }
                        r.insElem("tipodoc", documento);
                    } else {
                        r.insElem("cespvid", 0);
                        r.insElem("tipodoc", "Alien.");
                        r.insElem("cespesercv", "");
                        r.insElem("cespsezv", "");
                        r.insElem("cespdataven", "");
                        r.insElem("cespnumven", "");
                        r.insElem("cespnumestv", "");
                        r.insElem("cespcliente", "");
                        r.insElem("cespnotev", "");
                        r.insElem("cespimpvend", 0.00);
                        r.insElem("vplusc", 0.00);
                        r.insElem("minusvalenza", 0.00);
                        r.insElem("vconsistenzac", 0.00);
                        r.insElem("vfondoc", 0.00);
                        r.insElem("vresiduoc", 0.00);
                        r.insElem("cespqtav", 0.00);
                        r.insElem("cespmovvjazzid", -1);
                    }
                } else {
                    double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, r.leggiIntero("cespid"), esercizioprece, datacalcolo);
                    totbene = OpValute.arrotondaMat(vv[1], 2);
                    totfondo = OpValute.arrotondaMat(vv[3], 2);
                    double totresiduo = totbene - totfondo;

                    String qry2 = "SELECT cespvid,vplusc,cespcliente,cespesercv,cespsezv,cespimpvend,cespnotev,cespnumven,cespnumestv,cespdataven,vconsistenzac,vfondoc,vresiduoc,cespnotev,cespqtav,cespmovvjazzid FROM cespitiven"
                            + " WHERE cespdataven = \"" + r.leggiStringa("mbsdata") + "\""
                            + " AND cespesercv = \"" + r.leggiStringa("mbseserc") + "\" ";
                    if (r.leggiDouble("mbsimporto") != 0.00) {
                        qry2 += " AND cespimpvend = " + r.leggiDouble("mbsimporto");
                    }
                    qry2 += " AND cespvcespid = " + r.leggiIntero("cespid");
                    QueryInterrogazioneClient query = new QueryInterrogazioneClient(connAz, qry2);
                    query.apertura();
                    Record r2 = query.successivo();
                    query.chiusura();
                    if (r2 != null) {
                        r.insElem("cespvid", r2.leggiIntero("cespvid"));
                        r.insElem("cespesercv", r2.leggiStringa("cespesercv"));
                        r.insElem("cespsezv", r2.leggiStringa("cespsezv"));
                        r.insElem("cespdataven", r2.leggiStringa("cespdataven"));
//                        r.insElem("cespdataalien", r.leggiStringa("cespdataalien"));
                        r.insElem("cespnumven", r2.leggiStringa("cespnumven"));
                        r.insElem("cespnumestv", r2.leggiStringa("cespnumestv"));
                        r.insElem("cespcliente", r2.leggiStringa("cespcliente"));
                        r.insElem("cespnotev", r2.leggiStringa("cespnotev"));
                        r.insElem("cespimpvend", r2.leggiDouble("cespimpvend"));
                        r.insElem("vconsistenzac", r2.leggiDouble("vconsistenzac"));
                        r.insElem("vfondoc", r2.leggiDouble("vfondoc"));
                        r.insElem("cespmovvjazzid", r2.leggiIntero("cespmovvjazzid"));

                        double residuo = r2.leggiDouble("vconsistenzac") - r2.leggiDouble("vfondoc");
                        double plusvalenza = OpValute.arrotondaMat(r2.leggiDouble("cespimpvend") - residuo, 2);
                        if (r2.leggiDouble("vplusc") != plusvalenza) {
                            lanciaPatchPlusvalenza(r2, plusvalenza);
                        }
//                        double plusvalenza = 0;
//                        if (totbene == r2.leggiDouble("vconsistenzac") && r2.leggiDouble("vconsistenzac") > 0) {
//                            //vendita totale                            
//                            if (r2.leggiDouble("vconsistenzac") > 0) {
//                                plusvalenza = OpValute.arrotondaMat(r2.leggiDouble("cespimpvend") - totresiduo, 2);
//                                if (r2.leggiDouble("vplusc") != plusvalenza) {
////                                    lanciaPatchPlusvalenza(r.leggiStringa("cespordine"));
//                                }
//                            }
//                        }

                        totbene -= r2.leggiDouble("vconsistenzac");
                        totfondo -= r2.leggiDouble("vfondoc");

                        if (r2.leggiDouble("vplusc") > 0) {
                            r.insElem("vplusc", r2.leggiDouble("vplusc"));
                            r.insElem("minusvalenza", 0.00);
                            totaleplus += r2.leggiDouble("vplusc");
                        } else {
                            r.insElem("vplusc", 0.00);
                            r.insElem("minusvalenza", r2.leggiDouble("vplusc"));
                            totaleminus += r2.leggiDouble("vplusc");
                        }
                        r.insElem("vresiduoc", r2.leggiDouble("vresiduoc"));
                        r.insElem("cespqtav", r2.leggiDouble("cespqtav"));
                        String documento = "";
                        if (r2.leggiDouble("vconsistenzac") > 0) {
                            if (r2.leggiDouble("cespimpvend") != 0) {
                                documento = "FAT.";
                            } else {
                                documento = "P.N.";
                            }
                        } else {
                            documento = "N.C.";
                        }
                        r.insElem("tipodoc", documento);
                    } else {
                        r.insElem("cespvid", 0);
                        r.insElem("tipodoc", "Alien.");
                        r.insElem("cespesercv", "");
                        r.insElem("cespsezv", "");
                        r.insElem("cespdataven", "");
                        r.insElem("cespdataalien", "");
                        r.insElem("cespnumestv", "");
                        r.insElem("cespnumven", "");
                        r.insElem("cespcliente", "");
                        r.insElem("cespnotev", "");
                        r.insElem("cespimpvend", 0.00);
                        r.insElem("vplusc", 0.00);
                        r.insElem("minusvalenza", 0.00);
                        r.insElem("vconsistenzac", 0.00);
                        r.insElem("vfondoc", 0.00);
                        r.insElem("vresiduoc", 0.00);
                        r.insElem("cespqtav", 0.00);
                        r.insElem("cespmovvjazzid", -1);
                    }
                }
                vcespiti.add(r);

                r = qRic.successivo();
            }
            qRic.chiusura();

            if (!vcespiti.isEmpty()) {
                Record rins = new Record();
                rins.insElem("cespid", -1);
                rins.insElem("cespcategid", -1);
                rins.insElem("cespordine", "");
                rins.insElem("cespdescriz1", "");
                rins.insElem("cespdatafunz", "");
                rins.insElem("cespdataalien", "");
                rins.insElem("tipodoc", "");
                rins.insElem("cespvid", -1);
                rins.insElem("cespesercv", "");
                rins.insElem("cespsezv", "");
                rins.insElem("cespdataven", "");
                rins.insElem("cespnumven", "");
                rins.insElem("cespcliente", "TOTALE anno " + cespeserc.substring(0, 4));
                rins.insElem("cespnotev", "");
                rins.insElem("cespimpvend", 0.00);
                rins.insElem("vplusc", totaleplus);
                rins.insElem("minusvalenza", totaleminus);
                rins.insElem("mbsdata", "");
                rins.insElem("mbsimpbene", -totaleanno);
                rins.insElem("mbsimpofondo", -totalefondo);
                rins.insElem("mbsnote", "");
                rins.insElem("cespqtav", 0.00);
                rins.insElem("cespmovvjazzid", -1);

                vcespiti.add(rins);

            } else {
                JOptionPane.showMessageDialog(this, "Nessun movimento ", "Invio",
                        JOptionPane.INFORMATION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
            }

            mostraLista();
        }
    }

    private void mostraLista() {

        Data oggi = new Data();

        final String[] names = {
            COL_CODICE, COL_DESCR, COL_ANNO, COL_CLIENTE, COL_DOC, COL_VBENE, COL_VFONDO, COL_DTALIEN, COL_IMPVE, COL_RESIDUO, COL_PLU, COL_NC, COL_RETT, COL_PN
        };
        final Object[][] data = new Object[vcespiti.size()][names.length];

        id = new int[vcespiti.size()];
        idC = new int[vcespiti.size()];

        for (int i = 0; i < vcespiti.size(); i++) {
            Record r = vcespiti.get(i);
            data[i][0] = r.leggiStringa("cespordine");
            data[i][1] = r.leggiStringa("cespdescriz1");
            data[i][11] = "";
            
            if (r.leggiIntero("cespid") > 0) {
                data[i][2] = r.leggiStringa("cespeserc").substring(0, 4) + "  "
                        + (new Data(r.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                data[i][3] = r.leggiStringa("cespcliente") + " - " + r.leggiStringa("cespnotev");
                if (!r.leggiStringa("cespsezv").equals("")) {
                    data[i][4] = r.leggiStringa("tipodoc") + " " + r.leggiStringa("cespsezv") + "/" + r.leggiStringa("cespnumven")
                            + " del " + (new Data(r.leggiStringa("mbsdata"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                } else {
                    data[i][4] = r.leggiStringa("tipodoc") + "/del " + (new Data(r.leggiStringa("mbsdata"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                }
                data[i][5] = Formattazione.formValuta(r.leggiDouble("mbsimpbene"), 12, 2, 2);
                data[i][6] = Formattazione.formValuta(r.leggiDouble("mbsimpofondo"), 12, 2, 2);
                
                
                String esercizioprece = "" + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4)))
                    + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4)));
                String datacalcolo = "" + (Formattazione.estraiIntero(r.leggiStringa("mbsdata").substring(0, 4))) + "-12-31";
                double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, r.leggiIntero("cespid"), esercizioprece, datacalcolo);
                totbene = OpValute.arrotondaMat(vv[1], 2);
                totfondo = OpValute.arrotondaMat(vv[3], 2);
                double residuo = totbene - totfondo;
                String datacalcolodopo="" + (Formattazione.estraiIntero(datacalcolo.substring(0, 4))+1) + "-12-31";
                double vvdopo[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, r.leggiIntero("cespid"), esercizioprece, datacalcolodopo);
                double totbenedopo = OpValute.arrotondaMat(vvdopo[1], 2);
                if (r.leggiStringa("cespdataalien").equals("0000-00-00") && totbene == 0.0 && totbenedopo==0.0) {
                    data[i][7] = "DA CONVERTIRE";
                } else if (r.leggiStringa("cespdataalien").equals("0000-00-00") && totbene != 0.0 && totbenedopo!=0.0) {
                    data[i][7] = "";
                } else {
                    data[i][7] = (new Data(r.leggiStringa("cespdataalien"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                }
                data[i][8] = Formattazione.formValuta(r.leggiDouble("cespimpvend"), 12, 2, 2);
                data[i][9] = Formattazione.formValuta(r.leggiDouble("vresiduoc"), 12, 2, 0);
                if (r.leggiDouble("vplusc") > 0) {
                    data[i][10] = Formattazione.formValuta(r.leggiDouble("vplusc"), 12, 2, 3);
                } else {
                    data[i][10] = Formattazione.formValuta(r.leggiDouble("minusvalenza"), 12, 2, 3);
                }
            } else {
                if (r.leggiStringa("cespesercv").length() > 4) {
                    data[i][2] = r.leggiStringa("cespesercv").substring(0, 4);
                } else {
                    data[i][2] = r.leggiStringa("cespesercv");
                }
                data[i][3] = r.leggiStringa("cespcliente");
                if (!r.leggiStringa("mbsdata").equals("")) {
                    data[i][4] = (new Data(r.leggiStringa("mbsdata"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
                } else {
                    data[i][4] = "";
                }
                data[i][5] = Formattazione.formValuta(r.leggiDouble("mbsimpbene"), 12, 2, 2);
                data[i][6] = Formattazione.formValuta(r.leggiDouble("mbsimpofondo"), 12, 2, 2);
                data[i][7] = "";
                data[i][8] = "";
                data[i][9] = "";
                if (r.leggiDouble("vplusc") > 0) {
                    data[i][10] = Formattazione.formValuta(r.leggiDouble("vplusc"), 12, 2, 3);
                } else {
                    data[i][10] = Formattazione.formValuta(r.leggiDouble("minusvalenza"), 12, 2, 3);
                }
            }

            id[i] = r.leggiIntero("cespid");
            idC[i] = r.leggiIntero("cespcategid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_RETT)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_NC)
                        || col == FunzioniUtilita.getColumnIndex(tab, COL_PN)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tab, true);

        TableColumn buttonColumn1 = tab.getColumn(COL_RETT);
        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespid") != -1) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_create_white_20dp.png")));
                    button.setToolTipText("Modifica valori senza interagire con prima nota. ATTENZIONE!");
                    button.setEnabled(true);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim20x20/ic_create_white_20dp.png")));
                    button.setToolTipText("Modifica valori senza interagire con prima nota. ATTENZIONE!");
                    button.setEnabled(false);
                }

            }
        });
        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record rvar = vcespiti.get(row);
                int modo = 0;
                int tipo = -1;
                if (rvar.leggiDouble("cespimpvend") >= 0) {
                    tipo = 1;
                } else {
                    tipo = 2;
                }

                if (rvar.leggiIntero("cespvid") == 0) {
                    modo = 1;//solo movimentobs
                } else {
                    modo = 2;
                }

                DialogUtilitaModificaVendita d = new DialogUtilitaModificaVendita(formGen, true, connAz, azienda, rvar, modo, tipo);
                UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                d.show();

                if (comboCategorieBs.getSelectedIndex() > 0) {
                    interrogazione(catbsid[comboCategorieBs.getSelectedIndex()], -1);
                } else if (idCat > 0) {
                    interrogazione(idCat, idCesp);
                } else {
                    interrogazione(-1, -1);
                }

            }
        });
        buttonColumn1.setCellRenderer(button1);
        buttonColumn1.setCellEditor(button1);

        TableColumn buttonColumn2 = tab.getColumn(COL_PN);
        TablePopButton button2 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.leggiIntero("cespmovvjazzid") < 1 && r.leggiIntero("cespvid") >= 0) {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_compare_arrows_white_24dp.png")));
                    button.setToolTipText("Collega a contabilizzazione");
                    button.setEnabled(true);
                } else {
                    button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_compare_arrows_white_24dp.png")));
                    button.setToolTipText("Legame esistente id del documento: " + r.leggiIntero("cespmovvjazzid"));
                    button.setEnabled(false);
                }
            }
        });
        button2.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                if (r.esisteCampo("cespvid")) {
                    if (r.leggiIntero("cespmovvjazzid") < 1 && r.leggiIntero("cespvid") >= 0) {
                        int modo = 0;
                        if (r.leggiIntero("cespvid") == 0) {
                            modo = 1;//solo movimentobs
                        } else {
                            modo = 2;
                        }
                        DialogUtilitaLegaMovimentoJazzVendita d = new DialogUtilitaLegaMovimentoJazzVendita(formGen, true, connAz, azienda, r, modo);
                        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                        d.show();
                        if (comboCategorieBs.getSelectedIndex() > 0) {
                            interrogazione(catbsid[comboCategorieBs.getSelectedIndex()], -1);
                        } else if (idCat > 0) {
                            interrogazione(idCat, idCesp);
                        } else {
                            interrogazione(-1, -1);
                        }
                    }
                }
            }
        });
        buttonColumn2.setCellRenderer(button2);
        buttonColumn2.setCellEditor(button2);

        TableColumn buttonColumn3 = tab.getColumn(COL_NC);
        TablePopButton button3 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                Record r = vcespiti.get(row);
                if (r.esisteCampo("cespvid")) {
                    if (r.leggiIntero("cespvid") > 0 && r.leggiDouble("cespimpvend") >= 0) {
                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_remove_circle_white_24dp.png")));
                        button.setToolTipText("Registra Nota credito cliente su riga documento vendita");
                        button.setEnabled(true);
                    } else {
                        button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_remove_circle_white_24dp.png")));
                        button.setToolTipText("");
                        button.setEnabled(false);
                    }
                }
            }
        });
        button3.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                Record r = vcespiti.get(row);
                if (r.esisteCampo("cespvid")) {
                    if (r.leggiIntero("cespvid") > 0 && r.leggiDouble("vconsistenzac") > 0) {
                        int modo = 1;
                        int tipo = 2;
                        DialogUtilitaModificaVendita d = new DialogUtilitaModificaVendita(formGen, true, connAz, azienda, r, modo, tipo);
                        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
                        d.show();
                        if (comboCategorieBs.getSelectedIndex() > 0) {
                            interrogazione(catbsid[comboCategorieBs.getSelectedIndex()], -1);
                        } else if (idCat > 0) {
                            interrogazione(idCat, idCesp);
                        } else {
                            interrogazione(-1, -1);
                        }
                    }
                }
            }
        });
        buttonColumn3.setCellRenderer(button3);
        buttonColumn3.setCellEditor(button3);

        tab.getColumn(COL_CODICE).setPreferredWidth(92);
        tab.getColumn(COL_DESCR).setPreferredWidth(300);
        tab.getColumn(COL_ANNO).setPreferredWidth(110);
        tab.getColumn(COL_CLIENTE).setPreferredWidth(240);
        tab.getColumn(COL_DOC).setPreferredWidth(130);
        tab.getColumn(COL_VBENE).setPreferredWidth(88);
        tab.getColumn(COL_VFONDO).setPreferredWidth(88);
        tab.getColumn(COL_DTALIEN).setPreferredWidth(90);
        tab.getColumn(COL_IMPVE).setPreferredWidth(88);
        tab.getColumn(COL_RESIDUO).setPreferredWidth(88);
        tab.getColumn(COL_PLU).setPreferredWidth(80);
        tab.getColumn(COL_NC).setPreferredWidth(36);
        tab.getColumn(COL_RETT).setPreferredWidth(36);
        tab.getColumn(COL_PN).setPreferredWidth(36);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_IMPVE).setCellRenderer(rightRenderer);
        tab.getColumn(COL_VBENE).setCellRenderer(rightRenderer);
        tab.getColumn(COL_PLU).setCellRenderer(rightRenderer);
        tab.getColumn(COL_VFONDO).setCellRenderer(rightRenderer);
        tab.getColumn(COL_RESIDUO).setCellRenderer(rightRenderer);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        tab.getColumn(COL_ANNO).setCellRenderer(centerRenderer);
        tab.getColumn(COL_DTALIEN).setCellRenderer(centerRenderer);
    }

    public void popolaComboCategorieBs() {
        comboCategorieBs.removeAllItems();
        comboCategorieBs.addItem("< ............... >");
        String qry = "SELECT catbsid,catbscod,catbsdescr FROM categoriebs"
                + " WHERE catbsstato = 1"
                + " ORDER BY catbsgruppo,catbsspecie,catbssotto,catbscod";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        if (q.numeroRecord() > 0) {
            catbsid = new int[q.numeroRecord() + 1];
            catbsid[0] = -1;
            for (int i = 0; i < q.numeroRecord(); i++) {
                Record rl = q.successivo();
                catbsid[i + 1] = rl.leggiIntero("catbsid");
                comboCategorieBs.addItem(rl.leggiStringa("catbscod") + "-" + rl.leggiStringa("catbsdescr"));
            }
        }
    }

    @Override
    public void notificaChiusura(PannelloPop origine, String key) {

    }

    @Override
    public void notificaCambioTitolo(PannelloPop origine, String key, String nuovoTitolo) {

    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo) {

    }

    @Override
    public void notificaAperturaPannello(PannelloPop origine, String keyorigine, PannelloPop nuovoPannello, String keynuovo, String nuovoTitolo, Color coloresfondo, int idVis) {

    }

    class CambioEsercizio implements ExecGestioni {

        public void lancia() {
            esercSelez = comboEsercizi.leggiEsercizio();

            if (esercSelez.compareTo(esContabile) == 0) {
                buttonNuovaVendita.setEnabled(true);
                buttonVenditaNoContab.setVisible(true);
            } else {
                buttonNuovaVendita.setEnabled(false);
                buttonVenditaNoContab.setVisible(true);
            }
            if (comboCategorieBs.getSelectedIndex() > 0) {
                interrogazione(catbsid[comboCategorieBs.getSelectedIndex()], -1);
            } else {
                interrogazione(-1, -1);
            }
        }
    }

    private void lanciaPatchPlusvalenza(Record rmod, double plus) {
//        JOptionPane.showMessageDialog(formGen, "Plusvalenza/minusvalenza errate! Modifica l'alienazione\ncespite n. ordine " + cespitecodice, "",
//                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/error.png")));
        int idc = rmod.leggiIntero("cespvid");
        String qry = "UPDATE cespitiven SET vplusc = " + plus + " WHERE cespvid = " + idc;
        QueryAggiornamentoClient qmod = new QueryAggiornamentoClient(connAz, qry);
        qmod.esecuzione();

    }

    private void lanciaPatchDataVendita(int idc, String dataven) {
        String qry = "UPDATE cespiti SET cespdataalien = \"" + dataven + "\" WHERE cespid = " + idc;
        QueryAggiornamentoClient qmod = new QueryAggiornamentoClient(connAz, qry);
        qmod.esecuzione();
    }

    private void lanciaPatchVenditaAssente(Record rassente) {
        String esercizioprece = "" + (Formattazione.estraiIntero(rassente.leggiStringa("mbsdata").substring(0, 4)))
                + (Formattazione.estraiIntero(rassente.leggiStringa("mbsdata").substring(0, 4)));
        String datacalcolo = "" + (Formattazione.estraiIntero(rassente.leggiStringa("mbsdata").substring(0, 4))) + "-12-31";
        double vv[] = BusinessLogicCespiti.calcoloImmobilizzazioneSintetica(connAz.conn, azienda, rassente.leggiIntero("cespid"), esercizioprece, datacalcolo);
        totbene = OpValute.arrotondaMat(vv[1], 2);
        totfondo = OpValute.arrotondaMat(vv[3], 2);
        Record ra1 = new Record();
        ra1.insElem("cespvcespid", rassente.leggiIntero("cespid"));
        ra1.insElem("cespesercv", rassente.leggiStringa("mbseserc"));
        ra1.insElem("vconsistenzac", totbene);
        ra1.insElem("vfondoc", totfondo);
        ra1.insElem("vresiduoc", totbene - totfondo);
        ra1.insElem("cespimpvend", rassente.leggiDouble("mbsimporto"));
        ra1.insElem("cespqtav", 0.00);
        double plus = rassente.leggiDouble("mbsimporto") - totbene + totfondo;
        if (plus > 0) {
            ra1.insElem("vplusc", plus);
        } else {
            ra1.insElem("vplusc", plus * -1);
        }
        ra1.insElem("cespmovvjazzid", -1);
        ra1.insElem("cespnotev", rassente.leggiStringa("mbsnote"));
        ra1.insElem("cespdataven", rassente.leggiStringa("mbsdata"));
        ra1.insElem("cespnumven", "");
        ra1.insElem("cespsezv", "");
        ra1.insElem("cespcliente", -1);
        QueryAggiornamentoClient qi = new QueryAggiornamentoClient(connAz, QueryAggiornamentoClient.INS, "cespitiven", "cespvid", ra1);
        qi.esecuzione();
    }
}
