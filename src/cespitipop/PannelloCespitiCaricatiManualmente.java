package cespitipop;

import java.awt.*;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import nucleo.ConnessioneServer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import nucleo.Data;
import nucleo.Formattazione;
import nucleo.FunzioniUtilita;
import nucleo.OpValute;
import nucleo.PannelloPop;
import nucleo.PannelloPopListener;
import nucleo.QueryAggiornamentoClient;
import nucleo.QueryInterrogazioneClient;
import nucleo.Record;
import nucleo.SimpleHeaderRenderer;
import nucleo.UtilForm;
import nucleo.WaitPop;
import nucleo.businesslogic.BusinessLogicAmbiente;
import nucleo.businesslogic.BusinessLogicImpostazioni;
import nucleo.businesslogic.cespiti.BusinessLogicCespiti;
import ui.beans.PopButton;
import ui.beans.TablePopButton;
import ui.eventi.GestoreEventiBeans;

public class PannelloCespitiCaricatiManualmente extends javax.swing.JPanel implements PannelloPop {

    private String COL_DESCR = "Descrizione";
    private String COL_ANNO = "Anno";
    private String COL_VALORE = "Valore";
    private String COL_AMM = "Ammort.";
    private String COL_FONDO = "Fondo";
    private String COL_RESIDUO = "Residuo";
    private String COL_NOTE = "Note";
    private String COL_ELI = "Elim.";

    private ConnessioneServer connAz = null;
    private ConnessioneServer connCom = null;

    private Window padre;
    private Frame formGen;

    private int idVis = -1;
//    private int flagSoglia = 0;
    private int idSelez = -1;

    private String azienda = "";
    private String esContabile = "";

    private int[] id;
    private int[] idC;

    ArrayList<Record> vcespiti = new ArrayList();
    ArrayList<Record> vcatbsid = new ArrayList();

    private boolean altrifiltri = false;

    SimpleHeaderRenderer headerRenderer = new SimpleHeaderRenderer();

    String[] dtes = null;

    Data oggi = new Data();

    public PannelloCespitiCaricatiManualmente() {
        this.initComponents();
        this.setSize(1120, 680);
        this.setPreferredSize(new Dimension(1120, 680));
    }

    public PannelloCespitiCaricatiManualmente(Window padrea, Frame formGen) {
        this();
        this.padre = padrea;
        this.formGen = formGen;
    }

    public Window getPadre() {
        return padre;
    }

    @Override
    public void inizializza(ConnessioneServer connAz, ConnessioneServer connCom, String azienda, String esContabile, int idVis, WaitPop wait, String key) {
        this.connAz = connAz;
        this.connCom = connCom;
        this.azienda = azienda;
        this.esContabile = esContabile;
        this.idVis = idVis;

        jLabel11.setVisible(false);
        annofine.setVisible(false);
        jLabel12.setVisible(false);
        buttonChiudiCaricamento.setVisible(false);

        //
        dtes = BusinessLogicAmbiente.leggiEsercizio(azienda, esContabile);
        popolaComboCategorieBs();
        comboCategorieBs.setSelectedIndex(0);

        String annoinizjazz = (BusinessLogicImpostazioni.leggiProprieta(connAz.conn, "cespiti_jazz", "biliniz"));
        int aaf = Formattazione.estraiIntero(annoinizjazz);
        annofine.setText("" + (aaf - 1));//primo anno

//        annofine.setEventoKeyReleased(new GestoreEventiBeans() {
//            @Override
//            public void lancia()
//            {
//                if (comboCategorieBs.getSelectedIndex() > 0
//                        ) {
//                    Record rcateg = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1);
//                    interrogazione(rcateg.leggiIntero("catbsid"));
//                } else 
//                {
//                    interrogazione(-1);
//                }
//            }
//        });
        descr.setEventoKeyReleased(new GestoreEventiBeans() {
            @Override
            public void lancia() {
                if (comboCategorieBs.getSelectedIndex() > 0) {
                    Record rcateg = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1);
                    interrogazione(rcateg.leggiIntero("catbsid"));
                } else {
                    interrogazione(-1);
                }
            }
        });

        annofine.setEnabled(false);
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pFiltri = new javax.swing.JPanel();
        comboCategorieBs = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        descr = new ui.beans.CampoTestoPop();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        annofine = new ui.beans.CampoInteroPop();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        catbsspecie = new javax.swing.JLabel();
        buttonNuovoCaricamento = new ui.beans.PopButton();
        buttonChiudiCaricamento = new ui.beans.PopButton();
        jLabel12 = new javax.swing.JLabel();

        setName(""); // NOI18N
        setPreferredSize(new java.awt.Dimension(1020, 700));
        setLayout(new java.awt.BorderLayout());

        pFiltri.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        pFiltri.setMinimumSize(new java.awt.Dimension(0, 40));
        pFiltri.setPreferredSize(new java.awt.Dimension(600, 80));
        pFiltri.setLayout(null);

        comboCategorieBs.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        comboCategorieBs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboCategorieBsActionPerformed(evt);
            }
        });
        pFiltri.add(comboCategorieBs);
        comboCategorieBs.setBounds(88, 8, 440, 24);

        jLabel9.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Categoria");
        pFiltri.add(jLabel9);
        jLabel9.setBounds(12, 8, 68, 24);

        descr.setBackground(new java.awt.Color(255, 255, 255));
        pFiltri.add(descr);
        descr.setBounds(636, 8, 308, 24);

        jLabel10.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Descrizione");
        pFiltri.add(jLabel10);
        jLabel10.setBounds(540, 8, 88, 24);

        jLabel11.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Registrati fino all'anno");
        pFiltri.add(jLabel11);
        jLabel11.setBounds(672, 44, 184, 24);

        annofine.setInteri(4);
        pFiltri.add(annofine);
        annofine.setBounds(864, 44, 80, 24);

        add(pFiltri, java.awt.BorderLayout.PAGE_START);

        tab.setBackground(new java.awt.Color(255, 255, 232));
        tab.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tab);

        add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 40));
        jPanel1.setLayout(null);

        catbsspecie.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        catbsspecie.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        catbsspecie.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(10, 150, 150)));
        catbsspecie.setOpaque(true);
        jPanel1.add(catbsspecie);
        catbsspecie.setBounds(88, 8, 440, 24);

        buttonNuovoCaricamento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_add_circle_white_24dp.png"))); // NOI18N
        buttonNuovoCaricamento.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonNuovoCaricamento.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonNuovoCaricamento.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonNuovoCaricamento.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonNuovoCaricamento.setToolTipText("Inserisci nuovo cespite");
        buttonNuovoCaricamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuovoCaricamentoActionPerformed(evt);
            }
        });
        jPanel1.add(buttonNuovoCaricamento);
        buttonNuovoCaricamento.setBounds(16, 4, 56, 32);

        buttonChiudiCaricamento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_lock_white_24dp.png"))); // NOI18N
        buttonChiudiCaricamento.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        buttonChiudiCaricamento.setMaximumSize(new java.awt.Dimension(45, 22));
        buttonChiudiCaricamento.setMinimumSize(new java.awt.Dimension(45, 22));
        buttonChiudiCaricamento.setPreferredSize(new java.awt.Dimension(45, 22));
        buttonChiudiCaricamento.setToolTipText("Nuova categoria");
        buttonChiudiCaricamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonChiudiCaricamentoActionPerformed(evt);
            }
        });
        jPanel1.add(buttonChiudiCaricamento);
        buttonChiudiCaricamento.setBounds(1008, 4, 56, 32);

        jLabel12.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Chiudi il caricamento manuale ed effettua le chiusure esercizio");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(564, 8, 436, 24);

        add(jPanel1, java.awt.BorderLayout.PAGE_END);
    }// </editor-fold>//GEN-END:initComponents

    private void tabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMouseClicked
        if (evt.getClickCount() == 2 && tab.getSelectedRow() >= 0) {
            idSelez = id[tab.convertRowIndexToModel(tab.getSelectedRow())];
            DialogAnagraficaCespite d = new DialogAnagraficaCespite(formGen, true, connAz, connCom, azienda, esContabile, idSelez);
            UtilForm.posRelSchermo(d, UtilForm.CENTRO);
            d.show();
        }
    }//GEN-LAST:event_tabMouseClicked

    private void comboCategorieBsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboCategorieBsActionPerformed

        if (comboCategorieBs.getSelectedIndex() > 0) {
            Record rcateg = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1);
            switch (rcateg.leggiIntero("catbstipo")) {
                case 2:
                    catbsspecie.setText("Immobilizzazioni MATERIALI");
                    break;
                case 3:
                    catbsspecie.setText("Immobilizzazioni IMMATERIALI");
                    break;
                default:
                    catbsspecie.setText("ASSENTE");
                    break;
            }

            interrogazione(rcateg.leggiIntero("catbsid"));
        } else {
            catbsspecie.setText("");
            interrogazione(-1);
        }
    }//GEN-LAST:event_comboCategorieBsActionPerformed

    private void buttonNuovoCaricamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuovoCaricamentoActionPerformed
        int categ = -1;
        int aa = 2019;
        if (comboCategorieBs.getSelectedIndex() > 0) {
            Record rcateg = vcatbsid.get(comboCategorieBs.getSelectedIndex() - 1);
            categ = rcateg.leggiIntero("catbsid");
        }

        if (annofine.getValoreIntero() > 0) {
            aa = annofine.getValoreIntero();
        }

        DialogCaricamentoStoricoCespiti d = new DialogCaricamentoStoricoCespiti(formGen, true, connAz, azienda, esContabile, categ, aa);
        UtilForm.posRelSchermo((Window) d, UtilForm.CENTRO);
        d.show();
        requestFocus();

        interrogazione(-1);
    }//GEN-LAST:event_buttonNuovoCaricamentoActionPerformed

    private void buttonChiudiCaricamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonChiudiCaricamentoActionPerformed

        JOptionPane.showMessageDialog(this, "Ho chiuso il caricamento.\n Vai sul menu' principale cespiti\n effettua la chiusura esercizio " + annofine.getText().trim(), "Invio",
                JOptionPane.ERROR_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/info.png")));
        this.requestFocus();
    }//GEN-LAST:event_buttonChiudiCaricamentoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private ui.beans.CampoInteroPop annofine;
    private ui.beans.PopButton buttonChiudiCaricamento;
    private ui.beans.PopButton buttonNuovoCaricamento;
    private javax.swing.JLabel catbsspecie;
    private javax.swing.JComboBox<String> comboCategorieBs;
    private ui.beans.CampoTestoPop descr;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel pFiltri;
    private javax.swing.JTable tab;
    // End of variables declaration//GEN-END:variables

    @Override
    public void impostaListener(PannelloPopListener listener) {

    }

    private void interrogazione(int catbsid) {
        vcespiti = new ArrayList();
        String dataregord = (String) oggi.formatta(Data.AAAA_MM_GG);

        String qry = "SELECT cespid,cespeserc,cespcategid,cespordine,cespdescriz1,cespdatafunz,cespdataalien,cespmoviniid FROM cespiti"
                + " WHERE cespintero = 1";

        if (catbsid > 0) {
            qry += " AND cespcategid = " + catbsid;
        }

        if (!descr.getText().trim().equals("")) {
            qry += " AND cespdescriz1 LIKE \"%" + (String) descr.getText().trim() + "%\"";
        }

//        if (annofine.getValoreIntero() > 0)
//        {
//            qry += " AND cespeserc <= \"" + "" + annofine.getValoreIntero() + "" + annofine.getValoreIntero() + "\"";
//        }
        qry += " ORDER BY cespeserc,cespdatafunz,cespcategid,cespordine";
//        System.out.println(qry);
        QueryInterrogazioneClient qRic = new QueryInterrogazioneClient(connAz, qry);
        qRic.apertura();
        int num = qRic.numeroRecord();
        for (int i = 0; i < num; i++) {
            Record r = qRic.successivo();
            String qrya = "SELECT cespfornit,cespeserca,cespvaloreacqc FROM cespitiacq WHERE cespacespid = " + r.leggiIntero("cespid")
                    + " ORDER BY cespeserca,cespdataacq";
            QueryInterrogazioneClient qRa = new QueryInterrogazioneClient(connAz, qrya);
            qRa.apertura();
            Record ra = qRa.successivo();
            qRa.chiusura();

            if (ra != null) {
                r.insElem("cespeserca", ra.leggiStringa("cespeserca"));
                r.insElem("cespfornit", ra.leggiStringa("cespfornit"));
                r.insElem("cespvaloreacqc", ra.leggiDouble("cespvaloreacqc"));
            } else {
                r.insElem("cespeserca", r.leggiStringa("cespeserc"));
                r.insElem("cespfornit", "");
                r.insElem("cespvaloreacqc", (double) 0.00);
            }

            double vv[] = BusinessLogicCespiti.calcoloValoriAllaData(connAz.conn, azienda, r.leggiIntero("cespid"), r.leggiIntero("cespmoviniid"), dataregord);
            double totbene = OpValute.arrotondaMat(vv[8], 2);
            double totfondo = OpValute.arrotondaMat(vv[9], 2);
            double totresiduo = totbene - totfondo;
            r.insElem("totbene", totbene);
            r.insElem("totfondo", totfondo);
            r.insElem("totresiduo", totresiduo);
            double vf[] = BusinessLogicCespiti.calcoloFondo(connAz.conn, azienda, r.leggiIntero("cespid"), "", dataregord);
            double ammortamento = OpValute.arrotondaMat(vf[1], 2);
//            double variaz = totfondo - ammortamento;
            r.insElem("ammortamento", ammortamento);
//            r.insElem("variaz", variaz);

            vcespiti.add(r);
        }
        qRic.chiusura();

        mostraLista();
    }

    private void mostraLista() {
        String dataregord = (String) oggi.formatta(Data.AAAA_MM_GG);

        final String[] names
                = {
                    COL_DESCR, COL_ANNO, COL_VALORE, COL_AMM, COL_FONDO, COL_RESIDUO, COL_NOTE, COL_ELI
                };
        final Object[][] data = new Object[vcespiti.size()][names.length];

        id = new int[vcespiti.size()];
        idC = new int[vcespiti.size()];

        for (int i = 0; i < vcespiti.size(); i++) {
            Record r = vcespiti.get(i);

            data[i][0] = r.leggiStringa("cespordine") + " - " + r.leggiStringa("cespdescriz1");

            if (!r.nullo("cespeserca")) {
                data[i][1] = r.leggiStringa("cespeserca").substring(0, 4) + "  "
                        + (new Data(r.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            } else {
                data[i][1] = r.leggiStringa("cespeserc").substring(0, 4) + "  "
                        + (new Data(r.leggiStringa("cespdatafunz"), Data.AAAA_MM_GG)).formatta(Data.GG_MM_AAAA, "/");
            }

            data[i][2] = Formattazione.formValuta(r.leggiDouble("totbene"), 12, 2, 1);
            data[i][3] = Formattazione.formValuta(r.leggiDouble("ammortamento"), 12, 2, 1);
            data[i][4] = Formattazione.formValuta(r.leggiDouble("totfondo"), 12, 2, 1);
            data[i][5] = Formattazione.formValuta(r.leggiDouble("totresiduo"), 12, 2, 1);
            String note = "";
            if (r.leggiStringa("cespdatafunz").compareTo(dataregord) > 0) {
                note = "non ancora in funzione";
            } else if (r.leggiStringa("cespdataalien").compareTo("0000-00-00") != 0 && r.leggiStringa("cespdataalien").compareTo(dtes[0]) < 0) {
                note = "venduto anni precedenti";
            } else if (r.leggiStringa("cespdataalien").compareTo("0000-00-00") != 0) {
                note = "venduto nell'anno";
            } else if (r.leggiDouble("totbene") == r.leggiDouble("totfondo")) {
                note = "ammortizzato";
            } else if (r.leggiStringa("cespdatafunz").substring(0, 4).equals(esContabile.substring(0, 4))) {
                note = "primo anno";
            } else {
                ;
            }

            data[i][6] = note;

            id[i] = r.leggiIntero("cespid");
            idC[i] = r.leggiIntero("cespcategid");
        }

        TableModel dataModel = new AbstractTableModel() {
            public int getColumnCount() {
                return names.length;
            }

            public int getRowCount() {
                return data.length;
            }

            public Object getValueAt(int row, int col) {
                return data[row][col];
            }

            public String getColumnName(int column) {
                return names[column];
            }

            public Class getColumnClass(int col) {
                return getValueAt(0, col).getClass();
            }

            public boolean isCellEditable(int row, int col) {
                if (col == FunzioniUtilita.getColumnIndex(tab, COL_ELI)) {
                    return true;
                } else {
                    return false;
                }
            }

            public void setValueAt(Object aValue, int row, int column) {
                data[row][column] = aValue;
            }
        };
        tab.setModel(dataModel);
        FunzioniUtilita.impostaTabellaPopNoColor(tab, true);

        tab.getColumn(COL_DESCR).setPreferredWidth(400);
        tab.getColumn(COL_ANNO).setPreferredWidth(120);
        tab.getColumn(COL_VALORE).setPreferredWidth(110);
        tab.getColumn(COL_AMM).setPreferredWidth(110);
        tab.getColumn(COL_FONDO).setPreferredWidth(110);
        tab.getColumn(COL_RESIDUO).setPreferredWidth(110);
        tab.getColumn(COL_NOTE).setPreferredWidth(200);
        tab.getColumn(COL_ELI).setPreferredWidth(40);

        TableColumn buttonColumn1 = tab.getColumn(COL_ELI);
        TablePopButton button1 = new TablePopButton(new TablePopButton.TableButtonCustomizer() {
            public void customize(PopButton button, int row, int column) {
                button.setIcon(new javax.swing.ImageIcon(getClass().getResource("/popimg/iconepng/dim24x24/ic_delete_white_24dp.png")));
                button.setToolTipText("Elimina TUTTI i dati del cespite");
            }
        });
        button1.addHandler(new TablePopButton.TableButtonPressedHandler() {
            @Override
            public void onButtonPress(int row, int column) {
                eliminaCespiteCompletamente(id[row]);
                if (comboCategorieBs.getSelectedIndex() > 0) {
                    interrogazione(idC[row]);
                } else {
                    interrogazione(-1);
                }
            }
        });

        buttonColumn1.setCellRenderer(button1);
        buttonColumn1.setCellEditor(button1);

        tab.getColumn(COL_ELI).setHeaderRenderer(headerRenderer);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        tab.getColumn(COL_AMM).setCellRenderer(rightRenderer);
        tab.getColumn(COL_VALORE).setCellRenderer(rightRenderer);
        tab.getColumn(COL_FONDO).setCellRenderer(rightRenderer);
        tab.getColumn(COL_RESIDUO).setCellRenderer(rightRenderer);
    }

    public void popolaComboCategorieBs() {
        comboCategorieBs.removeAllItems();
        comboCategorieBs.addItem("<  scegli categoria  >");
        vcatbsid = new ArrayList();
        String qry = "SELECT catbsid,catbstipo,catbscod,catbsdescr FROM categoriebs"
                + " WHERE catbsstato = 1"
                + " ORDER BY catbsgruppo,catbsspecie,catbstipo,catbssotto,catbsdescr";
        QueryInterrogazioneClient q = new QueryInterrogazioneClient(connAz, qry);
        q.apertura();
        Record rl = q.successivo();
        while (rl != null) {
            vcatbsid.add(rl);
            comboCategorieBs.addItem(rl.leggiStringa("catbscod") + "-" + rl.leggiStringa("catbsdescr"));
            rl = q.successivo();
        }
        q.chiusura();
    }

    private void eliminaCespiteCompletamente(int idcespite) {
        int ris = JOptionPane.showConfirmDialog(this, "Vuoi davvero ELIMINARE completamente il cespite? ",
                "Conferma", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, new javax.swing.ImageIcon(getClass().getResource("/popimg/dialogs/confirm.png")));
        if (ris == JOptionPane.YES_OPTION) {
            String qrydelete = "DELETE FROM cespiti WHERE cespid = " + idcespite;
            QueryAggiornamentoClient qi = new QueryAggiornamentoClient(connAz, qrydelete);
            qi.esecuzione();

            qrydelete = "DELETE FROM cespitiacq WHERE cespacespid = " + idcespite;
            QueryAggiornamentoClient qi1 = new QueryAggiornamentoClient(connAz, qrydelete);
            qi1.esecuzione();

            qrydelete = "DELETE FROM cespitiven WHERE cespvcespid = " + idcespite;
            QueryAggiornamentoClient qi2 = new QueryAggiornamentoClient(connAz, qrydelete);
            qi2.esecuzione();

            qrydelete = "DELETE FROM movimentibs WHERE mbscespid = " + idcespite;
            QueryAggiornamentoClient qi3 = new QueryAggiornamentoClient(connAz, qrydelete);
            qi3.esecuzione();
        }
    }
}
